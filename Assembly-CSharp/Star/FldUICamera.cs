﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000390 RID: 912
	public class FldUICamera : MonoBehaviour
	{
		// Token: 0x06001139 RID: 4409 RVA: 0x0005A5A0 File Offset: 0x000589A0
		public static void CreateUICamera()
		{
			float num = 1.7786666f;
			float num2 = (float)Screen.width / (float)Screen.height;
			float num3 = num / num2 * 750f;
			GameObject gameObject = new GameObject("UI_Camera");
			Camera camera = gameObject.AddComponent<Camera>();
			camera.orthographic = true;
			camera.orthographicSize = num3 * 0.5f;
			camera.cullingMask = LayerMask.GetMask(new string[]
			{
				"FldUI"
			});
			camera.depth = 100f;
			camera.nearClipPlane = 0f;
			camera.farClipPlane = 100f;
			camera.clearFlags = CameraClearFlags.Nothing;
		}

		// Token: 0x0600113A RID: 4410 RVA: 0x0005A634 File Offset: 0x00058A34
		public static Vector3 CalcCanvasPos(Camera pcamera, Vector3 pworldpos)
		{
			float num = 1.7786666f;
			float num2 = (float)Screen.width / (float)Screen.height;
			float num3 = num / num2 * 750f;
			Vector3 vector = pcamera.WorldToViewportPoint(pworldpos);
			Vector3 zero = Vector3.zero;
			zero.x = 1334f * vector.x - 667f;
			zero.y = num3 * vector.y - num3 * 0.5f;
			return zero;
		}

		// Token: 0x0600113B RID: 4411 RVA: 0x0005A6A4 File Offset: 0x00058AA4
		public static Vector3 CalcScreenCanvasPos(Vector2 pscreen)
		{
			float num = 1.7786666f;
			float num2 = (float)Screen.width / (float)Screen.height;
			float num3 = num / num2 * 750f;
			Vector2 zero = Vector2.zero;
			zero.x = pscreen.x / (float)Screen.width;
			zero.y = pscreen.y / (float)Screen.height;
			Vector3 zero2 = Vector3.zero;
			zero2.x = 1334f * zero.x - 667f;
			zero2.y = num3 * zero.y - num3 * 0.5f;
			return zero2;
		}

		// Token: 0x04001813 RID: 6163
		private const float WD_KEY = 1334f;

		// Token: 0x04001814 RID: 6164
		private const float HI_KEY = 750f;
	}
}
