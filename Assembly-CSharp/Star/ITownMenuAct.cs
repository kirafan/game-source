﻿using System;

namespace Star
{
	// Token: 0x020006AC RID: 1708
	public interface ITownMenuAct
	{
		// Token: 0x0600221B RID: 8731
		void SetUp(TownBuilder pbuild);

		// Token: 0x0600221C RID: 8732
		void UpdateMenu(TownObjHandleMenu pmenu);
	}
}
