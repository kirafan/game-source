﻿using System;

namespace Star
{
	// Token: 0x02000269 RID: 617
	[Serializable]
	public struct WordLibraryListDB_Param
	{
		// Token: 0x040013E3 RID: 5091
		public string m_TitleName;

		// Token: 0x040013E4 RID: 5092
		public string m_Descript;
	}
}
