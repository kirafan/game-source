﻿using System;
using FieldPartyMemberResponseTypes;

namespace Star
{
	// Token: 0x0200058B RID: 1419
	public class RoomComAPIFriendFieldPartyGetAll : INetComHandle
	{
		// Token: 0x06001BA9 RID: 7081 RVA: 0x000928DD File Offset: 0x00090CDD
		public RoomComAPIFriendFieldPartyGetAll()
		{
			this.ApiName = "player/field_party/member/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}

		// Token: 0x0400228A RID: 8842
		public long playerId;
	}
}
