﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004F8 RID: 1272
	public interface IMissionFunction
	{
		// Token: 0x0600191F RID: 6431
		bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist);
	}
}
