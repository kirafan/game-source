﻿using System;
using Star.UI.BackGround;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x0200040B RID: 1035
	public class EditState_LimitBreakResult : EditState
	{
		// Token: 0x060013AE RID: 5038 RVA: 0x00069485 File Offset: 0x00067885
		public EditState_LimitBreakResult(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013AF RID: 5039 RVA: 0x0006949D File Offset: 0x0006789D
		public override int GetStateID()
		{
			return 13;
		}

		// Token: 0x060013B0 RID: 5040 RVA: 0x000694A1 File Offset: 0x000678A1
		public override void OnStateEnter()
		{
			this.m_MixEffectScene = this.m_Owner.MixEffectScenes[1];
			this.m_Step = EditState_LimitBreakResult.eStep.StartFadeOut;
		}

		// Token: 0x060013B1 RID: 5041 RVA: 0x000694BD File Offset: 0x000678BD
		public override void OnStateExit()
		{
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x000694BF File Offset: 0x000678BF
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x000694C8 File Offset: 0x000678C8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_LimitBreakResult.eStep.StartFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_LimitBreakResult.eStep.StartFadeWait;
				break;
			case EditState_LimitBreakResult.eStep.StartFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = EditState_LimitBreakResult.eStep.First;
				}
				break;
			case EditState_LimitBreakResult.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_LimitBreakResult.eStep.ChildSceneLoadWait;
				break;
			case EditState_LimitBreakResult.eStep.ChildSceneLoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_Owner.LimitBreakResult.m_CharaMngID);
					CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
					this.m_MixEffectScene.Prepare(param.m_CharaID);
					this.m_Step = EditState_LimitBreakResult.eStep.EffectLoadWait;
				}
				break;
			case EditState_LimitBreakResult.eStep.EffectLoadWait:
				if (this.m_MixEffectScene.IsPrepareError())
				{
					this.m_Step = EditState_LimitBreakResult.eStep.PrepareError;
				}
				else if (this.m_MixEffectScene.IsCompletePrepare())
				{
					this.m_Step = EditState_LimitBreakResult.eStep.Setup;
				}
				break;
			case EditState_LimitBreakResult.eStep.Setup:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Hide(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(-1f, null);
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Effect);
					this.m_Step = EditState_LimitBreakResult.eStep.Main_0;
				}
				break;
			case EditState_LimitBreakResult.eStep.Main_0:
			{
				Vector2 vector;
				bool flag = InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0);
				if (flag || this.m_MixEffectScene.IsCompletePlay())
				{
					this.m_Step = EditState_LimitBreakResult.eStep.Main_1;
					if (flag)
					{
						this.m_MixEffectScene.OnSkip();
					}
				}
				break;
			}
			case EditState_LimitBreakResult.eStep.Main_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_UI.PlayIn();
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Loop);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.2f, null);
					this.m_Step = EditState_LimitBreakResult.eStep.Main_2;
				}
				break;
			case EditState_LimitBreakResult.eStep.Main_2:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_NextState = 11;
					this.m_Step = EditState_LimitBreakResult.eStep.EndFadeOut;
				}
				break;
			case EditState_LimitBreakResult.eStep.EndFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_LimitBreakResult.eStep.EndFadeWait;
				break;
			case EditState_LimitBreakResult.eStep.EndFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_LimitBreakResult.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_LimitBreakResult.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_MixEffectScene.Suspend();
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Show(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(0.2f, null);
					this.m_Step = EditState_LimitBreakResult.eStep.None;
					return this.m_NextState;
				}
				break;
			case EditState_LimitBreakResult.eStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.m_Step = EditState_LimitBreakResult.eStep.PrepareErrorWait;
				break;
			}
			return -1;
		}

		// Token: 0x060013B4 RID: 5044 RVA: 0x0006982C File Offset: 0x00067C2C
		public bool SetupUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<LimitBreakResultUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.LimitBreakResult);
			return true;
		}

		// Token: 0x060013B5 RID: 5045 RVA: 0x00069886 File Offset: 0x00067C86
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001AA6 RID: 6822
		private EditState_LimitBreakResult.eStep m_Step = EditState_LimitBreakResult.eStep.None;

		// Token: 0x04001AA7 RID: 6823
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.LimitBreakResultUI;

		// Token: 0x04001AA8 RID: 6824
		public LimitBreakResultUI m_UI;

		// Token: 0x04001AA9 RID: 6825
		public MixEffectScene m_MixEffectScene;

		// Token: 0x04001AAA RID: 6826
		private const float FADE_SEC = 0.2f;

		// Token: 0x0200040C RID: 1036
		private enum eStep
		{
			// Token: 0x04001AAC RID: 6828
			None = -1,
			// Token: 0x04001AAD RID: 6829
			StartFadeOut,
			// Token: 0x04001AAE RID: 6830
			StartFadeWait,
			// Token: 0x04001AAF RID: 6831
			First,
			// Token: 0x04001AB0 RID: 6832
			ChildSceneLoadWait,
			// Token: 0x04001AB1 RID: 6833
			EffectLoadWait,
			// Token: 0x04001AB2 RID: 6834
			Setup,
			// Token: 0x04001AB3 RID: 6835
			Main_0,
			// Token: 0x04001AB4 RID: 6836
			Main_1,
			// Token: 0x04001AB5 RID: 6837
			Main_2,
			// Token: 0x04001AB6 RID: 6838
			EndFadeOut,
			// Token: 0x04001AB7 RID: 6839
			EndFadeWait,
			// Token: 0x04001AB8 RID: 6840
			UnloadChildSceneWait,
			// Token: 0x04001AB9 RID: 6841
			PrepareError,
			// Token: 0x04001ABA RID: 6842
			PrepareErrorWait
		}
	}
}
