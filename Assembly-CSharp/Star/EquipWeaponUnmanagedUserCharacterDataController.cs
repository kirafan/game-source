﻿using System;

namespace Star
{
	// Token: 0x020002D6 RID: 726
	public class EquipWeaponUnmanagedUserCharacterDataController : EquipWeaponCharacterDataControllerExistExGen<EquipWeaponUnmanagedUserCharacterDataController, UserCharacterDataWrapper>
	{
		// Token: 0x06000E2E RID: 3630 RVA: 0x0004D8E7 File Offset: 0x0004BCE7
		public EquipWeaponUnmanagedUserCharacterDataController() : base(eCharacterDataControllerType.UnmanagedUserCharacterData)
		{
		}

		// Token: 0x06000E2F RID: 3631 RVA: 0x0004D8F0 File Offset: 0x0004BCF0
		public EquipWeaponUnmanagedUserCharacterDataController Setup(UserCharacterData userCharacterData, UserWeaponData userWeaponData = null)
		{
			base.SetupBase(-1, -1);
			return this.Apply(userCharacterData, userWeaponData);
		}

		// Token: 0x06000E30 RID: 3632 RVA: 0x0004D903 File Offset: 0x0004BD03
		private EquipWeaponUnmanagedUserCharacterDataController Apply(UserCharacterData userCharacterData, UserWeaponData userWeaponData)
		{
			base.ApplyCharacterDataEx(new UserCharacterDataWrapper(userCharacterData));
			this.weaponData = userWeaponData;
			return (EquipWeaponUnmanagedUserCharacterDataController)this.ApplyBase(-1, -1);
		}

		// Token: 0x06000E31 RID: 3633 RVA: 0x0004D926 File Offset: 0x0004BD26
		protected override EquipWeaponPartyMemberController ApplyBase(int in_partyIndex, int in_partySlotIndex)
		{
			return this;
		}

		// Token: 0x06000E32 RID: 3634 RVA: 0x0004D929 File Offset: 0x0004BD29
		public UserWeaponData GetWeaponData()
		{
			return this.weaponData;
		}

		// Token: 0x06000E33 RID: 3635 RVA: 0x0004D934 File Offset: 0x0004BD34
		public override int GetWeaponIdNaked()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.WeaponID;
			}
			return result;
		}

		// Token: 0x06000E34 RID: 3636 RVA: 0x0004D960 File Offset: 0x0004BD60
		public override int GetWeaponSkillID()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.SkillLearnData.SkillID;
			}
			return result;
		}

		// Token: 0x06000E35 RID: 3637 RVA: 0x0004D994 File Offset: 0x0004BD94
		public override sbyte GetWeaponSkillExpTableID()
		{
			sbyte result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.SkillLearnData.ExpTableID;
			}
			return result;
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x0004D9C8 File Offset: 0x0004BDC8
		public override int GetWeaponLv()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.Lv;
			}
			return result;
		}

		// Token: 0x06000E37 RID: 3639 RVA: 0x0004D9F4 File Offset: 0x0004BDF4
		public override int GetWeaponMaxLv()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.MaxLv;
			}
			return result;
		}

		// Token: 0x06000E38 RID: 3640 RVA: 0x0004DA20 File Offset: 0x0004BE20
		public override long GetWeaponExp()
		{
			return this.weaponData.Param.Exp;
		}

		// Token: 0x06000E39 RID: 3641 RVA: 0x0004DA32 File Offset: 0x0004BE32
		public override int GetWeaponSkillLv()
		{
			return this.weaponData.Param.SkillLearnData.SkillLv;
		}

		// Token: 0x06000E3A RID: 3642 RVA: 0x0004DA49 File Offset: 0x0004BE49
		public override int GetWeaponSkillMaxLv()
		{
			return this.weaponData.Param.SkillLearnData.SkillMaxLv;
		}

		// Token: 0x06000E3B RID: 3643 RVA: 0x0004DA60 File Offset: 0x0004BE60
		public override EditUtility.OutputCharaParam GetWeaponParam()
		{
			return EditUtility.CalcWeaponParam(this.weaponData);
		}

		// Token: 0x04001597 RID: 5527
		protected UserWeaponData weaponData;
	}
}
