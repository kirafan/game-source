﻿using System;
using Star.UI.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x02000713 RID: 1811
	public class TownMakerPoint : MonoBehaviour
	{
		// Token: 0x1700025B RID: 603
		// (get) Token: 0x060023D2 RID: 9170 RVA: 0x000C09CA File Offset: 0x000BEDCA
		public int Index
		{
			get
			{
				return this.m_Index;
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x060023D3 RID: 9171 RVA: 0x000C09D2 File Offset: 0x000BEDD2
		public int OrderInLayer
		{
			get
			{
				return this.m_OrderInLayer;
			}
		}

		// Token: 0x060023D4 RID: 9172 RVA: 0x000C09DA File Offset: 0x000BEDDA
		private void Awake()
		{
		}

		// Token: 0x060023D5 RID: 9173 RVA: 0x000C09DC File Offset: 0x000BEDDC
		public void SetEnable(bool flg)
		{
			if (flg)
			{
				this.m_Sprite.enabled = true;
				this.m_Arrow.SetActive(true);
				iTween.ValueTo(base.gameObject, iTween.Hash(new object[]
				{
					"from",
					0f,
					"to",
					1f,
					"time",
					0.75f,
					"loopType",
					iTween.LoopType.pingPong,
					"onupdate",
					"SetEnableColor"
				}));
			}
			else
			{
				this.m_Sprite.enabled = false;
				this.m_Arrow.SetActive(false);
				iTween.Stop(base.gameObject);
			}
		}

		// Token: 0x060023D6 RID: 9174 RVA: 0x000C0AA7 File Offset: 0x000BEEA7
		private void SetEnableColor(float value)
		{
			this.m_Sprite.color = Color.Lerp(new Color(1f, 0f, 1f), new Color(0.7f, 0f, 0.7f), value);
		}

		// Token: 0x04002AD0 RID: 10960
		[SerializeField]
		private SpriteRenderer m_Sprite;

		// Token: 0x04002AD1 RID: 10961
		[SerializeField]
		private TownBuildArrow m_Arrow;

		// Token: 0x04002AD2 RID: 10962
		[SerializeField]
		private int m_Index;

		// Token: 0x04002AD3 RID: 10963
		[SerializeField]
		private int m_OrderInLayer;
	}
}
