﻿using System;

namespace Star
{
	// Token: 0x0200071F RID: 1823
	public class TownBuildChangeAreaParam : ITownEventCommand
	{
		// Token: 0x0600240A RID: 9226 RVA: 0x000C18CA File Offset: 0x000BFCCA
		public TownBuildChangeAreaParam(int fbuildpoint, long fmanageid, int fobjid)
		{
			this.m_Type = eTownRequest.ChangeArea;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
		}

		// Token: 0x0600240B RID: 9227 RVA: 0x000C18F0 File Offset: 0x000BFCF0
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			TownBuildLinkPoint buildPointTreeNode = pbuilder.GetBuildTree().GetBuildPointTreeNode(this.m_BuildPointID);
			if (buildPointTreeNode != null)
			{
				buildPointTreeNode.m_Pakage.GetHandle().RecvEvent(new TownHandleActionChangeObj(this.m_ObjID, this.m_ManageID));
			}
			return this.m_Enable;
		}

		// Token: 0x04002AFF RID: 11007
		public int m_BuildPointID;

		// Token: 0x04002B00 RID: 11008
		public int m_ObjID;

		// Token: 0x04002B01 RID: 11009
		public long m_ManageID;
	}
}
