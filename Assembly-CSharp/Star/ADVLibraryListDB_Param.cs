﻿using System;

namespace Star
{
	// Token: 0x0200015D RID: 349
	[Serializable]
	public struct ADVLibraryListDB_Param
	{
		// Token: 0x0400094D RID: 2381
		public int m_LibraryListID;

		// Token: 0x0400094E RID: 2382
		public int m_Category;

		// Token: 0x0400094F RID: 2383
		public string m_ListName;
	}
}
