﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x02000489 RID: 1161
	public class ShopState_TradeSale : ShopState
	{
		// Token: 0x060016BA RID: 5818 RVA: 0x00076A19 File Offset: 0x00074E19
		public ShopState_TradeSale(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016BB RID: 5819 RVA: 0x00076A31 File Offset: 0x00074E31
		public override int GetStateID()
		{
			return 4;
		}

		// Token: 0x060016BC RID: 5820 RVA: 0x00076A34 File Offset: 0x00074E34
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_TradeSale.eStep.First;
		}

		// Token: 0x060016BD RID: 5821 RVA: 0x00076A3D File Offset: 0x00074E3D
		public override void OnStateExit()
		{
		}

		// Token: 0x060016BE RID: 5822 RVA: 0x00076A3F File Offset: 0x00074E3F
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016BF RID: 5823 RVA: 0x00076A48 File Offset: 0x00074E48
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_TradeSale.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_TradeSale.eStep.LoadWait;
				break;
			case ShopState_TradeSale.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeSale.eStep.PlayIn;
				}
				break;
			case ShopState_TradeSale.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_TradeSale.eStep.Main;
				}
				break;
			case ShopState_TradeSale.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 2;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeSale.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_TradeSale.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_TradeSale.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016C0 RID: 5824 RVA: 0x00076B6C File Offset: 0x00074F6C
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopTradeSaleListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickExecuteSaleButton += this.OnClickSaleButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.ItemSale);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060016C1 RID: 5825 RVA: 0x00076C0B File Offset: 0x0007500B
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060016C2 RID: 5826 RVA: 0x00076C1A File Offset: 0x0007501A
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060016C3 RID: 5827 RVA: 0x00076C36 File Offset: 0x00075036
		private void OnClickSaleButtonCallBack(int itemID, int amount)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_ItemSale(itemID, amount, new Action<MeigewwwParam, Itemsale>(this.OnResponse_ItemSale));
		}

		// Token: 0x060016C4 RID: 5828 RVA: 0x00076C58 File Offset: 0x00075058
		private void OnResponse_ItemSale(MeigewwwParam wwwParam, Itemsale param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				this.m_UI.Refresh();
				this.m_UI.CloseConfirm();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_SELL, 1f, 0, -1, -1);
			}
		}

		// Token: 0x04001D74 RID: 7540
		private ShopState_TradeSale.eStep m_Step = ShopState_TradeSale.eStep.None;

		// Token: 0x04001D75 RID: 7541
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopTradeSaleListUI;

		// Token: 0x04001D76 RID: 7542
		private ShopTradeSaleListUI m_UI;

		// Token: 0x0200048A RID: 1162
		private enum eStep
		{
			// Token: 0x04001D78 RID: 7544
			None = -1,
			// Token: 0x04001D79 RID: 7545
			First,
			// Token: 0x04001D7A RID: 7546
			LoadStart,
			// Token: 0x04001D7B RID: 7547
			LoadWait,
			// Token: 0x04001D7C RID: 7548
			PlayIn,
			// Token: 0x04001D7D RID: 7549
			Main,
			// Token: 0x04001D7E RID: 7550
			UnloadChildSceneWait
		}
	}
}
