﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000D4 RID: 212
	public class BattleOption
	{
		// Token: 0x0600059D RID: 1437 RVA: 0x0001D03C File Offset: 0x0001B43C
		public void Setup(BattleTimeScaler scaler, LocalSaveData localSaveData)
		{
			this.m_Scaler = scaler;
			this.m_LocalSaveData = localSaveData;
			if (this.m_LocalSaveData != null)
			{
				this.m_IsAuto = this.m_LocalSaveData.BattleAutoMode;
				this.m_IsSkipTogetherAttackInput = this.m_LocalSaveData.BattleSkipTogetherAttackInput;
			}
			this.SetSpeedLv(LocalSaveData.eBattleSpeedLv.PL, this.GetSpeedLv(LocalSaveData.eBattleSpeedLv.PL), false);
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x0001D093 File Offset: 0x0001B493
		public bool GetIsAuto()
		{
			return this.m_IsAuto;
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x0001D09C File Offset: 0x0001B49C
		public void SetIsAuto(bool flg)
		{
			this.m_IsAuto = flg;
			if (this.m_IsAuto)
			{
				Screen.sleepTimeout = -1;
			}
			else
			{
				Screen.sleepTimeout = -2;
			}
			if (this.m_LocalSaveData != null)
			{
				this.m_LocalSaveData.BattleAutoMode = this.m_IsAuto;
			}
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x0001D0E9 File Offset: 0x0001B4E9
		public bool ToggleAuto()
		{
			this.SetIsAuto(!this.m_IsAuto);
			return this.m_IsAuto;
		}

		// Token: 0x060005A1 RID: 1441 RVA: 0x0001D100 File Offset: 0x0001B500
		public int GetSpeedLv(LocalSaveData.eBattleSpeedLv type)
		{
			if (this.m_LocalSaveData != null)
			{
				return this.m_LocalSaveData.GetBattleSpeedLv(type);
			}
			return 0;
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x0001D11C File Offset: 0x0001B51C
		public void SetSpeedLv(LocalSaveData.eBattleSpeedLv type, int lv, bool isDirtyUpdate = true)
		{
			lv %= 2;
			if (this.m_LocalSaveData != null)
			{
				this.m_LocalSaveData.SetBattleSpeedLv(type, lv);
			}
			if (!isDirtyUpdate)
			{
				this.m_Scaler.SetBaseTimeScale(LocalSaveData.BATTLE_SPEED_LV_SCALE[lv]);
			}
			else
			{
				this.m_Scaler.IsDirty = true;
			}
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x0001D170 File Offset: 0x0001B570
		public int ToggleSpeedLv(LocalSaveData.eBattleSpeedLv type, bool isDirtyUpdate = true)
		{
			if (this.m_LocalSaveData != null)
			{
				int num = this.m_LocalSaveData.ToggleBattleSpeedLv(type);
				this.SetSpeedLv(type, num, isDirtyUpdate);
				return num;
			}
			return 0;
		}

		// Token: 0x060005A4 RID: 1444 RVA: 0x0001D1A1 File Offset: 0x0001B5A1
		public bool GetIsSkipTogetherAttackInput()
		{
			return this.m_IsSkipTogetherAttackInput;
		}

		// Token: 0x060005A5 RID: 1445 RVA: 0x0001D1A9 File Offset: 0x0001B5A9
		public void SetIsSkipTogetherAttackInput(bool flg)
		{
			this.m_IsSkipTogetherAttackInput = flg;
			if (this.m_LocalSaveData != null)
			{
				this.m_LocalSaveData.BattleSkipTogetherAttackInput = this.m_IsSkipTogetherAttackInput;
			}
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x0001D1CE File Offset: 0x0001B5CE
		public bool ToggleIsSkipTogetherAttackInput()
		{
			this.SetIsSkipTogetherAttackInput(!this.m_IsSkipTogetherAttackInput);
			return this.m_IsSkipTogetherAttackInput;
		}

		// Token: 0x0400047E RID: 1150
		private LocalSaveData m_LocalSaveData;

		// Token: 0x0400047F RID: 1151
		private BattleTimeScaler m_Scaler;

		// Token: 0x04000480 RID: 1152
		private bool m_IsAuto;

		// Token: 0x04000481 RID: 1153
		private bool m_IsSkipTogetherAttackInput;
	}
}
