﻿using System;

namespace Star
{
	// Token: 0x02000107 RID: 263
	public enum eSkillActionEventType
	{
		// Token: 0x040006BE RID: 1726
		Solve,
		// Token: 0x040006BF RID: 1727
		DamageAnim,
		// Token: 0x040006C0 RID: 1728
		DamageEffect,
		// Token: 0x040006C1 RID: 1729
		EffectPlay,
		// Token: 0x040006C2 RID: 1730
		EffectProjectile_Straight,
		// Token: 0x040006C3 RID: 1731
		EffectProjectile_Parabola,
		// Token: 0x040006C4 RID: 1732
		EffectProjectile_Penetrate,
		// Token: 0x040006C5 RID: 1733
		EffectAttach,
		// Token: 0x040006C6 RID: 1734
		EffectDetach,
		// Token: 0x040006C7 RID: 1735
		TrailAttach,
		// Token: 0x040006C8 RID: 1736
		TrailDetach,
		// Token: 0x040006C9 RID: 1737
		WeaponThrow_Parabola,
		// Token: 0x040006CA RID: 1738
		WeaponVisible,
		// Token: 0x040006CB RID: 1739
		WeaponReset,
		// Token: 0x040006CC RID: 1740
		PlaySE,
		// Token: 0x040006CD RID: 1741
		PlayVoice,
		// Token: 0x040006CE RID: 1742
		Num
	}
}
