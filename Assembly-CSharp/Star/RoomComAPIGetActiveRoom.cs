﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x02000590 RID: 1424
	public class RoomComAPIGetActiveRoom : INetComHandle
	{
		// Token: 0x06001BAE RID: 7086 RVA: 0x000929AF File Offset: 0x00090DAF
		public RoomComAPIGetActiveRoom()
		{
			this.ApiName = "player/room/active/get";
			this.Request = false;
			this.ResponseType = typeof(Getactiveroom);
		}
	}
}
