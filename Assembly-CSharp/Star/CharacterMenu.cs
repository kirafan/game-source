﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000150 RID: 336
	public class CharacterMenu
	{
		// Token: 0x06000966 RID: 2406 RVA: 0x0003BE30 File Offset: 0x0003A230
		public void Update()
		{
			CharacterMenu.eMode mode = this.m_Mode;
			if (mode == CharacterMenu.eMode.Idle)
			{
				this.UpdateIdleMode();
			}
		}

		// Token: 0x06000967 RID: 2407 RVA: 0x0003BE5C File Offset: 0x0003A25C
		public void Setup(CharacterHandler charaHndl)
		{
			this.m_Owner = charaHndl;
			this.m_Owner.CharaAnim.ChangeDir(CharacterDefine.eDir.L, false);
			Transform[] componentsInChildren = this.m_Owner.GetComponentsInChildren<Transform>();
			int layer = LayerMask.NameToLayer("UI");
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].gameObject.layer = layer;
			}
			this.m_BodyID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_Owner.CharaParam.CharaID).m_BodyID;
			this.RequestIdleMode(0f, 0f);
		}

		// Token: 0x06000968 RID: 2408 RVA: 0x0003BEFD File Offset: 0x0003A2FD
		public void Destroy()
		{
			this.m_Owner = null;
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000969 RID: 2409 RVA: 0x0003BF06 File Offset: 0x0003A306
		public CharacterMenu.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x0003BF10 File Offset: 0x0003A310
		public void RequestIdleMode(float blendSec = 0f, float delaySec = 0f)
		{
			this.m_Mode = CharacterMenu.eMode.Idle;
			this.m_UpdateIdleStep = CharacterMenu.eUpdateIdleStep.ExecAnimation_Wait;
			this.m_BlendSec = blendSec;
			string actionKey;
			if (this.m_Owner.CharaAnim.Dir == CharacterDefine.eDir.L)
			{
				if (this.m_BodyID == 1)
				{
					actionKey = "room_idle_skirt_L";
				}
				else
				{
					actionKey = "room_idle_L";
				}
			}
			else if (this.m_BodyID == 1)
			{
				actionKey = "room_idle_skirt_R";
			}
			else
			{
				actionKey = "room_idle_R";
			}
			if (!this.m_Owner.CharaAnim.IsPlayingAnim(actionKey))
			{
				this.m_Owner.CharaAnim.PlayAnim(actionKey, WrapMode.Loop, this.m_BlendSec, 0f);
			}
			this.m_Owner.CharaAnim.SetBlink(true);
			int num = (this.m_Owner.CharaAnim.Dir != CharacterDefine.eDir.L) ? 1 : 0;
			this.m_Owner.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_BLINK_BATTLE[num, 0]);
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x0003C008 File Offset: 0x0003A408
		private void UpdateIdleMode()
		{
			CharacterMenu.eUpdateIdleStep updateIdleStep = this.m_UpdateIdleStep;
			if (updateIdleStep != CharacterMenu.eUpdateIdleStep.ExecAnimation_Wait)
			{
			}
		}

		// Token: 0x040008EB RID: 2283
		private CharacterHandler m_Owner;

		// Token: 0x040008EC RID: 2284
		private float m_BlendSec;

		// Token: 0x040008ED RID: 2285
		private int m_BodyID;

		// Token: 0x040008EE RID: 2286
		private CharacterMenu.eMode m_Mode;

		// Token: 0x040008EF RID: 2287
		private CharacterMenu.eUpdateIdleStep m_UpdateIdleStep = CharacterMenu.eUpdateIdleStep.None;

		// Token: 0x040008F0 RID: 2288
		public const float DEFAULT_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x02000151 RID: 337
		public enum eMode
		{
			// Token: 0x040008F2 RID: 2290
			None = -1,
			// Token: 0x040008F3 RID: 2291
			Idle
		}

		// Token: 0x02000152 RID: 338
		private enum eUpdateIdleStep
		{
			// Token: 0x040008F5 RID: 2293
			None = -1,
			// Token: 0x040008F6 RID: 2294
			ExecAnimation_Wait,
			// Token: 0x040008F7 RID: 2295
			End
		}
	}
}
