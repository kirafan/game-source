﻿using System;
using FieldPartyMemberResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200034D RID: 845
	public class FieldPartyAPIChangeScheduleAll : INetComHandle
	{
		// Token: 0x0600101E RID: 4126 RVA: 0x00055CD7 File Offset: 0x000540D7
		public FieldPartyAPIChangeScheduleAll()
		{
			this.ApiName = "player/field_party/member/change_schedule_all";
			this.Request = true;
			this.ResponseType = typeof(ChangescheduleAll);
		}

		// Token: 0x0400172D RID: 5933
		public ChangeScheduleMember[] changeScheduleMembers;
	}
}
