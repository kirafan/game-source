﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000261 RID: 609
	public class WeaponExpDB : ScriptableObject
	{
		// Token: 0x040013C3 RID: 5059
		public WeaponExpDB_Param[] m_Params;
	}
}
