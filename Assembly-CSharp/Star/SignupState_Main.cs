﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.BackGround;
using WWWTypes;

namespace Star
{
	// Token: 0x0200049B RID: 1179
	public class SignupState_Main : SignupState
	{
		// Token: 0x06001720 RID: 5920 RVA: 0x0007854C File Offset: 0x0007694C
		public SignupState_Main(SignupMain owner) : base(owner)
		{
		}

		// Token: 0x06001721 RID: 5921 RVA: 0x0007855C File Offset: 0x0007695C
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x0007855F File Offset: 0x0007695F
		public override void OnStateEnter()
		{
			this.m_Step = SignupState_Main.eStep.First;
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x00078568 File Offset: 0x00076968
		public override void OnStateExit()
		{
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x0007856A File Offset: 0x0007696A
		public override void OnDispose()
		{
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x0007856C File Offset: 0x0007696C
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case SignupState_Main.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Owner.SignupUI.OnClickDecide += this.OnClickDecide;
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Quest, false);
					this.m_Step = SignupState_Main.eStep.WaitLoadBG;
				}
				break;
			case SignupState_Main.eStep.WaitLoadBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = SignupState_Main.eStep.FadeIn_Wait;
				}
				break;
			case SignupState_Main.eStep.FadeIn_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Owner.SignupUI.PlayIn();
					this.m_Step = SignupState_Main.eStep.Main;
				}
				break;
			case SignupState_Main.eStep.QuestGetAll:
				SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestGetAll(new Action(this.OnResponse_QuestGetAll), true);
				this.m_Step = SignupState_Main.eStep.QuestGetAll_Wait;
				break;
			case SignupState_Main.eStep.Mission:
				SingletonMonoBehaviour<GameSystem>.Inst.MissionMng.Prepare();
				this.m_Step = SignupState_Main.eStep.Mission_Wait;
				break;
			case SignupState_Main.eStep.Mission_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.MissionMng.IsDonePrepare())
				{
					this.m_Step = SignupState_Main.eStep.TransitNextScene;
				}
				break;
			case SignupState_Main.eStep.TransitNextScene:
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				this.m_Step = SignupState_Main.eStep.TransitNextScene_Wait;
				break;
			case SignupState_Main.eStep.TransitNextScene_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
					this.m_Step = SignupState_Main.eStep.None;
					if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq() == eTutorialSeq.SignupComplete_NextIsPresentGet)
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Present);
					}
					else
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Town);
					}
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001726 RID: 5926 RVA: 0x000787A1 File Offset: 0x00076BA1
		private void OnClickDecide()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_Signup(this.m_Owner.SignupUI.UserName, string.Empty, new Action<MeigewwwParam, Signup>(this.OnResponse_Signup));
		}

		// Token: 0x06001727 RID: 5927 RVA: 0x000787D4 File Offset: 0x00076BD4
		private void OnResponse_Signup(MeigewwwParam wwwParam, Signup param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					if (result != ResultCode.PLAYER_ALREADY_EXISTS)
					{
					}
					APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
					this.m_Step = SignupState_Main.eStep.Main;
				}
				else
				{
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.NgWordError), result.ToMessageString(), null);
					this.m_Owner.SignupUI.UserName = string.Empty;
					this.m_Step = SignupState_Main.eStep.Main;
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_Login(new Action<MeigewwwParam, Login>(this.OnResponse_Login), new Action<MeigewwwParam, Getall>(this.OnResponse_GetAll));
			}
		}

		// Token: 0x06001728 RID: 5928 RVA: 0x00078894 File Offset: 0x00076C94
		private void OnResponse_Login(MeigewwwParam wwwParam, Login param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.PLAYER_NOT_FOUND)
				{
					APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
					this.m_Step = SignupState_Main.eStep.Main;
				}
				else
				{
					APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
					this.m_Step = SignupState_Main.eStep.Main;
				}
			}
		}

		// Token: 0x06001729 RID: 5929 RVA: 0x000788FC File Offset: 0x00076CFC
		private void OnResponse_GetAll(MeigewwwParam wwwParam, Getall param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
				this.m_Step = SignupState_Main.eStep.Main;
			}
			else
			{
				this.m_Step = SignupState_Main.eStep.QuestGetAll;
			}
		}

		// Token: 0x0600172A RID: 5930 RVA: 0x00078946 File Offset: 0x00076D46
		private void OnResponse_QuestGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestChapterGetAll(new Action(this.OnResponse_QuestChapterGetAll), true);
		}

		// Token: 0x0600172B RID: 5931 RVA: 0x00078965 File Offset: 0x00076D65
		private void OnResponse_QuestChapterGetAll()
		{
			this.m_Step = SignupState_Main.eStep.Mission;
		}

		// Token: 0x04001DD4 RID: 7636
		private SignupState_Main.eStep m_Step = SignupState_Main.eStep.None;

		// Token: 0x0200049C RID: 1180
		private enum eStep
		{
			// Token: 0x04001DD6 RID: 7638
			None = -1,
			// Token: 0x04001DD7 RID: 7639
			First,
			// Token: 0x04001DD8 RID: 7640
			WaitLoadBG,
			// Token: 0x04001DD9 RID: 7641
			FadeIn_Wait,
			// Token: 0x04001DDA RID: 7642
			Main,
			// Token: 0x04001DDB RID: 7643
			QuestGetAll,
			// Token: 0x04001DDC RID: 7644
			QuestGetAll_Wait,
			// Token: 0x04001DDD RID: 7645
			Mission,
			// Token: 0x04001DDE RID: 7646
			Mission_Wait,
			// Token: 0x04001DDF RID: 7647
			TransitNextScene,
			// Token: 0x04001DE0 RID: 7648
			TransitNextScene_Wait
		}
	}
}
