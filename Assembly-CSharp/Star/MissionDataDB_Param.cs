﻿using System;

namespace Star
{
	// Token: 0x020004EB RID: 1259
	[Serializable]
	public struct MissionDataDB_Param
	{
		// Token: 0x04001F8A RID: 8074
		public int m_ID;

		// Token: 0x04001F8B RID: 8075
		public ushort m_Category;

		// Token: 0x04001F8C RID: 8076
		public ushort m_ActionNo;

		// Token: 0x04001F8D RID: 8077
		public int m_ExtentionScriptID;

		// Token: 0x04001F8E RID: 8078
		public int m_ExtentionFunKey;

		// Token: 0x04001F8F RID: 8079
		public string m_TargetMessage;

		// Token: 0x04001F90 RID: 8080
		public int m_RewardType;

		// Token: 0x04001F91 RID: 8081
		public int m_RewardID;

		// Token: 0x04001F92 RID: 8082
		public int m_RewardNum;

		// Token: 0x04001F93 RID: 8083
		public int m_LimitTime;

		// Token: 0x04001F94 RID: 8084
		public int m_StartYear;

		// Token: 0x04001F95 RID: 8085
		public short m_StartMonth;

		// Token: 0x04001F96 RID: 8086
		public short m_StartDay;

		// Token: 0x04001F97 RID: 8087
		public int m_EndDays;

		// Token: 0x04001F98 RID: 8088
		public int m_ListUpNo;

		// Token: 0x04001F99 RID: 8089
		public int m_MakeUpType;

		// Token: 0x04001F9A RID: 8090
		public int m_SubCode;

		// Token: 0x04001F9B RID: 8091
		public ushort m_PopupTiming;
	}
}
