﻿using System;

namespace Star
{
	// Token: 0x0200022F RID: 559
	public enum eScheduleConditionDayType
	{
		// Token: 0x04000F4F RID: 3919
		None = -1,
		// Token: 0x04000F50 RID: 3920
		Weekday,
		// Token: 0x04000F51 RID: 3921
		Holiday
	}
}
