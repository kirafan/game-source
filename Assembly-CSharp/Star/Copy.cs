﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Star
{
	// Token: 0x020004DF RID: 1247
	public static class Copy
	{
		// Token: 0x060018C8 RID: 6344 RVA: 0x00081154 File Offset: 0x0007F554
		public static T DeepCopy<T>(T target)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			T result;
			try
			{
				binaryFormatter.Serialize(memoryStream, target);
				memoryStream.Position = 0L;
				result = (T)((object)binaryFormatter.Deserialize(memoryStream));
			}
			finally
			{
				memoryStream.Close();
			}
			return result;
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x000811AC File Offset: 0x0007F5AC
		public static object DeepCopy(this object target)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			object result;
			try
			{
				binaryFormatter.Serialize(memoryStream, target);
				memoryStream.Position = 0L;
				result = binaryFormatter.Deserialize(memoryStream);
			}
			finally
			{
				memoryStream.Close();
			}
			return result;
		}
	}
}
