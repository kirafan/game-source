﻿using System;

namespace Star
{
	// Token: 0x020006AB RID: 1707
	public class TownHandleActionTouchSelect : ITownHandleAction
	{
		// Token: 0x0600221A RID: 8730 RVA: 0x000B5581 File Offset: 0x000B3981
		public TownHandleActionTouchSelect(bool fselect)
		{
			this.m_Type = eTownEventAct.SelectTouch;
			this.m_Select = fselect;
		}

		// Token: 0x040028A4 RID: 10404
		public bool m_Select;
	}
}
