﻿using System;

namespace Star
{
	// Token: 0x02000B4C RID: 2892
	public class UserMasterOrbData
	{
		// Token: 0x06003D37 RID: 15671 RVA: 0x001366E0 File Offset: 0x00134AE0
		public UserMasterOrbData()
		{
			this.MngID = -1L;
			this.OrbID = -1;
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06003D38 RID: 15672 RVA: 0x001366F7 File Offset: 0x00134AF7
		// (set) Token: 0x06003D39 RID: 15673 RVA: 0x001366FF File Offset: 0x00134AFF
		public long MngID { get; set; }

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06003D3A RID: 15674 RVA: 0x00136708 File Offset: 0x00134B08
		// (set) Token: 0x06003D3B RID: 15675 RVA: 0x00136710 File Offset: 0x00134B10
		public int OrbID { get; set; }

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06003D3C RID: 15676 RVA: 0x00136719 File Offset: 0x00134B19
		// (set) Token: 0x06003D3D RID: 15677 RVA: 0x00136721 File Offset: 0x00134B21
		public int Lv { get; set; }

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x06003D3E RID: 15678 RVA: 0x0013672A File Offset: 0x00134B2A
		// (set) Token: 0x06003D3F RID: 15679 RVA: 0x00136732 File Offset: 0x00134B32
		public long Exp { get; set; }

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x06003D40 RID: 15680 RVA: 0x0013673B File Offset: 0x00134B3B
		// (set) Token: 0x06003D41 RID: 15681 RVA: 0x00136743 File Offset: 0x00134B43
		public bool IsEquip { get; set; }
	}
}
