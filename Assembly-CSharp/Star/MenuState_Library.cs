﻿using System;
using Star.UI;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000435 RID: 1077
	public class MenuState_Library : MenuState
	{
		// Token: 0x060014AE RID: 5294 RVA: 0x0006CDB3 File Offset: 0x0006B1B3
		public MenuState_Library(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014AF RID: 5295 RVA: 0x0006CDCB File Offset: 0x0006B1CB
		public override int GetStateID()
		{
			return 4;
		}

		// Token: 0x060014B0 RID: 5296 RVA: 0x0006CDCE File Offset: 0x0006B1CE
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Library.eStep.First;
		}

		// Token: 0x060014B1 RID: 5297 RVA: 0x0006CDD7 File Offset: 0x0006B1D7
		public override void OnStateExit()
		{
		}

		// Token: 0x060014B2 RID: 5298 RVA: 0x0006CDD9 File Offset: 0x0006B1D9
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014B3 RID: 5299 RVA: 0x0006CDE4 File Offset: 0x0006B1E4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Library.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Library.eStep.LoadWait;
				break;
			case MenuState_Library.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Library.eStep.PlayIn;
				}
				break;
			case MenuState_Library.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Owner.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Training);
					this.m_Step = MenuState_Library.eStep.Main;
				}
				break;
			case MenuState_Library.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
						this.m_Owner.m_NpcUI.PlayOut();
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case MenuLibraryUI.eButton.TitleExp:
							this.m_Owner.m_NpcUI.PlayOut();
							this.m_Owner.LibraryStartMode = MenuLibraryWordUI.eMode.ContentTitle;
							this.m_NextState = 12;
							break;
						case MenuLibraryUI.eButton.Word:
							this.m_Owner.m_NpcUI.PlayOut();
							this.m_Owner.LibraryStartMode = MenuLibraryWordUI.eMode.Word;
							this.m_NextState = 12;
							break;
						case MenuLibraryUI.eButton.OriginalChara:
							this.m_Owner.m_NpcUI.PlayOut();
							this.m_NextState = 14;
							break;
						case MenuLibraryUI.eButton.ADV:
							this.m_NextState = 13;
							break;
						case MenuLibraryUI.eButton.OPMovie:
							this.m_Owner.m_NpcUI.PlayOut();
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart = true;
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetMoviePlayParam("mv_op", SceneDefine.eSceneID.Menu);
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.MoviePlay;
							this.m_NextState = 2147483646;
							break;
						default:
							this.m_Owner.m_NpcUI.PlayOut();
							if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome)
							{
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
								this.m_NextState = 2147483646;
							}
							else
							{
								this.m_NextState = 1;
							}
							break;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Library.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Library.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Library.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014B4 RID: 5300 RVA: 0x0006D064 File Offset: 0x0006B464
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuLibraryUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			if (!SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Library);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Library);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014B5 RID: 5301 RVA: 0x0006D140 File Offset: 0x0006B540
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Library)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				this.m_Owner.m_NpcUI.PlayOut();
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome)
				{
					this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
					this.m_NextState = 2147483646;
				}
				else
				{
					this.m_NextState = 1;
				}
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060014B6 RID: 5302 RVA: 0x0006D1D0 File Offset: 0x0006B5D0
		private void OnClickButton()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060014B7 RID: 5303 RVA: 0x0006D1D8 File Offset: 0x0006B5D8
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001B9A RID: 7066
		private MenuState_Library.eStep m_Step = MenuState_Library.eStep.None;

		// Token: 0x04001B9B RID: 7067
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuLibraryUI;

		// Token: 0x04001B9C RID: 7068
		private MenuLibraryUI m_UI;

		// Token: 0x02000436 RID: 1078
		private enum eStep
		{
			// Token: 0x04001B9E RID: 7070
			None = -1,
			// Token: 0x04001B9F RID: 7071
			First,
			// Token: 0x04001BA0 RID: 7072
			LoadWait,
			// Token: 0x04001BA1 RID: 7073
			PlayIn,
			// Token: 0x04001BA2 RID: 7074
			Main,
			// Token: 0x04001BA3 RID: 7075
			UnloadChildSceneWait
		}
	}
}
