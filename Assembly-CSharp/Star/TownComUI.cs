﻿using System;

namespace Star
{
	// Token: 0x020004B4 RID: 1204
	public class TownComUI : IMainComCommand
	{
		// Token: 0x06001777 RID: 6007 RVA: 0x000795B3 File Offset: 0x000779B3
		public TownComUI(bool fopen)
		{
			this.m_IsOpen = fopen;
		}

		// Token: 0x06001778 RID: 6008 RVA: 0x000795C2 File Offset: 0x000779C2
		public void GetBuildInfomation(out int pbuildpoint, out long pbuildmngid, out int pbuildobjid)
		{
			pbuildpoint = this.m_BuildPoint;
			pbuildmngid = this.m_SelectHandle.GetManageID();
			pbuildobjid = this.m_SelectHandle.GetObjID();
		}

		// Token: 0x06001779 RID: 6009 RVA: 0x000795E6 File Offset: 0x000779E6
		public override int GetHashCode()
		{
			return 2;
		}

		// Token: 0x04001E47 RID: 7751
		public bool m_IsOpen;

		// Token: 0x04001E48 RID: 7752
		public eTownUICategory m_OpenUI;

		// Token: 0x04001E49 RID: 7753
		public int m_BuildPoint;

		// Token: 0x04001E4A RID: 7754
		public ITownObjectHandler m_SelectHandle;
	}
}
