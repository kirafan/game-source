﻿using System;

namespace Star
{
	// Token: 0x02000378 RID: 888
	public struct ScheduleUIData
	{
		// Token: 0x060010EB RID: 4331 RVA: 0x000592CC File Offset: 0x000576CC
		public string GetSegName()
		{
			string empty = string.Empty;
			return ScheduleNameUtil.GetScheduleNameTag(this.m_NameTagID).m_TagName;
		}

		// Token: 0x060010EC RID: 4332 RVA: 0x000592F4 File Offset: 0x000576F4
		public int GetSegNameID()
		{
			return this.m_SegNameID;
		}

		// Token: 0x040017D7 RID: 6103
		public eScheduleTypeUIType m_Type;

		// Token: 0x040017D8 RID: 6104
		public int m_NameTagID;

		// Token: 0x040017D9 RID: 6105
		public int m_SegNameID;

		// Token: 0x040017DA RID: 6106
		public int m_WakeTime;

		// Token: 0x040017DB RID: 6107
		public int m_Time;
	}
}
