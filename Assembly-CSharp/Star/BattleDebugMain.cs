﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000271 RID: 625
	public class BattleDebugMain : GameStateMain
	{
		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000B97 RID: 2967 RVA: 0x00043C55 File Offset: 0x00042055
		public BattleSystem System
		{
			get
			{
				return this.m_System;
			}
		}

		// Token: 0x06000B98 RID: 2968 RVA: 0x00043C5D File Offset: 0x0004205D
		private void Start()
		{
			base.SetNextState(1);
		}

		// Token: 0x06000B99 RID: 2969 RVA: 0x00043C66 File Offset: 0x00042066
		protected override void Update()
		{
			base.Update();
			this.m_System.SystemUpdate();
		}

		// Token: 0x06000B9A RID: 2970 RVA: 0x00043C79 File Offset: 0x00042079
		private void LateUpdate()
		{
			this.m_System.SystemLateUpdate();
		}

		// Token: 0x06000B9B RID: 2971 RVA: 0x00043C88 File Offset: 0x00042088
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID != 1)
			{
				return null;
			}
			return new BattleDebugState_Main(this);
		}

		// Token: 0x0400140D RID: 5133
		public const int STATE_MAIN = 1;

		// Token: 0x0400140E RID: 5134
		[SerializeField]
		private BattleSystem m_System;
	}
}
