﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x0200068E RID: 1678
	public class TownComAPIObjLeveUp : INetComHandle
	{
		// Token: 0x060021BF RID: 8639 RVA: 0x000B3A92 File Offset: 0x000B1E92
		public TownComAPIObjLeveUp()
		{
			this.ApiName = "player/town_facility/level_up";
			this.Request = true;
			this.ResponseType = typeof(Levelup);
		}

		// Token: 0x0400282F RID: 10287
		public long managedTownFacilityId;

		// Token: 0x04002830 RID: 10288
		public int nextLevel;

		// Token: 0x04002831 RID: 10289
		public int openState;

		// Token: 0x04002832 RID: 10290
		public long actionTime;
	}
}
