﻿using System;

namespace Star
{
	// Token: 0x02000564 RID: 1380
	public class ActXlsKeyWait : ActXlsKeyBase
	{
		// Token: 0x06001B05 RID: 6917 RVA: 0x0008F541 File Offset: 0x0008D941
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_DummyNo);
			this.m_WaitType = (CActScriptKeyWait.eWait)pio.Enum(this.m_WaitType, typeof(CActScriptKeyWait.eWait));
		}

		// Token: 0x040021ED RID: 8685
		public int m_DummyNo;

		// Token: 0x040021EE RID: 8686
		public CActScriptKeyWait.eWait m_WaitType;
	}
}
