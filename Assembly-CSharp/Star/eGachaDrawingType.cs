﻿using System;

namespace Star
{
	// Token: 0x02000234 RID: 564
	public enum eGachaDrawingType
	{
		// Token: 0x04000F6F RID: 3951
		UGem1 = 1,
		// Token: 0x04000F70 RID: 3952
		Gem1,
		// Token: 0x04000F71 RID: 3953
		Gem10,
		// Token: 0x04000F72 RID: 3954
		Item
	}
}
