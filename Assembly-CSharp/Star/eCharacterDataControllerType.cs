﻿using System;

namespace Star
{
	// Token: 0x020002CB RID: 715
	public enum eCharacterDataControllerType
	{
		// Token: 0x04001587 RID: 5511
		Error,
		// Token: 0x04001588 RID: 5512
		ManagedBattleParty,
		// Token: 0x04001589 RID: 5513
		ManagedSupportParty,
		// Token: 0x0400158A RID: 5514
		UnmanagedSupportParty,
		// Token: 0x0400158B RID: 5515
		UnmanagedUserCharacterData,
		// Token: 0x0400158C RID: 5516
		EquipWeaponUnmanagedCharacterDataWrapper,
		// Token: 0x0400158D RID: 5517
		UnmanagedCharacterDataWrapper,
		// Token: 0x0400158E RID: 5518
		Empty,
		// Token: 0x0400158F RID: 5519
		AfterLimitBreakParam,
		// Token: 0x04001590 RID: 5520
		Locked
	}
}
