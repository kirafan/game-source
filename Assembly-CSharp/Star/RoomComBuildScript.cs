﻿using System;
using System.Collections.Generic;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x0200059A RID: 1434
	public class RoomComBuildScript : IFldNetComModule
	{
		// Token: 0x06001BC7 RID: 7111 RVA: 0x000930B0 File Offset: 0x000914B0
		public RoomComBuildScript(long froommngid) : base(IFldNetComManager.Instance)
		{
			this.m_Table = new List<RoomComBuildScript.StackScriptCommand>();
			this.m_ObjID = 0;
			this.m_ObjNum = 1;
			this.m_RoomMngID = froommngid;
			this.m_ManageID = -1L;
			this.m_Step = RoomComBuildScript.eStep.Check;
		}

		// Token: 0x06001BC8 RID: 7112 RVA: 0x000930EC File Offset: 0x000914EC
		public void SetManageID(long fmanageid)
		{
			this.m_ManageID = fmanageid;
		}

		// Token: 0x06001BC9 RID: 7113 RVA: 0x000930F8 File Offset: 0x000914F8
		public void StackSendCmd(RoomComBuildScript.eCmd fcalc, long fkeycode, int foption, IFldNetComModule.CallBack pcallback = null)
		{
			RoomComBuildScript.StackScriptCommand stackScriptCommand = new RoomComBuildScript.StackScriptCommand();
			stackScriptCommand.m_NextStep = fcalc;
			stackScriptCommand.m_KeyCode = fkeycode;
			stackScriptCommand.m_OptionCode = foption;
			stackScriptCommand.m_Callback = pcallback;
			this.m_Table.Add(stackScriptCommand);
		}

		// Token: 0x06001BCA RID: 7114 RVA: 0x00093134 File Offset: 0x00091534
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06001BCB RID: 7115 RVA: 0x00093144 File Offset: 0x00091544
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case RoomComBuildScript.eStep.Check:
				if (this.m_Table.Count != 0)
				{
					this.m_Step = RoomComBuildScript.eStep.ComCheck;
					switch (this.m_Table[0].m_NextStep)
					{
					case RoomComBuildScript.eCmd.BuyObj:
						this.m_CreateObj = true;
						this.m_ObjID = (int)this.m_Table[0].m_KeyCode;
						this.m_ObjNum = this.m_Table[0].m_OptionCode;
						this.SendAddObjUp();
						break;
					case RoomComBuildScript.eCmd.AddBuildIn:
						this.SendAddPlacementObject();
						break;
					case RoomComBuildScript.eCmd.ChangePlacement:
					case RoomComBuildScript.eCmd.DelPlacement:
						this.SendStateChange();
						break;
					case RoomComBuildScript.eCmd.SaleObject:
						this.SendSaleObject();
						break;
					case RoomComBuildScript.eCmd.CallEvt:
						this.m_Step = RoomComBuildScript.eStep.ComEnd;
						break;
					}
					base.EntryCallback(this.m_Table[0].m_Callback);
					this.m_Table.RemoveAt(0);
				}
				else
				{
					this.m_Step = RoomComBuildScript.eStep.End;
				}
				break;
			case RoomComBuildScript.eStep.ComEnd:
				if (this.m_Callback != null)
				{
					this.m_Callback(this);
				}
				this.m_Step = RoomComBuildScript.eStep.Check;
				break;
			case RoomComBuildScript.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x06001BCC RID: 7116 RVA: 0x00093294 File Offset: 0x00091694
		private void SendAddObjUp()
		{
			RoomComAPIObjBuy roomComAPIObjBuy = new RoomComAPIObjBuy();
			roomComAPIObjBuy.roomObjectId = this.m_ObjID.ToString();
			roomComAPIObjBuy.amount = this.m_ObjNum.ToString();
			this.m_NetMng.SendCom(roomComAPIObjBuy, new INetComHandle.ResponseCallbak(this.CallbackObjectAdd), false);
		}

		// Token: 0x06001BCD RID: 7117 RVA: 0x000932F0 File Offset: 0x000916F0
		private void CallbackObjectAdd(INetComHandle phandle)
		{
			Buy buy = phandle.GetResponse() as Buy;
			if (buy != null)
			{
				string[] array = buy.managedRoomObjectIds.Split(new char[]
				{
					','
				});
				for (int i = 0; i < array.Length; i++)
				{
					if (long.TryParse(array[i], out this.m_ManageID))
					{
						RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(this.m_ObjID);
						UserRoomObjectData userRoomObjectData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
						if (userRoomObjectData == null)
						{
							userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
							userRoomObjectData.SetServerData(this.m_ManageID, this.m_ObjID);
							SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomObjDatas.Add(userRoomObjectData);
						}
						else
						{
							userRoomObjectData.SetServerData(this.m_ManageID, this.m_ObjID);
						}
					}
				}
				APIUtility.wwwToUserData(buy.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
			}
			this.m_Step = RoomComBuildScript.eStep.ComEnd;
		}

		// Token: 0x06001BCE RID: 7118 RVA: 0x000933F8 File Offset: 0x000917F8
		private void SendAddPlacementObject()
		{
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_RoomMngID);
			RoomComAPISet roomComAPISet = new RoomComAPISet();
			roomComAPISet.managedRoomId = this.m_RoomMngID;
			roomComAPISet.floorId = manageIDToUserRoomData.FloorID;
			roomComAPISet.arrangeData = UserRoomUtil.CreateRoomDataString(manageIDToUserRoomData);
			this.m_NetMng.SendCom(roomComAPISet, new INetComHandle.ResponseCallbak(this.CallbackPlacementSet), false);
		}

		// Token: 0x06001BCF RID: 7119 RVA: 0x00093454 File Offset: 0x00091854
		private void CallbackPlacementSet(INetComHandle phandle)
		{
			this.m_Step = RoomComBuildScript.eStep.ComEnd;
		}

		// Token: 0x06001BD0 RID: 7120 RVA: 0x00093460 File Offset: 0x00091860
		private void SendStateChange()
		{
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_RoomMngID);
			RoomComAPISet roomComAPISet = new RoomComAPISet();
			roomComAPISet.managedRoomId = this.m_RoomMngID;
			roomComAPISet.floorId = manageIDToUserRoomData.FloorID;
			roomComAPISet.arrangeData = UserRoomUtil.CreateRoomDataString(manageIDToUserRoomData);
			this.m_NetMng.SendCom(roomComAPISet, new INetComHandle.ResponseCallbak(this.CallbackPlacementSet), false);
		}

		// Token: 0x06001BD1 RID: 7121 RVA: 0x000934BC File Offset: 0x000918BC
		private void SendSaleObject()
		{
			RoomComAPIObjSale roomComAPIObjSale = new RoomComAPIObjSale();
			roomComAPIObjSale.managedRoomObjectId = this.m_ManageID.ToString();
			this.m_NetMng.SendCom(roomComAPIObjSale, new INetComHandle.ResponseCallbak(this.CallbackObjectSale), false);
		}

		// Token: 0x06001BD2 RID: 7122 RVA: 0x00093500 File Offset: 0x00091900
		private void CallbackObjectSale(INetComHandle phandle)
		{
			UserRoomUtil.ReleaseUserRoomObjData(this.m_ManageID);
			Sale sale = phandle.GetResponse() as Sale;
			APIUtility.wwwToUserData(sale.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
			this.m_Step = RoomComBuildScript.eStep.ComEnd;
		}

		// Token: 0x040022B5 RID: 8885
		public int m_ObjID;

		// Token: 0x040022B6 RID: 8886
		public int m_ObjNum;

		// Token: 0x040022B7 RID: 8887
		public long m_ManageID;

		// Token: 0x040022B8 RID: 8888
		public long m_RoomMngID;

		// Token: 0x040022B9 RID: 8889
		public bool m_CreateObj;

		// Token: 0x040022BA RID: 8890
		private RoomComBuildScript.eStep m_Step;

		// Token: 0x040022BB RID: 8891
		public List<RoomComBuildScript.StackScriptCommand> m_Table;

		// Token: 0x0200059B RID: 1435
		public enum eCmd
		{
			// Token: 0x040022BD RID: 8893
			BuyObj,
			// Token: 0x040022BE RID: 8894
			AddBuildIn,
			// Token: 0x040022BF RID: 8895
			ChangePlacement,
			// Token: 0x040022C0 RID: 8896
			DelPlacement,
			// Token: 0x040022C1 RID: 8897
			SaleObject,
			// Token: 0x040022C2 RID: 8898
			CallEvt
		}

		// Token: 0x0200059C RID: 1436
		public enum eStep
		{
			// Token: 0x040022C4 RID: 8900
			Check,
			// Token: 0x040022C5 RID: 8901
			ComEnd,
			// Token: 0x040022C6 RID: 8902
			ComCheck,
			// Token: 0x040022C7 RID: 8903
			End
		}

		// Token: 0x0200059D RID: 1437
		public class StackScriptCommand
		{
			// Token: 0x040022C8 RID: 8904
			public RoomComBuildScript.eCmd m_NextStep;

			// Token: 0x040022C9 RID: 8905
			public long m_KeyCode;

			// Token: 0x040022CA RID: 8906
			public int m_OptionCode;

			// Token: 0x040022CB RID: 8907
			public IFldNetComModule.CallBack m_Callback;
		}
	}
}
