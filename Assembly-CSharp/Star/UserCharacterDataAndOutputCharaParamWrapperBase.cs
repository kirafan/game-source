﻿using System;

namespace Star
{
	// Token: 0x020002A4 RID: 676
	public class UserCharacterDataAndOutputCharaParamWrapperBase : UserCharacterDataWrapperBase
	{
		// Token: 0x06000CC6 RID: 3270 RVA: 0x00048DFF File Offset: 0x000471FF
		public UserCharacterDataAndOutputCharaParamWrapperBase(eCharacterDataType characterDataType, UserCharacterData in_data, EditUtility.OutputCharaParam param) : base(characterDataType, in_data)
		{
			this.m_Param = param;
		}

		// Token: 0x06000CC7 RID: 3271 RVA: 0x00048E10 File Offset: 0x00047210
		public override int GetLv()
		{
			return this.m_Param.Lv;
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x00048E1D File Offset: 0x0004721D
		public override long GetNowExp()
		{
			return this.m_Param.NowExp;
		}

		// Token: 0x06000CC9 RID: 3273 RVA: 0x00048E2A File Offset: 0x0004722A
		public override long GetNowMaxExp()
		{
			return this.m_Param.NextMaxExp;
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x00048E37 File Offset: 0x00047237
		public override EditUtility.OutputCharaParam GetCharaParamLvOnly()
		{
			return this.m_Param;
		}

		// Token: 0x04001547 RID: 5447
		protected EditUtility.OutputCharaParam m_Param;
	}
}
