﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200001D RID: 29
	public class ADVMotionListDB : ScriptableObject
	{
		// Token: 0x040000B7 RID: 183
		public ADVMotionListDB_Param[] m_Params;
	}
}
