﻿using System;

namespace Star
{
	// Token: 0x02000181 RID: 385
	[Serializable]
	public struct EffectListDB_Param
	{
		// Token: 0x04000ABC RID: 2748
		public string m_PackName;

		// Token: 0x04000ABD RID: 2749
		public string m_EffectID;

		// Token: 0x04000ABE RID: 2750
		public int m_InstanceMax;

		// Token: 0x04000ABF RID: 2751
		public int[] m_PlayFrames;

		// Token: 0x04000AC0 RID: 2752
		public int[] m_CueIDs;
	}
}
