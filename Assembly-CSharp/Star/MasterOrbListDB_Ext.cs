﻿using System;

namespace Star
{
	// Token: 0x02000198 RID: 408
	public static class MasterOrbListDB_Ext
	{
		// Token: 0x06000AF6 RID: 2806 RVA: 0x000415E4 File Offset: 0x0003F9E4
		public static MasterOrbListDB_Param GetParam(this MasterOrbListDB self, int orbID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == orbID)
				{
					return self.m_Params[i];
				}
			}
			return default(MasterOrbListDB_Param);
		}
	}
}
