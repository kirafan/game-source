﻿using System;
using System.Collections.Generic;
using Star.UI;
using Star.UI.Global;
using Star.UI.LoginBonus;
using UnityEngine;

namespace Star
{
	// Token: 0x020004BD RID: 1213
	public class TownState_Home : TownState
	{
		// Token: 0x060017CC RID: 6092 RVA: 0x0007BCE5 File Offset: 0x0007A0E5
		public TownState_Home(TownMain owner) : base(owner)
		{
		}

		// Token: 0x060017CD RID: 6093 RVA: 0x0007BCFD File Offset: 0x0007A0FD
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x060017CE RID: 6094 RVA: 0x0007BD00 File Offset: 0x0007A100
		public override void OnStateEnter()
		{
			this.m_Step = TownState_Home.eStep.First;
		}

		// Token: 0x060017CF RID: 6095 RVA: 0x0007BD09 File Offset: 0x0007A109
		public override void OnStateExit()
		{
			this.m_Owner.DestroyReq();
		}

		// Token: 0x060017D0 RID: 6096 RVA: 0x0007BD16 File Offset: 0x0007A116
		public override void OnDispose()
		{
			this.m_LoginBonusUI = null;
		}

		// Token: 0x060017D1 RID: 6097 RVA: 0x0007BD20 File Offset: 0x0007A120
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case TownState_Home.eStep.First:
				this.m_IsLoginBonusCall = SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.IsLoginBonusCall();
				if (this.m_IsLoginBonusCall)
				{
					this.m_Step = TownState_Home.eStep.LoginBonusCheck;
				}
				else
				{
					this.m_Step = TownState_Home.eStep.InfoBannerRequest;
				}
				break;
			case TownState_Home.eStep.LoginBonusCheck:
				SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_LoginBonus(new Action(this.OnResponseLoginBonus), false);
				this.m_Step = TownState_Home.eStep.LoginBonusCheckWait;
				break;
			case TownState_Home.eStep.LoginBonusLoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = TownState_Home.eStep.LoginBonusPlayIn;
				}
				break;
			case TownState_Home.eStep.LoginBonusPlayIn:
				if (this.SetupAndPlayLoginBonus())
				{
					this.m_Step = TownState_Home.eStep.LoginBonusWait;
				}
				break;
			case TownState_Home.eStep.LoginBonusWait:
				if (this.m_LoginBonusUI.IsMenuEnd())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					this.m_Step = TownState_Home.eStep.UnloadLoginBonusSceneWait_0;
				}
				break;
			case TownState_Home.eStep.UnloadLoginBonusSceneWait_0:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = TownState_Home.eStep.UnloadLoginBonusSceneWait_1;
				}
				break;
			case TownState_Home.eStep.UnloadLoginBonusSceneWait_1:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = TownState_Home.eStep.InfoBannerRequest;
				}
				break;
			case TownState_Home.eStep.InfoBannerRequest:
				if (!this.m_Owner.IsDoneInfoAPI)
				{
					this.m_Owner.HomeUI.GetInfoBanner().RequestGetAndPrepare(new Action(this.OnResponseInfo));
					this.m_Step = TownState_Home.eStep.InfoBannerRequestWait;
				}
				else
				{
					this.m_Step = TownState_Home.eStep.HomePlayIn;
				}
				break;
			case TownState_Home.eStep.HomePlayIn:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsOverlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
				}
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopSystem, true);
				this.m_Owner.StartMode(TownMain.eMode.Home);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
				this.m_Owner.SetTransitCallBack(new Action(this.OnTransitCallBack));
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.m_Owner.gameObject, true);
				this.m_Step = TownState_Home.eStep.WaitLoadOpen;
				break;
			case TownState_Home.eStep.WaitLoadOpen:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = TownState_Home.eStep.HomeStart;
				}
				break;
			case TownState_Home.eStep.HomeStart:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.m_Owner.gameObject, false);
				this.m_Step = TownState_Home.eStep.HomeStartWait;
				eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
				if (tutoarialSeq == eTutorialSeq.ADVPrologueEndPlayed)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.TUTORIAL_FINISH, new Action(this.OnCompleteTutorialTipsWindow), null);
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RegisterPushNotificationService();
					bool flag = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.IsExistSaveResponse();
					if (!flag)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.ResetOnQuestLogSet();
					}
					if (this.m_IsLoginBonusCall && SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.GetLoginBonusTotal() != null)
					{
						LoginBonusTotal loginBonusTotal = SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.GetLoginBonusTotal();
						ePresentType[] array = new ePresentType[loginBonusTotal.m_Presents.Length];
						int[] array2 = new int[loginBonusTotal.m_Presents.Length];
						int[] array3 = new int[loginBonusTotal.m_Presents.Length];
						for (int i = 0; i < loginBonusTotal.m_Presents.Length; i++)
						{
							array[i] = loginBonusTotal.m_Presents[i].m_PresentType;
							array2[i] = loginBonusTotal.m_Presents[i].m_PresentID;
							array3[i] = loginBonusTotal.m_Presents[i].m_Amount;
						}
						this.m_Owner.HomeUI.OpenTotalLogin(new TotalLoginUIData(loginBonusTotal.m_Day, array, array2, array3), new Action(this.OnEndTotalLoginBonusData));
					}
					else if (flag)
					{
						this.RestartQuestFlow();
					}
					else
					{
						this.OpenInfo();
					}
				}
				break;
			}
			case TownState_Home.eStep.InfoWait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.IsOpen(OverlayUIPlayerManager.eOverlaySceneID.Info))
				{
					this.HomeStartCommon();
					this.m_Step = TownState_Home.eStep.HomeMain;
				}
				break;
			case TownState_Home.eStep.HomeMain:
				if (this.m_Owner.HomeUI.IsMenuEnd())
				{
					if (this.m_IsTutorialFinish)
					{
						this.m_NextState = 2147483646;
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
					}
					else if (this.m_IsQuestRestart)
					{
						bool flag2 = false;
						int num = -1;
						BattleSystemData battleSystemData = BattleUtility.SaveResponseToBSD(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.SaveResponse);
						if (battleSystemData != null && battleSystemData.Phase == -1)
						{
							QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(battleSystemData.QuestID);
							if (questData != null)
							{
								num = questData.m_Param.m_AdvID_Prev;
								if (num != -1)
								{
									flag2 = true;
									if (LocalSaveData.Inst.ADVSkipOnBattle && questData.IsCleared() && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(num))
									{
										flag2 = false;
									}
								}
							}
						}
						if (flag2)
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(num, SceneDefine.eSceneID.Battle, false);
						}
						else
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Battle;
						}
						this.m_NextState = 2147483646;
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton != GlobalUI.eShortCutButton.Home)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_Owner.Transit)
						{
						case TownMain.eTransit.Town:
							this.m_NextState = 1;
							break;
						case TownMain.eTransit.Quest:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Quest;
							break;
						case TownMain.eTransit.Edit:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Edit;
							break;
						case TownMain.eTransit.Room:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
							break;
						case TownMain.eTransit.Gacha:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Gacha;
							break;
						case TownMain.eTransit.Shop:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Shop;
							break;
						case TownMain.eTransit.Training:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Training;
							break;
						case TownMain.eTransit.ADV:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
							break;
						case TownMain.eTransit.Present:
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Present;
							break;
						}
					}
					return this.m_NextState;
				}
				this.m_Owner.UpdateEditing();
				break;
			}
			return -1;
		}

		// Token: 0x060017D2 RID: 6098 RVA: 0x0007C448 File Offset: 0x0007A848
		private void OnEndTotalLoginBonusData()
		{
			bool flag = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.IsExistSaveResponse();
			if (flag)
			{
				this.RestartQuestFlow();
			}
			else
			{
				this.OpenInfo();
			}
		}

		// Token: 0x060017D3 RID: 6099 RVA: 0x0007C47C File Offset: 0x0007A87C
		private void RestartQuestFlow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.QuestRestartConfirmTitle, eText_MessageDB.QuestRestartConfirmMessage, new Action<int>(this.OnConfirmQuestRestart));
		}

		// Token: 0x060017D4 RID: 6100 RVA: 0x0007C4A4 File Offset: 0x0007A8A4
		private void OnConfirmQuestRestart(int answer)
		{
			if (answer == 0)
			{
				int questIDFromSaveResponse = BattleUtility.GetQuestIDFromSaveResponse(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.SaveResponse);
				QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(questIDFromSaveResponse);
				if (questData != null)
				{
					this.m_IsQuestRestart = true;
					this.m_Step = TownState_Home.eStep.HomeMain;
					this.m_Owner.HomeUI.PlayOut();
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.QuestRestartFailedTitle, eText_MessageDB.QuestRestartFailedMessage, null);
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.QuestRestartDeleteTitle, eText_MessageDB.QuestRestartDeleteMessage, new Action<int>(this.OnConfirmQuestDelete));
			}
		}

		// Token: 0x060017D5 RID: 6101 RVA: 0x0007C54D File Offset: 0x0007A94D
		private void OnConfirmQuestDelete(int answer)
		{
			if (answer == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_DeleteBSD(new Action(this.OnResponseDeleteBSD));
			}
			else
			{
				this.RestartQuestFlow();
			}
		}

		// Token: 0x060017D6 RID: 6102 RVA: 0x0007C57C File Offset: 0x0007A97C
		private void OnResponseDeleteBSD()
		{
			this.OpenInfo();
		}

		// Token: 0x060017D7 RID: 6103 RVA: 0x0007C584 File Offset: 0x0007A984
		private void OpenInfo()
		{
			if (!SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsOccuredOpenInfo)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsOccuredOpenInfo = true;
				SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Info, new InfoUIArg(null), null, null);
				this.m_Step = TownState_Home.eStep.InfoWait;
			}
			else
			{
				this.HomeStartCommon();
				this.m_Step = TownState_Home.eStep.HomeMain;
			}
		}

		// Token: 0x060017D8 RID: 6104 RVA: 0x0007C5E8 File Offset: 0x0007A9E8
		private void HomeStartCommon()
		{
			this.m_Owner.HomeUI.SetEnablePlayVoice(true);
		}

		// Token: 0x060017D9 RID: 6105 RVA: 0x0007C5FB File Offset: 0x0007A9FB
		private void OnCompleteTutorialTipsWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.Request_TutorialFinish(new Action(this.OnResponseTutorialFinish));
		}

		// Token: 0x060017DA RID: 6106 RVA: 0x0007C618 File Offset: 0x0007AA18
		private void OnResponseTutorialFinish()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.RegisterPushNotificationService();
			this.OpenInfo();
		}

		// Token: 0x060017DB RID: 6107 RVA: 0x0007C62C File Offset: 0x0007AA2C
		private void OnResponseLoginBonus()
		{
			bool flag = SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.IsAvailableLoginBonus();
			if (flag && (SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.GetLoginBonusNormal() != null || SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.GetLoginBonusEvents() != null))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RefreshBadgeTimeSec = 0f;
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = TownState_Home.eStep.LoginBonusLoadWait;
			}
			else
			{
				this.m_Step = TownState_Home.eStep.InfoBannerRequest;
			}
		}

		// Token: 0x060017DC RID: 6108 RVA: 0x0007C6C0 File Offset: 0x0007AAC0
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Home)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.AppQuitConfirmTitle, eText_MessageDB.AppQuitConfirmMessage, new Action<int>(this.OnConfirmAppQuit));
				}
				else
				{
					if (!isCallFromShortCut)
					{
						this.m_Owner.Transit = TownMain.eTransit.Town;
					}
					this.GoToMenuEnd();
				}
			}
		}

		// Token: 0x060017DD RID: 6109 RVA: 0x0007C75E File Offset: 0x0007AB5E
		public void OnTransitCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060017DE RID: 6110 RVA: 0x0007C766 File Offset: 0x0007AB66
		private void GoToMenuEnd()
		{
			this.m_Owner.EndMode(this.m_Owner.Transit == TownMain.eTransit.Town);
		}

		// Token: 0x060017DF RID: 6111 RVA: 0x0007C784 File Offset: 0x0007AB84
		private bool SetupAndPlayLoginBonus()
		{
			this.m_LoginBonusUI = UIUtility.GetMenuComponent<LoginBonusUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_LoginBonusUI == null)
			{
				return false;
			}
			List<PageUIData> list = new List<PageUIData>();
			LoginManager loginManager = SingletonMonoBehaviour<GameSystem>.Inst.LoginManager;
			LoginBonus loginBonusNormal = loginManager.GetLoginBonusNormal();
			if (loginBonusNormal != null)
			{
				list.Add(this.CreateLoginBonusUIData(loginBonusNormal, false));
			}
			if (loginManager.GetLoginBonusEvents() != null)
			{
				for (int i = 0; i < loginManager.GetLoginBonusEvents().Length; i++)
				{
					list.Add(this.CreateLoginBonusUIData(loginManager.GetLoginBonusEvents()[i], true));
				}
			}
			this.m_LoginBonusUI.Setup(list);
			this.m_LoginBonusUI.PlayIn();
			this.m_Owner.ReleaseInputBlock();
			return true;
		}

		// Token: 0x060017E0 RID: 6112 RVA: 0x0007C854 File Offset: 0x0007AC54
		private PageUIData CreateLoginBonusUIData(LoginBonus data, bool isEvent)
		{
			List<RewardUIData> list = new List<RewardUIData>();
			for (int i = 0; i < data.m_Days.Length; i++)
			{
				RewardUIData item = new RewardUIData(isEvent, data.m_Days[i].m_IconID, i);
				list.Add(item);
			}
			PageUIData.eType type = PageUIData.eType.Normal;
			if (isEvent)
			{
				type = PageUIData.eType.Event;
			}
			return new PageUIData(type, data.m_ID, list, data.m_DayIndex);
		}

		// Token: 0x060017E1 RID: 6113 RVA: 0x0007C8B9 File Offset: 0x0007ACB9
		public void OnResponseInfo()
		{
			this.m_Owner.IsDoneInfoAPI = true;
			this.m_Step = TownState_Home.eStep.HomePlayIn;
		}

		// Token: 0x060017E2 RID: 6114 RVA: 0x0007C8CF File Offset: 0x0007ACCF
		private void OnConfirmAppQuit(int answer)
		{
			if (answer == 0)
			{
				Application.Quit();
			}
		}

		// Token: 0x04001E97 RID: 7831
		private TownState_Home.eStep m_Step = TownState_Home.eStep.None;

		// Token: 0x04001E98 RID: 7832
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.LoginBonusUI;

		// Token: 0x04001E99 RID: 7833
		private LoginBonusUI m_LoginBonusUI;

		// Token: 0x04001E9A RID: 7834
		private bool m_IsTutorialFinish;

		// Token: 0x04001E9B RID: 7835
		private bool m_IsQuestRestart;

		// Token: 0x04001E9C RID: 7836
		private bool m_IsLoginBonusCall;

		// Token: 0x020004BE RID: 1214
		private enum eStep
		{
			// Token: 0x04001E9E RID: 7838
			None = -1,
			// Token: 0x04001E9F RID: 7839
			First,
			// Token: 0x04001EA0 RID: 7840
			LoginBonusCheck,
			// Token: 0x04001EA1 RID: 7841
			LoginBonusCheckWait,
			// Token: 0x04001EA2 RID: 7842
			LoginBonusLoadWait,
			// Token: 0x04001EA3 RID: 7843
			LoginBonusPlayIn,
			// Token: 0x04001EA4 RID: 7844
			LoginBonusWait,
			// Token: 0x04001EA5 RID: 7845
			UnloadLoginBonusSceneWait_0,
			// Token: 0x04001EA6 RID: 7846
			UnloadLoginBonusSceneWait_1,
			// Token: 0x04001EA7 RID: 7847
			InfoBannerRequest,
			// Token: 0x04001EA8 RID: 7848
			InfoBannerRequestWait,
			// Token: 0x04001EA9 RID: 7849
			HomePlayIn,
			// Token: 0x04001EAA RID: 7850
			WaitLoadOpen,
			// Token: 0x04001EAB RID: 7851
			HomeStart,
			// Token: 0x04001EAC RID: 7852
			HomeStartWait,
			// Token: 0x04001EAD RID: 7853
			InfoWait,
			// Token: 0x04001EAE RID: 7854
			HomeMain
		}
	}
}
