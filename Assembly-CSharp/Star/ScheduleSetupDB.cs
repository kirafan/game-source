﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200030F RID: 783
	public class ScheduleSetupDB : ScriptableObject
	{
		// Token: 0x04001640 RID: 5696
		public ScheduleSetupDB_Param[] m_Params;
	}
}
