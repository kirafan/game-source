﻿using System;

namespace Star
{
	// Token: 0x02000236 RID: 566
	public enum eTutorialSeq
	{
		// Token: 0x04000F79 RID: 3961
		Finish = -1,
		// Token: 0x04000F7A RID: 3962
		Unplayed,
		// Token: 0x04000F7B RID: 3963
		SignupComplete_NextIsPresentGet,
		// Token: 0x04000F7C RID: 3964
		PresentGot_NextIsADVGachaPrevPlay,
		// Token: 0x04000F7D RID: 3965
		ADVGachaPrevPlayed_NextIsGachaPlay,
		// Token: 0x04000F7E RID: 3966
		GachaPlayed_NextIsGachaDecide,
		// Token: 0x04000F7F RID: 3967
		GachaDecided_NextIsADVGachaAfterPlay,
		// Token: 0x04000F80 RID: 3968
		ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play,
		// Token: 0x04000F81 RID: 3969
		ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play,
		// Token: 0x04000F82 RID: 3970
		ADVPrologueIntro2Played_NextIsQuestLogSet,
		// Token: 0x04000F83 RID: 3971
		QuestLogSeted_NextIsADVPrologueEnd,
		// Token: 0x04000F84 RID: 3972
		ADVPrologueEndPlayed
	}
}
