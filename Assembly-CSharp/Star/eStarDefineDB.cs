﻿using System;

namespace Star
{
	// Token: 0x02000221 RID: 545
	public enum eStarDefineDB
	{
		// Token: 0x04000E5B RID: 3675
		UpgradeSameElementBonus,
		// Token: 0x04000E5C RID: 3676
		UpgradeBonus_0,
		// Token: 0x04000E5D RID: 3677
		UpgradeBonus_1,
		// Token: 0x04000E5E RID: 3678
		UpgradeBonus_2,
		// Token: 0x04000E5F RID: 3679
		UpgradeProbability_0,
		// Token: 0x04000E60 RID: 3680
		UpgradeProbability_1,
		// Token: 0x04000E61 RID: 3681
		UpgradeProbability_2,
		// Token: 0x04000E62 RID: 3682
		WeaponUpgradeBonus_0,
		// Token: 0x04000E63 RID: 3683
		WeaponUpgradeBonus_1,
		// Token: 0x04000E64 RID: 3684
		WeaponUpgradeBonus_2,
		// Token: 0x04000E65 RID: 3685
		WeaponUpgradeProbability_0,
		// Token: 0x04000E66 RID: 3686
		WeaponUpgradeProbability_1,
		// Token: 0x04000E67 RID: 3687
		WeaponUpgradeProbability_2,
		// Token: 0x04000E68 RID: 3688
		WeaponLimitExtendNum,
		// Token: 0x04000E69 RID: 3689
		WeaponLimitExtendAmount,
		// Token: 0x04000E6A RID: 3690
		WeaponLimitExtendMax,
		// Token: 0x04000E6B RID: 3691
		RoomObjectLimitExtendNum,
		// Token: 0x04000E6C RID: 3692
		RoomObjectLimitExtendAmount,
		// Token: 0x04000E6D RID: 3693
		RoomObjectLimitExtendMax,
		// Token: 0x04000E6E RID: 3694
		BattleContinueAmount,
		// Token: 0x04000E6F RID: 3695
		ReduceBuildTimeTownFacilityAmount,
		// Token: 0x04000E70 RID: 3696
		ReduceBuildTimeTownFacilityPeriod,
		// Token: 0x04000E71 RID: 3697
		StaminaAdditiveAmount,
		// Token: 0x04000E72 RID: 3698
		MonthlyPurchaseAmount_Under15,
		// Token: 0x04000E73 RID: 3699
		MonthlyPurchaseAmount_From16To19,
		// Token: 0x04000E74 RID: 3700
		TownFacilityLimitExtendNum,
		// Token: 0x04000E75 RID: 3701
		TownFacilityLimitExtendAmount,
		// Token: 0x04000E76 RID: 3702
		TownFacilityLimitExtendMax,
		// Token: 0x04000E77 RID: 3703
		UpgradeWeaponMaterialBonus,
		// Token: 0x04000E78 RID: 3704
		Max
	}
}
