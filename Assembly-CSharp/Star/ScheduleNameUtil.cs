﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000396 RID: 918
	public class ScheduleNameUtil
	{
		// Token: 0x0600114A RID: 4426 RVA: 0x0005A92D File Offset: 0x00058D2D
		public static void DatabaseSetUp()
		{
			if (ScheduleNameUtil.ms_ScheduleNameDB == null)
			{
				ScheduleNameUtil.ms_ScheduleNameDB = new ScheduleNameUtil.ScheduleNameData();
				ScheduleNameUtil.ms_ScheduleNameDB.LoadResInManager("Prefab/FldDB/ScheduleName", ".asset");
			}
		}

		// Token: 0x0600114B RID: 4427 RVA: 0x0005A957 File Offset: 0x00058D57
		public static bool IsSetUp()
		{
			return ScheduleNameUtil.ms_ScheduleNameDB.m_Active;
		}

		// Token: 0x0600114C RID: 4428 RVA: 0x0005A964 File Offset: 0x00058D64
		public static ScheduleNameDB_Param GetScheduleNameTag(int fkeyid)
		{
			ScheduleNameDB_Param result = ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[0];
			for (int i = 0; i < ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params.Length; i++)
			{
				if (ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[i].m_ID == fkeyid)
				{
					result = ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x0600114D RID: 4429 RVA: 0x0005A9EF File Offset: 0x00058DEF
		public static ScheduleNameDB GetScheduleDataBase()
		{
			return ScheduleNameUtil.ms_ScheduleNameDB.m_DB;
		}

		// Token: 0x0600114E RID: 4430 RVA: 0x0005A9FB File Offset: 0x00058DFB
		public static void DatabaseRelease()
		{
			if (ScheduleNameUtil.ms_ScheduleNameDB != null)
			{
				ScheduleNameUtil.ms_ScheduleNameDB.Release();
				ScheduleNameUtil.ms_ScheduleNameDB.m_DB = null;
			}
			ScheduleNameUtil.ms_ScheduleNameDB = null;
		}

		// Token: 0x0600114F RID: 4431 RVA: 0x0005AA24 File Offset: 0x00058E24
		public static void CalcScheduleTagToDropItemKey(UserFieldCharaData.RoomInCharaData pchara, int fscheduletagid, int fscheduletagtime, int fcharaid, eTownMoveState fupmove)
		{
			float num = -1f;
			if (fupmove == eTownMoveState.TargetMove)
			{
				num = 1f;
			}
			else if (fupmove == eTownMoveState.NearmissMove)
			{
				num = 0.5f;
			}
			if (num > 0f)
			{
				int num2 = UnityEngine.Random.Range(0, 100);
				int num3 = -1;
				int num4 = ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params.Length;
				for (int i = 0; i < num4; i++)
				{
					if (ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[i].m_ID == fscheduletagid)
					{
						num2 -= (int)((float)ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[i].m_DropMakeWakeUp * num);
						if (num2 <= 0)
						{
							num3 = i;
						}
						break;
					}
				}
				if (num3 >= 0)
				{
					num2 = UnityEngine.Random.Range(0, 100);
					num4 = ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropResultNo.Length;
					if (num4 < ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropWakeUp.Length)
					{
						num4 = ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropWakeUp.Length;
					}
					for (int j = 0; j < num4; j++)
					{
						num2 -= (int)ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropWakeUp[j];
						if (num2 <= 0)
						{
							eXlsDropCalcType flags = (eXlsDropCalcType)ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_Flags;
							if (flags != eXlsDropCalcType.normal)
							{
								if (flags != eXlsDropCalcType.chara)
								{
									if (flags == eXlsDropCalcType.build)
									{
										FieldObjDropItem.DropItemInfo dropItemInfo = default(FieldObjDropItem.DropItemInfo);
										if (UserTownUtil.CalcTownBuildToDropItemState(ref dropItemInfo, pchara.TownBuildManageID))
										{
											dropItemInfo.CalcTimeToUp((long)fscheduletagtime);
											pchara.SetScheduleDropKey((int)(ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropResultNo[j] + (uint)dropItemInfo.m_TableID), 1f);
										}
									}
								}
								else
								{
									int num5 = (int)ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropResultNo[j];
									num5 += UserCharaUtil.GetCharaIDToNameParam(fcharaid).m_DropItemKey;
									UserScheduleData.DropState fieldDropItems = UserTownUtil.GetFieldDropItems(num5);
									if (fieldDropItems != null)
									{
										if (fieldDropItems.m_Category == UserScheduleData.eDropCategory.Money)
										{
											FieldObjDropItem.DropItemInfo dropItemInfo2 = default(FieldObjDropItem.DropItemInfo);
											if (UserTownUtil.FindTownBuildToDropItemState(ref dropItemInfo2, eTownObjectCategory.Money))
											{
												dropItemInfo2.CalcTimeToUp((long)fscheduletagtime);
												pchara.SetScheduleDropKey(num5 + dropItemInfo2.m_TableID, 1f);
											}
											else
											{
												pchara.SetScheduleDropKey(num5, 1f);
											}
										}
										else
										{
											pchara.SetScheduleDropKey(num5, 1f);
										}
									}
								}
							}
							else
							{
								pchara.SetScheduleDropKey((int)ScheduleNameUtil.ms_ScheduleNameDB.m_DB.m_Params[num3].m_DropResultNo[j], 1f);
							}
							break;
						}
					}
				}
			}
		}

		// Token: 0x0400181C RID: 6172
		public static ScheduleNameUtil.ScheduleNameData ms_ScheduleNameDB;

		// Token: 0x02000397 RID: 919
		public class ScheduleNameData : IDataBaseResource
		{
			// Token: 0x06001151 RID: 4433 RVA: 0x0005AD02 File Offset: 0x00059102
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
				this.m_DB = (pdataobj as ScheduleNameDB);
			}

			// Token: 0x0400181D RID: 6173
			public ScheduleNameDB m_DB;
		}
	}
}
