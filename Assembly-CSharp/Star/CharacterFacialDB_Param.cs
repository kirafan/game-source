﻿using System;

namespace Star
{
	// Token: 0x02000170 RID: 368
	[Serializable]
	public struct CharacterFacialDB_Param
	{
		// Token: 0x040009C9 RID: 2505
		public int m_ID;

		// Token: 0x040009CA RID: 2506
		public CharacterFacialDB_Data[] m_Datas;
	}
}
