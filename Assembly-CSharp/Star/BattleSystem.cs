﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Meige;
using PlayerResponseTypes;
using Star.Town;
using Star.UI;
using Star.UI.Battle;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020000E1 RID: 225
	public class BattleSystem : MonoBehaviour
	{
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600060F RID: 1551 RVA: 0x0001ED48 File Offset: 0x0001D148
		public BattleSystemData BSD
		{
			get
			{
				return this.m_BSD;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000610 RID: 1552 RVA: 0x0001ED50 File Offset: 0x0001D150
		public BattleBG BattleBG
		{
			get
			{
				return this.m_BattleBG;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000611 RID: 1553 RVA: 0x0001ED58 File Offset: 0x0001D158
		public Camera MainCamera
		{
			get
			{
				return this.m_MainCamera;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000612 RID: 1554 RVA: 0x0001ED60 File Offset: 0x0001D160
		public Camera ResultCamera
		{
			get
			{
				return this.m_ResultCamera;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x0001ED68 File Offset: 0x0001D168
		public BattleCamera BattleCamera
		{
			get
			{
				return this.m_BattleCamera;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000614 RID: 1556 RVA: 0x0001ED70 File Offset: 0x0001D170
		public Transform MainCameraTransform
		{
			get
			{
				return this.m_MainCameraTransform;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000615 RID: 1557 RVA: 0x0001ED78 File Offset: 0x0001D178
		public SkillActionPlayer SkillActionPlayer
		{
			get
			{
				return this.m_SkillActionPlayer;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000616 RID: 1558 RVA: 0x0001ED80 File Offset: 0x0001D180
		public Transform CenterLocator_Player
		{
			get
			{
				return this.m_CenterLocator_Player;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000617 RID: 1559 RVA: 0x0001ED88 File Offset: 0x0001D188
		public Transform CenterLocator_Enemy
		{
			get
			{
				return this.m_CenterLocator_Enemy;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000618 RID: 1560 RVA: 0x0001ED90 File Offset: 0x0001D190
		public BattleUniqueSkillCutIn UniqueSkillCutIn
		{
			get
			{
				return this.m_UniqueSkillCutIn;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000619 RID: 1561 RVA: 0x0001ED98 File Offset: 0x0001D198
		public BattlePartyData[] Parties
		{
			get
			{
				return this.m_Parties;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600061A RID: 1562 RVA: 0x0001EDA0 File Offset: 0x0001D1A0
		public BattlePartyData PlayerParty
		{
			get
			{
				return this.m_Parties[0];
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600061B RID: 1563 RVA: 0x0001EDAA File Offset: 0x0001D1AA
		public BattlePartyData EnemyParty
		{
			get
			{
				return this.m_Parties[1];
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600061C RID: 1564 RVA: 0x0001EDB4 File Offset: 0x0001D1B4
		public BattleOrder Order
		{
			get
			{
				return this.m_Order;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600061D RID: 1565 RVA: 0x0001EDBC File Offset: 0x0001D1BC
		public QuestListDB_Param QuestParam
		{
			get
			{
				return this.m_QuestParam;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x0600061E RID: 1566 RVA: 0x0001EDC4 File Offset: 0x0001D1C4
		public BattleTutorial Tutorial
		{
			get
			{
				return this.m_Tutorial;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600061F RID: 1567 RVA: 0x0001EDCC File Offset: 0x0001D1CC
		public BattleOption Option
		{
			get
			{
				return this.m_Option;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000620 RID: 1568 RVA: 0x0001EDD4 File Offset: 0x0001D1D4
		public BattleTimeScaler TimeScaler
		{
			get
			{
				return this.m_Scaler;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000621 RID: 1569 RVA: 0x0001EDDC File Offset: 0x0001D1DC
		public BattleResult QuestResult
		{
			get
			{
				return this.m_Result;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000622 RID: 1570 RVA: 0x0001EDE4 File Offset: 0x0001D1E4
		public BattleUniqueSkillScene UniqueSkillScene
		{
			get
			{
				return this.m_UniqueSkillScene;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000623 RID: 1571 RVA: 0x0001EDEC File Offset: 0x0001D1EC
		public bool IsInUniqueSkillScene
		{
			get
			{
				return this.m_UniqueSkillScene != null;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x0001EDFC File Offset: 0x0001D1FC
		public bool IsRequestLastBlow
		{
			get
			{
				return this.m_IsRequestLastBlow;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x0001EE04 File Offset: 0x0001D204
		// (set) Token: 0x06000626 RID: 1574 RVA: 0x0001EE59 File Offset: 0x0001D259
		public bool IsEnableTouchChangeTarget
		{
			get
			{
				BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
				if (seq != BattleTutorial.eSeq.None)
				{
					return (seq == BattleTutorial.eSeq._05_WAVE_IDX_0_TARGET || (seq == BattleTutorial.eSeq._07_WAVE_IDX_1_TIPS_START && this.BSD.WaveIdx == 0) || seq == BattleTutorial.eSeq.Max) && this.m_IsEnableTouchChangeTarget;
				}
				return this.m_IsEnableTouchChangeTarget;
			}
			set
			{
				this.m_IsEnableTouchChangeTarget = value;
			}
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x0001EE62 File Offset: 0x0001D262
		public void SystemAwake()
		{
			if (this.SystemAwakeCommonConstruct(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.WorkRecvID, SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.WorkQuestID))
			{
			}
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x0001EE90 File Offset: 0x0001D290
		private bool SystemAwakeCommonConstruct(long recvID, int questID)
		{
			if (this.m_IsAwaked)
			{
				return false;
			}
			this.m_IsAwaked = true;
			bool flag = false;
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.ADVPrologueIntro2Played_NextIsQuestLogSet && questID == 1100010)
			{
				flag = true;
			}
			this.m_ObjRoot = base.gameObject.transform;
			this.m_MainCamera = Camera.main;
			this.m_MainCameraTransform = Camera.main.transform;
			this.m_BattleCamera = this.m_MainCamera.GetComponent<BattleCamera>();
			this.m_SkillActionPlayer = base.gameObject.GetComponent<SkillActionPlayer>();
			this.m_SkillActionPlayer.Setup(this);
			this.m_DropObjectManager = new BattleDropObjectManager();
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			this.m_Result = questMng.GetResult();
			if (this.m_Result == null)
			{
				this.m_Result = questMng.CreateResult(questID, false, -1L);
			}
			QuestManager.QuestData questData = questMng.GetQuestData(questID);
			if (questData != null)
			{
				this.m_QuestParam = questData.m_Param;
			}
			bool flag2 = !flag && questMng.IsExistSaveResponse();
			if (flag2)
			{
				this.m_BSD = BattleUtility.SaveResponseToBSD(questMng.SaveResponse);
			}
			if (this.m_BSD == null)
			{
				this.m_BSD = new BattleSystemData();
			}
			this.BSD.Ver = "1.0.3";
			this.BSD.Phase = 0;
			this.BSD.RecvID = recvID;
			this.BSD.QuestID = questID;
			this.BSD.WaveNum = QuestListDB_Ext.GetWaveNum(ref this.m_QuestParam);
			if (this.BSD.Gauge == null)
			{
				this.BSD.Gauge = new BattleTogetherGauge();
			}
			if (this.BSD.TBuff == null)
			{
				this.BSD.TBuff = questMng.WorkTBuff;
				if (this.BSD.TBuff == null)
				{
					this.BSD.TBuff = TownBuff.CalcTownBuff();
				}
			}
			if (this.BSD.Enemies == null)
			{
				this.BSD.Enemies = questMng.WorkEnemies;
				if (this.BSD.Enemies == null)
				{
					BattleUtility.CreateOccurEnemy(ref this.m_QuestParam, out this.BSD.Enemies);
				}
			}
			if (this.BSD.ScheduleItems == null || this.BSD.ScheduleItems.Count == 0)
			{
				this.BSD.ScheduleItems = questMng.WorkScheduleItems;
				if (this.BSD.ScheduleItems == null || this.BSD.ScheduleItems.Count == 0)
				{
					List<int> charaIDs = this.CalcPlayerCharaIDs(this.BSD.PtyMngID);
					float probAdd = BattleUtility.CalcDropItemProbAdd(charaIDs, ref this.m_QuestParam);
					BattleUtility.CreateScheduleDropItems(this.BSD.Enemies, probAdd, out this.BSD.ScheduleItems);
				}
			}
			this.m_Result.SetupDropItems(this.BSD.ScheduleItems, this.BSD.WaveIdx);
			if (this.BSD.PLs == null || this.BSD.PLs.Length == 0)
			{
				this.BSD.PLs = new BattleSystemDataForPL[6];
				for (int i = 0; i < this.BSD.PLs.Length; i++)
				{
					BattleSystemDataForPL battleSystemDataForPL = new BattleSystemDataForPL();
					battleSystemDataForPL.Param = new CharacterBattleParam();
					battleSystemDataForPL.Cmds = new BattleCommandData[4];
					for (int j = 0; j < battleSystemDataForPL.Cmds.Length; j++)
					{
						battleSystemDataForPL.Cmds[j] = new BattleCommandData();
					}
					this.BSD.PLs[i] = battleSystemDataForPL;
				}
			}
			if (this.BSD.PLJoinIdxs == null || this.BSD.PLJoinIdxs.Length == 0)
			{
				this.BSD.PLJoinIdxs = new int[6];
				for (int k = 0; k < this.BSD.PLJoinIdxs.Length; k++)
				{
					this.BSD.PLJoinIdxs[k] = k;
				}
			}
			if (flag)
			{
				this.m_Tutorial.SetSeq(BattleTutorial.eSeq._00_WAVE_IDX_0_TIPS_START);
			}
			this.m_Mode = BattleSystem.eMode.Prepare;
			this.SetPrepareStep(BattleSystem.ePrepareStep.First);
			SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.OpenQuickIfNotDisplay(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
			return true;
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x0001F2C0 File Offset: 0x0001D6C0
		private List<int> CalcPlayerCharaIDs(long partyMngID)
		{
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData;
			if (partyMngID == -1L)
			{
				userBattlePartyData = userDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex];
			}
			else
			{
				userBattlePartyData = userDataMng.GetUserBattlePartyData(partyMngID);
			}
			List<int> list = new List<int>();
			int num = 5;
			for (int i = 0; i < num; i++)
			{
				int num2 = -1;
				long memberAt = userBattlePartyData.GetMemberAt(i);
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(memberAt);
				if (userCharaData != null)
				{
					num2 = userCharaData.Param.CharaID;
				}
				if (num2 != -1)
				{
					list.Add(num2);
				}
			}
			if (this.m_QuestParam.m_CpuFriendCharaID != -1)
			{
				list.Add(this.m_QuestParam.m_CpuFriendCharaID);
			}
			else if (globalParam.QuestFriendData != null && globalParam.QuestUserSupportData != null)
			{
				list.Add(globalParam.QuestUserSupportData.CharaID);
			}
			return list;
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x0001F3BB File Offset: 0x0001D7BB
		public void SystemDestoryOnlyUI()
		{
			if (this.m_Mode == BattleSystem.eMode.None)
			{
				return;
			}
			this.m_IsDestroyOnlyUI = true;
			this.m_Mode = BattleSystem.eMode.Destroy;
			this.SetDestroyStep(BattleSystem.eDestroyStep.First);
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x0001F3DF File Offset: 0x0001D7DF
		public bool IsComplteDestoryOnlyUI()
		{
			return this.m_Mode == BattleSystem.eMode.Destroy && this.m_DestroyStep == BattleSystem.eDestroyStep.End;
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x0001F3F9 File Offset: 0x0001D7F9
		public void SystemDestory()
		{
			if (this.m_Mode == BattleSystem.eMode.None)
			{
				return;
			}
			this.m_IsDestroyOnlyUI = false;
			this.m_Mode = BattleSystem.eMode.Destroy;
			this.SetDestroyStep(BattleSystem.eDestroyStep.First);
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x0001F41D File Offset: 0x0001D81D
		public bool IsComplteDestory()
		{
			return this.m_Mode == BattleSystem.eMode.Destroy && this.m_DestroyStep == BattleSystem.eDestroyStep.End;
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x0001F438 File Offset: 0x0001D838
		public void SystemUpdate()
		{
			if (!this.m_IsAwaked)
			{
				return;
			}
			this.m_Scaler.Update();
			this.m_Timer.Update(Time.deltaTime);
			this.m_DropObjectManager.Update();
			BattleSystem.eMode eMode = this.m_Mode;
			switch (this.m_Mode)
			{
			case BattleSystem.eMode.Prepare:
				eMode = this.Update_Prepare();
				break;
			case BattleSystem.eMode.BattleMain:
				eMode = this.Update_Main();
				break;
			case BattleSystem.eMode.Destroy:
				eMode = this.Update_Destroy();
				break;
			}
			if (eMode != this.m_Mode)
			{
				this.m_Mode = eMode;
				switch (this.m_Mode)
				{
				case BattleSystem.eMode.Prepare:
					this.SetPrepareStep(BattleSystem.ePrepareStep.First);
					break;
				case BattleSystem.eMode.BattleMain:
					this.SetMainStep(BattleSystem.eMainStep.First);
					break;
				}
			}
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x0001F520 File Offset: 0x0001D920
		public void SystemLateUpdate()
		{
			switch (this.m_Mode)
			{
			case BattleSystem.eMode.BattleMain:
				this.LateUpdate_Main();
				break;
			}
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x0001F569 File Offset: 0x0001D969
		private void OnDestroy()
		{
			if (this.m_BattleUI != null)
			{
				this.m_BattleUI.Destroy();
				this.m_BattleUI = null;
			}
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x0001F58E File Offset: 0x0001D98E
		public bool IsPrepareing()
		{
			return this.m_Mode <= BattleSystem.eMode.Prepare;
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x0001F59C File Offset: 0x0001D99C
		public bool IsBattleFinished()
		{
			return this.m_Mode == BattleSystem.eMode.BattleFinished;
		}

		// Token: 0x06000633 RID: 1587 RVA: 0x0001F5A7 File Offset: 0x0001D9A7
		private bool CheckClearAllWave()
		{
			return this.BSD.WaveIdx == this.BSD.WaveNum - 1 && this.EnemyParty.CheckDeadAll();
		}

		// Token: 0x06000634 RID: 1588 RVA: 0x0001F5DB File Offset: 0x0001D9DB
		private bool CheckClearCurrentWave()
		{
			return this.EnemyParty.CheckDeadAll();
		}

		// Token: 0x06000635 RID: 1589 RVA: 0x0001F5F0 File Offset: 0x0001D9F0
		private bool CheckGameOver()
		{
			return this.PlayerParty.CheckDeadAll();
		}

		// Token: 0x06000636 RID: 1590 RVA: 0x0001F5FD File Offset: 0x0001D9FD
		private bool IsWarningWave()
		{
			return this.m_QuestParam.m_IsWarning == 1 && this.BSD.WaveIdx == this.BSD.WaveNum - 1;
		}

		// Token: 0x06000637 RID: 1591 RVA: 0x0001F630 File Offset: 0x0001DA30
		private void SetupWaveData(int waveIndex)
		{
			this.BSD.WaveIdx = waveIndex;
			this.m_IsAfterOrderSlide = false;
			this.PlayerParty.WavePreProcess(this.BSD.WaveIdx);
			this.EnemyParty.WavePreProcess(this.BSD.WaveIdx);
			if (this.m_Order == null)
			{
				this.m_Order = new BattleOrder(this.BSD.Gauge);
			}
			else
			{
				this.m_Order.Reset();
			}
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					CharacterHandler member = this.m_Parties[i].GetMember((BattleDefine.eJoinMember)j);
					if (member != null && !member.CharaBattle.Param.IsDead())
					{
						float orderValue = BattleCommandParser.CalcOrderValue(member.CharaBattle.Param.FixedSpd(), 1f, 1f);
						this.m_Order.AddFrameData(BattleOrder.eFrameType.Chara, member, orderValue, null);
					}
				}
			}
			this.m_Order.SortOrder();
			bool flag = this.m_Order.CanBeTogehterAttack();
			this.m_Order.GetTogetherData().SetupOnTurnStart(flag);
			this.m_Order.SetTogetherAttackExecutable(flag);
			this.m_InputData.Reset();
			this.ClearExecCommand();
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x0001F778 File Offset: 0x0001DB78
		private BattleOrder.eFrameType TurnStartProcess()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			this.m_Order.UpdateOrderValue();
			this.m_Order.SaveFrames();
			bool flag = this.m_Order.CanBeTogehterAttack();
			this.m_Order.GetTogetherData().SetupOnTurnStart(flag);
			this.m_Order.SetTogetherAttackExecutable(flag);
			this.IsEnableTouchChangeTarget = false;
			this.m_ExecutedMemberChange = false;
			this.m_IsGoBackFriendCheck = false;
			if (frameDataAt.IsCharaType)
			{
				for (int i = 0; i < 2; i++)
				{
					this.m_Parties[i].TurnStartProcess(owner);
				}
				this.SetupBattleUIOnTurnStart(owner);
			}
			this.m_IsAfterOrderSlide = false;
			this.RequestSaveIfFailed();
			return frameDataAt.m_FrameType;
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x0001F838 File Offset: 0x0001DC38
		private void TurnEndProcess()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler characterHandler = null;
			if (frameDataAt.IsCharaType)
			{
				characterHandler = frameDataAt.m_Owner;
			}
			this.m_SkillActionPlayer.Destroy();
			BattleCommandSolveResult battleCommandSolveResult = null;
			if (this.m_ExecCommand != null)
			{
				battleCommandSolveResult = this.m_ExecCommand.SolveResult;
			}
			if (battleCommandSolveResult != null && !this.m_Order.GetTogetherData().IsAvailable())
			{
				int num = 0;
				float num2 = 0f;
				int num3 = 0;
				float num4 = 0f;
				float num5 = 1f;
				foreach (CharacterHandler characterHandler2 in battleCommandSolveResult.m_CharaResults.Keys)
				{
					if (characterHandler2.CharaBattle.MyParty.IsPlayerParty)
					{
						float num6 = 0f;
						int damageHitDataNum = battleCommandSolveResult.m_CharaResults[characterHandler2].GetDamageHitDataNum();
						for (int i = 0; i < damageHitDataNum; i++)
						{
							BattleCommandSolveResult.HitData damageHitDataAt = battleCommandSolveResult.m_CharaResults[characterHandler2].GetDamageHitDataAt(i);
							int registHit_Or_defaultHit_Or_weakHit = damageHitDataAt.m_registHit_Or_defaultHit_Or_weakHit;
							if (registHit_Or_defaultHit_Or_weakHit != -1)
							{
								if (registHit_Or_defaultHit_Or_weakHit != 0)
								{
									if (registHit_Or_defaultHit_Or_weakHit == 1)
									{
										if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage_weak) > num6)
										{
											num6 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage_weak);
										}
									}
								}
								else if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage) > num6)
								{
									num6 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage);
								}
							}
							else if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage_regist) > num6)
							{
								num6 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnDamage_regist);
							}
						}
						num++;
						num2 = num6;
					}
					else
					{
						float num7 = 0f;
						int damageHitDataNum2 = battleCommandSolveResult.m_CharaResults[characterHandler2].GetDamageHitDataNum();
						for (int j = 0; j < damageHitDataNum2; j++)
						{
							BattleCommandSolveResult.HitData damageHitDataAt2 = battleCommandSolveResult.m_CharaResults[characterHandler2].GetDamageHitDataAt(j);
							int registHit_Or_defaultHit_Or_weakHit2 = damageHitDataAt2.m_registHit_Or_defaultHit_Or_weakHit;
							if (registHit_Or_defaultHit_Or_weakHit2 != -1)
							{
								if (registHit_Or_defaultHit_Or_weakHit2 != 0)
								{
									if (registHit_Or_defaultHit_Or_weakHit2 == 1)
									{
										if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack_weak) > num7)
										{
											num7 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack_weak);
										}
									}
								}
								else if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack) > num7)
								{
									num7 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack);
								}
							}
							else if (BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack_regist) > num7)
							{
								num7 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttack_regist);
							}
							if (damageHitDataAt2.m_IsCritical)
							{
								num5 = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnAttackCriticalCoef);
							}
						}
						num3++;
						num4 = num7;
					}
				}
				if (num > 0)
				{
					num2 /= (float)num;
					this.CalcTogetherAttackGauge(num2);
				}
				if (num3 > 0)
				{
					num4 /= (float)num3;
					num4 *= num5;
					this.CalcTogetherAttackGauge(num4);
				}
			}
			if (this.m_ExecCommand != null)
			{
				this.m_ExecCommand.SolveResult.Clear();
			}
			for (int k = 0; k < 2; k++)
			{
				this.m_Parties[k].TurnEndProcess(characterHandler);
			}
			this.UpdateAllCharacterStatus(1, true, true);
			if (frameDataAt.IsCharaType)
			{
				if (this.m_Order.GetTogetherData().IsAvailable())
				{
					characterHandler.CharaBattle.MyParty.IncrementActionCount(BattlePartyData.eActionCountType.TogetherAttackUseCount);
					characterHandler.CharaBattle.MyParty.ResetTogetherAttackChainCoefChange();
					int count = this.m_Order.GetTogetherData().GetCount();
					this.CalcTogetherAttackGauge((float)(-(float)count));
					List<CharacterHandler> list = new List<CharacterHandler>();
					for (int l = 0; l < this.m_Order.GetTogetherData().GetJoinOrder().Count; l++)
					{
						list.Add(this.PlayerParty.GetMember(this.m_Order.GetTogetherData().GetJoinOrder()[l]));
					}
					BattleCommandParser.UpdateOrderAfterTogetherAttack(this.m_Order, list);
					BattleOrderIndicator order = this.m_BattleUI.GetOrder();
					order.SetFrames(this.m_Order, 0, BattleOrderIndicator.eMoveType.Immediate);
					order.SetEnableRender(true);
					this.m_BattleUI.GetTogetherButton().SetEnableRender(true);
				}
				else if (this.m_ExecCommand != null)
				{
					if (characterHandler.CharaBattle.MyParty.IsPlayerParty && !characterHandler.IsFriendChara)
					{
						bool flag = this.m_ExecCommand.IncrementSkillUseNum();
						if (flag)
						{
							this.m_BattleUI.PlayCommandButtonLevelup(this.m_ExecCommand.CommandIndex);
						}
					}
					BattleCommandData.eCommandType commandType = this.m_ExecCommand.CommandType;
					if (commandType != BattleCommandData.eCommandType.NormalAttack)
					{
						if (commandType == BattleCommandData.eCommandType.Skill || commandType == BattleCommandData.eCommandType.WeaponSkill)
						{
							characterHandler.CharaBattle.MyParty.IncrementActionCount(BattlePartyData.eActionCountType.SkillUseCount);
						}
					}
					else
					{
						characterHandler.CharaBattle.MyParty.IncrementActionCount(BattlePartyData.eActionCountType.NormalAttackUseCount);
					}
				}
			}
			this.ClearExecCommand();
			this.m_DeadPlayerDatas.Clear();
			this.m_AutoMemberChangeDatas.Clear();
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x0001FD20 File Offset: 0x0001E120
		private bool TurnSkipProcess()
		{
			bool flag = false;
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			if (owner.CharaBattle.IsStun())
			{
				flag = true;
				string text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeStun, new object[]
				{
					owner.GetNickName()
				}), new object[0]);
				this.m_BattleUI.GetBattleMessage().SetMessage(text, false);
			}
			else if (owner.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Sleep))
			{
				flag = true;
				string text2 = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeSleepOn, new object[]
				{
					owner.GetNickName()
				}), new object[0]);
				this.m_BattleUI.GetBattleMessage().SetMessage(text2, false);
			}
			else if (BattleCommandParser.SolveParalysisCancel(owner))
			{
				flag = true;
				string text3 = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeParalysis, new object[]
				{
					owner.GetNickName()
				}), new object[0]);
				this.m_BattleUI.GetBattleMessage().SetMessage(text3, false);
			}
			if (flag)
			{
				int selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, -1, false);
				BattleOrderIndicator order = this.m_BattleUI.GetOrder();
				order.SetFrames(this.m_Order, selectingIndex, BattleOrderIndicator.eMoveType.Selecting);
			}
			return flag;
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x0001FE80 File Offset: 0x0001E280
		private bool StunCheckProcess()
		{
			this.m_StunCharaHndls = BattleCommandParser.UpdateOrderOnStun(this.m_Order);
			if (this.m_StunCharaHndls != null && this.m_StunCharaHndls.Count > 0)
			{
				if (this.m_StunCharaHndls[0].CharaBattle.MyParty.IsPlayerParty)
				{
					this.m_BattleCamera.ZoomStart(BattleCamera.eZoomType.ToPlayerOnStun);
				}
				else
				{
					this.m_BattleCamera.ZoomStart(BattleCamera.eZoomType.ToEnemyOnStun);
				}
				this.m_BattleUI.ShiftDownCommandSet(true, false);
				return true;
			}
			return false;
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x0001FF08 File Offset: 0x0001E308
		private void StunOccurProcess()
		{
			if (this.m_StunCharaHndls != null && this.m_StunCharaHndls.Count > 0)
			{
				for (int i = 0; i < this.m_StunCharaHndls.Count; i++)
				{
					this.m_StunCharaHndls[i].CharaBattle.AttachStunOccurEffect();
				}
				if (this.m_StunCharaHndls[0].CharaBattle.MyParty.IsPlayerParty)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_STUN_1, 1f, 0, -1, -1);
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_STUN_2, 1f, 0, -1, -1);
				}
			}
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x0001FFC4 File Offset: 0x0001E3C4
		private void StunApplyProcess()
		{
			if (this.m_StunCharaHndls != null && this.m_StunCharaHndls.Count > 0)
			{
				for (int i = 0; i < this.m_StunCharaHndls.Count; i++)
				{
					this.m_StunCharaHndls[i].CharaBattle.RequestIdleMode(0.25f, 0f, true);
				}
				this.m_BattleCamera.Shake(BattleCamera.eShakeType.Stun);
				this.TimeScaler.SetChangeTimeScale(0f, 0.10000001f, 0.5f);
				BattleOrderIndicator order = this.m_BattleUI.GetOrder();
				order.SlideStunFrames(this.m_Order);
				CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
				if (frameOwnerAt.CharaBattle.MyParty.IsPlayerParty)
				{
					int num = 0;
					for (int j = 0; j < this.m_StunCharaHndls.Count; j++)
					{
						if (this.m_StunCharaHndls[j].CharaBattle.MyParty.IsEnemyParty)
						{
							num++;
						}
					}
					float value = BattleUtility.DefVal(eBattleDefineDB.TogetherCharge_OnStun) * (float)num;
					this.CalcTogetherAttackGauge(value);
				}
			}
		}

		// Token: 0x0600063E RID: 1598 RVA: 0x000200E5 File Offset: 0x0001E4E5
		private void StunApplyPostProcess()
		{
			this.m_BattleCamera.ToDefault(0f);
			this.m_BattleUI.ShiftDownCommandSet(false, false);
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x00020104 File Offset: 0x0001E504
		private bool TurnEndPreProcess(BattleDefine.eTurnPreEndEvent type)
		{
			if (type != BattleDefine.eTurnPreEndEvent.AwakeFromSleep)
			{
				if (type != BattleDefine.eTurnPreEndEvent.AbnormalPoison)
				{
					if (type == BattleDefine.eTurnPreEndEvent.Regene)
					{
						BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
						if (frameDataAt.IsCharaType)
						{
							CharacterHandler owner = frameDataAt.m_Owner;
							if (owner.CharaBattle.Param.GetRegene().IsEnable())
							{
								bool isUnhappy = false;
								int num = BattleCommandParser.SolveRegene(owner, out isUnhappy);
								if (num > 0)
								{
									owner.CharaBattle.RequestAutoFacial(9, 1f);
								}
								Transform targetTransform;
								float offsetYfromTargetTransform;
								owner.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out targetTransform, out offsetYfromTargetTransform);
								this.m_BattleUI.DisplayDamage(num, false, 0, isUnhappy, false, targetTransform, offsetYfromTargetTransform);
								this.UpdateUICharaStatus(owner.CharaBattle.MyParty.PartyType, owner.CharaBattle.Join, 1, true, false);
								string effectID = "ef_btl_recover_00";
								Vector3 position = owner.CacheTransform.position;
								int sortingOrder = 1000;
								SkillActionUtility.CorrectEffectParam(this.m_MainCamera, effectID, ref position, ref sortingOrder);
								SingletonMonoBehaviour<EffectManager>.Inst.Play(effectID, position, Quaternion.Euler(0f, 180f, 0f), Vector3.one, owner.CacheTransform.parent, false, sortingOrder, 1f, true);
								return true;
							}
						}
					}
				}
				else
				{
					BattleOrder.FrameData frameDataAt2 = this.m_Order.GetFrameDataAt(0);
					if (frameDataAt2.IsCharaType)
					{
						CharacterHandler owner2 = frameDataAt2.m_Owner;
						if (owner2.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Poison))
						{
							int value = BattleCommandParser.SolvePoison(owner2);
							owner2.CharaBattle.RequestDamageMode();
							Transform targetTransform2;
							float offsetYfromTargetTransform2;
							owner2.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out targetTransform2, out offsetYfromTargetTransform2);
							this.m_BattleUI.DisplayDamage(value, false, 0, false, true, targetTransform2, offsetYfromTargetTransform2);
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ACT_HIT_1_SLASH, 1f, 0, -1, -1);
							this.UpdateUICharaStatus(owner2.CharaBattle.MyParty.PartyType, owner2.CharaBattle.Join, 1, true, false);
							return true;
						}
					}
				}
			}
			else
			{
				BattleCommandSolveResult battleCommandSolveResult = null;
				if (this.m_ExecCommand != null)
				{
					battleCommandSolveResult = this.m_ExecCommand.SolveResult;
				}
				List<CharacterHandler> list = new List<CharacterHandler>();
				if (battleCommandSolveResult != null)
				{
					foreach (CharacterHandler characterHandler in battleCommandSolveResult.m_CharaResults.Keys)
					{
						if (characterHandler.CharaBattle.Param.CanPossibleCancelSleep())
						{
							list.Add(characterHandler);
						}
					}
				}
				if (list.Count > 0)
				{
					if (list.Count == 1)
					{
						this.m_BattleUI.GetBattleMessage().SetMessage(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeSleepOff_Single, new object[]
						{
							list[0].GetNickName()
						}), false);
					}
					else
					{
						this.m_BattleUI.GetBattleMessage().SetMessage(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeSleepOff_Multiple), false);
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000640 RID: 1600 RVA: 0x00020424 File Offset: 0x0001E824
		private void OrderSlide()
		{
			this.m_IsAfterOrderSlide = true;
			this.m_Order.SlideFrames();
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SlideFrames(this.m_Order);
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			if (frameDataAt.IsCharaType)
			{
				CharacterHandler owner = frameDataAt.m_Owner;
				if (owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					this.m_BattleUI.SetOwnerImage(owner.CharaParam.CharaID, true);
				}
				if (owner.CharaBattle.MyParty.IsPlayerParty)
				{
					this.SetupCommandButton(true, false);
				}
				else
				{
					this.m_BattleUI.SetCommandSetBlank();
				}
			}
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x000204D4 File Offset: 0x0001E8D4
		private bool CommandInputStart()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			if (owner.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Confusion))
			{
				this.CommandDecisonByConfusion();
				return true;
			}
			if (owner.CharaBattle.MyParty.IsPlayerParty)
			{
				if (this.m_Option.GetIsAuto())
				{
					this.CommandDecisonByAuto();
				}
				else
				{
					this.IsEnableTouchChangeTarget = true;
					this.m_BattleUI.StartCommandInput();
					this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
					int num = 0;
					this.m_BattleUI.SetCommandButtonSelecting(num);
					this.SelectCommandButton(num, true);
				}
				this.m_Scaler.IsDirty = true;
				return false;
			}
			this.CommandDecisionByAI();
			this.m_Scaler.IsDirty = true;
			return false;
		}

		// Token: 0x06000642 RID: 1602 RVA: 0x000205AC File Offset: 0x0001E9AC
		private void CommandDecisonByConfusion()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			int num = BattleCommandParser.SolveConfusionCommand(owner);
			if (owner.CharaBattle.MyParty.IsPlayerParty)
			{
				this.m_BattleUI.SetCommandButtonDecided(num, true);
			}
			this.SelectCommandButton(num, true);
			this.m_InputData.InputCommand_Decide(false);
			string text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeConfusion, new object[]
			{
				owner.GetNickName()
			}), new object[0]);
			this.m_BattleUI.GetBattleMessage().SetMessage(text, false);
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x0002064C File Offset: 0x0001EA4C
		private void CommandDecisonByAuto()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			int num = BattleCommandParser.SolveAutoCommand(owner);
			if (owner.CharaBattle.MyParty.IsPlayerParty)
			{
				this.m_BattleUI.SetCommandButtonDecided(num, true);
			}
			this.SelectCommandButton(num, true);
			this.m_InputData.InputCommand_Decide(false);
		}

		// Token: 0x06000644 RID: 1604 RVA: 0x000206AC File Offset: 0x0001EAAC
		private void CommandDecisionByAI()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			BattleAISolveResult battleAISolveResult = BattleAIParser.SolveBattleAI(owner);
			this.m_InputData.SelectCommandIndex = battleAISolveResult.m_CommandIndex;
			int selectingIndex;
			BattleCommandData battleCommandData;
			if (battleAISolveResult.m_IsChargeSkill)
			{
				selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, this.m_InputData.SelectCommandIndex, true);
				battleCommandData = owner.CharaBattle.GetUniqueSkillCommandData();
			}
			else
			{
				selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, this.m_InputData.SelectCommandIndex, false);
				battleCommandData = owner.CharaBattle.GetCommandDataAt(this.m_InputData.SelectCommandIndex);
			}
			BattleCommandParser.OptimizeTargetIndex(owner, battleCommandData.MainSkillTargetType);
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SetFrames(this.m_Order, selectingIndex, BattleOrderIndicator.eMoveType.Selecting);
			this.m_InputData.InputCommand_Decide(battleAISolveResult.m_IsChargeSkill);
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x0002078C File Offset: 0x0001EB8C
		private void ExecCommandOfTurnOwner()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			this.m_ExecCommand = owner.CharaBattle.RequestCommandMode(this.m_SkillActionPlayer, this.m_InputData.SelectCommandIndex, this.m_InputData.SelectIsUniqueSkill, 0);
			this.m_BattleUI.GetBattleMessage().SetMessage(this.m_ExecCommand.SkillParam.m_SkillName, false);
			this.m_BattleUI.GetOrder().MoveOwnerFrameToSelectFrame();
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x00020810 File Offset: 0x0001EC10
		private void ExecSkillCard()
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			int selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, -1, false);
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SetFrames(this.m_Order, selectingIndex, BattleOrderIndicator.eMoveType.Selecting);
			order.MoveOwnerFrameToSelectFrame();
			this.m_ExecCommand = frameDataAt.m_CardArgs.m_Command;
			SkillActionPlan sap = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FindSAP(this.m_ExecCommand.SAP_ID);
			SkillActionGraphics sag = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FindSAG(this.m_ExecCommand.SAG_ID);
			List<CharacterHandler> tgtCharaList;
			List<CharacterHandler> myCharaList;
			BattleCommandParser.GetTakeCharaList(this.m_ExecCommand, out tgtCharaList, out myCharaList);
			this.m_SkillActionPlayer.Play(sap, sag, frameDataAt.m_Owner, this.m_ExecCommand, tgtCharaList, myCharaList, null);
			this.m_BattleUI.GetBattleMessage().SetMessage(frameDataAt.m_CardArgs.m_Command.SkillParam.m_SkillName, false);
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x000208FC File Offset: 0x0001ECFC
		private bool IsCompleteExecCommandAnim()
		{
			return (!(this.m_SkillActionPlayer != null) || !this.m_SkillActionPlayer.IsPlaying()) && (this.m_ExecCommand == null || this.m_ExecCommand.Owner.CharaBattle.Mode != CharacterBattle.eMode.Command);
		}

		// Token: 0x06000648 RID: 1608 RVA: 0x00020955 File Offset: 0x0001ED55
		private void ClearExecCommand()
		{
			this.m_ExecCommand = null;
			this.m_InputData.Reset();
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x0002096C File Offset: 0x0001ED6C
		private void PostAfterExecCommand(BattleCommandSolveResult solveResult)
		{
			foreach (CharacterHandler characterHandler in solveResult.m_CharaDiedWithThisCommand.Keys)
			{
				int orderIndex;
				float orderValue;
				this.m_Order.RemoveFrameData(BattleOrder.eFrameType.Chara, characterHandler, out orderIndex, out orderValue);
				if (characterHandler.CharaBattle.MyParty.IsPlayerParty)
				{
					BattleSystem.DeadCharaData item;
					item.m_Join = characterHandler.CharaBattle.Join;
					item.m_OrderIndex = orderIndex;
					item.m_OrderValue = orderValue;
					this.m_DeadPlayerDatas.Add(item);
				}
				else
				{
					BattleDropItem battleDropItem = this.JudgeDropItem(this.BSD.WaveIdx, characterHandler.CharaBattle.Join);
					if (battleDropItem != null)
					{
						this.m_DropItemsFromChara.AddOrReplace(characterHandler, battleDropItem);
					}
				}
				characterHandler.CharaBattle.RequestDeadMode();
				characterHandler.CharaBattle.Param.UpdateDeadDetailIfReady();
			}
			if (this.m_DeadPlayerDatas != null && this.m_DeadPlayerDatas.Count > 0)
			{
				int index = UnityEngine.Random.Range(0, this.m_DeadPlayerDatas.Count);
				this.m_CharaHndlForGameOver = this.PlayerParty.GetMember(this.m_DeadPlayerDatas[index].m_Join);
			}
			this.UpdateAllCharacterStatus(2, true, false);
			this.m_BattleUI.PlayInReward();
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x00020AD8 File Offset: 0x0001EED8
		private bool IsCompletePostAfterExecCommand()
		{
			for (int i = 0; i < 2; i++)
			{
				BattlePartyData battlePartyData = this.m_Parties[i];
				for (int j = 0; j < 3; j++)
				{
					CharacterHandler member = battlePartyData.GetMember((BattleDefine.eJoinMember)j);
					if (!(member == null))
					{
						if (member.CharaBattle.IsPlayingDeadAnim())
						{
							return false;
						}
					}
				}
			}
			return !this.m_DropObjectManager.IsPlayingTreasureBox();
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x00020B50 File Offset: 0x0001EF50
		private void OnEndDeadAnimation(CharacterHandler charaHndl)
		{
			if (this.m_DropItemsFromChara.ContainsKey(charaHndl))
			{
				BattleDropItem dropItem = this.m_DropItemsFromChara[charaHndl];
				Vector3 position = charaHndl.CacheTransform.position;
				Vector3 targetPos = this.m_BattleUI.GetWorldRewardPosition();
				int sortingOrder = charaHndl.CharaAnim.GetSortingOrder() + 1;
				this.m_DropObjectManager.PlayTreasureBox(dropItem, position, targetPos, this.m_ObjRoot, sortingOrder, new Action<BattleDropItem>(this.OnCompleteDropItem));
				this.m_DropItemsFromChara.Remove(charaHndl);
			}
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x00020BD4 File Offset: 0x0001EFD4
		private void OnCompleteDropItem(BattleDropItem dropItem)
		{
			this.m_Result.AddItem(dropItem);
			this.m_BattleUI.GetReward().SetRewardNum(dropItem.GetTreasureBoxIndex(), this.m_Result.m_DropItems.Count, true);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x00020C0C File Offset: 0x0001F00C
		public void JudgeAndRequestLastBlow(CharacterHandler charaHndl)
		{
			if (this.m_IsRequestLastBlow)
			{
				return;
			}
			if (!this.EnemyParty.CheckDeadAll())
			{
				return;
			}
			if (this.BSD.WaveIdx != this.BSD.WaveNum - 1)
			{
				return;
			}
			this.m_IsRequestLastBlow = true;
			this.m_LastBlowCharaHndl = charaHndl;
			if (this.m_UniqueSkillScene == null)
			{
				this.m_Scaler.SetChangeTimeScale(0.5f, 2.4f, 1f);
				this.m_BattleCamera.Shake(BattleCamera.eShakeType.LastBlow);
			}
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00020C94 File Offset: 0x0001F094
		public void CalcTogetherAttackGauge(float value)
		{
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			if (seq != BattleTutorial.eSeq.None && seq < BattleTutorial.eSeq._11_WAVE_IDX_1_TIPS_UNIQUE_SKILL)
			{
				value = 0f;
			}
			this.m_Order.GetGauge().CalcGauge(value);
			this.m_BattleUI.GetTogetherButton().Apply(this.m_Order.GetGauge().GetValue(), false);
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x00020CF6 File Offset: 0x0001F0F6
		private bool CanBeContinueTogetherAttack()
		{
			return this.m_Order.GetTogetherData().IsAvailable() && !this.m_Order.GetTogetherData().IsEnd() && !this.CheckClearCurrentWave();
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x00020D34 File Offset: 0x0001F134
		private bool IsExistUniqueSkillSceneOfTurnOwner()
		{
			int count = this.m_Order.GetTogetherData().GetCount();
			List<BattleDefine.eJoinMember> joinOrder = this.m_Order.GetTogetherData().GetJoinOrder();
			BattleDefine.eJoinMember join = joinOrder[count];
			CharacterHandler member = this.PlayerParty.GetMember(join);
			BattleCommandData uniqueSkillCommandData = member.CharaBattle.GetUniqueSkillCommandData();
			return uniqueSkillCommandData.IsExistUniqueSkillScene;
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x00020D94 File Offset: 0x0001F194
		private bool ExecTogetherAttackOfTurnOwner()
		{
			int count = this.m_Order.GetTogetherData().GetCount();
			List<BattleDefine.eJoinMember> joinOrder = this.m_Order.GetTogetherData().GetJoinOrder();
			BattleDefine.eJoinMember join = joinOrder[count];
			CharacterHandler member = this.PlayerParty.GetMember(join);
			BattleCommandData uniqueSkillCommandData = member.CharaBattle.GetUniqueSkillCommandData();
			this.m_ExecCommand = uniqueSkillCommandData;
			BattleCommandParser.OptimizeTargetIndex(member, this.m_ExecCommand.MainSkillTargetType);
			this.m_ExecCommand.IncrementSkillUseNum();
			bool isExistUniqueSkillScene = this.m_ExecCommand.IsExistUniqueSkillScene;
			if (isExistUniqueSkillScene)
			{
				this.m_Order.GetTogetherData().IncreaseCount();
				this.m_UniqueSkillScene = new BattleUniqueSkillScene();
				CharacterHandler tgtSingleCharaHndlFromCommand = BattleCommandParser.GetTgtSingleCharaHndlFromCommand(member, this.m_ExecCommand);
				List<CharacterHandler> list = BattleCommandParser.GetTgtAllCharaHndlsFromCommand(member, this.m_ExecCommand);
				if (list == null)
				{
					list = new List<CharacterHandler>();
					if (tgtSingleCharaHndlFromCommand != null)
					{
						list.Add(tgtSingleCharaHndlFromCommand);
					}
				}
				CharacterHandler mySingleCharaHndlFromCommand = BattleCommandParser.GetMySingleCharaHndlFromCommand(member, this.m_ExecCommand);
				List<CharacterHandler> list2 = BattleCommandParser.GetMyAllCharaHndlsFromCommand(member, this.m_ExecCommand, true);
				if (list2 == null)
				{
					list2 = new List<CharacterHandler>();
					if (mySingleCharaHndlFromCommand != null)
					{
						list2.Add(mySingleCharaHndlFromCommand);
					}
				}
				bool isFadeOutStart = true;
				this.m_UniqueSkillScene.Prepare(uniqueSkillCommandData.UniqueSkillScene, this, this.m_MainCamera, member, list, tgtSingleCharaHndlFromCommand, list2, mySingleCharaHndlFromCommand, isFadeOutStart, count);
				this.m_BattleUI.Hide();
				this.m_BattleUI.PlayOutReward();
			}
			else
			{
				this.m_BattleUI.GetTransitFade().PlayOut();
			}
			return isExistUniqueSkillScene;
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x00020F14 File Offset: 0x0001F314
		private void ExecTogetherAttackOfTurnOwnerNotEffect()
		{
			int count = this.m_Order.GetTogetherData().GetCount();
			List<BattleDefine.eJoinMember> joinOrder = this.m_Order.GetTogetherData().GetJoinOrder();
			BattleDefine.eJoinMember join = joinOrder[count];
			CharacterHandler member = this.PlayerParty.GetMember(join);
			this.m_ExecCommand = member.CharaBattle.RequestCommandMode(this.m_SkillActionPlayer, this.m_InputData.SelectCommandIndex, this.m_InputData.SelectIsUniqueSkill, count);
			this.m_Order.GetTogetherData().IncreaseCount();
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x00020F98 File Offset: 0x0001F398
		private void DisplaySkillNameTogetherAttackOfTurnOwner()
		{
			int count = this.m_Order.GetTogetherData().GetCount();
			List<BattleDefine.eJoinMember> joinOrder = this.m_Order.GetTogetherData().GetJoinOrder();
			BattleDefine.eJoinMember join = joinOrder[count];
			CharacterHandler member = this.PlayerParty.GetMember(join);
			BattleCommandData uniqueSkillCommandData = member.CharaBattle.GetUniqueSkillCommandData();
			this.m_BattleUI.GetUniqueSkillNameWindow().SetText(uniqueSkillCommandData.SkillParam.m_SkillName, null, uniqueSkillCommandData.SkillLv);
		}

		// Token: 0x06000654 RID: 1620 RVA: 0x00021014 File Offset: 0x0001F414
		private bool PostTogetherAttackOfTurnOwner()
		{
			this.PostAfterExecCommand(this.m_ExecCommand.SolveResult);
			int totalDamageHitNum = this.m_ExecCommand.SolveResult.GetTotalDamageHitNum();
			int totalRecoverHitNum = this.m_ExecCommand.SolveResult.GetTotalRecoverHitNum();
			int totalDamageHitValue = this.m_ExecCommand.SolveResult.GetTotalDamageHitValue();
			int totalRecoverHitValue = this.m_ExecCommand.SolveResult.GetTotalRecoverHitValue();
			if (totalDamageHitNum > 0 && totalRecoverHitNum == 0)
			{
				this.m_BattleUI.GetTotalDamageDisplay().PlayTotalDamageOnly(1.8f, totalDamageHitValue, this.m_ExecCommand.Owner.CharaBattle.TogetherAttackChainCount);
			}
			else if (totalRecoverHitNum > 0 && totalDamageHitNum == 0)
			{
				this.m_BattleUI.GetTotalDamageDisplay().PlayTotalRecoverOnly(1.8f, totalRecoverHitValue, this.m_ExecCommand.Owner.CharaBattle.TogetherAttackChainCount);
			}
			else if (totalDamageHitNum != 0 || totalRecoverHitNum != 0)
			{
				this.m_BattleUI.GetTotalDamageDisplay().Play(1.8f, totalDamageHitValue, totalRecoverHitValue, 0);
			}
			return totalDamageHitNum != 0 || totalRecoverHitNum != 0;
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x00021124 File Offset: 0x0001F524
		private void ReturnFromCancelTogetherAttack()
		{
			int num = this.m_InputData.SelectCommandIndex;
			if (num == -1)
			{
				num = 0;
			}
			this.m_InputData.Reset();
			TogetherButton togetherButton = this.m_BattleUI.GetTogetherButton();
			togetherButton.SetEnableRender(true);
			this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
			this.m_BattleUI.SetCommandButtonSelecting(num);
			this.SelectCommandButton(num, true);
			this.SetTurnOwnerMarker(true);
		}

		// Token: 0x06000656 RID: 1622 RVA: 0x000211A0 File Offset: 0x0001F5A0
		private void ExecMasterSkill()
		{
			this.BSD.MstSkillFlg = true;
			this.BSD.MstSkillCount++;
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			this.m_ExecCommand = this.m_MasterOrbData.GetCommandDataAt(this.m_InputData.SelectMasterSkillIndex);
			this.m_ExecCommand.SetOwner(frameOwnerAt);
			if (this.m_InputData.SelectSingleTargetIndex != BattleDefine.eJoinMember.None)
			{
				frameOwnerAt.CharaBattle.MyParty.SingleTargetIndex = this.m_InputData.SelectSingleTargetIndex;
			}
			this.m_Order.RevertFrames();
			SkillActionPlan sap = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FindSAP(this.m_ExecCommand.SAP_ID);
			SkillActionGraphics sag = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FindSAG(this.m_ExecCommand.SAG_ID);
			List<CharacterHandler> tgtCharaList;
			List<CharacterHandler> myCharaList;
			BattleCommandParser.GetTakeCharaList(this.m_ExecCommand, out tgtCharaList, out myCharaList);
			this.m_SkillActionPlayer.Play(sap, sag, frameOwnerAt, this.m_ExecCommand, tgtCharaList, myCharaList, null);
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00021298 File Offset: 0x0001F698
		private void ChangePlayerJoinMember_GotoBench()
		{
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			if (frameOwnerAt != null)
			{
				this.ChangePlayerJoinMember_GotoBenchMove(frameOwnerAt);
				this.SetTurnOwnerMarker(false);
			}
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x000212CC File Offset: 0x0001F6CC
		private void ChangePlayerJoinMember_GotoBenchMove(CharacterHandler charaHndl)
		{
			Vector3 backLocatorPos = this.m_PosLocator_Player.GetBackLocatorPos((int)charaHndl.CharaBattle.Join);
			backLocatorPos.z = charaHndl.CacheTransform.localPosition.z;
			charaHndl.CharaBattle.RequestOutMode(backLocatorPos, 0f, false);
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x0002131C File Offset: 0x0001F71C
		private bool IsCompleteGotoBench()
		{
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			return frameOwnerAt != null && frameOwnerAt.CharaBattle.IsArrivalOutToPos();
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x00021350 File Offset: 0x0001F750
		private bool ChangePlayerJoinMember_GotoJoin()
		{
			CharacterHandler member = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_1 + this.m_InputData.SelectBenchIndex);
			if (!(member != null))
			{
				return false;
			}
			if (member.IsDonePreparePerfect())
			{
				BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
				this.ChangePlayerJoinMember_GotoJoinMove(member, (int)frameDataAt.m_Owner.CharaBattle.Join);
				return true;
			}
			member.PrepareWithoutResourceSetup(false);
			return false;
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x000213C0 File Offset: 0x0001F7C0
		private bool IsDonePrepareChangePlayerJoinMember()
		{
			CharacterHandler member = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_1 + this.m_InputData.SelectBenchIndex);
			return !(member != null) || member.IsDonePreparePerfect();
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x00021404 File Offset: 0x0001F804
		private void ChangePlayerJoinMember_GotoJoinMove(CharacterHandler charaHndl, int locatorIndex)
		{
			Vector3 backLocatorPos = this.m_PosLocator_Player.GetBackLocatorPos(locatorIndex);
			Vector3 locatorPos = this.m_PosLocator_Player.GetLocatorPos(locatorIndex);
			charaHndl.CharaBattle.RequestInMode(0f);
			charaHndl.CharaBattle.MoveToPositionTime(backLocatorPos, locatorPos, 6f * (1f / (float)Application.targetFrameRate), 0f);
			charaHndl.SetEnableRender(true);
		}

		// Token: 0x0600065D RID: 1629 RVA: 0x00021468 File Offset: 0x0001F868
		private bool IsCompleteGotoJoin()
		{
			for (BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Bench_1; eJoinMember <= BattleDefine.eJoinMember.Bench_Friend; eJoinMember++)
			{
				CharacterHandler member = this.PlayerParty.GetMember(eJoinMember);
				if (!(member == null))
				{
					if (member.CharaBattle.IsMoveTo)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600065E RID: 1630 RVA: 0x000214B8 File Offset: 0x0001F8B8
		private void ChangePlayerJoinMember()
		{
			this.PlayerParty.IncrementActionCount(BattlePartyData.eActionCountType.MemberChangeCount);
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			BattleDefine.eJoinMember join = frameDataAt.m_Owner.CharaBattle.Join;
			BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Bench_1 + this.m_InputData.SelectBenchIndex;
			CharacterHandler member = this.PlayerParty.GetMember(join);
			CharacterHandler member2 = this.PlayerParty.GetMember(eJoinMember);
			if (member != null)
			{
				member.SetEnableRender(false);
			}
			this.PlayerParty.ChangeJoinMember(join, eJoinMember);
			this.m_Order.RevertFrames();
			BattleOrder.FrameData frameDataAt2 = this.m_Order.GetFrameDataAt(0);
			frameDataAt2.m_Owner = this.PlayerParty.GetMember(join);
			frameDataAt2.m_OrderValue = 0f;
			this.m_Order.SaveFrames();
			this.UpdateUIPlayerStatus(join, 1, false, false);
			this.m_BattleUI.SetOwnerImage(member2.CharaParam.CharaID, true);
			this.SetTurnOwnerMarker(true);
			this.m_InputData.Reset();
			this.m_BattleUI.StartCommandInput();
			this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
			this.SetupCommandButton(false, true);
			int num = 0;
			this.m_BattleUI.SetCommandButtonSelecting(num);
			this.SelectCommandButton(num, true);
			if (!this.IsWarningWave())
			{
				member2.PlayVoice(eSoundVoiceListDB.voice_401, 1f);
			}
			else
			{
				member2.PlayVoice(eSoundVoiceListDB.voice_401, 1f);
			}
		}

		// Token: 0x0600065F RID: 1631 RVA: 0x0002162C File Offset: 0x0001FA2C
		private bool CalcAutoMemberChange()
		{
			this.m_AutoMemberChangeDatas.Clear();
			int num = 0;
			List<CharacterHandler> benchMembers = this.PlayerParty.GetBenchMembers(true);
			for (int i = 0; i < this.m_DeadPlayerDatas.Count; i++)
			{
				BattleDefine.eJoinMember join = this.m_DeadPlayerDatas[i].m_Join;
				CharacterHandler member = this.PlayerParty.GetMember(join);
				if (!(member == null))
				{
					BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.None;
					if (member.IsFriendChara)
					{
						CharacterHandler member2 = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_Friend);
						if (member2 != null && !member2.CharaBattle.Param.IsDead())
						{
							eJoinMember = member2.CharaBattle.Join;
						}
					}
					if (eJoinMember == BattleDefine.eJoinMember.None)
					{
						for (int j = num; j < benchMembers.Count; j++)
						{
							CharacterHandler characterHandler = benchMembers[j];
							if (characterHandler != null)
							{
								eJoinMember = characterHandler.CharaBattle.Join;
								num++;
								break;
							}
						}
					}
					if (eJoinMember == BattleDefine.eJoinMember.None)
					{
						break;
					}
					BattleSystem.AutoMemberChangeData item;
					item.m_Join = join;
					item.m_Bench = eJoinMember;
					item.m_OrderIndex = this.m_DeadPlayerDatas[i].m_OrderIndex;
					item.m_OrderValue = this.m_DeadPlayerDatas[i].m_OrderValue;
					this.m_AutoMemberChangeDatas.Add(item);
				}
			}
			this.m_DeadPlayerDatas.Clear();
			return this.m_AutoMemberChangeDatas.Count > 0;
		}

		// Token: 0x06000660 RID: 1632 RVA: 0x000217C0 File Offset: 0x0001FBC0
		private bool ChangePlayerJoinMember_GotoJoinOnPlayerDead()
		{
			bool flag = true;
			for (int i = 0; i < this.m_AutoMemberChangeDatas.Count; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember(this.m_AutoMemberChangeDatas[i].m_Bench);
				if (member != null && !member.IsDonePreparePerfect())
				{
					member.PrepareWithoutResourceSetup(false);
					flag = false;
				}
			}
			if (flag)
			{
				for (int j = 0; j < this.m_AutoMemberChangeDatas.Count; j++)
				{
					CharacterHandler member2 = this.PlayerParty.GetMember(this.m_AutoMemberChangeDatas[j].m_Bench);
					if (member2 != null)
					{
						this.ChangePlayerJoinMember_GotoJoinMove(member2, (int)this.m_AutoMemberChangeDatas[j].m_Join);
					}
				}
			}
			return flag;
		}

		// Token: 0x06000661 RID: 1633 RVA: 0x000218A0 File Offset: 0x0001FCA0
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnPlayerDead()
		{
			for (int i = 0; i < this.m_AutoMemberChangeDatas.Count; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember(this.m_AutoMemberChangeDatas[i].m_Bench);
				if (member != null && !member.IsDonePreparePerfect())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000662 RID: 1634 RVA: 0x00021904 File Offset: 0x0001FD04
		private void ChangePlayerDeadMember()
		{
			try
			{
				for (int i = 0; i < this.m_AutoMemberChangeDatas.Count; i++)
				{
					CharacterHandler member = this.PlayerParty.GetMember(this.m_AutoMemberChangeDatas[i].m_Bench);
					this.PlayerParty.ChangeJoinMember(this.m_AutoMemberChangeDatas[i].m_Join, this.m_AutoMemberChangeDatas[i].m_Bench);
					this.m_Order.AddFrameData(BattleOrder.eFrameType.Chara, member, this.m_AutoMemberChangeDatas[i].m_OrderValue, null);
					this.UpdateUIPlayerStatus(this.m_AutoMemberChangeDatas[i].m_Join, 1, false, false);
				}
				this.m_Order.SortOrder();
			}
			catch (Exception ex)
			{
			}
		}

		// Token: 0x06000663 RID: 1635 RVA: 0x000219EC File Offset: 0x0001FDEC
		private bool InterruptFriendJoinSelect()
		{
			if (this.BSD.FrdUsed)
			{
				return false;
			}
			BattleSystem.eMainStep mainStep = this.m_MainStep;
			if (mainStep != BattleSystem.eMainStep.OrderSlide_Wait && mainStep != BattleSystem.eMainStep.OrderSlide_AfterWait)
			{
				if (mainStep != BattleSystem.eMainStep.CommandInput_Wait)
				{
					this.m_InterruptFriendOccurredSituation = BattleSystem.eInterruptFriendOccurredSituation.None;
					return false;
				}
				this.m_InterruptFriendOccurredSituation = BattleSystem.eInterruptFriendOccurredSituation.CommandInput;
			}
			else
			{
				this.m_InterruptFriendOccurredSituation = BattleSystem.eInterruptFriendOccurredSituation.OrderSliding;
			}
			BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.None;
			for (int i = 0; i < 3; i++)
			{
				BattleDefine.eJoinMember eJoinMember2 = (BattleDefine.eJoinMember)i;
				CharacterHandler member = this.PlayerParty.GetMember(eJoinMember2);
				if (member == null || member.CharaBattle.Param.IsDead())
				{
					eJoinMember = eJoinMember2;
					break;
				}
			}
			this.SetTurnOwnerMarker(false);
			if (eJoinMember != BattleDefine.eJoinMember.None)
			{
				this.m_InputData.InputInterruptFriend_Select(eJoinMember);
				this.ExecInterruptFriend();
				this.m_BattleUI.PlayOutTopUI();
				this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_0);
			}
			else
			{
				this.m_InputData.InputInterruptFriend_Start();
				this.IsEnableTouchChangeTarget = true;
				List<CharacterHandler> joinMembers = this.PlayerParty.GetJoinMembers(true);
				this.UpdateTargetMarkerOnInterruptFriend(joinMembers[0].CharaBattle.Join);
				this.m_BattleUI.OpenInterruptFriendJoinSelect();
				this.SetMainStep(BattleSystem.eMainStep.InterruptFriendJoinSelect_Wait);
			}
			return true;
		}

		// Token: 0x06000664 RID: 1636 RVA: 0x00021B24 File Offset: 0x0001FF24
		private BattleSystem.eMainStep ReturnFromCancelInterruptFriend()
		{
			int num = this.m_InputData.SelectCommandIndex;
			if (num == -1)
			{
				num = 0;
			}
			this.m_InputData.Reset();
			this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
			this.m_BattleUI.AppearOwnerImage();
			BattleSystem.eInterruptFriendOccurredSituation interruptFriendOccurredSituation = this.m_InterruptFriendOccurredSituation;
			if (interruptFriendOccurredSituation == BattleSystem.eInterruptFriendOccurredSituation.OrderSliding)
			{
				BattleOrderIndicator order = this.m_BattleUI.GetOrder();
				order.SetFrames(this.m_Order, 0, BattleOrderIndicator.eMoveType.Immediate);
				this.IsEnableTouchChangeTarget = false;
				this.m_BattleUI.GetFriendButton().SetEnableInput(false);
				return BattleSystem.eMainStep.OrderCheck;
			}
			if (interruptFriendOccurredSituation != BattleSystem.eInterruptFriendOccurredSituation.CommandInput)
			{
				return BattleSystem.eMainStep.None;
			}
			this.m_BattleUI.StartCommandInput();
			this.m_BattleUI.SetCommandButtonSelecting(num);
			this.SelectCommandButton(num, true);
			this.SetTurnOwnerMarker(true);
			return BattleSystem.eMainStep.CommandInput_Wait;
		}

		// Token: 0x06000665 RID: 1637 RVA: 0x00021BF4 File Offset: 0x0001FFF4
		private void UpdateTouchChangeInterruptFriedJoin()
		{
			if (!this.IsEnableTouchChangeTarget)
			{
				return;
			}
			List<CharacterHandler> joinMembers = this.PlayerParty.GetJoinMembers(true);
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			RaycastHit raycastHit;
			if (info.m_bAvailable && info.m_TouchPhase == TouchPhase.Began && Physics.Raycast(this.m_MainCamera.ScreenPointToRay(info.m_CurrentPos), out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
			{
				"Character"
			})))
			{
				CharacterHandler component = raycastHit.transform.gameObject.GetComponent<CharacterHandler>();
				if (component != null)
				{
					if (!component.CharaBattle.MyParty.IsPlayerParty)
					{
						return;
					}
					if (component.CharaBattle.Param.IsDead())
					{
						return;
					}
					this.UpdateTargetMarkerOnInterruptFriend(component.CharaBattle.Join);
				}
			}
		}

		// Token: 0x06000666 RID: 1638 RVA: 0x00021CD4 File Offset: 0x000200D4
		private void ExecInterruptFriend()
		{
			this.BSD.FrdUsed = true;
			this.BSD.FrdUseCount++;
			CharacterHandler friend = this.PlayerParty.GetFriend();
			if (friend.FriendType == CharacterDefine.eFriendType.Registered)
			{
				this.BSD.FrdRemainTurn = (int)BattleUtility.DefVal(eBattleDefineDB.RegistedFriendTurn);
			}
			else
			{
				this.BSD.FrdRemainTurn = (int)BattleUtility.DefVal(eBattleDefineDB.UnregistedFriendTurn);
			}
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
		}

		// Token: 0x06000667 RID: 1639 RVA: 0x00021D50 File Offset: 0x00020150
		private void ChangePlayerJoinMember_GotoBenchOnInterruputFriend()
		{
			CharacterHandler member = this.PlayerParty.GetMember(this.m_InputData.InterruptJoin);
			if (member == null)
			{
				return;
			}
			if (member.CharaBattle.Param.IsDead())
			{
				return;
			}
			this.ChangePlayerJoinMember_GotoBenchMove(member);
		}

		// Token: 0x06000668 RID: 1640 RVA: 0x00021DA0 File Offset: 0x000201A0
		private bool IsCompleteGotoBenchOnInterruputFriend()
		{
			CharacterHandler member = this.PlayerParty.GetMember(this.m_InputData.InterruptJoin);
			return member == null || member.CharaBattle.Param.IsDead() || member.CharaBattle.IsArrivalOutToPos();
		}

		// Token: 0x06000669 RID: 1641 RVA: 0x00021DF4 File Offset: 0x000201F4
		private bool ChangePlayerJoinMember_GotoJoinOnInterruputFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			if (!(friend != null))
			{
				return false;
			}
			if (friend.IsDonePreparePerfect())
			{
				this.ChangePlayerJoinMember_GotoJoinMove(friend, (int)this.m_InputData.InterruptJoin);
				return true;
			}
			friend.PrepareWithoutResourceSetup(false);
			return false;
		}

		// Token: 0x0600066A RID: 1642 RVA: 0x00021E44 File Offset: 0x00020244
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnInterruputFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			return !(friend != null) || friend.IsDonePreparePerfect();
		}

		// Token: 0x0600066B RID: 1643 RVA: 0x00021E7C File Offset: 0x0002027C
		private bool IsCompleteGotoJoinOnInterruputFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			return friend == null || !friend.CharaBattle.IsMoveTo;
		}

		// Token: 0x0600066C RID: 1644 RVA: 0x00021EB4 File Offset: 0x000202B4
		private void ChangePlayerJoinMemberOnInterruputFriend()
		{
			BattleDefine.eJoinMember interruptJoin = this.m_InputData.InterruptJoin;
			BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Bench_Friend;
			CharacterHandler member = this.PlayerParty.GetMember(interruptJoin);
			CharacterHandler member2 = this.PlayerParty.GetMember(eJoinMember);
			if (member != null)
			{
				member.SetEnableRender(false);
			}
			this.PlayerParty.ChangeJoinMember(interruptJoin, eJoinMember);
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			this.m_Order.RemoveFrameData(BattleOrder.eFrameType.Chara, owner, false);
			this.m_Order.RemoveFrameData(BattleOrder.eFrameType.Chara, member, false);
			this.m_Order.InsertFrameData(0, BattleOrder.eFrameType.Chara, member2, 0f, null);
			if (owner != member)
			{
				this.m_Order.InsertFrameData(1, BattleOrder.eFrameType.Chara, owner, 0f, null);
			}
			this.m_Order.SaveFrames();
			this.UpdateUIPlayerStatus(interruptJoin, 1, false, false);
			this.SetTurnOwnerMarker(true);
			this.m_InputData.Reset();
			this.IsEnableTouchChangeTarget = true;
			this.m_BattleUI.StartCommandInput();
			this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
			this.SetupCommandButton(false, true);
			int num = 0;
			this.m_BattleUI.SetCommandButtonSelecting(num);
			this.SelectCommandButton(num, true);
			this.m_BattleUI.PlayInTopUI();
			if (!this.IsWarningWave())
			{
				member2.PlayVoice(eSoundVoiceListDB.voice_401, 1f);
			}
			else
			{
				member2.PlayVoice(eSoundVoiceListDB.voice_401, 1f);
			}
		}

		// Token: 0x0600066D RID: 1645 RVA: 0x00022028 File Offset: 0x00020428
		private bool CalcGoBackFriend()
		{
			if (this.m_IsGoBackFriendCheck)
			{
				return false;
			}
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			if (!frameDataAt.IsCharaType)
			{
				return false;
			}
			if (!frameDataAt.m_Owner.IsFriendChara)
			{
				return false;
			}
			if (this.CheckClearAllWave())
			{
				return false;
			}
			this.m_IsGoBackFriendCheck = true;
			this.BSD.FrdRemainTurn--;
			return this.BSD.FrdRemainTurn <= 0;
		}

		// Token: 0x0600066E RID: 1646 RVA: 0x000220AC File Offset: 0x000204AC
		private void ChangePlayerJoinMember_GotoBenchOnGoBackFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			if (friend == null)
			{
				return;
			}
			if (friend.CharaBattle.Param.IsDead())
			{
				return;
			}
			this.ChangePlayerJoinMember_GotoBenchMove(friend);
		}

		// Token: 0x0600066F RID: 1647 RVA: 0x000220F0 File Offset: 0x000204F0
		private bool IsCompleteGotoBenchOnGoBackFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			return friend == null || friend.CharaBattle.Param.IsDead() || friend.CharaBattle.IsArrivalOutToPos();
		}

		// Token: 0x06000670 RID: 1648 RVA: 0x0002213C File Offset: 0x0002053C
		private bool ChangePlayerJoinMember_GotoJoinOnGoBackFriend()
		{
			CharacterHandler member = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_Friend);
			if (member == null)
			{
				return false;
			}
			if (member.CharaBattle.Param.IsDead())
			{
				return false;
			}
			CharacterHandler friend = this.PlayerParty.GetFriend();
			if (friend == null)
			{
				return false;
			}
			if (member.IsDonePreparePerfect())
			{
				this.ChangePlayerJoinMember_GotoJoinMove(member, (int)friend.CharaBattle.Join);
				return true;
			}
			member.PrepareWithoutResourceSetup(false);
			return false;
		}

		// Token: 0x06000671 RID: 1649 RVA: 0x000221BC File Offset: 0x000205BC
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnGoBackFriend()
		{
			CharacterHandler member = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_Friend);
			return !(member != null) || member.IsDonePreparePerfect();
		}

		// Token: 0x06000672 RID: 1650 RVA: 0x000221F4 File Offset: 0x000205F4
		private bool IsCompleteGotoJoinOnGoBackFriend()
		{
			CharacterHandler member = this.PlayerParty.GetMember(BattleDefine.eJoinMember.Bench_Friend);
			return member == null || !member.CharaBattle.IsMoveTo;
		}

		// Token: 0x06000673 RID: 1651 RVA: 0x0002222C File Offset: 0x0002062C
		private void ChangePlayerJoinMemberOnGoBackFriend()
		{
			CharacterHandler friend = this.PlayerParty.GetFriend();
			BattleDefine.eJoinMember join = friend.CharaBattle.Join;
			BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Bench_Friend;
			CharacterHandler member = this.PlayerParty.GetMember(join);
			CharacterHandler member2 = this.PlayerParty.GetMember(eJoinMember);
			if (member != null)
			{
				member.SetEnableRender(false);
			}
			this.PlayerParty.ChangeJoinMember(join, eJoinMember);
			int commandButtonSelecting = 0;
			this.m_BattleUI.SetCommandButtonSelecting(commandButtonSelecting);
			if (member2 != null && !member2.CharaBattle.Param.IsDead())
			{
				int frameNum = this.m_Order.GetFrameNum();
				for (int i = 0; i < frameNum; i++)
				{
					BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(i);
					if (frameDataAt.CompareOwnerCharaOnly(member))
					{
						frameDataAt.m_Owner = member2;
					}
				}
				this.UpdateUIPlayerStatus(join, 1, false, false);
				BattleOrderIndicator order = this.m_BattleUI.GetOrder();
				order.SetEnableRender(true);
				order.SetFrames(this.m_Order, order.SelectingIndex, BattleOrderIndicator.eMoveType.Immediate);
				this.m_BattleUI.GetDescription().SetText(string.Empty);
				this.m_BattleUI.SetOwnerImage(-1, true);
			}
			else
			{
				for (int j = this.m_Order.GetFrameNum() - 1; j >= 0; j--)
				{
					BattleOrder.FrameData frameDataAt2 = this.m_Order.GetFrameDataAt(j);
					if (frameDataAt2.CompareOwnerCharaOnly(member) && j != 0)
					{
						this.m_Order.RemoveFrameDataAt(j);
					}
				}
				this.UpdateUIPlayerStatus(join, 0, false, false);
				BattleOrderIndicator order2 = this.m_BattleUI.GetOrder();
				order2.SetEnableRender(true);
				order2.SetFrames(this.m_Order, 0, BattleOrderIndicator.eMoveType.Slide);
				this.m_BattleUI.GetDescription().SetText(string.Empty);
				this.m_BattleUI.SetOwnerImage(-1, true);
			}
		}

		// Token: 0x06000674 RID: 1652 RVA: 0x00022408 File Offset: 0x00020808
		private void UpdateTouchChangeTargetIndex()
		{
			if (!this.IsEnableTouchChangeTarget)
			{
				return;
			}
			if (this.m_InputData.SelectCommandIndex == -1)
			{
				return;
			}
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			RaycastHit raycastHit;
			if (info.m_bAvailable && info.m_TouchPhase == TouchPhase.Began && Physics.Raycast(this.m_MainCamera.ScreenPointToRay(info.m_CurrentPos), out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
			{
				"Character"
			})))
			{
				CharacterHandler component = raycastHit.transform.gameObject.GetComponent<CharacterHandler>();
				if (component != null && !component.CharaBattle.Param.IsDead())
				{
					BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
					if (!frameDataAt.IsCharaType)
					{
						return;
					}
					CharacterHandler owner = frameDataAt.m_Owner;
					if (!owner.CharaBattle.MyParty.IsPlayerParty)
					{
						return;
					}
					BattleCommandData commandDataAt = owner.CharaBattle.GetCommandDataAt(this.m_InputData.SelectCommandIndex);
					bool flag = false;
					eSkillTargetType mainSkillTargetType = commandDataAt.MainSkillTargetType;
					if (mainSkillTargetType != eSkillTargetType.TgtSingle)
					{
						if (mainSkillTargetType == eSkillTargetType.MySingle)
						{
							if (component.CharaBattle.MyParty == owner.CharaBattle.MyParty)
							{
								this.UpdateTargetMarker(component.CharaBattle.Join, false);
								flag = true;
							}
						}
					}
					else if (component.CharaBattle.MyParty != owner.CharaBattle.MyParty)
					{
						BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
						if (seq == BattleTutorial.eSeq._05_WAVE_IDX_0_TARGET)
						{
							if (this.m_Tutorial.GetSingleTargetIndex() != component.CharaBattle.Join)
							{
								goto IL_2BB;
							}
							for (BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Join_1; eJoinMember <= BattleDefine.eJoinMember.Join_3; eJoinMember++)
							{
								CharacterHandler member = this.EnemyParty.GetMember(eJoinMember);
								if (member != null)
								{
									member.CharaBattle.ResetMeshColor();
								}
								member = this.PlayerParty.GetMember(eJoinMember);
								if (member != null)
								{
									member.CharaBattle.ResetMeshColor();
								}
							}
							this.m_BattleBG.ResetColor();
							this.m_BattleUI.SetEnableInput(true);
							TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
							CommandButton commandButton = this.m_BattleUI.GetCommandButton(0);
							if (commandButton != null)
							{
								tutorialMessage.Open(eTutorialMessageListDB.BTL_ATTACK_AFTER_ENEMY_TAP);
								tutorialMessage.SetEnableArrow(true);
								tutorialMessage.SetArrowPosition(commandButton.CacheTransform.position);
								tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
								UIUtility.AttachCanvasForChangeRenderOrder(commandButton.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
							}
							this.m_Tutorial.ApplyNextSeq();
						}
						this.UpdateTargetMarker(component.CharaBattle.Join, false);
						flag = true;
					}
					IL_2BB:
					if (flag)
					{
						int selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, this.m_InputData.SelectCommandIndex, false);
						BattleOrderIndicator order = this.m_BattleUI.GetOrder();
						order.SetFrames(this.m_Order, selectingIndex, BattleOrderIndicator.eMoveType.Selecting);
					}
					bool flag2 = commandDataAt.MainSkillTargetType == eSkillTargetType.TgtSingle || commandDataAt.MainSkillTargetType == eSkillTargetType.TgtAll;
					BattleTutorial.eSeq seq2 = this.m_Tutorial.GetSeq();
					if (seq2 == BattleTutorial.eSeq._05_WAVE_IDX_0_TARGET)
					{
						flag2 = false;
					}
					if (flag2)
					{
						component.CharaBattle.PlayCharaTouchVoice();
					}
				}
			}
		}

		// Token: 0x06000675 RID: 1653 RVA: 0x00022754 File Offset: 0x00020B54
		private void UpdateTouchChangeTargetIndexOnTogetherAttack()
		{
			if (!this.IsEnableTouchChangeTarget)
			{
				return;
			}
			List<BattleDefine.eJoinMember> list = this.m_Order.CalcTogetherAttackDefaultOrders();
			if (list.Count <= 0)
			{
				return;
			}
			List<CharacterHandler> members = this.PlayerParty.GetMembers(list);
			bool flag = false;
			for (int i = 0; i < members.Count; i++)
			{
				BattleCommandData uniqueSkillCommandData = members[i].CharaBattle.GetUniqueSkillCommandData();
				eSkillTargetType mainSkillTargetType = uniqueSkillCommandData.MainSkillTargetType;
				if (mainSkillTargetType == eSkillTargetType.TgtSingle)
				{
					flag = true;
				}
			}
			if (!flag)
			{
				return;
			}
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			RaycastHit raycastHit;
			if (info.m_bAvailable && info.m_TouchPhase == TouchPhase.Began && Physics.Raycast(this.m_MainCamera.ScreenPointToRay(info.m_CurrentPos), out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
			{
				"Character"
			})))
			{
				CharacterHandler component = raycastHit.transform.gameObject.GetComponent<CharacterHandler>();
				if (component != null && !component.CharaBattle.Param.IsDead())
				{
					BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
					if (!frameDataAt.IsCharaType)
					{
						return;
					}
					CharacterHandler owner = frameDataAt.m_Owner;
					if (!owner.CharaBattle.MyParty.IsPlayerParty)
					{
						return;
					}
					if (component.CharaBattle.MyParty != owner.CharaBattle.MyParty)
					{
						this.UpdateTargetMarker(component.CharaBattle.Join, true);
					}
				}
			}
		}

		// Token: 0x06000676 RID: 1654 RVA: 0x000228E4 File Offset: 0x00020CE4
		private void InitPlacementParty(bool isBackStart = true)
		{
			BattlePosLocator battlePosLocator = this.m_PosLocator_Player;
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
				if (!(member == null))
				{
					float delaySec = 0.2f * (float)i;
					if (i < 3 && member.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
					{
						if (isBackStart)
						{
							member.CacheTransform.localPosition = battlePosLocator.GetBackLocatorPos(i);
						}
						else
						{
							member.CacheTransform.localPosition = battlePosLocator.GetLocatorPos(i);
						}
						member.CacheTransform.SetParent(this.m_ObjRoot, false);
						member.SetEnableRender(true);
						member.CharaBattle.RequestIdleMode(0f, delaySec, false);
					}
					else
					{
						member.CacheTransform.localPosition = new Vector3(0f, 10f, 0f);
						member.CacheTransform.SetParent(this.m_ObjRoot, false);
						member.SetEnableRender(false);
					}
				}
			}
			int joinCharaNum = this.EnemyParty.GetJoinCharaNum(false);
			battlePosLocator = this.m_PosLocator_Enemy[joinCharaNum - 1];
			for (int j = 0; j < 6; j++)
			{
				CharacterHandler member2 = this.EnemyParty.GetMember((BattleDefine.eJoinMember)j);
				if (!(member2 == null))
				{
					float delaySec2 = 0.2f * (float)j;
					if (member2.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
					{
						member2.CacheTransform.localPosition = battlePosLocator.GetLocatorPos(j);
						member2.CacheTransform.SetParent(this.m_ObjRoot, false);
						member2.SetEnableRender(true);
						member2.CharaBattle.RequestIdleMode(0f, delaySec2, false);
					}
					else
					{
						member2.CacheTransform.localPosition = new Vector3(0f, 999f, 0f);
						member2.CacheTransform.SetParent(this.m_ObjRoot, false);
						member2.SetEnableRender(false);
					}
				}
			}
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00022AE0 File Offset: 0x00020EE0
		private void WaveInParty()
		{
			BattlePosLocator posLocator_Player = this.m_PosLocator_Player;
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
				if (!(member == null))
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						float num = 0.15f * (float)i;
						if (i < 3)
						{
							member.SetEnableRender(true);
							member.CharaBattle.RequestInMode(num);
							member.CharaBattle.MoveToPositionTime(posLocator_Player.GetBackLocatorPos(i), posLocator_Player.GetLocatorPos(i), 0.20000002f, num);
						}
						else
						{
							member.CacheTransform.localPosition = new Vector3(0f, 999f, 0f);
							member.SetEnableRender(true);
							member.CharaBattle.RequestIdleMode(0f, num, false);
						}
					}
				}
			}
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00022BC0 File Offset: 0x00020FC0
		private bool IsCompleteWaveInParty()
		{
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					CharacterHandler member = this.m_Parties[i].GetMember((BattleDefine.eJoinMember)j);
					if (member != null && member.CharaBattle.IsMoveTo)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00022C20 File Offset: 0x00021020
		private bool CheckTransitWaveOut()
		{
			for (int i = 0; i < 2; i++)
			{
				BattlePartyData battlePartyData = this.m_Parties[i];
				for (int j = 0; j < 3; j++)
				{
					BattleDefine.eJoinMember join = (BattleDefine.eJoinMember)j;
					CharacterHandler member = battlePartyData.GetMember(join);
					if (member != null && member.CharaBattle.IsPlayingDeadAnim())
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600067A RID: 1658 RVA: 0x00022C88 File Offset: 0x00021088
		private void WaveOutPartyJoinOnlyFromCurrentPos()
		{
			for (int i = 0; i < 3; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
				if (!(member == null))
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						Vector3 locatorPos = this.m_PosLocator_Player.GetLocatorPos((int)member.CharaBattle.Join);
						locatorPos.x -= 3.6f;
						locatorPos.z = member.CacheTransform.localPosition.z;
						float delaySec = 0.15f * (float)i;
						member.SetEnableRender(true);
						member.CharaBattle.RequestOutMode(locatorPos, delaySec, true);
					}
				}
			}
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x00022D44 File Offset: 0x00021144
		private bool IsCompleteWaveOutParty()
		{
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					CharacterHandler member = this.m_Parties[i].GetMember((BattleDefine.eJoinMember)j);
					if (member != null && member.CharaBattle.IsMoveTo)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x00022DA3 File Offset: 0x000211A3
		public void RequestSort()
		{
			this.m_IsRequestSort = true;
		}

		// Token: 0x0600067D RID: 1661 RVA: 0x00022DAC File Offset: 0x000211AC
		private void UpdateSort()
		{
			if (!this.m_IsRequestSort)
			{
				return;
			}
			BattleSystem.eSortMode eSortMode = BattleSystem.eSortMode.Default;
			if (this.m_KiraraJumpScene != null)
			{
				eSortMode = BattleSystem.eSortMode.KiraraJumpScene;
			}
			else if (this.m_UniqueSkillScene != null)
			{
				eSortMode = BattleSystem.eSortMode.UniqueSkillScene;
			}
			if (eSortMode != BattleSystem.eSortMode.Default)
			{
				if (eSortMode != BattleSystem.eSortMode.KiraraJumpScene)
				{
					if (eSortMode == BattleSystem.eSortMode.UniqueSkillScene)
					{
						this.m_SortDatas.Clear();
						List<CharacterHandler> sortTargetCharaHndls = this.m_UniqueSkillScene.GetSortTargetCharaHndls();
						if (sortTargetCharaHndls != null)
						{
							for (int i = 0; i < sortTargetCharaHndls.Count; i++)
							{
								this.m_SortDatas.Add(new BattleSystem.SortData(sortTargetCharaHndls[i].CacheTransform, sortTargetCharaHndls[i], null, null));
							}
						}
						List<BattleUniqueSkillBaseObjectHandler.CharaRender> charaRenderCache = this.m_UniqueSkillScene.GetCharaRenderCache();
						if (charaRenderCache != null)
						{
							for (int j = 0; j < charaRenderCache.Count; j++)
							{
								if (charaRenderCache[j] != null)
								{
									this.m_SortDatas.Add(new BattleSystem.SortData(null, null, null, charaRenderCache[j]));
								}
							}
						}
						List<Renderer> renderCache = this.m_UniqueSkillScene.GetRenderCache();
						if (renderCache != null)
						{
							for (int k = 0; k < renderCache.Count; k++)
							{
								this.m_SortDatas.Add(new BattleSystem.SortData(renderCache[k].transform, null, renderCache[k], null));
							}
						}
						List<BattleSystem.SortData> sortDatas = this.m_SortDatas;
						if (BattleSystem.<>f__mg$cache2 == null)
						{
							BattleSystem.<>f__mg$cache2 = new Comparison<BattleSystem.SortData>(BattleSystem.CompareSortData_UniqueSkillScene);
						}
						Sort.StableSort<BattleSystem.SortData>(sortDatas, BattleSystem.<>f__mg$cache2);
						for (int l = 0; l < this.m_SortDatas.Count; l++)
						{
							int sortingOrder = -l;
							if (this.m_SortDatas[l].m_CharaHndl != null)
							{
								this.m_SortDatas[l].m_CharaHndl.CharaAnim.SetSortingOrder(sortingOrder);
							}
							else if (this.m_SortDatas[l].m_US_CharaRender != null)
							{
								for (int m = 0; m < this.m_SortDatas[l].m_US_CharaRender.m_RenderCache.Count; m++)
								{
									this.m_SortDatas[l].m_US_CharaRender.m_RenderCache[m].sortingOrder = sortingOrder;
								}
							}
							else
							{
								this.m_SortDatas[l].m_Render.sortingOrder = sortingOrder;
							}
						}
						this.m_UniqueSkillScene.SetBaseObjectParticleSortingOrder(1);
					}
				}
				else
				{
					this.m_SortDatas.Clear();
					List<CharacterHandler> sortTargetCharaHndls2 = this.m_KiraraJumpScene.GetSortTargetCharaHndls();
					if (sortTargetCharaHndls2 != null)
					{
						for (int n = 0; n < sortTargetCharaHndls2.Count; n++)
						{
							this.m_SortDatas.Add(new BattleSystem.SortData(sortTargetCharaHndls2[n].CacheTransform, sortTargetCharaHndls2[n], null, null));
						}
					}
					Renderer[] baseObjectRenderers = this.m_KiraraJumpScene.GetBaseObjectRenderers();
					if (baseObjectRenderers != null)
					{
						for (int num = 0; num < baseObjectRenderers.Length; num++)
						{
							this.m_SortDatas.Add(new BattleSystem.SortData(baseObjectRenderers[num].transform, null, baseObjectRenderers[num], null));
						}
					}
					List<BattleSystem.SortData> sortDatas2 = this.m_SortDatas;
					if (BattleSystem.<>f__mg$cache1 == null)
					{
						BattleSystem.<>f__mg$cache1 = new Comparison<BattleSystem.SortData>(BattleSystem.CompareSortData_KiraraJumpScene);
					}
					Sort.StableSort<BattleSystem.SortData>(sortDatas2, BattleSystem.<>f__mg$cache1);
					for (int num2 = 0; num2 < this.m_SortDatas.Count; num2++)
					{
						int sortingOrder2 = -num2;
						if (this.m_SortDatas[num2].m_CharaHndl != null)
						{
							this.m_SortDatas[num2].m_CharaHndl.CharaAnim.SetSortingOrder(sortingOrder2);
						}
						else
						{
							this.m_SortDatas[num2].m_Render.sortingOrder = sortingOrder2;
						}
					}
					this.m_KiraraJumpScene.SetBaseObjectSortingOrder(1);
				}
			}
			else
			{
				this.m_SortDatas.Clear();
				for (int num3 = 0; num3 < 2; num3++)
				{
					for (int num4 = 0; num4 < 6; num4++)
					{
						CharacterHandler member = this.m_Parties[num3].GetMember((BattleDefine.eJoinMember)num4);
						if (member != null)
						{
							this.m_SortDatas.Add(new BattleSystem.SortData(member.CacheTransform, member, null, null));
						}
					}
				}
				List<BattleSystem.SortData> sortDatas3 = this.m_SortDatas;
				if (BattleSystem.<>f__mg$cache0 == null)
				{
					BattleSystem.<>f__mg$cache0 = new Comparison<BattleSystem.SortData>(BattleSystem.CompareSortData_CharaOnly);
				}
				Sort.StableSort<BattleSystem.SortData>(sortDatas3, BattleSystem.<>f__mg$cache0);
				for (int num5 = 0; num5 < this.m_SortDatas.Count; num5++)
				{
					if (this.m_SortDatas[num5].m_CharaHndl != null)
					{
						this.m_SortDatas[num5].m_CharaHndl.CharaAnim.SetSortingOrder(-(num5 * 10));
						this.m_SortDatas[num5].m_CharaHndl.CacheTransform.positionZ((float)(num5 + 1) * 6f);
					}
				}
			}
			this.m_IsRequestSort = false;
		}

		// Token: 0x0600067E RID: 1662 RVA: 0x000232C8 File Offset: 0x000216C8
		private static int CompareSortData_CharaOnly(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			if (A.m_Transform.position.y > B.m_Transform.position.y)
			{
				return 1;
			}
			if (A.m_Transform.position.y < B.m_Transform.position.y)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x0600067F RID: 1663 RVA: 0x00023330 File Offset: 0x00021730
		private static int CompareSortData_KiraraJumpScene(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			if (A.m_Transform.position.z < B.m_Transform.position.z)
			{
				return 1;
			}
			if (A.m_Transform.position.z > B.m_Transform.position.z)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06000680 RID: 1664 RVA: 0x00023398 File Offset: 0x00021798
		private static int CompareSortData_UniqueSkillScene(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			try
			{
				if (A.m_Transform.position.z < B.m_Transform.position.z)
				{
					return 1;
				}
			}
			catch
			{
				UnityEngine.Debug.Log(string.Empty);
			}
			if (A.m_Transform.position.z > B.m_Transform.position.z)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x00023430 File Offset: 0x00021830
		public void KiraraJumpScenePreProcess()
		{
			this.m_BattleBG.SetEnableRender(false);
			this.m_BattleCamera.Pause();
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x00023449 File Offset: 0x00021849
		public void RevertKiraraJumpSceneToSystem()
		{
			this.m_BattleBG.SetEnableRender(true);
			this.m_BattleCamera.Resume();
			this.InitPlacementParty(false);
		}

		// Token: 0x06000683 RID: 1667 RVA: 0x0002346C File Offset: 0x0002186C
		public void RequestKiraraJumpVoice()
		{
			List<BattleDefine.eJoinMember> joinOrder = this.m_Order.GetTogetherData().GetJoinOrder();
			if (joinOrder != null)
			{
				int index = UnityEngine.Random.Range(0, joinOrder.Count);
				CharacterHandler member = this.PlayerParty.GetMember(joinOrder[index]);
				if (member != null)
				{
					member.PlayVoice(eSoundVoiceListDB.voice_412, 1f);
				}
			}
		}

		// Token: 0x06000684 RID: 1668 RVA: 0x000234CC File Offset: 0x000218CC
		public void UniqueSkillScenePreProcess()
		{
			this.m_BattleBG.SetEnableRender(false);
			this.m_BattleCamera.Pause();
		}

		// Token: 0x06000685 RID: 1669 RVA: 0x000234E5 File Offset: 0x000218E5
		public void RevertUniqueSkillSceneToSystem()
		{
			this.m_BattleBG.SetEnableRender(true);
			this.m_BattleCamera.Resume();
			this.InitPlacementParty(false);
		}

		// Token: 0x06000686 RID: 1670 RVA: 0x00023508 File Offset: 0x00021908
		private void ExecRetry()
		{
			this.PlayerParty.ResetOnRevival();
			for (int i = 0; i < 6; i++)
			{
				this.PlayerParty.SetJoinMemberIndex((BattleDefine.eJoinMember)i, i);
			}
			this.m_Order.Reset();
			for (int j = 0; j < 2; j++)
			{
				for (int k = 0; k < 3; k++)
				{
					CharacterHandler member = this.m_Parties[j].GetMember((BattleDefine.eJoinMember)k);
					if (!(member == null))
					{
						if (!member.CharaBattle.Param.IsDead())
						{
							float orderValue = BattleCommandParser.CalcOrderValue(member.CharaBattle.Param.FixedSpd(), 1f, 1f);
							this.m_Order.AddFrameData(BattleOrder.eFrameType.Chara, member, orderValue, null);
						}
					}
				}
			}
			this.m_Order.SortOrder();
			bool flag = this.m_Order.CanBeTogehterAttack();
			this.m_Order.GetTogetherData().SetupOnTurnStart(flag);
			this.m_Order.SetTogetherAttackExecutable(flag);
			this.m_InputData.Reset();
			this.ClearExecCommand();
			this.BSD.MstSkillFlg = false;
			this.BSD.FrdUsed = false;
			this.m_ExecutedMemberChange = false;
			this.m_Option.SetIsAuto(false);
			this.m_BattleUI.SetAutoFlag(this.m_Option.GetIsAuto());
			this.m_BattleUI.PlayIn();
			this.m_BattleCamera.ToDefault(0f);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
			for (int l = 0; l < 6; l++)
			{
				CharacterHandler member2 = this.PlayerParty.GetMember((BattleDefine.eJoinMember)l);
				if (member2 != null && !member2.IsDonePreparePerfect())
				{
					member2.PrepareWithoutResourceSetup(false);
				}
			}
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x000236D0 File Offset: 0x00021AD0
		private bool IsDonePrepareOnRetry()
		{
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
				if (member != null && !member.IsDonePreparePerfect())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00023718 File Offset: 0x00021B18
		public void CalcResultPreRequest()
		{
			QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(this.m_QuestParam.m_ID);
			if (questData == null)
			{
				return;
			}
			this.m_Result.m_QuestID = questData.m_Param.m_ID;
			this.m_Result.m_InterruptFriendUseNum = this.BSD.FrdUseCount;
			this.m_Result.m_MasterSkillUseNum = this.BSD.MstSkillCount;
			if (this.m_Result.m_Status == BattleResult.eStatus.Clear)
			{
				List<BattleOccurEnemy.OneWave> waves = this.BSD.Enemies.GetWaves();
				for (int i = 0; i < waves.Count; i++)
				{
					BattleOccurEnemy.OneWave oneWave = waves[i];
					for (int j = 0; j < oneWave.m_Enemies.Count; j++)
					{
						if (oneWave.m_Enemies[j].m_EnemyID != -1)
						{
							this.m_Result.AddKilledEnemy(oneWave.m_Enemies[j].m_EnemyID);
						}
					}
				}
			}
			this.m_Result.m_IsFirstClear = false;
			if (questData != null && questData.m_ClearRank == QuestDefine.eClearRank.None)
			{
				this.m_Result.m_IsFirstClear = true;
			}
			this.m_Result.m_DeadCount = 0;
			for (int k = 0; k < 6; k++)
			{
				BattleDefine.eJoinMember join = (BattleDefine.eJoinMember)k;
				CharacterHandler member = this.PlayerParty.GetMember(join);
				if (member != null && member.CharaBattle.Param.IsDead())
				{
					this.m_Result.m_DeadCount++;
				}
			}
			this.m_Result.m_ContinueCount = this.BSD.ContCount;
			if (this.m_QuestParam.m_IsLoserBattle == 1)
			{
				this.m_Result.m_ClearRank = QuestDefine.eClearRank.StarMax;
			}
			else if (this.BSD.ContCount > 0)
			{
				this.m_Result.m_ClearRank = QuestDefine.eClearRank.Star1;
			}
			else if (this.m_Result.m_DeadCount == 0)
			{
				this.m_Result.m_ClearRank = QuestDefine.eClearRank.StarMax;
			}
			else if (this.m_Result.m_DeadCount == 1)
			{
				this.m_Result.m_ClearRank = QuestDefine.eClearRank.Star2;
			}
			else
			{
				this.m_Result.m_ClearRank = QuestDefine.eClearRank.Star1;
			}
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x0002395C File Offset: 0x00021D5C
		private BattleDropItem JudgeDropItem(int waveIndex, BattleDefine.eJoinMember join)
		{
			BattleDropItem[] items = this.BSD.ScheduleItems[waveIndex].Items;
			if (items[(int)join].IsDrop())
			{
				return items[(int)join];
			}
			return null;
		}

		// Token: 0x0600068A RID: 1674 RVA: 0x00023994 File Offset: 0x00021D94
		private void RequestSaveOnWaveStart()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_SaveBSD(this.BSD, 3);
		}

		// Token: 0x0600068B RID: 1675 RVA: 0x000239AD File Offset: 0x00021DAD
		private void RequestSaveIfFailed()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_SaveBSD(null, -1);
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600068C RID: 1676 RVA: 0x000239C1 File Offset: 0x00021DC1
		public BattleUI BattleUI
		{
			get
			{
				return this.m_BattleUI;
			}
		}

		// Token: 0x0600068D RID: 1677 RVA: 0x000239CC File Offset: 0x00021DCC
		private void PrecedeInitializeBattleUI()
		{
			if (this.m_BattleUI == null)
			{
				this.m_BattleUI = UIUtility.GetMenuComponent<BattleUI>("Battle_BattleUI");
			}
			if (this.m_BattleUI != null)
			{
				this.m_BattleUI.PrepareCharaResource(this.PlayerParty.GetAvailableCharaIDs());
			}
		}

		// Token: 0x0600068E RID: 1678 RVA: 0x00023A24 File Offset: 0x00021E24
		private bool InitializeBattleUI()
		{
			if (this.m_BattleUI == null)
			{
				this.m_BattleUI = UIUtility.GetMenuComponent<BattleUI>("Battle_BattleUI");
				if (this.m_BattleUI == null)
				{
					return false;
				}
			}
			this.m_BattleUI.Setup(this, this.m_InstaniatedFriendCharaID, this.m_MasterOrbData.OrbID);
			this.m_BattleUI.OnMemberChange += this.OnMemberChange;
			this.m_BattleUI.OnSelectMemberChange += this.OnSelectMemberChange;
			this.m_BattleUI.OnCancelMemberChange += this.OnCancelMemberChange;
			this.m_BattleUI.OnSelectMasterSkill += this.OnSelectMasterSkill;
			this.m_BattleUI.OnClickTogetherButton += this.OnClickTogetherButton;
			this.m_BattleUI.OnDecideTogetherMember += this.OnDecideTogetherMember;
			this.m_BattleUI.OnDecideTogether += this.OnDecideTogether;
			this.m_BattleUI.OnCancelTogether += this.OnCancelTogether;
			this.m_BattleUI.OnChangeCommandButton += this.OnChangeCommandButton;
			this.m_BattleUI.OnClickCommandButton += this.OnClickCommandButton;
			this.m_BattleUI.OnClickFriendButton += this.OnClickFriendButton;
			this.m_BattleUI.OnJudgeInterruptFriendJoinSelect += this.OnJudgeInterruptFriendJoinSelect;
			this.m_BattleUI.OnDecideInterruptFriendJoinSelect += this.OnDecideInterruptFriendJoinSelect;
			this.m_BattleUI.OnCancelInterruptFriendJoinSelect += this.OnCancelInterruptFriendJoinSelect;
			this.m_BattleUI.OnClickAutoButton += this.OnClickAutoButton;
			this.m_BattleUI.OnClickMenuButton += this.OnClickMenuButton;
			this.m_BattleUI.OnCancelBattleMenuWindow += this.OnCancelBattleMenuWindow;
			this.m_BattleUI.OnClickRetireButton += this.OnClickRetireButton;
			this.m_BattleUI.SetAutoFlag(this.m_Option.GetIsAuto());
			this.m_BattleUI.SetEnableInput(false);
			return true;
		}

		// Token: 0x0600068F RID: 1679 RVA: 0x00023C44 File Offset: 0x00022044
		private void SetupBattleUIOnWaveStart()
		{
			if (this.m_Result.m_DropItems != null)
			{
				for (int i = 0; i < this.m_Result.m_DropItems.Count; i++)
				{
					this.m_BattleUI.GetReward().SetRewardNum(this.m_Result.m_DropItems[i].GetTreasureBoxIndex(), this.m_Result.m_DropItems.Count, false);
				}
			}
			this.m_BattleUI.SetWave(this.BSD.WaveIdx, this.BSD.WaveNum);
			this.SetTurnOwnerMarker(false);
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			int commandButtonNum = this.m_BattleUI.GetCommandButtonNum();
			for (int j = 0; j < commandButtonNum; j++)
			{
				this.m_BattleUI.GetCommandButton(j).Hide();
			}
			this.m_BattleUI.GetTogetherButton().Apply(this.BSD.Gauge.GetValue(), true);
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SetFrames(this.m_Order, 0, BattleOrderIndicator.eMoveType.Immediate);
		}

		// Token: 0x06000690 RID: 1680 RVA: 0x00023D58 File Offset: 0x00022158
		private void SetupBattleUIOnTurnStart(CharacterHandler topCharaHndl)
		{
			if (topCharaHndl.CharaBattle.MyParty.IsPlayerParty)
			{
				this.SetTurnOwnerMarker(true);
				TogetherButton togetherButton = this.m_BattleUI.GetTogetherButton();
				togetherButton.Apply(this.m_Order.GetGauge().GetValue(), false);
				togetherButton.SetExecutableTogetherAttack(this.m_Order.GetTogetherData().Executable());
				if (!this.m_IsAfterOrderSlide)
				{
					this.SetupCommandButton(true, true);
				}
			}
			else if (!this.m_IsAfterOrderSlide)
			{
				this.m_BattleUI.SetCommandSetBlank();
			}
			this.UpdateUIStun(topCharaHndl, true);
			this.UpdateTargetMarker(BattleDefine.eJoinMember.None, false);
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SetEnableRender(true);
			this.m_BattleUI.PlayInTopUI();
		}

		// Token: 0x06000691 RID: 1681 RVA: 0x00023E16 File Offset: 0x00022216
		private void CloseBattleUIOnPlayerTurn()
		{
			this.SetTurnOwnerMarker(false);
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
		}

		// Token: 0x06000692 RID: 1682 RVA: 0x00023E2C File Offset: 0x0002222C
		public void UpdateAllCharacterStatus(int enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
			for (int i = 0; i < 2; i++)
			{
				BattlePartyData battlePartyData = this.m_Parties[i];
				for (int j = 0; j < 3; j++)
				{
					CharacterHandler member = battlePartyData.GetMember((BattleDefine.eJoinMember)j);
					int num = enableRenderChange;
					if (num == 1 && member != null && member.CharaBattle.Param.IsDead())
					{
						num = 0;
					}
					this.UpdateUICharaStatus(battlePartyData.PartyType, (BattleDefine.eJoinMember)j, num, isMoveHpBar, isUpdateIconOnly);
				}
			}
		}

		// Token: 0x06000693 RID: 1683 RVA: 0x00023EAE File Offset: 0x000222AE
		public void UpdateUICharaStatus(BattleDefine.ePartyType partyType, BattleDefine.eJoinMember join, int enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
			if (partyType == BattleDefine.ePartyType.Player)
			{
				this.UpdateUIPlayerStatus(join, enableRenderChange, isMoveHpBar, isUpdateIconOnly);
			}
			else
			{
				this.UpdateUIEnemyStatus(join, enableRenderChange, isMoveHpBar, isUpdateIconOnly);
			}
		}

		// Token: 0x06000694 RID: 1684 RVA: 0x00023ED4 File Offset: 0x000222D4
		private void UpdateUIPlayerStatus(BattleDefine.eJoinMember join, int enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
			CharacterStatus playerStatus = this.m_BattleUI.GetPlayerStatus(join);
			if (playerStatus != null)
			{
				CharacterHandler member = this.PlayerParty.GetMember(join);
				if (member != null)
				{
					if (!isUpdateIconOnly)
					{
						playerStatus.SetHp(member.CharaBattle.Param.GetHp(), member.CharaBattle.Param.FixedMaxHp(), isMoveHpBar);
					}
					playerStatus.SetStun(member.CharaBattle.GetStunRatio(), isMoveHpBar);
					playerStatus.SetElementType(member.CharaParam.ElementType);
					playerStatus.SetIcons(member.CharaBattle.GetFlagForUIStatusIcon());
					if (enableRenderChange != 2)
					{
						playerStatus.SetEnableRender(enableRenderChange == 1, false);
					}
					this.m_BattleUI.SetPlayerStatusPosition((int)join, member.CacheTransform.position);
				}
				else
				{
					playerStatus.SetEnableRender(false, false);
				}
			}
		}

		// Token: 0x06000695 RID: 1685 RVA: 0x00023FAC File Offset: 0x000223AC
		private void UpdateUIEnemyStatus(BattleDefine.eJoinMember join, int enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
			CharacterStatus enemyStatus = this.m_BattleUI.GetEnemyStatus(join);
			if (enemyStatus != null)
			{
				CharacterHandler member = this.EnemyParty.GetMember(join);
				if (member != null)
				{
					if (!isUpdateIconOnly)
					{
						enemyStatus.SetHp(member.CharaBattle.Param.GetHp(), member.CharaBattle.Param.FixedMaxHp(), isMoveHpBar);
					}
					enemyStatus.SetStun(member.CharaBattle.GetStunRatio(), isMoveHpBar);
					enemyStatus.SetElementType(member.CharaParam.ElementType);
					enemyStatus.SetIcons(member.CharaBattle.GetFlagForUIStatusIcon());
					enemyStatus.SetCharge(member.CharaBattle.Param.GetChargeCount(), member.CharaBattle.Param.GetChargeCountMax());
					if (enableRenderChange != 2)
					{
						enemyStatus.SetEnableRender(enableRenderChange == 1, false);
					}
					this.m_BattleUI.SetEnemyStatusPosition((int)join, member.CacheTransform.position);
				}
				else
				{
					enemyStatus.SetEnableRender(false, false);
				}
			}
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x000240AC File Offset: 0x000224AC
		public void UpdateUIStun(CharacterHandler charaHndl, bool isMoveBar)
		{
			CharacterStatus characterStatus;
			if (charaHndl.CharaBattle.MyParty.PartyType == BattleDefine.ePartyType.Player)
			{
				characterStatus = this.m_BattleUI.GetPlayerStatus(charaHndl.CharaBattle.Join);
			}
			else
			{
				characterStatus = this.m_BattleUI.GetEnemyStatus(charaHndl.CharaBattle.Join);
			}
			if (characterStatus != null)
			{
				characterStatus.SetStun(charaHndl.CharaBattle.GetStunRatio(), isMoveBar);
			}
		}

		// Token: 0x06000697 RID: 1687 RVA: 0x00024124 File Offset: 0x00022524
		private void SetTurnOwnerMarker(bool flg)
		{
			Vector3 worldPoint = Vector3.zero;
			if (flg)
			{
				CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
				if (frameOwnerAt != null)
				{
					worldPoint = frameOwnerAt.CacheTransform.position + this.uiTurnOwnerMarkerPosOffset * frameOwnerAt.CacheTransform.lossyScale.x;
				}
			}
			this.m_BattleUI.SetTurnOwnerMarker(flg, worldPoint);
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x00024194 File Offset: 0x00022594
		private void UpdateTargetMarker(BattleDefine.eJoinMember join, bool isUseUniqueSkill = false)
		{
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			if (frameOwnerAt == null)
			{
				return;
			}
			if (join != BattleDefine.eJoinMember.None)
			{
				frameOwnerAt.CharaBattle.MyParty.SingleTargetIndex = join;
			}
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			BattleCommandData battleCommandData;
			if (!isUseUniqueSkill)
			{
				if (this.m_InputData.SelectCommandIndex == -1)
				{
					return;
				}
				battleCommandData = frameOwnerAt.CharaBattle.GetCommandDataAt(this.m_InputData.SelectCommandIndex);
			}
			else
			{
				battleCommandData = frameOwnerAt.CharaBattle.GetUniqueSkillCommandData();
			}
			if (battleCommandData != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_SELECT, 1f, 0, -1, -1);
				switch (battleCommandData.MainSkillTargetType)
				{
				case eSkillTargetType.Self:
				{
					CharacterHandler characterHandler = frameOwnerAt;
					Transform refTransform;
					float y;
					characterHandler.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform, out y);
					TargetMarker.eMode mode = TargetMarker.eMode.Normal;
					this.m_BattleUI.SetTargetMarker(0, true, refTransform, new Vector3(0f, y, 0f), mode);
					this.m_BattleUI.ClearTargetMarkerOrderIcon();
					this.m_BattleUI.SetTargetMarkerToOrderIcon(characterHandler.CharaBattle.MyParty.PartyType, characterHandler.CharaBattle.Join, true);
					break;
				}
				case eSkillTargetType.TgtSingle:
				{
					CharacterHandler member = frameOwnerAt.CharaBattle.TgtParty.GetMember(frameOwnerAt.CharaBattle.MyParty.SingleTargetIndex);
					Transform refTransform2;
					float y2;
					member.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform2, out y2);
					TargetMarker.eMode mode2 = TargetMarker.eMode.Normal;
					int num = BattleCommandParser.JudgeElementCompatibility(battleCommandData.GetAttackSkillElementTypes(), member.CharaParam.ElementType);
					if (num > 0)
					{
						mode2 = TargetMarker.eMode.Weak;
					}
					else if (num < 0)
					{
						mode2 = TargetMarker.eMode.Resist;
					}
					this.m_BattleUI.SetTargetMarker(0, true, refTransform2, new Vector3(0f, y2, 0f), mode2);
					this.m_BattleUI.ClearTargetMarkerOrderIcon();
					this.m_BattleUI.SetTargetMarkerToOrderIcon(member.CharaBattle.MyParty.PartyType, member.CharaBattle.Join, true);
					break;
				}
				case eSkillTargetType.TgtAll:
				{
					this.m_BattleUI.ClearTargetMarkerOrderIcon();
					int num2 = 0;
					List<CharacterHandler> joinMembers = frameOwnerAt.CharaBattle.TgtParty.GetJoinMembers(false);
					for (int i = 0; i < joinMembers.Count; i++)
					{
						CharacterHandler characterHandler2 = joinMembers[i];
						if (!characterHandler2.CharaBattle.Param.IsDead())
						{
							Transform refTransform3;
							float y3;
							characterHandler2.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform3, out y3);
							TargetMarker.eMode mode3 = TargetMarker.eMode.Normal;
							int num3 = BattleCommandParser.JudgeElementCompatibility(battleCommandData.GetAttackSkillElementTypes(), characterHandler2.CharaParam.ElementType);
							if (num3 > 0)
							{
								mode3 = TargetMarker.eMode.Weak;
							}
							else if (num3 < 0)
							{
								mode3 = TargetMarker.eMode.Resist;
							}
							this.m_BattleUI.SetTargetMarker(num2, true, refTransform3, new Vector3(0f, y3, 0f), mode3);
							this.m_BattleUI.SetTargetMarkerToOrderIcon(characterHandler2.CharaBattle.MyParty.PartyType, characterHandler2.CharaBattle.Join, true);
							num2++;
						}
					}
					break;
				}
				case eSkillTargetType.MySingle:
				{
					CharacterHandler member2 = frameOwnerAt.CharaBattle.MyParty.GetMember(frameOwnerAt.CharaBattle.MyParty.SingleTargetIndex);
					Transform refTransform4;
					float y4;
					member2.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform4, out y4);
					TargetMarker.eMode mode4 = TargetMarker.eMode.Normal;
					this.m_BattleUI.SetTargetMarker(0, true, refTransform4, new Vector3(0f, y4, 0f), mode4);
					this.m_BattleUI.ClearTargetMarkerOrderIcon();
					this.m_BattleUI.SetTargetMarkerToOrderIcon(member2.CharaBattle.MyParty.PartyType, member2.CharaBattle.Join, true);
					break;
				}
				case eSkillTargetType.MyAll:
				{
					this.m_BattleUI.ClearTargetMarkerOrderIcon();
					int num4 = 0;
					List<CharacterHandler> joinMembers2 = frameOwnerAt.CharaBattle.MyParty.GetJoinMembers(false);
					for (int j = 0; j < joinMembers2.Count; j++)
					{
						CharacterHandler characterHandler3 = joinMembers2[j];
						if (!characterHandler3.CharaBattle.Param.IsDead())
						{
							Transform refTransform5;
							float y5;
							characterHandler3.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform5, out y5);
							TargetMarker.eMode mode5 = TargetMarker.eMode.Normal;
							this.m_BattleUI.SetTargetMarker(num4, true, refTransform5, new Vector3(0f, y5, 0f), mode5);
							num4++;
							this.m_BattleUI.SetTargetMarkerToOrderIcon(characterHandler3.CharaBattle.MyParty.PartyType, characterHandler3.CharaBattle.Join, true);
						}
					}
					break;
				}
				}
			}
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x00024604 File Offset: 0x00022A04
		private void UpdateTargetMarkerOnInterruptFriend(BattleDefine.eJoinMember join)
		{
			this.m_InputData.InputInterruptFriend_Select(join);
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			CharacterHandler member = this.PlayerParty.GetMember(join);
			if (member != null)
			{
				Transform refTransform;
				float y;
				member.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out refTransform, out y);
				TargetMarker.eMode mode = TargetMarker.eMode.Normal;
				this.m_BattleUI.SetTargetMarker(0, true, refTransform, new Vector3(0f, y, 0f), mode);
			}
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x00024678 File Offset: 0x00022A78
		private void SetupCommandButton(bool isUnSelect, bool isUpdateSlideUIOnUnSelect = true)
		{
			BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
			if (!frameDataAt.IsCharaType)
			{
				return;
			}
			CharacterHandler owner = frameDataAt.m_Owner;
			if (!owner.CharaBattle.MyParty.IsPlayerParty)
			{
				return;
			}
			int commandButtonNum = this.m_BattleUI.GetCommandButtonNum();
			for (int i = 0; i < commandButtonNum; i++)
			{
				CommandButton commandButton = this.m_BattleUI.GetCommandButton(i);
				commandButton.SetCommandIndex(i);
				commandButton.SetMode(CommandButton.eMode.None);
				if (i < owner.CharaBattle.CommandNum)
				{
					BattleCommandData commandDataAt = owner.CharaBattle.GetCommandDataAt(i);
					switch (commandDataAt.CommandType)
					{
					case BattleCommandData.eCommandType.NormalAttack:
						commandButton.SetupNormalAttack((eSkillType)commandDataAt.SkillParam.m_SkillType, commandDataAt.GetAttackSkillElementTypes());
						break;
					case BattleCommandData.eCommandType.Skill:
						commandButton.SetupSkill((eSkillType)commandDataAt.SkillParam.m_SkillType, commandDataAt.GetAttackSkillElementTypes(), owner.CharaParam.ClassType, owner.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Silence));
						break;
					case BattleCommandData.eCommandType.WeaponSkill:
						commandButton.SetupSkill((eSkillType)commandDataAt.SkillParam.m_SkillType, commandDataAt.GetAttackSkillElementTypes(), owner.CharaParam.ClassType, owner.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Silence));
						break;
					}
					commandButton.SetRecastGauge(commandDataAt.RecastVal, commandDataAt.RecastMax, commandDataAt.RecastRecoveredFlg, commandDataAt.Owner.CharaBattle);
					commandDataAt.ResetRecastRecoverdFlg();
					commandButton.SetEnableRender(true);
				}
				else
				{
					commandButton.PlayOut();
				}
			}
			if (isUnSelect)
			{
				this.m_InputData.SelectCommandIndex = -1;
				if (isUpdateSlideUIOnUnSelect)
				{
					BattleOrderIndicator order = this.m_BattleUI.GetOrder();
					order.SetFrames(this.m_Order, 0, BattleOrderIndicator.eMoveType.Immediate);
				}
				this.m_BattleUI.SetAllCommandButtonNotSelect();
				this.m_BattleUI.GetDescription().SetText(string.Empty);
				this.m_BattleUI.AppearOwnerImage();
				this.UpdateTargetMarker(BattleDefine.eJoinMember.None, false);
			}
		}

		// Token: 0x0600069B RID: 1691 RVA: 0x0002488C File Offset: 0x00022C8C
		private void SelectCommandButton(int commandIndex, bool isUpdateOrder = true)
		{
			this.m_InputData.SelectCommandIndex = commandIndex;
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			int selectingIndex = 0;
			if (isUpdateOrder)
			{
				selectingIndex = BattleCommandParser.UpdateOrder(this.m_Order, this.m_InputData.SelectCommandIndex, false);
			}
			else
			{
				for (int i = this.m_Order.GetFrameNum() - 1; i >= 0; i--)
				{
					BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(i);
					if (frameDataAt.CompareOwnerCharaOnly(frameOwnerAt))
					{
						selectingIndex = i;
						break;
					}
				}
			}
			BattleCommandData commandDataAt = frameOwnerAt.CharaBattle.GetCommandDataAt(this.m_InputData.SelectCommandIndex);
			BattleCommandParser.OptimizeTargetIndex(frameOwnerAt, commandDataAt.MainSkillTargetType);
			BattleOrderIndicator order = this.m_BattleUI.GetOrder();
			order.SetEnableRender(true);
			order.SetFrames(this.m_Order, selectingIndex, BattleOrderIndicator.eMoveType.Selecting);
			if (frameOwnerAt.CharaBattle.MyParty.IsPlayerParty)
			{
				this.UpdateTargetMarker(BattleDefine.eJoinMember.None, false);
				this.m_BattleUI.SetOwnerImage(frameOwnerAt.CharaParam.CharaID, true);
				this.m_BattleUI.AppearOwnerImage();
				this.m_BattleUI.GetDescription().SetText(commandDataAt.SkillParam.m_SkillDetail);
			}
			else
			{
				this.m_BattleUI.GetDescription().SetText(string.Empty);
			}
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x000249D8 File Offset: 0x00022DD8
		private void OnChangeCommandButton(int commandIndex, bool isSilence)
		{
			this.SelectCommandButton(commandIndex, true);
			if (isSilence)
			{
				CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
				this.m_BattleUI.GetBattleMessage().SetMessage(string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeSilence, new object[]
				{
					frameOwnerAt.GetNickName()
				}), new object[0]), false);
			}
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			if (seq == BattleTutorial.eSeq._03_WAVE_IDX_0_SWIPE && this.m_Tutorial.JudgeCommandBtnIndexHistory())
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.Clear();
				int[] needCommandBtnIndexHistory = this.m_Tutorial.NeedCommandBtnIndexHistory;
				for (int i = 0; i < needCommandBtnIndexHistory.Length; i++)
				{
					CommandButton commandButton = this.m_BattleUI.GetCommandButton(needCommandBtnIndexHistory[i]);
					if (commandButton != null)
					{
						TutorialTarget component = commandButton.GetComponent<TutorialTarget>();
						if (component != null)
						{
							component.SetEnable(false);
						}
					}
				}
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_ELEMENT, new Action(this.OnCompleteTutorialTipsWindow), new Action(this.OnOpenedTutorialTipsWindow));
			}
		}

		// Token: 0x0600069D RID: 1693 RVA: 0x00024B00 File Offset: 0x00022F00
		private void OnClickCommandButton(int newCommandIndex)
		{
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			switch (seq)
			{
			case BattleTutorial.eSeq._01_WAVE_IDX_0_ATK:
			case BattleTutorial.eSeq._06_WAVE_IDX_0_ATK_AFTER_TARGET:
				break;
			default:
				if (seq != BattleTutorial.eSeq._10_WAVE_IDX_1_ATK_AFTER_TIPS_STUN)
				{
					goto IL_81;
				}
				break;
			case BattleTutorial.eSeq._04_WAVE_IDX_0_TIPS_ELEMENT:
				return;
			}
			CommandButton commandButton = this.m_BattleUI.GetCommandButton(0);
			if (commandButton != null)
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.Clear();
				UIUtility.DetachCanvasForChangeRenderOrder(commandButton.gameObject, true);
			}
			this.m_Tutorial.ApplyNextSeq();
			IL_81:
			this.m_InputData.InputCommand_Decide(false);
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x00024B9A File Offset: 0x00022F9A
		private void OnClickFriendButton()
		{
			this.m_Option.SetIsAuto(false);
			this.m_BattleUI.SetAutoFlag(this.m_Option.GetIsAuto());
			this.InterruptFriendJoinSelect();
		}

		// Token: 0x0600069F RID: 1695 RVA: 0x00024BC8 File Offset: 0x00022FC8
		private bool OnJudgeInterruptFriendJoinSelect()
		{
			CharacterHandler member = this.PlayerParty.GetMember(this.m_InputData.InterruptJoin);
			if (member.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Isolation))
			{
				string textCommon = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeIsolation, new object[]
				{
					member.GetNickName()
				});
				this.m_BattleUI.GetBattleMessage().SetMessage(textCommon, false);
				return false;
			}
			return true;
		}

		// Token: 0x060006A0 RID: 1696 RVA: 0x00024C3B File Offset: 0x0002303B
		private void OnDecideInterruptFriendJoinSelect()
		{
			this.m_InputData.InputInterruptFriend_Decide();
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x00024C48 File Offset: 0x00023048
		private void OnCancelInterruptFriendJoinSelect()
		{
			this.m_InputData.InputInterruptFriend_Cancel();
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x00024C55 File Offset: 0x00023055
		private void OnMemberChange()
		{
			this.m_BattleUI.GetFriendButton().SetEnableInput(false);
			this.m_InputData.InputMaster_Start();
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x00024C74 File Offset: 0x00023074
		private void OnSelectMemberChange(int index)
		{
			this.m_InputData.InputMemberChange_Decide(index);
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			frameOwnerAt.CharaBattle.Param.MemberChangeRecastFull();
			this.m_ExecutedMemberChange = true;
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x00024CB1 File Offset: 0x000230B1
		private void OnCancelMemberChange()
		{
			this.m_InputData.InputMaster_Cancel();
		}

		// Token: 0x060006A5 RID: 1701 RVA: 0x00024CBE File Offset: 0x000230BE
		private void OnSelectMasterSkill(int masterSkillIndex, BattleDefine.eJoinMember singleTargetIndex)
		{
			this.m_InputData.InputMasterSkill_Decide(masterSkillIndex, singleTargetIndex);
		}

		// Token: 0x060006A6 RID: 1702 RVA: 0x00024CD0 File Offset: 0x000230D0
		private void OnClickTogetherButton()
		{
			List<BattleDefine.eJoinMember> list = this.m_Order.CalcTogetherAttackDefaultOrders();
			if (list != null && list.Count > 0)
			{
				this.m_InputData.InputTogetherAttack_Start();
			}
			else
			{
				this.m_BattleUI.GetBattleMessage().SetMessage(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeCantTogetherAttack), false);
				this.m_BattleUI.StartCommandInput();
			}
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			if (seq == BattleTutorial.eSeq._12_WAVE_IDX_1_UNIQUE_SKILL)
			{
				TogetherButton togetherButton = this.m_BattleUI.GetTogetherButton();
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.Clear();
				UIUtility.DetachCanvasForChangeRenderOrder(togetherButton.gameObject, true);
				this.m_Tutorial.ApplyNextSeq();
			}
		}

		// Token: 0x060006A7 RID: 1703 RVA: 0x00024D84 File Offset: 0x00023184
		private void OnDecideTogetherMember(BattleDefine.eJoinMember join)
		{
		}

		// Token: 0x060006A8 RID: 1704 RVA: 0x00024D86 File Offset: 0x00023186
		private void OnDecideTogether(List<BattleDefine.eJoinMember> joins)
		{
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			this.m_InputData.InputTogetherAttack_Decide();
			this.m_Order.GetTogetherData().SetAvailable(true);
			this.m_Order.GetTogetherData().SetJoinOrder(joins);
		}

		// Token: 0x060006A9 RID: 1705 RVA: 0x00024DC1 File Offset: 0x000231C1
		private void OnCancelTogether()
		{
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			this.m_BattleBG.ChangeColorFromCurrent(Color.white, 0.3f);
			this.m_InputData.InputTogetherAttack_Cancel();
		}

		// Token: 0x060006AA RID: 1706 RVA: 0x00024DF0 File Offset: 0x000231F0
		private void OpenMasterMenu()
		{
			this.m_BattleUI.GetMemberChange().SetMasterOrb(this.m_MasterOrbData, this.BSD.MstSkillFlg);
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					BattleDefine.eJoinMember join = (BattleDefine.eJoinMember)j;
					CharacterHandler member = this.m_Parties[i].GetMember(join);
					if (member != null)
					{
						bool isInteractable = !member.CharaBattle.Param.IsDead();
						if (i == 0)
						{
							this.m_BattleUI.SetMasterSkillTarget((BattleDefine.ePartyType)i, join, member.CharaID, isInteractable);
						}
						else
						{
							this.m_BattleUI.SetMasterSkillTarget((BattleDefine.ePartyType)i, join, member.CharaResource.ResourceID, isInteractable);
						}
					}
					else
					{
						this.m_BattleUI.SetMasterSkillTarget((BattleDefine.ePartyType)i, join, -1, false);
					}
				}
			}
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			for (int k = 0; k < 2; k++)
			{
				BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.Bench_1 + k;
				CharacterHandler member2 = this.PlayerParty.GetMember(eJoinMember);
				BenchStatus benchStatus = this.m_BattleUI.GetBenchStatus(eJoinMember);
				if (member2 != null && !member2.CharaBattle.Param.IsDead())
				{
					benchStatus.SetEnableRender(true);
					bool isStateAbnormalIsolation = frameOwnerAt.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Isolation);
					benchStatus.SetupForOpenMemberChange(member2.CharaID, member2.CharaParam.Lv, member2.CharaParam.MaxLv, member2.CharaBattle.Param.GetHp(), member2.CharaBattle.Param.FixedMaxHp(), member2.CharaParam.ElementType, member2.CharaBattle.Param.GetMemberChangeRecast(), member2.CharaBattle.Param.GetMemberChangeRecastMax(), member2.GetNickName(), frameOwnerAt.GetNickName(), isStateAbnormalIsolation, this.m_ExecutedMemberChange, frameOwnerAt.IsFriendChara);
				}
				else
				{
					benchStatus.SetEnableRender(false);
				}
			}
			this.SetTurnOwnerMarker(false);
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			this.m_BattleUI.OpenMemberChange();
		}

		// Token: 0x060006AB RID: 1707 RVA: 0x00025008 File Offset: 0x00023408
		private void CloseMasterMenu(bool isReturnToInputCommand)
		{
			this.m_BattleUI.CloseMemberChange();
			if (isReturnToInputCommand)
			{
				this.SetTurnOwnerMarker(true);
				int num = this.m_InputData.SelectCommandIndex;
				if (num == -1)
				{
					num = 0;
				}
				this.m_InputData.Reset();
				this.m_BattleUI.StartCommandInput();
				this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
				this.m_BattleUI.StartCommandInput();
				this.m_BattleUI.SetCommandButtonSelecting(num);
				this.SelectCommandButton(num, true);
				this.SetTurnOwnerMarker(true);
				this.m_BattleUI.AppearOwnerImage();
			}
		}

		// Token: 0x060006AC RID: 1708 RVA: 0x000250A8 File Offset: 0x000234A8
		private void OpenTogetherAttackMenu()
		{
			this.SetTurnOwnerMarker(false);
			CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
			if (frameOwnerAt == null)
			{
				return;
			}
			BattleCommandParser.OptimizeTargetIndex(frameOwnerAt, eSkillTargetType.TgtSingle);
			this.m_BattleUI.SetTargetMarkerAllEnableRender(false);
			this.UpdateTargetMarker(BattleDefine.eJoinMember.None, true);
			List<BattleDefine.eJoinMember> list = this.m_Order.CalcTogetherAttackDefaultOrders();
			if (list.Count <= 0)
			{
				return;
			}
			List<CharacterHandler> members = this.PlayerParty.GetMembers(list);
			int[] array = new int[members.Count];
			eElementType[] array2 = new eElementType[members.Count];
			BattleDefine.eJoinMember[] array3 = new BattleDefine.eJoinMember[members.Count];
			int[] array4 = new int[members.Count];
			for (int i = 0; i < members.Count; i++)
			{
				array[i] = members[i].CharaID;
				array2[i] = members[i].CharaParam.ElementType;
				array3[i] = members[i].CharaBattle.Join;
				array4[i] = members[i].CharaBattle.GetUniqueSkillCommandData().SkillID;
			}
			if (this.m_Option.GetIsSkipTogetherAttackInput() || this.m_IsAutoTogetherAttack)
			{
				this.m_IsAutoTogetherAttack = false;
				this.m_BattleUI.OpenTogetherCutInSkipSelect(array, array2, array3, array4, list);
				this.OnDecideTogether(list);
			}
			else
			{
				this.m_BattleUI.OpenTogetherSelect(array, array2, array3, array4, (int)this.m_Order.GetGauge().GetValue());
			}
			this.m_BattleBG.ChangeColorFromCurrent(BattleDefine.BG_COLOR_UNIQUESKILL, 0.3f);
		}

		// Token: 0x060006AD RID: 1709 RVA: 0x00025239 File Offset: 0x00023639
		private void OnClickAutoButton()
		{
			this.ToggleAuto();
		}

		// Token: 0x060006AE RID: 1710 RVA: 0x00025244 File Offset: 0x00023644
		private void UpdateTouchCancleAuto()
		{
			Vector2 pos;
			if (InputTouch.Inst.DetectTriggerOfSingleTouch(out pos, 0) && !UIUtility.CheckPointerOverTarget(pos, this.m_BattleUI.GetAutoButton().gameObject) && this.m_Option.GetIsAuto())
			{
				this.ToggleAuto();
			}
		}

		// Token: 0x060006AF RID: 1711 RVA: 0x00025294 File Offset: 0x00023694
		private void ToggleAuto()
		{
			this.m_Option.ToggleAuto();
			if (!this.m_Tutorial.IsSeq(BattleTutorial.eSeq.None))
			{
				this.m_Option.SetIsAuto(false);
			}
			this.m_BattleUI.SetAutoFlag(this.m_Option.GetIsAuto());
		}

		// Token: 0x060006B0 RID: 1712 RVA: 0x000252E0 File Offset: 0x000236E0
		private void OnClickMenuButton()
		{
			this.IsEnableTouchChangeTarget = false;
			this.m_BattleUI.OpenBattleMenuWindow();
		}

		// Token: 0x060006B1 RID: 1713 RVA: 0x000252F4 File Offset: 0x000236F4
		private void OnCancelBattleMenuWindow()
		{
			this.IsEnableTouchChangeTarget = true;
			this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
		}

		// Token: 0x060006B2 RID: 1714 RVA: 0x0002531B File Offset: 0x0002371B
		private void OnClickRetireButton()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.BattleRetireTitle, eText_MessageDB.BattleRetire, new Action<int>(this.OnClosedWindow_BattleRetire));
		}

		// Token: 0x060006B3 RID: 1715 RVA: 0x00025344 File Offset: 0x00023744
		private void OnClosedWindow_BattleRetire(int answer)
		{
			if (answer == 0)
			{
				this.SetMainStep(BattleSystem.eMainStep.Retire);
			}
			else if (this.m_BattleUI.IsExectuteRetireFromDirect)
			{
				this.m_BattleUI.OnCancelBattleMenuWindowCallBack();
				this.m_BattleUI.IsExectuteRetireFromDirect = false;
			}
			else
			{
				this.m_BattleUI.OpenBattleMenuWindow();
			}
		}

		// Token: 0x060006B4 RID: 1716 RVA: 0x0002539C File Offset: 0x0002379C
		private void OpenRetryWindow()
		{
			int num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.BattleContinueAmount);
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.BattleRetryTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.BattleRetry, new object[]
			{
				num
			}), null, new Action<int>(this.OnClosedWindow_BattleRetry));
		}

		// Token: 0x060006B5 RID: 1717 RVA: 0x00025415 File Offset: 0x00023815
		private void OnClosedWindow_BattleRetry(int answer)
		{
			if (answer == 0)
			{
				this.SetMainStep(BattleSystem.eMainStep.RetryRequest);
			}
			else
			{
				this.SetMainStep(BattleSystem.eMainStep.RetryCancel);
			}
		}

		// Token: 0x060006B6 RID: 1718 RVA: 0x00025432 File Offset: 0x00023832
		private void SetDestroyStep(BattleSystem.eDestroyStep step)
		{
			this.m_DestroyStep = step;
		}

		// Token: 0x060006B7 RID: 1719 RVA: 0x0002543C File Offset: 0x0002383C
		private BattleSystem.eMode Update_Destroy()
		{
			switch (this.m_DestroyStep)
			{
			case BattleSystem.eDestroyStep.First:
				Time.timeScale = 1f;
				this.TimeScaler.SetBaseTimeScale(1f);
				if (this.m_IsDestroyOnlyUI)
				{
					this.SetDestroyStep(BattleSystem.eDestroyStep.DestroyOnlyUI_0);
				}
				else
				{
					LocalSaveData.Inst.Save();
					this.SetDestroyStep(BattleSystem.eDestroyStep.Destroy_0);
				}
				break;
			case BattleSystem.eDestroyStep.DestroyOnlyUI_0:
				if (this.m_BattleUI != null)
				{
					this.m_BattleUI.Destroy();
					UnityEngine.Object.Destroy(this.m_BattleUI.gameObject);
					this.m_BattleUI = null;
				}
				this.SetDestroyStep(BattleSystem.eDestroyStep.UnloadUnusedAssets);
				break;
			case BattleSystem.eDestroyStep.Destroy_0:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.m_BattleUI == null)
					{
						this.SetDestroyStep(BattleSystem.eDestroyStep.Destroy_2);
					}
					else
					{
						this.m_BattleUI.Destroy();
						UnityEngine.Object.Destroy(this.m_BattleUI.gameObject);
						this.m_BattleUI = null;
						this.SetDestroyStep(BattleSystem.eDestroyStep.Destroy_1);
					}
				}
				break;
			case BattleSystem.eDestroyStep.Destroy_1:
				this.SetDestroyStep(BattleSystem.eDestroyStep.Destroy_2);
				break;
			case BattleSystem.eDestroyStep.Destroy_2:
				this.m_FriendCutIn.Destroy();
				if (this.m_SkillActionPlayer != null)
				{
					this.m_SkillActionPlayer.Destroy();
					base.gameObject.RemoveComponent<SkillActionPlayer>();
					this.m_SkillActionPlayer = null;
				}
				if (this.m_BattleBG != null)
				{
					UnityEngine.Object.Destroy(this.m_BattleBG.gameObject);
					this.m_BattleBG = null;
				}
				if (this.m_BgResourceObjHndl != null)
				{
					this.m_BgLoader.UnloadAll(false);
					this.m_BgResourceObjHndl = null;
				}
				this.m_DropObjectManager.Destroy();
				this.PlayerParty.Destroy();
				this.EnemyParty.Destroy();
				this.m_CharaHndlForGameOver = null;
				SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.UnloadAll(false);
				this.SetDestroyStep(BattleSystem.eDestroyStep.Destroy_3);
				break;
			case BattleSystem.eDestroyStep.Destroy_3:
				this.m_BgLoader.Update();
				if (this.m_BgLoader.IsCompleteUnloadAll())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.IsCompleteUnloadAll())
					{
						SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.UnloadEffectAll(new string[]
						{
							"btl_always"
						});
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Battle);
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.BGM_uniqueSkill);
						this.SetDestroyStep(BattleSystem.eDestroyStep.UnloadUnusedAssets);
					}
				}
				break;
			case BattleSystem.eDestroyStep.UnloadUnusedAssets:
				SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
				this.SetDestroyStep(BattleSystem.eDestroyStep.UnloadUnusedAssets_Wait);
				break;
			case BattleSystem.eDestroyStep.UnloadUnusedAssets_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
				{
					this.SetDestroyStep(BattleSystem.eDestroyStep.End);
				}
				break;
			}
			return BattleSystem.eMode.Destroy;
		}

		// Token: 0x060006B8 RID: 1720 RVA: 0x000256F0 File Offset: 0x00023AF0
		private void SetMainStep(BattleSystem.eMainStep step)
		{
			this.m_MainStep = step;
			BattleSystem.eMainStep mainStep = this.m_MainStep;
			if (mainStep != BattleSystem.eMainStep.First)
			{
				if (mainStep == BattleSystem.eMainStep.TurnPreEnd)
				{
					this.m_TurnPreEndStep = 0;
				}
			}
			else
			{
				SoundCueSheetDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM);
				if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(param.m_CueSheet, this.m_BgmCueName))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
				}
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsOverlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
				}
			}
		}

		// Token: 0x060006B9 RID: 1721 RVA: 0x0002579C File Offset: 0x00023B9C
		private BattleSystem.eMode Update_Main()
		{
			if (this.m_Scaler != null && this.m_Scaler.IsDirty)
			{
				if (this.IsInUniqueSkillScene || this.m_MainStep == BattleSystem.eMainStep.UniqueSkillScenePrepare_WaitOnNotEffect_0)
				{
					this.m_Scaler.SetBaseTimeScale(LocalSaveData.Inst.GetBattleSpeedScale(LocalSaveData.eBattleSpeedLv.UniqueSkill));
					this.m_Scaler.IsDirty = false;
				}
				else if (this.m_Order != null)
				{
					CharacterHandler frameOwnerAt = this.m_Order.GetFrameOwnerAt(0);
					if (frameOwnerAt != null)
					{
						if (frameOwnerAt.CharaBattle.MyParty.IsEnemyParty)
						{
							this.m_Scaler.SetBaseTimeScale(LocalSaveData.Inst.GetBattleSpeedScale(LocalSaveData.eBattleSpeedLv.EN));
						}
						else
						{
							this.m_Scaler.SetBaseTimeScale(LocalSaveData.Inst.GetBattleSpeedScale(LocalSaveData.eBattleSpeedLv.PL));
						}
						this.m_Scaler.IsDirty = false;
					}
				}
			}
			if (this.m_KiraraJumpScene != null)
			{
				this.m_KiraraJumpScene.Update();
			}
			if (this.m_UniqueSkillScene != null)
			{
				this.m_UniqueSkillScene.Update();
			}
			this.UpdateTouchCancleAuto();
			switch (this.m_MainStep)
			{
			case BattleSystem.eMainStep.First:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.RequestSort();
					this.m_BattleUI.Hide();
					if (!this.m_BattleBG.IsPlayingAnim())
					{
						this.m_BattleBG.Play();
					}
					if (this.BSD.WaveIdx != 0 && this.m_BattleUI.GetTransitFade().IsEnableRender())
					{
						this.m_BattleUI.GetTransitFade().PlayOut();
					}
					if (this.IsWarningWave())
					{
						this.SetMainStep(BattleSystem.eMainStep.WarningStart);
					}
					else
					{
						this.SetMainStep(BattleSystem.eMainStep.WaveInCharacter);
					}
				}
				break;
			case BattleSystem.eMainStep.WarningStart:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.m_BattleUI.GetTransitFade().IsHideComplete())
					{
						this.m_BattleUI.GetWarningStart().Play();
						this.m_PlayedWarningVoice = this.EnemyParty.PlayVoice("voice_400");
						SoundCueSheetDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM);
						if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(param.m_CueSheet, this.m_BgmCueName))
						{
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
						}
						this.SetMainStep(BattleSystem.eMainStep.WarningStart_Wait);
					}
				}
				break;
			case BattleSystem.eMainStep.WarningStart_Wait:
				if (!this.m_BattleUI.GetWarningStart().IsPlaying())
				{
					this.SetMainStep(BattleSystem.eMainStep.WaveInCharacter);
				}
				break;
			case BattleSystem.eMainStep.WaveInCharacter:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.m_BattleUI.GetTransitFade().IsHideComplete())
					{
						this.WaveInParty();
						this.SetMainStep(BattleSystem.eMainStep.WaveInCharacter_Wait);
					}
				}
				break;
			case BattleSystem.eMainStep.WaveInCharacter_Wait:
				this.RequestSort();
				if (this.IsCompleteWaveInParty())
				{
					if (this.BSD.WaveIdx == 0 && !this.IsWarningWave())
					{
						if (!this.m_PlayedWarningVoice)
						{
							this.PlayerParty.PlayVoice(eSoundVoiceListDB.voice_401);
						}
					}
					else if (this.IsWarningWave() && !this.m_PlayedWarningVoice)
					{
						this.PlayerParty.PlayVoice(eSoundVoiceListDB.voice_401);
					}
					for (int i = 3; i < 6; i++)
					{
						CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
						if (member != null)
						{
							member.SetEnableRender(false);
						}
					}
					if (this.BSD.WaveIdx == 0 && this.BSD.WaveNum > 1 && this.BSD.ContCount == 0)
					{
						this.SetMainStep(BattleSystem.eMainStep.BattleStart);
					}
					else
					{
						this.UpdateAllCharacterStatus(1, false, false);
						this.SetMainStep(BattleSystem.eMainStep.OrderCheck);
					}
				}
				break;
			case BattleSystem.eMainStep.BattleStart:
				this.m_Timer.SetTimeAndPlay(0.65f);
				this.SetMainStep(BattleSystem.eMainStep.BattleStart_Wait_0);
				break;
			case BattleSystem.eMainStep.BattleStart_Wait_0:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_BattleCamera.ToDefault(0f);
					this.SetMainStep(BattleSystem.eMainStep.BattleStart_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.BattleStart_Wait_1:
				if (!this.m_BattleCamera.IsMoving)
				{
					this.m_TitleCutIn.Play(BattleTitleCutIn.eType.Start);
					this.SetMainStep(BattleSystem.eMainStep.BattleStart_Wait_2);
				}
				break;
			case BattleSystem.eMainStep.BattleStart_Wait_2:
				if (this.m_TitleCutIn.IsCompletePlay())
				{
					this.UpdateAllCharacterStatus(1, false, false);
					this.SetMainStep(BattleSystem.eMainStep.OrderCheck);
				}
				break;
			case BattleSystem.eMainStep.OrderSlide:
				this.OrderSlide();
				this.SetMainStep(BattleSystem.eMainStep.OrderSlide_Wait);
				break;
			case BattleSystem.eMainStep.OrderSlide_Wait:
				if (!this.m_BattleUI.GetOrder().IsSlide())
				{
					this.SetMainStep(BattleSystem.eMainStep.OrderSlide_AfterWait);
				}
				break;
			case BattleSystem.eMainStep.OrderSlide_AfterWait:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_BattleUI.GetFriendButton().SetEnableInput(false);
					this.SetMainStep(BattleSystem.eMainStep.OrderCheck);
				}
				break;
			case BattleSystem.eMainStep.OrderCheck:
				if (!this.m_BattleUI.GetOrder().IsPopup())
				{
					BattleOrder.eFrameType eFrameType = this.TurnStartProcess();
					if (eFrameType != BattleOrder.eFrameType.Chara)
					{
						if (eFrameType == BattleOrder.eFrameType.Card)
						{
							this.SetMainStep(BattleSystem.eMainStep.CommandExecute);
						}
					}
					else
					{
						this.SetMainStep(BattleSystem.eMainStep.TurnSkipCheck);
					}
				}
				break;
			case BattleSystem.eMainStep.TurnSkipCheck:
				if (this.TurnSkipProcess())
				{
					this.m_Timer.SetTimeAndPlay(1.8f);
					this.SetMainStep(BattleSystem.eMainStep.TurnSkip_Wait);
				}
				else
				{
					this.SetMainStep(BattleSystem.eMainStep.CommandInputStartCheck);
				}
				break;
			case BattleSystem.eMainStep.TurnSkip_Wait:
				if (this.m_Timer.Sec <= 0f)
				{
					this.SetMainStep(BattleSystem.eMainStep.TurnPreEnd);
				}
				break;
			case BattleSystem.eMainStep.CommandInputStartCheck:
				if (this.CommandInputStart())
				{
					this.m_Timer.SetTimeAndPlay(1.8f);
				}
				if (!this.m_Tutorial.IsSeq(BattleTutorial.eSeq.None))
				{
					BattleOrder.FrameData frameDataAt = this.m_Order.GetFrameDataAt(0);
					if (frameDataAt.m_Owner.CharaBattle.MyParty.IsPlayerParty)
					{
						BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
						switch (seq)
						{
						case BattleTutorial.eSeq._00_WAVE_IDX_0_TIPS_START:
							if (this.BSD.WaveIdx == 0)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_BATTLE, new Action(this.OnCompleteTutorialTipsWindow), null);
							}
							break;
						default:
							if (seq != BattleTutorial.eSeq._07_WAVE_IDX_1_TIPS_START)
							{
								if (seq == BattleTutorial.eSeq._11_WAVE_IDX_1_TIPS_UNIQUE_SKILL)
								{
									if (this.BSD.WaveIdx == 1)
									{
										SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_UNIQUESKILL, new Action(this.OnCompleteTutorialTipsWindow), null);
									}
								}
							}
							else if (this.BSD.WaveIdx == 1)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_MASTER, new Action(this.OnCompleteTutorialTipsWindow), null);
							}
							break;
						case BattleTutorial.eSeq._02_WAVE_IDX_0_TIPS_COMMAND:
							if (this.BSD.WaveIdx == 0)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_COMMAND, new Action(this.OnCompleteTutorialTipsWindow), null);
							}
							break;
						}
					}
				}
				this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
				break;
			case BattleSystem.eMainStep.CommandInput_Wait:
				if (this.m_Timer.Sec <= 0f)
				{
					CharacterHandler frameOwnerAt2 = this.m_Order.GetFrameOwnerAt(0);
					if (this.m_InputData.Result == BattleInputData.eResult.None && frameOwnerAt2.CharaBattle.MyParty.IsPlayerParty && this.m_Option.GetIsAuto())
					{
						this.CommandDecisonByAuto();
					}
					switch (this.m_InputData.Result)
					{
					case BattleInputData.eResult.Command:
						this.CloseBattleUIOnPlayerTurn();
						this.SetMainStep(BattleSystem.eMainStep.CommandExecute);
						break;
					case BattleInputData.eResult.Master:
						this.OpenMasterMenu();
						this.SetMainStep(BattleSystem.eMainStep.MasterUIInput_Wait);
						break;
					case BattleInputData.eResult.TogetherAttack:
						this.OpenTogetherAttackMenu();
						this.SetMainStep(BattleSystem.eMainStep.TogetherAttackInput_Wait);
						break;
					default:
						this.UpdateTouchChangeTargetIndex();
						this.m_BattleUI.UpdateAndroidBackKey();
						break;
					}
				}
				break;
			case BattleSystem.eMainStep.MasterUIInput_Wait:
				if (this.m_InputData.IsCancel)
				{
					this.CloseMasterMenu(true);
					this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
				}
				else if (this.m_InputData.SelectMasterSkillIndex != -1)
				{
					this.SetMainStep(BattleSystem.eMainStep.MasterSkillExecute_0);
				}
				else if (this.m_InputData.SelectBenchIndex != -1)
				{
					this.CloseMasterMenu(false);
					this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_0);
				}
				break;
			case BattleSystem.eMainStep.MasterSkillExecute_0:
				this.ExecMasterSkill();
				this.SetMainStep(BattleSystem.eMainStep.MasterSkillExecute_1);
				break;
			case BattleSystem.eMainStep.MasterSkillExecute_1:
				if (!this.m_SkillActionPlayer.IsPlaying())
				{
					this.m_SkillActionPlayer.Destroy();
					int num = this.m_InputData.SelectCommandIndex;
					if (num == -1)
					{
						num = 0;
					}
					this.m_InputData.Reset();
					this.m_Order.SaveFrames();
					this.m_BattleUI.StartCommandInput();
					this.m_BattleUI.GetFriendButton().SetEnableInput(!this.BSD.FrdUsed);
					this.SetupCommandButton(true, true);
					this.m_BattleUI.SetCommandButtonSelecting(num);
					this.SelectCommandButton(num, true);
					this.UpdateAllCharacterStatus(2, true, false);
					this.SetTurnOwnerMarker(true);
					BattleTutorial.eSeq seq2 = this.m_Tutorial.GetSeq();
					if (seq2 == BattleTutorial.eSeq._09_WAVE_IDX_1_TIPS_CHARGESTUN)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.BTL_CHARGE, new Action(this.OnCompleteTutorialTipsWindow), null);
					}
					this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
				}
				break;
			case BattleSystem.eMainStep.MemberChangeExecute_0:
				this.ChangePlayerJoinMember_GotoBench();
				this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_1);
				break;
			case BattleSystem.eMainStep.MemberChangeExecute_1:
				this.RequestSort();
				if (this.IsCompleteGotoBench())
				{
					bool flag = this.ChangePlayerJoinMember_GotoJoin();
					if (flag)
					{
						this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_4);
					}
					else
					{
						this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_2);
					}
				}
				break;
			case BattleSystem.eMainStep.MemberChangeExecute_2:
				if (this.IsDonePrepareChangePlayerJoinMember())
				{
					this.PlayerParty.PrepareEffect();
					this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_3);
				}
				break;
			case BattleSystem.eMainStep.MemberChangeExecute_3:
				if (!this.PlayerParty.IsPreparingEffect())
				{
					this.ChangePlayerJoinMember_GotoJoin();
					this.SetMainStep(BattleSystem.eMainStep.MemberChangeExecute_4);
				}
				break;
			case BattleSystem.eMainStep.MemberChangeExecute_4:
				this.RequestSort();
				if (this.IsCompleteGotoJoin())
				{
					this.ChangePlayerJoinMember();
					this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
				}
				break;
			case BattleSystem.eMainStep.AutoMemberChangeExecute_0:
			{
				bool flag2 = this.ChangePlayerJoinMember_GotoJoinOnPlayerDead();
				if (flag2)
				{
					this.SetMainStep(BattleSystem.eMainStep.AutoMemberChangeExecute_3);
				}
				else
				{
					this.SetMainStep(BattleSystem.eMainStep.AutoMemberChangeExecute_1);
				}
				break;
			}
			case BattleSystem.eMainStep.AutoMemberChangeExecute_1:
				if (this.IsDonePrepareChangePlayerJoinMember_GotoJoinOnPlayerDead())
				{
					this.PlayerParty.PrepareEffect();
					this.SetMainStep(BattleSystem.eMainStep.AutoMemberChangeExecute_2);
				}
				break;
			case BattleSystem.eMainStep.AutoMemberChangeExecute_2:
				if (!this.PlayerParty.IsPreparingEffect())
				{
					this.ChangePlayerJoinMember_GotoJoinOnPlayerDead();
					this.SetMainStep(BattleSystem.eMainStep.AutoMemberChangeExecute_3);
				}
				break;
			case BattleSystem.eMainStep.AutoMemberChangeExecute_3:
				this.RequestSort();
				if (this.IsCompleteGotoJoin())
				{
					this.ChangePlayerDeadMember();
					this.SetMainStep(BattleSystem.eMainStep.PostCommandExecute);
				}
				break;
			case BattleSystem.eMainStep.GoBackFriendExecute_0:
				this.ChangePlayerJoinMember_GotoBenchOnGoBackFriend();
				this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_1);
				break;
			case BattleSystem.eMainStep.GoBackFriendExecute_1:
				this.RequestSort();
				if (this.IsCompleteGotoBenchOnGoBackFriend())
				{
					bool flag3 = this.ChangePlayerJoinMember_GotoJoinOnGoBackFriend();
					if (flag3)
					{
						this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_4);
					}
					else
					{
						this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_2);
					}
				}
				break;
			case BattleSystem.eMainStep.GoBackFriendExecute_2:
				if (this.IsDonePrepareChangePlayerJoinMember_GotoJoinOnGoBackFriend())
				{
					this.PlayerParty.PrepareEffect();
					this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_3);
				}
				break;
			case BattleSystem.eMainStep.GoBackFriendExecute_3:
				if (!this.PlayerParty.IsPreparingEffect())
				{
					this.ChangePlayerJoinMember_GotoJoinOnGoBackFriend();
					this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_4);
				}
				break;
			case BattleSystem.eMainStep.GoBackFriendExecute_4:
				this.RequestSort();
				if (this.IsCompleteGotoJoinOnGoBackFriend())
				{
					this.ChangePlayerJoinMemberOnGoBackFriend();
					this.SetMainStep(BattleSystem.eMainStep.PostCommandExecute);
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendJoinSelect_Wait:
				if (this.m_InputData.IsCancel)
				{
					BattleSystem.eMainStep mainStep = this.ReturnFromCancelInterruptFriend();
					this.SetMainStep(mainStep);
				}
				else if (this.m_InputData.IsDecideInterruptJoin)
				{
					this.ExecInterruptFriend();
					this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_0);
				}
				else
				{
					this.UpdateTouchChangeInterruptFriedJoin();
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_0:
			{
				CharacterHandler friend = this.PlayerParty.GetFriend();
				this.m_FriendCutIn.Play(friend.CharaID);
				this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_1);
				break;
			}
			case BattleSystem.eMainStep.InterruptFriendExecute_1:
				if (this.m_FriendCutIn.IsPrepareError())
				{
					this.SetMainStep(BattleSystem.eMainStep.PrepareError);
				}
				else if (this.m_FriendCutIn.IsCompletePlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeInFromCurrentColor(-1f, null);
					this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_2);
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_2:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.ChangePlayerJoinMember_GotoBenchOnInterruputFriend();
					this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_3);
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_3:
				this.RequestSort();
				if (this.IsCompleteGotoBenchOnInterruputFriend())
				{
					bool flag4 = this.ChangePlayerJoinMember_GotoJoinOnInterruputFriend();
					if (flag4)
					{
						this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_6);
					}
					else
					{
						this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_4);
					}
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_4:
				if (this.IsDonePrepareChangePlayerJoinMember_GotoJoinOnInterruputFriend())
				{
					this.PlayerParty.PrepareEffect();
					this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_5);
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_5:
				if (!this.PlayerParty.IsPreparingEffect())
				{
					this.ChangePlayerJoinMember_GotoJoinOnInterruputFriend();
					this.SetMainStep(BattleSystem.eMainStep.InterruptFriendExecute_6);
				}
				break;
			case BattleSystem.eMainStep.InterruptFriendExecute_6:
				this.RequestSort();
				if (this.IsCompleteGotoJoinOnInterruputFriend())
				{
					this.ChangePlayerJoinMemberOnInterruputFriend();
					this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
				}
				break;
			case BattleSystem.eMainStep.TogetherAttackInput_Wait:
				if (this.m_InputData.IsCancel)
				{
					if (this.m_BattleUI.GetTogetherSelect().IsSelectEnd())
					{
						this.ReturnFromCancelTogetherAttack();
						this.SetMainStep(BattleSystem.eMainStep.CommandInput_Wait);
					}
				}
				else if (this.m_Order.GetTogetherData().IsAvailable())
				{
					if (this.m_BattleUI.GetTogetherCutIn().IsFilledScreen())
					{
						this.SetMainStep(BattleSystem.eMainStep.KiraraJumpScenePrepare);
					}
				}
				else
				{
					this.UpdateTouchChangeTargetIndexOnTogetherAttack();
				}
				break;
			case BattleSystem.eMainStep.TogetherAttackExecute_0:
			{
				this.m_Scaler.IsDirty = true;
				bool flag5 = this.ExecTogetherAttackOfTurnOwner();
				if (flag5)
				{
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillScenePrepare_Wait);
				}
				else
				{
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillScenePrepare_WaitOnNotEffect_0);
				}
				break;
			}
			case BattleSystem.eMainStep.KiraraJumpScenePrepare:
			{
				this.m_KiraraJumpScene = new BattleKiraraJumpScene();
				List<CharacterHandler> members = this.PlayerParty.GetMembers(this.m_Order.GetTogetherData().GetJoinOrder());
				this.m_KiraraJumpScene.Prepare(this, this.MainCamera, members);
				this.SetMainStep(BattleSystem.eMainStep.KiraraJumpScenePrepare_Wait);
				break;
			}
			case BattleSystem.eMainStep.KiraraJumpScenePrepare_Wait:
				if (this.m_KiraraJumpScene.IsPrepareError())
				{
					this.SetMainStep(BattleSystem.eMainStep.PrepareError);
				}
				else if (this.m_KiraraJumpScene.IsCompltePrepare())
				{
					this.m_KiraraJumpScene.Play();
					this.SetMainStep(BattleSystem.eMainStep.KiraraJumpScenePlaying);
				}
				break;
			case BattleSystem.eMainStep.KiraraJumpScenePlaying:
				if (this.m_KiraraJumpScene.IsCompltePlay())
				{
					this.m_KiraraJumpScene.Destory();
					this.SetMainStep(BattleSystem.eMainStep.KiraraJumpSceneDestroy_Wait_0);
				}
				break;
			case BattleSystem.eMainStep.KiraraJumpSceneDestroy_Wait_0:
				if (this.m_KiraraJumpScene.IsComplteDestory())
				{
					this.m_KiraraJumpScene = null;
					this.RequestSort();
					this.SetMainStep(BattleSystem.eMainStep.TogetherAttackExecute_0);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillScenePrepare_Wait:
				if (this.m_UniqueSkillScene.IsPrepareError())
				{
					this.SetMainStep(BattleSystem.eMainStep.PrepareError);
				}
				else if (this.m_UniqueSkillScene.IsCompltePrepare())
				{
					this.m_UniqueSkillScene.Play();
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillScenePlaying);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillScenePrepare_WaitOnNotEffect_0:
				if (this.m_BattleUI.GetTransitFade().IsHideComplete())
				{
					this.DisplaySkillNameTogetherAttackOfTurnOwner();
					this.m_BattleUI.PlayOutReward();
					this.m_Timer.SetTimeAndPlay(1.5f);
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillScenePrepare_WaitOnNotEffect_1);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillScenePrepare_WaitOnNotEffect_1:
				if (this.m_Timer.Sec <= 0f)
				{
					this.SetMainStep(BattleSystem.eMainStep.CommandExecute);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillScenePlaying:
				if (this.m_UniqueSkillScene.IsCompltePlay())
				{
					this.m_UniqueSkillScene.Destory();
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_0);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_0:
				if (this.m_UniqueSkillScene.IsComplteDestory())
				{
					this.m_UniqueSkillSceneUnloadCueSheetNames = this.m_UniqueSkillScene.UnloadCueSheetNames;
					this.m_UniqueSkillScene = null;
					int totalDamageHitNum = this.m_ExecCommand.SolveResult.GetTotalDamageHitNum();
					int totalRecoverHitNum = this.m_ExecCommand.SolveResult.GetTotalRecoverHitNum();
					if (totalDamageHitNum > 0 && totalRecoverHitNum == 0)
					{
						if (this.m_ExecCommand.Owner.CharaBattle.MyParty.IsPlayerParty)
						{
							this.m_BattleCamera.SetZoom(BattleCamera.eZoomType.ToEnemyOnUniqueSkillResult);
						}
						else
						{
							this.m_BattleCamera.SetZoom(BattleCamera.eZoomType.ToPlayerOnUniqueSkillResult);
						}
					}
					else if (totalRecoverHitNum > 0 && totalDamageHitNum == 0)
					{
						if (this.m_ExecCommand.Owner.CharaBattle.MyParty.IsPlayerParty)
						{
							this.m_BattleCamera.SetZoom(BattleCamera.eZoomType.ToPlayerOnUniqueSkillResult);
						}
						else
						{
							this.m_BattleCamera.SetZoom(BattleCamera.eZoomType.ToEnemyOnUniqueSkillResult);
						}
					}
					this.m_BattleUI.PlayInReward();
					this.m_BattleUI.GetTransitFade().PlayInQuick(BattleUniqueSkillScene.FADE_COLOR);
					this.m_BattleUI.GetTransitFade().PlayOut();
					this.RequestSort();
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_1:
				if (this.m_BattleUI.GetTransitFade().IsHideComplete())
				{
					bool flag6 = this.PostTogetherAttackOfTurnOwner();
					if (flag6)
					{
						this.m_Timer.SetTimeAndPlay(2f);
					}
					else
					{
						this.m_Timer.SetTimeAndPlay(0.8f);
					}
					this.SetMainStep(BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_2);
				}
				break;
			case BattleSystem.eMainStep.UniqueSkillSceneDestroy_Wait_2:
				if (this.m_Timer.Sec <= 0f)
				{
					if (this.IsCompletePostAfterExecCommand())
					{
						if (this.m_UniqueSkillSceneUnloadCueSheetNames != null)
						{
							for (int j = 0; j < this.m_UniqueSkillSceneUnloadCueSheetNames.Count; j++)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(this.m_UniqueSkillSceneUnloadCueSheetNames[j]);
							}
						}
						this.m_UniqueSkillSceneUnloadCueSheetNames = null;
						bool flag7 = this.CanBeContinueTogetherAttack();
						if ((!flag7 || (flag7 && !this.IsExistUniqueSkillSceneOfTurnOwner())) && !this.m_BattleCamera.IsZoomed)
						{
							this.m_BattleCamera.ToDefault(0.8f);
						}
						this.SetMainStep(BattleSystem.eMainStep.PostCommandExecute);
					}
				}
				break;
			case BattleSystem.eMainStep.CommandExecute:
			{
				BattleOrder.FrameData frameDataAt2 = this.m_Order.GetFrameDataAt(0);
				if (frameDataAt2.IsCharaType)
				{
					if (this.m_InputData.Result != BattleInputData.eResult.TogetherAttack)
					{
						this.ExecCommandOfTurnOwner();
					}
					else
					{
						this.ExecTogetherAttackOfTurnOwnerNotEffect();
					}
				}
				else
				{
					this.ExecSkillCard();
					this.m_Timer.SetTimeAndPlay(1.8f);
				}
				this.SetMainStep(BattleSystem.eMainStep.CommandExecute_Wait_0);
				break;
			}
			case BattleSystem.eMainStep.CommandExecute_Wait_0:
				this.RequestSort();
				if (this.m_Timer.Sec <= 0f)
				{
					if (this.IsCompleteExecCommandAnim())
					{
						this.PostAfterExecCommand(this.m_ExecCommand.SolveResult);
						this.SetMainStep(BattleSystem.eMainStep.CommandExecute_Wait_1);
					}
				}
				break;
			case BattleSystem.eMainStep.CommandExecute_Wait_1:
				this.RequestSort();
				if (this.IsCompletePostAfterExecCommand())
				{
					if (this.m_InputData.Result != BattleInputData.eResult.TogetherAttack)
					{
						this.m_Timer.SetTimeAndPlay(0.2f);
					}
					else
					{
						if (this.m_SkillActionPlayer != null)
						{
							this.m_SkillActionPlayer.Destroy();
						}
						this.m_Timer.SetTimeAndPlay(0.5f);
						for (int k = 0; k < 2; k++)
						{
							for (int l = 0; l < 6; l++)
							{
								CharacterHandler member2 = this.Parties[k].GetMember((BattleDefine.eJoinMember)l);
								if (member2 != null)
								{
									member2.CharaBattle.ChangeMeshColor(member2.CharaAnim.GetCurrentMeshColor(), Color.white, 0.18f);
								}
							}
						}
					}
					this.SetMainStep(BattleSystem.eMainStep.CommandExecute_Wait_2);
				}
				break;
			case BattleSystem.eMainStep.CommandExecute_Wait_2:
				if (this.m_Timer.Sec <= 0f)
				{
					this.SetMainStep(BattleSystem.eMainStep.PostCommandExecute);
				}
				break;
			case BattleSystem.eMainStep.PostCommandExecute:
				if (!this.m_BattleCamera.IsMoving)
				{
					bool flag8 = true;
					if (this.CanBeContinueTogetherAttack())
					{
						flag8 = false;
						this.SetMainStep(BattleSystem.eMainStep.TogetherAttackExecute_0);
					}
					else if (this.CalcAutoMemberChange())
					{
						flag8 = false;
						this.SetMainStep(BattleSystem.eMainStep.AutoMemberChangeExecute_0);
					}
					else if (this.CalcGoBackFriend())
					{
						flag8 = false;
						this.SetMainStep(BattleSystem.eMainStep.GoBackFriendExecute_0);
					}
					if (flag8)
					{
						this.SetMainStep(BattleSystem.eMainStep.StunExecute);
					}
				}
				break;
			case BattleSystem.eMainStep.StunExecute:
				if (this.StunCheckProcess())
				{
					this.SetMainStep(BattleSystem.eMainStep.StunExecute_Wait_0);
				}
				else
				{
					this.SetMainStep(BattleSystem.eMainStep.TurnPreEnd);
				}
				break;
			case BattleSystem.eMainStep.StunExecute_Wait_0:
				if (!this.m_BattleCamera.IsMoving)
				{
					this.StunOccurProcess();
					this.m_Timer.SetTimeAndPlay(0.10000001f);
					this.SetMainStep(BattleSystem.eMainStep.StunExecute_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.StunExecute_Wait_1:
				if (this.m_Timer.Sec <= 0f)
				{
					this.StunApplyProcess();
					this.m_Timer.SetTimeAndPlay(1.5f);
					this.SetMainStep(BattleSystem.eMainStep.StunExecute_Wait_2);
				}
				break;
			case BattleSystem.eMainStep.StunExecute_Wait_2:
				if (this.m_Timer.Sec <= 0f)
				{
					if (!this.m_BattleUI.GetOrder().IsSlide())
					{
						this.StunApplyPostProcess();
						this.SetMainStep(BattleSystem.eMainStep.StunExecute_Wait_3);
					}
				}
				break;
			case BattleSystem.eMainStep.StunExecute_Wait_3:
				if (!this.m_BattleCamera.IsMoving)
				{
					this.SetMainStep(BattleSystem.eMainStep.TurnPreEnd);
				}
				break;
			case BattleSystem.eMainStep.TurnPreEnd:
				this.m_BattleBG.ChangeColorFromCurrent(Color.white, 0.18f);
				for (int m = 0; m < 2; m++)
				{
					for (int n = 0; n < 6; n++)
					{
						CharacterHandler member3 = this.Parties[m].GetMember((BattleDefine.eJoinMember)n);
						if (member3 != null)
						{
							member3.CharaBattle.ChangeMeshColor(member3.CharaAnim.GetCurrentMeshColor(), Color.white, 0.18f);
						}
					}
				}
				if (this.CheckGameOver() || this.CheckClearAllWave())
				{
					this.SetMainStep(BattleSystem.eMainStep.TurnEnd);
				}
				else
				{
					SoundCueSheetDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM);
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(param2.m_CueSheet, this.m_BgmCueName))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(param2.m_CueSheet, this.m_BgmCueName, 1f, 0, -1, -1);
					}
					this.SetMainStep(BattleSystem.eMainStep.TurnPreEnd_Wait);
				}
				break;
			case BattleSystem.eMainStep.TurnPreEnd_Wait:
				if (this.m_Timer.Sec <= 0f)
				{
					bool flag9 = this.TurnEndPreProcess((BattleDefine.eTurnPreEndEvent)this.m_TurnPreEndStep);
					if (flag9)
					{
						this.m_Timer.SetTimeAndPlay(this.TURN_PRE_END_EVENT_WAIT_TIMES[this.m_TurnPreEndStep]);
					}
					this.m_TurnPreEndStep++;
					if (this.m_TurnPreEndStep >= 3)
					{
						this.SetMainStep(BattleSystem.eMainStep.TurnEnd);
					}
				}
				break;
			case BattleSystem.eMainStep.TurnEnd:
				this.TurnEndProcess();
				if (this.CheckGameOver())
				{
					this.SetMainStep(BattleSystem.eMainStep.GameOver);
				}
				else if (this.CheckClearAllWave())
				{
					this.SetMainStep(BattleSystem.eMainStep.BattleClear);
				}
				else if (this.CheckClearCurrentWave())
				{
					this.SetMainStep(BattleSystem.eMainStep.WaveOut);
				}
				else if (this.m_Order.GetTogetherData().IsAvailable())
				{
					this.m_BattleUI.PlayInTopUI();
					this.SetMainStep(BattleSystem.eMainStep.OrderCheck);
				}
				else
				{
					this.SetMainStep(BattleSystem.eMainStep.OrderSlide);
				}
				break;
			case BattleSystem.eMainStep.WaveOut:
				if (this.CheckTransitWaveOut())
				{
					this.m_BattleUI.PlayOut();
					this.m_Timer.SetTimeAndPlay(0.23f);
					this.SetMainStep(BattleSystem.eMainStep.WaveOut_Wait_0);
				}
				break;
			case BattleSystem.eMainStep.WaveOut_Wait_0:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_BattleCamera.ZoomStart(BattleCamera.eZoomType.WaveOut);
					this.SetMainStep(BattleSystem.eMainStep.WaveOut_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.WaveOut_Wait_1:
				if (!this.m_BattleCamera.IsMoving)
				{
					this.WaveOutPartyJoinOnlyFromCurrentPos();
					this.m_Timer.SetTimeAndPlay(0.7f);
					this.SetMainStep(BattleSystem.eMainStep.WaveOut_Wait_2);
				}
				break;
			case BattleSystem.eMainStep.WaveOut_Wait_2:
				this.RequestSort();
				if (this.m_Timer.Sec <= 0f)
				{
					if (this.IsCompleteWaveOutParty())
					{
						this.m_BattleUI.GetTransitFade().PlayIn(this.BSD.WaveIdx + 1, this.BSD.WaveNum);
						this.SetMainStep(BattleSystem.eMainStep.WaveOut_Wait_3);
					}
				}
				break;
			case BattleSystem.eMainStep.WaveOut_Wait_3:
				this.RequestSort();
				if (this.m_BattleUI.GetTransitFade().IsShowComplete())
				{
					this.m_BattleCamera.ResetDefault();
					this.EnemyParty.Destroy();
					this.BSD.WaveIdx++;
					return BattleSystem.eMode.Prepare;
				}
				break;
			case BattleSystem.eMainStep.BattleClear:
				this.m_Result.m_Status = BattleResult.eStatus.Clear;
				this.m_BattleCamera.ZoomStart(BattleCamera.eZoomType.ToPlayerOnBattleEnd);
				this.m_BattleUI.PlayOut();
				this.m_Timer.SetTimeAndPlay(1.1f);
				this.SetMainStep(BattleSystem.eMainStep.BattleClear_Wait_0);
				break;
			case BattleSystem.eMainStep.BattleClear_Wait_0:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_TitleCutIn.Play(BattleTitleCutIn.eType.Clear);
					for (int num2 = 0; num2 < 6; num2++)
					{
						CharacterHandler member4 = this.PlayerParty.GetMember((BattleDefine.eJoinMember)num2);
						if (member4 != null && !member4.CharaBattle.Param.IsDead())
						{
							member4.CharaBattle.RequestWinMode(false);
						}
					}
					if (this.m_LastBlowCharaHndl != null)
					{
						this.m_LastBlowCharaHndl.PlayVoice(eSoundVoiceListDB.voice_417, 1f);
					}
					else
					{
						this.PlayerParty.PlayVoice(eSoundVoiceListDB.voice_417);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(eSoundBgmListDB.BGM_BATTLE_WIN, 1f, 0, -1, -1);
					this.m_Timer.SetTimeAndPlay(3.5f);
					this.SetMainStep(BattleSystem.eMainStep.BattleClear_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.BattleClear_Wait_1:
				if (this.m_TitleCutIn.IsCompletePlay())
				{
					if (this.m_Timer.Sec <= 0f)
					{
						this.SetMainStep(BattleSystem.eMainStep.ToResult);
					}
				}
				break;
			case BattleSystem.eMainStep.GameOver:
				this.m_Result.m_Status = BattleResult.eStatus.GameOver;
				this.m_BattleCamera.ZoomStart(BattleCamera.eZoomType.ToEnemyOnBattleEnd);
				this.m_BattleUI.PlayOut();
				this.m_Timer.SetTimeAndPlay(1.1f);
				this.SetMainStep(BattleSystem.eMainStep.GameOver_Wait_0);
				break;
			case BattleSystem.eMainStep.GameOver_Wait_0:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_TitleCutIn.Play(BattleTitleCutIn.eType.Failed);
					if (!this.EnemyParty.PlayVoice("voice_419") && this.m_CharaHndlForGameOver != null)
					{
						this.m_CharaHndlForGameOver.PlayVoice(eSoundVoiceListDB.voice_419, 1f);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SuspendBGM();
					this.m_Timer.SetTimeAndPlay(3f);
					this.SetMainStep(BattleSystem.eMainStep.GameOver_Wait_1);
				}
				break;
			case BattleSystem.eMainStep.GameOver_Wait_1:
				if (this.m_TitleCutIn.IsCompletePlay())
				{
					if (this.m_Timer.Sec <= 0f)
					{
						if (this.m_QuestParam.m_IsLoserBattle == 1)
						{
							this.m_Result.m_Status = BattleResult.eStatus.Clear;
							this.m_Result.m_IsLoserClear = true;
							this.SetMainStep(BattleSystem.eMainStep.ToResult);
						}
						else
						{
							if (this.m_QuestParam.m_IsRetryLimit == 1)
							{
								this.SetMainStep(BattleSystem.eMainStep.None);
								return BattleSystem.eMode.BattleFinished;
							}
							this.SetMainStep(BattleSystem.eMainStep.RetrySelect);
						}
					}
				}
				break;
			case BattleSystem.eMainStep.RetrySelect:
				this.OpenRetryWindow();
				this.SetMainStep(BattleSystem.eMainStep.RetrySelect_Wait);
				break;
			case BattleSystem.eMainStep.RetryCancel:
				this.SetMainStep(BattleSystem.eMainStep.None);
				return BattleSystem.eMode.BattleFinished;
			case BattleSystem.eMainStep.RetryRequest:
			{
				bool flag10 = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestRetry(new Action<MeigewwwParam, Questlogretry>(this.OnResponse_QuestRetry));
				if (flag10)
				{
					this.SetMainStep(BattleSystem.eMainStep.RetryRequest_Wait);
				}
				break;
			}
			case BattleSystem.eMainStep.Retry:
				this.ExecRetry();
				this.SetMainStep(BattleSystem.eMainStep.Retry_Wait);
				break;
			case BattleSystem.eMainStep.Retry_Wait:
				if (this.IsDonePrepareOnRetry())
				{
					if (!this.m_BattleCamera.IsMoving)
					{
						this.SetMainStep(BattleSystem.eMainStep.WaveInCharacter);
					}
				}
				break;
			case BattleSystem.eMainStep.ToResult:
				this.SetMainStep(BattleSystem.eMainStep.ToResult_Wait);
				break;
			case BattleSystem.eMainStep.ToResult_Wait:
				if (this.m_BattleUI.IsMenuEnd())
				{
					this.SetMainStep(BattleSystem.eMainStep.None);
					return BattleSystem.eMode.BattleFinished;
				}
				break;
			case BattleSystem.eMainStep.Retire:
				this.m_Result.m_Status = BattleResult.eStatus.Retire;
				return BattleSystem.eMode.BattleFinished;
			case BattleSystem.eMainStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.SetMainStep(BattleSystem.eMainStep.PrepareError_Wait);
				break;
			}
			return BattleSystem.eMode.BattleMain;
		}

		// Token: 0x060006BA RID: 1722 RVA: 0x00027595 File Offset: 0x00025995
		private void LateUpdate_Main()
		{
			if (this.m_UniqueSkillScene != null)
			{
				this.m_UniqueSkillScene.LateUpdate();
			}
			this.UpdateSort();
		}

		// Token: 0x060006BB RID: 1723 RVA: 0x000275B4 File Offset: 0x000259B4
		private void OnResponse_QuestRetry(MeigewwwParam wwwParam, Questlogretry param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			switch (result)
			{
			case ResultCode.UNLIMITED_GEM_IS_SHORT:
			case ResultCode.GEM_IS_SHORT:
			{
				GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
				inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, inst.DbMng.GetTextMessage(eText_MessageDB.BattleRetryGemIsShortTitle), inst.DbMng.GetTextMessage(eText_MessageDB.BattleRetryGemIsShort), null, new Action<int>(this.OnConfirmPurchaseGem));
				break;
			}
			default:
				if (result != ResultCode.SUCCESS)
				{
					APIUtility.ShowErrorWindowRetry(wwwParam, null);
				}
				else
				{
					this.BSD.ContCount++;
					this.SetMainStep(BattleSystem.eMainStep.Retry);
				}
				break;
			}
		}

		// Token: 0x060006BC RID: 1724 RVA: 0x00027662 File Offset: 0x00025A62
		private void OnConfirmPurchaseGem(int answer)
		{
			if (answer == 0)
			{
				SingletonMonoBehaviour<GemShopPlayer>.Inst.Open(0, new Action(this.OpenRetryWindow));
			}
			else
			{
				this.OpenRetryWindow();
			}
		}

		// Token: 0x060006BD RID: 1725 RVA: 0x0002768C File Offset: 0x00025A8C
		private void OnOpenedTutorialTipsWindow()
		{
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			if (seq == BattleTutorial.eSeq._03_WAVE_IDX_0_SWIPE)
			{
				this.m_Tutorial.ApplyNextSeq();
			}
		}

		// Token: 0x060006BE RID: 1726 RVA: 0x000276C4 File Offset: 0x00025AC4
		private void OnCompleteTutorialTipsWindow()
		{
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			this.m_Tutorial.ApplyNextSeq();
			TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
			switch (seq)
			{
			case BattleTutorial.eSeq._00_WAVE_IDX_0_TIPS_START:
			{
				CommandButton commandButton = this.m_BattleUI.GetCommandButton(0);
				if (commandButton != null)
				{
					tutorialMessage.Open(eTutorialMessageListDB.BTL_ATTACK);
					tutorialMessage.SetEnableArrow(true);
					tutorialMessage.SetArrowPosition(commandButton.CacheTransform.position);
					tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
					UIUtility.AttachCanvasForChangeRenderOrder(commandButton.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
				}
				break;
			}
			case BattleTutorial.eSeq._02_WAVE_IDX_0_TIPS_COMMAND:
			{
				tutorialMessage.Open(eTutorialMessageListDB.BTL_CMD_SWIPE);
				int[] needCommandBtnIndexHistory = this.m_Tutorial.NeedCommandBtnIndexHistory;
				CommandButton commandButton2;
				for (int i = 0; i < needCommandBtnIndexHistory.Length; i++)
				{
					commandButton2 = this.m_BattleUI.GetCommandButton(needCommandBtnIndexHistory[i]);
					if (commandButton2 != null)
					{
						TutorialTarget tutorialTarget = commandButton2.gameObject.AddComponent<TutorialTarget>();
						if (tutorialTarget != null)
						{
							tutorialTarget.SetEnable(true);
						}
					}
				}
				commandButton2 = this.m_BattleUI.GetCommandButton(2);
				Vector2 startPos = (!commandButton2) ? Vector2.zero : new Vector2(commandButton2.CacheTransform.position.x, commandButton2.CacheTransform.position.y);
				commandButton2 = this.m_BattleUI.GetCommandButton(0);
				Vector2 endPos = (!commandButton2) ? Vector2.zero : new Vector2(commandButton2.CacheTransform.position.x, commandButton2.CacheTransform.position.y);
				tutorialMessage.SetEnableTrace(true);
				tutorialMessage.SetTracePosition(startPos, endPos);
				break;
			}
			case BattleTutorial.eSeq._04_WAVE_IDX_0_TIPS_ELEMENT:
			{
				this.SelectCommandButton(0, true);
				tutorialMessage.Open(eTutorialMessageListDB.BTL_ENEMY_TAP);
				tutorialMessage.SetEnableDark(false, 1f, true);
				CharacterHandler owner = this.m_Order.GetFrameDataAt(0).m_Owner;
				BattleDefine.eJoinMember singleTargetIndex = owner.CharaBattle.MyParty.SingleTargetIndex;
				BattleDefine.eJoinMember eJoinMember = BattleDefine.eJoinMember.None;
				float sec = 0.3f;
				Color endColor = new Color(0.25f, 0.25f, 0.25f);
				for (BattleDefine.eJoinMember eJoinMember2 = BattleDefine.eJoinMember.Join_1; eJoinMember2 <= BattleDefine.eJoinMember.Join_3; eJoinMember2++)
				{
					CharacterHandler member = this.EnemyParty.GetMember(eJoinMember2);
					if (member != null)
					{
						if (singleTargetIndex != eJoinMember2 && eJoinMember == BattleDefine.eJoinMember.None)
						{
							eJoinMember = eJoinMember2;
							Transform transform;
							float num;
							member.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out transform, out num);
							tutorialMessage.SetEnableArrow(true);
							tutorialMessage.SetArrowWorldPosition(transform.position);
						}
						if (eJoinMember != eJoinMember2)
						{
							member.CharaBattle.ChangeMeshColor(Color.white, endColor, sec);
						}
					}
					member = this.PlayerParty.GetMember(eJoinMember2);
					if (member != null)
					{
						member.CharaBattle.ChangeMeshColor(Color.white, endColor, sec);
					}
				}
				this.m_BattleBG.ChangeColor(Color.white, endColor, sec);
				this.m_Tutorial.SetSingleTargetIndex(eJoinMember);
				this.m_BattleUI.SetEnableInput(false);
				break;
			}
			case BattleTutorial.eSeq._07_WAVE_IDX_1_TIPS_START:
				tutorialMessage.Open(eTutorialMessageListDB.BTL_ILLUST_SLIDE);
				tutorialMessage.SetEnableTrace(true);
				tutorialMessage.SetTracePositionOffset(this.m_BattleUI.GetOwnerImageObject(), new Vector2(-120f, 64f), new Vector2(120f, -64f));
				UIUtility.AttachCanvasForChangeRenderOrder(this.m_BattleUI.GetOwnerImageObject(), tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
				this.m_BattleUI.SetEnableInputOwnerImage(true);
				break;
			case BattleTutorial.eSeq._09_WAVE_IDX_1_TIPS_CHARGESTUN:
			{
				this.m_BattleUI.SetEnableInput(true);
				CommandButton commandButton3 = this.m_BattleUI.GetCommandButton(0);
				if (commandButton3 != null)
				{
					tutorialMessage.Open(eTutorialMessageListDB.BTL_STUN);
					tutorialMessage.SetEnableArrow(true);
					tutorialMessage.SetArrowPosition(commandButton3.CacheTransform.position);
					tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
					UIUtility.AttachCanvasForChangeRenderOrder(commandButton3.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
				}
				break;
			}
			case BattleTutorial.eSeq._11_WAVE_IDX_1_TIPS_UNIQUE_SKILL:
			{
				this.CalcTogetherAttackGauge(float.MaxValue);
				this.m_Order.GetTogetherData().SetExecutable(true);
				TogetherButton togetherButton = this.m_BattleUI.GetTogetherButton();
				togetherButton.Apply(this.m_Order.GetGauge().GetValue(), false);
				togetherButton.SetExecutableTogetherAttack(true);
				tutorialMessage.Open(eTutorialMessageListDB.BTL_UNIQUESKILL);
				tutorialMessage.SetEnableArrow(true);
				tutorialMessage.SetArrowPosition(togetherButton.transform.position);
				tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				UIUtility.AttachCanvasForChangeRenderOrder(togetherButton.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
				break;
			}
			}
		}

		// Token: 0x060006BF RID: 1727 RVA: 0x00027B70 File Offset: 0x00025F70
		private void SetPrepareStep(BattleSystem.ePrepareStep step)
		{
			this.m_PrepareStep = step;
		}

		// Token: 0x060006C0 RID: 1728 RVA: 0x00027B7C File Offset: 0x00025F7C
		private BattleSystem.eMode Update_Prepare()
		{
			if (this.m_BgLoader != null)
			{
				this.m_BgLoader.Update();
			}
			switch (this.m_PrepareStep)
			{
			case BattleSystem.ePrepareStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					GC.Collect();
					this.m_Parties[1] = new BattlePartyData(BattleDefine.ePartyType.Enemy, null);
					if (!this.m_IsFirstPrepared)
					{
						this.m_IsFirstPrepared = true;
						SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(0f);
						if (this.m_DropObjectManager != null)
						{
							this.m_DropObjectManager.Setup(this.m_ObjRoot);
						}
						this.m_Parties[0] = new BattlePartyData(BattleDefine.ePartyType.Player, this.BSD.PLJoinIdxs);
						this.m_Option.Setup(this.m_Scaler, LocalSaveData.Inst);
						this.m_MasterOrbData = new BattleMasterOrbData();
						UserMasterOrbData equipUserMasterOrbData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetEquipUserMasterOrbData();
						if (equipUserMasterOrbData != null)
						{
							this.m_MasterOrbData.Setup(equipUserMasterOrbData.OrbID, equipUserMasterOrbData.Lv);
						}
						if (!this.m_Tutorial.IsSeq(BattleTutorial.eSeq.None))
						{
							this.m_Option.SetSpeedLv(LocalSaveData.eBattleSpeedLv.UniqueSkill, 0, false);
							this.m_Option.SetSpeedLv(LocalSaveData.eBattleSpeedLv.EN, 0, false);
							this.m_Option.SetSpeedLv(LocalSaveData.eBattleSpeedLv.PL, 0, false);
							this.m_Option.SetIsAuto(false);
							this.m_Option.SetIsSkipTogetherAttackInput(true);
							this.m_MasterOrbData.Setup(1, 1);
						}
						this.m_PrepareStatus_Sound = 0;
						this.m_IsNeedPrepares[0] = true;
						this.m_IsNeedPrepares[1] = true;
						this.m_IsNeedPrepares[2] = true;
						this.m_IsNeedPrepares[3] = true;
						this.m_IsNeedPrepares[5] = true;
						this.m_IsNeedPrepares[4] = true;
						this.m_IsNeedPrepares[6] = true;
						this.m_IsNeedPrepares[7] = true;
						this.StartPrepare_Sound();
						this.StartPrepare_BG();
						this.StartPrepare_AlwaysEffect();
						this.StartPrepare_Player();
						this.StartPrepare_Enemy();
					}
					else
					{
						for (int i = 0; i < 6; i++)
						{
							CharacterHandler member = this.PlayerParty.GetMember((BattleDefine.eJoinMember)i);
							if (member != null)
							{
								member.CharaBattle.TgtParty = this.EnemyParty;
							}
						}
						this.m_PrepareStatus_Sound = 1;
						this.m_IsNeedPrepares[0] = true;
						this.m_IsNeedPrepares[1] = false;
						this.m_IsNeedPrepares[2] = false;
						this.m_IsNeedPrepares[3] = false;
						this.m_IsNeedPrepares[5] = true;
						this.m_IsNeedPrepares[4] = false;
						this.m_IsNeedPrepares[6] = true;
						this.m_IsNeedPrepares[7] = false;
						this.StartPrepare_Sound();
						this.StartPrepare_Enemy();
					}
					this.SetPrepareStep(BattleSystem.ePrepareStep.Prepare_0);
				}
				break;
			case BattleSystem.ePrepareStep.Prepare_0:
			{
				bool flag = true;
				flag &= this.UpdatePrepare_Sound();
				flag &= this.UpdatePrepare_BG();
				flag &= this.UpdatePrepare_AlwaysEffect();
				flag &= this.UpdatePrepare_Player();
				flag &= this.UpdatePrepare_Enemy();
				if (flag)
				{
					this.StartPrepare_PlayerEffect();
					this.StartPrepare_EnemyEffect();
					this.StartPrepare_UI();
					this.SetPrepareStep(BattleSystem.ePrepareStep.Prepare_1);
				}
				break;
			}
			case BattleSystem.ePrepareStep.Prepare_1:
			{
				bool flag2 = true;
				flag2 &= this.UpdatePrepare_PlayerEffect();
				flag2 &= this.UpdatePrepare_EnemyEffect();
				flag2 &= this.UpdatePrepare_UI();
				if (flag2)
				{
					this.SetPrepareStep(BattleSystem.ePrepareStep.End);
				}
				break;
			}
			case BattleSystem.ePrepareStep.End:
				BattleUtility.SetupCharaNameOnBattle(SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.GetCharas());
				this.InitPlacementParty(true);
				this.SetupWaveData(this.BSD.WaveIdx);
				this.SetupBattleUIOnWaveStart();
				if (this.m_Result != null && this.m_BattleCamera != null && this.BSD.WaveIdx == 0 && this.BSD.WaveNum > 1 && this.BSD.ContCount == 0)
				{
					this.m_BattleCamera.SetZoom(BattleCamera.eZoomType.QuestStart);
				}
				this.RequestSaveOnWaveStart();
				return BattleSystem.eMode.BattleMain;
			case BattleSystem.ePrepareStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.SetPrepareStep(BattleSystem.ePrepareStep.PrepareError_Wait);
				break;
			}
			return BattleSystem.eMode.Prepare;
		}

		// Token: 0x060006C1 RID: 1729 RVA: 0x00027F70 File Offset: 0x00026370
		private bool StartPrepare_Sound()
		{
			int num = 0;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			if (this.m_PrepareStatus_Sound == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Battle);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.BGM_uniqueSkill);
			}
			return true;
		}

		// Token: 0x060006C2 RID: 1730 RVA: 0x00027FBC File Offset: 0x000263BC
		private bool UpdatePrepare_Sound()
		{
			int num = 0;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			int prepareStatus_Sound = this.m_PrepareStatus_Sound;
			if (prepareStatus_Sound == 0)
			{
				SoundDefine.eLoadStatus cueSheetLoadStatus = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetCueSheetLoadStatus(eSoundCueSheetDB.Battle);
				SoundDefine.eLoadStatus cueSheetLoadStatus2 = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetCueSheetLoadStatus(eSoundCueSheetDB.BGM_uniqueSkill);
				if (cueSheetLoadStatus != SoundDefine.eLoadStatus.Loading && cueSheetLoadStatus2 != SoundDefine.eLoadStatus.Loading)
				{
					this.m_PrepareStatus_Sound = 1;
				}
				return false;
			}
			if (prepareStatus_Sound != 1)
			{
				if (prepareStatus_Sound == 2)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsCompletePrepareBGM())
					{
						this.m_PrepareStatus_Sound = 3;
					}
				}
				this.m_IsNeedPrepares[num] = false;
				return true;
			}
			if (!this.IsWarningWave())
			{
				this.m_BgmCueName = this.m_QuestParam.m_BGMCueName;
				if (string.IsNullOrEmpty(this.m_BgmCueName))
				{
					this.m_BgmCueName = "bgm_battle_1";
				}
			}
			else
			{
				this.m_BgmCueName = this.m_QuestParam.m_LastWaveBGMCueName;
				if (string.IsNullOrEmpty(this.m_BgmCueName))
				{
					this.m_BgmCueName = "bgm_battle_2";
				}
			}
			SoundCueSheetDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM);
			bool flag = false;
			if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(param.m_CueSheet, this.m_BgmCueName))
			{
				flag = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PrepareBGM(param.m_CueSheet, this.m_BgmCueName, 1f, 0, -1, -1);
			}
			if (!flag)
			{
				this.m_IsNeedPrepares[num] = false;
				return true;
			}
			this.m_PrepareStatus_Sound = 2;
			return false;
		}

		// Token: 0x060006C3 RID: 1731 RVA: 0x00028144 File Offset: 0x00026544
		private bool StartPrepare_BG()
		{
			int num = 1;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			this.m_BgLoader = new ABResourceLoader(-1);
			int bgID = this.m_QuestParam.m_BgID;
			this.m_BgResourceObjHndl = this.m_BgLoader.Load(BattleUtility.GetBattleBgResourcePath(bgID), new MeigeResource.Option[0]);
			return true;
		}

		// Token: 0x060006C4 RID: 1732 RVA: 0x00028198 File Offset: 0x00026598
		private bool UpdatePrepare_BG()
		{
			int num = 1;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			if (this.m_BgResourceObjHndl.IsError())
			{
				this.m_BgResourceObjHndl.Retry();
				return false;
			}
			if (!this.m_BgResourceObjHndl.IsDone())
			{
				return false;
			}
			GameObject original = this.m_BgResourceObjHndl.Objs[0] as GameObject;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(original, BattleDefine.BG_POS, Quaternion.Euler(0f, 180f, 0f));
			this.m_BattleBG = gameObject.GetComponent<BattleBG>();
			if (this.m_BattleBG == null)
			{
				this.m_BattleBG = gameObject.AddComponent<BattleBG>();
			}
			if (this.m_BgResourceObjHndl != null)
			{
				this.m_BgLoader.UnloadAll(false);
				this.m_BgResourceObjHndl = null;
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006C5 RID: 1733 RVA: 0x0002826C File Offset: 0x0002666C
		private bool StartPrepare_AlwaysEffect()
		{
			int num = 2;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadPack("btl_always"))
			{
				this.m_IsNeedPrepares[num] = false;
				return false;
			}
			return true;
		}

		// Token: 0x060006C6 RID: 1734 RVA: 0x000282B4 File Offset: 0x000266B4
		private bool UpdatePrepare_AlwaysEffect()
		{
			int num = 2;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
			{
				return false;
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006C7 RID: 1735 RVA: 0x000282F4 File Offset: 0x000266F4
		private bool StartPrepare_Player()
		{
			int num = 3;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			List<CharacterHandler> list = this.m_PrepareCharaHndls[0];
			list.Clear();
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			this.PlayerParty.PartyMngID = -1L;
			UserBattlePartyData userBattlePartyData;
			if (this.BSD.PtyMngID == -1L)
			{
				userBattlePartyData = userDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex];
			}
			else
			{
				userBattlePartyData = userDataMng.GetUserBattlePartyData(this.BSD.PtyMngID);
			}
			int num2 = 5;
			for (int i = 0; i < num2; i++)
			{
				bool flag = false;
				CharacterHandler characterHandler = null;
				CharacterParam characterParam = null;
				NamedParam namedParam = null;
				WeaponParam weaponParam = null;
				int resourceID = -1;
				this.PlayerParty.PartyMngID = userBattlePartyData.MngID;
				long memberAt = userBattlePartyData.GetMemberAt(i);
				long weaponAt = userBattlePartyData.GetWeaponAt(i);
				if (memberAt != -1L)
				{
					UserCharacterData userCharaData = userDataMng.GetUserCharaData(memberAt);
					if (userCharaData != null)
					{
						flag = true;
						characterParam = userCharaData.Param;
						CharacterListDB_Param param = dbMng.CharaListDB.GetParam(characterParam.CharaID);
						namedParam = null;
						UserNamedData userNamedData = userDataMng.GetUserNamedData((eCharaNamedType)param.m_NamedType);
						if (userNamedData != null)
						{
							namedParam = new NamedParam();
							CharacterUtility.SetupNamedParam(namedParam, userNamedData.FriendShip, (eCharaNamedType)param.m_NamedType);
						}
						UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponAt);
						if (userWpnData != null)
						{
							weaponParam = userWpnData.Param;
						}
						else
						{
							weaponParam = new WeaponParam();
							CharacterUtility.SetupDefaultWeaponParam(weaponParam, characterParam.ClassType);
						}
						resourceID = param.m_ResourceID;
					}
				}
				if (flag)
				{
					characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, namedParam, weaponParam, eCharaResourceType.Player, resourceID, CharacterDefine.eMode.Battle, true, CharacterDefine.eFriendType.None, "P" + i, this.m_ObjRoot);
				}
				if (characterHandler != null)
				{
					bool flag2 = false;
					BattleDefine.eJoinMember eJoinMember = this.PlayerParty.SlotIndexToJoin(i);
					if (eJoinMember >= BattleDefine.eJoinMember.Join_1 && eJoinMember <= BattleDefine.eJoinMember.Join_3)
					{
						flag2 = true;
					}
					if (flag2)
					{
						characterHandler.Prepare();
					}
					else
					{
						characterHandler.PrepareWithoutResourceSetup(true);
					}
				}
				list.Add(characterHandler);
			}
			CharacterHandler item = null;
			if (this.m_QuestParam.m_CpuFriendCharaID != -1)
			{
				item = this.InstantiateFriend(CharacterDefine.eFriendType.Registered, this.m_QuestParam.m_CpuFriendCharaID, this.m_QuestParam.m_CpuFriendCharaLv, this.m_QuestParam.m_CpuFriendLimitBreak, this.m_QuestParam.m_CpuFriendSkillLv, new int[]
				{
					this.m_QuestParam.m_CpuFriendSkillLv,
					this.m_QuestParam.m_CpuFriendSkillLv
				}, this.m_QuestParam.m_CpuFriendWeaponID, this.m_QuestParam.m_CpuFriendWeaponLv, this.m_QuestParam.m_CpuFriendSkillLv, 1);
			}
			else if (this.BSD.FrdCharaID != -1)
			{
				item = this.InstantiateFriend((CharacterDefine.eFriendType)this.BSD.FrdType, this.BSD.FrdCharaID, this.BSD.FrdCharaLv, this.BSD.FrdLB, this.BSD.FrdUSLv, this.BSD.FrdCSLvs, this.BSD.FrdWpID, this.BSD.FrdWpLv, this.BSD.FrdWpSLv, this.BSD.Frdship);
			}
			list.Add(item);
			return true;
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x00028650 File Offset: 0x00026A50
		private bool UpdatePrepare_Player()
		{
			int num = 3;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			List<CharacterHandler> list = this.m_PrepareCharaHndls[0];
			bool flag = false;
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i] != null && list[i].IsPreparing())
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				return false;
			}
			int num2 = 6;
			for (int j = 0; j < list.Count; j++)
			{
				this.PlayerParty.SetMemberAt(j, list[j]);
			}
			for (int k = 0; k < num2; k++)
			{
				this.PlayerParty.SetJoinMemberIndex((BattleDefine.eJoinMember)k, this.BSD.PLJoinIdxs[k]);
			}
			for (int l = 0; l < num2; l++)
			{
				BattleDefine.eJoinMember join = this.PlayerParty.SlotIndexToJoin(l);
				CharacterHandler member = this.PlayerParty.GetMember(join);
				if (member != null)
				{
					eTitleType titleType = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(member.CharaParam.NamedType);
					float[] townBuffParams = null;
					if (this.BSD.TBuff != null)
					{
						townBuffParams = this.BSD.TBuff.GetBuffParamArray(titleType, member.CharaParam.ClassType, member.CharaParam.ElementType);
					}
					member.CharaBattle.System = this;
					member.CharaBattle.MyParty = this.PlayerParty;
					member.CharaBattle.TgtParty = this.EnemyParty;
					member.CharaBattle.OnEndDeadAnimation += this.OnEndDeadAnimation;
					member.CharaBattle.SetupParams(this.BSD.PLs[l], townBuffParams);
					member.CharaAnim.ChangeDir(CharacterDefine.eDir.L, false);
				}
			}
			this.PrecedeInitializeBattleUI();
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x00028850 File Offset: 0x00026C50
		private bool StartPrepare_PlayerEffect()
		{
			int num = 4;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			this.PlayerParty.PrepareEffect();
			return true;
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x0002887C File Offset: 0x00026C7C
		private bool UpdatePrepare_PlayerEffect()
		{
			int num = 4;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			if (this.PlayerParty.IsPreparingEffect())
			{
				return false;
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x000288B8 File Offset: 0x00026CB8
		private bool StartPrepare_Enemy()
		{
			int num = 5;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			List<CharacterHandler> list = this.m_PrepareCharaHndls[1];
			list.Clear();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.EnemyParty.PartyMngID = -1L;
			List<BattleOccurEnemy.OneWave> waves = this.BSD.Enemies.GetWaves();
			BattleOccurEnemy.OneWave oneWave = waves[this.BSD.WaveIdx];
			int num2 = 6;
			for (int i = 0; i < num2; i++)
			{
				CharacterHandler characterHandler = null;
				if (i < oneWave.m_Enemies.Count && oneWave.m_Enemies[i].m_EnemyID != -1)
				{
					CharacterParam characterParam = new CharacterParam();
					QuestEnemyListDB_Param param = dbMng.QuestEnemyListDB.GetParam(oneWave.m_Enemies[i].m_EnemyID);
					CharacterUtility.SetupCharacterParamFromQuestEnemyParam(characterParam, ref param, oneWave.m_Enemies[i].m_EnemyLv);
					characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, null, new WeaponParam(), eCharaResourceType.Enemy, param.m_ResourceID, CharacterDefine.eMode.Battle, false, CharacterDefine.eFriendType.None, "E" + i, this.m_ObjRoot);
				}
				if (characterHandler != null)
				{
					characterHandler.Prepare();
				}
				list.Add(characterHandler);
			}
			return true;
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x00028A08 File Offset: 0x00026E08
		private bool UpdatePrepare_Enemy()
		{
			int num = 5;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			List<CharacterHandler> list = this.m_PrepareCharaHndls[1];
			bool flag = false;
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i] != null && list[i].IsPreparing())
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				return false;
			}
			int num2 = 6;
			for (int j = 0; j < list.Count; j++)
			{
				this.EnemyParty.SetMemberAt(j, list[j]);
			}
			for (int k = 0; k < num2; k++)
			{
				this.EnemyParty.SetJoinMemberIndex((BattleDefine.eJoinMember)k, k);
			}
			for (int l = 0; l < num2; l++)
			{
				CharacterHandler member = this.EnemyParty.GetMember((BattleDefine.eJoinMember)l);
				if (member != null)
				{
					member.CharaBattle.System = this;
					member.CharaBattle.MyParty = this.EnemyParty;
					member.CharaBattle.TgtParty = this.PlayerParty;
					member.CharaBattle.OnEndDeadAnimation += this.OnEndDeadAnimation;
					member.CharaBattle.SetupParams(null);
					member.CharaAnim.ChangeDir(CharacterDefine.eDir.R, false);
				}
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x00028B74 File Offset: 0x00026F74
		private bool StartPrepare_EnemyEffect()
		{
			int num = 6;
			if (!this.m_IsNeedPrepares[num])
			{
				return false;
			}
			this.EnemyParty.PrepareEffect();
			return true;
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x00028BA0 File Offset: 0x00026FA0
		private bool UpdatePrepare_EnemyEffect()
		{
			int num = 6;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			if (this.EnemyParty.IsPreparingEffect())
			{
				return false;
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x00028BDC File Offset: 0x00026FDC
		private bool StartPrepare_UI()
		{
			int num = 7;
			return this.m_IsNeedPrepares[num];
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x00028BFC File Offset: 0x00026FFC
		private bool UpdatePrepare_UI()
		{
			int num = 7;
			if (!this.m_IsNeedPrepares[num])
			{
				return true;
			}
			if (!this.InitializeBattleUI())
			{
				return false;
			}
			this.m_IsNeedPrepares[num] = false;
			return true;
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x00028C34 File Offset: 0x00027034
		private CharacterHandler InstantiateFriend(CharacterDefine.eFriendType friendType, int charaID, int charaLv, int limitBreak, int uniqueSkillLv, int[] classSkillLvs, int weaponID, int weaponLv, int weaponSkillLv, int friendShip)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			CharacterParam characterParam = new CharacterParam();
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			characterParam.UniqueSkillLearnData = new SkillLearnData(param.m_CharaSkillID, uniqueSkillLv, dbMng.SkillExpDB.GetMaxLv(param.m_CharaSkillExpTableID), 0L, param.m_CharaSkillExpTableID, true);
			characterParam.ClassSkillLearnDatas = new List<SkillLearnData>();
			for (int i = 0; i < param.m_ClassSkillIDs.Length; i++)
			{
				SkillLearnData item = new SkillLearnData(param.m_ClassSkillIDs[i], classSkillLvs[i], dbMng.SkillExpDB.GetMaxLv(param.m_ClassSkillExpTableIDs[i]), 0L, param.m_ClassSkillExpTableIDs[i], true);
				characterParam.ClassSkillLearnDatas.Add(item);
			}
			int maxLv = EditUtility.CalcMaxLvFromLimitBreak(charaID, limitBreak);
			CharacterUtility.SetupCharacterParam(characterParam, -1L, charaID, charaLv, maxLv, 0L, limitBreak);
			NamedParam namedParam = new NamedParam();
			CharacterUtility.SetupNamedParam(namedParam, friendShip, (eCharaNamedType)param.m_NamedType);
			WeaponParam weaponParam = null;
			if (weaponID != -1)
			{
				weaponParam = new WeaponParam();
				CharacterUtility.SetupWeaponParam(weaponParam, -1L, weaponID, weaponLv, 0L, weaponSkillLv, 0L);
			}
			CharacterHandler characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, namedParam, weaponParam, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Battle, true, friendType, "PF", this.m_ObjRoot);
			if (characterHandler != null)
			{
				bool flag = false;
				BattleDefine.eJoinMember eJoinMember = this.PlayerParty.SlotIndexToJoin(5);
				if (eJoinMember >= BattleDefine.eJoinMember.Join_1 && eJoinMember <= BattleDefine.eJoinMember.Join_3)
				{
					flag = true;
				}
				if (flag)
				{
					characterHandler.Prepare();
				}
				else
				{
					characterHandler.PrepareWithoutResourceSetup(true);
				}
				this.m_InstaniatedFriendCharaID = charaID;
			}
			return characterHandler;
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x00028DCC File Offset: 0x000271CC
		[Conditional("APP_DEBUG")]
		private void StartStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x00028DCE File Offset: 0x000271CE
		[Conditional("APP_DEBUG")]
		private void StartStopwatchForDebugPrepare(BattleSystem.ePrepareTarget prepareTarget)
		{
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x00028DD0 File Offset: 0x000271D0
		[Conditional("APP_DEBUG")]
		private void StopStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x060006D5 RID: 1749 RVA: 0x00028DD2 File Offset: 0x000271D2
		[Conditional("APP_DEBUG")]
		private void StopStopwatchForDebugPrepare(BattleSystem.ePrepareTarget prepareTarget)
		{
		}

		// Token: 0x040004DB RID: 1243
		[SerializeField]
		private BattlePosLocator m_PosLocator_Player;

		// Token: 0x040004DC RID: 1244
		[SerializeField]
		private BattlePosLocator[] m_PosLocator_Enemy;

		// Token: 0x040004DD RID: 1245
		[SerializeField]
		private Transform m_CenterLocator_Player;

		// Token: 0x040004DE RID: 1246
		[SerializeField]
		private Transform m_CenterLocator_Enemy;

		// Token: 0x040004DF RID: 1247
		[SerializeField]
		private BattleFriendCutIn m_FriendCutIn;

		// Token: 0x040004E0 RID: 1248
		[SerializeField]
		private BattleTitleCutIn m_TitleCutIn;

		// Token: 0x040004E1 RID: 1249
		[SerializeField]
		private BattleUniqueSkillCutIn m_UniqueSkillCutIn;

		// Token: 0x040004E2 RID: 1250
		[SerializeField]
		private Camera m_ResultCamera;

		// Token: 0x040004E3 RID: 1251
		private BattleSystemData m_BSD;

		// Token: 0x040004E4 RID: 1252
		private BattlePartyData[] m_Parties = new BattlePartyData[2];

		// Token: 0x040004E5 RID: 1253
		private bool m_IsAwaked;

		// Token: 0x040004E6 RID: 1254
		private Transform m_ObjRoot;

		// Token: 0x040004E7 RID: 1255
		private BattleBG m_BattleBG;

		// Token: 0x040004E8 RID: 1256
		private Camera m_MainCamera;

		// Token: 0x040004E9 RID: 1257
		private BattleCamera m_BattleCamera;

		// Token: 0x040004EA RID: 1258
		private Transform m_MainCameraTransform;

		// Token: 0x040004EB RID: 1259
		private SkillActionPlayer m_SkillActionPlayer;

		// Token: 0x040004EC RID: 1260
		private SimpleTimer m_Timer = new SimpleTimer(0f);

		// Token: 0x040004ED RID: 1261
		private string m_BgmCueName;

		// Token: 0x040004EE RID: 1262
		private BattleOrder m_Order;

		// Token: 0x040004EF RID: 1263
		private BattleResult m_Result;

		// Token: 0x040004F0 RID: 1264
		private QuestListDB_Param m_QuestParam;

		// Token: 0x040004F1 RID: 1265
		private BattleInputData m_InputData = new BattleInputData();

		// Token: 0x040004F2 RID: 1266
		private BattleTutorial m_Tutorial = new BattleTutorial();

		// Token: 0x040004F3 RID: 1267
		private BattleOption m_Option = new BattleOption();

		// Token: 0x040004F4 RID: 1268
		private BattleTimeScaler m_Scaler = new BattleTimeScaler();

		// Token: 0x040004F5 RID: 1269
		private BattleMasterOrbData m_MasterOrbData;

		// Token: 0x040004F6 RID: 1270
		private BattleCommandData m_ExecCommand;

		// Token: 0x040004F7 RID: 1271
		private BattleDropObjectManager m_DropObjectManager;

		// Token: 0x040004F8 RID: 1272
		private Dictionary<CharacterHandler, BattleDropItem> m_DropItemsFromChara = new Dictionary<CharacterHandler, BattleDropItem>();

		// Token: 0x040004F9 RID: 1273
		private BattleKiraraJumpScene m_KiraraJumpScene;

		// Token: 0x040004FA RID: 1274
		private BattleUniqueSkillScene m_UniqueSkillScene;

		// Token: 0x040004FB RID: 1275
		private List<string> m_UniqueSkillSceneUnloadCueSheetNames;

		// Token: 0x040004FC RID: 1276
		private CharacterHandler m_LastBlowCharaHndl;

		// Token: 0x040004FD RID: 1277
		private bool m_IsRequestLastBlow;

		// Token: 0x040004FE RID: 1278
		private bool m_PlayedWarningVoice;

		// Token: 0x040004FF RID: 1279
		private bool m_ExecutedMemberChange;

		// Token: 0x04000500 RID: 1280
		private bool m_IsGoBackFriendCheck;

		// Token: 0x04000501 RID: 1281
		private bool m_IsEnableTouchChangeTarget;

		// Token: 0x04000502 RID: 1282
		private bool m_IsAutoTogetherAttack;

		// Token: 0x04000503 RID: 1283
		private bool m_IsAfterOrderSlide;

		// Token: 0x04000504 RID: 1284
		private int m_InstaniatedFriendCharaID = -1;

		// Token: 0x04000505 RID: 1285
		private List<BattleSystem.AutoMemberChangeData> m_AutoMemberChangeDatas = new List<BattleSystem.AutoMemberChangeData>();

		// Token: 0x04000506 RID: 1286
		private List<BattleSystem.DeadCharaData> m_DeadPlayerDatas = new List<BattleSystem.DeadCharaData>();

		// Token: 0x04000507 RID: 1287
		private CharacterHandler m_CharaHndlForGameOver;

		// Token: 0x04000508 RID: 1288
		private List<CharacterHandler> m_StunCharaHndls;

		// Token: 0x04000509 RID: 1289
		private BattleSystem.eMode m_Mode = BattleSystem.eMode.None;

		// Token: 0x0400050A RID: 1290
		private BattleSystem.eInterruptFriendOccurredSituation m_InterruptFriendOccurredSituation = BattleSystem.eInterruptFriendOccurredSituation.None;

		// Token: 0x0400050B RID: 1291
		private List<BattleSystem.SortData> m_SortDatas = new List<BattleSystem.SortData>();

		// Token: 0x0400050C RID: 1292
		private bool m_IsRequestSort;

		// Token: 0x0400050D RID: 1293
		[SerializeField]
		private BattleUI m_BattleUI;

		// Token: 0x0400050E RID: 1294
		[SerializeField]
		private Vector3 uiTurnOwnerMarkerPosOffset = new Vector3(0f, 1.3f, 0f);

		// Token: 0x0400050F RID: 1295
		private bool m_IsDestroyOnlyUI;

		// Token: 0x04000510 RID: 1296
		private BattleSystem.eDestroyStep m_DestroyStep;

		// Token: 0x04000511 RID: 1297
		private const float NOTICE_WAIT_TIME = 1.8f;

		// Token: 0x04000512 RID: 1298
		private const float AFTER_ORDER_SLIDE_WAIT_TIME = 0.3f;

		// Token: 0x04000513 RID: 1299
		private const float POST_SLILL_CARD_EXE_WAIT_TIME = 1.8f;

		// Token: 0x04000514 RID: 1300
		private const float POST_COMMAND_EXE_WAIT_TIME = 0.2f;

		// Token: 0x04000515 RID: 1301
		private const float POST_COMMAND_EXE_WAIT_TIME_TOGETHER_ATTACK_NOT_EFFECT = 0.5f;

		// Token: 0x04000516 RID: 1302
		private const float TOGETHER_ATTACK_NOT_EFFECT_WAIT_TIME = 1.5f;

		// Token: 0x04000517 RID: 1303
		private const float POST_TOGETHER_ATTACK_OF_TURN_OWNER_WAIT_TIME = 2f;

		// Token: 0x04000518 RID: 1304
		private const float POST_TOGETHER_ATTACK_OF_TURN_OWNER_WAIT_TIME_NOHIT = 0.8f;

		// Token: 0x04000519 RID: 1305
		private const float BATTLE_START_WAVEIN_WAIT_TIME = 0.65f;

		// Token: 0x0400051A RID: 1306
		private const float WAVEOUT_PRE_ZOOM_WAIT_TIME = 0.23f;

		// Token: 0x0400051B RID: 1307
		private const float WAVEOUT_PRE_FADE_WAIT_TIME = 0.7f;

		// Token: 0x0400051C RID: 1308
		private const float BATTLE_CLEAR_ZOOM_WAIT_TIME = 1.1f;

		// Token: 0x0400051D RID: 1309
		private const float BATTLE_CLEAR_MINIMUM_WAIT_TIME = 3.5f;

		// Token: 0x0400051E RID: 1310
		private const float STUN_OCCUR_WAIT_TIME = 0.10000001f;

		// Token: 0x0400051F RID: 1311
		private const float STUN_APPLY_WAIT_TIME = 1.5f;

		// Token: 0x04000520 RID: 1312
		private readonly float[] TURN_PRE_END_EVENT_WAIT_TIMES = new float[]
		{
			1.5f,
			1.5f,
			2.6f
		};

		// Token: 0x04000521 RID: 1313
		private const float GAME_OVER_MINIMUM_WAIT_TIME = 3f;

		// Token: 0x04000522 RID: 1314
		private int m_TurnPreEndStep;

		// Token: 0x04000523 RID: 1315
		private BattleSystem.eMainStep m_MainStep;

		// Token: 0x04000524 RID: 1316
		private ABResourceLoader m_BgLoader;

		// Token: 0x04000525 RID: 1317
		private ABResourceObjectHandler m_BgResourceObjHndl;

		// Token: 0x04000526 RID: 1318
		private List<CharacterHandler>[] m_PrepareCharaHndls = new List<CharacterHandler>[]
		{
			new List<CharacterHandler>(),
			new List<CharacterHandler>()
		};

		// Token: 0x04000527 RID: 1319
		private bool m_IsFirstPrepared;

		// Token: 0x04000528 RID: 1320
		private BattleSystem.ePrepareStep m_PrepareStep;

		// Token: 0x04000529 RID: 1321
		private bool[] m_IsNeedPrepares = new bool[]
		{
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true
		};

		// Token: 0x0400052A RID: 1322
		private int m_PrepareStatus_Sound;

		// Token: 0x0400052B RID: 1323
		[CompilerGenerated]
		private static Comparison<BattleSystem.SortData> <>f__mg$cache0;

		// Token: 0x0400052C RID: 1324
		[CompilerGenerated]
		private static Comparison<BattleSystem.SortData> <>f__mg$cache1;

		// Token: 0x0400052D RID: 1325
		[CompilerGenerated]
		private static Comparison<BattleSystem.SortData> <>f__mg$cache2;

		// Token: 0x020000E2 RID: 226
		private struct AutoMemberChangeData
		{
			// Token: 0x0400052E RID: 1326
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x0400052F RID: 1327
			public BattleDefine.eJoinMember m_Bench;

			// Token: 0x04000530 RID: 1328
			public int m_OrderIndex;

			// Token: 0x04000531 RID: 1329
			public float m_OrderValue;
		}

		// Token: 0x020000E3 RID: 227
		private struct DeadCharaData
		{
			// Token: 0x04000532 RID: 1330
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x04000533 RID: 1331
			public int m_OrderIndex;

			// Token: 0x04000534 RID: 1332
			public float m_OrderValue;
		}

		// Token: 0x020000E4 RID: 228
		public enum eMode
		{
			// Token: 0x04000536 RID: 1334
			None = -1,
			// Token: 0x04000537 RID: 1335
			Prepare,
			// Token: 0x04000538 RID: 1336
			BattleMain,
			// Token: 0x04000539 RID: 1337
			BattleFinished,
			// Token: 0x0400053A RID: 1338
			Destroy
		}

		// Token: 0x020000E5 RID: 229
		[Serializable]
		public class HogeItems
		{
			// Token: 0x0400053B RID: 1339
			public BattleDropItem[] m_Items;
		}

		// Token: 0x020000E6 RID: 230
		private enum eInterruptFriendOccurredSituation
		{
			// Token: 0x0400053D RID: 1341
			None = -1,
			// Token: 0x0400053E RID: 1342
			OrderSliding,
			// Token: 0x0400053F RID: 1343
			CommandInput
		}

		// Token: 0x020000E7 RID: 231
		public enum eSortMode
		{
			// Token: 0x04000541 RID: 1345
			Default,
			// Token: 0x04000542 RID: 1346
			KiraraJumpScene,
			// Token: 0x04000543 RID: 1347
			UniqueSkillScene
		}

		// Token: 0x020000E8 RID: 232
		public class SortData
		{
			// Token: 0x060006D7 RID: 1751 RVA: 0x00028DDC File Offset: 0x000271DC
			public SortData(Transform transform, CharacterHandler charaHndl, Renderer render, BattleUniqueSkillBaseObjectHandler.CharaRender US_CharaRender)
			{
				this.m_Transform = transform;
				this.m_CharaHndl = charaHndl;
				this.m_Render = render;
				this.m_US_CharaRender = US_CharaRender;
				if (this.m_US_CharaRender != null)
				{
					this.m_Transform = this.m_US_CharaRender.m_Transform;
				}
			}

			// Token: 0x04000544 RID: 1348
			public Transform m_Transform;

			// Token: 0x04000545 RID: 1349
			public CharacterHandler m_CharaHndl;

			// Token: 0x04000546 RID: 1350
			public BattleUniqueSkillBaseObjectHandler.CharaRender m_US_CharaRender;

			// Token: 0x04000547 RID: 1351
			public Renderer m_Render;
		}

		// Token: 0x020000EB RID: 235
		private enum eDestroyStep
		{
			// Token: 0x0400056A RID: 1386
			None = -1,
			// Token: 0x0400056B RID: 1387
			First,
			// Token: 0x0400056C RID: 1388
			DestroyOnlyUI_0,
			// Token: 0x0400056D RID: 1389
			Destroy_0,
			// Token: 0x0400056E RID: 1390
			Destroy_1,
			// Token: 0x0400056F RID: 1391
			Destroy_2,
			// Token: 0x04000570 RID: 1392
			Destroy_3,
			// Token: 0x04000571 RID: 1393
			UnloadUnusedAssets,
			// Token: 0x04000572 RID: 1394
			UnloadUnusedAssets_Wait,
			// Token: 0x04000573 RID: 1395
			End
		}

		// Token: 0x020000EC RID: 236
		private enum eMainStep
		{
			// Token: 0x04000575 RID: 1397
			None = -1,
			// Token: 0x04000576 RID: 1398
			First,
			// Token: 0x04000577 RID: 1399
			WarningStart,
			// Token: 0x04000578 RID: 1400
			WarningStart_Wait,
			// Token: 0x04000579 RID: 1401
			WaveInCharacter,
			// Token: 0x0400057A RID: 1402
			WaveInCharacter_Wait,
			// Token: 0x0400057B RID: 1403
			BattleStart,
			// Token: 0x0400057C RID: 1404
			BattleStart_Wait_0,
			// Token: 0x0400057D RID: 1405
			BattleStart_Wait_1,
			// Token: 0x0400057E RID: 1406
			BattleStart_Wait_2,
			// Token: 0x0400057F RID: 1407
			OrderSlide,
			// Token: 0x04000580 RID: 1408
			OrderSlide_Wait,
			// Token: 0x04000581 RID: 1409
			OrderSlide_AfterWait,
			// Token: 0x04000582 RID: 1410
			OrderCheck,
			// Token: 0x04000583 RID: 1411
			TurnSkipCheck,
			// Token: 0x04000584 RID: 1412
			TurnSkip_Wait,
			// Token: 0x04000585 RID: 1413
			CommandInputStartCheck,
			// Token: 0x04000586 RID: 1414
			CommandInput_Wait,
			// Token: 0x04000587 RID: 1415
			MasterUIInput_Wait,
			// Token: 0x04000588 RID: 1416
			MasterSkillExecute_0,
			// Token: 0x04000589 RID: 1417
			MasterSkillExecute_1,
			// Token: 0x0400058A RID: 1418
			MemberChangeExecute_0,
			// Token: 0x0400058B RID: 1419
			MemberChangeExecute_1,
			// Token: 0x0400058C RID: 1420
			MemberChangeExecute_2,
			// Token: 0x0400058D RID: 1421
			MemberChangeExecute_3,
			// Token: 0x0400058E RID: 1422
			MemberChangeExecute_4,
			// Token: 0x0400058F RID: 1423
			AutoMemberChangeExecute_0,
			// Token: 0x04000590 RID: 1424
			AutoMemberChangeExecute_1,
			// Token: 0x04000591 RID: 1425
			AutoMemberChangeExecute_2,
			// Token: 0x04000592 RID: 1426
			AutoMemberChangeExecute_3,
			// Token: 0x04000593 RID: 1427
			AutoMemberChangeExecute_4,
			// Token: 0x04000594 RID: 1428
			GoBackFriendExecute_0,
			// Token: 0x04000595 RID: 1429
			GoBackFriendExecute_1,
			// Token: 0x04000596 RID: 1430
			GoBackFriendExecute_2,
			// Token: 0x04000597 RID: 1431
			GoBackFriendExecute_3,
			// Token: 0x04000598 RID: 1432
			GoBackFriendExecute_4,
			// Token: 0x04000599 RID: 1433
			InterruptFriendJoinSelect_Wait,
			// Token: 0x0400059A RID: 1434
			InterruptFriendExecute_0,
			// Token: 0x0400059B RID: 1435
			InterruptFriendExecute_1,
			// Token: 0x0400059C RID: 1436
			InterruptFriendExecute_2,
			// Token: 0x0400059D RID: 1437
			InterruptFriendExecute_3,
			// Token: 0x0400059E RID: 1438
			InterruptFriendExecute_4,
			// Token: 0x0400059F RID: 1439
			InterruptFriendExecute_5,
			// Token: 0x040005A0 RID: 1440
			InterruptFriendExecute_6,
			// Token: 0x040005A1 RID: 1441
			InterruptFriendExecute_7,
			// Token: 0x040005A2 RID: 1442
			InterruptFriendExecute_8,
			// Token: 0x040005A3 RID: 1443
			TogetherAttackInput_Wait,
			// Token: 0x040005A4 RID: 1444
			TogetherAttackExecute_0,
			// Token: 0x040005A5 RID: 1445
			KiraraJumpScenePrepare,
			// Token: 0x040005A6 RID: 1446
			KiraraJumpScenePrepare_Wait,
			// Token: 0x040005A7 RID: 1447
			KiraraJumpScenePlaying,
			// Token: 0x040005A8 RID: 1448
			KiraraJumpSceneDestroy_Wait_0,
			// Token: 0x040005A9 RID: 1449
			KiraraJumpSceneDestroy_Wait_1,
			// Token: 0x040005AA RID: 1450
			UniqueSkillScenePrepare_Wait,
			// Token: 0x040005AB RID: 1451
			UniqueSkillScenePrepare_WaitOnNotEffect_0,
			// Token: 0x040005AC RID: 1452
			UniqueSkillScenePrepare_WaitOnNotEffect_1,
			// Token: 0x040005AD RID: 1453
			UniqueSkillScenePlaying,
			// Token: 0x040005AE RID: 1454
			UniqueSkillSceneDestroy_Wait_0,
			// Token: 0x040005AF RID: 1455
			UniqueSkillSceneDestroy_Wait_1,
			// Token: 0x040005B0 RID: 1456
			UniqueSkillSceneDestroy_Wait_2,
			// Token: 0x040005B1 RID: 1457
			UniqueSkillSceneDestroy_Wait_3,
			// Token: 0x040005B2 RID: 1458
			CommandExecute,
			// Token: 0x040005B3 RID: 1459
			CommandExecute_Wait_0,
			// Token: 0x040005B4 RID: 1460
			CommandExecute_Wait_1,
			// Token: 0x040005B5 RID: 1461
			CommandExecute_Wait_2,
			// Token: 0x040005B6 RID: 1462
			PostCommandExecute,
			// Token: 0x040005B7 RID: 1463
			StunExecute,
			// Token: 0x040005B8 RID: 1464
			StunExecute_Wait_0,
			// Token: 0x040005B9 RID: 1465
			StunExecute_Wait_1,
			// Token: 0x040005BA RID: 1466
			StunExecute_Wait_2,
			// Token: 0x040005BB RID: 1467
			StunExecute_Wait_3,
			// Token: 0x040005BC RID: 1468
			TurnPreEnd,
			// Token: 0x040005BD RID: 1469
			TurnPreEnd_Wait,
			// Token: 0x040005BE RID: 1470
			TurnEnd,
			// Token: 0x040005BF RID: 1471
			WaveOut,
			// Token: 0x040005C0 RID: 1472
			WaveOut_Wait_0,
			// Token: 0x040005C1 RID: 1473
			WaveOut_Wait_1,
			// Token: 0x040005C2 RID: 1474
			WaveOut_Wait_2,
			// Token: 0x040005C3 RID: 1475
			WaveOut_Wait_3,
			// Token: 0x040005C4 RID: 1476
			BattleClear,
			// Token: 0x040005C5 RID: 1477
			BattleClear_Wait_0,
			// Token: 0x040005C6 RID: 1478
			BattleClear_Wait_1,
			// Token: 0x040005C7 RID: 1479
			GameOver,
			// Token: 0x040005C8 RID: 1480
			GameOver_Wait_0,
			// Token: 0x040005C9 RID: 1481
			GameOver_Wait_1,
			// Token: 0x040005CA RID: 1482
			RetrySelect,
			// Token: 0x040005CB RID: 1483
			RetrySelect_Wait,
			// Token: 0x040005CC RID: 1484
			RetryCancel,
			// Token: 0x040005CD RID: 1485
			RetryRequest,
			// Token: 0x040005CE RID: 1486
			RetryRequest_Wait,
			// Token: 0x040005CF RID: 1487
			Retry,
			// Token: 0x040005D0 RID: 1488
			Retry_Wait,
			// Token: 0x040005D1 RID: 1489
			ToResult,
			// Token: 0x040005D2 RID: 1490
			ToResult_Wait,
			// Token: 0x040005D3 RID: 1491
			Retire,
			// Token: 0x040005D4 RID: 1492
			PrepareError,
			// Token: 0x040005D5 RID: 1493
			PrepareError_Wait
		}

		// Token: 0x020000ED RID: 237
		private enum ePrepareStep
		{
			// Token: 0x040005D7 RID: 1495
			None = -1,
			// Token: 0x040005D8 RID: 1496
			First,
			// Token: 0x040005D9 RID: 1497
			Prepare_0,
			// Token: 0x040005DA RID: 1498
			Prepare_1,
			// Token: 0x040005DB RID: 1499
			End,
			// Token: 0x040005DC RID: 1500
			PrepareError,
			// Token: 0x040005DD RID: 1501
			PrepareError_Wait
		}

		// Token: 0x020000EE RID: 238
		private enum ePrepareTarget
		{
			// Token: 0x040005DF RID: 1503
			Sound,
			// Token: 0x040005E0 RID: 1504
			BG,
			// Token: 0x040005E1 RID: 1505
			AlwaysEffect,
			// Token: 0x040005E2 RID: 1506
			Player,
			// Token: 0x040005E3 RID: 1507
			PlayerEffect,
			// Token: 0x040005E4 RID: 1508
			Enemy,
			// Token: 0x040005E5 RID: 1509
			EnemyEffect,
			// Token: 0x040005E6 RID: 1510
			UI,
			// Token: 0x040005E7 RID: 1511
			Num
		}
	}
}
