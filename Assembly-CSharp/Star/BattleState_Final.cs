﻿using System;

namespace Star
{
	// Token: 0x020003E1 RID: 993
	public class BattleState_Final : BattleState
	{
		// Token: 0x060012E6 RID: 4838 RVA: 0x00064F7D File Offset: 0x0006337D
		public BattleState_Final(BattleMain owner) : base(owner)
		{
		}

		// Token: 0x060012E7 RID: 4839 RVA: 0x00064F8D File Offset: 0x0006338D
		public override int GetStateID()
		{
			return 3;
		}

		// Token: 0x060012E8 RID: 4840 RVA: 0x00064F90 File Offset: 0x00063390
		public override void OnStateEnter()
		{
			this.m_Step = BattleState_Final.eStep.First;
		}

		// Token: 0x060012E9 RID: 4841 RVA: 0x00064F99 File Offset: 0x00063399
		public override void OnStateExit()
		{
		}

		// Token: 0x060012EA RID: 4842 RVA: 0x00064F9C File Offset: 0x0006339C
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case BattleState_Final.eStep.First:
				if (this.m_Owner.PrevStateID == 2)
				{
					this.m_Step = BattleState_Final.eStep.Destory;
				}
				else
				{
					this.m_Step = BattleState_Final.eStep.FadeOut;
				}
				break;
			case BattleState_Final.eStep.FadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(-1f, null);
				this.m_Step = BattleState_Final.eStep.FadeOut_Wait;
				break;
			case BattleState_Final.eStep.FadeOut_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = BattleState_Final.eStep.Destory;
				}
				break;
			case BattleState_Final.eStep.Destory:
				this.m_Owner.System.SystemDestory();
				this.m_Step = BattleState_Final.eStep.Destory_Wait;
				break;
			case BattleState_Final.eStep.Destory_Wait:
				if (this.m_Owner.System.IsComplteDestory())
				{
					this.m_Step = BattleState_Final.eStep.End;
				}
				break;
			case BattleState_Final.eStep.End:
			{
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				globalParam.IsRequestAPIWhenEnterQuestScene = true;
				globalParam.QuestFirstCleared = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult().m_IsFirstClear;
				globalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.List;
				BattleResult.eStatus status = this.m_Owner.System.QuestResult.m_Status;
				if (status != BattleResult.eStatus.Clear)
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Quest);
				}
				else
				{
					int num = this.m_Owner.System.QuestParam.m_AdvID_After;
					if (this.m_Owner.System.QuestParam.m_AdvID_AfterBranch != -1 && this.m_Owner.System.QuestResult.m_IsLoserClear)
					{
						num = this.m_Owner.System.QuestParam.m_AdvID_AfterBranch;
					}
					bool flag = false;
					if (num != -1)
					{
						flag = true;
						if (LocalSaveData.Inst.ADVSkipOnBattle && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(num))
						{
							flag = false;
						}
					}
					if (flag)
					{
						globalParam.SetAdvParam(num, SceneDefine.eSceneID.Quest, false);
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
					}
					else
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Quest);
					}
				}
				this.m_Step = BattleState_Final.eStep.None;
				break;
			}
			}
			return -1;
		}

		// Token: 0x040019B8 RID: 6584
		private BattleState_Final.eStep m_Step = BattleState_Final.eStep.None;

		// Token: 0x020003E2 RID: 994
		private enum eStep
		{
			// Token: 0x040019BA RID: 6586
			None = -1,
			// Token: 0x040019BB RID: 6587
			First,
			// Token: 0x040019BC RID: 6588
			FadeOut,
			// Token: 0x040019BD RID: 6589
			FadeOut_Wait,
			// Token: 0x040019BE RID: 6590
			Destory,
			// Token: 0x040019BF RID: 6591
			Destory_Wait,
			// Token: 0x040019C0 RID: 6592
			End
		}
	}
}
