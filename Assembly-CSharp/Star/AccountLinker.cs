﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x0200007A RID: 122
	public class AccountLinker : MonoBehaviour
	{
		// Token: 0x060003D3 RID: 979 RVA: 0x00013235 File Offset: 0x00011635
		public static void CheckCreate()
		{
			CGlobalInstance.CreateInstance();
			CGlobalInstance.EntryClass<AccountLinker>();
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x00013244 File Offset: 0x00011644
		public static void SetAutoDestroy()
		{
			AccountLinker @class = CGlobalInstance.GetClass<AccountLinker>();
			@class.m_Step = AccountLinker.eStep.AutoDestory;
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00013260 File Offset: 0x00011660
		private string CreateUserAccountID()
		{
			return string.Format("{0}_{1}", CRC32.Calc(IAcountController.GetUserName()), IAcountController.GetUserID());
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00013290 File Offset: 0x00011690
		public static void SetAcountLinkData()
		{
			AccountLinker @class = CGlobalInstance.GetClass<AccountLinker>();
			@class.m_ActiveResult = AccountLinker.eResult.Access;
			@class.m_Step = AccountLinker.eStep.CheckLogIn;
			@class.enabled = true;
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x000132B8 File Offset: 0x000116B8
		public static void BuildUpAcountLinkPlayer()
		{
			AccountLinker @class = CGlobalInstance.GetClass<AccountLinker>();
			@class.m_ActiveResult = AccountLinker.eResult.Access;
			@class.m_Step = AccountLinker.eStep.MovePlayerLogIn;
			@class.enabled = true;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x000132E0 File Offset: 0x000116E0
		private void Update()
		{
			switch (this.m_Step)
			{
			case AccountLinker.eStep.Non:
				base.enabled = false;
				break;
			case AccountLinker.eStep.CheckLogIn:
				if (!IAcountController.IsSetup())
				{
					IAcountController.Create();
				}
				this.m_Step = AccountLinker.eStep.LinkSet;
				IAcountController.ResetCallback();
				break;
			case AccountLinker.eStep.LinkSet:
			{
				IAcountController.eState state = IAcountController.GetState();
				Debug.Log("Link LogIn Check : " + state);
				if (state == IAcountController.eState.Online)
				{
					this.m_Step = AccountLinker.eStep.ComCheck;
					this.SendPlayerLinkCom();
				}
				else if (state == IAcountController.eState.SignOut)
				{
					this.m_Step = AccountLinker.eStep.End;
					this.m_ActiveResult = AccountLinker.eResult.LogInError;
				}
				break;
			}
			case AccountLinker.eStep.End:
				this.m_Step = AccountLinker.eStep.Non;
				break;
			case AccountLinker.eStep.MovePlayerLogIn:
				if (!IAcountController.IsSetup())
				{
					IAcountController.Create();
				}
				this.m_Step = AccountLinker.eStep.LinkMoveCom;
				IAcountController.ResetCallback();
				break;
			case AccountLinker.eStep.LinkMoveCom:
			{
				IAcountController.eState state2 = IAcountController.GetState();
				Debug.Log("LinkMove LogIn Check : " + state2);
				if (state2 == IAcountController.eState.Online)
				{
					this.m_Step = AccountLinker.eStep.ComCheck;
					this.SendLinkMoveCom();
				}
				else if (state2 == IAcountController.eState.SignOut)
				{
					this.m_Step = AccountLinker.eStep.End;
					this.m_ActiveResult = AccountLinker.eResult.LogInError;
				}
				break;
			}
			case AccountLinker.eStep.AutoDestory:
				UnityEngine.Object.Destroy(this);
				break;
			}
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00013424 File Offset: 0x00011824
		private void SendPlayerLinkCom()
		{
			PlayerRequestTypes.Set set = new PlayerRequestTypes.Set();
			set.name = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Name;
			set.comment = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Comment;
			set.linkId = this.CreateUserAccountID();
			MeigewwwParam wwwParam = PlayerRequest.Set(set, new MeigewwwParam.Callback(this.CallbackUserLinkSet));
			NetworkQueueManager.Request(wwwParam);
			Debug.Log("Link Set : " + set.linkId);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060003DA RID: 986 RVA: 0x000134B4 File Offset: 0x000118B4
		private void CallbackUserLinkSet(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Set set = PlayerResponse.Set(wwwParam, ResponseCommon.DialogType.None, null);
			if (set == null)
			{
				Debug.Log("AccountLinker.CallbackUserLinkSet invalid error");
				this.m_ActiveResult = AccountLinker.eResult.ComError;
				this.m_Step = AccountLinker.eStep.End;
				return;
			}
			ResultCode result = set.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				this.m_ActiveResult = AccountLinker.eResult.ComError;
				this.m_Step = AccountLinker.eStep.End;
				Debug.Log("AccountLinker.CallbackUserLinkSet ComError");
			}
			else
			{
				this.m_ActiveResult = AccountLinker.eResult.Success;
				this.m_Step = AccountLinker.eStep.End;
				Debug.Log("AccountLinker.CallbackUserLinkSet Success");
			}
		}

		// Token: 0x060003DB RID: 987 RVA: 0x00013544 File Offset: 0x00011944
		private void SendLinkMoveCom()
		{
			PlayerRequestTypes.Linkmoveset linkmoveset = new PlayerRequestTypes.Linkmoveset();
			linkmoveset.linkId = this.CreateUserAccountID();
			linkmoveset.uuid = (this.m_WorkUUID = APIUtility.GenerateUUID());
			linkmoveset.platform = Platform.Get();
			MeigewwwParam wwwParam = PlayerRequest.Linkmoveset(linkmoveset, new MeigewwwParam.Callback(this.CallbackUserLinkMove));
			NetworkQueueManager.Request(wwwParam);
			Debug.Log("Link Move " + linkmoveset.linkId + " : " + linkmoveset.uuid);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060003DC RID: 988 RVA: 0x000135CC File Offset: 0x000119CC
		private void CallbackUserLinkMove(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Linkmoveset linkmoveset = PlayerResponse.Linkmoveset(wwwParam, ResponseCommon.DialogType.None, null);
			if (linkmoveset == null)
			{
				Debug.Log("AccountLinker.CallbackUserLinkMove invalid error");
				this.m_ActiveResult = AccountLinker.eResult.ComError;
				this.m_Step = AccountLinker.eStep.End;
				return;
			}
			ResultCode result = linkmoveset.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				this.m_ActiveResult = AccountLinker.eResult.ComError;
				this.m_Step = AccountLinker.eStep.End;
				Debug.Log("AccountLinker.CallbackUserLinkMove invalid ComError");
			}
			else
			{
				AccessSaveData.Inst.UUID = this.m_WorkUUID;
				AccessSaveData.Inst.AccessToken = linkmoveset.accessToken;
				AccessSaveData.Inst.Save();
				this.m_ActiveResult = AccountLinker.eResult.Success;
				this.m_Step = AccountLinker.eStep.End;
				Debug.Log("AccountLinker.CallbackUserLinkMove invalid Success");
			}
		}

		// Token: 0x060003DD RID: 989 RVA: 0x00013684 File Offset: 0x00011A84
		public static AccountLinker.eResult GetState()
		{
			AccountLinker @class = CGlobalInstance.GetClass<AccountLinker>();
			return @class.m_ActiveResult;
		}

		// Token: 0x04000209 RID: 521
		private AccountLinker.eResult m_ActiveResult;

		// Token: 0x0400020A RID: 522
		private AccountLinker.eStep m_Step;

		// Token: 0x0400020B RID: 523
		private string m_WorkUUID;

		// Token: 0x0200007B RID: 123
		public enum eResult
		{
			// Token: 0x0400020D RID: 525
			Access,
			// Token: 0x0400020E RID: 526
			Success,
			// Token: 0x0400020F RID: 527
			LogInError,
			// Token: 0x04000210 RID: 528
			ComError
		}

		// Token: 0x0200007C RID: 124
		private enum eStep
		{
			// Token: 0x04000212 RID: 530
			Non,
			// Token: 0x04000213 RID: 531
			CheckLogIn,
			// Token: 0x04000214 RID: 532
			LinkSet,
			// Token: 0x04000215 RID: 533
			ComCheck,
			// Token: 0x04000216 RID: 534
			End,
			// Token: 0x04000217 RID: 535
			MovePlayerLogIn,
			// Token: 0x04000218 RID: 536
			LinkMoveCom,
			// Token: 0x04000219 RID: 537
			AutoDestory
		}
	}
}
