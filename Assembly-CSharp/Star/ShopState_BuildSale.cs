﻿using System;
using Star.UI.Room;

namespace Star
{
	// Token: 0x0200047F RID: 1151
	public class ShopState_BuildSale : ShopState
	{
		// Token: 0x06001681 RID: 5761 RVA: 0x00075617 File Offset: 0x00073A17
		public ShopState_BuildSale(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x06001682 RID: 5762 RVA: 0x0007563A File Offset: 0x00073A3A
		public override int GetStateID()
		{
			return 22;
		}

		// Token: 0x06001683 RID: 5763 RVA: 0x0007563E File Offset: 0x00073A3E
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_BuildSale.eStep.First;
		}

		// Token: 0x06001684 RID: 5764 RVA: 0x00075647 File Offset: 0x00073A47
		public override void OnStateExit()
		{
		}

		// Token: 0x06001685 RID: 5765 RVA: 0x00075649 File Offset: 0x00073A49
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001686 RID: 5766 RVA: 0x00075654 File Offset: 0x00073A54
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_BuildSale.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_BuildSale.eStep.LoadWait;
				break;
			case ShopState_BuildSale.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildSale.eStep.PlayIn;
				}
				break;
			case ShopState_BuildSale.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_BuildSale.eStep.Main;
				}
				break;
			case ShopState_BuildSale.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						RoomShopListUI.eTransit transit = this.m_UI.Transit;
						if (transit != RoomShopListUI.eTransit.Room)
						{
							this.m_NextState = 20;
						}
						else
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
							this.m_NextState = 2147483646;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildSale.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_BuildSale.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_BuildSale.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001687 RID: 5767 RVA: 0x000757B4 File Offset: 0x00073BB4
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<RoomShopListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.SetMode(RoomObjListWindow.eMode.Have);
			this.m_UI.Setup(this.m_RoomObjShopList);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001688 RID: 5768 RVA: 0x0007583C File Offset: 0x00073C3C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x0007584B File Offset: 0x00073C4B
		private void OnClickButtonCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x0600168A RID: 5770 RVA: 0x00075853 File Offset: 0x00073C53
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001D3F RID: 7487
		private ShopState_BuildSale.eStep m_Step = ShopState_BuildSale.eStep.None;

		// Token: 0x04001D40 RID: 7488
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.RoomShopListUI;

		// Token: 0x04001D41 RID: 7489
		private RoomShopListUI m_UI;

		// Token: 0x04001D42 RID: 7490
		private RoomObjShopList m_RoomObjShopList = new RoomObjShopList();

		// Token: 0x02000480 RID: 1152
		private enum eStep
		{
			// Token: 0x04001D44 RID: 7492
			None = -1,
			// Token: 0x04001D45 RID: 7493
			First,
			// Token: 0x04001D46 RID: 7494
			LoadStart,
			// Token: 0x04001D47 RID: 7495
			LoadWait,
			// Token: 0x04001D48 RID: 7496
			PlayIn,
			// Token: 0x04001D49 RID: 7497
			Main,
			// Token: 0x04001D4A RID: 7498
			UnloadChildSceneWait
		}
	}
}
