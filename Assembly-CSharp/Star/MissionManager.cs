﻿using System;
using System.Collections.Generic;
using Meige;
using Star.UI.Mission;
using UnityEngine;

namespace Star
{
	// Token: 0x02000504 RID: 1284
	public class MissionManager
	{
		// Token: 0x06001933 RID: 6451 RVA: 0x00082E4C File Offset: 0x0008124C
		public void Prepare()
		{
			if (MissionManager.Inst == null)
			{
				MissionManager.Inst = this;
			}
			if (this.IsDonePrepare())
			{
				return;
			}
			if (this.m_PopUpMask == null)
			{
				this.m_PopUpMask = new PopupMaskFunc();
				this.m_PopUpMask.PopUpActive(eXlsPopupTiming.PopNon, true);
			}
			this.m_MissionApi = IMissionServerApi.CreateServerApi();
			if (this.m_Loader == null)
			{
				this.m_Loader = new ABResourceLoader(-1);
			}
			this.m_LoadingHndl = this.m_Loader.Load("prefab/ui/missionprecomp.muast", new MeigeResource.Option[0]);
			this.m_PrepareStep = MissionManager.ePrepareStep.Preparing;
		}

		// Token: 0x06001934 RID: 6452 RVA: 0x00082EE0 File Offset: 0x000812E0
		public bool IsDonePrepare()
		{
			return !(this.m_MissionApi == null) && this.m_MissionApi.IsAvailable() && !(this.m_PopUpModel == null) && this.m_PopUpMask != null && this.m_PrepareStep == MissionManager.ePrepareStep.DonePrepare;
		}

		// Token: 0x06001935 RID: 6453 RVA: 0x00082F44 File Offset: 0x00081344
		public void ForceResetOnReturnTitle()
		{
			if (this.m_MissionApi != null)
			{
				IMissionServerApi.DestroyServerApi();
			}
			if (this.m_LoadingHndl != null)
			{
				if (this.m_Loader == null)
				{
					this.m_Loader.Unload(this.m_LoadingHndl);
				}
				this.m_LoadingHndl = null;
			}
			if (this.m_PopUpMask != null)
			{
				this.m_PopUpMask.Clear();
			}
			this.m_PrepareStep = MissionManager.ePrepareStep.None;
			this.m_MissionUI = null;
			this.m_IsOpenUI = false;
			this.m_Step = MissionManager.eStep.None;
		}

		// Token: 0x06001936 RID: 6454 RVA: 0x00082FC8 File Offset: 0x000813C8
		private void Preparing()
		{
			if (this.m_LoadingHndl != null && this.m_LoadingHndl.IsDone())
			{
				if (this.m_LoadingHndl.IsError())
				{
					this.m_LoadingHndl.Retry();
				}
				else
				{
					this.m_PopUpModel = (this.m_LoadingHndl.FindObj("MissionPreComp", true) as GameObject);
					this.m_Loader.Unload(this.m_LoadingHndl);
					this.m_LoadingHndl = null;
					this.m_PrepareStep = MissionManager.ePrepareStep.DonePrepare;
				}
			}
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0008304B File Offset: 0x0008144B
		public static void SetPopUpActive(eXlsPopupTiming fcategoryid, bool factive)
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_PopUpMask.PopUpActive(fcategoryid, factive);
			}
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x00083068 File Offset: 0x00081468
		public static void UnlockAction(eXlsMissionSeg fcategory, object factionno)
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_MissionApi.UnlockAction(fcategory, (int)factionno);
			}
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x0008308A File Offset: 0x0008148A
		public static void CompleteMission(long fcompmangeid)
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_MissionApi.CompleteMission(fcompmangeid);
			}
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x000830A6 File Offset: 0x000814A6
		public static void EntryRefreshCallback(RefreshCallback pcallback, int fkeyid)
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_MissionApi.EntryRefreshCallback(pcallback, fkeyid);
			}
		}

		// Token: 0x0600193B RID: 6459 RVA: 0x000830C3 File Offset: 0x000814C3
		public static void ReleaseRefreshCallback(int fkeyid)
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_MissionApi.ReleaseRefreshCallback(fkeyid);
			}
		}

		// Token: 0x0600193C RID: 6460 RVA: 0x000830DF File Offset: 0x000814DF
		public static int GetNewListUpNum()
		{
			if (MissionManager.Inst != null)
			{
				return MissionManager.Inst.m_MissionApi.GetPreCompNum();
			}
			return 0;
		}

		// Token: 0x0600193D RID: 6461 RVA: 0x000830FC File Offset: 0x000814FC
		public MissionUIDataPack GetCategoryToMissionUIData(eMissionCategory Category)
		{
			List<IMissionPakage> missionList = this.m_MissionApi.GetMissionList();
			MissionUIDataPack missionUIDataPack = new MissionUIDataPack();
			int count = missionList.Count;
			int num = 0;
			for (int i = 0; i < count; i++)
			{
				if (missionList[i].m_Type == Category)
				{
					num++;
				}
			}
			if (num != 0)
			{
				missionUIDataPack.m_Table = new IMissionUIData[num];
				num = 0;
				for (int i = 0; i < count; i++)
				{
					if (missionList[i].m_Type == Category)
					{
						missionUIDataPack.m_Table[num] = new IMissionUIData();
						missionUIDataPack.m_Table[num].m_Type = missionList[i].m_Type;
						missionUIDataPack.m_Table[num].m_TargetMessage = missionList[i].GetTargetMessage();
						missionUIDataPack.m_Table[num].m_RewardType = missionList[i].m_RewardCategory;
						missionUIDataPack.m_Table[num].m_RewardID = missionList[i].m_RewardNo;
						missionUIDataPack.m_Table[num].m_RewardNum = missionList[i].m_RewardNum;
						missionUIDataPack.m_Table[num].m_DatabaseID = missionList[i].m_ListUpID;
						missionUIDataPack.m_Table[num].m_ManageID = missionList[i].m_KeyManageID;
						missionUIDataPack.m_Table[num].m_Rate = missionList[i].m_Rate;
						missionUIDataPack.m_Table[num].m_RateMax = missionList[i].m_RateBase;
						missionUIDataPack.m_Table[num].m_State = missionList[i].m_State;
						missionUIDataPack.m_Table[num].m_LimitTime = missionList[i].m_LimitTime;
						num++;
					}
				}
			}
			else
			{
				missionUIDataPack.m_Table = new IMissionUIData[0];
			}
			return missionUIDataPack;
		}

		// Token: 0x0600193E RID: 6462 RVA: 0x000832BD File Offset: 0x000816BD
		public void OpenUI()
		{
			if (this.m_IsOpenUI)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(SingletonMonoBehaviour<GameSystem>.Inst.gameObject, true);
			this.m_IsOpenUI = true;
			this.m_Step = MissionManager.eStep.First;
		}

		// Token: 0x0600193F RID: 6463 RVA: 0x000832F3 File Offset: 0x000816F3
		public void CloseUI()
		{
			if (this.m_MissionUI != null)
			{
				this.m_MissionUI.PlayOut();
			}
		}

		// Token: 0x06001940 RID: 6464 RVA: 0x00083311 File Offset: 0x00081711
		public bool IsOpenUI()
		{
			return this.m_IsOpenUI;
		}

		// Token: 0x06001941 RID: 6465 RVA: 0x0008331C File Offset: 0x0008171C
		public void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			if (this.m_PrepareStep == MissionManager.ePrepareStep.Preparing)
			{
				this.Preparing();
				return;
			}
			switch (this.m_Step)
			{
			case MissionManager.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.MissionUI, true);
				this.m_MissionApi.StarApi();
				this.m_Step = MissionManager.eStep.LoadWait;
				break;
			case MissionManager.eStep.LoadWait:
				if (this.m_MissionApi.IsListUp() && SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.MissionUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.MissionUI);
					this.m_Step = MissionManager.eStep.PlayIn;
				}
				break;
			case MissionManager.eStep.PlayIn:
				this.m_MissionUI = UIUtility.GetMenuComponent<MissionUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.MissionUI].m_SceneName);
				if (!(this.m_MissionUI == null))
				{
					this.m_MissionUI.Setup();
					this.m_MissionUI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(SingletonMonoBehaviour<GameSystem>.Inst.gameObject, false);
					this.m_Step = MissionManager.eStep.Main;
				}
				break;
			case MissionManager.eStep.Main:
				if (this.m_MissionUI == null)
				{
					this.m_IsOpenUI = false;
					this.m_Step = MissionManager.eStep.None;
				}
				else if (this.m_MissionUI.IsMenuEnd())
				{
					Resources.UnloadUnusedAssets();
					this.m_MissionUI = null;
					SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.UnloadChildSceneAsync(SceneDefine.eChildSceneID.MissionUI);
					this.m_Step = MissionManager.eStep.UnloadChildSceneWait;
				}
				break;
			case MissionManager.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.MissionUI))
				{
					this.m_Step = MissionManager.eStep.End;
				}
				break;
			case MissionManager.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.MissionUI))
				{
					this.m_IsOpenUI = false;
					this.m_Step = MissionManager.eStep.None;
				}
				break;
			}
			if (this.m_PopUpModel != null)
			{
				if (this.m_PopUpWindow == null)
				{
					if (this.m_MissionApi.IsPreComp())
					{
						IMissionPakage preCompMission = this.m_MissionApi.GetPreCompMission(this.m_PopUpMask);
						if (preCompMission != null)
						{
							GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_PopUpModel);
							UnityEngine.Object.DontDestroyOnLoad(gameObject);
							this.m_PopUpWindow = gameObject.AddComponent<MissionPopupControll>();
							this.m_PopUpWindow.SetUp(preCompMission);
						}
					}
				}
				else if (this.m_PopUpWindow.IsEnd())
				{
					this.m_PopUpWindow.SetAutoDestroy();
					this.m_PopUpWindow = null;
				}
			}
		}

		// Token: 0x06001942 RID: 6466 RVA: 0x00083591 File Offset: 0x00081991
		public static void RebuildMissionList()
		{
			if (MissionManager.Inst != null)
			{
				MissionManager.Inst.m_MissionApi.RebuildMissionList();
			}
		}

		// Token: 0x04002002 RID: 8194
		public static MissionManager Inst;

		// Token: 0x04002003 RID: 8195
		private MissionManager.eStep m_Step = MissionManager.eStep.None;

		// Token: 0x04002004 RID: 8196
		private MissionManager.ePrepareStep m_PrepareStep = MissionManager.ePrepareStep.None;

		// Token: 0x04002005 RID: 8197
		private MissionUI m_MissionUI;

		// Token: 0x04002006 RID: 8198
		private bool m_IsOpenUI;

		// Token: 0x04002007 RID: 8199
		private IMissionServerApi m_MissionApi;

		// Token: 0x04002008 RID: 8200
		private GameObject m_PopUpModel;

		// Token: 0x04002009 RID: 8201
		private MissionPopupControll m_PopUpWindow;

		// Token: 0x0400200A RID: 8202
		private PopupMaskFunc m_PopUpMask;

		// Token: 0x0400200B RID: 8203
		private ABResourceLoader m_Loader;

		// Token: 0x0400200C RID: 8204
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x02000505 RID: 1285
		private enum eStep
		{
			// Token: 0x0400200E RID: 8206
			None = -1,
			// Token: 0x0400200F RID: 8207
			First,
			// Token: 0x04002010 RID: 8208
			LoadWait,
			// Token: 0x04002011 RID: 8209
			PlayIn,
			// Token: 0x04002012 RID: 8210
			Main,
			// Token: 0x04002013 RID: 8211
			PlayOut,
			// Token: 0x04002014 RID: 8212
			UnloadChildSceneWait,
			// Token: 0x04002015 RID: 8213
			End
		}

		// Token: 0x02000506 RID: 1286
		private enum ePrepareStep
		{
			// Token: 0x04002017 RID: 8215
			None = -1,
			// Token: 0x04002018 RID: 8216
			Preparing,
			// Token: 0x04002019 RID: 8217
			DonePrepare
		}
	}
}
