﻿using System;

namespace Star
{
	// Token: 0x020001BC RID: 444
	public static class UniqueSkillSoundDB_Ext
	{
		// Token: 0x06000B3A RID: 2874 RVA: 0x00042CA0 File Offset: 0x000410A0
		public static void SetSoundFrameController(this UniqueSkillSoundDB self, string cueSheet, SoundFrameController sfc)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CueSheet == cueSheet)
				{
					bool flag = cueSheet.Contains("Voice");
					for (int j = 0; j < self.m_Params[i].m_Datas.Length; j++)
					{
						string cueName;
						if (flag)
						{
							cueName = string.Format("voice_kirarajump_{0:D3}", j);
						}
						else
						{
							cueName = self.m_Params[i].m_Datas[j].m_CueID;
						}
						sfc.AddRequestParam(self.m_Params[i].m_Datas[j].m_PlayFrame, cueName, flag, false);
					}
					break;
				}
			}
		}
	}
}
