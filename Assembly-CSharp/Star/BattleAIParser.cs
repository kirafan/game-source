﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Star
{
	// Token: 0x02000099 RID: 153
	public static class BattleAIParser
	{
		// Token: 0x06000439 RID: 1081 RVA: 0x00014838 File Offset: 0x00012C38
		public static BattleAISolveResult SolveBattleAI(CharacterHandler executor)
		{
			BattleAISolveResult battleAISolveResult = new BattleAISolveResult();
			if (executor.CharaBattle.Param.IsChargeCountMax())
			{
				BattleAIParser.SolveBattleAI_ChargeSkill(executor, battleAISolveResult);
			}
			else
			{
				BattleAIParser.SolveBattleAI_Skill(executor, battleAISolveResult);
			}
			return battleAISolveResult;
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x00014874 File Offset: 0x00012C74
		private static void SolveBattleAI_ChargeSkill(CharacterHandler executor, BattleAISolveResult out_result)
		{
			out_result.m_IsChargeSkill = true;
			int patternIndex = -1;
			BattleAIParser.SolveBattleAI_CommandDatas(executor, executor.CharaBattle.GetUniqueSkillCommandDatas(), patternIndex, executor.BattleAIData.m_ChargeCommandDatas, executor.BattleAIData.m_SingleTargetPriorityWhenHateSame, out_result);
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x000148B4 File Offset: 0x00012CB4
		private static void SolveBattleAI_Skill(CharacterHandler executor, BattleAISolveResult out_result)
		{
			out_result.m_IsChargeSkill = false;
			BattleAIParser.SolveBattleAI_ChangePattern(executor);
			int num = executor.CharaBattle.GetAIPatternIndex();
			if (num >= executor.BattleAIData.m_PatternDatas.Count)
			{
				num = 0;
			}
			BattleAIPatternData battleAIPatternData = executor.BattleAIData.m_PatternDatas[num];
			BattleAIParser.SolveBattleAI_CommandDatas(executor, executor.CharaBattle.GetCommandDatas(), num, battleAIPatternData.m_CommandDatas, executor.BattleAIData.m_SingleTargetPriorityWhenHateSame, out_result);
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0001492C File Offset: 0x00012D2C
		private static void SolveBattleAI_ChangePattern(CharacterHandler executor)
		{
			BattleAIData battleAIData = executor.BattleAIData;
			if (battleAIData.m_PatternDatas.Count <= 1)
			{
				return;
			}
			if (battleAIData.m_PatternChangeDatas.Count <= 0)
			{
				return;
			}
			for (int i = 0; i < battleAIData.m_PatternChangeDatas.Count; i++)
			{
				if (!executor.CharaBattle.IsAIPatternChangeHitIndex(i))
				{
					BattleAIPatternChangeData battleAIPatternChangeData = battleAIData.m_PatternChangeDatas[i];
					if (BattleAIParser.JudgeBattleAIPatternChangeConditions(executor, battleAIPatternChangeData.m_Conditions) && executor.CharaBattle.GetAIPatternIndex() != battleAIPatternChangeData.m_ChangeToPatternIndex)
					{
						executor.CharaBattle.SetAIPatternIndex(battleAIPatternChangeData.m_ChangeToPatternIndex);
						executor.CharaBattle.AddAIPatternChangeHitIndex(i);
						break;
					}
				}
			}
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x000149EC File Offset: 0x00012DEC
		private static bool SolveBattleAI_CommandDatas(CharacterHandler executor, List<BattleCommandData> commands, int patternIndex, List<BattleAICommandData> aiCommandDatas, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame, BattleAISolveResult out_result)
		{
			int num = -1;
			if (aiCommandDatas != null)
			{
				int i = 0;
				while (i < aiCommandDatas.Count)
				{
					if (aiCommandDatas[i].m_ExecNum <= 0)
					{
						goto IL_46;
					}
					int aiexecedNum = executor.CharaBattle.GetAIExecedNum(patternIndex, i);
					if (aiexecedNum < aiCommandDatas[i].m_ExecNum)
					{
						goto IL_46;
					}
					IL_64:
					i++;
					continue;
					IL_46:
					if (BattleAIParser.JudgeBattleAICommandConditions(executor, aiCommandDatas[i].m_Conditions))
					{
						num = i;
						break;
					}
					goto IL_64;
				}
			}
			if (num != -1)
			{
				BattleAICommandData battleAICommandData = aiCommandDatas[num];
				BattleAIParser.RequestResetActionCount(executor.CharaBattle.TgtParty, battleAICommandData.m_Conditions);
				int num2 = -1;
				float num3 = 0f;
				int count = battleAICommandData.m_ExecDatas.Count;
				for (int j = 0; j < count; j++)
				{
					num3 += (float)battleAICommandData.m_ExecDatas[j].m_Ratio;
				}
				if (num3 == 0f)
				{
					num3 = 100f;
				}
				float[] array = new float[count];
				for (int k = 0; k < count; k++)
				{
					array[k] = (float)battleAICommandData.m_ExecDatas[k].m_Ratio / num3;
				}
				float num4 = UnityEngine.Random.Range(0f, 1f);
				float num5 = 0f;
				for (int l = 0; l < count; l++)
				{
					if (num4 >= num5 && num4 <= num5 + array[l])
					{
						num2 = l;
						break;
					}
					num5 += array[l];
				}
				if (num2 == -1)
				{
					num2 = 0;
				}
				BattleAIExecData battleAIExecData = battleAICommandData.m_ExecDatas[num2];
				out_result.m_CommandIndex = battleAIExecData.m_CommandIndex;
				int count2 = commands.Count;
				if (out_result.m_CommandIndex >= count2)
				{
					out_result.m_CommandIndex = 0;
				}
				out_result.m_TargetJoin = BattleAIParser.SolveTargetJoin(executor, commands[out_result.m_CommandIndex], battleAIExecData.m_IsCommandTargetSelectionInOrder, battleAIExecData.m_SingleConditions, singleTargetPriorityWhenHateSame);
				executor.CharaBattle.MyParty.SingleTargetIndex = out_result.m_TargetJoin;
				executor.CharaBattle.SetAIWriteFlags(battleAIExecData.m_AIFlags);
				executor.CharaBattle.IncrementAIExecedNum(patternIndex, num);
				return true;
			}
			out_result.m_CommandIndex = 0;
			out_result.m_TargetJoin = BattleAIParser.SolveTargetJoin(executor, commands[out_result.m_CommandIndex], false, null, singleTargetPriorityWhenHateSame);
			executor.CharaBattle.MyParty.SingleTargetIndex = out_result.m_TargetJoin;
			return false;
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00014C68 File Offset: 0x00013068
		private static bool JudgeBattleAIPatternChangeConditions(CharacterHandler executor, List<BattleAIPatternChangeCondition> conditions)
		{
			for (int i = 0; i < conditions.Count; i++)
			{
				if (!BattleAIParser.JudgeBattleAIPatternChangeCondition(executor, conditions[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x00014CA4 File Offset: 0x000130A4
		private static bool JudgeBattleAIPatternChangeCondition(CharacterHandler executor, BattleAIPatternChangeCondition condition)
		{
			eBattleAIPatternChangeConditionType type = condition.m_Type;
			return type != eBattleAIPatternChangeConditionType.DeadCount && false;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x00014CCC File Offset: 0x000130CC
		private static bool JudgeBattleAICommandConditions(CharacterHandler executor, List<BattleAICommandCondition> conditions)
		{
			for (int i = 0; i < conditions.Count; i++)
			{
				if (!BattleAIParser.JudgeBattleAICommandCondition(executor, conditions[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x00014D08 File Offset: 0x00013108
		private static bool JudgeBattleAICommandCondition(CharacterHandler executor, BattleAICommandCondition condition)
		{
			eBattleAICommandConditionType type = condition.m_Type;
			switch (type)
			{
			case eBattleAICommandConditionType.TgtParty_AliveNum:
				return BattleAIParser.Judge_TgtParty_AliveNum(executor, condition);
			case eBattleAICommandConditionType.TgtParty_StateAbnormalCount:
				return BattleAIParser.Judge_TgtParty_StateAbnormalCount(executor, condition);
			case eBattleAICommandConditionType.TgtParty_NormalAttackUseCount:
				return BattleAIParser.Judge_TgtParty_NormalAttackUseCount(executor, condition);
			case eBattleAICommandConditionType.TgtParty_SkillUseCount:
				return BattleAIParser.Judge_TgtParty_SkillUseCount(executor, condition);
			case eBattleAICommandConditionType.TgtParty_TogetherAttackUseCount:
				return BattleAIParser.Judge_TgtParty_TogetherAttackUseCount(executor, condition);
			case eBattleAICommandConditionType.TgtParty_MemberChangeCount:
				return BattleAIParser.Judge_TgtParty_MemberChangeCount(executor, condition);
			default:
				switch (type)
				{
				case eBattleAICommandConditionType.Self_HpRange:
					return BattleAIParser.Judge_Self_HpRange(executor, condition);
				case eBattleAICommandConditionType.Self_StateAbnormal:
					return BattleAIParser.Judge_Self_StateAbnormal(executor, condition);
				case eBattleAICommandConditionType.Self_StatusChange:
					return BattleAIParser.Judge_Self_StatusChange(executor, condition);
				case eBattleAICommandConditionType.Self_ChargeCount:
					return BattleAIParser.Judge_Self_ChargeCount(executor, condition);
				default:
					if (type != eBattleAICommandConditionType.MyParty_AliveNum)
					{
						return type == eBattleAICommandConditionType.Flag && BattleAIParser.Judge_Flag(executor, condition);
					}
					return BattleAIParser.Judge_MyParty_AliveNum(executor, condition);
				}
				break;
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x00014DCE File Offset: 0x000131CE
		private static bool Judge_Self_HpRange(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return executor.CharaBattle.Param.IsHpRange(condition.m_Args[0] / 100f, condition.m_Args[1] / 100f);
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x00014DFC File Offset: 0x000131FC
		private static bool Judge_Self_StateAbnormal(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			if (num == 0)
			{
				if (num2 == 0)
				{
					if (!executor.CharaBattle.Param.IsEnableStateAbnormalAny())
					{
						return true;
					}
				}
				else if (executor.CharaBattle.Param.IsEnableStateAbnormalAny())
				{
					return true;
				}
				return false;
			}
			if (num != 1)
			{
				eStateAbnormal stateAbnormal = (eStateAbnormal)(num - 2);
				bool flag = executor.CharaBattle.Param.IsEnableStateAbnormal(stateAbnormal);
				if (num2 == 0)
				{
					if (flag)
					{
						return true;
					}
				}
				else if (!flag)
				{
					return true;
				}
				return false;
			}
			if (num2 == 0)
			{
				if (executor.CharaBattle.Param.IsEnableStateAbnormalAny())
				{
					return true;
				}
			}
			else if (!executor.CharaBattle.Param.IsEnableStateAbnormalAny())
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x00014ED8 File Offset: 0x000132D8
		private static bool Judge_Self_StatusChange(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			int[] statusBuffFlags = executor.CharaBattle.Param.GetStatusBuffFlags();
			if (num >= 0 && num < 6)
			{
				BattleDefine.eStatus eStatus = (BattleDefine.eStatus)num;
				int num3 = statusBuffFlags[(int)eStatus];
				if (num2 != 0)
				{
					if (num3 <= 0)
					{
						return true;
					}
				}
				else if (num3 > 0)
				{
					return true;
				}
			}
			else if (num >= 6 && num < 12)
			{
				BattleDefine.eStatus eStatus2 = (BattleDefine.eStatus)(num - 6);
				int num4 = statusBuffFlags[(int)eStatus2];
				if (num2 != 0)
				{
					if (num4 >= 0)
					{
						return true;
					}
				}
				else if (num4 < 0)
				{
					return true;
				}
			}
			else if (num == 13)
			{
				if (num2 != 0)
				{
					bool flag = true;
					for (int i = 0; i < statusBuffFlags.Length; i++)
					{
						if (statusBuffFlags[i] > 0)
						{
							flag = false;
						}
					}
					if (flag)
					{
						return true;
					}
				}
				else
				{
					for (int j = 0; j < statusBuffFlags.Length; j++)
					{
						if (statusBuffFlags[j] > 0)
						{
							return true;
						}
					}
				}
			}
			else if (num == 14)
			{
				if (num2 != 0)
				{
					bool flag2 = true;
					for (int k = 0; k < statusBuffFlags.Length; k++)
					{
						if (statusBuffFlags[k] < 0)
						{
							flag2 = false;
						}
					}
					if (flag2)
					{
						return true;
					}
				}
				else
				{
					for (int l = 0; l < statusBuffFlags.Length; l++)
					{
						if (statusBuffFlags[l] < 0)
						{
							return true;
						}
					}
				}
			}
			else if (num2 != 0)
			{
				for (int m = 0; m < statusBuffFlags.Length; m++)
				{
					if (statusBuffFlags[m] != 0)
					{
						return true;
					}
				}
			}
			else
			{
				bool flag3 = true;
				for (int n = 0; n < statusBuffFlags.Length; n++)
				{
					if (statusBuffFlags[n] != 0)
					{
						flag3 = false;
					}
				}
				if (flag3)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x000150FC File Offset: 0x000134FC
		private static bool Judge_Self_ChargeCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			int chargeCount = executor.CharaBattle.Param.GetChargeCount();
			if (num2 != 0)
			{
				if (num2 != 1)
				{
					if (num2 == 2)
					{
						if (chargeCount == num)
						{
							return true;
						}
					}
				}
				else if (chargeCount <= num)
				{
					return true;
				}
			}
			else if (chargeCount >= num)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x00015174 File Offset: 0x00013574
		private static bool Judge_MyParty_AliveNum(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			int joinCharaNum = executor.CharaBattle.MyParty.GetJoinCharaNum(true);
			if (num2 != 0)
			{
				if (num2 != 1)
				{
					if (num2 == 2)
					{
						if (joinCharaNum == num)
						{
							return true;
						}
					}
				}
				else if (joinCharaNum <= num)
				{
					return true;
				}
			}
			else if (joinCharaNum >= num)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x000151EC File Offset: 0x000135EC
		private static bool Judge_TgtParty_AliveNum(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			int joinCharaNum = executor.CharaBattle.TgtParty.GetJoinCharaNum(true);
			if (num2 != 0)
			{
				if (num2 != 1)
				{
					if (num2 == 2)
					{
						if (joinCharaNum == num)
						{
							return true;
						}
					}
				}
				else if (joinCharaNum <= num)
				{
					return true;
				}
			}
			else if (joinCharaNum >= num)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x00015264 File Offset: 0x00013664
		private static bool Judge_TgtParty_StateAbnormalCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int num2 = (int)condition.m_Args[1];
			int num3 = (int)condition.m_Args[2];
			int num4;
			if (num != 0)
			{
				if (num != 1)
				{
					eStateAbnormal stateAbnormal = (eStateAbnormal)(num - 2);
					num4 = executor.CharaBattle.TgtParty.GetStateAbnormalCountSpecific(stateAbnormal, true);
				}
				else
				{
					num4 = executor.CharaBattle.TgtParty.GetStateAbnormalCountAny(true);
				}
			}
			else
			{
				num4 = executor.CharaBattle.TgtParty.GetStateAbnormalCountNone(true);
			}
			if (num3 != 0)
			{
				if (num3 != 1)
				{
					if (num3 == 2)
					{
						if (num4 == num2)
						{
							return true;
						}
					}
				}
				else if (num4 <= num2)
				{
					return true;
				}
			}
			else if (num4 >= num2)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x00015334 File Offset: 0x00013734
		private static bool Judge_TgtParty_NormalAttackUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int actionCount = executor.CharaBattle.TgtParty.GetActionCount(BattlePartyData.eActionCountType.NormalAttackUseCount);
			return actionCount >= num;
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x00015368 File Offset: 0x00013768
		private static bool Judge_TgtParty_SkillUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int actionCount = executor.CharaBattle.TgtParty.GetActionCount(BattlePartyData.eActionCountType.SkillUseCount);
			return actionCount >= num;
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0001539C File Offset: 0x0001379C
		private static bool Judge_TgtParty_TogetherAttackUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int actionCount = executor.CharaBattle.TgtParty.GetActionCount(BattlePartyData.eActionCountType.TogetherAttackUseCount);
			return actionCount >= num;
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x000153D0 File Offset: 0x000137D0
		private static bool Judge_TgtParty_MemberChangeCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			int num = (int)condition.m_Args[0];
			int actionCount = executor.CharaBattle.TgtParty.GetActionCount(BattlePartyData.eActionCountType.MemberChangeCount);
			return actionCount >= num;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x00015404 File Offset: 0x00013804
		private static bool Judge_Flag(CharacterHandler executor, BattleAICommandCondition condition)
		{
			List<BattleAIFlag> list = new List<BattleAIFlag>();
			int num = condition.m_Args.Length / 2;
			for (int i = 0; i < num; i++)
			{
				list.Add(new BattleAIFlag
				{
					m_ID = (int)condition.m_Args[i * 2],
					m_IsFlag = ((int)condition.m_Args[i * 2 + 1] == 1)
				});
			}
			return executor.CharaBattle.JudgeAIFlags(list);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x00015488 File Offset: 0x00013888
		private static BattleDefine.eJoinMember SolveTargetJoin(CharacterHandler executor, BattleCommandData commandData, bool isCommandTargetSelectionInOrder, List<BattleAICommandTargetSingleCondition> singleConditions, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame)
		{
			eSkillTargetType mainSkillTargetType = commandData.MainSkillTargetType;
			List<CharacterHandler> list;
			if (mainSkillTargetType != eSkillTargetType.TgtSingle)
			{
				if (mainSkillTargetType != eSkillTargetType.MySingle)
				{
					return BattleDefine.eJoinMember.None;
				}
				list = executor.CharaBattle.MyParty.GetJoinMembers(true);
			}
			else
			{
				list = executor.CharaBattle.TgtParty.GetJoinMembers(true);
			}
			if (list.Count == 1)
			{
				return list[0].CharaBattle.Join;
			}
			List<CharacterHandler> list2 = new List<CharacterHandler>(list);
			List<CharacterHandler> list3 = new List<CharacterHandler>();
			if (isCommandTargetSelectionInOrder)
			{
				for (int i = 0; i < list.Count; i++)
				{
					float hateChangeValue = list[i].CharaBattle.Param.GetHateChangeValue();
					if (hateChangeValue > 0f)
					{
						list3.Add(list[i]);
					}
					else if (hateChangeValue < 0f)
					{
						list2.Remove(list[i]);
					}
				}
				if (list3.Count == 1)
				{
					return list3[0].CharaBattle.Join;
				}
				if (list3.Count > 1)
				{
					list = list3;
				}
				else if (list2.Count != 0)
				{
					if (list2.Count == 1)
					{
						return list2[0].CharaBattle.Join;
					}
					BattleDefine.eJoinMember eJoinMember = executor.CharaBattle.GetOldSingleTargetIndex(commandData.MainSkillTargetType);
					bool flag = false;
					while (!flag)
					{
						eJoinMember = (eJoinMember + 1) % BattleDefine.eJoinMember.Bench_1;
						for (int j = 0; j < list2.Count; j++)
						{
							if (list2[j].CharaBattle.Join == eJoinMember)
							{
								flag = true;
								break;
							}
						}
					}
					return eJoinMember;
				}
			}
			List<BattleAIParser.BattleAIHateResult> list4 = new List<BattleAIParser.BattleAIHateResult>();
			int min = (int)BattleUtility.DefVal(eBattleDefineDB.HateValueMin);
			int num = (int)BattleUtility.DefVal(eBattleDefineDB.HateValueMax);
			int num2 = (int)BattleUtility.DefVal(eBattleDefineDB.HateAIConditionCoef);
			for (int k = 0; k < list.Count; k++)
			{
				BattleAIParser.BattleAIHateResult item;
				item.m_Join = list[k].CharaBattle.Join;
				item.m_HateValue = (float)UnityEngine.Random.Range(min, num + 1);
				float hateChangeValue2 = list[k].CharaBattle.Param.GetHateChangeValue();
				item.m_HateValue *= 1f + hateChangeValue2;
				if (!isCommandTargetSelectionInOrder)
				{
					item.m_HitSingleConditionCount = BattleAIParser.CalcHitSingleConditionCount(executor, list[k], singleConditions);
					item.m_HateValue += (float)(num2 * item.m_HitSingleConditionCount);
					if (list[k].CharaBattle.Join == BattleDefine.eJoinMember.Join_1)
					{
						item.m_HateValue += BattleUtility.DefVal(eBattleDefineDB.HateValueJoin1);
					}
				}
				else
				{
					item.m_HitSingleConditionCount = 0;
				}
				list4.Add(item);
			}
			return BattleAIParser.SolveTargetJoinFromHateResults(list4, singleTargetPriorityWhenHateSame);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x00015764 File Offset: 0x00013B64
		private static BattleDefine.eJoinMember SolveTargetJoinFromHateResults(List<BattleAIParser.BattleAIHateResult> hateResults, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame)
		{
			bool flag = true;
			bool flag2 = true;
			for (int i = 1; i < hateResults.Count; i++)
			{
				if (hateResults[i].m_HateValue != hateResults[i - 1].m_HateValue)
				{
					flag = false;
				}
				if (hateResults[i].m_HitSingleConditionCount != hateResults[i - 1].m_HitSingleConditionCount)
				{
					flag2 = false;
				}
			}
			if (!flag)
			{
				if (BattleAIParser.<>f__mg$cache0 == null)
				{
					BattleAIParser.<>f__mg$cache0 = new Comparison<BattleAIParser.BattleAIHateResult>(BattleAIParser.CompareHateResult);
				}
				Sort.StableSort<BattleAIParser.BattleAIHateResult>(hateResults, BattleAIParser.<>f__mg$cache0);
				int index = 0;
				int num = 1;
				for (int j = 1; j < hateResults.Count; j++)
				{
					if (hateResults[j].m_HateValue != hateResults[index].m_HateValue)
					{
						break;
					}
					num++;
				}
				for (int k = 1; k < num; k++)
				{
					if (hateResults[k].m_HitSingleConditionCount != hateResults[index].m_HitSingleConditionCount)
					{
						num = k;
						break;
					}
				}
				if (num > 1)
				{
					if (singleTargetPriorityWhenHateSame == eSingleTargetPriorityWhenHateSame.ABC)
					{
						if (BattleAIParser.<>f__mg$cache1 == null)
						{
							BattleAIParser.<>f__mg$cache1 = new Comparison<BattleAIParser.BattleAIHateResult>(BattleAIParser.CompareHateResult_JoinABC);
						}
						hateResults.Sort(BattleAIParser.<>f__mg$cache1);
					}
					else
					{
						if (BattleAIParser.<>f__mg$cache2 == null)
						{
							BattleAIParser.<>f__mg$cache2 = new Comparison<BattleAIParser.BattleAIHateResult>(BattleAIParser.CompareHateResult_JoinCBA);
						}
						hateResults.Sort(BattleAIParser.<>f__mg$cache2);
					}
				}
				return hateResults[0].m_Join;
			}
			if (flag2)
			{
				return hateResults[0].m_Join;
			}
			int index2 = 0;
			for (int l = 1; l < hateResults.Count; l++)
			{
				if (hateResults[l].m_HitSingleConditionCount > hateResults[index2].m_HitSingleConditionCount)
				{
					index2 = l;
				}
			}
			return hateResults[index2].m_Join;
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0001597C File Offset: 0x00013D7C
		private static int CompareHateResult(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			if (A.m_HateValue > B.m_HateValue)
			{
				return -1;
			}
			if (A.m_HateValue < B.m_HateValue)
			{
				return 1;
			}
			if (A.m_HitSingleConditionCount > B.m_HitSingleConditionCount)
			{
				return -1;
			}
			if (A.m_HitSingleConditionCount < B.m_HitSingleConditionCount)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x000159DE File Offset: 0x00013DDE
		private static int CompareHateResult_JoinABC(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			if (A.m_Join > B.m_Join)
			{
				return 1;
			}
			if (A.m_Join < B.m_Join)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x00015A0B File Offset: 0x00013E0B
		private static int CompareHateResult_JoinCBA(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			if (A.m_Join > B.m_Join)
			{
				return -1;
			}
			if (A.m_Join < B.m_Join)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x00015A38 File Offset: 0x00013E38
		private static int CalcHitSingleConditionCount(CharacterHandler executor, CharacterHandler targetCharaHndl, List<BattleAICommandTargetSingleCondition> singleConditions)
		{
			int num = 0;
			if (singleConditions != null)
			{
				for (int i = 0; i < singleConditions.Count; i++)
				{
					switch (singleConditions[i].m_Type)
					{
					case eBattleAICommandTargetSingleConditionType.Dying:
						if (BattleAIParser.Judge_Dying(targetCharaHndl, singleConditions[i]))
						{
							num++;
						}
						break;
					case eBattleAICommandTargetSingleConditionType.Element:
						if (BattleAIParser.Judge_Element(targetCharaHndl, singleConditions[i]))
						{
							num++;
						}
						break;
					case eBattleAICommandTargetSingleConditionType.Class:
						if (BattleAIParser.Judge_Class(targetCharaHndl, singleConditions[i]))
						{
							num++;
						}
						break;
					case eBattleAICommandTargetSingleConditionType.StateAbnormal:
						if (BattleAIParser.Judge_StateAbnormal(targetCharaHndl, singleConditions[i]))
						{
							num++;
						}
						break;
					case eBattleAICommandTargetSingleConditionType.WeakElement:
						if (BattleAIParser.Judge_WeakElement(executor, targetCharaHndl, singleConditions[i]))
						{
							num++;
						}
						break;
					}
				}
			}
			return num;
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x00015B1C File Offset: 0x00013F1C
		private static bool Judge_Dying(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			int num = (int)((float)charaHndl.CharaBattle.Param.FixedMaxHp() * (BattleUtility.DefVal(eBattleDefineDB.DyingHpRatio) * 0.01f));
			return charaHndl.CharaBattle.Param.GetHp() <= num;
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x00015B64 File Offset: 0x00013F64
		private static bool Judge_Element(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			eElementType eElementType = (eElementType)singleCondition.m_Args[0];
			return charaHndl.CharaParam.ElementType == eElementType;
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x00015B90 File Offset: 0x00013F90
		private static bool Judge_Class(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			eClassType eClassType = (eClassType)singleCondition.m_Args[0];
			return charaHndl.CharaParam.ClassType == eClassType;
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x00015BBC File Offset: 0x00013FBC
		private static bool Judge_StateAbnormal(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			int num = (int)singleCondition.m_Args[0];
			if (num != 0)
			{
				if (num != 1)
				{
					eStateAbnormal stateAbnormal = (eStateAbnormal)(num - 2);
					if (charaHndl.CharaBattle.Param.IsEnableStateAbnormal(stateAbnormal))
					{
						return true;
					}
				}
				else if (charaHndl.CharaBattle.Param.IsEnableStateAbnormalAny())
				{
					return true;
				}
			}
			else if (!charaHndl.CharaBattle.Param.IsEnableStateAbnormalAny())
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x00015C40 File Offset: 0x00014040
		private static bool Judge_WeakElement(CharacterHandler executor, CharacterHandler targetCharaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			eElementType strongElementType = BattleCommandParser.GetStrongElementType(executor.CharaParam.ElementType);
			return targetCharaHndl.CharaParam.ElementType == strongElementType;
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x00015C74 File Offset: 0x00014074
		private static void RequestResetActionCount(BattlePartyData party, List<BattleAICommandCondition> conditions)
		{
			for (int i = 0; i < conditions.Count; i++)
			{
				switch (conditions[i].m_Type)
				{
				case eBattleAICommandConditionType.TgtParty_NormalAttackUseCount:
					party.RequestResetActionCount(BattlePartyData.eActionCountType.NormalAttackUseCount);
					break;
				case eBattleAICommandConditionType.TgtParty_SkillUseCount:
					party.RequestResetActionCount(BattlePartyData.eActionCountType.SkillUseCount);
					break;
				case eBattleAICommandConditionType.TgtParty_TogetherAttackUseCount:
					party.RequestResetActionCount(BattlePartyData.eActionCountType.TogetherAttackUseCount);
					break;
				case eBattleAICommandConditionType.TgtParty_MemberChangeCount:
					party.RequestResetActionCount(BattlePartyData.eActionCountType.MemberChangeCount);
					break;
				}
			}
		}

		// Token: 0x040002B9 RID: 697
		[CompilerGenerated]
		private static Comparison<BattleAIParser.BattleAIHateResult> <>f__mg$cache0;

		// Token: 0x040002BA RID: 698
		[CompilerGenerated]
		private static Comparison<BattleAIParser.BattleAIHateResult> <>f__mg$cache1;

		// Token: 0x040002BB RID: 699
		[CompilerGenerated]
		private static Comparison<BattleAIParser.BattleAIHateResult> <>f__mg$cache2;

		// Token: 0x0200009A RID: 154
		private struct BattleAIHateResult
		{
			// Token: 0x040002BC RID: 700
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x040002BD RID: 701
			public float m_HateValue;

			// Token: 0x040002BE RID: 702
			public int m_HitSingleConditionCount;
		}
	}
}
