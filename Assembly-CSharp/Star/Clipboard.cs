﻿using System;

namespace Star
{
	// Token: 0x02000640 RID: 1600
	public static class Clipboard
	{
		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06001FA9 RID: 8105 RVA: 0x000ABD2B File Offset: 0x000AA12B
		// (set) Token: 0x06001FA8 RID: 8104 RVA: 0x000ABD23 File Offset: 0x000AA123
		public static string value
		{
			get
			{
				return Clipboard_Android.GetClipboard();
			}
			set
			{
				Clipboard_Android.SetClipboard(value);
			}
		}
	}
}
