﻿using System;

namespace Star
{
	// Token: 0x0200016D RID: 365
	[Serializable]
	public struct CharacterExpDB_Param
	{
		// Token: 0x040009C4 RID: 2500
		public int m_NextExp;

		// Token: 0x040009C5 RID: 2501
		public int m_UpgradeAmount;
	}
}
