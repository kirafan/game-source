﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200031B RID: 795
	public class FieldBuildMap
	{
		// Token: 0x06000F17 RID: 3863 RVA: 0x00050CA3 File Offset: 0x0004F0A3
		public FieldBuildMap(FieldBuildMapReq prequest, FieldBuildCmdQue pcmdque)
		{
			this.m_ReqManage = prequest;
			this.m_ComQue = pcmdque;
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00050CC4 File Offset: 0x0004F0C4
		public void EntryCallbackBuildUp(BuildUpCallback pbuildup, BuildUpCallback pchangeup)
		{
			this.m_BuildUpCallback = pbuildup;
			this.m_ChangeCallback = pchangeup;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x00050CD4 File Offset: 0x0004F0D4
		public void PlayRequest(FieldBuildMapReq prequest, long ftimekey)
		{
			TownSearchResult townSearchResult = default(TownSearchResult);
			int num = this.m_BuildList.Count;
			for (int i = 0; i < num; i++)
			{
				this.m_BuildList[i].m_StateUp = false;
			}
			num = prequest.GetRequestNum();
			for (int i = 0; i < num; i++)
			{
				FieldObjHandleBuild buildPointState = this.GetBuildPointState(prequest.GetRequest(i).m_OldPointMngID);
				if (buildPointState != null)
				{
					buildPointState.DelCharaManageID(prequest.GetRequest(i).m_ManageID);
				}
			}
			for (int i = 0; i < num; i++)
			{
				townSearchResult.ClearParam();
				prequest.GetRequest(i).m_BfCallback(this, ref townSearchResult);
				FieldObjHandleBuild buildPointState = this.GetBuildPointState(townSearchResult.m_MoveTargetMngID);
				if (buildPointState != null)
				{
					if (buildPointState.CheckBuildInChara(prequest.GetRequest(i).m_ManageID))
					{
						buildPointState.AddCharaManageID(prequest.GetRequest(i).m_ManageID);
						prequest.GetRequest(i).m_AfCallBack(townSearchResult.m_UpState, buildPointState.m_UserMngID, ftimekey);
					}
					else
					{
						prequest.GetRequest(i).m_AfCallBack(eTownMoveState.None, buildPointState.m_UserMngID, ftimekey);
					}
				}
			}
			num = this.m_BuildList.Count;
			for (int i = 0; i < num; i++)
			{
				if (this.m_BuildList[i].m_StateUp && this.m_BuildList[i].m_Callback != null)
				{
					this.m_BuildList[i].m_Callback(eCallBackType.StayChange, this.m_BuildList[i], true);
				}
			}
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x00050E74 File Offset: 0x0004F274
		public void SetUpTownData()
		{
			UserTownData townData = UserDataUtil.TownData;
			int buildObjectDataNum = townData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = townData.GetBuildObjectDataAt(i);
				FieldObjHandleBuild fieldObjHandleBuild = new FieldObjHandleBuild();
				fieldObjHandleBuild.SetUpBuild(buildObjectDataAt.m_ManageID, buildObjectDataAt.m_BuildPointIndex);
				this.m_BuildList.Add(fieldObjHandleBuild);
			}
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00050ED0 File Offset: 0x0004F2D0
		public static UserTownData.BuildObjectData CreateTownBuildData(int fbuildpoint, int fobjid, long fsetmngid, bool fnewup, long fsettime)
		{
			bool flag = false;
			if (fnewup)
			{
				flag = UserTownUtil.IsBuildMarks(fobjid);
			}
			return UserTownUtil.CreateUserTownBuildData(fsetmngid, fbuildpoint, fobjid, !flag, fsettime, 0L);
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x00050F00 File Offset: 0x0004F300
		private void CallbackCompleteAddTownBuild(IFldNetComModule pmodule)
		{
			FldComBuildScript fldComBuildScript = pmodule as FldComBuildScript;
			if (fldComBuildScript != null)
			{
				TownMain[] array = UnityEngine.Object.FindObjectsOfType<TownMain>();
				if (array != null && array.Length > 0)
				{
					TownComAddBuildComEnd pcmd = new TownComAddBuildComEnd(fldComBuildScript.m_BuildUp);
					array[0].OnComEvent(pcmd);
				}
			}
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x00050F44 File Offset: 0x0004F344
		public void AddTownBuildInMakeID(int fbuildpoint, int fobjid, long fsetmngid, bool fnewup)
		{
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.SetMarkTime(ScheduleTimeUtil.GetSystemTimeMs());
			int pointToBuildType = TownUtility.GetPointToBuildType(fbuildpoint);
			if (pointToBuildType != 0)
			{
				if (pointToBuildType != 268435456)
				{
					if (pointToBuildType != 536870912)
					{
						if (pointToBuildType != 1073741824)
						{
						}
					}
				}
			}
			else
			{
				if (this.GetFieldBuildPointState(fbuildpoint) == null)
				{
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddObj, 1L, UserTownUtil.GetSettingOptionKey(1), fbuildpoint, new IFldNetComModule.CallBack(this.CallbackCompleteAddTownBuild));
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddBuildIn, -1L, 1, fbuildpoint, null);
				}
				fbuildpoint = TownUtility.MakeBuildContentID(fbuildpoint);
			}
			if (fnewup)
			{
				fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddObj, (long)fobjid, 0, fbuildpoint, new IFldNetComModule.CallBack(this.CallbackCompleteAddTownBuild));
			}
			else
			{
				UserTownObjectData townObjData = UserTownUtil.GetTownObjData(fsetmngid);
				if (townObjData != null)
				{
					townObjData.OpenState = UserTownObjectData.eOpenState.Open;
					townObjData.BuildPoint = fbuildpoint;
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ObjStateChg, fsetmngid, fobjid, fbuildpoint, null);
				}
			}
			fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddBuildIn, fsetmngid, fobjid, fbuildpoint, new IFldNetComModule.CallBack(this.CallbackAddTownBuildIn));
			fldComBuildScript.PlaySend();
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x0005104C File Offset: 0x0004F44C
		private void CallbackAddTownBuildIn(IFldNetComModule pmodule)
		{
			FldComBuildScript fldComBuildScript = pmodule as FldComBuildScript;
			for (int i = 0; i < fldComBuildScript.m_BuildUp.Count; i++)
			{
				FieldObjHandleBuild fieldObjHandleBuild = new FieldObjHandleBuild();
				fieldObjHandleBuild.SetUpBuild(fldComBuildScript.m_BuildUp[i].m_ManageID, fldComBuildScript.m_BuildUp[i].m_BuildPoint);
				this.m_BuildList.Add(fieldObjHandleBuild);
				if (this.m_BuildUpCallback != null)
				{
					this.m_BuildUpCallback(fldComBuildScript.m_BuildUp[i].m_BuildPoint, fldComBuildScript.m_BuildUp[i].m_ObjID, fldComBuildScript.m_BuildUp[i].m_ManageID);
				}
			}
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x00051100 File Offset: 0x0004F500
		public void ChangeTownBuildObject(int fbuildpoint, int fobjid, long fmanageid, Action<bool> pcallback = null)
		{
			if (fbuildpoint != -1 && UserTownUtil.CheckPointToBuilding(fbuildpoint, fobjid))
			{
				FldComBuildScript fldComBuildScript = new FldComBuildScript();
				fldComBuildScript.SetMarkTime(ScheduleTimeUtil.GetSystemTimeMs());
				if (fmanageid == -1L)
				{
					if (UserTownUtil.IsAddTownObjData(fobjid))
					{
						fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddObj, (long)fobjid, 0, fbuildpoint, new IFldNetComModule.CallBack(this.CallbackCompleteAddTownBuild));
					}
					else
					{
						fmanageid = UserTownUtil.GetStoreTownObj(fobjid);
						UserTownObjectData townObjData = UserTownUtil.GetTownObjData(fmanageid);
						townObjData.OpenState = UserTownObjectData.eOpenState.Open;
						townObjData.BuildPoint = fbuildpoint;
						fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ObjStateChg, fmanageid, fobjid, fbuildpoint, null);
					}
				}
				else
				{
					UserTownObjectData townObjData = UserTownUtil.GetTownObjData(fmanageid);
					townObjData.OpenState = UserTownObjectData.eOpenState.Open;
					townObjData.BuildPoint = fbuildpoint;
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ObjStateChg, fmanageid, fobjid, fbuildpoint, null);
				}
				long num = this.RemoveTownBuildObject(-1L, fbuildpoint, null);
				if (num != -1L)
				{
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.StoreObjState, num, 0, -1, null);
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.AddBuildIn, fmanageid, fobjid, fbuildpoint, new IFldNetComModule.CallBack(this.CallbackBuildMake));
				}
				fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.CallEvt, 0L, 0, 0, new IFldNetComModule.CallBack(this.CallbackBuildStateChg));
				fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ChangeBuild, 0L, 0, 0, null);
				fldComBuildScript.PlaySend();
				FieldBuildCmdAdd pque = new FieldBuildCmdAdd(fmanageid, fbuildpoint, fobjid);
				this.m_ComQue.AddComQue(pque, pcallback);
			}
			else
			{
				Debug.LogWarning("Town Buildding Point Error\n");
			}
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x00051234 File Offset: 0x0004F634
		private void CallbackBuildMake(IFldNetComModule pmodule)
		{
			FldComBuildScript fldComBuildScript = pmodule as FldComBuildScript;
			for (int i = 0; i < fldComBuildScript.m_BuildUp.Count; i++)
			{
				FieldObjHandleBuild fieldObjHandleBuild = new FieldObjHandleBuild();
				fieldObjHandleBuild.SetUpBuild(fldComBuildScript.m_BuildUp[i].m_ManageID, fldComBuildScript.m_BuildUp[i].m_BuildPoint);
				this.m_BuildList.Add(fieldObjHandleBuild);
			}
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x000512A0 File Offset: 0x0004F6A0
		private void CallbackBuildStateChg(IFldNetComModule pmodule)
		{
			FldComBuildScript fldComBuildScript = pmodule as FldComBuildScript;
			for (int i = 0; i < fldComBuildScript.m_BuildUp.Count; i++)
			{
				this.ChangeTownBuildInMakeID(fldComBuildScript.m_BuildUp[i].m_BuildPoint, fldComBuildScript.m_BuildUp[i].m_ObjID, fldComBuildScript.m_BuildUp[i].m_ManageID);
				if (this.m_ChangeCallback != null)
				{
					this.m_ChangeCallback(fldComBuildScript.m_BuildUp[i].m_BuildPoint, fldComBuildScript.m_BuildUp[i].m_ObjID, fldComBuildScript.m_BuildUp[i].m_ManageID);
				}
			}
			FieldBuildCmdAdd fieldBuildCmdAdd = this.m_ComQue.GetQue(typeof(FieldBuildCmdAdd)) as FieldBuildCmdAdd;
			if (fieldBuildCmdAdd != null)
			{
				fieldBuildCmdAdd.CheckCallback(true);
			}
		}

		// Token: 0x06000F22 RID: 3874 RVA: 0x0005137C File Offset: 0x0004F77C
		public void ChangeTownBuildInMakeID(int fbuildpoint, int fobjid, long fsetmngid)
		{
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == fbuildpoint)
				{
					this.m_BuildList[i].m_TownObjectID = fobjid;
					this.m_BuildList[i].m_UserMngID = fsetmngid;
					break;
				}
			}
		}

		// Token: 0x06000F23 RID: 3875 RVA: 0x000513E8 File Offset: 0x0004F7E8
		public long RemoveTownBuildObject(long fmanageid, int fbuildpoint, Action<bool> pcallback = null)
		{
			int count = this.m_BuildList.Count;
			long result = -1L;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == fbuildpoint)
				{
					result = this.m_BuildList[i].m_UserMngID;
					UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_BuildList[i].m_UserMngID);
					if (townObjData == null)
					{
						this.m_BuildList[i].m_UserMngID = UserTownUtil.MakeBuildObjectToFreeManageID();
						UserTownUtil.AddTownObjData(new UserTownObjectData(this.m_BuildList[i].m_TownObjectID, this.m_BuildList[i].m_UserMngID));
						townObjData = UserTownUtil.GetTownObjData(this.m_BuildList[i].m_UserMngID);
					}
					townObjData.OpenState = UserTownObjectData.eOpenState.Non;
					UserDataUtil.TownData.RemoveBuildObjectDataAtBuildPointIndex(this.m_BuildList[i].m_BuildPoint);
					if (fmanageid != -1L)
					{
						FldComBuildScript fldComBuildScript = new FldComBuildScript();
						fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.StoreObjState, fmanageid, 0, -1, null);
						fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.DelBuild, 0L, 0, 0, new IFldNetComModule.CallBack(this.CallbackBuildRemove));
						fldComBuildScript.PlaySend();
						FieldBuildCmdRemove pque = new FieldBuildCmdRemove(fmanageid, fbuildpoint);
						this.m_ComQue.AddComQue(pque, pcallback);
					}
					else
					{
						this.m_BuildList[i].BackBuildToRemove(this.m_ReqManage, false);
						this.m_BuildList.RemoveAt(i);
					}
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F24 RID: 3876 RVA: 0x00051558 File Offset: 0x0004F958
		private void CallbackBuildRemove(IFldNetComModule pmodule)
		{
			FieldBuildCmdRemove fieldBuildCmdRemove = this.m_ComQue.GetQue(typeof(FieldBuildCmdRemove)) as FieldBuildCmdRemove;
			if (fieldBuildCmdRemove != null)
			{
				int count = this.m_BuildList.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_BuildList[i].m_BuildPoint == fieldBuildCmdRemove.m_BuildPoint)
					{
						this.m_BuildList[i].BackBuildToRemove(this.m_ReqManage, true);
						this.m_BuildList.RemoveAt(i);
						break;
					}
				}
				fieldBuildCmdRemove.CheckCallback(true);
				this.m_ComQue.RemoveQue(typeof(FieldBuildCmdRemove));
			}
		}

		// Token: 0x06000F25 RID: 3877 RVA: 0x00051608 File Offset: 0x0004FA08
		public FieldObjHandleBuild GetBuildPointState(long fbuildmngid)
		{
			FieldObjHandleBuild result = null;
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_UserMngID == fbuildmngid)
				{
					result = this.m_BuildList[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x00051660 File Offset: 0x0004FA60
		public void AreaSwapBuildTable(int fpointindex1, int fpointindex2)
		{
			List<FieldBuildMap.CBuildStateEvent> list = new List<FieldBuildMap.CBuildStateEvent>();
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (TownUtility.GetBuildAreaID(this.m_BuildList[i].m_BuildPoint) == fpointindex1)
				{
					this.m_BuildList[i].m_BuildPoint = TownUtility.ChangeBuildAreaID(this.m_BuildList[i].m_BuildPoint, 255);
				}
			}
			for (int i = 0; i < count; i++)
			{
				if (TownUtility.GetBuildAreaID(this.m_BuildList[i].m_BuildPoint) == fpointindex2)
				{
					int num = TownUtility.ChangeBuildAreaID(this.m_BuildList[i].m_BuildPoint, fpointindex1);
					this.m_BuildList[i].m_BuildPoint = num;
					UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID).ChangeBuildPoint(num, true);
					if (this.m_BuildList[i].m_Callback != null)
					{
						list.Add(new FieldBuildMap.CBuildStateEvent(this.m_BuildList[i], eCallBackType.SwapBuild));
					}
				}
			}
			for (int i = 0; i < count; i++)
			{
				if (TownUtility.GetBuildAreaID(this.m_BuildList[i].m_BuildPoint) == 255)
				{
					int num = TownUtility.ChangeBuildAreaID(this.m_BuildList[i].m_BuildPoint, fpointindex2);
					this.m_BuildList[i].m_BuildPoint = num;
					UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID).ChangeBuildPoint(num, true);
					if (this.m_BuildList[i].m_Callback != null)
					{
						list.Add(new FieldBuildMap.CBuildStateEvent(this.m_BuildList[i], eCallBackType.SwapBuild));
					}
				}
			}
			count = list.Count;
			for (int i = 0; i < count; i++)
			{
				list[i].m_Base.m_Callback(list[i].m_Type, list[i].m_Base, false);
			}
			list.Clear();
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.CheckBuildState, 0L, 0, 0, null);
			fldComBuildScript.PlaySend();
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x00051898 File Offset: 0x0004FC98
		public void AreaMoveBuild(int fpointindex1, int fpointindex2)
		{
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (TownUtility.GetBuildAreaID(this.m_BuildList[i].m_BuildPoint) == fpointindex1)
				{
					this.m_BuildList[i].m_BuildPoint = TownUtility.ChangeBuildAreaID(this.m_BuildList[i].m_BuildPoint, fpointindex2);
					UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID).ChangeBuildPoint(this.m_BuildList[i].m_BuildPoint, true);
					if (this.m_BuildList[i].m_Callback != null)
					{
						this.m_BuildList[i].m_Callback(eCallBackType.MoveBuild, this.m_BuildList[i], false);
					}
				}
			}
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.CheckBuildState, 0L, 0, 0, null);
			fldComBuildScript.PlaySend();
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x00051990 File Offset: 0x0004FD90
		public void BuildSwapPointTable(int fpoint1, int fpoint2)
		{
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == fpoint1)
				{
					this.m_BuildList[i].m_BuildPoint = -1;
					break;
				}
			}
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == fpoint2)
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID);
					buildObjectDataAtBuildMngID.ChangeBuildPoint(fpoint1, true);
					this.m_BuildList[i].m_BuildPoint = fpoint1;
					if (this.m_BuildList[i].m_Callback != null)
					{
						this.m_BuildList[i].m_Callback(eCallBackType.SwapBuild, this.m_BuildList[i], false);
					}
					break;
				}
			}
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == -1)
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID);
					buildObjectDataAtBuildMngID.ChangeBuildPoint(fpoint2, true);
					this.m_BuildList[i].m_BuildPoint = fpoint2;
					if (this.m_BuildList[i].m_Callback != null)
					{
						this.m_BuildList[i].m_Callback(eCallBackType.SwapBuild, this.m_BuildList[i], false);
					}
					break;
				}
			}
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.CheckBuildState, 0L, 0, 0, null);
			fldComBuildScript.PlaySend();
		}

		// Token: 0x06000F29 RID: 3881 RVA: 0x00051B40 File Offset: 0x0004FF40
		public void BuildMovePoint(int fnowpoint, int fnextpoint)
		{
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].m_BuildPoint == fnowpoint)
				{
					this.m_BuildList[i].m_BuildPoint = fnextpoint;
					UserDataUtil.TownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID).ChangeBuildPoint(fnextpoint, true);
					if (this.m_BuildList[i].m_Callback != null)
					{
						this.m_BuildList[i].m_Callback(eCallBackType.MoveBuild, this.m_BuildList[i], false);
					}
					FldComBuildScript fldComBuildScript = new FldComBuildScript();
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.CheckBuildState, 0L, 0, 0, null);
					fldComBuildScript.PlaySend();
					break;
				}
			}
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x00051C10 File Offset: 0x00050010
		public void ChangeBuildLevelUp(int fbuildpoint, long fmanageid, bool fopen)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			FieldObjHandleBuild buildPointState = this.GetBuildPointState(fmanageid);
			if (buildPointState != null)
			{
				for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex == fbuildpoint)
					{
						long systemTimeMs = ScheduleTimeUtil.GetSystemTimeMs();
						buildObjectDataAt.ChangeLevelUp(systemTimeMs, fopen);
						FldComBuildScript fldComBuildScript = new FldComBuildScript();
						fldComBuildScript.SetMarkTime(systemTimeMs);
						if (fopen)
						{
							fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.LevelUpBuild, fmanageid, 1, 0, new IFldNetComModule.CallBack(this.CallbackLevelUpBuild));
						}
						else
						{
							fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.LevelUpWait, fmanageid, 0, 0, null);
						}
						fldComBuildScript.PlaySend();
						break;
					}
				}
			}
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x00051CC0 File Offset: 0x000500C0
		private void CallbackLevelUpBuild(IFldNetComModule pmodule)
		{
			FldComBuildScript fldComBuildScript = pmodule as FldComBuildScript;
			for (int i = 0; i < fldComBuildScript.m_BuildUp.Count; i++)
			{
				FieldObjHandleBuild buildPointState = this.GetBuildPointState(fldComBuildScript.m_BuildUp[i].m_ManageID);
				if (buildPointState != null && buildPointState.m_Callback != null)
				{
					buildPointState.m_Callback(eCallBackType.LevelUp, buildPointState, false);
				}
			}
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x00051D28 File Offset: 0x00050128
		public FieldObjHandleBuild GetFieldBuildState(long fmanageid)
		{
			FieldObjHandleBuild result = null;
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i] != null && this.m_BuildList[i].m_UserMngID == fmanageid)
				{
					result = this.m_BuildList[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x00051D90 File Offset: 0x00050190
		public FieldObjHandleBuild GetFieldBuildPointState(int fbuildpoint)
		{
			FieldObjHandleBuild result = null;
			int count = this.m_BuildList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i] != null && this.m_BuildList[i].m_BuildPoint == fbuildpoint)
				{
					result = this.m_BuildList[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x00051DF8 File Offset: 0x000501F8
		public bool GetCheckBuildAreaMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			bool result = false;
			TownAccessCheck.ChrCheckDataBase chrCheckDataBase = new TownAccessCheck.ChrCheckDataBase(fchrmngid);
			TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
			int count = this.m_BuildList.Count;
			presult.ClearParam();
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].CheckBuildInChara(fchrmngid))
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID);
					if (buildObjectDataAtBuildMngID.m_IsOpen && TownUtility.GetPointToBuildType(buildObjectDataAtBuildMngID.m_BuildPointIndex) == 536870912)
					{
						buildCheckDataBase.SetBuildParam(buildObjectDataAtBuildMngID.m_ManageID);
						if (TownAccessCheck.IsAccessSearchBuild(ref chrCheckDataBase, ref buildCheckDataBase, fscheduletag))
						{
							presult.m_MoveTargetMngID = buildObjectDataAtBuildMngID.m_ManageID;
							presult.m_UpState = eTownMoveState.TargetMove;
							result = true;
							break;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x00051ED4 File Offset: 0x000502D4
		public bool GetFreeBufAccessMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			bool result = false;
			List<int> list = new List<int>();
			int count = this.m_BuildList.Count;
			presult.ClearParam();
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].CheckBuildInChara(fchrmngid))
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID);
					if (buildObjectDataAtBuildMngID.m_IsOpen)
					{
						list.Add(i);
					}
				}
			}
			count = list.Count;
			if (count != 0)
			{
				TownAccessCheck.ChrCheckDataBase chrCheckDataBase = new TownAccessCheck.ChrCheckDataBase(fchrmngid);
				TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
				for (int i = 0; i < count; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[list[i]].m_UserMngID);
					buildCheckDataBase.SetBuildParam(buildObjectDataAtBuildMngID.m_ManageID);
					if (TownAccessCheck.IsAccessSearchBuild(ref chrCheckDataBase, ref buildCheckDataBase, fscheduletag))
					{
						presult.m_MoveTargetMngID = buildObjectDataAtBuildMngID.m_ManageID;
						presult.m_UpState = eTownMoveState.TargetMove;
						result = true;
						break;
					}
				}
			}
			list.Clear();
			return result;
		}

		// Token: 0x06000F30 RID: 3888 RVA: 0x00052004 File Offset: 0x00050404
		public bool GetCheckBuildMenuMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			TownAccessCheck.ChrCheckDataBase chrCheckDataBase = new TownAccessCheck.ChrCheckDataBase(fchrmngid);
			TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
			int count = this.m_BuildList.Count;
			presult.ClearParam();
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildList[i].CheckBuildInChara(fchrmngid))
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(this.m_BuildList[i].m_UserMngID);
					buildCheckDataBase.SetBuildParam(buildObjectDataAtBuildMngID.m_ManageID);
					if (TownAccessCheck.IsAccessSearchBuild(ref chrCheckDataBase, ref buildCheckDataBase, fscheduletag))
					{
						presult.m_MoveTargetMngID = buildObjectDataAtBuildMngID.m_ManageID;
						presult.m_UpState = eTownMoveState.TargetMove;
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0400169C RID: 5788
		public List<FieldObjHandleBuild> m_BuildList = new List<FieldObjHandleBuild>();

		// Token: 0x0400169D RID: 5789
		private BuildUpCallback m_BuildUpCallback;

		// Token: 0x0400169E RID: 5790
		private BuildUpCallback m_ChangeCallback;

		// Token: 0x0400169F RID: 5791
		private FieldBuildMapReq m_ReqManage;

		// Token: 0x040016A0 RID: 5792
		private FieldBuildCmdQue m_ComQue;

		// Token: 0x0200031C RID: 796
		public class CBuildStateEvent
		{
			// Token: 0x06000F31 RID: 3889 RVA: 0x000520BD File Offset: 0x000504BD
			public CBuildStateEvent(FieldObjHandleBuild pbase, eCallBackType ftype)
			{
				this.m_Base = pbase;
				this.m_Type = ftype;
			}

			// Token: 0x040016A1 RID: 5793
			public FieldObjHandleBuild m_Base;

			// Token: 0x040016A2 RID: 5794
			public eCallBackType m_Type;
		}

		// Token: 0x0200031D RID: 797
		public class ListUpCheckState
		{
			// Token: 0x040016A3 RID: 5795
			public int m_PointIndex;

			// Token: 0x040016A4 RID: 5796
			public float m_Per;

			// Token: 0x040016A5 RID: 5797
			public bool m_FitMove;
		}
	}
}
