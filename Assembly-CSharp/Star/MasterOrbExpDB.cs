﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001CF RID: 463
	public class MasterOrbExpDB : ScriptableObject
	{
		// Token: 0x04000B02 RID: 2818
		public MasterOrbExpDB_Param[] m_Params;
	}
}
