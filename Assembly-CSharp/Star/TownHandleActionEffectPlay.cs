﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006A7 RID: 1703
	public class TownHandleActionEffectPlay : ITownHandleAction
	{
		// Token: 0x06002216 RID: 8726 RVA: 0x000B5509 File Offset: 0x000B3909
		public TownHandleActionEffectPlay(int feffectno, Vector3 fpos, Vector3 fsize)
		{
			this.m_Type = eTownEventAct.PlayEffect;
			this.m_EffectNo = feffectno;
			this.m_OffsetPos = fpos;
			this.m_Size = fsize;
		}

		// Token: 0x0400289B RID: 10395
		public int m_EffectNo;

		// Token: 0x0400289C RID: 10396
		public Vector3 m_OffsetPos;

		// Token: 0x0400289D RID: 10397
		public Vector3 m_Size;
	}
}
