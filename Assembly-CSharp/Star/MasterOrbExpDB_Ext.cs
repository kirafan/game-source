﻿using System;

namespace Star
{
	// Token: 0x02000197 RID: 407
	public static class MasterOrbExpDB_Ext
	{
		// Token: 0x06000AF3 RID: 2803 RVA: 0x000414E8 File Offset: 0x0003F8E8
		public static int GetNextExp(this MasterOrbExpDB self, int currentLv, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				int num2 = num + currentLv - 1;
				if (num2 >= 0 && num2 < self.m_Params.Length)
				{
					return self.m_Params[num2].m_NextExp;
				}
			}
			return 0;
		}

		// Token: 0x06000AF4 RID: 2804 RVA: 0x00041564 File Offset: 0x0003F964
		public static long[] GetNextMaxExps(this MasterOrbExpDB self, int lv_a, int lv_b, sbyte expTableID)
		{
			int num = lv_b - lv_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(lv_a + i, expTableID);
			}
			return array;
		}

		// Token: 0x06000AF5 RID: 2805 RVA: 0x000415A8 File Offset: 0x0003F9A8
		public static long GetCurrentExp(this MasterOrbExpDB self, int currentLv, long exp, sbyte expTableID)
		{
			long num = 0L;
			for (int i = 1; i < currentLv; i++)
			{
				num += (long)self.GetNextExp(i, expTableID);
			}
			if (exp >= num)
			{
				return exp - num;
			}
			return 0L;
		}
	}
}
