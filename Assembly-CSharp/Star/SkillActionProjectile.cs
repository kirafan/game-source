﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000131 RID: 305
	public class SkillActionProjectile
	{
		// Token: 0x060007E8 RID: 2024 RVA: 0x00033482 File Offset: 0x00031882
		public SkillActionProjectile(string callbackKey, string effectID, int sortingOrder, Transform parent, List<CharacterHandler> damageTargetCharaHndls)
		{
			this.CommonConstruct(callbackKey, effectID, sortingOrder, parent);
			this.m_DamageTargetCharaHndls = damageTargetCharaHndls;
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060007E9 RID: 2025 RVA: 0x0003349D File Offset: 0x0003189D
		public SkillActionMovementBase Movement
		{
			get
			{
				return this.m_Movement;
			}
		}

		// Token: 0x060007EA RID: 2026 RVA: 0x000334A8 File Offset: 0x000318A8
		private void CommonConstruct(string callbackKey, string effectID, int sortingOrder, Transform parent)
		{
			this.m_CallbackKey = callbackKey;
			this.m_Obj = new GameObject("Projectile");
			this.m_Transform = this.m_Obj.transform;
			this.m_Transform.SetParent(parent, false);
			this.m_EffectHndl = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged(effectID, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), null, false, WrapMode.ClampForever, sortingOrder, 1f, false);
			if (this.m_EffectHndl != null)
			{
				this.m_EffectHndl.CacheTransform.SetParent(this.m_Transform, false);
			}
		}

		// Token: 0x060007EB RID: 2027 RVA: 0x00033548 File Offset: 0x00031948
		public void Destroy()
		{
			if (this.m_EffectHndl != null)
			{
				SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(this.m_EffectHndl);
				this.m_EffectHndl = null;
			}
			if (this.m_Obj != null)
			{
				UnityEngine.Object.Destroy(this.m_Obj);
				this.m_Obj = null;
			}
			this.m_Movement = null;
			this.OnArrival = null;
		}

		// Token: 0x060007EC RID: 2028 RVA: 0x000335B0 File Offset: 0x000319B0
		public void Update()
		{
			if (this.m_Movement != null && this.m_Movement.IsMoving)
			{
				int num = -1;
				Vector3 zero = Vector3.zero;
				if (!this.m_Movement.Update(out num, out zero) && this.m_EffectHndl != null)
				{
					this.m_EffectHndl.DisableOtherThanParticles(0.15f);
				}
				if (num != -1)
				{
					if (this.m_DamageTargetCharaHndls != null)
					{
						if (this.m_DamageTargetCharaHndls != null && num < this.m_DamageTargetCharaHndls.Count)
						{
							this.OnArrival.Call(this.m_CallbackKey, zero, this.m_DamageTargetCharaHndls[num]);
						}
					}
					else
					{
						this.OnArrival.Call(this.m_CallbackKey, zero, null);
					}
				}
			}
		}

		// Token: 0x060007ED RID: 2029 RVA: 0x00033679 File Offset: 0x00031A79
		public void MoveStraight(bool isLocalPosition, Vector3 startPos, Vector3 targetPos, float speed)
		{
			this.m_Movement = new SkillActionMovementStraight(this.m_Transform, isLocalPosition, startPos, targetPos, speed);
			this.m_Movement.Play();
		}

		// Token: 0x060007EE RID: 2030 RVA: 0x0003369C File Offset: 0x00031A9C
		public void MoveParabola(bool isLocalPosition, Vector3 startPos, Vector3 targetPos, float arrivalSec, float gravityAccell)
		{
			this.m_Movement = new SkillActionMovementParabola(this.m_Transform, isLocalPosition, startPos, targetPos, arrivalSec, gravityAccell);
			this.m_Movement.Play();
		}

		// Token: 0x060007EF RID: 2031 RVA: 0x000336C1 File Offset: 0x00031AC1
		public void MovePenetrate(bool isLocalPosition, Vector3 startPos, List<Vector3> targetPosList, float speed, float delaySec, float aliveSec)
		{
			this.m_Movement = new SkillActionMovementPenetrate(this.m_Transform, isLocalPosition, startPos, targetPosList, speed, delaySec, aliveSec);
			this.m_Movement.Play();
		}

		// Token: 0x040007B0 RID: 1968
		private const float FADE_IN_TIME_ON_ARRIVAL = 0.15f;

		// Token: 0x040007B1 RID: 1969
		private GameObject m_Obj;

		// Token: 0x040007B2 RID: 1970
		private Transform m_Transform;

		// Token: 0x040007B3 RID: 1971
		private EffectHandler m_EffectHndl;

		// Token: 0x040007B4 RID: 1972
		private SkillActionMovementBase m_Movement;

		// Token: 0x040007B5 RID: 1973
		private List<CharacterHandler> m_DamageTargetCharaHndls;

		// Token: 0x040007B6 RID: 1974
		private string m_CallbackKey;

		// Token: 0x040007B7 RID: 1975
		public Action<string, Vector3, CharacterHandler> OnArrival;
	}
}
