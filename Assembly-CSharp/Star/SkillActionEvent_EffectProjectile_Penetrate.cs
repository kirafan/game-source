﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000117 RID: 279
	[Serializable]
	public class SkillActionEvent_EffectProjectile_Penetrate : SkillActionEvent_Base
	{
		// Token: 0x0400071A RID: 1818
		public string m_CallbackKey;

		// Token: 0x0400071B RID: 1819
		public string m_EffectID;

		// Token: 0x0400071C RID: 1820
		public short m_UniqueID;

		// Token: 0x0400071D RID: 1821
		public float m_Speed;

		// Token: 0x0400071E RID: 1822
		public float m_DelaySec;

		// Token: 0x0400071F RID: 1823
		public float m_AliveSec;

		// Token: 0x04000720 RID: 1824
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x04000721 RID: 1825
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x04000722 RID: 1826
		public Vector2 m_StartPosOffset;

		// Token: 0x04000723 RID: 1827
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x04000724 RID: 1828
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x04000725 RID: 1829
		public Vector2 m_TargetPosOffset;
	}
}
