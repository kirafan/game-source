﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x0200072A RID: 1834
	public class TownEventActBackCharaBind : ITownEventCommand
	{
		// Token: 0x06002430 RID: 9264 RVA: 0x000C23AA File Offset: 0x000C07AA
		public TownEventActBackCharaBind(long fmanageid, int fsyncuid)
		{
			this.m_Type = eTownRequest.BackCharaBind;
			this.m_LinkManageID = fmanageid;
			this.m_SyncUID = fsyncuid;
			this.m_Enable = true;
			this.m_LinkChara = new List<ITownObjectHandler>();
		}

		// Token: 0x06002431 RID: 9265 RVA: 0x000C23D9 File Offset: 0x000C07D9
		public void AddChara(ITownObjectHandler pchara)
		{
			this.m_LinkChara.Add(pchara);
		}

		// Token: 0x06002432 RID: 9266 RVA: 0x000C23E8 File Offset: 0x000C07E8
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (pbuilder.IsActiveSyncKey(this.m_SyncUID))
			{
				pbuilder.DelSyncKey(this.m_SyncUID);
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.Rebind);
				for (int i = 0; i < this.m_LinkChara.Count; i++)
				{
					this.m_LinkChara[i].RecvEvent(pact);
				}
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002B2E RID: 11054
		public long m_LinkManageID;

		// Token: 0x04002B2F RID: 11055
		public List<ITownObjectHandler> m_LinkChara;

		// Token: 0x04002B30 RID: 11056
		public int m_SyncUID;
	}
}
