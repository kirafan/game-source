﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000735 RID: 1845
	public class TownResourceBind : MonoBehaviour
	{
		// Token: 0x04002B4B RID: 11083
		[SerializeField]
		public TownResourceBind.Resource[] m_Table;

		// Token: 0x04002B4C RID: 11084
		[SerializeField]
		public Vector3 m_PosOffset = Vector3.zero;

		// Token: 0x02000736 RID: 1846
		[Serializable]
		public struct Resource
		{
			// Token: 0x04002B4D RID: 11085
			public UnityEngine.Object m_Res;
		}
	}
}
