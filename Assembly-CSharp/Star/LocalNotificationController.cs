﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using PreviewLabs;
using UnityEngine;

namespace Star
{
	// Token: 0x0200064C RID: 1612
	public class LocalNotificationController : MonoBehaviour
	{
		// Token: 0x0600205F RID: 8287 RVA: 0x000AD4F1 File Offset: 0x000AB8F1
		private void Awake()
		{
			LocalNotificationController.m_notificationIds = new List<int>();
		}

		// Token: 0x06002060 RID: 8288 RVA: 0x000AD4FD File Offset: 0x000AB8FD
		public static void RegisterForNotifications()
		{
		}

		// Token: 0x06002061 RID: 8289 RVA: 0x000AD4FF File Offset: 0x000AB8FF
		public static void SimpleNotification(int id, TimeSpan delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "", Color color = default(Color))
		{
			LocalNotificationController.SimpleNotification(id, (int)delay.TotalSeconds, title, message, smallIcon, largeIcon, color);
		}

		// Token: 0x06002062 RID: 8290 RVA: 0x000AD518 File Offset: 0x000AB918
		public static void SimpleNotification(int id, int delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "", Color color = default(Color))
		{
			LocalNotificationController.SendNotification(id, (long)delay, title, message, smallIcon, largeIcon, color, true, true, true);
		}

		// Token: 0x06002063 RID: 8291 RVA: 0x000AD540 File Offset: 0x000AB940
		public static void SendNotification(int id, long delay, string title, string message, string smallIcon, string bigIcon = "", Color32 bgColor = default(Color32), bool sound = true, bool vibrate = true, bool lights = true)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.NotificationUtils");
			if (androidJavaClass != null)
			{
				Debug.Log("NotificationPlutin Call.");
				androidJavaClass.CallStatic("RegisterLocalNotification", new object[]
				{
					id,
					delay * 1000L,
					title,
					message,
					message,
					(!sound) ? 0 : 1,
					(!vibrate) ? 0 : 1,
					(!lights) ? 0 : 1,
					bigIcon,
					smallIcon,
					(int)bgColor.r * 65536 + (int)bgColor.g * 256 + (int)bgColor.b,
					"com.amazonaws.unity.CustomUnityPlayerActivity"
				});
			}
			if (LocalNotificationController.m_notificationIds != null && !LocalNotificationController.m_notificationIds.Contains(id))
			{
				LocalNotificationController.m_notificationIds.Add(id);
			}
			LocalNotificationController.SavePrefs();
		}

		// Token: 0x06002064 RID: 8292 RVA: 0x000AD650 File Offset: 0x000ABA50
		public static void UnregisterNotification(int id)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.NotificationUtils");
			if (androidJavaClass != null)
			{
				androidJavaClass.CallStatic("UnregisterLocalNotification", new object[]
				{
					id
				});
			}
			if (LocalNotificationController.m_notificationIds != null && LocalNotificationController.m_notificationIds.Contains(id))
			{
				LocalNotificationController.m_notificationIds.Remove(id);
				LocalNotificationController.SavePrefs();
			}
		}

		// Token: 0x06002065 RID: 8293 RVA: 0x000AD6B4 File Offset: 0x000ABAB4
		public static void UnregisterNotificationAll()
		{
			LocalNotificationController.LoadPrefs();
			if (LocalNotificationController.m_notificationIds != null)
			{
				List<int> list = new List<int>(LocalNotificationController.m_notificationIds);
				foreach (int num in list)
				{
					Debug.Log("cancel id " + num);
					LocalNotificationController.UnregisterNotification(num);
				}
			}
		}

		// Token: 0x06002066 RID: 8294 RVA: 0x000AD73C File Offset: 0x000ABB3C
		private void OnApplicationPause(bool isPause)
		{
			if (isPause)
			{
			}
		}

		// Token: 0x06002067 RID: 8295 RVA: 0x000AD74C File Offset: 0x000ABB4C
		private static void SavePrefs()
		{
			string value = LocalNotificationController.Serialize<List<int>>(LocalNotificationController.m_notificationIds);
			if (!string.IsNullOrEmpty(value))
			{
				PreviewLabs.PlayerPrefs.SetString("LocalNotificationIds", value);
			}
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x000AD77C File Offset: 0x000ABB7C
		private static void LoadPrefs()
		{
			string @string = PreviewLabs.PlayerPrefs.GetString("LocalNotificationIds", string.Empty);
			if (!string.IsNullOrEmpty(@string))
			{
				LocalNotificationController.m_notificationIds = LocalNotificationController.Deserialize<List<int>>(@string);
			}
			if (LocalNotificationController.m_notificationIds == null)
			{
				LocalNotificationController.m_notificationIds = new List<int>();
			}
		}

		// Token: 0x06002069 RID: 8297 RVA: 0x000AD7C4 File Offset: 0x000ABBC4
		private static string Serialize<T>(T obj)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, obj);
			return Convert.ToBase64String(memoryStream.GetBuffer());
		}

		// Token: 0x0600206A RID: 8298 RVA: 0x000AD7F8 File Offset: 0x000ABBF8
		private static T Deserialize<T>(string str)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream serializationStream = new MemoryStream(Convert.FromBase64String(str));
			return (T)((object)binaryFormatter.Deserialize(serializationStream));
		}

		// Token: 0x040026C0 RID: 9920
		private const string fullClassName = "com.amazonaws.unity.NotificationUtils";

		// Token: 0x040026C1 RID: 9921
		private const string mainActivityClassName = "com.amazonaws.unity.CustomUnityPlayerActivity";

		// Token: 0x040026C2 RID: 9922
		private const string PREFS_KEY = "LocalNotificationIds";

		// Token: 0x040026C3 RID: 9923
		private static List<int> m_notificationIds;
	}
}
