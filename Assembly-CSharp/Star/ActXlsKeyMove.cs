﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000569 RID: 1385
	public class ActXlsKeyMove : ActXlsKeyBase
	{
		// Token: 0x06001B0F RID: 6927 RVA: 0x0008F67C File Offset: 0x0008DA7C
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_MoveTime);
			pio.Float(ref this.m_Speed);
			pio.Float(ref this.m_Offset.x);
			pio.Float(ref this.m_Offset.y);
			pio.Float(ref this.m_Offset.z);
			int func = (int)this.m_Func;
			pio.Int(ref func);
			this.m_Func = (CActScriptKeyMove.eMoveType)func;
		}

		// Token: 0x040021FA RID: 8698
		public CActScriptKeyMove.eMoveType m_Func;

		// Token: 0x040021FB RID: 8699
		public float m_Speed;

		// Token: 0x040021FC RID: 8700
		public int m_MoveTime;

		// Token: 0x040021FD RID: 8701
		public Vector3 m_Offset;
	}
}
