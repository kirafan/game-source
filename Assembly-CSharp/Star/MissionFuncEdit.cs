﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004FB RID: 1275
	public class MissionFuncEdit : IMissionFunction
	{
		// Token: 0x06001925 RID: 6437 RVA: 0x00082910 File Offset: 0x00080D10
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			switch (pparam.m_ExtensionScriptID)
			{
			case 0:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 1:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 2:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 3:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 4:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 5:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 6:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 7:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			}
			return result;
		}
	}
}
