﻿using System;

namespace Star
{
	// Token: 0x0200025D RID: 605
	[Serializable]
	public struct UniqueSkillSoundDB_Data
	{
		// Token: 0x040013BA RID: 5050
		public int m_PlayFrame;

		// Token: 0x040013BB RID: 5051
		public string m_CueID;
	}
}
