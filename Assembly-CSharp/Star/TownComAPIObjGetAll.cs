﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x0200068A RID: 1674
	public class TownComAPIObjGetAll : INetComHandle
	{
		// Token: 0x060021BB RID: 8635 RVA: 0x000B39EA File Offset: 0x000B1DEA
		public TownComAPIObjGetAll()
		{
			this.ApiName = "player/town_facility/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}
	}
}
