﻿using System;

namespace Star
{
	// Token: 0x020002AE RID: 686
	public abstract class UpgradeResultBeforeAfterPairPartCharacterWrapper : CharacterEditResultSceneBeforeAfterPairPartBasedOutputCharaParamCharacterDataWrapper<EditMain.UpgradeResultData>
	{
		// Token: 0x06000CE6 RID: 3302 RVA: 0x000490A6 File Offset: 0x000474A6
		public UpgradeResultBeforeAfterPairPartCharacterWrapper(eCharacterDataType characterDataType, EditMain.UpgradeResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}
	}
}
