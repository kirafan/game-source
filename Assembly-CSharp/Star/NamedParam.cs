﻿using System;

namespace Star
{
	// Token: 0x02000156 RID: 342
	[Serializable]
	public class NamedParam
	{
		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x060009CA RID: 2506 RVA: 0x0003C3A2 File Offset: 0x0003A7A2
		// (set) Token: 0x060009CB RID: 2507 RVA: 0x0003C3AA File Offset: 0x0003A7AA
		public int FriendShip { get; set; }

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x060009CC RID: 2508 RVA: 0x0003C3B3 File Offset: 0x0003A7B3
		// (set) Token: 0x060009CD RID: 2509 RVA: 0x0003C3BB File Offset: 0x0003A7BB
		public int FriendShipMax { get; set; }

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x060009CE RID: 2510 RVA: 0x0003C3C4 File Offset: 0x0003A7C4
		public bool IsFriendShipMax
		{
			get
			{
				return this.FriendShip >= this.FriendShipMax;
			}
		}
	}
}
