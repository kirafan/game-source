﻿using System;

namespace Star
{
	// Token: 0x020002A2 RID: 674
	public class UserCharacterDataWrapper : UserCharacterDataWrapperBase
	{
		// Token: 0x06000CA5 RID: 3237 RVA: 0x00048A30 File Offset: 0x00046E30
		public UserCharacterDataWrapper(UserCharacterData in_data) : base(eCharacterDataType.UserCharacterData, in_data)
		{
		}
	}
}
