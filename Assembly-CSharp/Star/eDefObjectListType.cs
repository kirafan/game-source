﻿using System;

namespace Star
{
	// Token: 0x02000305 RID: 773
	public enum eDefObjectListType
	{
		// Token: 0x040015FB RID: 5627
		Build,
		// Token: 0x040015FC RID: 5628
		Room,
		// Token: 0x040015FD RID: 5629
		BG,
		// Token: 0x040015FE RID: 5630
		Wall,
		// Token: 0x040015FF RID: 5631
		Floor,
		// Token: 0x04001600 RID: 5632
		Bedding,
		// Token: 0x04001601 RID: 5633
		System,
		// Token: 0x04001602 RID: 5634
		FldChara
	}
}
