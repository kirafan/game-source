﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002E9 RID: 745
	public class EquipWeaponManagedSupportPartiesController : EquipWeaponManagedPartiesControllerExGen<EquipWeaponManagedSupportPartiesController, EquipWeaponManagedSupportPartyController>
	{
		// Token: 0x06000E80 RID: 3712 RVA: 0x0004E0B8 File Offset: 0x0004C4B8
		public override EquipWeaponManagedSupportPartiesController SetupEx()
		{
			base.Setup();
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			List<UserSupportPartyData> userSupportPartyDatas = userDataMng.UserSupportPartyDatas;
			this.parties = new EquipWeaponManagedSupportPartyController[userSupportPartyDatas.Count];
			for (int i = 0; i < this.parties.Length; i++)
			{
				this.parties[i] = new EquipWeaponManagedSupportPartyController().SetupEx(i);
			}
			return this;
		}

		// Token: 0x06000E81 RID: 3713 RVA: 0x0004E11C File Offset: 0x0004C51C
		public override int HowManyPartyMemberAll()
		{
			return 8;
		}

		// Token: 0x06000E82 RID: 3714 RVA: 0x0004E11F File Offset: 0x0004C51F
		public override int HowManyPartyMemberEnable()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.SupportLimit;
		}
	}
}
