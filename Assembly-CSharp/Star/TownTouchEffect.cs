﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200069F RID: 1695
	public class TownTouchEffect : MonoBehaviour
	{
		// Token: 0x060021FF RID: 8703 RVA: 0x000B4E64 File Offset: 0x000B3264
		public static TownTouchEffect CreateTouchEffect()
		{
			GameObject gameObject = new GameObject("Touch_Effect");
			return gameObject.AddComponent<TownTouchEffect>();
		}

		// Token: 0x06002200 RID: 8704 RVA: 0x000B4E84 File Offset: 0x000B3284
		public void TouchFirst(Vector2 fpos)
		{
			this.m_TouchBack = fpos;
			this.m_TouchPos = fpos;
			TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(4);
			townEffectPlayer.SetTrs(FldUICamera.CalcScreenCanvasPos(this.m_TouchPos), Quaternion.identity, Vector3.one, 40);
		}

		// Token: 0x06002201 RID: 8705 RVA: 0x000B4EC8 File Offset: 0x000B32C8
		public void TouchUp(Vector2 fpos)
		{
			this.m_TouchPos = fpos;
			if ((this.m_TouchBack - this.m_TouchPos).magnitude >= 20f)
			{
				TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(5);
				townEffectPlayer.SetTrs(FldUICamera.CalcScreenCanvasPos(this.m_TouchPos), Quaternion.identity, Vector3.one, 40);
				TownTouchLineEffect townTouchLineEffect = townEffectPlayer.gameObject.AddComponent<TownTouchLineEffect>();
				townTouchLineEffect.TouchPos(FldUICamera.CalcScreenCanvasPos(this.m_TouchPos), FldUICamera.CalcScreenCanvasPos(this.m_TouchBack));
				this.m_TouchBack = fpos;
			}
		}

		// Token: 0x06002202 RID: 8706 RVA: 0x000B4F5C File Offset: 0x000B335C
		private void Update()
		{
		}

		// Token: 0x06002203 RID: 8707 RVA: 0x000B4F5E File Offset: 0x000B335E
		public void DestoryRequest()
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}

		// Token: 0x04002875 RID: 10357
		private Vector2 m_TouchPos;

		// Token: 0x04002876 RID: 10358
		private Vector2 m_TouchBack;
	}
}
