﻿using System;

namespace Star
{
	// Token: 0x020004B1 RID: 1201
	public class TownComManager
	{
		// Token: 0x06001770 RID: 6000 RVA: 0x00079511 File Offset: 0x00077911
		public TownComManager(int ftablenum)
		{
			this.m_Table = new IMainComCommand[ftablenum];
			this.m_Num = 0;
			this.m_Max = ftablenum;
		}

		// Token: 0x06001771 RID: 6001 RVA: 0x00079533 File Offset: 0x00077933
		public void PushComCommand(IMainComCommand pcmd)
		{
			if (this.m_Num < this.m_Max)
			{
				this.m_Table[this.m_Num] = pcmd;
				this.m_Num++;
			}
		}

		// Token: 0x06001772 RID: 6002 RVA: 0x00079562 File Offset: 0x00077962
		public int GetComCommandNum()
		{
			return this.m_Num;
		}

		// Token: 0x06001773 RID: 6003 RVA: 0x0007956A File Offset: 0x0007796A
		public IMainComCommand GetComCommand(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x06001774 RID: 6004 RVA: 0x00079574 File Offset: 0x00077974
		public void Flush()
		{
			for (int i = 0; i < this.m_Num; i++)
			{
				this.m_Table[i] = null;
			}
			this.m_Num = 0;
		}

		// Token: 0x04001E32 RID: 7730
		public int m_Num;

		// Token: 0x04001E33 RID: 7731
		public int m_Max;

		// Token: 0x04001E34 RID: 7732
		public IMainComCommand[] m_Table;
	}
}
