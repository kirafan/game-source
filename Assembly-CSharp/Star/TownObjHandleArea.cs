﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020006D1 RID: 1745
	public class TownObjHandleArea : ITownObjectHandler
	{
		// Token: 0x17000254 RID: 596
		// (get) Token: 0x0600228B RID: 8843 RVA: 0x000B88A7 File Offset: 0x000B6CA7
		// (set) Token: 0x0600228A RID: 8842 RVA: 0x000B8888 File Offset: 0x000B6C88
		public override bool isUnderConstruction
		{
			get
			{
				return this.m_isUnderConstruction;
			}
			set
			{
				this.m_isUnderConstruction = value;
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, value);
			}
		}

		// Token: 0x0600228C RID: 8844 RVA: 0x000B88B0 File Offset: 0x000B6CB0
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(fmanageid, buildPointIndex);
			buildMngIDTreeNode.SetBuilding(true);
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			FieldMapManager.EntryBuildState(fmanageid, new FeedBackResultCallback(this.CallbackAreaBuild));
		}

		// Token: 0x0600228D RID: 8845 RVA: 0x000B88F8 File Offset: 0x000B6CF8
		public bool IsCompleteAreaEvt()
		{
			return false;
		}

		// Token: 0x0600228E RID: 8846 RVA: 0x000B88FC File Offset: 0x000B6CFC
		private void CreateBuildTree(TownBuildPakage ppakage, GameObject proot, int fbuildpoint, long fbuildmngid, bool ftype)
		{
			TownBuildTree buildTree = this.m_Builder.GetBuildTree();
			TownBuildLinkPoint townBuildLinkPoint;
			if (ftype)
			{
				townBuildLinkPoint = new TownBuildLinkPoint(eBuildPointCategory.Contents);
			}
			else
			{
				townBuildLinkPoint = new TownBuildLinkPoint(eBuildPointCategory.Buf);
			}
			townBuildLinkPoint.m_Object = proot;
			townBuildLinkPoint.m_Pakage = ppakage;
			townBuildLinkPoint.m_AccessID = fbuildpoint;
			townBuildLinkPoint.m_AccessMngID = fbuildmngid;
			buildTree.AddBuildPoint(townBuildLinkPoint);
		}

		// Token: 0x0600228F RID: 8847 RVA: 0x000B8954 File Offset: 0x000B6D54
		protected override bool PrepareMain()
		{
			bool result = false;
			if (this.m_BuildStep == 0 && base.PrepareMain())
			{
				TownBuildPakage component = base.gameObject.GetComponent<TownBuildPakage>();
				this.m_BuildUpList = new TownObjHandleArea.CAreaPointSetUp();
				TownObjHandleArea.MakeBuildPoint(this.m_Transform, this.m_BuildUpList, component.m_Index, this.m_LayerID, 6);
				if (!this.m_InitUp)
				{
					float sizePer = this.m_Builder.m_GameCamera.GetSizePer();
					TownPartsFrameEvent townPartsFrameEvent = new TownPartsFrameEvent();
					townPartsFrameEvent.Stack(0, new TownPartsFrameEvent.CallEvent(this.CallAreaCreate), 0);
					townPartsFrameEvent.Stack((int)((1f - sizePer) * 10f), new TownPartsFrameEvent.CallEvent(this.CallAreaCreate), 1);
					townPartsFrameEvent.Stack(48 + (int)((1f - sizePer) * 10f), new TownPartsFrameEvent.CallEvent(this.CallAreaCreate), 2);
					townPartsFrameEvent.Stack(60 + (int)((1f - sizePer) * 10f), new TownPartsFrameEvent.CallEvent(this.CallAreaCreate), 3);
					townPartsFrameEvent.Play();
					this.m_Action.EntryAction(townPartsFrameEvent, 0);
					this.m_IsAvailable = false;
				}
				else
				{
					this.BuildUpAreaHandle();
				}
				this.m_BuildStep++;
				this.m_Transform.localScale = Vector3.one;
				result = true;
			}
			return result;
		}

		// Token: 0x06002290 RID: 8848 RVA: 0x000B8A94 File Offset: 0x000B6E94
		private void BuildUpAreaHandle()
		{
			TownHitNodeState component = base.gameObject.GetComponent<TownHitNodeState>();
			this.m_SubTreeTable = new TownObjHandleArea.TSubTreeState[this.m_BuildUpList.GetBufNum() + this.m_BuildUpList.GetContentNum()];
			this.m_SubTreeNum = 0;
			int num = this.m_BuildUpList.GetBufNum();
			for (int i = 0; i < num; i++)
			{
				TownObjHandleArea.CAreaBuildUpKey careaBuildUpKey = this.m_BuildUpList.GetBufPoint(i);
				TownBuildPakage pakage = careaBuildUpKey.m_Pakage;
				TownObjHandleBufFree townObjHandleBufFree = (TownObjHandleBufFree)ITownObjectHandler.CreateHandler(careaBuildUpKey.m_Object, TownUtility.eResCategory.BufFree, this.m_Builder);
				townObjHandleBufFree.SetupLink(TownUtility.MakeBuildPointID(pakage.m_Group, pakage.m_Index), component.GetHitNode(careaBuildUpKey.m_AccessKey).gameObject);
				pakage.SetLinkHandle(townObjHandleBufFree);
				this.CreateBuildTree(pakage, careaBuildUpKey.m_Object, townObjHandleBufFree.GetBuildAccessID(), townObjHandleBufFree.GetManageID(), false);
				this.m_SubTreeTable[this.m_SubTreeNum].m_BuildPoint = townObjHandleBufFree.GetBuildAccessID();
				this.m_SubTreeNum++;
			}
			num = this.m_BuildUpList.GetContentNum();
			for (int i = 0; i < num; i++)
			{
				TownObjHandleArea.CAreaBuildUpKey careaBuildUpKey = this.m_BuildUpList.GetContentPoint(i);
				TownBuildPakage pakage = careaBuildUpKey.m_Pakage;
				this.m_MarkerNode = careaBuildUpKey.m_Object.transform;
				this.m_ContentAccessKey = TownUtility.MakeBuildContentID(pakage.m_Group);
				TownObjHandleBufFree townObjHandleBufFree = (TownObjHandleBufFree)ITownObjectHandler.CreateHandler(careaBuildUpKey.m_Object, TownUtility.eResCategory.BufFree, this.m_Builder);
				townObjHandleBufFree.SetupLink(TownUtility.MakeBuildContentID(pakage.m_Group), component.GetHitNode(careaBuildUpKey.m_AccessKey).gameObject);
				pakage.SetLinkHandle(townObjHandleBufFree);
				this.CreateBuildTree(pakage, careaBuildUpKey.m_Object, townObjHandleBufFree.GetBuildAccessID(), townObjHandleBufFree.GetManageID(), true);
				this.m_SubTreeTable[this.m_SubTreeNum].m_BuildPoint = townObjHandleBufFree.GetBuildAccessID();
				this.m_SubTreeNum++;
			}
		}

		// Token: 0x06002291 RID: 8849 RVA: 0x000B8C84 File Offset: 0x000B7084
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleArea.eState.Init:
				if (this.m_IsPreparing && this.PrepareMain())
				{
					this.m_State = TownObjHandleArea.eState.Main;
				}
				break;
			case TownObjHandleArea.eState.EndStart:
				this.m_State = TownObjHandleArea.eState.End;
				break;
			case TownObjHandleArea.eState.End:
				base.Destroy();
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x06002292 RID: 8850 RVA: 0x000B8D08 File Offset: 0x000B7108
		public static void MakeBuildPoint(Transform ptrs, TownObjHandleArea.CAreaPointSetUp plist, int fparentkey, int flayerid, int fcnt)
		{
			if (fcnt >= 0)
			{
				int num = TownBuildPointUtil.SearchBuildPointDB(ptrs.name);
				if (num >= 0)
				{
					TownBuildPointDB_Param townBuildPointDB_Param;
					TownBuildPointUtil.GetBuildPointDB(out townBuildPointDB_Param, num);
					ushort attachType = townBuildPointDB_Param.m_AttachType;
					if (attachType != 3)
					{
						if (attachType != 4)
						{
							if (attachType != 7)
							{
							}
						}
						else
						{
							TownBuildPakage townBuildPakage = ptrs.gameObject.AddComponent<TownBuildPakage>();
							townBuildPakage.m_Index = townBuildPointDB_Param.m_AccessKey;
							townBuildPakage.m_Group = fparentkey;
							townBuildPakage.m_Layer = flayerid + townBuildPointDB_Param.m_Layer;
							TownObjHandleArea.CAreaBuildUpKey ppoint = new TownObjHandleArea.CAreaBuildUpKey(ptrs.gameObject, townBuildPakage, townBuildPointDB_Param.m_AccessKey);
							plist.AddContentPoint(ppoint);
							TownBuildPointUtil.RepointBuildKey(ref townBuildPointDB_Param, ptrs);
						}
					}
					else
					{
						TownBuildPakage townBuildPakage = ptrs.gameObject.AddComponent<TownBuildPakage>();
						townBuildPakage.m_Index = townBuildPointDB_Param.m_AccessKey;
						townBuildPakage.m_Group = fparentkey;
						townBuildPakage.m_Layer = flayerid + townBuildPointDB_Param.m_Layer;
						TownObjHandleArea.CAreaBuildUpKey ppoint = new TownObjHandleArea.CAreaBuildUpKey(ptrs.gameObject, townBuildPakage, townBuildPointDB_Param.m_AccessKey);
						plist.AddBufPoint(ppoint);
						TownBuildPointUtil.RepointBuildKey(ref townBuildPointDB_Param, ptrs);
					}
				}
				int childCount = ptrs.childCount;
				for (int i = 0; i < childCount; i++)
				{
					TownObjHandleArea.MakeBuildPoint(ptrs.GetChild(i), plist, fparentkey, flayerid, fcnt - 1);
				}
			}
		}

		// Token: 0x06002293 RID: 8851 RVA: 0x000B8E40 File Offset: 0x000B7240
		public override void DestroyRequest()
		{
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(this.m_ManageID, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(false);
				buildMngIDTreeNode.ChangeManageID(-1L);
			}
			if (this.m_SubTreeNum > 0)
			{
				for (int i = 0; i < this.m_SubTreeNum; i++)
				{
					this.m_Builder.GetBuildTree().DeletePoint(this.m_SubTreeTable[i].m_BuildPoint);
				}
			}
			this.m_State = TownObjHandleArea.eState.EndStart;
		}

		// Token: 0x06002294 RID: 8852 RVA: 0x000B8ED1 File Offset: 0x000B72D1
		public override void Destroy()
		{
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			this.m_State = TownObjHandleArea.eState.End;
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
			base.Destroy();
		}

		// Token: 0x06002295 RID: 8853 RVA: 0x000B8F04 File Offset: 0x000B7304
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			switch (pact.m_Type)
			{
			case eTownEventAct.OpenSelectUI:
			{
				TownPartsModelMaker paction = new TownPartsModelMaker(this.m_Builder, this.m_MarkerNode, base.gameObject, this.m_LayerID + 10);
				this.m_Action.EntryAction(paction, 0);
				break;
			}
			case eTownEventAct.CloseSelectUI:
			{
				ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsModelMaker));
				if (action != null)
				{
					TownPartsModelMaker townPartsModelMaker = (TownPartsModelMaker)action;
					townPartsModelMaker.SetEndMark();
				}
				break;
			}
			case eTownEventAct.ChangeObject:
			{
				TownHandleActionChangeObj townHandleActionChangeObj = (TownHandleActionChangeObj)pact;
				this.m_ManageID = townHandleActionChangeObj.m_ManageID;
				this.m_ObjID = townHandleActionChangeObj.m_ObjectID;
				FieldMapManager.EntryBuildState(this.m_ManageID, new FeedBackResultCallback(this.CallbackAreaBuild));
				TownBuildLinkPoint buildPointTreeNode = this.m_Builder.GetBuildTree().GetBuildPointTreeNode(this.m_BuildAccessID);
				if (buildPointTreeNode != null)
				{
					buildPointTreeNode.ChangeManageID(this.m_ManageID);
				}
				break;
			}
			case eTownEventAct.ChangeModel:
			{
				TownHandleActionModelObj townHandleActionModelObj = (TownHandleActionModelObj)pact;
				this.m_ManageID = townHandleActionModelObj.m_ManageID;
				this.m_ObjID = townHandleActionModelObj.m_ObjectID;
				FieldMapManager.EntryBuildState(this.m_ManageID, new FeedBackResultCallback(this.CallbackAreaBuild));
				TownBuildLinkPoint buildPointTreeNode2 = this.m_Builder.GetBuildTree().GetBuildPointTreeNode(this.m_BuildAccessID);
				if (buildPointTreeNode2 != null)
				{
					buildPointTreeNode2.ChangeManageID(this.m_ManageID);
				}
				break;
			}
			}
			return result;
		}

		// Token: 0x06002296 RID: 8854 RVA: 0x000B90B4 File Offset: 0x000B74B4
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				if (this.IsCompleteAreaEvt())
				{
					pbuilder.StackEventQue(new TownComComplete(TownUtility.eResCategory.Area)
					{
						m_BuildPoint = base.GetBuildAccessID()
					});
				}
				else
				{
					pbuilder.StackEventQue(new TownComUI(true)
					{
						m_SelectHandle = this,
						m_OpenUI = eTownUICategory.Area,
						m_BuildPoint = base.GetBuildAccessID()
					});
				}
			}
			return true;
		}

		// Token: 0x06002297 RID: 8855 RVA: 0x000B9120 File Offset: 0x000B7520
		public void CallbackAreaBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
			if (ftype != eCallBackType.MoveBuild)
			{
				if (ftype != eCallBackType.SwapBuild)
				{
					if (ftype != eCallBackType.CompBuild && ftype != eCallBackType.LevelUp)
					{
					}
				}
				else
				{
					TownPartsChangeArea paction = new TownPartsChangeArea(this.m_Builder, this, this.m_BuildAccessID, UserTownUtil.GetTownPointToBuildData(this.m_BuildAccessID).m_ObjID);
					this.m_Action.EntryAction(paction, 0);
					this.m_ManageID = UserTownUtil.GetTownPointToBuildData(this.m_BuildAccessID).m_ManageID;
				}
			}
			else
			{
				TownBuildMoveAreaParam prequest = new TownBuildMoveAreaParam(this.m_BuildAccessID, pbase.m_BuildPoint, this.m_ManageID, this.m_ObjID);
				this.m_Builder.BuildRequest(prequest);
			}
		}

		// Token: 0x06002298 RID: 8856 RVA: 0x000B91CC File Offset: 0x000B75CC
		private void ChangeColliderActive(bool factive)
		{
			TownHitNodeState component = base.gameObject.GetComponent<TownHitNodeState>();
			if (component != null)
			{
				component.SetHitActive(eTownOption.HitContent, factive);
			}
		}

		// Token: 0x06002299 RID: 8857 RVA: 0x000B91FC File Offset: 0x000B75FC
		private bool CallAreaCreate(int fkeyid)
		{
			bool result = true;
			switch (fkeyid)
			{
			case 0:
			{
				float sizePer = this.m_Builder.m_GameCamera.GetSizePer();
				this.m_Builder.m_GameCamera.SetSizeKey(this.m_Builder.m_GameCamera.GetSizeMax(), (float)((int)((1f - sizePer) * 14f)) / 30f, false);
				break;
			}
			case 1:
			{
				TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(4);
				TownHitNodeState component = base.gameObject.GetComponent<TownHitNodeState>();
				Transform locNode = component.GetLocNode(20);
				if (locNode != null)
				{
					townEffectPlayer.SetTrs(locNode.position + Vector3.back * 50f, Quaternion.identity, Vector3.one, this.m_LayerID + 50);
				}
				else
				{
					townEffectPlayer.SetTrs(base.transform.position + Vector3.back * 50f, Quaternion.identity, Vector3.one, this.m_LayerID + 50);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_BUILD, 1f, 0, -1, -1);
				break;
			}
			case 2:
				this.m_Builder.SetEventActeiveKey(this.m_ContentAccessKey, true);
				this.BuildUpAreaHandle();
				this.m_IsAvailable = true;
				break;
			case 3:
				this.isUnderConstruction = false;
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopTown, true);
				break;
			}
			return result;
		}

		// Token: 0x0400295C RID: 10588
		private bool m_isUnderConstruction;

		// Token: 0x0400295D RID: 10589
		private TownObjHandleArea.CAreaPointSetUp m_BuildUpList;

		// Token: 0x0400295E RID: 10590
		public int m_BuildStep;

		// Token: 0x0400295F RID: 10591
		public int m_ContentAccessKey;

		// Token: 0x04002960 RID: 10592
		public Transform m_MarkerNode;

		// Token: 0x04002961 RID: 10593
		private int m_SubTreeNum;

		// Token: 0x04002962 RID: 10594
		private TownObjHandleArea.TSubTreeState[] m_SubTreeTable;

		// Token: 0x04002963 RID: 10595
		public TownObjHandleArea.eState m_State;

		// Token: 0x020006D2 RID: 1746
		public class CAreaBuildUpKey
		{
			// Token: 0x0600229A RID: 8858 RVA: 0x000B9366 File Offset: 0x000B7766
			public CAreaBuildUpKey(GameObject pobj, TownBuildPakage pakage, int fkey)
			{
				this.m_Object = pobj;
				this.m_Pakage = pakage;
				this.m_AccessKey = fkey;
			}

			// Token: 0x04002964 RID: 10596
			public GameObject m_Object;

			// Token: 0x04002965 RID: 10597
			public TownBuildPakage m_Pakage;

			// Token: 0x04002966 RID: 10598
			public int m_AccessKey;
		}

		// Token: 0x020006D3 RID: 1747
		public class CAreaPointSetUp
		{
			// Token: 0x0600229B RID: 8859 RVA: 0x000B9383 File Offset: 0x000B7783
			public CAreaPointSetUp()
			{
				this.m_BufNode = new List<TownObjHandleArea.CAreaBuildUpKey>();
				this.m_BufNum = 0;
				this.m_ContentNode = new List<TownObjHandleArea.CAreaBuildUpKey>();
				this.m_ContentNum = 0;
				this.m_LocNode = new List<TownObjHandleArea.CAreaBuildUpKey>();
				this.m_LocNum = 0;
			}

			// Token: 0x0600229C RID: 8860 RVA: 0x000B93C1 File Offset: 0x000B77C1
			public int GetBufNum()
			{
				return this.m_BufNum;
			}

			// Token: 0x0600229D RID: 8861 RVA: 0x000B93C9 File Offset: 0x000B77C9
			public void AddBufPoint(TownObjHandleArea.CAreaBuildUpKey ppoint)
			{
				this.m_BufNode.Add(ppoint);
				this.m_BufNum++;
			}

			// Token: 0x0600229E RID: 8862 RVA: 0x000B93E5 File Offset: 0x000B77E5
			public TownObjHandleArea.CAreaBuildUpKey GetBufPoint(int findex)
			{
				return this.m_BufNode[findex];
			}

			// Token: 0x0600229F RID: 8863 RVA: 0x000B93F3 File Offset: 0x000B77F3
			public int GetContentNum()
			{
				return this.m_ContentNum;
			}

			// Token: 0x060022A0 RID: 8864 RVA: 0x000B93FB File Offset: 0x000B77FB
			public void AddContentPoint(TownObjHandleArea.CAreaBuildUpKey ppoint)
			{
				this.m_ContentNode.Add(ppoint);
				this.m_ContentNum++;
			}

			// Token: 0x060022A1 RID: 8865 RVA: 0x000B9417 File Offset: 0x000B7817
			public TownObjHandleArea.CAreaBuildUpKey GetContentPoint(int findex)
			{
				return this.m_ContentNode[findex];
			}

			// Token: 0x04002967 RID: 10599
			public List<TownObjHandleArea.CAreaBuildUpKey> m_BufNode;

			// Token: 0x04002968 RID: 10600
			public int m_BufNum;

			// Token: 0x04002969 RID: 10601
			public List<TownObjHandleArea.CAreaBuildUpKey> m_ContentNode;

			// Token: 0x0400296A RID: 10602
			public int m_ContentNum;

			// Token: 0x0400296B RID: 10603
			public List<TownObjHandleArea.CAreaBuildUpKey> m_LocNode;

			// Token: 0x0400296C RID: 10604
			public int m_LocNum;
		}

		// Token: 0x020006D4 RID: 1748
		private struct TSubTreeState
		{
			// Token: 0x0400296D RID: 10605
			public int m_BuildPoint;
		}

		// Token: 0x020006D5 RID: 1749
		public enum eState
		{
			// Token: 0x0400296F RID: 10607
			Init,
			// Token: 0x04002970 RID: 10608
			Main,
			// Token: 0x04002971 RID: 10609
			Sleep,
			// Token: 0x04002972 RID: 10610
			EndStart,
			// Token: 0x04002973 RID: 10611
			End
		}
	}
}
