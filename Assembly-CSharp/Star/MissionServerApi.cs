﻿using System;
using System.Collections.Generic;
using CommonResponseTypes;
using Meige;
using MISSIONResponseTypes;
using Newtonsoft.Json;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x0200050F RID: 1295
	public class MissionServerApi : IMissionServerApi
	{
		// Token: 0x06001970 RID: 6512 RVA: 0x00084044 File Offset: 0x00082444
		public static IMissionServerApi CreateServer()
		{
			MissionServerApi missionServerApi = SingletonMonoBehaviour<GameSystem>.Inst.GetComponent<MissionServerApi>();
			if (missionServerApi == null)
			{
				missionServerApi = SingletonMonoBehaviour<GameSystem>.Inst.gameObject.AddComponent<MissionServerApi>();
			}
			missionServerApi._CreateServer();
			return missionServerApi;
		}

		// Token: 0x06001971 RID: 6513 RVA: 0x00084080 File Offset: 0x00082480
		private void _CreateServer()
		{
			NetworkQueueManager.SetSystemRecvCallback(new Action<MeigewwwParam>(this.CallbackApiRecv));
			if (this.m_Loader == null)
			{
				this.m_Loader = new ABResourceLoader(-1);
			}
			this.m_LoadingHndl = this.m_Loader.Load("prefab/missiondata.muast", new MeigeResource.Option[0]);
			this.m_Step = MissionServerApi.eStep.Init;
		}

		// Token: 0x06001972 RID: 6514 RVA: 0x000840D8 File Offset: 0x000824D8
		public static void DestroyServer()
		{
			MissionServerApi component = SingletonMonoBehaviour<GameSystem>.Inst.GetComponent<MissionServerApi>();
			if (component != null)
			{
				component._DestroyServer();
			}
		}

		// Token: 0x06001973 RID: 6515 RVA: 0x00084104 File Offset: 0x00082504
		private void _DestroyServer()
		{
			NetworkQueueManager.SetSystemRecvCallback(null);
			if (this.m_LoadingHndl != null)
			{
				if (this.m_Loader == null)
				{
					this.m_Loader.Unload(this.m_LoadingHndl);
				}
				this.m_LoadingHndl = null;
			}
			this.m_List.Clear();
			this.m_NewCompList.Clear();
			this.m_PreCompList.Clear();
			this.m_SystemCheckTime = 0f;
			this.m_CheckUpSystemMission = false;
			this.m_CharaCheckTime = 0f;
			this.m_CheckUpCharaMission = false;
			this.m_SendSetList.Clear();
			this.m_SendCompList.Clear();
			this.m_SendComFlagSet = false;
			this.m_SendComFlagComp = false;
			this.m_CallbackList.Clear();
			this.m_DataList = null;
			this.m_Refresh = false;
			this.m_NewListUpKey = false;
			this.m_Step = MissionServerApi.eStep.None;
		}

		// Token: 0x06001974 RID: 6516 RVA: 0x000841D5 File Offset: 0x000825D5
		public override bool IsAvailable()
		{
			return this.m_Step >= MissionServerApi.eStep.Calc;
		}

		// Token: 0x06001975 RID: 6517 RVA: 0x000841E6 File Offset: 0x000825E6
		public override List<IMissionPakage> GetMissionList()
		{
			return this.m_List;
		}

		// Token: 0x06001976 RID: 6518 RVA: 0x000841EE File Offset: 0x000825EE
		public override bool IsPreComp()
		{
			return this.m_NewCompList.Count != 0;
		}

		// Token: 0x06001977 RID: 6519 RVA: 0x00084201 File Offset: 0x00082601
		public override int GetPreCompNum()
		{
			return this.m_PreCompList.Count;
		}

		// Token: 0x06001978 RID: 6520 RVA: 0x0008420E File Offset: 0x0008260E
		public override bool IsListUp()
		{
			return this.m_Step >= MissionServerApi.eStep.Calc;
		}

		// Token: 0x06001979 RID: 6521 RVA: 0x0008421C File Offset: 0x0008261C
		private void CheckSendStateEntry(long fmanageid)
		{
			bool flag = false;
			for (int i = 0; i < this.m_SendSetList.Count; i++)
			{
				if (this.m_SendSetList[i] == fmanageid)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				this.m_SendSetList.Add(fmanageid);
			}
		}

		// Token: 0x0600197A RID: 6522 RVA: 0x00084274 File Offset: 0x00082674
		public override IMissionPakage GetPreCompMission(PopupMaskFunc fmask)
		{
			IMissionPakage missionPakage = null;
			if (!this.m_SendComFlagSet)
			{
				for (int i = 0; i < this.m_NewCompList.Count; i++)
				{
					long num = this.m_NewCompList[i];
					for (int j = 0; j < this.m_List.Count; j++)
					{
						if (this.m_List[j].m_KeyManageID == num && fmask.IsPopUpActive(this.m_List[j].m_PopupTimingNo))
						{
							missionPakage = this.m_List[j];
							missionPakage.m_State = eMissionState.PreView;
							this.CheckSendStateEntry(num);
							break;
						}
					}
					if (missionPakage != null)
					{
						this.m_NewCompList.RemoveAt(i);
						break;
					}
				}
			}
			return missionPakage;
		}

		// Token: 0x0600197B RID: 6523 RVA: 0x00084340 File Offset: 0x00082740
		public override void UnlockAction(eXlsMissionSeg fcategory, int factionno)
		{
			IMissionFunction missionFunction = null;
			switch (fcategory)
			{
			case eXlsMissionSeg.System:
				missionFunction = new MissionFuncSystem();
				break;
			case eXlsMissionSeg.Battle:
				missionFunction = new MissionFuncBattle();
				break;
			case eXlsMissionSeg.Town:
				missionFunction = new MissionFuncTown();
				break;
			case eXlsMissionSeg.Room:
				missionFunction = new MissionFuncRoom();
				break;
			case eXlsMissionSeg.Edit:
				missionFunction = new MissionFuncEdit();
				break;
			case eXlsMissionSeg.Chara:
				missionFunction = new MissionFuncChara();
				break;
			}
			if (missionFunction != null)
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_ActionCategory == fcategory && this.m_List[i].m_ExtensionScriptID == factionno && this.m_List[i].m_State == eMissionState.Non)
					{
						if (missionFunction.CheckUnlock(this.m_List[i], this.m_List))
						{
							this.m_List[i].m_State = eMissionState.PreComp;
							this.m_NewCompList.Add(this.m_List[i].m_KeyManageID);
							this.m_PreCompList.Add(this.m_List[i].m_KeyManageID);
						}
						this.CheckSendStateEntry(this.m_List[i].m_KeyManageID);
					}
				}
			}
		}

		// Token: 0x0600197C RID: 6524 RVA: 0x0008449C File Offset: 0x0008289C
		private void UpdateFixLogList(IMissionPakage pelement)
		{
			MissionDataDB dataList = this.m_DataList;
			if (dataList == null)
			{
				return;
			}
			for (int i = 0; i < dataList.m_Params.Length; i++)
			{
				if (pelement.m_ListUpID == (long)dataList.m_Params[i].m_ID)
				{
					pelement.m_ActionCategory = (eXlsMissionSeg)dataList.m_Params[i].m_ActionNo;
					pelement.m_ExtensionScriptID = dataList.m_Params[i].m_ExtentionScriptID;
					pelement.m_TargetMessage = dataList.m_Params[i].m_TargetMessage;
					pelement.m_PopupTimingNo = dataList.m_Params[i].m_PopupTiming;
					switch (dataList.m_Params[i].m_Category)
					{
					case 0:
						pelement.m_Type = eMissionCategory.Day;
						break;
					case 1:
						pelement.m_Type = eMissionCategory.Week;
						break;
					case 2:
						pelement.m_Type = eMissionCategory.Unlock;
						break;
					case 3:
						pelement.m_Type = eMissionCategory.Event;
						break;
					case 4:
						pelement.m_Type = eMissionCategory.Unlock;
						break;
					}
					switch (pelement.m_State)
					{
					case eMissionState.PreComp:
						this.m_NewCompList.Add(pelement.m_KeyManageID);
						this.m_PreCompList.Add(pelement.m_KeyManageID);
						break;
					case eMissionState.PreView:
						this.m_PreCompList.Add(pelement.m_KeyManageID);
						break;
					}
					break;
				}
			}
		}

		// Token: 0x0600197D RID: 6525 RVA: 0x00084628 File Offset: 0x00082A28
		public override void CompleteMission(long fcompmanageid)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].m_KeyManageID == fcompmanageid)
				{
					this.m_List[i].m_State = eMissionState.Comp;
					for (int j = 0; j < this.m_PreCompList.Count; j++)
					{
						if (this.m_PreCompList[j] == this.m_List[i].m_KeyManageID)
						{
							this.m_PreCompList.RemoveAt(j);
							break;
						}
					}
					for (int j = 0; j < this.m_NewCompList.Count; j++)
					{
						if (this.m_NewCompList[j] == this.m_List[i].m_KeyManageID)
						{
							this.m_NewCompList.RemoveAt(j);
							break;
						}
					}
					this.m_SendCompList.Add(this.m_List[i].m_KeyManageID);
					break;
				}
			}
		}

		// Token: 0x0600197E RID: 6526 RVA: 0x0008473C File Offset: 0x00082B3C
		private void UpMissionLogElement(IMissionPakage pelement, PlayerMissionLog plog)
		{
			pelement.m_ListUpID = (long)plog.missionID;
			pelement.m_KeyManageID = plog.managedMissionId;
			pelement.m_SubCodeID = plog.subCode;
			pelement.m_State = eMissionState.Non;
			switch (plog.state)
			{
			case 0:
				pelement.m_State = eMissionState.Non;
				break;
			case 1:
				pelement.m_State = eMissionState.PreComp;
				break;
			case 2:
				pelement.m_State = eMissionState.PreView;
				break;
			default:
				pelement.m_State = eMissionState.Comp;
				break;
			}
			if (plog.reward != null && plog.reward.Length > 0)
			{
				switch (plog.reward[0].RewardType)
				{
				case 0:
					pelement.m_RewardCategory = eMissionRewardCategory.Non;
					break;
				case 1:
					pelement.m_RewardCategory = eMissionRewardCategory.Money;
					break;
				case 2:
					pelement.m_RewardCategory = eMissionRewardCategory.Item;
					break;
				case 3:
					pelement.m_RewardCategory = eMissionRewardCategory.KRRPoint;
					break;
				case 4:
					pelement.m_RewardCategory = eMissionRewardCategory.Stamina;
					break;
				case 5:
					pelement.m_RewardCategory = eMissionRewardCategory.Friendship;
					break;
				case 6:
					pelement.m_RewardCategory = eMissionRewardCategory.Gem;
					break;
				}
				pelement.m_RewardNo = plog.reward[0].RewardNo;
				pelement.m_RewardNum = plog.reward[0].RewardNum;
			}
			pelement.m_Rate = plog.rate;
			pelement.m_RateBase = plog.rateMax;
			pelement.m_LimitTime = plog.limitTime;
		}

		// Token: 0x0600197F RID: 6527 RVA: 0x000848B0 File Offset: 0x00082CB0
		private IMissionPakage CreateMissionPakage(int fobjid)
		{
			MissionDataDB dataList = this.m_DataList;
			if (dataList == null)
			{
				return null;
			}
			IMissionPakage missionPakage = null;
			for (int i = 0; i < dataList.m_Params.Length; i++)
			{
				if (fobjid == dataList.m_Params[i].m_ID)
				{
					switch (dataList.m_Params[i].m_ActionNo)
					{
					case 0:
					case 1:
					case 3:
					case 4:
						missionPakage = new IMissionPakage();
						break;
					case 5:
						missionPakage = new MissionPakageChara();
						break;
					}
					break;
				}
			}
			if (missionPakage == null)
			{
				missionPakage = new IMissionPakage();
			}
			return missionPakage;
		}

		// Token: 0x06001980 RID: 6528 RVA: 0x00084964 File Offset: 0x00082D64
		private void GetAllToList(PlayerMissionLog[] ptbl)
		{
			this.m_List.Clear();
			int num = ptbl.Length;
			for (int i = 0; i < num; i++)
			{
				IMissionPakage missionPakage = this.CreateMissionPakage(ptbl[i].missionID);
				this.UpMissionLogElement(missionPakage, ptbl[i]);
				this.UpdateFixLogList(missionPakage);
				this.m_List.Add(missionPakage);
			}
		}

		// Token: 0x06001981 RID: 6529 RVA: 0x000849C0 File Offset: 0x00082DC0
		private void RefreshAllToList(PlayerMissionLog[] ptbl)
		{
			this.m_NewListUpKey = false;
			int count = this.m_List.Count;
			int i;
			for (i = 0; i < count; i++)
			{
				this.m_List[i].m_ListUpID = -1L;
			}
			int num = ptbl.Length;
			for (int j = 0; j < num; j++)
			{
				count = this.m_List.Count;
				bool flag = false;
				for (i = 0; i < count; i++)
				{
					if (this.m_List[i].m_KeyManageID == ptbl[j].managedMissionId)
					{
						this.UpMissionLogElement(this.m_List[i], ptbl[j]);
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					IMissionPakage missionPakage = this.CreateMissionPakage(ptbl[j].missionID);
					this.UpMissionLogElement(missionPakage, ptbl[j]);
					this.UpdateFixLogList(missionPakage);
					this.m_List.Add(missionPakage);
				}
			}
			count = this.m_List.Count;
			i = 0;
			while (i < count)
			{
				if (this.m_List[i].m_ListUpID <= -1L)
				{
					this.m_List.RemoveAt(i);
					count = this.m_List.Count;
				}
				else
				{
					i++;
				}
			}
			this.m_NewListUpKey = true;
		}

		// Token: 0x06001982 RID: 6530 RVA: 0x00084B08 File Offset: 0x00082F08
		public void GetMissionLogList()
		{
			MeigewwwParam all = MISSIONRequest.GetAll(new MeigewwwParam.Callback(this.CommonGetAllCallback));
			NetworkQueueManager.Request(all);
		}

		// Token: 0x06001983 RID: 6531 RVA: 0x00084B30 File Offset: 0x00082F30
		public void CommonGetAllCallback(MeigewwwParam pparam)
		{
			CommonResponse commonResponse = ResponseCommon.Func<CommonResponse>(pparam, ResponseCommon.DialogType.Retry, null);
			if (commonResponse != null)
			{
				string responseJson = pparam.GetResponseJson();
				GetAll getAll = JsonConvert.DeserializeObject<GetAll>(responseJson);
				this.GetAllToList(getAll.missionLogs);
				this.m_Step++;
			}
		}

		// Token: 0x06001984 RID: 6532 RVA: 0x00084B74 File Offset: 0x00082F74
		public void RefreshMissionLogList()
		{
			MeigewwwParam all = MISSIONRequest.GetAll(new MeigewwwParam.Callback(this.CommonRefreshCallback));
			NetworkQueueManager.Request(all);
		}

		// Token: 0x06001985 RID: 6533 RVA: 0x00084B9C File Offset: 0x00082F9C
		public void CommonRefreshCallback(MeigewwwParam pparam)
		{
			CommonResponse commonResponse = ResponseCommon.Func<CommonResponse>(pparam, ResponseCommon.DialogType.Retry, null);
			if (commonResponse != null)
			{
				string responseJson = pparam.GetResponseJson();
				GetAll getAll = JsonConvert.DeserializeObject<GetAll>(responseJson);
				this.RefreshAllToList(getAll.missionLogs);
			}
		}

		// Token: 0x06001986 RID: 6534 RVA: 0x00084BD4 File Offset: 0x00082FD4
		public void SendMissionSetList()
		{
			this.m_SendComFlagSet = true;
			PlayerMissionLog[] array = new PlayerMissionLog[this.m_SendSetList.Count];
			for (int i = 0; i < this.m_SendSetList.Count; i++)
			{
				for (int j = 0; j < this.m_List.Count; j++)
				{
					if (this.m_List[j].m_KeyManageID == this.m_SendSetList[i])
					{
						array[i] = new PlayerMissionLog();
						array[i].managedMissionId = this.m_List[j].m_KeyManageID;
						array[i].missionID = (int)this.m_List[j].m_ListUpID;
						array[i].rate = this.m_List[j].m_Rate;
						array[i].rateMax = this.m_List[j].m_RateBase;
						array[i].state = (int)this.m_List[j].m_State;
						array[i].reward = null;
						array[i].limitTime = this.m_List[j].m_LimitTime;
						break;
					}
				}
			}
			this.m_SendSetList.Clear();
			MeigewwwParam wwwParam = MISSIONRequest.Set(array, new MeigewwwParam.Callback(this.CommonSetCallback));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001987 RID: 6535 RVA: 0x00084D24 File Offset: 0x00083124
		public void CommonSetCallback(MeigewwwParam pparam)
		{
			CommonResponse commonResponse = ResponseCommon.Func<CommonResponse>(pparam, ResponseCommon.DialogType.Retry, null);
			if (commonResponse != null && commonResponse.GetResult() == ResultCode.SUCCESS)
			{
				this.m_SendComFlagSet = false;
			}
		}

		// Token: 0x06001988 RID: 6536 RVA: 0x00084D54 File Offset: 0x00083154
		public void SendMissionCompList()
		{
			this.m_SendComFlagComp = true;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			MeigewwwParam wwwParam = MISSIONRequest.Complete(this.m_SendCompList[0], new MeigewwwParam.Callback(this.CommonCompCallback));
			this.m_SendCompList.RemoveAt(0);
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001989 RID: 6537 RVA: 0x00084DA8 File Offset: 0x000831A8
		public void CommonCompCallback(MeigewwwParam pparam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			CommonResponse commonResponse = ResponseCommon.Func<CommonResponse>(pparam, ResponseCommon.DialogType.Retry, null);
			if (commonResponse != null && commonResponse.GetResult() == ResultCode.SUCCESS)
			{
				string responseJson = pparam.GetResponseJson();
				Complete complete = JsonConvert.DeserializeObject<Complete>(responseJson);
				APIUtility.wwwToUserData(complete.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticePresentCount = complete.presents.Length;
				this.m_SendComFlagComp = false;
				this.m_Refresh = true;
			}
		}

		// Token: 0x0600198A RID: 6538 RVA: 0x00084E2C File Offset: 0x0008322C
		public void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			MissionServerApi.eStep step = this.m_Step;
			switch (step + 1)
			{
			case MissionServerApi.eStep.ComUp:
				if (this.m_LoadingHndl != null && this.m_LoadingHndl.IsDone())
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.m_LoadingHndl.Retry();
					}
					else
					{
						this.m_DataList = (this.m_LoadingHndl.FindObj("MissionData", true) as MissionDataDB);
						this.m_Loader.Unload(this.m_LoadingHndl);
						this.m_LoadingHndl = null;
					}
				}
				if (this.m_DataList != null && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID != -1L)
				{
					this.m_Step++;
					this.GetMissionLogList();
				}
				break;
			case MissionServerApi.eStep.End:
				if (this.m_Refresh)
				{
					this.m_Refresh = false;
					this.RefreshMissionLogList();
				}
				if (this.m_NewListUpKey)
				{
					for (int i = 0; i < this.m_CallbackList.Count; i++)
					{
						this.m_CallbackList[i].m_Callback();
					}
					this.m_NewListUpKey = false;
				}
				this.SystemDataUpCheck();
				if (this.m_SendSetList.Count != 0 && !this.m_SendComFlagSet)
				{
					this.SendMissionSetList();
				}
				if (this.m_SendCompList.Count != 0 && !this.m_SendComFlagComp)
				{
					this.SendMissionCompList();
				}
				break;
			}
		}

		// Token: 0x0600198B RID: 6539 RVA: 0x00084FE4 File Offset: 0x000833E4
		private void SystemDataUpCheck()
		{
			this.m_SystemCheckTime -= Time.deltaTime;
			if (this.m_SystemCheckTime < 0f || this.m_CheckUpSystemMission)
			{
				this.m_SystemCheckTime = 0.5f;
				this.m_CheckUpSystemMission = false;
				IMissionFunction missionFunction = new MissionFuncSystem();
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_ActionCategory == eXlsMissionSeg.System && this.m_List[i].m_State == eMissionState.Non && missionFunction.CheckUnlock(this.m_List[i], this.m_List))
					{
						this.m_List[i].m_State = eMissionState.PreComp;
						this.m_NewCompList.Add(this.m_List[i].m_KeyManageID);
						this.m_PreCompList.Add(this.m_List[i].m_KeyManageID);
						this.m_SendSetList.Add(this.m_List[i].m_KeyManageID);
					}
				}
			}
			this.m_CharaCheckTime -= Time.deltaTime;
			if (this.m_CharaCheckTime < 0f || this.m_CheckUpCharaMission)
			{
				this.m_CharaCheckTime = 0.5f;
				this.m_CheckUpCharaMission = false;
				IMissionFunction missionFunction2 = new MissionFuncChara();
				int count2 = this.m_List.Count;
				for (int j = 0; j < count2; j++)
				{
					if (this.m_List[j].m_ActionCategory == eXlsMissionSeg.Chara && this.m_List[j].m_State == eMissionState.Non && missionFunction2.CheckUnlock(this.m_List[j], this.m_List))
					{
						this.m_List[j].m_State = eMissionState.PreComp;
						this.m_NewCompList.Add(this.m_List[j].m_KeyManageID);
						this.m_PreCompList.Add(this.m_List[j].m_KeyManageID);
						this.m_SendSetList.Add(this.m_List[j].m_KeyManageID);
					}
				}
			}
		}

		// Token: 0x0600198C RID: 6540 RVA: 0x0008521C File Offset: 0x0008361C
		public void CallbackApiRecv(MeigewwwParam wwwParam)
		{
			int num = this.m_ApiDataList.Length;
			for (int i = 0; i < num; i++)
			{
				if (wwwParam.GetAPI().IndexOf(this.m_ApiDataList[i].m_SearchName) >= 0)
				{
					int programExtension = this.m_ApiDataList[i].m_ProgramExtension;
					if (programExtension != 0)
					{
						if (programExtension == 1)
						{
							this.BattleExtensionCheck(wwwParam, ref this.m_ApiDataList[i]);
						}
					}
					else
					{
						CommonResponse commonResponse = ResponseCommon.Func<CommonResponse>(wwwParam, ResponseCommon.DialogType.None, null);
						if (commonResponse.GetResult() == ResultCode.SUCCESS)
						{
							this.UnlockAction((eXlsMissionSeg)this.m_ApiDataList[i].m_CheckSegmentType, (int)this.m_ApiDataList[i].m_ActionNo);
						}
					}
					break;
				}
			}
		}

		// Token: 0x0600198D RID: 6541 RVA: 0x000852E8 File Offset: 0x000836E8
		private void BattleExtensionCheck(MeigewwwParam wwwParam, ref MissionServerApi.MissionApiLinkParam pdb)
		{
			int parameterNum = wwwParam.GetParameterNum();
			bool flag = true;
			for (int i = 0; i < parameterNum; i++)
			{
				MeigewwwParam.Parameter parameter;
				wwwParam.GetParameter(out parameter, i);
				if (parameter.paramName != null)
				{
					if (parameter.paramName == "state" && parameter.value.GetType() == typeof(int))
					{
						if (Convert.ToInt32(parameter.value) != 2)
						{
							flag = false;
							break;
						}
					}
					else if (parameter.paramName == "killedEnemies" && parameter.value.GetType() == typeof(string) && parameter.value != null)
					{
						string text = Convert.ToString(parameter.value);
						if (text.Length <= 2)
						{
							flag = false;
							break;
						}
					}
				}
			}
			if (flag)
			{
				this.UnlockAction((eXlsMissionSeg)pdb.m_CheckSegmentType, (int)pdb.m_ActionNo);
			}
		}

		// Token: 0x0600198E RID: 6542 RVA: 0x000853EC File Offset: 0x000837EC
		public override void EntryRefreshCallback(RefreshCallback pcallback, int fkeyid)
		{
			bool flag = false;
			for (int i = 0; i < this.m_CallbackList.Count; i++)
			{
				if (this.m_CallbackList[i].m_Key == fkeyid)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				MissionServerApi.RefreshCallInfo refreshCallInfo = new MissionServerApi.RefreshCallInfo();
				refreshCallInfo.m_Callback = pcallback;
				refreshCallInfo.m_Key = fkeyid;
				this.m_CallbackList.Add(refreshCallInfo);
			}
		}

		// Token: 0x0600198F RID: 6543 RVA: 0x0008545C File Offset: 0x0008385C
		public override void ReleaseRefreshCallback(int fkeyid)
		{
			for (int i = 0; i < this.m_CallbackList.Count; i++)
			{
				if (this.m_CallbackList[i].m_Key == fkeyid)
				{
					this.m_CallbackList.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x06001990 RID: 6544 RVA: 0x000854B0 File Offset: 0x000838B0
		public override void RebuildMissionList()
		{
			MeigewwwParam wwwParam = MISSIONRequest.Refresh(new MeigewwwParam.Callback(this.CommonMissionRefresh));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001991 RID: 6545 RVA: 0x000854D5 File Offset: 0x000838D5
		public void CommonMissionRefresh(MeigewwwParam pparam)
		{
		}

		// Token: 0x04002037 RID: 8247
		private const float SYSTEM_CHECK_TIME = 0.5f;

		// Token: 0x04002038 RID: 8248
		private List<IMissionPakage> m_List = new List<IMissionPakage>();

		// Token: 0x04002039 RID: 8249
		private List<long> m_NewCompList = new List<long>();

		// Token: 0x0400203A RID: 8250
		private List<long> m_PreCompList = new List<long>();

		// Token: 0x0400203B RID: 8251
		private float m_SystemCheckTime;

		// Token: 0x0400203C RID: 8252
		private bool m_CheckUpSystemMission;

		// Token: 0x0400203D RID: 8253
		private float m_CharaCheckTime;

		// Token: 0x0400203E RID: 8254
		private bool m_CheckUpCharaMission;

		// Token: 0x0400203F RID: 8255
		private List<long> m_SendSetList = new List<long>();

		// Token: 0x04002040 RID: 8256
		private List<long> m_SendCompList = new List<long>();

		// Token: 0x04002041 RID: 8257
		private bool m_SendComFlagSet;

		// Token: 0x04002042 RID: 8258
		private bool m_SendComFlagComp;

		// Token: 0x04002043 RID: 8259
		private List<MissionServerApi.RefreshCallInfo> m_CallbackList = new List<MissionServerApi.RefreshCallInfo>();

		// Token: 0x04002044 RID: 8260
		private ABResourceLoader m_Loader;

		// Token: 0x04002045 RID: 8261
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04002046 RID: 8262
		private MissionDataDB m_DataList;

		// Token: 0x04002047 RID: 8263
		private bool m_Refresh;

		// Token: 0x04002048 RID: 8264
		private bool m_NewListUpKey;

		// Token: 0x04002049 RID: 8265
		private MissionServerApi.MissionApiLinkParam[] m_ApiDataList = new MissionServerApi.MissionApiLinkParam[]
		{
			new MissionServerApi.MissionApiLinkParam("battle_party/set", eXlsMissionSeg.Edit, 0, 0),
			new MissionServerApi.MissionApiLinkParam("battle_party/set_all", eXlsMissionSeg.Edit, 0, 0),
			new MissionServerApi.MissionApiLinkParam("character/support/set_all", eXlsMissionSeg.Edit, 1, 0),
			new MissionServerApi.MissionApiLinkParam("weapon/make", eXlsMissionSeg.Edit, 5, 0),
			new MissionServerApi.MissionApiLinkParam("weapon/upgrade", eXlsMissionSeg.Edit, 6, 0)
		};

		// Token: 0x0400204A RID: 8266
		private MissionServerApi.eStep m_Step = MissionServerApi.eStep.None;

		// Token: 0x02000510 RID: 1296
		private class RefreshCallInfo
		{
			// Token: 0x0400204B RID: 8267
			public RefreshCallback m_Callback;

			// Token: 0x0400204C RID: 8268
			public int m_Key;
		}

		// Token: 0x02000511 RID: 1297
		public struct MissionApiLinkParam
		{
			// Token: 0x06001993 RID: 6547 RVA: 0x000854DF File Offset: 0x000838DF
			public MissionApiLinkParam(string fapiname, eXlsMissionSeg fsegment, int funckey, int fextno)
			{
				this.m_SearchName = fapiname;
				this.m_CheckSegmentType = (ushort)fsegment;
				this.m_ActionNo = (uint)funckey;
				this.m_ProgramExtension = fextno;
			}

			// Token: 0x0400204D RID: 8269
			public string m_SearchName;

			// Token: 0x0400204E RID: 8270
			public ushort m_CheckSegmentType;

			// Token: 0x0400204F RID: 8271
			public uint m_ActionNo;

			// Token: 0x04002050 RID: 8272
			public int m_ProgramExtension;
		}

		// Token: 0x02000512 RID: 1298
		public enum eStep
		{
			// Token: 0x04002052 RID: 8274
			None = -1,
			// Token: 0x04002053 RID: 8275
			Init,
			// Token: 0x04002054 RID: 8276
			ComUp,
			// Token: 0x04002055 RID: 8277
			Calc,
			// Token: 0x04002056 RID: 8278
			End
		}
	}
}
