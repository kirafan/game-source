﻿using System;

namespace Star
{
	// Token: 0x020001CE RID: 462
	[Serializable]
	public struct MasterOrbExpDB_Param
	{
		// Token: 0x04000AFF RID: 2815
		public int m_ID;

		// Token: 0x04000B00 RID: 2816
		public int m_Lv;

		// Token: 0x04000B01 RID: 2817
		public int m_NextExp;
	}
}
