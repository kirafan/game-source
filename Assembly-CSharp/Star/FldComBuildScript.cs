﻿using System;
using System.Collections.Generic;
using TownFacilityResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000355 RID: 853
	public class FldComBuildScript : IFldNetComModule
	{
		// Token: 0x0600103D RID: 4157 RVA: 0x00056110 File Offset: 0x00054510
		public FldComBuildScript() : base(IFldNetComManager.Instance)
		{
			this.m_Table = new List<FldComBuildScript.StackScriptCommand>();
			this.m_Step = FldComBuildScript.eStep.Check;
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x0005613A File Offset: 0x0005453A
		public ComItemUpState GetUpItemState()
		{
			return this.m_UpUserData;
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x00056142 File Offset: 0x00054542
		public void SetMarkTime(long fbuildtime)
		{
			this.m_TimeUp = fbuildtime;
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x0005614C File Offset: 0x0005454C
		public void AddBuildUpParam(long fmanageid, int fobjid, int fbuildpoint)
		{
			int count = this.m_BuildUp.Count;
			bool flag = false;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildUp[i].m_ManageID == fmanageid)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				FldComBuildScript.BuildUpParam buildUpParam = new FldComBuildScript.BuildUpParam();
				buildUpParam.m_ManageID = fmanageid;
				buildUpParam.m_ObjID = fobjid;
				buildUpParam.m_BuildPoint = fbuildpoint;
				this.m_BuildUp.Add(buildUpParam);
			}
		}

		// Token: 0x06001041 RID: 4161 RVA: 0x000561C4 File Offset: 0x000545C4
		public FldComBuildScript.BuildUpParam SearchBuildUpParam(int fobjid, int fbuildpoint)
		{
			int count = this.m_BuildUp.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_BuildUp[i].m_ObjID == fobjid && this.m_BuildUp[i].m_BuildPoint == fbuildpoint)
				{
					return this.m_BuildUp[i];
				}
			}
			return null;
		}

		// Token: 0x06001042 RID: 4162 RVA: 0x0005622B File Offset: 0x0005462B
		public int GetBuildUpNum()
		{
			return this.m_BuildUp.Count;
		}

		// Token: 0x06001043 RID: 4163 RVA: 0x00056238 File Offset: 0x00054638
		public FldComBuildScript.BuildUpParam GetIndexToBuildUpParam(int findex)
		{
			return this.m_BuildUp[findex];
		}

		// Token: 0x06001044 RID: 4164 RVA: 0x00056248 File Offset: 0x00054648
		private FldComBuildScript.StackScriptCommand CreateBuildCommand(FldComBuildScript.eCmd fcalc)
		{
			FldComBuildScript.StackScriptCommand stackScriptCommand = null;
			for (int i = 0; i < this.m_Table.Count; i++)
			{
				if (this.m_Table[i].m_NextStep == fcalc)
				{
					return this.m_Table[i];
				}
			}
			if (stackScriptCommand == null)
			{
				switch (fcalc)
				{
				case FldComBuildScript.eCmd.AddObj:
					stackScriptCommand = new FldComBuildScript.AddObjectCode();
					break;
				case FldComBuildScript.eCmd.AddBuildIn:
					stackScriptCommand = new FldComBuildScript.AddBuildObject();
					break;
				case FldComBuildScript.eCmd.ChangeBuild:
				case FldComBuildScript.eCmd.DelBuild:
					stackScriptCommand = new FldComBuildScript.BuildStateChange();
					break;
				case FldComBuildScript.eCmd.CheckBuildState:
					stackScriptCommand = new FldComBuildScript.BuildStateCheck();
					break;
				case FldComBuildScript.eCmd.LevelUpBuild:
					stackScriptCommand = new FldComBuildScript.BuildLevelUp();
					break;
				case FldComBuildScript.eCmd.GemLevelUpBuild:
					stackScriptCommand = new FldComBuildScript.BuildGemLevelUp();
					break;
				case FldComBuildScript.eCmd.LevelUpWait:
					stackScriptCommand = new FldComBuildScript.BuildLevelUpWait();
					break;
				case FldComBuildScript.eCmd.ObjStateChg:
				case FldComBuildScript.eCmd.StoreObjState:
					stackScriptCommand = new FldComBuildScript.BuildPointSet();
					break;
				case FldComBuildScript.eCmd.ItemUp:
					stackScriptCommand = new FldComBuildScript.BuildItemUp();
					break;
				default:
					stackScriptCommand = new FldComBuildScript.StackScriptCommand();
					break;
				}
				stackScriptCommand.m_NextStep = fcalc;
				this.m_Table.Add(stackScriptCommand);
			}
			return stackScriptCommand;
		}

		// Token: 0x06001045 RID: 4165 RVA: 0x00056358 File Offset: 0x00054758
		public void StackSendCmd(FldComBuildScript.eCmd fcalc, long fkeycode, int foption, int fsubkey, IFldNetComModule.CallBack pcallback = null)
		{
			FldComBuildScript.StackScriptCommand stackScriptCommand = this.CreateBuildCommand(fcalc);
			stackScriptCommand.AddCode(fkeycode, foption, fsubkey);
			stackScriptCommand.m_Callback = pcallback;
		}

		// Token: 0x06001046 RID: 4166 RVA: 0x0005637F File Offset: 0x0005477F
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06001047 RID: 4167 RVA: 0x00056390 File Offset: 0x00054790
		public override bool UpFunc()
		{
			bool result = true;
			FldComBuildScript.eStep step = this.m_Step;
			if (step != FldComBuildScript.eStep.Check)
			{
				if (step != FldComBuildScript.eStep.ComCheck)
				{
					if (step == FldComBuildScript.eStep.End)
					{
						result = false;
					}
				}
				else if (!this.m_Table[0].m_Active)
				{
					this.m_Table.RemoveAt(0);
					if (this.m_Callback != null)
					{
						this.m_Callback(this);
					}
					this.m_Step = FldComBuildScript.eStep.Check;
				}
			}
			else if (this.m_Table.Count != 0)
			{
				this.m_Step = FldComBuildScript.eStep.ComCheck;
				this.m_Table[0].MakeSendCom(this);
				base.EntryCallback(this.m_Table[0].m_Callback);
			}
			else
			{
				this.m_Step = FldComBuildScript.eStep.End;
				result = false;
			}
			return result;
		}

		// Token: 0x04001745 RID: 5957
		public List<FldComBuildScript.BuildUpParam> m_BuildUp = new List<FldComBuildScript.BuildUpParam>();

		// Token: 0x04001746 RID: 5958
		public long m_TimeUp;

		// Token: 0x04001747 RID: 5959
		public bool m_CreateObj;

		// Token: 0x04001748 RID: 5960
		private FldComBuildScript.eStep m_Step;

		// Token: 0x04001749 RID: 5961
		public List<FldComBuildScript.StackScriptCommand> m_Table;

		// Token: 0x0400174A RID: 5962
		public ComItemUpState m_UpUserData;

		// Token: 0x02000356 RID: 854
		public enum eCmd
		{
			// Token: 0x0400174C RID: 5964
			AddObj,
			// Token: 0x0400174D RID: 5965
			AddBuildIn,
			// Token: 0x0400174E RID: 5966
			ChangeBuild,
			// Token: 0x0400174F RID: 5967
			CheckBuildState,
			// Token: 0x04001750 RID: 5968
			LevelUpBuild,
			// Token: 0x04001751 RID: 5969
			GemLevelUpBuild,
			// Token: 0x04001752 RID: 5970
			LevelUpWait,
			// Token: 0x04001753 RID: 5971
			DelBuild,
			// Token: 0x04001754 RID: 5972
			ObjStateChg,
			// Token: 0x04001755 RID: 5973
			StoreObjState,
			// Token: 0x04001756 RID: 5974
			ItemUp,
			// Token: 0x04001757 RID: 5975
			CallEvt
		}

		// Token: 0x02000357 RID: 855
		public enum eStep
		{
			// Token: 0x04001759 RID: 5977
			Check,
			// Token: 0x0400175A RID: 5978
			ComCheck,
			// Token: 0x0400175B RID: 5979
			End
		}

		// Token: 0x02000358 RID: 856
		public class BuildUpParam
		{
			// Token: 0x0400175C RID: 5980
			public int m_ObjID;

			// Token: 0x0400175D RID: 5981
			public int m_BuildPoint;

			// Token: 0x0400175E RID: 5982
			public long m_ManageID;
		}

		// Token: 0x02000359 RID: 857
		public class StackScriptCommand
		{
			// Token: 0x0600104A RID: 4170 RVA: 0x0005646F File Offset: 0x0005486F
			public virtual void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x0600104B RID: 4171 RVA: 0x00056471 File Offset: 0x00054871
			public virtual void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x0600104C RID: 4172 RVA: 0x00056473 File Offset: 0x00054873
			public virtual bool IsAddCode()
			{
				return false;
			}

			// Token: 0x0400175F RID: 5983
			public FldComBuildScript.eCmd m_NextStep;

			// Token: 0x04001760 RID: 5984
			public IFldNetComModule.CallBack m_Callback;

			// Token: 0x04001761 RID: 5985
			public bool m_Active;

			// Token: 0x04001762 RID: 5986
			public FldComBuildScript m_IO;
		}

		// Token: 0x0200035A RID: 858
		public class AddBuildObject : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600104E RID: 4174 RVA: 0x0005649F File Offset: 0x0005489F
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x0600104F RID: 4175 RVA: 0x000564A2 File Offset: 0x000548A2
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				this.m_ManageID.Add(fkeycode);
				this.m_ObjID.Add(fsuboption);
				this.m_BuildPoint.Add(fsubkey);
			}

			// Token: 0x06001050 RID: 4176 RVA: 0x000564C8 File Offset: 0x000548C8
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				for (int i = 0; i < this.m_ManageID.Count; i++)
				{
					if (this.m_ManageID[i] == -1L)
					{
						FldComBuildScript.BuildUpParam buildUpParam = pbase.SearchBuildUpParam(this.m_ObjID[i], this.m_BuildPoint[i]);
						if (buildUpParam != null)
						{
							this.m_ManageID[i] = buildUpParam.m_ManageID;
						}
					}
					else
					{
						pbase.AddBuildUpParam(this.m_ManageID[i], this.m_ObjID[i], this.m_BuildPoint[i]);
					}
					UserTownData.BuildObjectData buildObject = FieldBuildMap.CreateTownBuildData(this.m_BuildPoint[i], this.m_ObjID[i], this.m_ManageID[i], pbase.m_CreateObj, pbase.m_TimeUp);
					UserDataUtil.TownData.AddBuildObjectData(buildObject);
				}
				TownComAPISet townComAPISet = new TownComAPISet();
				townComAPISet.managedTownId = userTownData.MngID;
				townComAPISet.gridData = UserTownUtil.CreateTownBuildListString(null);
				pbase.m_NetMng.SendCom(townComAPISet, new INetComHandle.ResponseCallbak(this.CallbackBuildSet), true);
			}

			// Token: 0x06001051 RID: 4177 RVA: 0x000565FC File Offset: 0x000549FC
			private void CallbackBuildSet(INetComHandle phandle)
			{
				this.m_Active = false;
			}

			// Token: 0x04001763 RID: 5987
			public List<long> m_ManageID = new List<long>();

			// Token: 0x04001764 RID: 5988
			public List<int> m_ObjID = new List<int>();

			// Token: 0x04001765 RID: 5989
			public List<int> m_BuildPoint = new List<int>();
		}

		// Token: 0x0200035B RID: 859
		public class BuildStateChange : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x06001053 RID: 4179 RVA: 0x00056610 File Offset: 0x00054A10
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				TownComAPISet townComAPISet = new TownComAPISet();
				townComAPISet.managedTownId = userTownData.MngID;
				townComAPISet.gridData = UserTownUtil.CreateTownBuildListString(null);
				pbase.m_NetMng.SendCom(townComAPISet, new INetComHandle.ResponseCallbak(this.CallbackBuildSet), true);
			}

			// Token: 0x06001054 RID: 4180 RVA: 0x0005666B File Offset: 0x00054A6B
			private void CallbackBuildSet(INetComHandle phandle)
			{
				this.m_Active = false;
			}
		}

		// Token: 0x0200035C RID: 860
		public class BuildStateCheck : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x06001056 RID: 4182 RVA: 0x0005667C File Offset: 0x00054A7C
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_IO = pbase;
				List<int> list = new List<int>();
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.IsCheckStateChenge())
					{
						list.Add(i);
					}
				}
				if (list.Count == 0)
				{
					this.m_Active = true;
					this.CallbackBuildPointSet(null);
				}
				else
				{
					TownComAPIObjbuildPointSetAll townComAPIObjbuildPointSetAll = new TownComAPIObjbuildPointSetAll();
					townComAPIObjbuildPointSetAll.townFacilitySetStates = new TownFacilitySetState[list.Count];
					for (int i = 0; i < list.Count; i++)
					{
						UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(list[i]);
						UserTownObjectData townObjData = UserTownUtil.GetTownObjData(buildObjectDataAt.m_ManageID);
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i] = new TownFacilitySetState();
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].managedTownFacilityId = buildObjectDataAt.m_ManageID;
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].buildPointIndex = townObjData.BuildPoint;
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].openState = (int)townObjData.OpenState;
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].buildTime = townObjData.BuildTime;
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].actionNo = 0;
						buildObjectDataAt.ClearChangeState();
					}
					pbase.m_NetMng.SendCom(townComAPIObjbuildPointSetAll, new INetComHandle.ResponseCallbak(this.CallbackBuildPointSet), true);
				}
			}

			// Token: 0x06001057 RID: 4183 RVA: 0x000567D4 File Offset: 0x00054BD4
			private void CallbackBuildPointSet(INetComHandle phandle)
			{
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				TownComAPISet townComAPISet = new TownComAPISet();
				townComAPISet.managedTownId = userTownData.MngID;
				townComAPISet.gridData = UserTownUtil.CreateTownBuildListString(null);
				this.m_IO.m_NetMng.SendCom(townComAPISet, new INetComHandle.ResponseCallbak(this.CallbackBuildSet), true);
			}

			// Token: 0x06001058 RID: 4184 RVA: 0x0005682D File Offset: 0x00054C2D
			private void CallbackBuildSet(INetComHandle phandle)
			{
				this.m_Active = false;
			}
		}

		// Token: 0x0200035D RID: 861
		public class BuildPointSet : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600105A RID: 4186 RVA: 0x0005685F File Offset: 0x00054C5F
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x0600105B RID: 4187 RVA: 0x00056862 File Offset: 0x00054C62
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				this.m_ManageID.Add(fkeycode);
				this.m_ObjID.Add(fsuboption);
				this.m_BuildPoint.Add(fsubkey);
			}

			// Token: 0x0600105C RID: 4188 RVA: 0x00056888 File Offset: 0x00054C88
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				this.m_IO = pbase;
				for (int i = 0; i < this.m_ManageID.Count; i++)
				{
					if (this.m_BuildPoint[i] >= 0)
					{
						pbase.AddBuildUpParam(this.m_ManageID[i], this.m_ObjID[i], this.m_BuildPoint[i]);
					}
				}
				TownComAPIObjbuildPointSetAll townComAPIObjbuildPointSetAll = new TownComAPIObjbuildPointSetAll();
				townComAPIObjbuildPointSetAll.townFacilitySetStates = new TownFacilitySetState[this.m_ManageID.Count];
				for (int i = 0; i < this.m_ManageID.Count; i++)
				{
					UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_ManageID[i]);
					townObjData.BuildPoint = this.m_BuildPoint[i];
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i] = new TownFacilitySetState();
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i].managedTownFacilityId = this.m_ManageID[i];
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i].buildPointIndex = townObjData.BuildPoint;
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i].openState = (int)townObjData.OpenState;
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i].buildTime = townObjData.BuildTime;
					if (pbase.m_TimeUp != 0L)
					{
						townComAPIObjbuildPointSetAll.townFacilitySetStates[i].buildTime = pbase.m_TimeUp;
						townObjData.BuildTime = pbase.m_TimeUp;
					}
					townComAPIObjbuildPointSetAll.townFacilitySetStates[i].actionNo = 0;
				}
				pbase.m_NetMng.SendCom(townComAPIObjbuildPointSetAll, new INetComHandle.ResponseCallbak(this.CallbackObjectStateChg), true);
			}

			// Token: 0x0600105D RID: 4189 RVA: 0x00056A0C File Offset: 0x00054E0C
			private void CallbackObjectStateChg(INetComHandle phandle)
			{
				Buildpointsetall buildpointsetall = phandle.GetResponse() as Buildpointsetall;
				if (buildpointsetall != null)
				{
					APIUtility.wwwToUserData(buildpointsetall.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
					this.m_Active = false;
				}
			}

			// Token: 0x04001766 RID: 5990
			public List<long> m_ManageID = new List<long>();

			// Token: 0x04001767 RID: 5991
			public List<int> m_ObjID = new List<int>();

			// Token: 0x04001768 RID: 5992
			public List<int> m_BuildPoint = new List<int>();
		}

		// Token: 0x0200035E RID: 862
		public class BuildLevelUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600105F RID: 4191 RVA: 0x00056A54 File Offset: 0x00054E54
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				this.m_ManageID = fkeycode;
				this.m_OptionCode = fsuboption;
			}

			// Token: 0x06001060 RID: 4192 RVA: 0x00056A64 File Offset: 0x00054E64
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				this.m_IO = pbase;
				UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_ManageID);
				this.m_UpObjID = townObjData.ObjID;
				this.m_UpBuildPoint = townObjData.BuildPoint;
				if (this.m_OptionCode == 0)
				{
					TownComAPIObjLeveUp townComAPIObjLeveUp = new TownComAPIObjLeveUp();
					townComAPIObjLeveUp.managedTownFacilityId = this.m_ManageID;
					townComAPIObjLeveUp.nextLevel = townObjData.Lv;
					townObjData.OpenState = UserTownObjectData.eOpenState.Open;
					townComAPIObjLeveUp.openState = 1;
					townComAPIObjLeveUp.actionTime = pbase.m_TimeUp;
					pbase.m_NetMng.SendCom(townComAPIObjLeveUp, new INetComHandle.ResponseCallbak(this.CallbackBuildLevelUp), true);
				}
				else
				{
					TownComAPIObjPreLeveUp townComAPIObjPreLeveUp = new TownComAPIObjPreLeveUp();
					townComAPIObjPreLeveUp.managedTownFacilityId = this.m_ManageID;
					townComAPIObjPreLeveUp.nextLevel = townObjData.Lv;
					townObjData.OpenState = UserTownObjectData.eOpenState.Open;
					townComAPIObjPreLeveUp.openState = 1;
					townComAPIObjPreLeveUp.buildTime = pbase.m_TimeUp;
					pbase.m_NetMng.SendCom(townComAPIObjPreLeveUp, new INetComHandle.ResponseCallbak(this.CallbackBuildLevelWait), true);
				}
			}

			// Token: 0x06001061 RID: 4193 RVA: 0x00056B55 File Offset: 0x00054F55
			private void CallbackBuildLevelUp(INetComHandle phandle)
			{
				this.m_Active = false;
				this.m_IO.AddBuildUpParam(this.m_ManageID, this.m_UpObjID, this.m_UpBuildPoint);
			}

			// Token: 0x06001062 RID: 4194 RVA: 0x00056B7C File Offset: 0x00054F7C
			private void CallbackBuildLevelWait(INetComHandle phandle)
			{
				Preparelevelup preparelevelup = phandle.GetResponse() as Preparelevelup;
				APIUtility.wwwToUserData(preparelevelup.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				this.m_IO.AddBuildUpParam(this.m_ManageID, this.m_UpObjID, this.m_UpBuildPoint);
				this.m_Active = false;
			}

			// Token: 0x04001769 RID: 5993
			public long m_ManageID;

			// Token: 0x0400176A RID: 5994
			public int m_OptionCode;

			// Token: 0x0400176B RID: 5995
			private int m_UpObjID;

			// Token: 0x0400176C RID: 5996
			private int m_UpBuildPoint;
		}

		// Token: 0x0200035F RID: 863
		public class BuildGemLevelUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x06001064 RID: 4196 RVA: 0x00056BDB File Offset: 0x00054FDB
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				this.m_ManageID = fkeycode;
				this.m_OptionCode = fsuboption;
			}

			// Token: 0x06001065 RID: 4197 RVA: 0x00056BEC File Offset: 0x00054FEC
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_ManageID);
				TownComAPIObjGemLeveUp townComAPIObjGemLeveUp = new TownComAPIObjGemLeveUp();
				townComAPIObjGemLeveUp.managedTownFacilityId = this.m_ManageID;
				townComAPIObjGemLeveUp.nextLevel = townObjData.Lv;
				townObjData.OpenState = UserTownObjectData.eOpenState.Open;
				townComAPIObjGemLeveUp.openState = 1;
				townComAPIObjGemLeveUp.actionTime = pbase.m_TimeUp;
				townComAPIObjGemLeveUp.remainingTime = UserTownUtil.GetRemainingTime(this.m_ManageID, townObjData.Lv - 1);
				pbase.m_NetMng.SendCom(townComAPIObjGemLeveUp, new INetComHandle.ResponseCallbak(this.CallbackBuildGemLevelUp), true);
			}

			// Token: 0x06001066 RID: 4198 RVA: 0x00056C78 File Offset: 0x00055078
			private void CallbackBuildGemLevelUp(INetComHandle phandle)
			{
				Gemlevelup gemlevelup = phandle.GetResponse() as Gemlevelup;
				APIUtility.wwwToUserData(gemlevelup.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				this.m_Active = false;
			}

			// Token: 0x0400176D RID: 5997
			public long m_ManageID;

			// Token: 0x0400176E RID: 5998
			public int m_OptionCode;
		}

		// Token: 0x02000360 RID: 864
		public class BuildLevelUpWait : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x06001068 RID: 4200 RVA: 0x00056CBA File Offset: 0x000550BA
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				this.m_ManageID = fkeycode;
				this.m_OptionCode = fsuboption;
			}

			// Token: 0x06001069 RID: 4201 RVA: 0x00056CCC File Offset: 0x000550CC
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_ManageID);
				TownComAPIObjPreLeveUp townComAPIObjPreLeveUp = new TownComAPIObjPreLeveUp();
				townComAPIObjPreLeveUp.managedTownFacilityId = this.m_ManageID;
				townComAPIObjPreLeveUp.nextLevel = townObjData.AddChkLevel(1);
				townObjData.OpenState = UserTownObjectData.eOpenState.OnWait;
				townComAPIObjPreLeveUp.openState = 4;
				townComAPIObjPreLeveUp.buildTime = pbase.m_TimeUp;
				pbase.m_NetMng.SendCom(townComAPIObjPreLeveUp, new INetComHandle.ResponseCallbak(this.CallbackBuildLevelWait), true);
			}

			// Token: 0x0600106A RID: 4202 RVA: 0x00056D40 File Offset: 0x00055140
			private void CallbackBuildLevelWait(INetComHandle phandle)
			{
				Preparelevelup preparelevelup = phandle.GetResponse() as Preparelevelup;
				APIUtility.wwwToUserData(preparelevelup.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				this.m_Active = false;
			}

			// Token: 0x0400176F RID: 5999
			public long m_ManageID;

			// Token: 0x04001770 RID: 6000
			public int m_OptionCode;
		}

		// Token: 0x02000361 RID: 865
		public class BuildItemUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600106C RID: 4204 RVA: 0x00056D8D File Offset: 0x0005518D
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x0600106D RID: 4205 RVA: 0x00056D90 File Offset: 0x00055190
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				FldComBuildScript.BuildItemUp.UpItemKey upItemKey = new FldComBuildScript.BuildItemUp.UpItemKey();
				upItemKey.m_ManageID = fkeycode;
				upItemKey.m_ItemNo = fsuboption;
				upItemKey.m_ItemNum = fsubkey;
				this.m_List.Add(upItemKey);
				this.m_SendNo = 0;
			}

			// Token: 0x0600106E RID: 4206 RVA: 0x00056DCC File Offset: 0x000551CC
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				this.m_IO = pbase;
				TownComAPIObjItemUp townComAPIObjItemUp = new TownComAPIObjItemUp();
				townComAPIObjItemUp.managedTownFacilityId = this.m_List[this.m_SendNo].m_ManageID;
				townComAPIObjItemUp.itemNo = this.m_List[this.m_SendNo].m_ItemNo;
				townComAPIObjItemUp.amount = this.m_List[this.m_SendNo].m_ItemNum;
				townComAPIObjItemUp.actionTime = pbase.m_TimeUp;
				pbase.m_NetMng.SendCom(townComAPIObjItemUp, new INetComHandle.ResponseCallbak(this.CallbackTownObjItemUp), true);
			}

			// Token: 0x0600106F RID: 4207 RVA: 0x00056E68 File Offset: 0x00055268
			private void CallbackTownObjItemUp(INetComHandle phandle)
			{
				Itemup itemup = phandle.GetResponse() as Itemup;
				if (this.m_IO.m_UpUserData == null)
				{
					this.m_IO.m_UpUserData = new ComItemUpState();
				}
				this.m_IO.m_UpUserData.CalcPlayerDataNewParam(itemup.player);
				this.m_IO.m_UpUserData.CalcPlayerItemNewParam(itemup.itemSummary);
				APIUtility.wwwToUserData(itemup.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				this.m_SendNo++;
				if (this.m_SendNo >= this.m_List.Count)
				{
					this.m_Active = false;
				}
				else
				{
					this.MakeSendCom(this.m_IO);
				}
			}

			// Token: 0x04001771 RID: 6001
			private List<FldComBuildScript.BuildItemUp.UpItemKey> m_List = new List<FldComBuildScript.BuildItemUp.UpItemKey>();

			// Token: 0x04001772 RID: 6002
			private int m_SendNo;

			// Token: 0x02000362 RID: 866
			public class UpItemKey
			{
				// Token: 0x04001773 RID: 6003
				public long m_ManageID;

				// Token: 0x04001774 RID: 6004
				public int m_ItemNo;

				// Token: 0x04001775 RID: 6005
				public int m_ItemNum;
			}
		}

		// Token: 0x02000363 RID: 867
		public class AddObjectCode : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x06001072 RID: 4210 RVA: 0x00056F40 File Offset: 0x00055340
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
				FldComBuildScript.AddObjectCode.AddObjState addObjState = new FldComBuildScript.AddObjectCode.AddObjState();
				addObjState.m_ObjID = (int)fkeycode;
				addObjState.m_BuildPoint = fsubkey;
				addObjState.m_OptionCode = fsuboption;
				this.m_List.Add(addObjState);
			}

			// Token: 0x06001073 RID: 4211 RVA: 0x00056F75 File Offset: 0x00055375
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x06001074 RID: 4212 RVA: 0x00056F78 File Offset: 0x00055378
			public override void MakeSendCom(FldComBuildScript pbase)
			{
				this.m_Active = true;
				this.m_IO = pbase;
				pbase.m_CreateObj = true;
				TownComAPIObjBuyAll townComAPIObjBuyAll = new TownComAPIObjBuyAll();
				townComAPIObjBuyAll.townFacilityBuyStates = new TownFacilityBuyState[this.m_List.Count];
				for (int i = 0; i < this.m_List.Count; i++)
				{
					townComAPIObjBuyAll.townFacilityBuyStates[i] = new TownFacilityBuyState();
					townComAPIObjBuyAll.townFacilityBuyStates[i].facilityId = this.m_List[i].m_ObjID;
					townComAPIObjBuyAll.townFacilityBuyStates[i].actionNo = this.m_List[i].m_OptionCode;
					townComAPIObjBuyAll.townFacilityBuyStates[i].actionTime = (townComAPIObjBuyAll.townFacilityBuyStates[i].buildTime = pbase.m_TimeUp);
					townComAPIObjBuyAll.townFacilityBuyStates[i].buildPointIndex = this.m_List[i].m_BuildPoint;
					townComAPIObjBuyAll.townFacilityBuyStates[i].openState = 2;
					townComAPIObjBuyAll.townFacilityBuyStates[i].nextLevel = 1;
					if (!UserTownUtil.IsBuildMarks(this.m_List[i].m_ObjID))
					{
						townComAPIObjBuyAll.townFacilityBuyStates[i].openState = 1;
					}
					this.m_List[i].m_OpenState = townComAPIObjBuyAll.townFacilityBuyStates[i].openState;
					this.m_List[i].m_Level = townComAPIObjBuyAll.townFacilityBuyStates[i].nextLevel;
				}
				pbase.m_NetMng.SendCom(townComAPIObjBuyAll, new INetComHandle.ResponseCallbak(this.CallbackObjectAdd), true);
			}

			// Token: 0x06001075 RID: 4213 RVA: 0x000570F8 File Offset: 0x000554F8
			private void CallbackObjectAdd(INetComHandle phandle)
			{
				Buyall buyall = phandle.GetResponse() as Buyall;
				APIUtility.wwwToUserData(buyall.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				string[] array = buyall.managedTownFacilityIds.Split(new char[]
				{
					','
				});
				for (int i = 0; i < array.Length; i++)
				{
					long num;
					long.TryParse(array[i], out num);
					this.m_IO.AddBuildUpParam(num, this.m_List[i].m_ObjID, this.m_List[i].m_BuildPoint);
					UserTownObjectData userTownObjectData = new UserTownObjectData(this.m_List[i].m_ObjID, num);
					userTownObjectData.ActionTime = (userTownObjectData.BuildTime = this.m_IO.m_TimeUp);
					userTownObjectData.BuildPoint = this.m_List[i].m_BuildPoint;
					userTownObjectData.OpenState = (UserTownObjectData.eOpenState)this.m_List[i].m_OpenState;
					userTownObjectData.Lv = 0;
					if (userTownObjectData.OpenState == UserTownObjectData.eOpenState.Open)
					{
						userTownObjectData.Lv = this.m_List[i].m_Level;
					}
					UserTownUtil.AddTownObjData(userTownObjectData);
				}
				this.m_Active = false;
			}

			// Token: 0x04001776 RID: 6006
			public List<FldComBuildScript.AddObjectCode.AddObjState> m_List = new List<FldComBuildScript.AddObjectCode.AddObjState>();

			// Token: 0x02000364 RID: 868
			public class AddObjState
			{
				// Token: 0x04001777 RID: 6007
				public int m_ObjID;

				// Token: 0x04001778 RID: 6008
				public int m_OptionCode;

				// Token: 0x04001779 RID: 6009
				public int m_BuildPoint;

				// Token: 0x0400177A RID: 6010
				public int m_OpenState;

				// Token: 0x0400177B RID: 6011
				public int m_Level;
			}
		}
	}
}
