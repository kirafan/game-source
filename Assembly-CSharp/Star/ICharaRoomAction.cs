﻿using System;

namespace Star
{
	// Token: 0x02000536 RID: 1334
	public interface ICharaRoomAction
	{
		// Token: 0x06001AA8 RID: 6824
		void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup);

		// Token: 0x06001AA9 RID: 6825
		bool UpdateObjEventMode(RoomObjectCtrlChara hbase);

		// Token: 0x06001AAA RID: 6826
		bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel);

		// Token: 0x06001AAB RID: 6827
		void Release();
	}
}
