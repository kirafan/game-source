﻿using System;
using System.Collections.Generic;
using Meige;
using Star.UI;
using Star.UI.GachaPlay;
using UnityEngine;

namespace Star
{
	// Token: 0x020003CF RID: 975
	public class GachaPlayScene
	{
		// Token: 0x1700015B RID: 347
		// (get) Token: 0x06001284 RID: 4740 RVA: 0x00062772 File Offset: 0x00060B72
		private Gacha.Result CurrentGachaResult
		{
			get
			{
				return (this.m_PlayIndex >= this.m_GachaResults.Count) ? null : this.m_GachaResults[this.m_PlayIndex];
			}
		}

		// Token: 0x06001285 RID: 4741 RVA: 0x000627A1 File Offset: 0x00060BA1
		public bool Prepare(List<Gacha.Result> gachaResults, Camera camera, bool isStartResult)
		{
			if (this.m_Mode != GachaPlayScene.eMode.None)
			{
				return false;
			}
			this.m_GachaResults = gachaResults;
			this.m_Camera = camera;
			this.m_IsStartResult = isStartResult;
			this.m_Mode = GachaPlayScene.eMode.Prepare;
			return true;
		}

		// Token: 0x06001286 RID: 4742 RVA: 0x000627CE File Offset: 0x00060BCE
		public bool IsCompletePrepare()
		{
			return this.m_Mode == GachaPlayScene.eMode.Prepare && this.m_PrepareStep == GachaPlayScene.ePrepareStep.End;
		}

		// Token: 0x06001287 RID: 4743 RVA: 0x000627E7 File Offset: 0x00060BE7
		public bool Destory()
		{
			if (!this.IsCompletePlay())
			{
				return false;
			}
			this.m_Mode = GachaPlayScene.eMode.Destroy;
			return true;
		}

		// Token: 0x06001288 RID: 4744 RVA: 0x000627FE File Offset: 0x00060BFE
		public bool IsCompleteDestory()
		{
			return this.m_Mode == GachaPlayScene.eMode.Destroy && this.m_DestroyStep == GachaPlayScene.eDestroyStep.End;
		}

		// Token: 0x06001289 RID: 4745 RVA: 0x00062818 File Offset: 0x00060C18
		public bool Play()
		{
			if (!this.IsCompletePrepare())
			{
				return false;
			}
			this.m_Mode = GachaPlayScene.eMode.UpdateMain;
			return true;
		}

		// Token: 0x0600128A RID: 4746 RVA: 0x0006282F File Offset: 0x00060C2F
		public bool IsCompletePlay()
		{
			return this.m_Mode == GachaPlayScene.eMode.UpdateMain && this.m_MainStep == GachaPlayScene.eMainStep.End;
		}

		// Token: 0x0600128B RID: 4747 RVA: 0x0006284C File Offset: 0x00060C4C
		public void Update()
		{
			this.m_Timer.Update(Time.deltaTime);
			GachaPlayScene.eMode eMode = this.m_Mode;
			GachaPlayScene.eMode mode = this.m_Mode;
			if (mode != GachaPlayScene.eMode.Prepare)
			{
				if (mode != GachaPlayScene.eMode.UpdateMain)
				{
					if (mode == GachaPlayScene.eMode.Destroy)
					{
						eMode = this.Update_Destroy();
					}
				}
				else
				{
					eMode = this.Update_Main();
				}
			}
			else
			{
				eMode = this.Update_Prepare();
			}
			if (eMode != this.m_Mode)
			{
				this.m_Mode = eMode;
				GachaPlayScene.eMode mode2 = this.m_Mode;
				if (mode2 != GachaPlayScene.eMode.Prepare)
				{
					if (mode2 != GachaPlayScene.eMode.UpdateMain)
					{
						if (mode2 == GachaPlayScene.eMode.Destroy)
						{
							this.SetDestroyStep(GachaPlayScene.eDestroyStep.First);
						}
					}
					else
					{
						this.SetMainStep(GachaPlayScene.eMainStep.First);
					}
				}
				else
				{
					this.SetPrepareStep(GachaPlayScene.ePrepareStep.First);
				}
			}
		}

		// Token: 0x0600128C RID: 4748 RVA: 0x0006290B File Offset: 0x00060D0B
		private void SetDestroyStep(GachaPlayScene.eDestroyStep step)
		{
			this.m_DestroyStep = step;
		}

		// Token: 0x0600128D RID: 4749 RVA: 0x00062914 File Offset: 0x00060D14
		private GachaPlayScene.eMode Update_Destroy()
		{
			this.m_Loader.Update();
			switch (this.m_DestroyStep)
			{
			case GachaPlayScene.eDestroyStep.First:
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Transit_Wait);
				break;
			case GachaPlayScene.eDestroyStep.Transit_Wait:
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_0);
				break;
			case GachaPlayScene.eDestroyStep.Destroy_0:
				this.StopSound();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Gacha);
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.Unload(this.m_LoadingHndl);
					this.m_LoadingHndl = null;
				}
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_1);
				break;
			case GachaPlayScene.eDestroyStep.Destroy_1:
				this.m_GachaObjectHandler.Destroy();
				this.m_GachaObjectHandler = null;
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_2);
				break;
			case GachaPlayScene.eDestroyStep.Destroy_2:
				UnityEngine.Object.Destroy(this.m_GachaObject);
				this.m_GachaObject = null;
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_3);
				break;
			case GachaPlayScene.eDestroyStep.Destroy_3:
				this.m_UI.Destroy();
				this.m_UI = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.UnloadChildSceneAsync(SceneDefine.eChildSceneID.GachaPlayUI);
				this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_4);
				break;
			case GachaPlayScene.eDestroyStep.Destroy_4:
				if (SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.GachaPlayUI))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.SetDestroyStep(GachaPlayScene.eDestroyStep.Destroy_5);
				}
				break;
			case GachaPlayScene.eDestroyStep.Destroy_5:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
				{
					this.SetDestroyStep(GachaPlayScene.eDestroyStep.End);
				}
				break;
			}
			return GachaPlayScene.eMode.Destroy;
		}

		// Token: 0x0600128E RID: 4750 RVA: 0x00062A84 File Offset: 0x00060E84
		private void SetMainStep(GachaPlayScene.eMainStep step)
		{
			this.m_MainStep = step;
			GachaPlayScene.eMainStep mainStep = this.m_MainStep;
			switch (mainStep)
			{
			case GachaPlayScene.eMainStep.Flavor:
			case GachaPlayScene.eMainStep.Result:
				this.m_UI.SetOnClickAllSkipButtonCallback(null);
				break;
			default:
				switch (mainStep)
				{
				case GachaPlayScene.eMainStep.AllSkip_0:
					this.StopSound();
					break;
				default:
					if (mainStep == GachaPlayScene.eMainStep.PlayLoop_Wait)
					{
						this.m_PlayLoopStep = GachaPlayScene.ePlayLoopStep.Chara;
					}
					break;
				case GachaPlayScene.eMainStep.SingleSkip_0:
					this.StopSound();
					break;
				}
				break;
			}
		}

		// Token: 0x0600128F RID: 4751 RVA: 0x00062B10 File Offset: 0x00060F10
		private GachaPlayScene.eMode Update_Main()
		{
			this.UpdateSound();
			if (this.m_AllGachaPlaySkipped)
			{
				switch (this.m_MainStep)
				{
				case GachaPlayScene.eMainStep.First:
				case GachaPlayScene.eMainStep.LoadCharaIllust:
				case GachaPlayScene.eMainStep.LoadCharaIllust_Wait:
					break;
				default:
					if (!this.m_AllGachaPlaySkippExecuted)
					{
						this.SetMainStep(GachaPlayScene.eMainStep.AllSkip_0);
					}
					break;
				}
			}
			else if (this.m_SingleGachaPlaySkipped)
			{
				switch (this.m_MainStep)
				{
				case GachaPlayScene.eMainStep.First:
				case GachaPlayScene.eMainStep.LoadCharaIllust:
				case GachaPlayScene.eMainStep.LoadCharaIllust_Wait:
					break;
				default:
					if (!this.m_SingleGachaPlaySkippExecuted)
					{
						this.SetMainStep(GachaPlayScene.eMainStep.SingleSkip_0);
					}
					break;
				}
			}
			switch (this.m_MainStep)
			{
			case GachaPlayScene.eMainStep.First:
				this.m_PlayIndex = 0;
				this.m_PlayLoopStep = GachaPlayScene.ePlayLoopStep.Chara;
				this.m_IsOpenedCutIn = false;
				this.m_IsPlayedCutInLoop = false;
				this.m_IsRePlayed = false;
				this.m_SingleGachaPlaySkipped = false;
				this.m_AllGachaPlaySkipped = false;
				this.m_AllGachaPlaySkippExecuted = false;
				this.m_SingleGachaPlaySkippExecuted = false;
				this.m_UI.OnClick -= this.OnClick;
				this.m_UI.OnClickCloseResult -= this.OnClickCloseResult;
				this.m_UI.OnCloseResult -= this.OnCloseResult;
				this.m_UI.OnCloseCharaFlavors -= this.OnCloseCharaFlavors;
				this.m_CharaListParam = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.CurrentGachaResult.m_CharaID);
				if (this.m_IsStartResult)
				{
					this.m_IsStartResult = false;
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
					this.SetMainStep(GachaPlayScene.eMainStep.Result);
				}
				else
				{
					this.SetMainStep(GachaPlayScene.eMainStep.LoadCharaIllust);
				}
				break;
			case GachaPlayScene.eMainStep.LoadCharaIllust:
				this.m_UI.LoadCharaIllust(this.m_CharaListParam.m_CharaID);
				this.SetMainStep(GachaPlayScene.eMainStep.LoadCharaIllust_Wait);
				this.m_SingleGachaPlaySkipped = false;
				this.m_SingleGachaPlaySkippExecuted = false;
				break;
			case GachaPlayScene.eMainStep.LoadCharaIllust_Wait:
				if (!this.m_UI.IsLoadingCharaIllust())
				{
					this.m_IsOpenedCutIn = false;
					this.m_IsPlayedCutInLoop = false;
					this.m_GachaObjectHandler.ResetCamera();
					if (this.m_PlayIndex == 0)
					{
						this.m_UI.OpenWaitTouchGroup();
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
						this.SetMainStep(GachaPlayScene.eMainStep.FirstFadeIn_Wait);
					}
					else
					{
						this.m_GachaObjectHandler.ResetMeshColor();
						this.m_GachaObjectHandler.ResetMeshVisibility();
						this.m_GachaObjectHandler.SetMagicCircle((eClassType)this.m_CharaListParam.m_Class);
						eRare rare = (eRare)this.m_CharaListParam.m_Rare;
						GachaObjectHandler.eAnimType eAnimType = this.RareToAnimType(rare, false);
						this.m_GachaObjectHandler.Play(eAnimType, 0f);
						int num = this.RareToLv(rare);
						if (num != 1)
						{
							if (num != 2)
							{
								this.m_GachaSounds[6].Play();
							}
							else
							{
								this.m_GachaSounds[4].Play();
							}
						}
						else
						{
							this.m_GachaSounds[2].Play();
						}
						if (eAnimType == GachaObjectHandler.eAnimType.Branch_lv3)
						{
							this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_0);
						}
						else
						{
							this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_Wait);
						}
					}
				}
				break;
			case GachaPlayScene.eMainStep.FirstFadeIn_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_UI.OnClick += this.OnClick;
					this.SetMainStep(GachaPlayScene.eMainStep.TapToStart);
				}
				break;
			case GachaPlayScene.eMainStep.PlayStart:
				this.m_GachaObjectHandler.SetMagicCircle((eClassType)this.m_CharaListParam.m_Class);
				this.m_GachaObjectHandler.Play(GachaObjectHandler.eAnimType.Start, 0f);
				this.m_GachaSounds[0].Play();
				this.SetMainStep(GachaPlayScene.eMainStep.PlayStart_Wait);
				break;
			case GachaPlayScene.eMainStep.PlayStart_Wait:
				if (!this.m_GachaObjectHandler.IsPlaying())
				{
					eRare rare2 = (eRare)this.m_CharaListParam.m_Rare;
					GachaObjectHandler.eAnimType eAnimType2 = this.RareToAnimType(rare2, false);
					this.m_GachaObjectHandler.Play(eAnimType2, 0f);
					int num2 = this.RareToLv(rare2);
					if (num2 != 1)
					{
						if (num2 != 2)
						{
							this.m_GachaSounds[6].Play();
						}
						else
						{
							this.m_GachaSounds[4].Play();
						}
					}
					else
					{
						this.m_GachaSounds[2].Play();
					}
					if (eAnimType2 == GachaObjectHandler.eAnimType.Branch_lv3)
					{
						this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_0);
					}
					else
					{
						this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_Wait);
					}
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_Wait:
				if (!this.m_GachaObjectHandler.IsPlaying())
				{
					this.PlayBranchLoop();
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.37f, null);
					this.SetMainStep(GachaPlayScene.eMainStep.PlayLoop_Wait);
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_0:
				if (!this.m_IsPlayedCutInLoop && !this.m_GachaObjectHandler.IsPlaying())
				{
					this.m_IsPlayedCutInLoop = true;
					this.m_GachaObjectHandler.Play(GachaObjectHandler.eAnimType.Branch_lv3_cutIn_loop, 0f);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.37f, null);
					this.m_GachaSounds[7].Play();
					this.OnCutIn();
				}
				if (this.m_IsOpenedCutIn && !this.m_UI.GetCutInGroup().IsOpen())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.4f, null);
					this.m_GachaSounds[7].Stop();
					this.m_GachaSounds[8].Play();
					this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_1);
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.CurrentGachaResult.m_ItemID == -1)
					{
						CharacterIllustOffsetDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaIllustOffsetDB.GetParam(this.m_CharaListParam.m_CharaID);
						this.m_UI.GetAppearCutIn().Prepare(this.m_CharaListParam.m_CharaID, param.m_GachaCutInPivotYs);
						this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_2);
					}
					else
					{
						this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_3);
					}
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_2:
				if (this.m_UI.GetAppearCutIn().IsDonePrepare())
				{
					this.m_UI.GetAppearCutIn().Play();
					this.m_GachaSounds[9].Play();
					this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_3);
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_3:
				if (!this.m_UI.GetAppearCutIn().IsPlaying())
				{
					this.m_Timer.SetTimeAndPlay(0.6f);
					this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_4);
				}
				break;
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_4:
				if (!this.m_IsPlayedCutInLoop && !this.m_GachaObjectHandler.IsPlaying())
				{
					this.m_IsPlayedCutInLoop = true;
					this.m_GachaObjectHandler.Play(GachaObjectHandler.eAnimType.Branch_lv3_cutIn_loop, 0f);
				}
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.m_Timer.Sec <= 0f)
					{
						this.PlayBranchLoop();
						this.m_UI.GetAppearCutIn().Destroy();
						SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
						SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.37f, null);
						this.SetMainStep(GachaPlayScene.eMainStep.PlayLoop_Wait);
					}
				}
				break;
			case GachaPlayScene.eMainStep.CurrentEnd:
				this.m_UI.CloseShowChara();
				this.m_UI.CloseShowItem();
				this.m_GachaObjectHandler.Fade(1f, 0f, 0.4f);
				this.m_GachaSounds[11].Stop();
				this.SetMainStep(GachaPlayScene.eMainStep.CurrentEnd_Wait_0);
				break;
			case GachaPlayScene.eMainStep.CurrentEnd_Wait_0:
				if (this.m_UI.IsCompleteCloseShowChara())
				{
					if (this.CurrentGachaResult.m_ItemID == -1 || this.m_UI.IsCompleteCloseShowItem())
					{
						this.m_Timer.SetTimeAndPlay(0.4f);
						this.SetMainStep(GachaPlayScene.eMainStep.CurrentEnd_Wait_1);
					}
				}
				break;
			case GachaPlayScene.eMainStep.CurrentEnd_Wait_1:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_PlayIndex++;
					if (this.m_PlayIndex < this.m_GachaResults.Count)
					{
						this.m_CharaListParam = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.CurrentGachaResult.m_CharaID);
						this.SetMainStep(GachaPlayScene.eMainStep.LoadCharaIllust);
					}
					else
					{
						this.SetMainStep(GachaPlayScene.eMainStep.Flavor);
					}
				}
				break;
			case GachaPlayScene.eMainStep.Flavor:
			{
				this.m_GachaObjectHandler.Pause();
				this.m_GachaObjectHandler.KillParticle();
				this.m_GachaObjectHandler.ResetMeshVisibility();
				this.m_GachaObjectHandler.ResetCamera();
				List<int> list = new List<int>();
				for (int i = 0; i < this.m_GachaResults.Count; i++)
				{
					if (this.m_GachaResults[i].m_ItemID == -1)
					{
						list.Add(this.m_GachaResults[i].m_CharaID);
					}
				}
				if (list.Count > 0)
				{
					this.m_UI.OpenCharaFlavors(list.ToArray());
					this.m_UI.OnCloseCharaFlavors += this.OnCloseCharaFlavors;
					this.SetMainStep(GachaPlayScene.eMainStep.Flavor_Wait);
				}
				else
				{
					this.SetMainStep(GachaPlayScene.eMainStep.Result);
				}
				break;
			}
			case GachaPlayScene.eMainStep.Result:
				this.m_UI.OpenResult(this.m_GachaResults);
				this.m_UI.OnClickCloseResult += this.OnClickCloseResult;
				this.m_UI.OnCloseResult += this.OnCloseResult;
				this.SetMainStep(GachaPlayScene.eMainStep.Result_Wait);
				break;
			case GachaPlayScene.eMainStep.RePlayTransit_0:
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				this.SetMainStep(GachaPlayScene.eMainStep.RePlayTransit_1);
				break;
			case GachaPlayScene.eMainStep.RePlayTransit_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_GachaObjectHandler.Pause();
					this.m_GachaObjectHandler.ResetMeshColor();
					this.m_GachaObjectHandler.ResetMeshVisibility();
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.SetMainStep(GachaPlayScene.eMainStep.RePlayTransit_2);
				}
				break;
			case GachaPlayScene.eMainStep.RePlayTransit_2:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.SetMainStep(GachaPlayScene.eMainStep.First);
				}
				break;
			case GachaPlayScene.eMainStep.AllSkip_0:
			{
				this.m_AllGachaPlaySkippExecuted = true;
				Color color = SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.GetColor();
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.3f, null);
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(color.a);
				this.SetMainStep(GachaPlayScene.eMainStep.AllSkip_1);
				break;
			}
			case GachaPlayScene.eMainStep.AllSkip_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_PlayIndex = this.m_GachaResults.Count;
					this.m_GachaObjectHandler.Pause();
					this.m_GachaObjectHandler.KillParticle();
					this.m_GachaObjectHandler.ResetMeshVisibility();
					this.m_GachaObjectHandler.ResetCamera();
					this.m_UI.AbortOnAllSkip();
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(0.3f, null);
					this.SetMainStep(GachaPlayScene.eMainStep.AllSkip_2);
				}
				break;
			case GachaPlayScene.eMainStep.AllSkip_2:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.SetMainStep(GachaPlayScene.eMainStep.Flavor);
				}
				break;
			case GachaPlayScene.eMainStep.SingleSkip_0:
				this.m_SingleGachaPlaySkippExecuted = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_GachaObjectHandler.Pause();
					this.m_GachaObjectHandler.KillParticle();
					this.m_GachaObjectHandler.ResetMeshVisibility();
					this.m_GachaObjectHandler.ResetCamera();
					this.m_UI.AbortOnAllSkip();
					this.m_CharaListParam = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.CurrentGachaResult.m_CharaID);
					if (this.m_CharaListParam.m_Rare >= 4)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0f, null);
						this.SetMainStep(GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_1);
					}
					else
					{
						this.PlayBranchLoop();
						this.SetMainStep(GachaPlayScene.eMainStep.PlayLoop_Wait);
					}
				}
				break;
			}
			return GachaPlayScene.eMode.UpdateMain;
		}

		// Token: 0x06001290 RID: 4752 RVA: 0x00063768 File Offset: 0x00061B68
		private void OnClick()
		{
			bool flag = false;
			switch (this.m_MainStep)
			{
			case GachaPlayScene.eMainStep.TapToStart:
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request("Voice_Original_006", "voice_gacha_000", null, null, false);
				this.m_UI.CloseWaitTouchGroup();
				this.m_UI.SetOnClickAllSkipButtonCallback(new Action(this.OnAllSkip));
				this.SetMainStep(GachaPlayScene.eMainStep.PlayStart);
				break;
			case GachaPlayScene.eMainStep.PlayStart:
			case GachaPlayScene.eMainStep.PlayStart_Wait:
			case GachaPlayScene.eMainStep.PlayBranch_Wait:
			case GachaPlayScene.eMainStep.PlayBranch_lv3_Wait_0:
				flag = true;
				break;
			case GachaPlayScene.eMainStep.PlayLoop_Wait:
			{
				GachaPlayScene.ePlayLoopStep playLoopStep = this.m_PlayLoopStep;
				if (playLoopStep != GachaPlayScene.ePlayLoopStep.Chara)
				{
					if (playLoopStep == GachaPlayScene.ePlayLoopStep.Item)
					{
						if (this.m_UI.IsCompleteOpenShowItem())
						{
							this.SetMainStep(GachaPlayScene.eMainStep.CurrentEnd);
						}
					}
				}
				else if (this.m_UI.IsCompleteOpenShowChara())
				{
					if (this.CurrentGachaResult.m_ItemID != -1)
					{
						this.m_UI.OpenShowItem(this.CurrentGachaResult.m_ItemID);
						this.m_PlayLoopStep = GachaPlayScene.ePlayLoopStep.Item;
					}
					else
					{
						this.SetMainStep(GachaPlayScene.eMainStep.CurrentEnd);
					}
				}
				break;
			}
			}
			if (flag && !this.m_AllGachaPlaySkipped && !this.m_SingleGachaPlaySkipped)
			{
				this.m_AllGachaPlaySkipped = false;
				this.m_SingleGachaPlaySkipped = true;
			}
		}

		// Token: 0x06001291 RID: 4753 RVA: 0x000638BA File Offset: 0x00061CBA
		private void OnAllSkip()
		{
			this.m_SingleGachaPlaySkipped = false;
			this.m_AllGachaPlaySkipped = true;
			this.m_UI.SetOnClickAllSkipButtonCallback(null);
			this.m_UI.OnClick -= this.OnClick;
		}

		// Token: 0x06001292 RID: 4754 RVA: 0x000638ED File Offset: 0x00061CED
		private void OnCloseCharaFlavors()
		{
			if (this.m_MainStep == GachaPlayScene.eMainStep.Flavor_Wait)
			{
				this.m_UI.OnCloseCharaFlavors -= this.OnCloseCharaFlavors;
				this.SetMainStep(GachaPlayScene.eMainStep.Result);
			}
		}

		// Token: 0x06001293 RID: 4755 RVA: 0x0006391C File Offset: 0x00061D1C
		private void OnClickCloseResult()
		{
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.GachaPlayed_NextIsGachaDecide)
			{
				this.m_UI.OpenReplayWindow(new Action(this.OnClickReplayYes), new Action(this.OnClickReplayNo));
			}
			else
			{
				this.m_UI.CloseResult();
			}
		}

		// Token: 0x06001294 RID: 4756 RVA: 0x00063973 File Offset: 0x00061D73
		private void OnClickReplayYes()
		{
			this.m_UI.OnClickCloseResult -= this.OnClickCloseResult;
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.Request_TutorialPartySet(new Action(this.OnResponse_TutorialPartySet));
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x000639A7 File Offset: 0x00061DA7
		private void OnClickReplayNo()
		{
			this.m_UI.OnClickCloseResult -= this.OnClickCloseResult;
			SingletonMonoBehaviour<GameSystem>.Inst.Gacha.Request_GachaRePlay(new Action<GachaDefine.eReturnGachaPlay, string>(this.OnResponse_GachaRePlay));
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x000639DC File Offset: 0x00061DDC
		private void OnResponse_GachaRePlay(GachaDefine.eReturnGachaPlay ret, string errorMessage)
		{
			switch (ret)
			{
			case GachaDefine.eReturnGachaPlay.Success:
				this.m_IsRePlayed = true;
				this.m_GachaResults = SingletonMonoBehaviour<GameSystem>.Inst.Gacha.GetResults();
				this.m_UI.CloseResult();
				break;
			}
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x00063A3D File Offset: 0x00061E3D
		private void OnResponse_TutorialPartySet()
		{
			this.m_UI.CloseResult();
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x00063A4C File Offset: 0x00061E4C
		private void OnCloseResult()
		{
			if (this.m_MainStep == GachaPlayScene.eMainStep.Result_Wait)
			{
				this.m_UI.OnCloseResult -= this.OnCloseResult;
				if (this.m_IsRePlayed)
				{
					this.SetMainStep(GachaPlayScene.eMainStep.RePlayTransit_0);
				}
				else
				{
					this.SetMainStep(GachaPlayScene.eMainStep.End);
				}
			}
		}

		// Token: 0x06001299 RID: 4761 RVA: 0x00063AA0 File Offset: 0x00061EA0
		private void OnCutIn()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			int num = -1;
			for (int i = 0; i < dbMng.GachaCutInListDB.m_Params.Length; i++)
			{
				if (!string.IsNullOrEmpty(dbMng.GachaCutInListDB.m_Params[i].m_CharaID))
				{
					string[] array = dbMng.GachaCutInListDB.m_Params[i].m_CharaID.Split(new char[]
					{
						';'
					});
					for (int j = 0; j < array.Length; j++)
					{
						int num2;
						if (int.TryParse(array[j], out num2) && this.m_CharaListParam.m_CharaID == num2)
						{
							num = i;
							break;
						}
					}
					if (num >= 0)
					{
						break;
					}
				}
			}
			if (num < 0 && UnityEngine.Random.Range(0, 2) == 0)
			{
				eTitleType titleType = CharacterUtility.GetTitleType((eCharaNamedType)this.m_CharaListParam.m_NamedType);
				for (int k = 0; k < dbMng.GachaCutInListDB.m_Params.Length; k++)
				{
					if (dbMng.GachaCutInListDB.m_Params[k].m_TitleType == (int)titleType)
					{
						num = k;
						break;
					}
				}
			}
			if (num < 0)
			{
				num = 0;
			}
			this.m_UI.OpenCutIn(dbMng.GachaCutInListDB.m_Params[num].m_CutInText, "Voice_Original_006", dbMng.GachaCutInListDB.m_Params[num].m_CutInVoiceCueName);
			this.m_IsOpenedCutIn = true;
		}

		// Token: 0x0600129A RID: 4762 RVA: 0x00063C2A File Offset: 0x0006202A
		private void OnCharaIllust()
		{
			this.m_UI.OpenShowChara(this.m_CharaListParam.m_Rare >= 3);
		}

		// Token: 0x0600129B RID: 4763 RVA: 0x00063C48 File Offset: 0x00062048
		private void UpdateSound()
		{
			for (int i = 0; i < this.m_GachaSounds.Length; i++)
			{
				this.m_GachaSounds[i].Update();
			}
		}

		// Token: 0x0600129C RID: 4764 RVA: 0x00063C7C File Offset: 0x0006207C
		private void StopSound()
		{
			for (int i = 0; i < this.m_GachaSounds.Length; i++)
			{
				this.m_GachaSounds[i].Stop();
			}
		}

		// Token: 0x0600129D RID: 4765 RVA: 0x00063CB0 File Offset: 0x000620B0
		private GachaObjectHandler.eAnimType PlayBranchLoop()
		{
			this.m_GachaObjectHandler.KillParticle();
			this.m_GachaObjectHandler.ResetMeshVisibility();
			eRare rare = (eRare)this.m_CharaListParam.m_Rare;
			GachaObjectHandler.eAnimType eAnimType = this.RareToAnimType(rare, true);
			this.m_GachaObjectHandler.Play(eAnimType, 0f);
			this.m_UI.OpenShowChara(rare >= eRare.Star4);
			int num = this.RareToLv(rare);
			if (num != 1)
			{
				if (num != 2)
				{
					this.m_GachaSounds[10].Play();
					this.m_GachaSounds[11].Play();
				}
				else
				{
					this.m_GachaSounds[5].Play();
					this.m_GachaSounds[11].Play();
				}
			}
			else
			{
				this.m_GachaSounds[3].Play();
				this.m_GachaSounds[11].Play();
			}
			return eAnimType;
		}

		// Token: 0x0600129E RID: 4766 RVA: 0x00063D88 File Offset: 0x00062188
		private GachaObjectHandler.eAnimType RareToAnimType(eRare rareType, bool isLoop)
		{
			switch (rareType)
			{
			case eRare.Star1:
			case eRare.Star2:
			case eRare.Star3:
				return (!isLoop) ? GachaObjectHandler.eAnimType.Branch_lv1 : GachaObjectHandler.eAnimType.Branch_lv1_loop;
			case eRare.Star4:
				return (!isLoop) ? GachaObjectHandler.eAnimType.Branch_lv2 : GachaObjectHandler.eAnimType.Branch_lv2_loop;
			default:
				return (!isLoop) ? GachaObjectHandler.eAnimType.Branch_lv3 : GachaObjectHandler.eAnimType.Branch_lv3_loop;
			}
		}

		// Token: 0x0600129F RID: 4767 RVA: 0x00063DD9 File Offset: 0x000621D9
		private int RareToLv(eRare rareType)
		{
			switch (rareType)
			{
			case eRare.Star1:
			case eRare.Star2:
			case eRare.Star3:
				return 1;
			case eRare.Star4:
				return 2;
			default:
				return 3;
			}
		}

		// Token: 0x060012A0 RID: 4768 RVA: 0x00063DFB File Offset: 0x000621FB
		private void SetPrepareStep(GachaPlayScene.ePrepareStep step)
		{
			this.m_PrepareStep = step;
		}

		// Token: 0x060012A1 RID: 4769 RVA: 0x00063E04 File Offset: 0x00062204
		private GachaPlayScene.eMode Update_Prepare()
		{
			this.m_Loader.Update();
			switch (this.m_PrepareStep)
			{
			case GachaPlayScene.ePrepareStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(0f);
					this.SetPrepareStep(GachaPlayScene.ePrepareStep.Prepare);
				}
				break;
			case GachaPlayScene.ePrepareStep.Prepare:
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PrepareBGM(eSoundBgmListDB.BGM_GACHAPLAY, 1f, 0, -1, -1);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Gacha);
				this.m_LoadingHndl = this.m_Loader.Load("gacha/gacha.muast", new MeigeResource.Option[0]);
				if (this.m_LoadingHndl == null)
				{
				}
				this.SetPrepareStep(GachaPlayScene.ePrepareStep.Prepare_Wait);
				break;
			case GachaPlayScene.ePrepareStep.Prepare_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsCompletePrepareBGM())
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets())
					{
						if (this.m_LoadingHndl != null)
						{
							if (this.m_LoadingHndl.IsError())
							{
								this.SetPrepareStep(GachaPlayScene.ePrepareStep.PrepareError);
								break;
							}
							if (!this.m_LoadingHndl.IsDone())
							{
								break;
							}
						}
						this.SetPrepareStep(GachaPlayScene.ePrepareStep.UIPrepare);
					}
				}
				break;
			case GachaPlayScene.ePrepareStep.UIPrepare:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.GachaPlayUI, true);
				this.SetPrepareStep(GachaPlayScene.ePrepareStep.UIPrepare_Wait);
				break;
			case GachaPlayScene.ePrepareStep.UIPrepare_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.GachaPlayUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.GachaPlayUI);
					this.SetPrepareStep(GachaPlayScene.ePrepareStep.Setup);
				}
				break;
			case GachaPlayScene.ePrepareStep.Setup:
				this.m_UI = UIUtility.GetMenuComponent<GachaPlayUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.GachaPlayUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					this.m_UI.Setup();
					GameObject gameObject = this.m_LoadingHndl.FindObj("Gacha", true) as GameObject;
					if (gameObject != null)
					{
						this.m_GachaObject = UnityEngine.Object.Instantiate<GameObject>(gameObject);
						this.m_GachaObjectHandler = this.m_GachaObject.AddComponent<GachaObjectHandler>();
					}
					MeigeAnimClipHolder[] array = new MeigeAnimClipHolder[this.ANIM_OBJECT_PATHS.Length];
					for (int i = 0; i < array.Length; i++)
					{
						GameObject gameObject2 = this.m_LoadingHndl.FindObj(this.ANIM_OBJECT_PATHS[i], true) as GameObject;
						if (gameObject2 != null)
						{
							array[i] = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
						}
					}
					if (this.m_GachaObjectHandler != null)
					{
						this.m_GachaObjectHandler.Setup(gameObject.name, array);
						this.m_GachaObjectHandler.AttachCamera(this.m_Camera);
					}
					if (this.m_LoadingHndl != null)
					{
						this.m_Loader.Unload(this.m_LoadingHndl);
						this.m_LoadingHndl = null;
					}
					this.SetPrepareStep(GachaPlayScene.ePrepareStep.End);
				}
				break;
			case GachaPlayScene.ePrepareStep.End:
				return GachaPlayScene.eMode.UpdateMain;
			case GachaPlayScene.ePrepareStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.SetPrepareStep(GachaPlayScene.ePrepareStep.PrepareError_Wait);
				break;
			}
			return GachaPlayScene.eMode.Prepare;
		}

		// Token: 0x0400191C RID: 6428
		private GachaPlayScene.eMode m_Mode = GachaPlayScene.eMode.None;

		// Token: 0x0400191D RID: 6429
		private bool m_IsStartResult;

		// Token: 0x0400191E RID: 6430
		private List<Gacha.Result> m_GachaResults;

		// Token: 0x0400191F RID: 6431
		private Camera m_Camera;

		// Token: 0x04001920 RID: 6432
		private GameObject m_GachaObject;

		// Token: 0x04001921 RID: 6433
		private GachaObjectHandler m_GachaObjectHandler;

		// Token: 0x04001922 RID: 6434
		private GachaPlayUI m_UI;

		// Token: 0x04001923 RID: 6435
		private SimpleTimer m_Timer = new SimpleTimer(0f);

		// Token: 0x04001924 RID: 6436
		public const string VOICE_CLAIRE_CUE_SHEET = "Voice_Original_006";

		// Token: 0x04001925 RID: 6437
		private const string RESOURCE_PATH = "gacha/gacha.muast";

		// Token: 0x04001926 RID: 6438
		private const string MODEL_OBJECT_PATH = "Gacha";

		// Token: 0x04001927 RID: 6439
		private readonly string[] ANIM_OBJECT_PATHS = new string[]
		{
			"MeigeAC_Gacha@start",
			"MeigeAC_Gacha@branch_lv1",
			"MeigeAC_Gacha@branch_lv1_loop",
			"MeigeAC_Gacha@branch_lv2",
			"MeigeAC_Gacha@branch_lv2_loop",
			"MeigeAC_Gacha@branch_lv3",
			"MeigeAC_Gacha@branch_lv3_cutin_loop",
			"MeigeAC_Gacha@branch_lv3_loop"
		};

		// Token: 0x04001928 RID: 6440
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x04001929 RID: 6441
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x0400192A RID: 6442
		private GachaPlayScene.eDestroyStep m_DestroyStep;

		// Token: 0x0400192B RID: 6443
		private const float GACHA_FADE_IN_SEC_ON_BRANCH_LOOP = 0.37f;

		// Token: 0x0400192C RID: 6444
		private const float GACHA_FADE_SEC = 0.4f;

		// Token: 0x0400192D RID: 6445
		private const float GACHA_FADE_WAIT_SEC = 0.2f;

		// Token: 0x0400192E RID: 6446
		private const float GACHA_FADE_OUT_TO_IN_SEC = 0.6f;

		// Token: 0x0400192F RID: 6447
		private const float GACHA_END_WAIT_SEC = 0.4f;

		// Token: 0x04001930 RID: 6448
		private const float SKIP_FADE_SEC = 0.3f;

		// Token: 0x04001931 RID: 6449
		private int m_PlayIndex;

		// Token: 0x04001932 RID: 6450
		private CharacterListDB_Param m_CharaListParam;

		// Token: 0x04001933 RID: 6451
		private GachaPlayScene.ePlayLoopStep m_PlayLoopStep;

		// Token: 0x04001934 RID: 6452
		private bool m_IsOpenedCutIn;

		// Token: 0x04001935 RID: 6453
		private bool m_IsPlayedCutInLoop;

		// Token: 0x04001936 RID: 6454
		private bool m_SingleGachaPlaySkipped;

		// Token: 0x04001937 RID: 6455
		private bool m_AllGachaPlaySkipped;

		// Token: 0x04001938 RID: 6456
		private bool m_SingleGachaPlaySkippExecuted;

		// Token: 0x04001939 RID: 6457
		private bool m_AllGachaPlaySkippExecuted;

		// Token: 0x0400193A RID: 6458
		private bool m_IsRePlayed;

		// Token: 0x0400193B RID: 6459
		private GachaPlayScene.GachaSound[] m_GachaSounds = new GachaPlayScene.GachaSound[]
		{
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_00, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_00, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_01, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_02, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_05, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_06, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_07, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_08, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_09A, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_09B, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_10, 0f),
			new GachaPlayScene.GachaSound(eSoundSeListDB.GACHA_03, 0f)
		};

		// Token: 0x0400193C RID: 6460
		private GachaPlayScene.eMainStep m_MainStep;

		// Token: 0x0400193D RID: 6461
		private GachaPlayScene.ePrepareStep m_PrepareStep;

		// Token: 0x020003D0 RID: 976
		public enum eMode
		{
			// Token: 0x0400193F RID: 6463
			None = -1,
			// Token: 0x04001940 RID: 6464
			Prepare,
			// Token: 0x04001941 RID: 6465
			UpdateMain,
			// Token: 0x04001942 RID: 6466
			Destroy
		}

		// Token: 0x020003D1 RID: 977
		public enum eGachaBranch
		{
			// Token: 0x04001944 RID: 6468
			Lv1,
			// Token: 0x04001945 RID: 6469
			Lv2,
			// Token: 0x04001946 RID: 6470
			Lv3,
			// Token: 0x04001947 RID: 6471
			Num
		}

		// Token: 0x020003D2 RID: 978
		private enum eDestroyStep
		{
			// Token: 0x04001949 RID: 6473
			None = -1,
			// Token: 0x0400194A RID: 6474
			First,
			// Token: 0x0400194B RID: 6475
			Transit_Wait,
			// Token: 0x0400194C RID: 6476
			Destroy_0,
			// Token: 0x0400194D RID: 6477
			Destroy_1,
			// Token: 0x0400194E RID: 6478
			Destroy_2,
			// Token: 0x0400194F RID: 6479
			Destroy_3,
			// Token: 0x04001950 RID: 6480
			Destroy_4,
			// Token: 0x04001951 RID: 6481
			Destroy_5,
			// Token: 0x04001952 RID: 6482
			End
		}

		// Token: 0x020003D3 RID: 979
		private enum ePlayLoopStep
		{
			// Token: 0x04001954 RID: 6484
			Chara,
			// Token: 0x04001955 RID: 6485
			Item
		}

		// Token: 0x020003D4 RID: 980
		public class GachaSound
		{
			// Token: 0x060012A2 RID: 4770 RVA: 0x00064110 File Offset: 0x00062510
			public GachaSound(eSoundSeListDB cueID, float delay)
			{
				this.m_CueID = cueID;
				this.m_Delay = delay;
			}

			// Token: 0x060012A3 RID: 4771 RVA: 0x00064126 File Offset: 0x00062526
			public void Stop()
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHndl, true);
				this.m_SoundHndl = null;
				this.m_RequestPlay = 0;
			}

			// Token: 0x060012A4 RID: 4772 RVA: 0x0006414C File Offset: 0x0006254C
			public void Play()
			{
				this.m_RequestPlay = 1;
				this.m_Timer = 0f;
				if (this.m_Delay <= 0f)
				{
					this.m_RequestPlay = 2;
					this.m_SoundHndl = new SoundHandler();
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_CueID, this.m_SoundHndl, 1f, 0, -1, -1);
				}
			}

			// Token: 0x060012A5 RID: 4773 RVA: 0x000641B4 File Offset: 0x000625B4
			public void Update()
			{
				if (this.m_SoundHndl != null && !this.m_SoundHndl.IsPlaying(false))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHndl, true);
					this.m_SoundHndl = null;
				}
				if (this.m_RequestPlay == 1)
				{
					this.m_Timer += Time.deltaTime;
					if (this.m_Timer >= this.m_Delay)
					{
						this.m_RequestPlay = 2;
						this.m_SoundHndl = new SoundHandler();
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_CueID, this.m_SoundHndl, 1f, 0, -1, -1);
					}
				}
			}

			// Token: 0x04001956 RID: 6486
			public eSoundSeListDB m_CueID;

			// Token: 0x04001957 RID: 6487
			public float m_Delay;

			// Token: 0x04001958 RID: 6488
			public float m_Timer;

			// Token: 0x04001959 RID: 6489
			public int m_RequestPlay;

			// Token: 0x0400195A RID: 6490
			public SoundHandler m_SoundHndl;
		}

		// Token: 0x020003D5 RID: 981
		private enum eGachaSound
		{
			// Token: 0x0400195C RID: 6492
			PLAY_START,
			// Token: 0x0400195D RID: 6493
			PLAY_START_RIPPLE,
			// Token: 0x0400195E RID: 6494
			BRANCH_LV1,
			// Token: 0x0400195F RID: 6495
			FINISH_LV1,
			// Token: 0x04001960 RID: 6496
			BRANCH_LV2,
			// Token: 0x04001961 RID: 6497
			FINISH_LV2,
			// Token: 0x04001962 RID: 6498
			BRANCH_LV3,
			// Token: 0x04001963 RID: 6499
			BRANCH_LOOP_LV3,
			// Token: 0x04001964 RID: 6500
			APPEAR_CUTIN_PREV,
			// Token: 0x04001965 RID: 6501
			APPEAR_CUTIN,
			// Token: 0x04001966 RID: 6502
			FINISH_LV3,
			// Token: 0x04001967 RID: 6503
			FINISH_LOOP,
			// Token: 0x04001968 RID: 6504
			Num
		}

		// Token: 0x020003D6 RID: 982
		private enum eMainStep
		{
			// Token: 0x0400196A RID: 6506
			None = -1,
			// Token: 0x0400196B RID: 6507
			First,
			// Token: 0x0400196C RID: 6508
			LoadCharaIllust,
			// Token: 0x0400196D RID: 6509
			LoadCharaIllust_Wait,
			// Token: 0x0400196E RID: 6510
			FirstFadeIn_Wait,
			// Token: 0x0400196F RID: 6511
			TapToStart,
			// Token: 0x04001970 RID: 6512
			PlayStart,
			// Token: 0x04001971 RID: 6513
			PlayStart_Wait,
			// Token: 0x04001972 RID: 6514
			PlayBranch_Wait,
			// Token: 0x04001973 RID: 6515
			PlayBranch_lv3_Wait_0,
			// Token: 0x04001974 RID: 6516
			PlayBranch_lv3_Wait_1,
			// Token: 0x04001975 RID: 6517
			PlayBranch_lv3_Wait_2,
			// Token: 0x04001976 RID: 6518
			PlayBranch_lv3_Wait_3,
			// Token: 0x04001977 RID: 6519
			PlayBranch_lv3_Wait_4,
			// Token: 0x04001978 RID: 6520
			PlayLoop_Wait,
			// Token: 0x04001979 RID: 6521
			CurrentEnd,
			// Token: 0x0400197A RID: 6522
			CurrentEnd_Wait_0,
			// Token: 0x0400197B RID: 6523
			CurrentEnd_Wait_1,
			// Token: 0x0400197C RID: 6524
			Flavor,
			// Token: 0x0400197D RID: 6525
			Flavor_Wait,
			// Token: 0x0400197E RID: 6526
			Result,
			// Token: 0x0400197F RID: 6527
			Result_Wait,
			// Token: 0x04001980 RID: 6528
			RePlayTransit_0,
			// Token: 0x04001981 RID: 6529
			RePlayTransit_1,
			// Token: 0x04001982 RID: 6530
			RePlayTransit_2,
			// Token: 0x04001983 RID: 6531
			AllSkip_0,
			// Token: 0x04001984 RID: 6532
			AllSkip_1,
			// Token: 0x04001985 RID: 6533
			AllSkip_2,
			// Token: 0x04001986 RID: 6534
			SingleSkip_0,
			// Token: 0x04001987 RID: 6535
			SingleSkip_1,
			// Token: 0x04001988 RID: 6536
			SingleSkip_2,
			// Token: 0x04001989 RID: 6537
			End
		}

		// Token: 0x020003D7 RID: 983
		private enum ePrepareStep
		{
			// Token: 0x0400198B RID: 6539
			None = -1,
			// Token: 0x0400198C RID: 6540
			First,
			// Token: 0x0400198D RID: 6541
			Prepare,
			// Token: 0x0400198E RID: 6542
			Prepare_Wait,
			// Token: 0x0400198F RID: 6543
			UIPrepare,
			// Token: 0x04001990 RID: 6544
			UIPrepare_Wait,
			// Token: 0x04001991 RID: 6545
			Setup,
			// Token: 0x04001992 RID: 6546
			End,
			// Token: 0x04001993 RID: 6547
			PrepareError,
			// Token: 0x04001994 RID: 6548
			PrepareError_Wait
		}
	}
}
