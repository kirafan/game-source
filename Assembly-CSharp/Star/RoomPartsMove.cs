﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061E RID: 1566
	public class RoomPartsMove : IRoomPartsAction
	{
		// Token: 0x06001E90 RID: 7824 RVA: 0x000A56D7 File Offset: 0x000A3AD7
		public RoomPartsMove(Transform ptrs, Vector3 fbasepos, Vector3 ftargetpos, float ftime, RoomPartsMove.ePathType ftype = RoomPartsMove.ePathType.Linear)
		{
			this.m_LineType = ftype;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BasePos = fbasepos;
			this.m_TargetPos = ftargetpos;
			this.m_Target = ptrs;
			this.m_Active = true;
		}

		// Token: 0x06001E91 RID: 7825 RVA: 0x000A5718 File Offset: 0x000A3B18
		public RoomPartsMove(Transform ptrs, Vector3 fbasepos, Vector3 fpoint1, Vector3 fpoint2, Vector3 ftargetpos, float ftime)
		{
			this.m_LineType = RoomPartsMove.ePathType.Bez;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BasePos = fbasepos;
			this.m_TargetPos = ftargetpos;
			this.m_Point1 = fpoint1;
			this.m_Point2 = fpoint2;
			this.m_Target = ptrs;
			this.m_Active = true;
		}

		// Token: 0x06001E92 RID: 7826 RVA: 0x000A5774 File Offset: 0x000A3B74
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			RoomPartsMove.ePathType lineType = this.m_LineType;
			if (lineType != RoomPartsMove.ePathType.Linear)
			{
				if (lineType != RoomPartsMove.ePathType.Bez)
				{
					if (lineType == RoomPartsMove.ePathType.LocalLinear)
					{
						if (this.m_Time >= this.m_MaxTime)
						{
							this.m_Target.localPosition = this.m_TargetPos;
							this.m_Active = false;
						}
						else
						{
							float num = this.m_Time / this.m_MaxTime;
							this.m_Target.localPosition = (this.m_TargetPos - this.m_BasePos) * num + this.m_BasePos;
						}
					}
				}
				else if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Target.position = this.m_TargetPos;
					this.m_Active = false;
				}
				else
				{
					float num = this.m_Time / this.m_MaxTime;
					float num2 = 1f - num;
					float num3 = num2 * num2;
					float num4 = num * num;
					float d = num2 * num3;
					float d2 = num * num4;
					num3 = num3 * num * 3f;
					num4 = num4 * num2 * 3f;
					this.m_Target.position = this.m_BasePos * d + this.m_Point1 * num3 + this.m_Point2 * num4 + this.m_TargetPos * d2;
				}
			}
			else if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Target.position = this.m_TargetPos;
				this.m_Active = false;
			}
			else
			{
				float num = this.m_Time / this.m_MaxTime;
				this.m_Target.position = (this.m_TargetPos - this.m_BasePos) * num + this.m_BasePos;
			}
			return this.m_Active;
		}

		// Token: 0x04002539 RID: 9529
		private Transform m_Target;

		// Token: 0x0400253A RID: 9530
		private Vector3 m_BasePos;

		// Token: 0x0400253B RID: 9531
		private Vector3 m_TargetPos;

		// Token: 0x0400253C RID: 9532
		private Vector3 m_Point1;

		// Token: 0x0400253D RID: 9533
		private Vector3 m_Point2;

		// Token: 0x0400253E RID: 9534
		private float m_Time;

		// Token: 0x0400253F RID: 9535
		private float m_MaxTime;

		// Token: 0x04002540 RID: 9536
		private RoomPartsMove.ePathType m_LineType;

		// Token: 0x0200061F RID: 1567
		public enum ePathType
		{
			// Token: 0x04002542 RID: 9538
			Linear,
			// Token: 0x04002543 RID: 9539
			Bez,
			// Token: 0x04002544 RID: 9540
			LocalLinear
		}
	}
}
