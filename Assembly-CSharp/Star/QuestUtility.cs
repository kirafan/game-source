﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x0200052C RID: 1324
	public static class QuestUtility
	{
		// Token: 0x06001A7D RID: 6781 RVA: 0x0008B3E0 File Offset: 0x000897E0
		public static TimeSpan CalcTimeSpan(QuestManager.EventData eventData)
		{
			return eventData.m_EndAt - SingletonMonoBehaviour<GameSystem>.Inst.ServerTime;
		}

		// Token: 0x06001A7E RID: 6782 RVA: 0x0008B404 File Offset: 0x00089804
		public static TimeSpan CalcTimeSpan(QuestManager.EventGroupData groupData)
		{
			TimeSpan timeSpan = TimeSpan.Zero;
			for (int i = 0; i < groupData.m_EventDatas.Count; i++)
			{
				TimeSpan timeSpan2 = QuestUtility.CalcTimeSpan(groupData.m_EventDatas[i]);
				if (timeSpan2 > timeSpan)
				{
					timeSpan = timeSpan2;
				}
			}
			return timeSpan;
		}

		// Token: 0x06001A7F RID: 6783 RVA: 0x0008B454 File Offset: 0x00089854
		public static QuestDefine.eQuestLogAddResult CheckQuestLogAdd(QuestManager.QuestData questData, QuestManager.EventData eventData, long partyMngID)
		{
			if (questData == null)
			{
				return QuestDefine.eQuestLogAddResult.AnyExConditionError;
			}
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			long value = userDataMng.UserData.Stamina.GetValue();
			if ((long)questData.m_Param.m_Stamina > value)
			{
				return QuestDefine.eQuestLogAddResult.StaminaIsShort;
			}
			if (eventData != null)
			{
				if (eventData.m_ExKeyItemID != -1)
				{
					int itemNum = userDataMng.GetItemNum(eventData.m_ExKeyItemID);
					if (itemNum < eventData.m_ExKeyItemNum)
					{
						return QuestDefine.eQuestLogAddResult.KeyItemIsShort;
					}
				}
				if (!QuestUtility.CheckExCondition(eventData, partyMngID))
				{
					return QuestDefine.eQuestLogAddResult.AnyExConditionError;
				}
			}
			return QuestDefine.eQuestLogAddResult.Success;
		}

		// Token: 0x06001A80 RID: 6784 RVA: 0x0008B4D8 File Offset: 0x000898D8
		public static bool CheckExCondition(QuestManager.EventData eventData, long partyMngID)
		{
			if (eventData == null)
			{
				return false;
			}
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			if (eventData.m_ExKeyItemID != -1)
			{
				int itemNum = userDataMng.GetItemNum(eventData.m_ExKeyItemID);
				if (itemNum <= 0)
				{
					return false;
				}
			}
			if (eventData.m_ExItemIDs != null)
			{
				for (int i = 0; i < eventData.m_ExItemIDs.Length; i++)
				{
					int itemNum2 = userDataMng.GetItemNum(eventData.m_ExItemIDs[i]);
					if (itemNum2 <= 0)
					{
						return false;
					}
				}
			}
			UserBattlePartyData userBattlePartyData = userDataMng.GetUserBattlePartyData(partyMngID);
			if (userBattlePartyData == null)
			{
				return false;
			}
			List<long> availableMemberMngIDs = userBattlePartyData.GetAvailableMemberMngIDs();
			List<UserCharacterData> userCharaDatas = userDataMng.GetUserCharaDatas(availableMemberMngIDs);
			if (eventData.m_ExTitleType != eTitleType.None)
			{
				for (int j = 0; j < userCharaDatas.Count; j++)
				{
					eTitleType titleType = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(userCharaDatas[j].Param.NamedType);
					if (titleType != eventData.m_ExTitleType)
					{
						return false;
					}
				}
			}
			if (eventData.m_ExNamedType != eCharaNamedType.None)
			{
				for (int k = 0; k < userCharaDatas.Count; k++)
				{
					if (userCharaDatas[k].Param.NamedType != eventData.m_ExNamedType)
					{
						return false;
					}
				}
			}
			if (eventData.m_ExRare != eRare.None)
			{
				for (int l = 0; l < userCharaDatas.Count; l++)
				{
					if (userCharaDatas[l].Param.RareType != eventData.m_ExRare)
					{
						return false;
					}
				}
			}
			if (eventData.m_ExCost > 0)
			{
				int num = EditUtility.CalcPartyTotalCost(userBattlePartyData.MngID);
				if (num > eventData.m_ExCost)
				{
					return false;
				}
			}
			if (eventData.m_ExElements != null)
			{
				for (int m = 0; m < userCharaDatas.Count; m++)
				{
					bool flag = false;
					for (int n = 0; n < eventData.m_ExElements.Length; n++)
					{
						if (eventData.m_ExElements[n] && userCharaDatas[m].Param.ElementType == (eElementType)n)
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						return false;
					}
				}
			}
			if (eventData.m_ExClasses != null)
			{
				for (int num2 = 0; num2 < userCharaDatas.Count; num2++)
				{
					bool flag2 = false;
					for (int num3 = 0; num3 < eventData.m_ExClasses.Length; num3++)
					{
						if (eventData.m_ExClasses[num3] && userCharaDatas[num2].Param.ClassType == (eClassType)num3)
						{
							flag2 = true;
							break;
						}
					}
					if (!flag2)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06001A81 RID: 6785 RVA: 0x0008B798 File Offset: 0x00089B98
		public static QuestUtility.eGroupClearRank CheckClearGroup(QuestManager.EventGroupData groupData)
		{
			QuestUtility.eGroupClearRank result = QuestUtility.eGroupClearRank.Complete;
			for (int i = 0; i < groupData.m_EventDatas.Count; i++)
			{
				QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(groupData.m_EventDatas[i].m_QuestID);
				if (questData != null)
				{
					if (questData.m_ClearRank == QuestDefine.eClearRank.None)
					{
						return QuestUtility.eGroupClearRank.None;
					}
					if (questData.m_ClearRank < QuestDefine.eClearRank.StarMax)
					{
						result = QuestUtility.eGroupClearRank.Clear;
					}
				}
			}
			return result;
		}

		// Token: 0x0200052D RID: 1325
		public enum eGroupClearRank
		{
			// Token: 0x04002100 RID: 8448
			None,
			// Token: 0x04002101 RID: 8449
			Clear,
			// Token: 0x04002102 RID: 8450
			Complete
		}
	}
}
