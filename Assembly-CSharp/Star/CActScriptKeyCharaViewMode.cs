﻿using System;

namespace Star
{
	// Token: 0x02000558 RID: 1368
	public class CActScriptKeyCharaViewMode : IRoomScriptData
	{
		// Token: 0x06001ADF RID: 6879 RVA: 0x0008F134 File Offset: 0x0008D534
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyCharaViewMode actXlsKeyCharaViewMode = (ActXlsKeyCharaViewMode)pbase;
			this.m_TargetName = actXlsKeyCharaViewMode.m_TargetName;
			this.m_View = actXlsKeyCharaViewMode.m_View;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x040021B7 RID: 8631
		public string m_TargetName;

		// Token: 0x040021B8 RID: 8632
		public bool m_View;

		// Token: 0x040021B9 RID: 8633
		public uint m_CRCKey;
	}
}
