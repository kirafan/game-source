﻿using System;

namespace Star
{
	// Token: 0x0200031E RID: 798
	public enum eTownMoveState
	{
		// Token: 0x040016A7 RID: 5799
		None,
		// Token: 0x040016A8 RID: 5800
		TargetMove,
		// Token: 0x040016A9 RID: 5801
		NearmissMove,
		// Token: 0x040016AA RID: 5802
		NonPointMove,
		// Token: 0x040016AB RID: 5803
		SetUp
	}
}
