﻿using System;
using TownFacilityResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200068C RID: 1676
	public class TownComAPIObjbuildPointSetAll : INetComHandle
	{
		// Token: 0x060021BD RID: 8637 RVA: 0x000B3A3E File Offset: 0x000B1E3E
		public TownComAPIObjbuildPointSetAll()
		{
			this.ApiName = "player/town_facility/build_point/set_all";
			this.Request = true;
			this.ResponseType = typeof(Buildpointsetall);
		}

		// Token: 0x0400282A RID: 10282
		public TownFacilitySetState[] townFacilitySetStates;
	}
}
