﻿using System;

namespace Star
{
	// Token: 0x02000609 RID: 1545
	public struct IVector3
	{
		// Token: 0x06001E56 RID: 7766 RVA: 0x000A39AC File Offset: 0x000A1DAC
		public IVector3(int fx, int fy, int fz)
		{
			this.x = fx;
			this.y = fy;
			this.z = fz;
		}

		// Token: 0x040024E2 RID: 9442
		public int x;

		// Token: 0x040024E3 RID: 9443
		public int y;

		// Token: 0x040024E4 RID: 9444
		public int z;
	}
}
