﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002ED RID: 749
	[Serializable]
	public class EffectControllParam_Visible : EffectControllParam
	{
		// Token: 0x06000E8E RID: 3726 RVA: 0x0004E208 File Offset: 0x0004C608
		public override void Apply(int index, EffectHandler effHndl)
		{
			if (index < 0 || index >= this.m_Args.Length)
			{
				return;
			}
			EffectControllParam_Visible.Arg arg = this.m_Args[index];
			for (int i = 0; i < this.m_TargetGos.Length; i++)
			{
				bool active = false;
				for (int j = 0; j < arg.m_Indices.Length; j++)
				{
					int num = arg.m_Indices[j];
					if (num == i)
					{
						active = true;
						break;
					}
				}
				this.m_TargetGos[i].SetActive(active);
			}
		}

		// Token: 0x040015AC RID: 5548
		public EffectControllParam_Visible.Arg[] m_Args;

		// Token: 0x040015AD RID: 5549
		public GameObject[] m_TargetGos;

		// Token: 0x020002EE RID: 750
		[Serializable]
		public class Arg
		{
			// Token: 0x040015AE RID: 5550
			public int[] m_Indices;
		}
	}
}
