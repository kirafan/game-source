﻿using System;
using System.Collections.Generic;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000581 RID: 1409
	public class RoomCharaManager : IRoomCharaManager
	{
		// Token: 0x06001B5F RID: 7007 RVA: 0x000908D4 File Offset: 0x0008ECD4
		public override void SetLinkManager(RoomBuilder pbuilder)
		{
			this.m_CharaTable = new RoomCharaPakage[0];
			this.m_PlayFloorID = pbuilder.GetFloorNO();
			SystemScheduleLinkManager manager = SystemScheduleLinkManager.GetManager();
			this.m_OutSubMemberRothe = manager.GetReserveSubIndex();
			this.m_InSubMemberRothe = manager.GetReserveIndex();
			this.m_SleepFloorBedNum = 5;
			base.SetLinkManager(pbuilder);
		}

		// Token: 0x06001B60 RID: 7008 RVA: 0x00090928 File Offset: 0x0008ED28
		public override void BackLinkManager()
		{
			SystemScheduleLinkManager manager = SystemScheduleLinkManager.GetManager();
			manager.SetReserveIndex(this.m_InSubMemberRothe, this.m_OutSubMemberRothe);
			base.BackLinkManager();
		}

		// Token: 0x06001B61 RID: 7009 RVA: 0x00090954 File Offset: 0x0008ED54
		public override bool IsBuild()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].IsPreparing())
				{
					return false;
				}
			}
			this.m_BuildUp = true;
			return true;
		}

		// Token: 0x06001B62 RID: 7010 RVA: 0x000909A4 File Offset: 0x0008EDA4
		private void CheckHourEventAction(RoomGreet.eCategory fevent)
		{
			SystemScheduleLinkManager manager = SystemScheduleLinkManager.GetManager();
			if (!manager.IsActionChara((int)fevent))
			{
				List<long> list = new List<long>();
				int num = this.m_CharaTable.Length;
				for (int i = 0; i < num; i++)
				{
					if (this.m_CharaTable[i] != null && this.m_CharaTable[i].IsScheduleOfRoom())
					{
						list.Add(this.m_CharaTable[i].m_ManageID);
					}
				}
				if (list.Count != 0)
				{
					int num2 = UnityEngine.Random.Range(0, list.Count - 1);
					manager.SaveActionChara(this.m_CharaTable[num2].m_ManageID, (int)fevent);
					base.CheckAction(fevent, this.m_CharaTable[num2].m_ManageID);
				}
			}
		}

		// Token: 0x06001B63 RID: 7011 RVA: 0x00090A5C File Offset: 0x0008EE5C
		private void AddRoomCharacter(long fmngid, int froomid)
		{
			RoomCharaPakage roomCharaPakage = this.CheckEntryCharaManager(fmngid, froomid);
			if (roomCharaPakage.IsScheduleOfRoom())
			{
				roomCharaPakage.SetUpInRoom(this.m_Builder.CacheTransform, true);
			}
			else
			{
				this.m_WaitUpChara += 1;
			}
		}

		// Token: 0x06001B64 RID: 7012 RVA: 0x00090AA4 File Offset: 0x0008EEA4
		private void Update()
		{
			int num = this.m_CharaTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].UpdatePakage(this.m_Builder);
				}
			}
			if (this.m_WaitUpChara != 0)
			{
				this.CheckSubMemberListUp();
			}
			if (this.m_WaitDropChara != 0)
			{
				this.CheckSubMemberDropOut();
			}
			if (this.m_BuildUp)
			{
				int hour = ScheduleTimeUtil.GetManageUnixTime().Hour;
				if (hour >= 6 && hour < 9)
				{
					this.CheckHourEventAction(RoomGreet.eCategory.Morning);
				}
				else if (hour >= 21 && hour < 24)
				{
					this.CheckHourEventAction(RoomGreet.eCategory.Night);
				}
			}
		}

		// Token: 0x06001B65 RID: 7013 RVA: 0x00090B5C File Offset: 0x0008EF5C
		public override void DeleteChara()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].Clear(this.m_Builder);
					this.m_CharaTable[i] = null;
				}
			}
		}

		// Token: 0x06001B66 RID: 7014 RVA: 0x00090BAB File Offset: 0x0008EFAB
		public override void RefreshCharaCall()
		{
			FieldMapManager.RefreshManageChara(new CharaBuildState(this.CallbackCharaState));
		}

		// Token: 0x06001B67 RID: 7015 RVA: 0x00090BC0 File Offset: 0x0008EFC0
		public override void DeleteCharacter(long fmanageid, bool fcutout = false)
		{
			bool flag = false;
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fmanageid)
				{
					this.m_CharaTable[i].Clear(this.m_Builder);
					if (this.m_CharaTable[i].IsScheduleChara())
					{
						if (fcutout)
						{
							this.m_CharaTable[i] = null;
							flag = true;
						}
						else
						{
							this.m_WaitUpChara += 1;
						}
					}
					else
					{
						this.m_CharaTable[i] = null;
						flag = true;
					}
					break;
				}
			}
			if (flag)
			{
				this.CheckMemberLimitCheck(fcutout);
				int i = 0;
				while (i < this.m_EntryNum)
				{
					if (this.m_CharaTable[i] == null)
					{
						for (int j = i + 1; j < this.m_EntryNum; j++)
						{
							this.m_CharaTable[j - 1] = this.m_CharaTable[j];
						}
						this.m_EntryNum--;
						this.m_CharaTable[this.m_EntryNum] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x06001B68 RID: 7016 RVA: 0x00090CE4 File Offset: 0x0008F0E4
		private void CheckRoomInManageChara(long fmanageid, int froomid, bool fcutout)
		{
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fmanageid)
				{
					if (this.m_CharaTable[i].m_RoomID != froomid)
					{
						if (froomid == this.m_PlayFloorID)
						{
							this.AddRoomCharacter(fmanageid, froomid);
							this.AddScheduleCharacter(fmanageid);
						}
						else
						{
							this.DeleteCharacter(fmanageid, fcutout);
						}
					}
					break;
				}
			}
		}

		// Token: 0x06001B69 RID: 7017 RVA: 0x00090D68 File Offset: 0x0008F168
		public override void ResetState()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_RoomObj != null)
				{
					this.m_CharaTable[i].m_RoomObj.SetNewGridStatus(this.m_Builder.GetGridStatus(0));
					this.m_CharaTable[i].m_RoomObj.CheckExistAnyOnCurrent(0);
				}
			}
		}

		// Token: 0x06001B6A RID: 7018 RVA: 0x00090DE8 File Offset: 0x0008F1E8
		public override void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_RoomObj != null && this.m_CharaTable[i].m_RoomObj.FloorID == 0)
				{
					this.m_CharaTable[i].m_RoomObj.CheckExistAnyOnCurrent(0);
				}
			}
		}

		// Token: 0x06001B6B RID: 7019 RVA: 0x00090E60 File Offset: 0x0008F260
		public override void SetUpRoomCharacter()
		{
			int roomInCharaNum = UserDataUtil.FieldChara.GetRoomInCharaNum();
			for (int i = 0; i < roomInCharaNum; i++)
			{
				if (UserDataUtil.FieldChara.GetRoomInCharaAt(i).RoomID == this.m_PlayFloorID)
				{
					this.AddRoomCharacter(UserDataUtil.FieldChara.GetRoomInCharaAt(i).ManageID, this.m_PlayFloorID);
				}
			}
			this.CheckSubMemberListUp();
		}

		// Token: 0x06001B6C RID: 7020 RVA: 0x00090EC8 File Offset: 0x0008F2C8
		private void CheckSubMemberListUp()
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			SystemScheduleLinkManager manager = SystemScheduleLinkManager.GetManager();
			int count = userDataMng.UserCharaDatas.Count;
			byte[] array = new byte[count];
			int num = 0;
			int num2 = 0;
			while (num < count && this.m_WaitUpChara != 0)
			{
				bool flag = false;
				for (int i = 0; i < 10; i++)
				{
					num2 = UnityEngine.Random.Range(0, count - 1);
					if (array[num2] == 0)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					while (array[num2] == 0)
					{
						num2++;
						if (num2 >= count)
						{
							num2 = 0;
						}
					}
				}
				array[num2] = 1;
				long mngID = userDataMng.UserCharaDatas[num2].MngID;
				num++;
				if (!userDataMng.UserFieldChara.IsEntryFldChara(mngID) && !this.IsRoomMateNamed(userDataMng.UserCharaDatas[num2].Param.NamedType) && this.m_WaitUpChara != 0)
				{
					RoomCharaPakage roomCharaPakage = this.CheckEntryCharaManager(mngID, -1);
					roomCharaPakage.SetUpInRoom(this.m_Builder.CacheTransform, false);
					if (!manager.IsReserveChara(mngID))
					{
						base.CheckAction(RoomGreet.eCategory.Visit, mngID);
					}
					manager.SaveReserveChara(mngID);
					this.m_WaitUpChara -= 1;
				}
			}
			this.m_WaitUpChara = 0;
		}

		// Token: 0x06001B6D RID: 7021 RVA: 0x00091020 File Offset: 0x0008F420
		private bool IsRoomMateNamed(eCharaNamedType fnametype)
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_Named == fnametype)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001B6E RID: 7022 RVA: 0x0009106C File Offset: 0x0008F46C
		public bool SetSleepFloor(long fmanageid, bool finit)
		{
			bool flag = false;
			for (int i = 0; i < this.m_SleepCharaNum; i++)
			{
				if (this.m_SleepManagerID[i] == fmanageid)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				this.m_SleepManagerID[this.m_SleepCharaNum] = fmanageid;
				this.m_SleepCharaNum++;
				return this.m_SleepCharaNum <= this.m_SleepFloorBedNum;
			}
			return false;
		}

		// Token: 0x06001B6F RID: 7023 RVA: 0x000910E0 File Offset: 0x0008F4E0
		public void ClrSleepFloor(long fmanageid, bool fsleep)
		{
			for (int i = 0; i < this.m_SleepCharaNum; i++)
			{
				if (this.m_SleepManagerID[i] == fmanageid)
				{
					for (int j = i + 1; j < this.m_SleepCharaNum; j++)
					{
						this.m_SleepManagerID[j - 1] = this.m_SleepManagerID[j];
					}
					this.m_SleepCharaNum--;
					break;
				}
			}
		}

		// Token: 0x06001B70 RID: 7024 RVA: 0x00091150 File Offset: 0x0008F550
		public void AddScheduleCharacter(long fmanageid)
		{
			this.m_WaitDropChara += 1;
		}

		// Token: 0x06001B71 RID: 7025 RVA: 0x00091164 File Offset: 0x0008F564
		private void CheckMemberLimitCheck(bool fchk)
		{
			if (fchk)
			{
				int roomInCharaNum = UserDataUtil.FieldChara.GetRoomInCharaNum();
				if (roomInCharaNum < this.m_EntryNum)
				{
					this.m_WaitDropChara = (short)(this.m_EntryNum - roomInCharaNum);
				}
			}
		}

		// Token: 0x06001B72 RID: 7026 RVA: 0x000911A0 File Offset: 0x0008F5A0
		private void CheckSubMemberDropOut()
		{
			int num = this.m_OutSubMemberRothe;
			do
			{
				num++;
				if (num >= this.m_CharaTable.Length)
				{
					num = 0;
				}
				if (this.m_CharaTable[num] != null)
				{
					RoomCharaPakage roomCharaPakage = this.m_CharaTable[num];
					if (!roomCharaPakage.IsScheduleChara() && roomCharaPakage.IsRoomIn() && this.m_WaitDropChara != 0)
					{
						roomCharaPakage.DropOutRoom();
						this.m_WaitDropChara -= 1;
					}
				}
			}
			while (num != this.m_OutSubMemberRothe && this.m_WaitDropChara != 0);
			this.m_OutSubMemberRothe = num;
			this.m_WaitDropChara = 0;
		}

		// Token: 0x06001B73 RID: 7027 RVA: 0x0009123C File Offset: 0x0008F63C
		public RoomCharaPakage CheckEntryCharaManager(long fmanageid, int froomid)
		{
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fmanageid)
				{
					return this.m_CharaTable[i];
				}
			}
			RoomCharaPakage roomCharaPakage = new RoomCharaPakage(fmanageid, froomid, this);
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] == null)
				{
					this.m_CharaTable[i] = roomCharaPakage;
					this.m_EntryNum++;
					return roomCharaPakage;
				}
			}
			this.AddRoomCharaPakage(roomCharaPakage);
			return roomCharaPakage;
		}

		// Token: 0x06001B74 RID: 7028 RVA: 0x000912D8 File Offset: 0x0008F6D8
		public void AddRoomCharaPakage(RoomCharaPakage pakage)
		{
			if (this.m_EntryNum >= this.m_EntryMax)
			{
				RoomCharaPakage[] array = new RoomCharaPakage[this.m_EntryMax + 5];
				long[] array2 = new long[this.m_EntryMax + 5];
				for (int i = 0; i < this.m_EntryMax; i++)
				{
					array[i] = this.m_CharaTable[i];
					array2[i] = this.m_SleepManagerID[i];
				}
				this.m_CharaTable = null;
				this.m_CharaTable = array;
				this.m_SleepManagerID = null;
				this.m_SleepManagerID = array2;
				this.m_EntryMax += 5;
			}
			this.m_CharaTable[this.m_EntryNum] = pakage;
			this.m_EntryNum++;
		}

		// Token: 0x06001B75 RID: 7029 RVA: 0x00091388 File Offset: 0x0008F788
		public void CallbackCharaState(eCharaBuildUp ftype, long fmanageid, int froomid)
		{
			if (ftype != eCharaBuildUp.Add)
			{
				if (ftype != eCharaBuildUp.Del)
				{
					if (ftype == eCharaBuildUp.ChangeRoom)
					{
						this.CheckRoomInManageChara(fmanageid, froomid, true);
					}
				}
				else
				{
					this.DeleteCharacter(fmanageid, true);
				}
			}
			else if (froomid == this.m_PlayFloorID)
			{
				int roomInCharaNum = UserDataUtil.FieldChara.GetRoomInCharaNum();
				for (int i = 0; i < roomInCharaNum; i++)
				{
					if (UserDataUtil.FieldChara.GetRoomInCharaAt(i).ManageID == fmanageid)
					{
						this.AddRoomCharacter(fmanageid, froomid);
						this.AddScheduleCharacter(fmanageid);
						break;
					}
				}
			}
		}

		// Token: 0x06001B76 RID: 7030 RVA: 0x00091424 File Offset: 0x0008F824
		public RoomCharaPakage GetCharaSetUp(int fhandle)
		{
			RoomCharaPakage result = null;
			int num = this.m_CharaTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_AccessKey == fhandle)
				{
					result = this.m_CharaTable[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06001B77 RID: 7031 RVA: 0x0009147D File Offset: 0x0008F87D
		public int GetCharaPakageMax()
		{
			return this.m_CharaTable.Length;
		}

		// Token: 0x06001B78 RID: 7032 RVA: 0x00091487 File Offset: 0x0008F887
		public RoomCharaPakage GetCharaPakage(int findex)
		{
			return this.m_CharaTable[findex];
		}

		// Token: 0x0400224E RID: 8782
		[SerializeField]
		public RoomCharaPakage[] m_CharaTable;

		// Token: 0x0400224F RID: 8783
		public int m_EntryNum;

		// Token: 0x04002250 RID: 8784
		public int m_EntryMax;

		// Token: 0x04002251 RID: 8785
		public int m_InSubMemberRothe;

		// Token: 0x04002252 RID: 8786
		public int m_OutSubMemberRothe;

		// Token: 0x04002253 RID: 8787
		private int m_MakeUID;

		// Token: 0x04002254 RID: 8788
		private short m_WaitUpChara;

		// Token: 0x04002255 RID: 8789
		private short m_WaitDropChara;

		// Token: 0x04002256 RID: 8790
		private long m_StepTime;

		// Token: 0x04002257 RID: 8791
		private long[] m_SleepManagerID;

		// Token: 0x04002258 RID: 8792
		private int m_SleepCharaNum;

		// Token: 0x04002259 RID: 8793
		private int m_SleepFloorBedNum;

		// Token: 0x0400225A RID: 8794
		private int m_PlayFloorID;

		// Token: 0x0400225B RID: 8795
		private bool m_BuildUp;
	}
}
