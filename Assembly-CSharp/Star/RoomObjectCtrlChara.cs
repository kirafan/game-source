﻿using System;
using Meige;
using Star.UI.Room;
using UnityEngine;
using UnityEngine.Rendering;

namespace Star
{
	// Token: 0x020005D3 RID: 1491
	public class RoomObjectCtrlChara : IRoomObjectControll
	{
		// Token: 0x06001CFC RID: 7420 RVA: 0x0009BC72 File Offset: 0x0009A072
		public CharacterHandler GetHandle()
		{
			return this.m_BaseHandle;
		}

		// Token: 0x06001CFD RID: 7421 RVA: 0x0009BC7A File Offset: 0x0009A07A
		public RoomAnimAccessMap GetAnimeMap()
		{
			return this.m_AnimeMap;
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x0009BC82 File Offset: 0x0009A082
		public RoomCharaHud GetUIHud()
		{
			return this.m_RoomCharaHud;
		}

		// Token: 0x06001CFF RID: 7423 RVA: 0x0009BC8A File Offset: 0x0009A08A
		public Vector2 GetGridPos()
		{
			return this.m_BlockData.Pos;
		}

		// Token: 0x06001D00 RID: 7424 RVA: 0x0009BC97 File Offset: 0x0009A097
		public AStar GetAStar()
		{
			return this.m_AStar;
		}

		// Token: 0x06001D01 RID: 7425 RVA: 0x0009BC9F File Offset: 0x0009A09F
		public void SetOffsetZ(float foffset)
		{
			this.m_DefaultConfig.m_OffsetZ = foffset;
		}

		// Token: 0x06001D02 RID: 7426 RVA: 0x0009BCB0 File Offset: 0x0009A0B0
		public override IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			IRoomObjectControll.ModelRenderConfig[] array = new IRoomObjectControll.ModelRenderConfig[1];
			array[0].m_RenderSort = this.m_DefaultConfig.m_RenderSort;
			array[0].m_OffsetZ = this.m_DefaultConfig.m_OffsetZ;
			array[0].m_RenderObject = this.m_DefaultConfig.m_RenderObject;
			return array;
		}

		// Token: 0x06001D03 RID: 7427 RVA: 0x0009BD0A File Offset: 0x0009A10A
		public override void SetSortKey(string ptargetname, int fsortkey)
		{
			this.m_DefaultConfig.m_RenderSort = (short)fsortkey;
		}

		// Token: 0x06001D04 RID: 7428 RVA: 0x0009BD1C File Offset: 0x0009A11C
		public override void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
			this.m_DefaultConfig.m_RenderSort = psort[0].m_RenderSort;
			this.m_DefaultConfig.m_OffsetZ = psort[0].m_OffsetZ;
			this.m_DefaultConfig.m_RenderObject = psort[0].m_RenderObject;
		}

		// Token: 0x06001D05 RID: 7429 RVA: 0x0009BD70 File Offset: 0x0009A170
		public int GetEntryPoint()
		{
			UserFieldCharaData.RoomInCharaData roomInChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInChara(this.m_ManageID);
			return roomInChara.LiveIdx;
		}

		// Token: 0x06001D06 RID: 7430 RVA: 0x0009BD9E File Offset: 0x0009A19E
		public void LinkManager(RoomBuilder builder, IRoomCharaManager pmanager)
		{
			this.m_Builder = builder;
			this.m_CharaManager = pmanager;
		}

		// Token: 0x06001D07 RID: 7431 RVA: 0x0009BDB0 File Offset: 0x0009A1B0
		public override void SetUpObjectKey(GameObject hbase)
		{
			this.m_LinkObj = null;
			this.m_DefaultSizeX = (this.m_DefaultSizeY = 1);
			this.m_ModelNode = hbase;
			this.m_BaseHandle = hbase.GetComponent<CharacterHandler>();
			this.m_BaseAnime = this.m_BaseHandle.CharaAnim;
			this.m_Dir = this.m_BaseAnime.Dir;
			this.m_Shadow = new RoomShadowControll();
			this.m_ShadowUp = false;
			this.m_CharaID = this.m_BaseHandle.CharaID;
			this.ShadowModelUp(this.m_CharaManager.GetShadowResource());
			Renderer[] componentsInChildren = hbase.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].receiveShadows = false;
				componentsInChildren[i].shadowCastingMode = ShadowCastingMode.Off;
			}
			Collider[] componentsInChildren2 = hbase.GetComponentsInChildren<Collider>();
			for (int i = 0; i < componentsInChildren2.Length; i++)
			{
				UnityEngine.Object.Destroy(componentsInChildren2[i]);
			}
			Vector2 blockSize = RoomUtility.GetBlockSize(2.5f, 2.5f);
			blockSize = RoomUtility.GetBlockSize(1f, 0f);
			Transform transform = RoomUtility.FindTransform(this.m_ModelNode.transform, "Hips", 6);
			if (transform != null)
			{
				this.SetCharaCollider(transform.gameObject, blockSize.x * 0.5f, blockSize.y * 4f, blockSize.y * 0.5f, 12, true);
			}
			else
			{
				this.SetCharaCollider(hbase, blockSize.x * 0.5f, blockSize.y * 4f, blockSize.y * 2f, 12, false);
			}
			this.m_UpDirFunc = new IRoomObjectControll.UpdateDirFunc(this.UpCharaSetDir);
			this.m_BaseHandle.transform.localScale = Vector3.one * 1.31f;
		}

		// Token: 0x06001D08 RID: 7432 RVA: 0x0009BF74 File Offset: 0x0009A374
		public void Initialize(int currentGridX, int currentGridY)
		{
			RoomGridState gridStatus = this.m_Builder.GetGridStatus(0);
			IVector3 floorSize = this.m_Builder.GetFloorSize(0);
			this.m_AStar = new AStar(floorSize.x, floorSize.y);
			this.m_AStar.SetData(gridStatus.GetGridMap());
			this.m_GridState = gridStatus;
			this.m_BlockData.SetPos((float)currentGridX, (float)currentGridY);
		}

		// Token: 0x06001D09 RID: 7433 RVA: 0x0009BFDB File Offset: 0x0009A3DB
		public void SetCurrentPos(Vector3 pos, int currentGridX, int currentGridY)
		{
			this.m_BaseHandle.CacheTransform.localPosition = pos;
			this.m_BlockData.SetPos((float)currentGridX, (float)currentGridY);
		}

		// Token: 0x06001D0A RID: 7434 RVA: 0x0009BFFD File Offset: 0x0009A3FD
		public void SetNewGridStatus(RoomGridState pGridState)
		{
			this.m_AStar.SetData(pGridState.GetGridMap());
			this.m_GridState = pGridState;
		}

		// Token: 0x06001D0B RID: 7435 RVA: 0x0009C018 File Offset: 0x0009A418
		public override void ReleaseObj()
		{
			if (this.m_Builder == null)
			{
				Debug.Log("Test");
			}
			if (this.m_RoomCharaHud != null)
			{
				UnityEngine.Object.Destroy(this.m_RoomCharaHud);
				this.m_RoomCharaHud = null;
			}
			if (this.m_Builder != null)
			{
				this.m_Builder.DetachSortObject(this);
			}
			this.m_BlockData = null;
			this.m_OwnerTrs = null;
			this.Destroy();
		}

		// Token: 0x06001D0C RID: 7436 RVA: 0x0009C094 File Offset: 0x0009A494
		public override void UpdateObj()
		{
			if (this.m_CallUpdate != null && !this.m_CallUpdate.UpdateMode(this))
			{
				this.m_CallUpdate = null;
				this.CheckNextUpdate();
				if (this.m_CallUpdate == null)
				{
					this.RequestIdleMode(1f);
				}
			}
			if (this.m_ShadowUp)
			{
				this.m_Shadow.UpdateShadow(this.m_OwnerTrs);
			}
		}

		// Token: 0x06001D0D RID: 7437 RVA: 0x0009C0FC File Offset: 0x0009A4FC
		private void ShadowModelUp(MeigeResource.Handler phandle)
		{
			if (phandle != null && phandle.IsDone)
			{
				GameObject asset = phandle.GetAsset<GameObject>();
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(asset);
				this.m_ShadowHandle = gameObject.transform;
				gameObject.transform.SetParent(this.m_OwnerTrs, false);
				gameObject.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
				gameObject.transform.localScale = new Vector3(1.4f, 1.5f, 1f);
				this.m_ShaderRender = gameObject.GetComponentInChildren<Renderer>();
				this.m_ShadowUp = true;
				this.m_ShadowPos.y = 0.015f;
				this.m_ShadowLength = 0.25f;
				if (this.m_Dir == CharacterDefine.eDir.R)
				{
					this.m_ShadowPos.x = -0.02f;
				}
				else
				{
					this.m_ShadowPos.x = 0.02f;
				}
				this.m_ShadowHandle.localPosition = this.m_ShadowPos;
				Transform pnode = RoomUtility.FindTransform(this.m_ModelNode.transform, "Hips", 6);
				this.m_Shadow.SetControllTrs(this.m_ShadowHandle, pnode, this.m_ModelNode.transform, 1.31f, gameObject.transform.localScale);
			}
		}

		// Token: 0x06001D0E RID: 7438 RVA: 0x0009C238 File Offset: 0x0009A638
		public override bool IsAction()
		{
			return false;
		}

		// Token: 0x06001D0F RID: 7439 RVA: 0x0009C23B File Offset: 0x0009A63B
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
			if (plink != null)
			{
				plink.BlockData.SetAttachObject(fattach);
				plink.SetAttachLink(!fattach);
			}
			if (fattach)
			{
				this.m_LinkObj = plink;
			}
			else
			{
				this.m_LinkObj = null;
			}
		}

		// Token: 0x06001D10 RID: 7440 RVA: 0x0009C278 File Offset: 0x0009A678
		public void SortingOrderChara(float flayerpos, int sortingOrder)
		{
			Vector3 localPosition = this.m_OwnerTrs.localPosition;
			localPosition.z = flayerpos + this.m_DefaultConfig.m_OffsetZ;
			this.m_OwnerTrs.localPosition = localPosition;
			this.m_BaseAnime.SetSortingOrder(sortingOrder + (int)this.m_DefaultConfig.m_RenderSort);
			if (this.m_ShadowUp)
			{
				this.m_ShaderRender.sortingOrder = sortingOrder - 1;
			}
		}

		// Token: 0x06001D11 RID: 7441 RVA: 0x0009C2E2 File Offset: 0x0009A6E2
		public override void LinkObjectParam(int forder, float fposz)
		{
			if (this.m_LinkObj != null)
			{
				this.m_LinkObj.SetSortingOrder(fposz, forder + 2);
			}
		}

		// Token: 0x06001D12 RID: 7442 RVA: 0x0009C309 File Offset: 0x0009A709
		public bool IsWakeUpAction()
		{
			return this.m_WakeUpActions;
		}

		// Token: 0x06001D13 RID: 7443 RVA: 0x0009C311 File Offset: 0x0009A711
		public void SetWakeUpAction(bool fset)
		{
			this.m_WakeUpActions = fset;
		}

		// Token: 0x06001D14 RID: 7444 RVA: 0x0009C31C File Offset: 0x0009A71C
		public void ChangeFloor(int floorID)
		{
			RoomGridState gridStatus = this.m_Builder.GetGridStatus(floorID);
			IVector3 floorSize = this.m_Builder.GetFloorSize(floorID);
			this.m_AStar = new AStar(floorSize.x, floorSize.y);
			this.m_AStar.SetData(gridStatus.GetGridMap());
			this.m_GridState = gridStatus;
			this.m_Builder.DetachSortObject(this);
			this.m_FloorID = floorID;
			this.m_Builder.AttachSortObject(this, (int)this.m_ManageID);
		}

		// Token: 0x06001D15 RID: 7445 RVA: 0x0009C39A File Offset: 0x0009A79A
		public void AttachHud(RoomCharaHud hud)
		{
			this.m_RoomCharaHud = hud;
			this.m_RoomCharaHud.CacheTransform.localPosition = Vector3.zero;
			this.m_RoomCharaHud.CacheTransform.SetParent(this.m_BaseHandle.CacheTransform, false);
		}

		// Token: 0x06001D16 RID: 7446 RVA: 0x0009C3D4 File Offset: 0x0009A7D4
		private void UpCharaSetDir()
		{
			this.m_BaseAnime.ChangeDir(this.m_Dir, true);
			if (this.m_Dir == CharacterDefine.eDir.R)
			{
				this.m_ShadowPos.x = -0.02f;
			}
			else
			{
				this.m_ShadowPos.x = 0.02f;
			}
			if (this.m_ShadowHandle != null)
			{
				this.m_ShadowHandle.localPosition = this.m_ShadowPos;
			}
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x0009C446 File Offset: 0x0009A846
		public void SetFace(int faceno)
		{
			if (this.m_Dir == CharacterDefine.eDir.R)
			{
				faceno += 41;
			}
			this.m_BaseAnime.SetFacial(faceno);
		}

		// Token: 0x06001D18 RID: 7448 RVA: 0x0009C466 File Offset: 0x0009A866
		public void PlayMotion(RoomDefine.eAnim ftype, WrapMode fmode)
		{
			this.m_BaseAnime.PlayAnim(this.m_AnimeMap.GetAnimeKey(ftype, this.m_Dir), fmode, 0f, 0f);
		}

		// Token: 0x06001D19 RID: 7449 RVA: 0x0009C490 File Offset: 0x0009A890
		public bool IsPlayingMotion(RoomDefine.eAnim ftype)
		{
			return this.m_BaseAnime.IsPlayingAnim(this.m_AnimeMap.GetAnimeKey(ftype, this.m_Dir));
		}

		// Token: 0x06001D1A RID: 7450 RVA: 0x0009C4AF File Offset: 0x0009A8AF
		public void SetBlink(bool fblink)
		{
			this.m_BaseAnime.SetBlink(fblink);
		}

		// Token: 0x06001D1B RID: 7451 RVA: 0x0009C4BD File Offset: 0x0009A8BD
		public void SetObjectLinkCut()
		{
		}

		// Token: 0x06001D1C RID: 7452 RVA: 0x0009C4BF File Offset: 0x0009A8BF
		public void ReleaseEntry()
		{
			this.m_Builder.DestroyObjectQue(this);
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x0009C4D0 File Offset: 0x0009A8D0
		public void SetCharaCollider(GameObject pobj, float fradius, float fhi, float foffset, int fsel, bool frot)
		{
			fhi -= fradius * 2f;
			if (fhi < 0f)
			{
				fhi = 0f;
			}
			fhi /= 2f;
			pobj.layer = LayerMask.NameToLayer("Character");
			MeshCollider meshCollider = pobj.AddComponent<MeshCollider>();
			Mesh mesh = new Mesh();
			int[] array = new int[(fsel - 2) * 3 + 6];
			if (frot)
			{
				array[0] = 0;
				array[2] = 1;
				array[1] = fsel + 1;
				int i;
				for (i = 0; i < (fsel - 2) / 2; i++)
				{
					array[i * 6 + 1 + 3] = i + 1;
					array[i * 6 + 3] = i + 2;
					array[i * 6 + 2 + 3] = fsel + 1 - i;
					array[i * 6 + 4 + 3] = i + 2;
					array[i * 6 + 3 + 3] = fsel - i;
					array[i * 6 + 5 + 3] = fsel + 1 - i;
				}
				array[i * 6 + 3] = i + 2;
				array[i * 6 + 1 + 3] = i + 1;
				array[i * 6 + 2 + 3] = i + 3;
			}
			else
			{
				array[0] = 0;
				array[1] = 1;
				array[2] = fsel + 1;
				int i;
				for (i = 0; i < (fsel - 2) / 2; i++)
				{
					array[i * 6 + 3] = i + 1;
					array[i * 6 + 1 + 3] = i + 2;
					array[i * 6 + 2 + 3] = fsel + 1 - i;
					array[i * 6 + 3 + 3] = i + 2;
					array[i * 6 + 4 + 3] = fsel - i;
					array[i * 6 + 5 + 3] = fsel + 1 - i;
				}
				array[i * 6 + 3] = i + 1;
				array[i * 6 + 1 + 3] = i + 2;
				array[i * 6 + 2 + 3] = i + 3;
			}
			Vector3[] array2 = new Vector3[fsel + 2];
			float num = 0f;
			array2[0] = new Vector3(0f, fhi + foffset + fradius, 0f);
			for (int i = 1; i < fsel / 4 + 1; i++)
			{
				num += 6.2831855f / (float)fsel;
				array2[i] = new Vector3(Mathf.Sin(num) * fradius, Mathf.Cos(num) * fradius + fhi + foffset, 0f);
				array2[fsel + 2 - i] = new Vector3(-Mathf.Sin(num) * fradius, Mathf.Cos(num) * fradius + fhi + foffset, 0f);
			}
			for (int i = fsel / 4 + 1; i < fsel / 2 + 1; i++)
			{
				array2[i] = new Vector3(Mathf.Sin(num) * fradius, Mathf.Cos(num) * fradius - fhi + foffset, 0f);
				array2[fsel + 2 - i] = new Vector3(-Mathf.Sin(num) * fradius, Mathf.Cos(num) * fradius - fhi + foffset, 0f);
				num += 6.2831855f / (float)fsel;
			}
			array2[fsel / 2 + 1] = new Vector3(0f, -fhi + foffset - fradius, 0f);
			mesh.vertices = array2;
			mesh.triangles = array;
			mesh.RecalculateBounds();
			meshCollider.sharedMesh = mesh;
			RoomHitConnect roomHitConnect = meshCollider.gameObject.AddComponent<RoomHitConnect>();
			roomHitConnect.m_Link = this;
			roomHitConnect.enabled = false;
		}

		// Token: 0x06001D1E RID: 7454 RVA: 0x0009C83C File Offset: 0x0009AC3C
		public bool IsIrqRequest()
		{
			return this.m_IrqReq.IsIrqRequest();
		}

		// Token: 0x06001D1F RID: 7455 RVA: 0x0009C849 File Offset: 0x0009AC49
		public void CancelEventCommand()
		{
			this.m_IrqReq.CancelEventCommand();
		}

		// Token: 0x06001D20 RID: 7456 RVA: 0x0009C856 File Offset: 0x0009AC56
		public void RequestDebugCommand(RoomActionCommand pcmd)
		{
			this.m_IrqReq.RequestDebugCommand(pcmd);
		}

		// Token: 0x06001D21 RID: 7457 RVA: 0x0009C864 File Offset: 0x0009AC64
		public void RequestIrqCommand(RoomActionCommand pcmd)
		{
			this.m_IrqReq.RequestIrqCommand(pcmd);
		}

		// Token: 0x06001D22 RID: 7458 RVA: 0x0009C874 File Offset: 0x0009AC74
		public void CharaDelete()
		{
			this.m_Builder.DetachSortObject(this);
			if (this.m_BaseHandle != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_BaseHandle);
			}
			this.m_CharaManager.DeleteCharacter(this.m_ManageID, false);
			this.m_Active = false;
		}

		// Token: 0x06001D23 RID: 7459 RVA: 0x0009C8CC File Offset: 0x0009ACCC
		public override void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
			this.m_BlockData.SetPos((float)fgridx, (float)fgridy);
			Vector2 floorPos = this.m_Builder.GetFloorPos(this.m_FloorID);
			Vector2 vector = RoomUtility.GridToTilePos((float)fgridx, (float)fgridy);
			if (this.m_OwnerTrs != null)
			{
				this.m_OwnerTrs.localPosition = new Vector3(vector.x + floorPos.x, vector.y + floorPos.y, 0f);
			}
		}

		// Token: 0x06001D24 RID: 7460 RVA: 0x0009C948 File Offset: 0x0009AD48
		protected void SetPosition(Vector2 fpos)
		{
			Vector3 localPosition = RoomUtility.GridToTilePos(fpos.x, fpos.y);
			this.m_BaseHandle.CacheTransform.localPosition = localPosition;
			this.m_BlockData.SetPos(fpos.x, fpos.y);
		}

		// Token: 0x06001D25 RID: 7461 RVA: 0x0009C998 File Offset: 0x0009AD98
		public void PushAction()
		{
		}

		// Token: 0x06001D26 RID: 7462 RVA: 0x0009C99A File Offset: 0x0009AD9A
		public void PopAction()
		{
		}

		// Token: 0x06001D27 RID: 7463 RVA: 0x0009C99C File Offset: 0x0009AD9C
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			bool result = true;
			MissionManager.UnlockAction(eXlsMissionSeg.Room, eXlsMissionRoomFuncType.CharaTouch);
			if (this.m_DebugKey)
			{
				if (base.FloorID == 0 && this.m_Mode != RoomCharaActMode.eMode.ObjEvent)
				{
					switch (fstep)
					{
					case eTouchStep.Begin:
						this.m_TouchStepUp = true;
						this.m_TouchToProfile = false;
						this.m_TouchTime = 0f;
						this.m_Builder.SetCameraToDebugMode(true);
						this.m_TouchOffset = Camera.main.WorldToScreenPoint(base.transform.position);
						this.m_TouchOffset = fgrid.m_TouchStart - this.m_TouchOffset;
						break;
					case eTouchStep.Drag:
						if (this.m_TouchStepUp)
						{
							Vector2 ftouchpos = fgrid.m_NowTouch - this.m_TouchOffset;
							IVector3 vector = new IVector3(0, 0, 0);
							if (this.m_Builder.GetFloorPoint(ref vector, ftouchpos, base.transform.position, 0, RoomBuilder.eEditObject.Floor))
							{
								this.SetGridPosition(vector.x, vector.y, 0);
							}
							this.RequestIdleMode(1f);
						}
						else
						{
							this.m_TouchStepUp = true;
							this.m_TouchTime = 0f;
							this.m_Builder.SetCameraToDebugMode(true);
							this.m_TouchOffset = Camera.main.WorldToScreenPoint(base.transform.position);
							this.m_TouchOffset = fgrid.m_TouchStart - this.m_TouchOffset;
						}
						break;
					case eTouchStep.End:
						if (this.m_TouchStepUp)
						{
						}
						this.m_TouchStepUp = false;
						this.m_Builder.SetCameraToDebugMode(false);
						break;
					case eTouchStep.Cancel:
						result = false;
						this.m_Builder.SetCameraToDebugMode(false);
						break;
					case eTouchStep.RayCheck:
						if (this.m_TouchStepUp)
						{
							Vector2 ftouchpos2 = fgrid.m_NowTouch - this.m_TouchOffset;
							IVector3 vector2 = new IVector3(0, 0, 0);
							if (this.m_Builder.GetFloorPoint(ref vector2, ftouchpos2, base.transform.position, 0, RoomBuilder.eEditObject.Floor))
							{
								this.SetGridPosition(vector2.x, vector2.y, 0);
							}
							this.RequestIdleMode(1f);
						}
						else
						{
							this.m_TouchStepUp = true;
							this.m_TouchTime = 0f;
							this.RequestIdleMode(1f);
							this.m_Builder.SetCameraToDebugMode(true);
							this.m_TouchOffset = Camera.main.WorldToScreenPoint(base.transform.position);
							this.m_TouchOffset = fgrid.m_TouchStart - this.m_TouchOffset;
						}
						result = false;
						break;
					}
				}
			}
			else
			{
				switch (fstep)
				{
				case eTouchStep.Begin:
					this.m_TouchStepUp = true;
					this.m_TouchToProfile = false;
					this.m_TouchTime = 0f;
					this.PushAction();
					break;
				case eTouchStep.Drag:
					if (this.m_TouchStepUp)
					{
						this.m_TouchTime += Time.deltaTime;
						if (this.m_TouchTime >= 0.5f)
						{
							if (!this.m_TouchToProfile)
							{
								RoomComUI roomComUI = new RoomComUI();
								if (this.m_ManageID != -1L)
								{
									roomComUI.m_OpenUI = eRoomUICategory.CharaTouch;
								}
								else
								{
									roomComUI.m_OpenUI = eRoomUICategory.CharaView;
								}
								roomComUI.m_ManageID = this.m_ManageID;
								roomComUI.m_CharaID = this.m_CharaID;
								roomComUI.m_SelectHandle = this;
								this.m_Builder.SendActionToMain(roomComUI);
							}
							this.m_TouchToProfile = true;
							this.PopAction();
						}
					}
					else
					{
						this.m_TouchStepUp = true;
						this.m_TouchToProfile = false;
						this.m_TouchTime = 0f;
						this.PushAction();
					}
					break;
				case eTouchStep.End:
					if (this.m_TouchStepUp)
					{
						if (base.FloorID == 0)
						{
							if (!this.m_TouchToProfile)
							{
								this.RequestIrqCommand(new RoomActionCommand
								{
									m_Command = RoomActionCommand.eActionCode.Action,
									m_ParamI1 = 2
								});
							}
						}
						else
						{
							this.SetWakeUpAction(true);
						}
						this.m_TouchStepUp = false;
					}
					this.m_TouchToProfile = false;
					this.PopAction();
					break;
				case eTouchStep.Cancel:
					this.PopAction();
					this.m_TouchToProfile = false;
					this.m_TouchStepUp = false;
					break;
				}
			}
			return result;
		}

		// Token: 0x06001D28 RID: 7464 RVA: 0x0009CDB3 File Offset: 0x0009B1B3
		public bool IsSetUpModel()
		{
			return this.m_OwnerTrs != null;
		}

		// Token: 0x06001D29 RID: 7465 RVA: 0x0009CDC1 File Offset: 0x0009B1C1
		public override void SetBaseColor(Color fcolor)
		{
			this.m_BaseColor = fcolor;
			if (this.m_BaseAnime != null)
			{
				this.m_BaseAnime.ChangeMeshColor(this.m_BaseColor, this.m_BaseColor, 0f);
			}
		}

		// Token: 0x06001D2A RID: 7466 RVA: 0x0009CDF1 File Offset: 0x0009B1F1
		private new void Awake()
		{
			base.Awake();
			this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingOrderChara);
			this.m_Category = eRoomObjectCategory.Num;
			this.m_IrqReq = new RoomCharaEventReq();
		}

		// Token: 0x06001D2B RID: 7467 RVA: 0x0009CE1E File Offset: 0x0009B21E
		protected void InitChara()
		{
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x0009CE20 File Offset: 0x0009B220
		public RoomCharaActMode.eMode GetActMode()
		{
			return this.m_Mode;
		}

		// Token: 0x06001D2D RID: 7469 RVA: 0x0009CE28 File Offset: 0x0009B228
		public void Setup(CharacterHandler charaHndl)
		{
			this.m_BaseHandle = charaHndl;
			this.m_IdleAct = new RoomCharaActIdle();
			this.m_MoveAct = new RoomCharaActMove();
			this.m_ObjAct = new RoomCharaActObject();
			this.m_RejoiceAct = new RoomCharaActRejoice();
			this.m_EnjoyAct = new RoomCharaActEnjoy();
			this.m_TweetAct = new RoomCharaActTweet();
			this.m_RoomOutAct = new RoomCharaActRoomOut();
			this.m_SleepAct = new RoomCharaActSleep();
			this.ChangeMode(RoomCharaActMode.eMode.None, this.m_IdleAct);
			this.m_BlockData = new RoomBlockData();
			this.m_BlockData.SetLinkObject(this);
			this.SetUpObjectKey(charaHndl.gameObject);
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_AnimeMap = RoomAnimePlay.CreateAnimeMap(dbMng.CharaListDB.GetParam(this.m_BaseHandle.CharaID).m_BodyID + 1);
			this.m_NextMode = new RoomCharaActMode();
			this.m_NextMode.ResetNextActionPer(RoomCharaActMode.eMode.Idle);
			this.m_ObjSearch = new RoomCharaObjSearch(4);
		}

		// Token: 0x06001D2E RID: 7470 RVA: 0x0009CF1E File Offset: 0x0009B31E
		public override void Destroy()
		{
			this.m_BaseHandle = null;
			this.m_Builder = null;
			this.m_AStar = null;
			this.m_BlockData = null;
			if (this.m_AnimeMap != null)
			{
				this.m_AnimeMap = null;
			}
			this.m_Mode = RoomCharaActMode.eMode.None;
		}

		// Token: 0x06001D2F RID: 7471 RVA: 0x0009CF58 File Offset: 0x0009B358
		private bool ChangeMode(RoomCharaActMode.eMode fmode, IRoomCharaAct pcall)
		{
			bool result = this.m_Mode != fmode;
			this.m_Mode = fmode;
			this.m_CallUpdate = pcall;
			return result;
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x0009CF84 File Offset: 0x0009B384
		public bool CheckExistAnyOnCurrent(int floorid)
		{
			bool flag = false;
			if (this.m_FloorID == floorid)
			{
				flag = this.m_MoveAct.IsMoveCost(this);
				if (flag)
				{
					if (this.m_CallUpdate != null)
					{
						this.m_CallUpdate.CancelMode(this, true);
					}
					Vector2 vector = this.m_GridState.CalcFreeMovePoint();
					Vector2 vector2 = RoomUtility.GridToTilePos(vector.x, vector.y);
					this.SetCurrentPos(new Vector3(vector2.x, vector2.y, 0f), (int)vector.x, (int)vector.y);
					this.RequestIdleMode(1f);
				}
			}
			return flag;
		}

		// Token: 0x06001D31 RID: 7473 RVA: 0x0009D028 File Offset: 0x0009B428
		public void RequestIdleMode(float foffset = 1f)
		{
			if (this.m_CallUpdate != null)
			{
				this.m_CallUpdate.CancelMode(this, true);
			}
			if (this.m_IdleAct != null)
			{
				bool foveranm = this.ChangeMode(RoomCharaActMode.eMode.Idle, this.m_IdleAct);
				this.m_IdleAct.RequestIdleMode(this, foveranm, 1f);
			}
		}

		// Token: 0x06001D32 RID: 7474 RVA: 0x0009D079 File Offset: 0x0009B479
		private void RequestMoveMode(Vector2 fendPos)
		{
			if (this.m_MoveAct.RequestMoveMode(this, fendPos))
			{
				this.ChangeMode(RoomCharaActMode.eMode.Move, this.m_MoveAct);
			}
			else
			{
				this.RequestIdleMode(1f);
			}
		}

		// Token: 0x06001D33 RID: 7475 RVA: 0x0009D0AC File Offset: 0x0009B4AC
		private bool RequestObjectEvent(IRoomObjectControll roomObjHndl)
		{
			bool flag = this.m_ObjAct.RequestObjectAct(this, roomObjHndl);
			if (flag)
			{
				this.ChangeMode(RoomCharaActMode.eMode.ObjEvent, this.m_ObjAct);
			}
			this.m_ObjSearch.ClearSearchInfo();
			return flag;
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x0009D0E7 File Offset: 0x0009B4E7
		public void AbortObjEventMode()
		{
			if (this.m_CallUpdate != null)
			{
				if (this.m_CallUpdate.CancelMode(this, true))
				{
					this.RequestIdleMode(1f);
				}
			}
			else
			{
				this.RequestIdleMode(1f);
			}
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x0009D124 File Offset: 0x0009B524
		public bool RequestSleep(int factionno)
		{
			bool flag = this.m_SleepAct.RequestSleep(this, factionno);
			if (!flag)
			{
				this.RequestIdleMode(1f);
			}
			else
			{
				this.ChangeMode(RoomCharaActMode.eMode.Sleep, this.m_SleepAct);
			}
			this.m_ObjSearch.ClearSearchInfo();
			return flag;
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x0009D170 File Offset: 0x0009B570
		protected void CheckNextUpdate()
		{
			float num = UnityEngine.Random.Range(0f, 100f);
			if (this.m_IrqReq.IsIrqRequest())
			{
				switch (this.m_IrqReq.GetCommand().m_Command)
				{
				case RoomActionCommand.eActionCode.Stop:
					this.RequestIdleMode(1f);
					break;
				case RoomActionCommand.eActionCode.Walk:
				{
					Vector2 fendPos;
					fendPos.x = (float)this.m_IrqReq.GetCommand().m_ParamI1;
					fendPos.y = (float)this.m_IrqReq.GetCommand().m_ParamI2;
					this.RequestMoveMode(fendPos);
					break;
				}
				case RoomActionCommand.eActionCode.Jump:
				{
					Vector2 position;
					position.x = (float)this.m_IrqReq.GetCommand().m_ParamI1;
					position.y = (float)this.m_IrqReq.GetCommand().m_ParamI2;
					this.SetPosition(position);
					this.RequestIdleMode(1f);
					break;
				}
				case RoomActionCommand.eActionCode.Action:
					switch (this.m_IrqReq.GetCommand().m_ParamI1)
					{
					case 0:
						this.RequestIdleMode(1f);
						break;
					case 1:
						this.m_EnjoyAct.RequestEnjoyMode(this);
						this.ChangeMode(RoomCharaActMode.eMode.Enjoy, this.m_EnjoyAct);
						break;
					case 2:
						this.m_TweetAct.RequestTweetMode(this);
						this.ChangeMode(RoomCharaActMode.eMode.Tweet, this.m_TweetAct);
						break;
					case 3:
						this.m_RejoiceAct.RequestRejoiceMode(this);
						this.ChangeMode(RoomCharaActMode.eMode.Rejoice, this.m_RejoiceAct);
						break;
					}
					break;
				case RoomActionCommand.eActionCode.Event:
				{
					bool flag = this.RequestObjectEvent(this.m_ObjSearch.CalcObjectEventFromCurrentPos(this.m_Builder, this, this.m_FloorID));
					break;
				}
				case RoomActionCommand.eActionCode.Erase:
					this.m_RoomOutAct.RequestRoomOut(this, this.m_IrqReq.GetCommand().m_ParamI1);
					this.ChangeMode(RoomCharaActMode.eMode.RoomOut, this.m_RoomOutAct);
					break;
				case RoomActionCommand.eActionCode.Sleep:
					this.RequestSleep(this.m_IrqReq.GetCommand().m_ParamI1);
					break;
				}
				this.m_IrqReq.ClrCommand();
			}
			else if (this.m_IrqReq.IsDebugMode())
			{
				this.RequestIdleMode(1f);
			}
			else
			{
				for (int i = 0; i < this.m_NextMode.m_NextActionNum; i++)
				{
					num -= this.m_NextMode.m_NextActionPer[i].m_Per;
					if (num <= 0f)
					{
						bool flag = true;
						RoomCharaActMode.eMode type = this.m_NextMode.m_NextActionPer[i].m_Type;
						switch (type + 1)
						{
						case RoomCharaActMode.eMode.Idle:
						case RoomCharaActMode.eMode.Move:
							this.RequestIdleMode(1f);
							break;
						case RoomCharaActMode.eMode.Tweet:
						{
							Vector2 fendPos2 = this.m_GridState.CalcFreeMovePoint();
							this.RequestMoveMode(fendPos2);
							break;
						}
						case RoomCharaActMode.eMode.Enjoy:
							this.RequestIdleMode(1f);
							break;
						case RoomCharaActMode.eMode.Rejoice:
							this.m_EnjoyAct.RequestEnjoyMode(this);
							this.ChangeMode(RoomCharaActMode.eMode.Enjoy, this.m_EnjoyAct);
							break;
						case RoomCharaActMode.eMode.ObjEvent:
							this.m_RejoiceAct.RequestRejoiceMode(this);
							this.ChangeMode(RoomCharaActMode.eMode.Rejoice, this.m_RejoiceAct);
							break;
						case RoomCharaActMode.eMode.Sleep:
							flag = this.RequestObjectEvent(this.m_ObjSearch.CalcObjectEventFromCurrentPos(this.m_Builder, this, this.m_FloorID));
							break;
						}
						if (flag)
						{
							this.m_NextMode.ResetNextActionPer(this.m_Mode);
							break;
						}
						num += this.m_NextMode.m_NextActionPer[i].m_Per;
					}
				}
			}
			this.m_IrqReq.ClearReq();
		}

		// Token: 0x0400239B RID: 9115
		public bool m_DebugKey;

		// Token: 0x0400239C RID: 9116
		private IRoomCharaManager m_CharaManager;

		// Token: 0x0400239D RID: 9117
		private IRoomObjectControll m_LinkObj;

		// Token: 0x0400239E RID: 9118
		private GameObject m_ModelNode;

		// Token: 0x0400239F RID: 9119
		protected CharacterHandler m_BaseHandle;

		// Token: 0x040023A0 RID: 9120
		protected CharacterAnim m_BaseAnime;

		// Token: 0x040023A1 RID: 9121
		private int m_CharaID;

		// Token: 0x040023A2 RID: 9122
		protected RoomAnimAccessMap m_AnimeMap;

		// Token: 0x040023A3 RID: 9123
		protected RoomCharaHud m_RoomCharaHud;

		// Token: 0x040023A4 RID: 9124
		protected Transform m_ShadowHandle;

		// Token: 0x040023A5 RID: 9125
		protected Vector3 m_ShadowPos = Vector3.zero;

		// Token: 0x040023A6 RID: 9126
		public RoomShadowControll m_Shadow;

		// Token: 0x040023A7 RID: 9127
		public float m_ShadowLength = 0.25f;

		// Token: 0x040023A8 RID: 9128
		private IRoomObjectControll.ModelRenderConfig m_DefaultConfig = default(IRoomObjectControll.ModelRenderConfig);

		// Token: 0x040023A9 RID: 9129
		private RoomGridState m_GridState;

		// Token: 0x040023AA RID: 9130
		private AStar m_AStar;

		// Token: 0x040023AB RID: 9131
		private Renderer m_ShaderRender;

		// Token: 0x040023AC RID: 9132
		private bool m_ShadowUp;

		// Token: 0x040023AD RID: 9133
		private bool m_WakeUpActions;

		// Token: 0x040023AE RID: 9134
		private Color m_BaseColor = Color.white;

		// Token: 0x040023AF RID: 9135
		private RoomCharaEventReq m_IrqReq;

		// Token: 0x040023B0 RID: 9136
		private bool m_TouchStepUp;

		// Token: 0x040023B1 RID: 9137
		private bool m_TouchToProfile;

		// Token: 0x040023B2 RID: 9138
		private float m_TouchTime;

		// Token: 0x040023B3 RID: 9139
		private Vector2 m_TouchOffset;

		// Token: 0x040023B4 RID: 9140
		private RoomCharaObjSearch m_ObjSearch;

		// Token: 0x040023B5 RID: 9141
		private RoomCharaActMode.eMode m_Mode = RoomCharaActMode.eMode.None;

		// Token: 0x040023B6 RID: 9142
		public IRoomCharaAct m_CallUpdate;

		// Token: 0x040023B7 RID: 9143
		private RoomCharaActMode m_NextMode;

		// Token: 0x040023B8 RID: 9144
		private RoomCharaActIdle m_IdleAct;

		// Token: 0x040023B9 RID: 9145
		private RoomCharaActMove m_MoveAct;

		// Token: 0x040023BA RID: 9146
		private RoomCharaActObject m_ObjAct;

		// Token: 0x040023BB RID: 9147
		private RoomCharaActRejoice m_RejoiceAct;

		// Token: 0x040023BC RID: 9148
		private RoomCharaActEnjoy m_EnjoyAct;

		// Token: 0x040023BD RID: 9149
		private RoomCharaActTweet m_TweetAct;

		// Token: 0x040023BE RID: 9150
		private RoomCharaActRoomOut m_RoomOutAct;

		// Token: 0x040023BF RID: 9151
		private RoomCharaActSleep m_SleepAct;
	}
}
