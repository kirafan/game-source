﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005CC RID: 1484
	public class RoomObjectCtrlBedding : RoomObjectModel
	{
		// Token: 0x06001CD7 RID: 7383 RVA: 0x0009AD5C File Offset: 0x0009915C
		public override void SetUpObjectKey(GameObject hbase)
		{
			this.m_ActionWait = 0;
			this.m_EntryLocater = 0;
			this.m_EntryLocaterMax = 0;
			this.m_Locator = null;
			Transform transform = RoomUtility.FindTransform(hbase.transform, "LOC_0", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			if (this.m_EntryLocater != 0)
			{
				this.m_EntryLocaterMax = this.m_EntryLocater;
				this.m_Locator = new RoomObjectCtrlBedding.TLocaterInfo[this.m_EntryLocater];
				this.m_EntryLocater = 0;
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_0", 16);
				if (transform != null)
				{
					this.m_Locator[this.m_EntryLocater].m_Trs = transform;
					this.m_Locator[this.m_EntryLocater].m_Link = null;
					this.m_Locator[this.m_EntryLocater].m_GridPos = RoomUtility.CalcDummyGridPos(transform.localPosition);
					this.m_EntryLocater++;
				}
			}
		}

		// Token: 0x06001CD8 RID: 7384 RVA: 0x0009AE60 File Offset: 0x00099260
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			if (base.IsInRange(gridX, gridY, fdir) && gridX >= this.m_BlockData.PosX && gridX < this.m_BlockData.PosX + this.m_BlockData.SizeX && gridY >= this.m_BlockData.PosY && gridY < this.m_BlockData.PosY + this.m_BlockData.SizeY)
			{
				int num = this.CalcAttachIndex((float)gridX, (float)gridY);
				if (num >= 0 && this.m_Locator[num].m_Link == null)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001CD9 RID: 7385 RVA: 0x0009AF08 File Offset: 0x00099308
		public override Transform GetAttachTransform(Vector2 fpos)
		{
			if (this.m_Locator != null)
			{
				int num = this.CalcAttachIndex(fpos.x, fpos.y);
				if (num >= 0 && this.m_Locator[num].m_Trs != null)
				{
					return this.m_Locator[num].m_Trs;
				}
			}
			return this.m_OwnerTrs;
		}

		// Token: 0x06001CDA RID: 7386 RVA: 0x0009AF70 File Offset: 0x00099370
		private int CalcAttachIndex(float fgridx, float fgridy)
		{
			float num;
			float num2;
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				num = fgridx - this.m_BlockData.PosSpaceX;
				num2 = fgridy - this.m_BlockData.PosSpaceY;
			}
			else
			{
				num2 = fgridx - this.m_BlockData.PosSpaceX;
				num = fgridy - this.m_BlockData.PosSpaceY;
			}
			int result = -1;
			float num3 = 1000000f;
			for (int i = 0; i < this.m_EntryLocaterMax; i++)
			{
				Vector3 vector = this.m_Locator[i].m_GridPos;
				float num4 = vector.x - num;
				float num5 = vector.y - num2;
				float num6 = num4 * num4 + num5 * num5;
				if (num6 < num3)
				{
					result = i;
					num3 = num6;
				}
			}
			return result;
		}

		// Token: 0x06001CDB RID: 7387 RVA: 0x0009B032 File Offset: 0x00099432
		public override void LinkKeyLock(bool flock)
		{
			if (flock)
			{
				this.m_LockCnt++;
			}
			else
			{
				this.m_LockCnt--;
			}
		}

		// Token: 0x06001CDC RID: 7388 RVA: 0x0009B05B File Offset: 0x0009945B
		public override bool IsAction()
		{
			return this.m_ActionWait < this.m_EntryLocaterMax && this.m_LockCnt < this.m_EntryLocaterMax;
		}

		// Token: 0x06001CDD RID: 7389 RVA: 0x0009B080 File Offset: 0x00099480
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
			if (plink != null)
			{
				plink.BlockData.SetAttachObject(fattach);
			}
			if (fattach)
			{
				plink.BlockData.SetAttachObject(fattach);
				if (this.m_ActionWait < this.m_EntryLocaterMax)
				{
					this.m_Locator[this.m_ActionWait].m_Link = plink;
					this.m_ActionWait++;
				}
			}
			else if (this.m_ActionWait != 0)
			{
				this.m_ActionWait--;
				this.m_Locator[this.m_ActionWait].m_Link = null;
			}
		}

		// Token: 0x06001CDE RID: 7390 RVA: 0x0009B124 File Offset: 0x00099524
		public override void LinkObjectParam(int forder, float fposz)
		{
			if (this.m_ActionWait != 0)
			{
				for (int i = 0; i < this.m_EntryLocater; i++)
				{
					if (this.m_Locator[i].m_Link != null)
					{
						this.m_Locator[i].m_Link.SetSortingOrder(fposz - 0.3f, forder + 1);
					}
				}
			}
		}

		// Token: 0x06001CDF RID: 7391 RVA: 0x0009B194 File Offset: 0x00099594
		public override void Destroy()
		{
			if (this.m_ActionWait != 0)
			{
				this.m_IsAvailable = false;
				for (int i = 0; i < this.m_ActionWait; i++)
				{
					RoomObjectCtrlChara roomObjectCtrlChara = this.m_Locator[i].m_Link as RoomObjectCtrlChara;
					if (roomObjectCtrlChara != null)
					{
						roomObjectCtrlChara.AbortObjEventMode();
					}
				}
			}
			base.Destroy();
		}

		// Token: 0x06001CE0 RID: 7392 RVA: 0x0009B1FC File Offset: 0x000995FC
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			bool flag = true;
			if (fstep == eTouchStep.End)
			{
				for (int i = 0; i < this.m_EntryLocater; i++)
				{
					if (this.m_Locator[i].m_Link != null)
					{
						RoomObjectCtrlChara roomObjectCtrlChara = this.m_Locator[i].m_Link as RoomObjectCtrlChara;
						if (roomObjectCtrlChara != null)
						{
							roomObjectCtrlChara.SetWakeUpAction(true);
							flag = false;
						}
					}
				}
				if (flag)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ROOM_OBJ_SELECT, 1f, 0, -1, -1);
				}
			}
			return !fgrid.IsMoveSlide();
		}

		// Token: 0x06001CE1 RID: 7393 RVA: 0x0009B2A8 File Offset: 0x000996A8
		public override void AbortLinkEventObj()
		{
			for (int i = 0; i < this.m_EntryLocaterMax; i++)
			{
				if (this.m_Locator[i].m_Link != null)
				{
					RoomObjectCtrlChara roomObjectCtrlChara = this.m_Locator[i].m_Link as RoomObjectCtrlChara;
					if (roomObjectCtrlChara != null)
					{
						roomObjectCtrlChara.AbortObjEventMode();
					}
				}
			}
		}

		// Token: 0x04002386 RID: 9094
		private int m_ActionWait;

		// Token: 0x04002387 RID: 9095
		private int m_EntryLocater;

		// Token: 0x04002388 RID: 9096
		private int m_EntryLocaterMax;

		// Token: 0x04002389 RID: 9097
		private int m_LockCnt;

		// Token: 0x0400238A RID: 9098
		private RoomObjectCtrlBedding.TLocaterInfo[] m_Locator;

		// Token: 0x020005CD RID: 1485
		public struct TLocaterInfo
		{
			// Token: 0x0400238B RID: 9099
			public Transform m_Trs;

			// Token: 0x0400238C RID: 9100
			public IRoomObjectControll m_Link;

			// Token: 0x0400238D RID: 9101
			public Vector2 m_GridPos;
		}
	}
}
