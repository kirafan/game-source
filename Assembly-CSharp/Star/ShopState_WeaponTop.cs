﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x02000493 RID: 1171
	public class ShopState_WeaponTop : ShopState
	{
		// Token: 0x060016F1 RID: 5873 RVA: 0x000779EF File Offset: 0x00075DEF
		public ShopState_WeaponTop(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016F2 RID: 5874 RVA: 0x00077A07 File Offset: 0x00075E07
		public override int GetStateID()
		{
			return 10;
		}

		// Token: 0x060016F3 RID: 5875 RVA: 0x00077A0B File Offset: 0x00075E0B
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponTop.eStep.First;
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x00077A14 File Offset: 0x00075E14
		public override void OnStateExit()
		{
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x00077A16 File Offset: 0x00075E16
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016F6 RID: 5878 RVA: 0x00077A20 File Offset: 0x00075E20
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponTop.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponTop.eStep.LoadWait;
				break;
			case ShopState_WeaponTop.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponTop.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponTop.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_WeaponTop.eStep.Main;
				}
				break;
			case ShopState_WeaponTop.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case ShopWeaponTopUI.eMainButton.Create:
							this.m_NextState = 11;
							break;
						case ShopWeaponTopUI.eMainButton.Upgrade:
							this.m_NextState = 12;
							break;
						case ShopWeaponTopUI.eMainButton.Sale:
							this.m_NextState = 14;
							break;
						default:
							this.m_NextState = 1;
							break;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponTop.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponTop.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_WeaponTop.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016F7 RID: 5879 RVA: 0x00077B94 File Offset: 0x00075F94
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopWeaponTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickMainButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.ShopWeapon);
			this.m_UI.PlayIn();
			if (this.m_Owner.IsRequestVocie)
			{
				this.m_Owner.m_NpcUI.PlayInVoice(eSoundVoiceControllListDB.Poruka_in);
				this.m_Owner.IsRequestVocie = false;
			}
			this.m_Owner.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Weapon);
			return true;
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x00077C71 File Offset: 0x00076071
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.m_Owner.m_NpcUI.PlayOut();
			this.GoToMenuEnd();
		}

		// Token: 0x060016F9 RID: 5881 RVA: 0x00077C90 File Offset: 0x00076090
		private void OnClickButtonCallBack()
		{
			if (this.m_UI.SelectButton == ShopWeaponTopUI.eMainButton.LimitExtend)
			{
				this.LimitExtend();
				return;
			}
			this.GoToMenuEnd();
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x00077CB0 File Offset: 0x000760B0
		private void LimitExtend()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimitCount + 1 <= (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[15].m_Value)
			{
				int num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[14].m_Value;
				int num2 = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[13].m_Value;
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)num))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendGemIsShort, new object[]
					{
						num
					}), null, null);
				}
				else
				{
					int weaponLimit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit;
					int num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit + (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[13].m_Value;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtend, new object[]
					{
						UIUtility.GemValueToString(num, true),
						num2
					}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendSub, new object[]
					{
						weaponLimit,
						num3,
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true),
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem - (long)num, true)
					}), new Action<int>(this.OnConfirmLimitAdd));
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponLimitExtendErr), null, null);
			}
		}

		// Token: 0x060016FB RID: 5883 RVA: 0x00077EF7 File Offset: 0x000762F7
		private void OnConfirmLimitAdd(int btn)
		{
			if (btn == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_WeaponLimitAdd(new Action<MeigewwwParam, Weaponlimitadd>(this.OnResponseLimitAdd));
			}
		}

		// Token: 0x060016FC RID: 5884 RVA: 0x00077F1C File Offset: 0x0007631C
		private void OnResponseLimitAdd(MeigewwwParam wwwParam, Weaponlimitadd param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
		}

		// Token: 0x060016FD RID: 5885 RVA: 0x00077F58 File Offset: 0x00076358
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001DB0 RID: 7600
		private ShopState_WeaponTop.eStep m_Step = ShopState_WeaponTop.eStep.None;

		// Token: 0x04001DB1 RID: 7601
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopWeaponTopUI;

		// Token: 0x04001DB2 RID: 7602
		private ShopWeaponTopUI m_UI;

		// Token: 0x02000494 RID: 1172
		private enum eStep
		{
			// Token: 0x04001DB4 RID: 7604
			None = -1,
			// Token: 0x04001DB5 RID: 7605
			First,
			// Token: 0x04001DB6 RID: 7606
			LoadStart,
			// Token: 0x04001DB7 RID: 7607
			LoadWait,
			// Token: 0x04001DB8 RID: 7608
			PlayIn,
			// Token: 0x04001DB9 RID: 7609
			Main,
			// Token: 0x04001DBA RID: 7610
			UnloadChildSceneWait
		}
	}
}
