﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000087 RID: 135
	[AddComponentMenu("ActionAggregation/ChildRotateZ")]
	[Serializable]
	public class ActionAggregationChildRotateZActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x0600041E RID: 1054 RVA: 0x000140B8 File Offset: 0x000124B8
		public override void Update()
		{
			if (base.GetState() == eActionAggregationState.Play)
			{
				float deltaTime = Time.deltaTime;
				this.m_ElapsedTime += deltaTime;
				this.m_RectTransform.eulerAngles = new Vector3(this.m_RectTransform.eulerAngles.x, this.m_RectTransform.eulerAngles.y, this.m_BaseRotateZ + this.m_RectTransform.eulerAngles.z + deltaTime * this.m_RotateZPerSecond);
				if (this.m_PlaySecond <= this.m_ElapsedTime)
				{
					base.gameObject.SetActive(false);
					base.SetState(eActionAggregationState.Finish);
				}
			}
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x00014162 File Offset: 0x00012562
		public override void PlayStartExtendProcess()
		{
			base.gameObject.SetActive(true);
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_ElapsedTime = 0f;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00014187 File Offset: 0x00012587
		public override void Skip()
		{
			base.gameObject.SetActive(false);
			base.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x04000230 RID: 560
		[SerializeField]
		private float m_PlaySecond;

		// Token: 0x04000231 RID: 561
		[SerializeField]
		private float m_RotateZPerSecond;

		// Token: 0x04000232 RID: 562
		private RectTransform m_RectTransform;

		// Token: 0x04000233 RID: 563
		private float m_BaseRotateZ;

		// Token: 0x04000234 RID: 564
		private float m_ElapsedTime;
	}
}
