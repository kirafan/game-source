﻿using System;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using UnityEngine;

namespace Star
{
	// Token: 0x02000660 RID: 1632
	public class PushNotificationController : MonoBehaviour
	{
		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06002108 RID: 8456 RVA: 0x000AFF05 File Offset: 0x000AE305
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06002109 RID: 8457 RVA: 0x000AFF0D File Offset: 0x000AE30D
		private AWSCredentials Credentials
		{
			get
			{
				if (this._credentials == null)
				{
					this._credentials = new CognitoAWSCredentials(this.IdentityPoolId, this._CognitoIdentityRegion);
				}
				return this._credentials;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x0600210A RID: 8458 RVA: 0x000AFF37 File Offset: 0x000AE337
		private IAmazonSimpleNotificationService SnsClient
		{
			get
			{
				if (this._snsClient == null)
				{
					this._snsClient = new AmazonSimpleNotificationServiceClient(this.Credentials, this._SNSRegion);
				}
				return this._snsClient;
			}
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x0600210B RID: 8459 RVA: 0x000AFF61 File Offset: 0x000AE361
		private RegionEndpoint _CognitoIdentityRegion
		{
			get
			{
				return RegionEndpoint.GetBySystemName(this.CognitoIdentityRegion);
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x0600210C RID: 8460 RVA: 0x000AFF6E File Offset: 0x000AE36E
		private RegionEndpoint _SNSRegion
		{
			get
			{
				return RegionEndpoint.GetBySystemName(this.SNSRegion);
			}
		}

		// Token: 0x0600210D RID: 8461 RVA: 0x000AFF7B File Offset: 0x000AE37B
		private void Start()
		{
			UnityInitializer.AttachToGameObject(base.gameObject);
		}

		// Token: 0x0600210E RID: 8462 RVA: 0x000AFF88 File Offset: 0x000AE388
		public void RegisterDevice()
		{
			if (string.IsNullOrEmpty(this.GoogleConsoleProjectId))
			{
				Debug.Log("sender id is null");
				return;
			}
			this.m_Log = string.Empty;
			GCM.Register(new Action<string>(this.RegisterAndroidDeviceCallback), new string[]
			{
				this.GoogleConsoleProjectId
			});
		}

		// Token: 0x0600210F RID: 8463 RVA: 0x000AFFDC File Offset: 0x000AE3DC
		private void RegisterAndroidDeviceCallback(string regId)
		{
			if (string.IsNullOrEmpty(regId))
			{
				this.m_Log = string.Format("Failed to get the registration id", new object[0]);
				return;
			}
			this.m_Log = string.Format("Your registration Id is = {0}", regId);
			this.SnsClient.CreatePlatformEndpointAsync(new CreatePlatformEndpointRequest
			{
				Token = regId,
				PlatformApplicationArn = this.AndroidPlatformApplicationArn
			}, new AmazonServiceCallback<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse>(this.CreatePlatformEndpointAsyncAndroidCallback), null);
		}

		// Token: 0x06002110 RID: 8464 RVA: 0x000B004E File Offset: 0x000AE44E
		private void CheckForDeviceToken()
		{
		}

		// Token: 0x06002111 RID: 8465 RVA: 0x000B0050 File Offset: 0x000AE450
		private void CreatePlatformEndpointAsyncAndroidCallback(AmazonServiceResult<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse> resultObject)
		{
			if (resultObject.Exception == null)
			{
				CreatePlatformEndpointResponse response = resultObject.Response;
				this._endpointArn = response.EndpointArn;
				this.m_Log += string.Format("Platform endpoint arn is = {0}", response.EndpointArn);
				Debug.Log("end point = " + response.EndpointArn);
			}
		}

		// Token: 0x06002112 RID: 8466 RVA: 0x000B00B4 File Offset: 0x000AE4B4
		public void UnregisterDevice()
		{
			if (!string.IsNullOrEmpty(this._endpointArn))
			{
				this.SnsClient.DeleteEndpointAsync(new DeleteEndpointRequest
				{
					EndpointArn = this._endpointArn
				}, new AmazonServiceCallback<DeleteEndpointRequest, DeleteEndpointResponse>(this.UnregisterDeviceCallback), null);
			}
		}

		// Token: 0x06002113 RID: 8467 RVA: 0x000B00FC File Offset: 0x000AE4FC
		private void UnregisterDeviceCallback(AmazonServiceResult<DeleteEndpointRequest, DeleteEndpointResponse> resultObject)
		{
			if (resultObject.Exception == null)
			{
				Debug.Log("Deleted the endpoint. You will not get any new notifications");
				this.m_Log = "You will not get any new notifications";
			}
		}

		// Token: 0x06002114 RID: 8468 RVA: 0x000B011E File Offset: 0x000AE51E
		public string GetLog()
		{
			return this.m_Log;
		}

		// Token: 0x04002733 RID: 10035
		private string IdentityPoolId = "ap-northeast-1:4c346987-0ccf-45c3-9ea8-227e81f09e91";

		// Token: 0x04002734 RID: 10036
		private string AndroidPlatformApplicationArn = "arn:aws:sns:ap-northeast-1:783388786474:app/GCM/develop_android";

		// Token: 0x04002735 RID: 10037
		private string GoogleConsoleProjectId = "939274666497";

		// Token: 0x04002736 RID: 10038
		private string CognitoIdentityRegion = "ap-northeast-1";

		// Token: 0x04002737 RID: 10039
		private string SNSRegion = "ap-northeast-1";

		// Token: 0x04002738 RID: 10040
		private string _endpointArn;

		// Token: 0x04002739 RID: 10041
		private AWSCredentials _credentials;

		// Token: 0x0400273A RID: 10042
		private IAmazonSimpleNotificationService _snsClient;

		// Token: 0x0400273B RID: 10043
		private string m_Log = string.Empty;
	}
}
