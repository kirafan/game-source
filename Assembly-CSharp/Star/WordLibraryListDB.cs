﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200026A RID: 618
	public class WordLibraryListDB : ScriptableObject
	{
		// Token: 0x040013E5 RID: 5093
		public WordLibraryListDB_Param[] m_Params;
	}
}
