﻿using System;

namespace Star
{
	// Token: 0x0200055C RID: 1372
	public class CActScriptKeyObjCfg : IRoomScriptData
	{
		// Token: 0x06001AE7 RID: 6887 RVA: 0x0008F258 File Offset: 0x0008D658
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyObjectCfg actXlsKeyObjectCfg = (ActXlsKeyObjectCfg)pbase;
			this.m_OffsetLayer = actXlsKeyObjectCfg.m_OffsetLayer;
			this.m_OffsetZ = actXlsKeyObjectCfg.m_OffsetZ;
			this.m_TargetName = actXlsKeyObjectCfg.m_TargetName;
			this.m_CRCKey = CRC32.Calc(actXlsKeyObjectCfg.m_TargetName);
		}

		// Token: 0x040021C2 RID: 8642
		public int m_OffsetLayer;

		// Token: 0x040021C3 RID: 8643
		public float m_OffsetZ;

		// Token: 0x040021C4 RID: 8644
		public string m_TargetName;

		// Token: 0x040021C5 RID: 8645
		public uint m_CRCKey;
	}
}
