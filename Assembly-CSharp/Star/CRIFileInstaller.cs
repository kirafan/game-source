﻿using System;
using System.Collections.Generic;
using System.IO;
using PreviewLabs;
using UnityEngine;

namespace Star
{
	// Token: 0x0200063E RID: 1598
	public class CRIFileInstaller
	{
		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06001F8C RID: 8076 RVA: 0x000AB411 File Offset: 0x000A9811
		public bool IsInstalling
		{
			get
			{
				return this.m_IsInstalling;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06001F8D RID: 8077 RVA: 0x000AB419 File Offset: 0x000A9819
		public bool IsError
		{
			get
			{
				return this.m_IsError;
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06001F8E RID: 8078 RVA: 0x000AB424 File Offset: 0x000A9824
		public float FinishedFileCount
		{
			get
			{
				float num = (float)this.m_FileCount;
				if (this.m_InstallingList != null)
				{
					for (int i = 0; i < this.m_InstallingList.Count; i++)
					{
						num += this.m_InstallingList[i].GetProgress();
					}
				}
				return num;
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06001F8F RID: 8079 RVA: 0x000AB475 File Offset: 0x000A9875
		public float FileCountMax
		{
			get
			{
				return (float)this.m_FileCountMax;
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06001F90 RID: 8080 RVA: 0x000AB47E File Offset: 0x000A987E
		public float Ratio
		{
			get
			{
				if (this.FileCountMax <= 0f)
				{
					return 1f;
				}
				return this.FinishedFileCount / this.FileCountMax;
			}
		}

		// Token: 0x06001F91 RID: 8081 RVA: 0x000AB4A4 File Offset: 0x000A98A4
		public void Update()
		{
			if (this.m_IsInstalling)
			{
				if (!this.m_IsError && this.m_InstallFileNames.Count > 0)
				{
					this.m_RemoveKeys.Clear();
					for (int i = 0; i < this.m_InstallFileNames.Count; i++)
					{
						if ((long)this.m_InstallingList.Count >= (long)((ulong)this.m_SimultaneouslyNum))
						{
							break;
						}
						uint idealVer = 0U;
						CriFsInstallRequest criFsInstallRequest = this.InstallProcess(this.m_InstallFileNames[i], out idealVer);
						if (criFsInstallRequest != null)
						{
							CRIFileInstaller.InstallData installData = new CRIFileInstaller.InstallData();
							installData.m_FileName = this.m_InstallFileNames[i];
							installData.m_IdealVer = idealVer;
							installData.m_Request = criFsInstallRequest;
							this.m_InstallingList.Add(installData);
						}
						else
						{
							this.m_FileCount++;
						}
						this.m_RemoveKeys.Add(i);
					}
					for (int j = this.m_RemoveKeys.Count - 1; j >= 0; j--)
					{
						this.m_InstallFileNames.RemoveAt(this.m_RemoveKeys[j]);
					}
				}
				for (int k = this.m_InstallingList.Count - 1; k >= 0; k--)
				{
					if (this.m_InstallingList[k].m_Request.isDone)
					{
						if (!string.IsNullOrEmpty(this.m_InstallingList[k].m_Request.error))
						{
							this.m_IsError = true;
							this.m_ErrorFileNames.Add(this.m_InstallingList[k].m_FileName);
						}
						else
						{
							this.SetVersion(this.m_InstallingList[k].m_FileName, this.m_InstallingList[k].m_IdealVer.ToString());
							this.m_FileCount++;
						}
						this.m_InstallingList[k].m_Request.Dispose();
						this.m_InstallingList.RemoveAt(k);
					}
				}
				if (this.m_IsError)
				{
					if (this.m_InstallingList.Count == 0)
					{
						this.m_IsInstalling = false;
					}
				}
				else if (this.m_InstallFileNames.Count <= 0 && this.m_InstallingList.Count <= 0)
				{
					this.m_IsInstalling = false;
				}
			}
		}

		// Token: 0x06001F92 RID: 8082 RVA: 0x000AB704 File Offset: 0x000A9B04
		public void Setup(string url)
		{
			this.m_URL = url;
			CRIFileInstaller.InstallPath = Path.Combine(CriWare.installTargetPath, "c.r").Replace("\\", "/");
			if (!CriFsWebInstaller.isInitialized)
			{
				CriFsWebInstaller.ModuleConfig defaultModuleConfig = CriFsWebInstaller.defaultModuleConfig;
				defaultModuleConfig.numInstallers = this.m_SimultaneouslyNum;
				defaultModuleConfig.inactiveTimeoutSec = 20U;
				CriFsWebInstaller.InitializeModule(defaultModuleConfig);
			}
			if (!Directory.Exists(CRIFileInstaller.InstallPath))
			{
				Directory.CreateDirectory(CRIFileInstaller.InstallPath);
			}
			this.m_IsInstalling = false;
			this.m_IsError = false;
			this.m_FileCountMax = 0;
			Debug.LogFormat("CRIFileInstaller.Setup()  >>> URL:{0}, InstallPath:{1}", new object[]
			{
				this.m_URL,
				CRIFileInstaller.InstallPath
			});
		}

		// Token: 0x06001F93 RID: 8083 RVA: 0x000AB7B7 File Offset: 0x000A9BB7
		public void Destroy()
		{
			if (CriFsWebInstaller.isInitialized)
			{
				CriFsWebInstaller.FinalizeModule();
			}
		}

		// Token: 0x06001F94 RID: 8084 RVA: 0x000AB7C8 File Offset: 0x000A9BC8
		public static int ParseCRIFileVersionDB(CRIFileVersionDB criFileVersionDB, bool isFirstDL, out Dictionary<string, uint> out_versionDict, out List<string> out_installFileNames, string specificFileNameWithoutExt = null)
		{
			out_versionDict = new Dictionary<string, uint>();
			out_installFileNames = new List<string>();
			for (int i = 0; i < criFileVersionDB.m_Params.Length; i++)
			{
				if (!isFirstDL || criFileVersionDB.m_Params[i].m_FirstDL == 1)
				{
					if (string.IsNullOrEmpty(specificFileNameWithoutExt) || !(criFileVersionDB.m_Params[i].m_FileName != specificFileNameWithoutExt))
					{
						string key = null;
						uint value = 0U;
						List<string> paths = criFileVersionDB.GetPaths(i, out key, out value);
						if (!paths.Contains("Star.acf"))
						{
							if (!out_versionDict.ContainsKey(key))
							{
								out_versionDict.Add(key, value);
							}
							out_installFileNames.AddRange(paths);
						}
					}
				}
			}
			return out_installFileNames.Count;
		}

		// Token: 0x06001F95 RID: 8085 RVA: 0x000AB89C File Offset: 0x000A9C9C
		public void Install(CRIFileVersionDB criFileVersionDB, bool isFirstDL, string specificFileNameWithoutExt)
		{
			Dictionary<string, uint> versionDict = new Dictionary<string, uint>();
			List<string> installFileNames = new List<string>();
			CRIFileInstaller.ParseCRIFileVersionDB(criFileVersionDB, isFirstDL, out versionDict, out installFileNames, specificFileNameWithoutExt);
			this.Install(versionDict, installFileNames);
		}

		// Token: 0x06001F96 RID: 8086 RVA: 0x000AB8CC File Offset: 0x000A9CCC
		public void Install(CRIFileVersionDB criFileVersionDB, bool isFirstDL)
		{
			Dictionary<string, uint> versionDict = new Dictionary<string, uint>();
			List<string> installFileNames = new List<string>();
			CRIFileInstaller.ParseCRIFileVersionDB(criFileVersionDB, isFirstDL, out versionDict, out installFileNames, null);
			this.Install(versionDict, installFileNames);
		}

		// Token: 0x06001F97 RID: 8087 RVA: 0x000AB8FC File Offset: 0x000A9CFC
		public void Install(Dictionary<string, uint> versionDict, List<string> installFileNames)
		{
			if (this.m_IsInstalling)
			{
				return;
			}
			this.m_IsInstalling = true;
			this.m_IsError = false;
			if (versionDict != null && installFileNames != null)
			{
				this.m_VersionDict = versionDict;
				this.m_InstallFileNames = installFileNames;
				this.m_FileCount = 0;
				this.m_FileCountMax = this.m_InstallFileNames.Count;
				this.m_InstallingList = new List<CRIFileInstaller.InstallData>();
			}
		}

		// Token: 0x06001F98 RID: 8088 RVA: 0x000AB960 File Offset: 0x000A9D60
		public void RetryInstall()
		{
			if (this.m_IsError)
			{
				for (int i = 0; i < this.m_ErrorFileNames.Count; i++)
				{
					Debug.LogFormat("CRIFileInstaller.RetryInstall() >>> [{0}]{1}", new object[]
					{
						i,
						this.m_ErrorFileNames[i]
					});
					this.m_InstallFileNames.Add(this.m_ErrorFileNames[i]);
				}
				this.m_ErrorFileNames.Clear();
				this.m_IsInstalling = true;
				this.m_IsError = false;
			}
		}

		// Token: 0x06001F99 RID: 8089 RVA: 0x000AB9EC File Offset: 0x000A9DEC
		public bool CheckVersionUpdate(CRIFileVersionDB criFileVersionDB, string fileName)
		{
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
			Dictionary<string, uint> dictionary = new Dictionary<string, uint>();
			List<string> list = new List<string>();
			CRIFileInstaller.ParseCRIFileVersionDB(criFileVersionDB, false, out dictionary, out list, fileNameWithoutExtension);
			if (dictionary.Count > 0 && list.Count > 0)
			{
				uint version = 0U;
				if (!dictionary.TryGetValue(fileNameWithoutExtension, out version))
				{
					return false;
				}
				if (!this.IsExist(fileName, version))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001F9A RID: 8090 RVA: 0x000ABA58 File Offset: 0x000A9E58
		private CriFsInstallRequest InstallProcess(string fileName, out uint out_idealVer)
		{
			out_idealVer = 0U;
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
			uint num = 0U;
			if (!this.m_VersionDict.TryGetValue(fileNameWithoutExtension, out num))
			{
				return null;
			}
			out_idealVer = num;
			bool flag = this.IsExist(fileName, num);
			if (flag)
			{
				return null;
			}
			string srcPath = this.GetSrcPath(fileName);
			string destPath = this.GetDestPath(fileName);
			return CriFsUtility.WebInstall(srcPath, destPath, null);
		}

		// Token: 0x06001F9B RID: 8091 RVA: 0x000ABAB8 File Offset: 0x000A9EB8
		public void InstallAcf(CRIFileVersionDB criFileVersionDB)
		{
			string key = null;
			uint value = 0U;
			string acf = criFileVersionDB.GetAcf(out key, out value);
			if (string.IsNullOrEmpty(acf))
			{
				return;
			}
			this.Install(new Dictionary<string, uint>
			{
				{
					key,
					value
				}
			}, new List<string>
			{
				acf
			});
		}

		// Token: 0x06001F9C RID: 8092 RVA: 0x000ABB08 File Offset: 0x000A9F08
		public bool CleanCache()
		{
			try
			{
				DirectoryInfo dirInfo = new DirectoryInfo(CRIFileInstaller.InstallPath);
				this.CleanCacheProcess(dirInfo, string.Empty);
			}
			catch (Exception ex)
			{
				return false;
			}
			return true;
		}

		// Token: 0x06001F9D RID: 8093 RVA: 0x000ABB4C File Offset: 0x000A9F4C
		private void CleanCacheProcess(DirectoryInfo dirInfo, string path = "")
		{
			foreach (FileInfo fileInfo in dirInfo.GetFiles())
			{
				string fileName = Path.Combine(path, fileInfo.Name).Replace("\\", "/");
				this.DeleteVersion(fileName);
				fileInfo.Delete();
			}
			foreach (DirectoryInfo directoryInfo in dirInfo.GetDirectories())
			{
				this.CleanCacheProcess(directoryInfo, Path.Combine(path, directoryInfo.Name).Replace("\\", "/"));
			}
		}

		// Token: 0x06001F9E RID: 8094 RVA: 0x000ABBED File Offset: 0x000A9FED
		private string GetSrcPath(string fileName)
		{
			return this.m_URL + fileName;
		}

		// Token: 0x06001F9F RID: 8095 RVA: 0x000ABBFB File Offset: 0x000A9FFB
		private string GetDestPath(string fileName)
		{
			return Path.Combine(CRIFileInstaller.InstallPath, fileName).Replace("\\", "/");
		}

		// Token: 0x06001FA0 RID: 8096 RVA: 0x000ABC18 File Offset: 0x000AA018
		private void SetVersion(string fileName, string version)
		{
			string fileVersionKey = this.GetFileVersionKey(fileName);
			PreviewLabs.PlayerPrefs.SetString(fileVersionKey, version);
			PreviewLabs.PlayerPrefs.Flush();
		}

		// Token: 0x06001FA1 RID: 8097 RVA: 0x000ABC3C File Offset: 0x000AA03C
		private void DeleteVersion(string fileName)
		{
			string fileVersionKey = this.GetFileVersionKey(fileName);
			PreviewLabs.PlayerPrefs.DeleteKey(fileVersionKey);
			PreviewLabs.PlayerPrefs.Flush();
		}

		// Token: 0x06001FA2 RID: 8098 RVA: 0x000ABC5C File Offset: 0x000AA05C
		public bool IsExist(string fileName)
		{
			string destPath = this.GetDestPath(fileName);
			return File.Exists(destPath);
		}

		// Token: 0x06001FA3 RID: 8099 RVA: 0x000ABC78 File Offset: 0x000AA078
		private bool IsExist(string fileName, uint version)
		{
			string fileVersionKey = this.GetFileVersionKey(fileName);
			string @string = PreviewLabs.PlayerPrefs.GetString(fileVersionKey, "0");
			if (@string != version.ToString())
			{
				return false;
			}
			string destPath = this.GetDestPath(fileName);
			return File.Exists(destPath);
		}

		// Token: 0x06001FA4 RID: 8100 RVA: 0x000ABCCC File Offset: 0x000AA0CC
		private string GetFileVersionKey(string fileName)
		{
			return string.Format("cri_{0}", fileName);
		}

		// Token: 0x04002629 RID: 9769
		public static string InstallPath;

		// Token: 0x0400262A RID: 9770
		private const string PREFS_FORMAT = "cri_{0}";

		// Token: 0x0400262B RID: 9771
		private const string SAVE_DIR_NAME = "c.r";

		// Token: 0x0400262C RID: 9772
		private string m_URL;

		// Token: 0x0400262D RID: 9773
		private bool m_IsInstalling;

		// Token: 0x0400262E RID: 9774
		private bool m_IsError;

		// Token: 0x0400262F RID: 9775
		private int m_FileCount;

		// Token: 0x04002630 RID: 9776
		private int m_FileCountMax;

		// Token: 0x04002631 RID: 9777
		private Dictionary<string, uint> m_VersionDict;

		// Token: 0x04002632 RID: 9778
		private List<string> m_ErrorFileNames = new List<string>();

		// Token: 0x04002633 RID: 9779
		private List<string> m_InstallFileNames;

		// Token: 0x04002634 RID: 9780
		private List<CRIFileInstaller.InstallData> m_InstallingList;

		// Token: 0x04002635 RID: 9781
		private uint m_SimultaneouslyNum = 2U;

		// Token: 0x04002636 RID: 9782
		private List<int> m_RemoveKeys = new List<int>();

		// Token: 0x0200063F RID: 1599
		public class InstallData
		{
			// Token: 0x06001FA7 RID: 8103 RVA: 0x000ABCF0 File Offset: 0x000AA0F0
			public float GetProgress()
			{
				if (this.m_Request != null && this.m_Request.progress >= 0f)
				{
					return this.m_Request.progress;
				}
				return 0f;
			}

			// Token: 0x04002637 RID: 9783
			public string m_FileName;

			// Token: 0x04002638 RID: 9784
			public uint m_IdealVer;

			// Token: 0x04002639 RID: 9785
			public CriFsInstallRequest m_Request;
		}
	}
}
