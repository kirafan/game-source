﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000623 RID: 1571
	public class RoomPartsTweenColor : IRoomPartsAction
	{
		// Token: 0x06001E9B RID: 7835 RVA: 0x000A5C98 File Offset: 0x000A4098
		public RoomPartsTweenColor(GameObject prender, Color fincolor, Color foutcolor, float ftime)
		{
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Base = fincolor;
			this.m_Target = foutcolor;
			MsbHandler[] componentsInChildren = prender.GetComponentsInChildren<MsbHandler>();
			this.m_MsbHandle = new RoomPartsTweenColor.ModelHandle[componentsInChildren.Length];
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				this.m_MsbHandle[i].m_Handle = componentsInChildren[i];
				int num = this.m_MsbHandle[i].m_Num = componentsInChildren[i].GetMsbObjectHandlerNum();
				this.m_MsbHandle[i].m_Color = new Color[num];
				for (int j = 0; j < num; j++)
				{
					MsbObjectHandler msbObjectHandler = componentsInChildren[i].GetMsbObjectHandler(j);
					this.m_MsbHandle[i].m_Color[j] = msbObjectHandler.GetWork().m_MeshColor;
				}
			}
			this.m_Active = true;
		}

		// Token: 0x06001E9C RID: 7836 RVA: 0x000A5D8B File Offset: 0x000A418B
		public void ChangeColor(Color fin, Color fout)
		{
			this.m_Base = fin;
			this.m_Target = fout;
		}

		// Token: 0x06001E9D RID: 7837 RVA: 0x000A5D9B File Offset: 0x000A419B
		public void SetEndStep()
		{
			this.m_Step = 3;
		}

		// Token: 0x06001E9E RID: 7838 RVA: 0x000A5DA4 File Offset: 0x000A41A4
		public override bool PartsUpdate()
		{
			Color fcolor = Color.white;
			switch (this.m_Step)
			{
			case 0:
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Time = this.m_MaxTime - (this.m_Time - this.m_MaxTime);
					this.m_Step = 1;
				}
				break;
			case 1:
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					this.m_Time = -this.m_Time;
					this.m_Step = 2;
				}
				break;
			case 2:
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Time = this.m_MaxTime - (this.m_Time - this.m_MaxTime);
					this.m_Step = 1;
				}
				break;
			case 3:
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					this.m_Time = this.m_MaxTime;
					this.m_Active = false;
				}
				break;
			}
			if (this.m_Active)
			{
				float b = this.m_Time / this.m_MaxTime;
				fcolor = (this.m_Target - this.m_Base) * b + this.m_Base;
				this.UpModelColor(fcolor);
			}
			else
			{
				this.BackModelColor();
			}
			return this.m_Active;
		}

		// Token: 0x06001E9F RID: 7839 RVA: 0x000A5F3C File Offset: 0x000A433C
		private void UpModelColor(Color fcolor)
		{
			int num = this.m_MsbHandle.Length;
			for (int i = 0; i < num; i++)
			{
				int num2 = this.m_MsbHandle[i].m_Num;
				for (int j = 0; j < num2; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHandle[i].m_Handle.GetMsbObjectHandler(j);
					msbObjectHandler.GetWork().m_MeshColor = fcolor * this.m_MsbHandle[i].m_Color[j];
				}
			}
		}

		// Token: 0x06001EA0 RID: 7840 RVA: 0x000A5FD0 File Offset: 0x000A43D0
		private void BackModelColor()
		{
			int num = this.m_MsbHandle.Length;
			for (int i = 0; i < num; i++)
			{
				int num2 = this.m_MsbHandle[i].m_Num;
				for (int j = 0; j < num2; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHandle[i].m_Handle.GetMsbObjectHandler(j);
					msbObjectHandler.GetWork().m_MeshColor = this.m_MsbHandle[i].m_Color[j];
				}
			}
		}

		// Token: 0x04002553 RID: 9555
		private RoomPartsTweenColor.ModelHandle[] m_MsbHandle;

		// Token: 0x04002554 RID: 9556
		private Color m_Base;

		// Token: 0x04002555 RID: 9557
		private Color m_Target;

		// Token: 0x04002556 RID: 9558
		private byte m_Step;

		// Token: 0x04002557 RID: 9559
		private float m_Time;

		// Token: 0x04002558 RID: 9560
		private float m_MaxTime;

		// Token: 0x02000624 RID: 1572
		private struct ModelHandle
		{
			// Token: 0x04002559 RID: 9561
			public MsbHandler m_Handle;

			// Token: 0x0400255A RID: 9562
			public int m_Num;

			// Token: 0x0400255B RID: 9563
			public Color[] m_Color;
		}
	}
}
