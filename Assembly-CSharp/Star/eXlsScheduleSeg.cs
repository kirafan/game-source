﻿using System;

namespace Star
{
	// Token: 0x02000302 RID: 770
	public enum eXlsScheduleSeg
	{
		// Token: 0x040015EB RID: 5611
		Non,
		// Token: 0x040015EC RID: 5612
		PopupChange,
		// Token: 0x040015ED RID: 5613
		EventWakeUp,
		// Token: 0x040015EE RID: 5614
		IfComp,
		// Token: 0x040015EF RID: 5615
		JumpKey,
		// Token: 0x040015F0 RID: 5616
		IFCloseness,
		// Token: 0x040015F1 RID: 5617
		IFHaveChara,
		// Token: 0x040015F2 RID: 5618
		IFHaveNamed
	}
}
