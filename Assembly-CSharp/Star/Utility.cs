﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Meige;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020004E9 RID: 1257
	public static class Utility
	{
		// Token: 0x0600190E RID: 6414 RVA: 0x0008235E File Offset: 0x0008075E
		public static string GetiOSVersion()
		{
			return null;
		}

		// Token: 0x0600190F RID: 6415 RVA: 0x00082364 File Offset: 0x00080764
		public static string ConvertKrrURL(string url)
		{
			return url;
		}

		// Token: 0x06001910 RID: 6416 RVA: 0x00082374 File Offset: 0x00080774
		[Conditional("UNITY_EDITOR")]
		public static void ReassignShader(Material mat)
		{
		}

		// Token: 0x06001911 RID: 6417 RVA: 0x00082378 File Offset: 0x00080778
		[Conditional("UNITY_EDITOR")]
		public static void ReassignShader(GameObject go)
		{
			Renderer[] array = go.GetComponents<Renderer>();
			foreach (Renderer renderer in array)
			{
				Material[] materials = renderer.materials;
				foreach (Material material in materials)
				{
				}
			}
			array = go.GetComponentsInChildren<Renderer>(true);
			foreach (Renderer renderer2 in array)
			{
				Material[] materials2 = renderer2.materials;
				foreach (Material material2 in materials2)
				{
				}
			}
			MeigeParticleEmitter[] componentsInChildren = go.GetComponentsInChildren<MeigeParticleEmitter>(true);
			foreach (MeigeParticleEmitter meigeParticleEmitter in componentsInChildren)
			{
			}
		}

		// Token: 0x06001912 RID: 6418 RVA: 0x0008245C File Offset: 0x0008085C
		[Conditional("UNITY_EDITOR")]
		public static void ReassignImageShader(GameObject go)
		{
			Image[] array = go.GetComponents<Image>();
			foreach (Image image in array)
			{
			}
			array = go.GetComponentsInChildren<Image>(true);
			foreach (Image image2 in array)
			{
			}
		}

		// Token: 0x06001913 RID: 6419 RVA: 0x000824B8 File Offset: 0x000808B8
		public static void ReassignSharedShader(GameObject go)
		{
			Renderer[] array = go.GetComponents<Renderer>();
			foreach (Renderer renderer in array)
			{
				Material[] sharedMaterials = renderer.sharedMaterials;
				foreach (Material material in sharedMaterials)
				{
				}
			}
			array = go.GetComponentsInChildren<Renderer>(true);
			foreach (Renderer renderer2 in array)
			{
				Material[] sharedMaterials2 = renderer2.sharedMaterials;
				foreach (Material material2 in sharedMaterials2)
				{
				}
			}
			MeigeParticleEmitter[] componentsInChildren = go.GetComponentsInChildren<MeigeParticleEmitter>();
			foreach (MeigeParticleEmitter meigeParticleEmitter in componentsInChildren)
			{
			}
		}

		// Token: 0x06001914 RID: 6420 RVA: 0x00082598 File Offset: 0x00080998
		public static bool CheckTapUI(GameObject go)
		{
			Transform transform = go.transform;
			while (transform.GetComponent<Canvas>() == null)
			{
				transform = transform.parent;
				if (transform == null)
				{
					return true;
				}
			}
			Canvas component = transform.GetComponent<Canvas>();
			if (component == null)
			{
				return true;
			}
			Vector2 position = RectTransformUtility.WorldToScreenPoint(component.worldCamera, go.GetComponent<RectTransform>().position);
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = position;
			List<RaycastResult> list = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list);
			if (list == null || list.Count == 0)
			{
				return true;
			}
			GameObject gameObject = list[0].gameObject;
			List<GameObject> allGameObject = go.GetAllGameObject();
			for (int i = 0; i < allGameObject.Count; i++)
			{
				if (allGameObject[i] == gameObject)
				{
					return true;
				}
			}
			return go == gameObject;
		}

		// Token: 0x06001915 RID: 6421 RVA: 0x00082694 File Offset: 0x00080A94
		public static List<GameObject> GetAllGameObject(this GameObject go)
		{
			List<GameObject> result = new List<GameObject>();
			Utility.GetChildrenGameObjects(go, ref result);
			return result;
		}

		// Token: 0x06001916 RID: 6422 RVA: 0x000826B0 File Offset: 0x00080AB0
		public static void GetChildrenGameObjects(GameObject go, ref List<GameObject> allChildren)
		{
			Transform componentInChildren = go.GetComponentInChildren<Transform>();
			if (componentInChildren.childCount == 0)
			{
				return;
			}
			IEnumerator enumerator = componentInChildren.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					allChildren.Add(transform.gameObject);
					Utility.GetChildrenGameObjects(transform.gameObject, ref allChildren);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
