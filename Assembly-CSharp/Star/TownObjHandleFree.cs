﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006E6 RID: 1766
	public class TownObjHandleFree : ITownObjectHandler
	{
		// Token: 0x060022F1 RID: 8945 RVA: 0x000BCA23 File Offset: 0x000BAE23
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			this.m_State = TownObjHandleFree.eState.Init;
		}

		// Token: 0x060022F2 RID: 8946 RVA: 0x000BCA3C File Offset: 0x000BAE3C
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleFree.eState.Init:
				this.m_State = TownObjHandleFree.eState.Main;
				this.m_IsAvailable = true;
				this.ModelSetUp();
				break;
			case TownObjHandleFree.eState.Sleep:
				this.m_State = TownObjHandleFree.eState.Main;
				break;
			case TownObjHandleFree.eState.WakeUp:
				this.m_State = TownObjHandleFree.eState.Main;
				this.ChangeColliderActive(true);
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x060022F3 RID: 8947 RVA: 0x000BCAC5 File Offset: 0x000BAEC5
		private void OnDestroy()
		{
			this.m_HitNode = null;
			this.m_HitModel = null;
		}

		// Token: 0x060022F4 RID: 8948 RVA: 0x000BCAD8 File Offset: 0x000BAED8
		public override bool RecvEvent(ITownHandleAction pact)
		{
			switch (pact.m_Type)
			{
			case eTownEventAct.OpenSelectUI:
			{
				TownPartsModelMaker paction = new TownPartsModelMaker(this.m_Builder, this.m_HitNode.transform, this.m_HitModel, this.m_LayerID + 10);
				this.m_Action.EntryAction(paction, 0);
				break;
			}
			case eTownEventAct.CloseSelectUI:
			{
				ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsModelMaker));
				if (action != null)
				{
					TownPartsModelMaker townPartsModelMaker = (TownPartsModelMaker)action;
					townPartsModelMaker.SetEndMark();
				}
				break;
			}
			case eTownEventAct.StepIn:
				this.m_State = TownObjHandleFree.eState.WakeUp;
				base.enabled = true;
				break;
			case eTownEventAct.StepOut:
				this.m_State = TownObjHandleFree.eState.Sleep;
				this.ChangeColliderActive(false);
				break;
			}
			return false;
		}

		// Token: 0x060022F5 RID: 8949 RVA: 0x000BCBB4 File Offset: 0x000BAFB4
		private void ChangeColliderActive(bool factive)
		{
			TownHitNodeState component = base.gameObject.GetComponent<TownHitNodeState>();
			if (component != null)
			{
				component.SetHitActive(eTownOption.HitContent, factive);
				if (factive)
				{
					base.LinkHitToNode(this.m_HitModel);
				}
			}
		}

		// Token: 0x060022F6 RID: 8950 RVA: 0x000BCBF3 File Offset: 0x000BAFF3
		public void SetBuildUpCallBack(TownObjHandleFree.CallbackBuildUp pcallback, TownBuildPakage ppakage)
		{
			this.m_CallbackUp = pcallback;
			this.m_CallBackLine = ppakage;
		}

		// Token: 0x060022F7 RID: 8951 RVA: 0x000BCC04 File Offset: 0x000BB004
		private void ModelSetUp()
		{
			if (this.m_CallbackUp != null)
			{
				this.m_CallbackUp(this.m_CallBackLine);
				this.m_CallBackLine = null;
				this.m_CallbackUp = null;
			}
			TownHitNodeState component = base.gameObject.GetComponent<TownHitNodeState>();
			if (component != null)
			{
				TownHitNodeState.HitState hitType = component.GetHitType(eTownOption.HitContent);
				if (hitType != null)
				{
					this.m_HitNode = hitType.m_Node;
					this.m_HitModel = hitType.m_Model;
					base.LinkHitToNode(this.m_HitModel);
				}
				component.SetHitActive(eTownOption.HitContent, true);
				component.SetHitActive(eTownOption.HitBuf, false);
			}
		}

		// Token: 0x060022F8 RID: 8952 RVA: 0x000BCC98 File Offset: 0x000BB098
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				pbuilder.StackEventQue(new TownComUI(true)
				{
					m_SelectHandle = this,
					m_OpenUI = eTownUICategory.FreeArea,
					m_BuildPoint = this.m_BuildAccessID
				});
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
			}
			return true;
		}

		// Token: 0x040029CD RID: 10701
		public TownObjHandleFree.eState m_State;

		// Token: 0x040029CE RID: 10702
		public TownObjHandleFree.CallbackBuildUp m_CallbackUp;

		// Token: 0x040029CF RID: 10703
		public TownBuildPakage m_CallBackLine;

		// Token: 0x040029D0 RID: 10704
		public GameObject m_HitNode;

		// Token: 0x040029D1 RID: 10705
		public GameObject m_HitModel;

		// Token: 0x020006E7 RID: 1767
		public enum eState
		{
			// Token: 0x040029D3 RID: 10707
			Init,
			// Token: 0x040029D4 RID: 10708
			Main,
			// Token: 0x040029D5 RID: 10709
			Sleep,
			// Token: 0x040029D6 RID: 10710
			WakeUp,
			// Token: 0x040029D7 RID: 10711
			EndStart,
			// Token: 0x040029D8 RID: 10712
			End
		}

		// Token: 0x020006E8 RID: 1768
		// (Invoke) Token: 0x060022FA RID: 8954
		public delegate void CallbackBuildUp(TownBuildPakage ppakage);
	}
}
