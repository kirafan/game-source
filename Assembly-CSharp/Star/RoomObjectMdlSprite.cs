﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005D5 RID: 1493
	public class RoomObjectMdlSprite : IRoomObjectControll
	{
		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06001D3E RID: 7486 RVA: 0x0009DA9E File Offset: 0x0009BE9E
		public int SubKeyID
		{
			get
			{
				return this.m_SubKeyID;
			}
		}

		// Token: 0x06001D3F RID: 7487 RVA: 0x0009DAA8 File Offset: 0x0009BEA8
		public void SetupSimple(eRoomObjectCategory category, int objID, int fsubkey, int flayerid, int frenderlayer, bool finit)
		{
			this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingOrderMdlSprite);
			this.m_Category = category;
			this.m_ObjID = objID;
			this.m_SubKeyID = fsubkey;
			this.m_LayerID = flayerid;
			this.m_RenderLayerID = frenderlayer;
			this.m_DefaultSizeX = 1;
			this.m_DefaultSizeY = 1;
			this.m_BlockData = new RoomBlockData(0f, 0f, 1, 1);
			this.m_BlockData.SetLinkObject(this);
			base.SetDir(CharacterDefine.eDir.L);
			this.m_RoomObjResource = new RoomObjectResMdlSprite();
			this.m_RoomObjResource.Setup(this);
			this.m_Action = new RoomPartsActionPakage();
		}

		// Token: 0x06001D40 RID: 7488 RVA: 0x0009DB46 File Offset: 0x0009BF46
		public override void UpdateObj()
		{
			this.m_Action.PakageUpdate();
		}

		// Token: 0x06001D41 RID: 7489 RVA: 0x0009DB54 File Offset: 0x0009BF54
		public override void Destroy()
		{
			this.ModelDestroy();
			if (this.m_Obj != null)
			{
				ObjectResourceManager.ObjectDestroy(this.m_Obj, true);
				this.m_Obj = null;
			}
			this.m_Render = null;
			if (this.m_DefaultSortID != null)
			{
				this.m_DefaultSortID = null;
			}
			base.Destroy();
		}

		// Token: 0x06001D42 RID: 7490 RVA: 0x0009DBAC File Offset: 0x0009BFAC
		public override void SetBaseColor(Color fcolor)
		{
			this.m_BaseColor = fcolor;
			if (this.m_Render != null)
			{
				int num = this.m_Render.Length;
				for (int i = 0; i < num; i++)
				{
					this.m_Render[i].material.SetColor("_Color", fcolor);
				}
			}
		}

		// Token: 0x06001D43 RID: 7491 RVA: 0x0009DC00 File Offset: 0x0009C000
		public void AttachSprite(GameObject sprite)
		{
			if (this.m_Render == null)
			{
				this.m_Obj = sprite;
				this.m_Render = sprite.GetComponents<Renderer>();
				this.m_DefaultSortID = new short[this.m_Render.Length];
				for (int i = 0; i < this.m_Render.Length; i++)
				{
					this.m_Render[i].sortingOrder = this.m_LayerID;
				}
				this.m_Obj.transform.SetParent(this.m_OwnerTrs, false);
				base.SetLayer(this.m_RenderLayerID);
				this.SetBaseColor(this.m_BaseColor);
			}
		}

		// Token: 0x06001D44 RID: 7492 RVA: 0x0009DC9C File Offset: 0x0009C09C
		public void SortingOrderMdlSprite(float flayerpos, int sortingOrder)
		{
			Vector3 localPosition = this.m_OwnerTrs.localPosition;
			localPosition.z = flayerpos;
			this.m_OwnerTrs.localPosition = localPosition;
			if (this.m_Render != null)
			{
				for (int i = 0; i < this.m_Render.Length; i++)
				{
					this.m_Render[i].sortingOrder = sortingOrder + (int)this.m_DefaultSortID[i];
				}
			}
		}

		// Token: 0x06001D45 RID: 7493 RVA: 0x0009DD04 File Offset: 0x0009C104
		public void SetFadeIn(float ftime)
		{
			this.m_Action.EntryAction(new RoomPartsSpMdlFade(this.m_Obj, new Color(1f, 1f, 1f, 0f) * this.m_BaseColor, Color.white * this.m_BaseColor, ftime), 0U);
		}

		// Token: 0x06001D46 RID: 7494 RVA: 0x0009DD60 File Offset: 0x0009C160
		private void ModelDestroy()
		{
			Renderer[] componentsInChildren = base.GetComponentsInChildren<Renderer>();
			if (componentsInChildren != null)
			{
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					if (componentsInChildren[i].materials != null)
					{
						for (int j = 0; j < componentsInChildren[i].materials.Length; j++)
						{
							if (componentsInChildren[i].materials[j] != null)
							{
								UnityEngine.Object.DestroyImmediate(componentsInChildren[i].materials[j]);
								componentsInChildren[i].materials[j] = null;
							}
						}
					}
				}
			}
		}

		// Token: 0x040023C0 RID: 9152
		protected int m_SubKeyID;

		// Token: 0x040023C1 RID: 9153
		private Renderer[] m_Render;

		// Token: 0x040023C2 RID: 9154
		private GameObject m_Obj;

		// Token: 0x040023C3 RID: 9155
		protected int m_SelectingMode;

		// Token: 0x040023C4 RID: 9156
		private short[] m_DefaultSortID;

		// Token: 0x040023C5 RID: 9157
		private int m_LayerID;

		// Token: 0x040023C6 RID: 9158
		private int m_RenderLayerID;

		// Token: 0x040023C7 RID: 9159
		private Color m_BaseColor = Color.white;

		// Token: 0x040023C8 RID: 9160
		private RoomPartsActionPakage m_Action;
	}
}
