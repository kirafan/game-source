﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006BB RID: 1723
	public class TownPartsChangeArea : ITownPartsAction
	{
		// Token: 0x06002251 RID: 8785 RVA: 0x000B678C File Offset: 0x000B4B8C
		public TownPartsChangeArea(TownBuilder pbuilder, ITownObjectHandler phandle, int fbackbuildpoint, int fobjid)
		{
			this.m_Builder = pbuilder;
			this.m_BackBuildPoint = fbackbuildpoint;
			this.m_Handle = phandle;
			this.m_NextObjectID = fobjid;
			this.m_Active = true;
			this.m_Step = TownPartsChangeArea.eStep.FadeOutIn;
		}

		// Token: 0x06002252 RID: 8786 RVA: 0x000B67C0 File Offset: 0x000B4BC0
		public override bool PartsUpdate()
		{
			TownPartsChangeArea.eStep step = this.m_Step;
			if (step != TownPartsChangeArea.eStep.FadeOutIn)
			{
				if (step != TownPartsChangeArea.eStep.FadeOut)
				{
					if (step == TownPartsChangeArea.eStep.ModelChange)
					{
						this.m_Active = false;
					}
				}
				else
				{
					this.m_Time += Time.deltaTime;
					if (this.m_Time >= this.m_MaxTime)
					{
						this.m_Step = TownPartsChangeArea.eStep.ModelChange;
						TownHandleActionModelObj pact = new TownHandleActionModelObj(this.m_NextObjectID, this.m_Handle.GetManageID());
						this.m_Handle.RecvEvent(pact);
					}
				}
			}
			else
			{
				TownBuildLinkPoint buildPointTreeNode = this.m_Builder.GetBuildTree().GetBuildPointTreeNode(this.m_BackBuildPoint);
				if (buildPointTreeNode != null)
				{
					buildPointTreeNode.ChangeManageID(-1L);
				}
				this.m_Step = TownPartsChangeArea.eStep.FadeOut;
				this.m_MaxTime = 0.1f;
				this.m_Time = 0f;
			}
			return this.m_Active;
		}

		// Token: 0x040028ED RID: 10477
		private TownBuilder m_Builder;

		// Token: 0x040028EE RID: 10478
		private float m_MaxTime;

		// Token: 0x040028EF RID: 10479
		private float m_Time;

		// Token: 0x040028F0 RID: 10480
		private TownPartsChangeArea.eStep m_Step;

		// Token: 0x040028F1 RID: 10481
		private ITownObjectHandler m_Handle;

		// Token: 0x040028F2 RID: 10482
		private int m_NextObjectID;

		// Token: 0x040028F3 RID: 10483
		private int m_BackBuildPoint;

		// Token: 0x020006BC RID: 1724
		public enum eStep
		{
			// Token: 0x040028F5 RID: 10485
			FadeOutIn,
			// Token: 0x040028F6 RID: 10486
			FadeOut,
			// Token: 0x040028F7 RID: 10487
			ModelChange
		}
	}
}
