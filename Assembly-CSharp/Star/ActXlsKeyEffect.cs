﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200056F RID: 1391
	public class ActXlsKeyEffect : ActXlsKeyBase
	{
		// Token: 0x06001B1B RID: 6939 RVA: 0x0008F7E0 File Offset: 0x0008DBE0
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_EffectNo);
			pio.Float(ref this.m_OffsetPos.x);
			pio.Float(ref this.m_OffsetPos.y);
			pio.Float(ref this.m_OffsetPos.z);
			pio.Float(ref this.m_Size);
		}

		// Token: 0x04002208 RID: 8712
		public int m_EffectNo;

		// Token: 0x04002209 RID: 8713
		public Vector3 m_OffsetPos;

		// Token: 0x0400220A RID: 8714
		public float m_Size;
	}
}
