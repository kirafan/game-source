﻿using System;

namespace Star
{
	// Token: 0x02000218 RID: 536
	[Serializable]
	public struct SoundSeListDB_Param
	{
		// Token: 0x04000DF4 RID: 3572
		public int m_ID;

		// Token: 0x04000DF5 RID: 3573
		public string m_CueName;

		// Token: 0x04000DF6 RID: 3574
		public int m_CueSheetID;
	}
}
