﻿using System;

namespace Star
{
	// Token: 0x020002B6 RID: 694
	public class EvolutionResultBeforeAfterPairAfterPartCharacterWrapper : EvolutionResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CFD RID: 3325 RVA: 0x000492AF File Offset: 0x000476AF
		public EvolutionResultBeforeAfterPairAfterPartCharacterWrapper(EditMain.EvolutionResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}

		// Token: 0x06000CFE RID: 3326 RVA: 0x000492B9 File Offset: 0x000476B9
		public override int GetCharaId()
		{
			if (this.m_Result == null)
			{
				return base.GetCharaId();
			}
			return this.m_Result.m_DestCharaID;
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x000492D8 File Offset: 0x000476D8
		protected override EditUtility.OutputCharaParam ConvertResultDataToCharaParam(EditMain.EvolutionResultData result)
		{
			return EditUtility.CalcCharaParamIgnoreTownBuff(this.m_Result.m_DestCharaID, this.data.Param.Lv, this.data.Param.Exp, base.GetUserNamedData(), null);
		}
	}
}
