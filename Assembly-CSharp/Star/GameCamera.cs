﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000134 RID: 308
	public class GameCamera : MonoBehaviour
	{
		// Token: 0x060007FB RID: 2043 RVA: 0x00033B74 File Offset: 0x00031F74
		private void Awake()
		{
			if (this.m_PinchEventListener != null)
			{
				this.m_PinchEventListener.OnPinch += this.OnPinch;
			}
			this.m_ThresholdMovePixel = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * this.m_ThresholdMoveRatio;
		}

		// Token: 0x060007FC RID: 2044 RVA: 0x00033BD5 File Offset: 0x00031FD5
		private void Update()
		{
			this.UpdateMove();
		}

		// Token: 0x060007FD RID: 2045 RVA: 0x00033BDD File Offset: 0x00031FDD
		public void SetIsControllable(bool flg)
		{
			this.m_IsControllable = flg;
			if (!this.m_IsControllable)
			{
				this.IsDragging = false;
				this.m_IsPinching = false;
			}
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x00033C00 File Offset: 0x00032000
		private void SetZoom(float size)
		{
			if (float.IsInfinity(size) || float.IsNaN(size))
			{
				return;
			}
			float orthographicSize = Mathf.Clamp(size, this.m_SizeMin, this.m_SizeMax);
			this.m_MoveRange = this.CalcMoveRange(orthographicSize);
			this.m_Camera.orthographicSize = orthographicSize;
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x00033DF4 File Offset: 0x000321F4
		private void OnPinch(PinchEventListener listener)
		{
			if (!this.m_IsControllable)
			{
				return;
			}
			if (listener.IsPinching)
			{
				if (!this.m_IsPinching)
				{
					this.m_IsPinching = true;
					this.m_SizeOnStartPinch = this.m_Camera.orthographicSize;
				}
				this.SetZoom(this.m_SizeOnStartPinch / listener.PinchRatio);
			}
			else
			{
				this.m_IsPinching = false;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000800 RID: 2048 RVA: 0x00033E5A File Offset: 0x0003225A
		// (set) Token: 0x06000801 RID: 2049 RVA: 0x00033E65 File Offset: 0x00032265
		public bool IsDragging
		{
			get
			{
				return this.m_DragState == GameCamera.eDragState.Dragging;
			}
			set
			{
				if (value)
				{
					this.m_DragState = GameCamera.eDragState.Dragging;
				}
				else
				{
					this.m_DragState = GameCamera.eDragState.None;
				}
			}
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x00033E80 File Offset: 0x00032280
		public void SetMoveRange(float rangeW, float rangeH)
		{
			this.m_RangeW = rangeW;
			this.m_RangeH = rangeH;
			this.UpdateMoveRange();
		}

		// Token: 0x06000803 RID: 2051 RVA: 0x00033E96 File Offset: 0x00032296
		private void UpdateMoveRange()
		{
			this.m_MoveRange = this.CalcMoveRange(this.m_Camera.orthographicSize);
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x00033EB0 File Offset: 0x000322B0
		private Rect CalcMoveRange(float orthographicSize)
		{
			float num = orthographicSize * this.m_Camera.aspect;
			float num2 = num + num;
			float num3 = orthographicSize + orthographicSize;
			float num4 = this.m_RangeW - num2;
			if (num4 < 0f)
			{
				num4 = 0f;
			}
			float num5 = this.m_RangeH - num3;
			if (num5 < 0f)
			{
				num5 = 0f;
			}
			return new Rect(-num4 / 2f, -num5 / 2f, num4, num5);
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x00033F2C File Offset: 0x0003232C
		private void UpdateMove()
		{
			if (this.m_IsControllable)
			{
				int availableTouchCnt = InputTouch.Inst.GetAvailableTouchCnt();
				GameCamera.eDragState dragState = this.m_DragState;
				if (dragState != GameCamera.eDragState.None)
				{
					if (dragState != GameCamera.eDragState.Ready)
					{
						if (dragState == GameCamera.eDragState.Dragging)
						{
							bool flag = true;
							if (availableTouchCnt == 1 && !this.m_IsPinching)
							{
								InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
								if (info.m_bAvailable && (info.m_TouchPhase == TouchPhase.Moved || info.m_TouchPhase == TouchPhase.Stationary))
								{
									flag = false;
									this.m_vMove = info.m_DiffFromPrev * info.m_DiffFromPrev.z;
									this.m_vMove.x = this.m_vMove.x * (this.m_Camera.orthographicSize * 2f / (float)Screen.width * this.m_Camera.aspect);
									this.m_vMove.y = this.m_vMove.y * (this.m_Camera.orthographicSize * 2f / (float)Screen.height);
									this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
								}
							}
							if (flag)
							{
								this.m_DragState = GameCamera.eDragState.None;
							}
						}
					}
					else
					{
						bool flag2 = true;
						if (availableTouchCnt == 1 && !this.m_IsPinching)
						{
							InputTouch.TOUCH_INFO info2 = InputTouch.Inst.GetInfo(0);
							if (info2.m_bAvailable && (info2.m_TouchPhase == TouchPhase.Moved || info2.m_TouchPhase == TouchPhase.Stationary))
							{
								flag2 = false;
								if (info2.m_DiffFromStart.z >= this.m_ThresholdMovePixel)
								{
									this.m_DragState = GameCamera.eDragState.Dragging;
								}
							}
						}
						if (flag2)
						{
							this.m_DragState = GameCamera.eDragState.None;
						}
					}
				}
				else if (availableTouchCnt == 1 && !this.m_IsPinching)
				{
					InputTouch.TOUCH_INFO info3 = InputTouch.Inst.GetInfo(0);
					if (info3.m_bAvailable && info3.m_TouchPhase == TouchPhase.Began && !UIUtility.CheckPointOverUI(info3.m_StartPos))
					{
						this.m_DragState = GameCamera.eDragState.Ready;
					}
				}
			}
			if (!this.IsDragging && !this.m_IsPinching)
			{
				this.m_vMove *= 1f - this.m_Inertia;
				if (this.m_vMove.sqrMagnitude > 0f)
				{
					this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
				}
			}
			if (this.m_IsPinching)
			{
				this.m_vMove = Vector2.zero;
			}
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x040007C4 RID: 1988
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040007C5 RID: 1989
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x040007C6 RID: 1990
		[SerializeField]
		private PinchEventListener m_PinchEventListener;

		// Token: 0x040007C7 RID: 1991
		[SerializeField]
		private float m_ThresholdMoveRatio = 0.005f;

		// Token: 0x040007C8 RID: 1992
		private float m_ThresholdMovePixel;

		// Token: 0x040007C9 RID: 1993
		private bool m_IsControllable = true;

		// Token: 0x040007CA RID: 1994
		[SerializeField]
		private float m_SizeMin = 3f;

		// Token: 0x040007CB RID: 1995
		[SerializeField]
		private float m_SizeMax = 5.5f;

		// Token: 0x040007CC RID: 1996
		private float m_SizeOnStartPinch = 1f;

		// Token: 0x040007CD RID: 1997
		private bool m_IsPinching;

		// Token: 0x040007CE RID: 1998
		[SerializeField]
		private float m_Inertia = 0.04f;

		// Token: 0x040007CF RID: 1999
		private Rect m_MoveRange;

		// Token: 0x040007D0 RID: 2000
		private float m_RangeW = 10000f;

		// Token: 0x040007D1 RID: 2001
		private float m_RangeH = 10000f;

		// Token: 0x040007D2 RID: 2002
		private Vector2 m_vMove = Vector2.zero;

		// Token: 0x040007D3 RID: 2003
		private GameCamera.eDragState m_DragState = GameCamera.eDragState.None;

		// Token: 0x02000135 RID: 309
		private enum eDragState
		{
			// Token: 0x040007D5 RID: 2005
			None = -1,
			// Token: 0x040007D6 RID: 2006
			Ready,
			// Token: 0x040007D7 RID: 2007
			Dragging
		}
	}
}
