﻿using System;

namespace Star
{
	// Token: 0x02000262 RID: 610
	[Serializable]
	public struct WeaponListDB_Param
	{
		// Token: 0x040013C4 RID: 5060
		public int m_ID;

		// Token: 0x040013C5 RID: 5061
		public string m_WeaponName;

		// Token: 0x040013C6 RID: 5062
		public int m_Rare;

		// Token: 0x040013C7 RID: 5063
		public int m_ClassType;

		// Token: 0x040013C8 RID: 5064
		public int m_Cost;

		// Token: 0x040013C9 RID: 5065
		public int m_ResourceID_L;

		// Token: 0x040013CA RID: 5066
		public int m_ResourceID_R;

		// Token: 0x040013CB RID: 5067
		public sbyte m_ExpTableID;

		// Token: 0x040013CC RID: 5068
		public int m_LimitLv;

		// Token: 0x040013CD RID: 5069
		public int m_InitAtk;

		// Token: 0x040013CE RID: 5070
		public int m_InitMgc;

		// Token: 0x040013CF RID: 5071
		public int m_InitDef;

		// Token: 0x040013D0 RID: 5072
		public int m_InitMDef;

		// Token: 0x040013D1 RID: 5073
		public int m_MaxAtk;

		// Token: 0x040013D2 RID: 5074
		public int m_MaxMgc;

		// Token: 0x040013D3 RID: 5075
		public int m_MaxDef;

		// Token: 0x040013D4 RID: 5076
		public int m_MaxMDef;

		// Token: 0x040013D5 RID: 5077
		public int m_SkillID;

		// Token: 0x040013D6 RID: 5078
		public sbyte m_SkillExpTableID;

		// Token: 0x040013D7 RID: 5079
		public int m_SaleAmount;

		// Token: 0x040013D8 RID: 5080
		public int[] m_UpgradeBonusMaterialItemIDs;
	}
}
