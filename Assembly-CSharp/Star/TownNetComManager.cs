﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000694 RID: 1684
	public class TownNetComManager : IFldNetComManager
	{
		// Token: 0x060021C6 RID: 8646 RVA: 0x000B3B98 File Offset: 0x000B1F98
		private void SetNextStep(TownNetComManager.eComStep fnextup)
		{
			bool flag = true;
			while (flag)
			{
				switch (fnextup)
				{
				case TownNetComManager.eComStep.ListIn:
					if (!FldComDefaultDB.IsSetUp())
					{
						base.AddModule(new FldComDefaultDB());
						flag = false;
					}
					else
					{
						fnextup = TownNetComManager.eComStep.DefObjSetUp;
					}
					continue;
				case TownNetComManager.eComStep.DefObjSetUp:
					if (!UserTownObjectData.IsSetUpConnect())
					{
						base.AddModule(new TownComObjSetUp());
						flag = false;
					}
					else
					{
						fnextup = TownNetComManager.eComStep.DefSetUp;
					}
					continue;
				case TownNetComManager.eComStep.DefSetUp:
					if (!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.IsSetUpConnect())
					{
						base.AddModule(new TownComSetUp());
						flag = false;
					}
					else
					{
						fnextup = TownNetComManager.eComStep.Update;
					}
					continue;
				}
				flag = false;
			}
			this.m_Step = fnextup;
		}

		// Token: 0x060021C7 RID: 8647 RVA: 0x000B3C56 File Offset: 0x000B2056
		public static void SetUpManager(GameObject ptarget, bool resetnet)
		{
			TownNetComManager.Inst = ptarget.AddComponent<TownNetComManager>();
			TownNetComManager.Inst.AwakeCom();
			TownNetComManager.Inst.SetNextStep(TownNetComManager.eComStep.ListIn);
		}

		// Token: 0x060021C8 RID: 8648 RVA: 0x000B3C78 File Offset: 0x000B2078
		private void OnDestroy()
		{
			if (TownNetComManager.Inst != null)
			{
				TownNetComManager.Inst.DestroyCom();
				TownNetComManager.Inst = null;
			}
		}

		// Token: 0x060021C9 RID: 8649 RVA: 0x000B3C9A File Offset: 0x000B209A
		public static bool IsSetUp()
		{
			return TownNetComManager.Inst.m_Step == TownNetComManager.eComStep.Update;
		}

		// Token: 0x060021CA RID: 8650 RVA: 0x000B3CA9 File Offset: 0x000B20A9
		private void Update()
		{
			base.UpCom();
			if (!base.UpModule() && this.m_Step != TownNetComManager.eComStep.Update)
			{
				this.SetNextStep(this.m_Step + 1);
			}
		}

		// Token: 0x04002840 RID: 10304
		private static TownNetComManager Inst;

		// Token: 0x04002841 RID: 10305
		private TownNetComManager.eComStep m_Step;

		// Token: 0x02000695 RID: 1685
		public enum eComStep
		{
			// Token: 0x04002843 RID: 10307
			ListIn,
			// Token: 0x04002844 RID: 10308
			DefObjSetUp,
			// Token: 0x04002845 RID: 10309
			DefSetUp,
			// Token: 0x04002846 RID: 10310
			Update
		}
	}
}
