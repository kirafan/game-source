﻿using System;

namespace Star
{
	// Token: 0x020004F0 RID: 1264
	public enum eXlsMissionRewardType
	{
		// Token: 0x04001FAE RID: 8110
		None,
		// Token: 0x04001FAF RID: 8111
		Money,
		// Token: 0x04001FB0 RID: 8112
		Item,
		// Token: 0x04001FB1 RID: 8113
		KRRPoint,
		// Token: 0x04001FB2 RID: 8114
		Stamina,
		// Token: 0x04001FB3 RID: 8115
		Friendship,
		// Token: 0x04001FB4 RID: 8116
		LimitedGem
	}
}
