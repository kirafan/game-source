﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000331 RID: 817
	public class FieldObjHandleBuild
	{
		// Token: 0x06000F7D RID: 3965 RVA: 0x00053064 File Offset: 0x00051464
		public FieldObjHandleBuild()
		{
			this.m_StackQue = new List<FieldObjHandleBuild.StackEventQue>();
			this.m_StackQueNum = 0;
			this.m_StoreCharaMngID = new long[2];
			this.m_StoreMax = 2;
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x00053094 File Offset: 0x00051494
		public void AddCharaManageID(long fcharamngid)
		{
			if (this.m_StoreNum >= this.m_StoreMax)
			{
				long[] array = new long[this.m_StoreMax + 2];
				for (int i = 0; i < this.m_StoreMax; i++)
				{
					array[i] = this.m_StoreCharaMngID[i];
				}
				this.m_StoreCharaMngID = null;
				this.m_StoreCharaMngID = array;
				this.m_StoreMax += 2;
			}
			this.m_StoreCharaMngID[this.m_StoreNum] = fcharamngid;
			this.m_StoreNum++;
			this.m_StateUp = true;
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x00053124 File Offset: 0x00051524
		public void DelCharaManageID(long fcharamngid)
		{
			for (int i = 0; i < this.m_StoreNum; i++)
			{
				if (this.m_StoreCharaMngID[i] == fcharamngid)
				{
					for (int j = i + 1; j < this.m_StoreNum; j++)
					{
						this.m_StoreCharaMngID[j - 1] = this.m_StoreCharaMngID[j];
					}
					this.m_StoreNum--;
					this.m_StoreCharaMngID[this.m_StoreNum] = -1L;
					this.m_StateUp = true;
					break;
				}
			}
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x000531AC File Offset: 0x000515AC
		public void BackBuildToRemove(FieldBuildMapReq prequest, bool fcallevt = false)
		{
			for (int i = 0; i < this.m_StoreNum; i++)
			{
				FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_StoreCharaMngID[i]);
				if (charaState != null)
				{
					charaState.BackRoomMove(prequest);
				}
			}
			if (this.m_Callback != null && fcallevt)
			{
				this.m_Callback(eCallBackType.RemoveBuild, this, false);
			}
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x0005320C File Offset: 0x0005160C
		public bool CheckBuildInChara(long fmanageid)
		{
			for (int i = 0; i < this.m_StoreNum; i++)
			{
				if (this.m_StoreCharaMngID[i] == fmanageid)
				{
					return true;
				}
			}
			return this.m_BuildInEntryMax > this.m_StoreNum;
		}

		// Token: 0x06000F82 RID: 3970 RVA: 0x00053250 File Offset: 0x00051650
		public void SetUpBuild(long fmngid, int fbuildpoint)
		{
			this.m_UserMngID = fmngid;
			this.m_BuildPoint = fbuildpoint;
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(this.m_UserMngID);
			if (townBuildData != null)
			{
				this.m_TownObjectID = townBuildData.m_ObjID;
				this.m_BuildInEntryMax = (int)UserTownUtil.GetTownResouceData(this.m_TownObjectID).m_EntryCharaNum;
			}
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x000532A4 File Offset: 0x000516A4
		public void SetUpSystemBuild(long fmngid, int fbuildpoint, int fresid)
		{
			this.m_UserMngID = fmngid;
			this.m_BuildPoint = fbuildpoint;
			this.m_TownObjectID = fresid;
			this.m_BuildInEntryMax = (int)UserTownUtil.GetTownResouceData(this.m_TownObjectID).m_EntryCharaNum;
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x000532E0 File Offset: 0x000516E0
		public bool CalcDropItemListNum(ref FieldObjDropItem.DropItemInfo pstate)
		{
			bool result = false;
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(this.m_UserMngID);
			if (townBuildData != null && townBuildData.m_IsOpen && townBuildData.m_ActionTime != 0L)
			{
				long num = ScheduleTimeUtil.GetSystemTimeMs() - townBuildData.m_ActionTime;
				if (num > 0L && TownItemDropDatabase.CalcDropPeformsnce(ref pstate, UserTownUtil.GetTownResouceData(this.m_TownObjectID).m_LevelUpListID, townBuildData.m_Lv) != null)
				{
					pstate.CalcTimeToUp(num);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x0005335C File Offset: 0x0005175C
		private bool CalcDropItemList(FieldObjDropItem plist)
		{
			bool result = false;
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(this.m_UserMngID);
			if (townBuildData != null && townBuildData.m_IsOpen && townBuildData.m_ActionTime != 0L)
			{
				long systemTimeMs = ScheduleTimeUtil.GetSystemTimeMs();
				long num = systemTimeMs - townBuildData.m_ActionTime;
				if (num > 0L)
				{
					TownItemDropDatabase.DropItemTool dropItemTool = TownItemDropDatabase.CalcDropPeformsnce(ref plist.m_Info, UserTownUtil.GetTownResouceData(this.m_TownObjectID).m_LevelUpListID, townBuildData.m_Lv);
					if (dropItemTool != null)
					{
						plist.m_Info.CalcTimeToUp(num);
						int calcNum = plist.m_Info.m_CalcNum;
						int tableID = plist.m_Info.m_TableID;
						int num2 = dropItemTool.m_Table[tableID].CalcItemIndex();
						plist.CheckDropItem(dropItemTool.m_Table[tableID].m_Table[num2].m_ResultNo, calcNum);
						townBuildData.SetActionTime(systemTimeMs);
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x0005344B File Offset: 0x0005184B
		public void LinkFeedBackCall(FeedBackResultCallback pback)
		{
			this.m_Callback = pback;
		}

		// Token: 0x06000F87 RID: 3975 RVA: 0x00053454 File Offset: 0x00051854
		public void Destroy()
		{
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x00053456 File Offset: 0x00051856
		public void UpdateHandle()
		{
		}

		// Token: 0x06000F89 RID: 3977 RVA: 0x00053458 File Offset: 0x00051858
		public int GetStackQueNum()
		{
			return (int)this.m_StackQueNum;
		}

		// Token: 0x06000F8A RID: 3978 RVA: 0x00053460 File Offset: 0x00051860
		public FieldObjHandleBuild.StackEventQue GetIndexToQue(int findex)
		{
			return this.m_StackQue[findex];
		}

		// Token: 0x06000F8B RID: 3979 RVA: 0x00053470 File Offset: 0x00051870
		public void ClearQue()
		{
			int count = this.m_StackQue.Count;
			int i = 0;
			while (i < count)
			{
				if (this.m_StackQue[i].m_Erase)
				{
					this.m_StackQue.RemoveAt(i);
					count = this.m_StackQue.Count;
					this.m_StackQueNum -= 1;
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x06000F8C RID: 3980 RVA: 0x000534E0 File Offset: 0x000518E0
		private void StackQue(FieldObjHandleBuild.eQueType ftype, int fkeyid, int fnum)
		{
			FieldObjHandleBuild.StackEventQue stackEventQue = new FieldObjHandleBuild.StackEventQue();
			stackEventQue.m_Type = ftype;
			stackEventQue.m_KeyID = fkeyid;
			stackEventQue.m_Num = fnum;
			this.m_StackQue.Add(stackEventQue);
			this.m_StackQueNum += 1;
		}

		// Token: 0x06000F8D RID: 3981 RVA: 0x00053524 File Offset: 0x00051924
		public void SendDropItemCom(IFldNetComModule.CallBack pcallback)
		{
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.SetMarkTime(ScheduleTimeUtil.GetSystemTimeMs());
			FieldObjDropItem fieldObjDropItem = new FieldObjDropItem();
			this.CalcDropItemList(fieldObjDropItem);
			if (fieldObjDropItem.m_List != null)
			{
				for (int i = 0; i < fieldObjDropItem.m_List.Length; i++)
				{
					fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ItemUp, this.m_UserMngID, fieldObjDropItem.m_List[i].m_ResultNo, fieldObjDropItem.m_List[i].m_AddNum, pcallback);
				}
				fldComBuildScript.PlaySend();
			}
		}

		// Token: 0x040016DD RID: 5853
		public int m_BuildPoint;

		// Token: 0x040016DE RID: 5854
		public int m_TownObjectID;

		// Token: 0x040016DF RID: 5855
		public long[] m_StoreCharaMngID;

		// Token: 0x040016E0 RID: 5856
		public int m_StoreNum;

		// Token: 0x040016E1 RID: 5857
		public int m_StoreMax;

		// Token: 0x040016E2 RID: 5858
		public long m_UserMngID;

		// Token: 0x040016E3 RID: 5859
		public int m_BuildInEntryMax;

		// Token: 0x040016E4 RID: 5860
		public FeedBackResultCallback m_Callback;

		// Token: 0x040016E5 RID: 5861
		public bool m_StateUp;

		// Token: 0x040016E6 RID: 5862
		public List<FieldObjHandleBuild.StackEventQue> m_StackQue;

		// Token: 0x040016E7 RID: 5863
		public short m_StackQueNum;

		// Token: 0x02000332 RID: 818
		public enum eQueType
		{
			// Token: 0x040016E9 RID: 5865
			Event,
			// Token: 0x040016EA RID: 5866
			Item,
			// Token: 0x040016EB RID: 5867
			Money
		}

		// Token: 0x02000333 RID: 819
		public class StackEventQue
		{
			// Token: 0x040016EC RID: 5868
			public FieldObjHandleBuild.eQueType m_Type;

			// Token: 0x040016ED RID: 5869
			public bool m_Erase;

			// Token: 0x040016EE RID: 5870
			public int m_KeyID;

			// Token: 0x040016EF RID: 5871
			public int m_Num;
		}
	}
}
