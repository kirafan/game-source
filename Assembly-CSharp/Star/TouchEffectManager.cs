﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AD8 RID: 2776
	public class TouchEffectManager : MonoBehaviour
	{
		// Token: 0x06003A15 RID: 14869 RVA: 0x001281FD File Offset: 0x001265FD
		public void Start()
		{
			this.Setup();
		}

		// Token: 0x06003A16 RID: 14870 RVA: 0x00128208 File Offset: 0x00126608
		public void Setup()
		{
			this.m_ActiveEffects = new List<TouchEffect>();
			this.m_InactiveEffects = new List<TouchEffect>();
			for (int i = 0; i < this.m_EffectLimit; i++)
			{
				TouchEffect touchEffect = UnityEngine.Object.Instantiate<TouchEffect>(this.m_EffectPrefab, base.transform, false);
				touchEffect.transform.SetParent(base.transform);
				touchEffect.gameObject.SetActive(false);
				this.m_InactiveEffects.Add(touchEffect);
			}
			this.m_TouchInfos = new List<TouchEffectManager.TouchInfoAdapter>();
			for (int j = 0; j < 3; j++)
			{
				this.m_TouchInfos.Add(new TouchEffectManager.TouchInfoAdapter().Setup(j));
			}
		}

		// Token: 0x06003A17 RID: 14871 RVA: 0x001282B4 File Offset: 0x001266B4
		private void Update()
		{
			for (int i = 0; i < this.m_TouchInfos.Count; i++)
			{
				this.m_TouchInfos[i].Update();
			}
			if (0 < this.m_ActiveEffects.Count)
			{
				for (int j = 0; j < this.m_ActiveEffects.Count; j++)
				{
					if (this.m_ActiveEffects[j].IsFinished())
					{
						this.m_InactiveEffects.Add(this.m_ActiveEffects[j]);
						this.m_ActiveEffects.RemoveAt(j);
					}
				}
			}
			for (int k = 0; k < this.m_TouchInfos.Count; k++)
			{
				TouchEffectManager.TouchInfoAdapter touchInfoAdapter = this.m_TouchInfos[k];
				if (touchInfoAdapter.GetState() == TouchEffectManager.TouchInfoAdapter.eState.TouchJustNow)
				{
					if (this.m_InactiveEffects.Count == 0 && 0 < this.m_ActiveEffects.Count)
					{
						TouchEffect touchEffect = this.m_ActiveEffects[0];
						this.m_ActiveEffects.RemoveAt(0);
						touchEffect.Skip();
						this.m_InactiveEffects.Add(touchEffect);
					}
					if (0 < this.m_InactiveEffects.Count)
					{
						TouchEffect touchEffect2 = this.m_InactiveEffects[0];
						this.m_InactiveEffects.RemoveAt(0);
						touchEffect2.Play(touchInfoAdapter.GetPosition(), this.m_Canvas, this.m_uiCamera, this.m_wCamera);
						this.m_ActiveEffects.Add(touchEffect2);
					}
				}
			}
		}

		// Token: 0x04004155 RID: 16725
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x04004156 RID: 16726
		[SerializeField]
		private Camera m_uiCamera;

		// Token: 0x04004157 RID: 16727
		[SerializeField]
		private Camera m_wCamera;

		// Token: 0x04004158 RID: 16728
		[SerializeField]
		private int m_EffectLimit;

		// Token: 0x04004159 RID: 16729
		[SerializeField]
		private TouchEffect m_EffectPrefab;

		// Token: 0x0400415A RID: 16730
		private List<TouchEffect> m_ActiveEffects;

		// Token: 0x0400415B RID: 16731
		private List<TouchEffect> m_InactiveEffects;

		// Token: 0x0400415C RID: 16732
		private List<TouchEffectManager.TouchInfoAdapter> m_TouchInfos;

		// Token: 0x02000AD9 RID: 2777
		public class TouchInfoAdapter
		{
			// Token: 0x06003A19 RID: 14873 RVA: 0x00128437 File Offset: 0x00126837
			public TouchEffectManager.TouchInfoAdapter Setup(int in_index)
			{
				this.index = in_index;
				this.state = TouchEffectManager.TouchInfoAdapter.eState.None;
				return this;
			}

			// Token: 0x06003A1A RID: 14874 RVA: 0x00128448 File Offset: 0x00126848
			public void Update()
			{
				InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(this.index);
				if (info.m_bAvailable)
				{
					if (info.m_TouchPhase == TouchPhase.Began && this.state == TouchEffectManager.TouchInfoAdapter.eState.None)
					{
						this.state = TouchEffectManager.TouchInfoAdapter.eState.TouchJustNow;
					}
					else if (info.m_TouchPhase == TouchPhase.Ended || info.m_TouchPhase == TouchPhase.Canceled)
					{
						this.state = TouchEffectManager.TouchInfoAdapter.eState.None;
					}
					else
					{
						this.state = TouchEffectManager.TouchInfoAdapter.eState.Touching;
					}
				}
				else
				{
					this.state = TouchEffectManager.TouchInfoAdapter.eState.None;
				}
			}

			// Token: 0x06003A1B RID: 14875 RVA: 0x001284CE File Offset: 0x001268CE
			public TouchEffectManager.TouchInfoAdapter.eState GetState()
			{
				return this.state;
			}

			// Token: 0x06003A1C RID: 14876 RVA: 0x001284D8 File Offset: 0x001268D8
			public Vector2 GetPosition()
			{
				return InputTouch.Inst.GetInfo(this.index).m_CurrentPos;
			}

			// Token: 0x0400415D RID: 16733
			private int index;

			// Token: 0x0400415E RID: 16734
			private TouchEffectManager.TouchInfoAdapter.eState state;

			// Token: 0x02000ADA RID: 2778
			public enum eState
			{
				// Token: 0x04004160 RID: 16736
				None,
				// Token: 0x04004161 RID: 16737
				TouchJustNow,
				// Token: 0x04004162 RID: 16738
				Touching
			}
		}
	}
}
