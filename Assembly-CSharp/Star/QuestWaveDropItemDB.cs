﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001EA RID: 490
	public class QuestWaveDropItemDB : ScriptableObject
	{
		// Token: 0x04000BE6 RID: 3046
		public QuestWaveDropItemDB_Param[] m_Params;
	}
}
