﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x0200059E RID: 1438
	public class RoomComListSetUp : IFldNetComModule
	{
		// Token: 0x06001BD4 RID: 7124 RVA: 0x0009354D File Offset: 0x0009194D
		public RoomComListSetUp() : base(IFldNetComManager.Instance)
		{
		}

		// Token: 0x06001BD5 RID: 7125 RVA: 0x0009355C File Offset: 0x0009195C
		public bool SetUp()
		{
			RoomComAPIGetActiveRoom phandle = new RoomComAPIGetActiveRoom();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackGetRoomList), false);
			return true;
		}

		// Token: 0x06001BD6 RID: 7126 RVA: 0x0009358C File Offset: 0x0009198C
		private void CallbackGetRoomList(INetComHandle phandle)
		{
			Getactiveroom getactiveroom = phandle.GetResponse() as Getactiveroom;
			SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, getactiveroom.managedRoomId);
			this.m_Step = RoomComListSetUp.eStep.End;
		}

		// Token: 0x06001BD7 RID: 7127 RVA: 0x000935DC File Offset: 0x000919DC
		public bool DefaultListUp()
		{
			RoomComAPISetActiveRoom roomComAPISetActiveRoom = new RoomComAPISetActiveRoom();
			roomComAPISetActiveRoom.managedRoomId = UserRoomUtil.GetActiveRoomMngID(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
			this.m_NetMng.SendCom(roomComAPISetActiveRoom, new INetComHandle.ResponseCallbak(this.CallbackDefaultRoomList), false);
			return true;
		}

		// Token: 0x06001BD8 RID: 7128 RVA: 0x00093628 File Offset: 0x00091A28
		private void CallbackDefaultRoomList(INetComHandle phandle)
		{
			this.m_Step = RoomComListSetUp.eStep.End;
		}

		// Token: 0x06001BD9 RID: 7129 RVA: 0x00093634 File Offset: 0x00091A34
		public override bool UpFunc()
		{
			bool result = true;
			RoomComListSetUp.eStep step = this.m_Step;
			if (step != RoomComListSetUp.eStep.GetState)
			{
				if (step != RoomComListSetUp.eStep.WaitCheck)
				{
					if (step == RoomComListSetUp.eStep.End)
					{
						result = false;
					}
				}
			}
			else
			{
				this.SetUp();
				this.m_Step = RoomComListSetUp.eStep.WaitCheck;
			}
			return result;
		}

		// Token: 0x040022CC RID: 8908
		private RoomComListSetUp.eStep m_Step;

		// Token: 0x0200059F RID: 1439
		public enum eStep
		{
			// Token: 0x040022CE RID: 8910
			GetState,
			// Token: 0x040022CF RID: 8911
			WaitCheck,
			// Token: 0x040022D0 RID: 8912
			End
		}
	}
}
