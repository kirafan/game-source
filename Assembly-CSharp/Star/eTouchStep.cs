﻿using System;

namespace Star
{
	// Token: 0x020005BD RID: 1469
	public enum eTouchStep
	{
		// Token: 0x04002347 RID: 9031
		Begin,
		// Token: 0x04002348 RID: 9032
		Drag,
		// Token: 0x04002349 RID: 9033
		End,
		// Token: 0x0400234A RID: 9034
		Cancel,
		// Token: 0x0400234B RID: 9035
		RayCheck
	}
}
