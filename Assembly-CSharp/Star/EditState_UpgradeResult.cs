﻿using System;
using Star.UI.BackGround;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x02000415 RID: 1045
	public class EditState_UpgradeResult : EditState
	{
		// Token: 0x06001400 RID: 5120 RVA: 0x0006A9BA File Offset: 0x00068DBA
		public EditState_UpgradeResult(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001401 RID: 5121 RVA: 0x0006A9D2 File Offset: 0x00068DD2
		public override int GetStateID()
		{
			return 10;
		}

		// Token: 0x06001402 RID: 5122 RVA: 0x0006A9D6 File Offset: 0x00068DD6
		public override void OnStateEnter()
		{
			this.m_MixEffectScene = this.m_Owner.MixEffectScenes[0];
			this.m_Step = EditState_UpgradeResult.eStep.StartFadeOut;
		}

		// Token: 0x06001403 RID: 5123 RVA: 0x0006A9F2 File Offset: 0x00068DF2
		public override void OnStateExit()
		{
		}

		// Token: 0x06001404 RID: 5124 RVA: 0x0006A9F4 File Offset: 0x00068DF4
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x0006AA00 File Offset: 0x00068E00
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_UpgradeResult.eStep.StartFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_UpgradeResult.eStep.StartFadeWait;
				break;
			case EditState_UpgradeResult.eStep.StartFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = EditState_UpgradeResult.eStep.First;
				}
				break;
			case EditState_UpgradeResult.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_UpgradeResult.eStep.ChildSceneLoadWait;
				break;
			case EditState_UpgradeResult.eStep.ChildSceneLoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_Owner.UpgradeResult.m_CharaMngID);
					CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
					this.m_MixEffectScene.Prepare(param.m_CharaID);
					this.m_Step = EditState_UpgradeResult.eStep.EffectLoadWait;
				}
				break;
			case EditState_UpgradeResult.eStep.EffectLoadWait:
				if (this.m_MixEffectScene.IsPrepareError())
				{
					this.m_Step = EditState_UpgradeResult.eStep.PrepareError;
				}
				else if (this.m_MixEffectScene.IsCompletePrepare())
				{
					this.m_Step = EditState_UpgradeResult.eStep.Setup;
				}
				break;
			case EditState_UpgradeResult.eStep.Setup:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Hide(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(-1f, null);
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Effect);
					this.m_Step = EditState_UpgradeResult.eStep.Main_0;
				}
				break;
			case EditState_UpgradeResult.eStep.Main_0:
			{
				Vector2 vector;
				bool flag = InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0);
				if (flag || this.m_MixEffectScene.IsCompletePlay())
				{
					this.m_Step = EditState_UpgradeResult.eStep.Main_1;
					if (flag)
					{
						this.m_MixEffectScene.OnSkip();
					}
				}
				break;
			}
			case EditState_UpgradeResult.eStep.Main_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_UI.PlayIn();
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Loop);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.2f, null);
					this.m_Step = EditState_UpgradeResult.eStep.Main_2;
				}
				break;
			case EditState_UpgradeResult.eStep.Main_2:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_NextState = 9;
					this.m_Step = EditState_UpgradeResult.eStep.EndFadeOut;
				}
				break;
			case EditState_UpgradeResult.eStep.EndFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_UpgradeResult.eStep.EndFadeWait;
				break;
			case EditState_UpgradeResult.eStep.EndFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_UpgradeResult.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_UpgradeResult.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_MixEffectScene.Suspend();
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Show(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(0.2f, null);
					this.m_Step = EditState_UpgradeResult.eStep.None;
					return this.m_NextState;
				}
				break;
			case EditState_UpgradeResult.eStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.m_Step = EditState_UpgradeResult.eStep.PrepareErrorWait;
				break;
			}
			return -1;
		}

		// Token: 0x06001406 RID: 5126 RVA: 0x0006AD78 File Offset: 0x00069178
		public bool SetupUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<UpgradeResultUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.UpgradeResult);
			return true;
		}

		// Token: 0x06001407 RID: 5127 RVA: 0x0006ADD2 File Offset: 0x000691D2
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001ADB RID: 6875
		private EditState_UpgradeResult.eStep m_Step = EditState_UpgradeResult.eStep.None;

		// Token: 0x04001ADC RID: 6876
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.UpgradeResultUI;

		// Token: 0x04001ADD RID: 6877
		public UpgradeResultUI m_UI;

		// Token: 0x04001ADE RID: 6878
		public MixEffectScene m_MixEffectScene;

		// Token: 0x04001ADF RID: 6879
		private const float FADE_SEC = 0.2f;

		// Token: 0x02000416 RID: 1046
		private enum eStep
		{
			// Token: 0x04001AE1 RID: 6881
			None = -1,
			// Token: 0x04001AE2 RID: 6882
			StartFadeOut,
			// Token: 0x04001AE3 RID: 6883
			StartFadeWait,
			// Token: 0x04001AE4 RID: 6884
			First,
			// Token: 0x04001AE5 RID: 6885
			ChildSceneLoadWait,
			// Token: 0x04001AE6 RID: 6886
			EffectLoadWait,
			// Token: 0x04001AE7 RID: 6887
			Setup,
			// Token: 0x04001AE8 RID: 6888
			Main_0,
			// Token: 0x04001AE9 RID: 6889
			Main_1,
			// Token: 0x04001AEA RID: 6890
			Main_2,
			// Token: 0x04001AEB RID: 6891
			EndFadeOut,
			// Token: 0x04001AEC RID: 6892
			EndFadeWait,
			// Token: 0x04001AED RID: 6893
			UnloadChildSceneWait,
			// Token: 0x04001AEE RID: 6894
			PrepareError,
			// Token: 0x04001AEF RID: 6895
			PrepareErrorWait
		}
	}
}
