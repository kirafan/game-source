﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200027C RID: 636
	public class DebugDisplay : MonoBehaviour
	{
		// Token: 0x06000BDE RID: 3038 RVA: 0x00045A21 File Offset: 0x00043E21
		private void Awake()
		{
			Application.logMessageReceived += this.LogCallback;
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x00045A34 File Offset: 0x00043E34
		public void LogCallback(string condition, string stackTrace, LogType type)
		{
			string arg = null;
			string arg2 = null;
			switch (type)
			{
			case LogType.Error:
			case LogType.Assert:
				arg = stackTrace.Remove(0, stackTrace.IndexOf("\n") + 1);
				arg2 = "red";
				break;
			case LogType.Warning:
				arg = stackTrace.Remove(0, stackTrace.IndexOf("\n") + 1);
				arg2 = "yellow";
				break;
			case LogType.Exception:
				arg = stackTrace;
				arg2 = "red";
				break;
			}
			if (this.m_LogStack.Count == 30)
			{
				this.m_LogStack.Dequeue();
			}
			string item = string.Format("<color={0}>{1}</color> <color=white>on {2}</color>", arg2, condition, arg);
			this.m_LogStack.Enqueue(item);
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x00045AEC File Offset: 0x00043EEC
		private void OnGUI()
		{
			if (GUI.Button(new Rect((float)(Screen.width - 200), (float)(Screen.height - 100), 200f, 100f), "debug.log on/off"))
			{
				this.m_IsFlg = !this.m_IsFlg;
			}
			if (this.m_LogStack == null)
			{
				return;
			}
			if (this.m_IsFlg)
			{
				float num = 16f;
				Rect rect = new Rect(num, num, (float)Screen.width - num * 2f, (float)Screen.height - num * 2f);
				GUI.Box(rect, string.Empty);
				GUILayout.BeginArea(rect);
				GUIStyle guistyle = new GUIStyle();
				guistyle.wordWrap = true;
				guistyle.fontSize = 16;
				foreach (string text in this.m_LogStack)
				{
					GUILayout.Label(text, guistyle, new GUILayoutOption[0]);
				}
				GUILayout.EndArea();
			}
		}

		// Token: 0x04001465 RID: 5221
		private const int LOG_MAX = 30;

		// Token: 0x04001466 RID: 5222
		private Queue<string> m_LogStack = new Queue<string>(30);

		// Token: 0x04001467 RID: 5223
		public bool m_IsFlg = true;
	}
}
