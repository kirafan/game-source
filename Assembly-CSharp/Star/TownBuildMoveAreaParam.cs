﻿using System;

namespace Star
{
	// Token: 0x02000723 RID: 1827
	public class TownBuildMoveAreaParam : ITownEventCommand
	{
		// Token: 0x06002412 RID: 9234 RVA: 0x000C19F5 File Offset: 0x000BFDF5
		public TownBuildMoveAreaParam(int fbackpoint, int fbuildpoint, long fmanageid, int fobjid)
		{
			this.m_Type = eTownRequest.ChangeArea;
			this.m_BackBuildPoint = fbackpoint;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_Step = TownBuildMoveAreaParam.eStep.BuildQue;
			this.m_Enable = true;
		}

		// Token: 0x06002413 RID: 9235 RVA: 0x000C1A30 File Offset: 0x000BFE30
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			TownBuildMoveAreaParam.eStep step = this.m_Step;
			if (step != TownBuildMoveAreaParam.eStep.BuildQue)
			{
				if (step != TownBuildMoveAreaParam.eStep.SetUpCheck)
				{
					if (step == TownBuildMoveAreaParam.eStep.Final)
					{
						if (pbuilder.IsBuildMngIDToBuildUp(this.m_ManageID))
						{
							this.m_Enable = false;
						}
					}
				}
				else
				{
					TownBuildUpAreaParam pparam = new TownBuildUpAreaParam(this.m_BuildPointID, this.m_ManageID, this.m_ObjID);
					if (pbuilder.BuildToArea(pparam))
					{
						this.m_Step = TownBuildMoveAreaParam.eStep.Final;
					}
				}
			}
			else
			{
				TownBuildLinkPoint buildPointTreeNode = pbuilder.GetBuildTree().GetBuildPointTreeNode(this.m_BackBuildPoint);
				if (buildPointTreeNode != null)
				{
					buildPointTreeNode.m_Pakage.BackBuildMode();
					this.m_Step = TownBuildMoveAreaParam.eStep.SetUpCheck;
				}
				else
				{
					this.m_Step = TownBuildMoveAreaParam.eStep.SetUpCheck;
				}
			}
			return this.m_Enable;
		}

		// Token: 0x04002B0B RID: 11019
		public int m_BackBuildPoint;

		// Token: 0x04002B0C RID: 11020
		public int m_BuildPointID;

		// Token: 0x04002B0D RID: 11021
		public int m_ObjID;

		// Token: 0x04002B0E RID: 11022
		public long m_ManageID;

		// Token: 0x04002B0F RID: 11023
		public TownBuildMoveAreaParam.eStep m_Step;

		// Token: 0x02000724 RID: 1828
		public enum eStep
		{
			// Token: 0x04002B11 RID: 11025
			BuildQue,
			// Token: 0x04002B12 RID: 11026
			SetUpCheck,
			// Token: 0x04002B13 RID: 11027
			Final
		}
	}
}
