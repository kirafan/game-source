﻿using System;

namespace Star
{
	// Token: 0x02000472 RID: 1138
	public class RoomState : GameStateBase
	{
		// Token: 0x06001635 RID: 5685 RVA: 0x000744E7 File Offset: 0x000728E7
		public RoomState(RoomMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001636 RID: 5686 RVA: 0x000744FD File Offset: 0x000728FD
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001637 RID: 5687 RVA: 0x00074500 File Offset: 0x00072900
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001638 RID: 5688 RVA: 0x00074502 File Offset: 0x00072902
		public override void OnStateExit()
		{
		}

		// Token: 0x06001639 RID: 5689 RVA: 0x00074504 File Offset: 0x00072904
		public override void OnDispose()
		{
		}

		// Token: 0x0600163A RID: 5690 RVA: 0x00074506 File Offset: 0x00072906
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0600163B RID: 5691 RVA: 0x00074509 File Offset: 0x00072909
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001CE3 RID: 7395
		protected RoomMain m_Owner;

		// Token: 0x04001CE4 RID: 7396
		protected int m_NextState = -1;
	}
}
