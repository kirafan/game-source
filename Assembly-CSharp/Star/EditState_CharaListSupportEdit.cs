﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020003FC RID: 1020
	public class EditState_CharaListSupportEdit : EditState_CharaListPartyEditBase
	{
		// Token: 0x06001367 RID: 4967 RVA: 0x00067C2A File Offset: 0x0006602A
		public EditState_CharaListSupportEdit(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001368 RID: 4968 RVA: 0x00067C33 File Offset: 0x00066033
		public override int GetDefaultNextSceneState()
		{
			return 4;
		}

		// Token: 0x06001369 RID: 4969 RVA: 0x00067C38 File Offset: 0x00066038
		protected override void OnStateUpdateMainOnClickButton()
		{
			CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
			if (selectButton != CharaListUI.eButton.Remove)
			{
				if (selectButton == CharaListUI.eButton.Chara)
				{
					EditUtility.AssignSupportPartyMember(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyMemberIndex, this.m_Owner.CharaListUI.SelectCharaMngID);
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
				}
			}
			else
			{
				EditUtility.RemoveSupportPartyMember(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyMemberIndex);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
			}
		}

		// Token: 0x0600136A RID: 4970 RVA: 0x00067CED File Offset: 0x000660ED
		protected override void SetListEditMode()
		{
			this.m_Owner.CharaListUI.SetSupportEditMode(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyMemberIndex);
		}
	}
}
