﻿using System;

namespace Star
{
	// Token: 0x02000193 RID: 403
	public static class EffectListDB_Ext
	{
		// Token: 0x06000AED RID: 2797 RVA: 0x000412D0 File Offset: 0x0003F6D0
		public static bool IsExistSoundSetting(this EffectListDB self, int index)
		{
			return self.m_Params[index].m_PlayFrames != null && self.m_Params[index].m_PlayFrames.Length > 0 && self.m_Params[index].m_CueIDs != null && self.m_Params[index].m_CueIDs.Length > 0;
		}

		// Token: 0x06000AEE RID: 2798 RVA: 0x00041340 File Offset: 0x0003F740
		public static void SetupSoundFrameController(this EffectListDB self, int index, SoundFrameController sfc)
		{
			for (int i = 0; i < self.m_Params[index].m_PlayFrames.Length; i++)
			{
				sfc.AddRequestParam(self.m_Params[index].m_PlayFrames[i], (eSoundSeListDB)self.m_Params[index].m_CueIDs[i], true);
			}
		}
	}
}
