﻿using System;

namespace Star
{
	// Token: 0x02000215 RID: 533
	[Serializable]
	public struct SoundHomeBgmListDB_Param
	{
		// Token: 0x04000D3C RID: 3388
		public string m_StartAt;

		// Token: 0x04000D3D RID: 3389
		public string m_EndAt;

		// Token: 0x04000D3E RID: 3390
		public int m_CueID;
	}
}
