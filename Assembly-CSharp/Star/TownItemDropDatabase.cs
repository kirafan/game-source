﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000383 RID: 899
	public class TownItemDropDatabase
	{
		// Token: 0x06001115 RID: 4373 RVA: 0x00059B39 File Offset: 0x00057F39
		public static void DatabaseSetUp()
		{
			if (TownItemDropDatabase.ms_DropItemDB == null)
			{
				TownItemDropDatabase.ms_DropItemDB = new TownItemDropDatabase.DropItemList();
				TownItemDropDatabase.ms_DropItemDB.SetUpResource(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB);
			}
		}

		// Token: 0x06001116 RID: 4374 RVA: 0x00059B68 File Offset: 0x00057F68
		public static bool IsSetUp()
		{
			return true;
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x00059B6B File Offset: 0x00057F6B
		public static void DatabaseRelease()
		{
			TownItemDropDatabase.ms_DropItemDB = null;
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x00059B74 File Offset: 0x00057F74
		public static TownItemDropDatabase.DropItemTool GetKeyIDToDatabase(int fkeyid)
		{
			if (TownItemDropDatabase.ms_DropItemDB != null)
			{
				int num = TownItemDropDatabase.ms_DropItemDB.m_DataList.Length;
				for (int i = 0; i < num; i++)
				{
					if (TownItemDropDatabase.ms_DropItemDB.m_DataList[i].m_KeyID == fkeyid)
					{
						return TownItemDropDatabase.ms_DropItemDB.m_DataList[i];
					}
				}
			}
			return null;
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x00059BD0 File Offset: 0x00057FD0
		public static TownItemDropDatabase.DropItemTool CalcDropPeformsnce(ref FieldObjDropItem.DropItemInfo pstate, int fkeyid, int flevel)
		{
			TownItemDropDatabase.DropItemTool keyIDToDatabase = TownItemDropDatabase.GetKeyIDToDatabase(fkeyid);
			if (keyIDToDatabase != null)
			{
				for (int i = 0; i < keyIDToDatabase.m_Table.Length; i++)
				{
					if (keyIDToDatabase.m_Table[i].m_Level == flevel && keyIDToDatabase.m_Table[i].m_LimitNum != 0)
					{
						pstate.m_TableID = i;
						pstate.m_UpTime = (long)keyIDToDatabase.m_Table[i].m_ChkTime;
						pstate.m_LimitNum = keyIDToDatabase.m_Table[i].m_LimitNum;
						pstate.m_FirstDropID = keyIDToDatabase.m_Table[i].m_Table[0].m_ResultNo;
						pstate.m_CalcNum = 0;
						pstate.m_Max = false;
						break;
					}
				}
			}
			return keyIDToDatabase;
		}

		// Token: 0x040017FA RID: 6138
		public static TownItemDropDatabase.DropItemList ms_DropItemDB;

		// Token: 0x02000384 RID: 900
		public struct DropItemData
		{
			// Token: 0x040017FB RID: 6139
			public short m_WakeUp;

			// Token: 0x040017FC RID: 6140
			public int m_ResultNo;
		}

		// Token: 0x02000385 RID: 901
		public struct DropItemPakage
		{
			// Token: 0x0600111A RID: 4378 RVA: 0x00059C9C File Offset: 0x0005809C
			public int CalcItemIndex()
			{
				int num = 0;
				int i = UnityEngine.Random.Range(0, 10000);
				while (i >= 0)
				{
					if (num >= this.m_Table.Length)
					{
						num = 0;
						break;
					}
					i -= (int)this.m_Table[num].m_WakeUp;
					if (i <= 0)
					{
						break;
					}
					num++;
				}
				return num;
			}

			// Token: 0x0600111B RID: 4379 RVA: 0x00059CFD File Offset: 0x000580FD
			public bool IsChkListUpOneItem()
			{
				return this.m_Table.Length <= 1;
			}

			// Token: 0x040017FD RID: 6141
			public int m_Level;

			// Token: 0x040017FE RID: 6142
			public int m_LimitNum;

			// Token: 0x040017FF RID: 6143
			public int m_ChkTime;

			// Token: 0x04001800 RID: 6144
			public TownItemDropDatabase.DropItemData[] m_Table;
		}

		// Token: 0x02000386 RID: 902
		public class DropItemTool
		{
			// Token: 0x04001801 RID: 6145
			public int m_KeyID;

			// Token: 0x04001802 RID: 6146
			public TownItemDropDatabase.DropItemPakage[] m_Table;
		}

		// Token: 0x02000387 RID: 903
		public class DropItemList
		{
			// Token: 0x0600111E RID: 4382 RVA: 0x00059D20 File Offset: 0x00058120
			public void SetUpResource(TownObjectLevelUpDB pdb)
			{
				TownItemDropDatabase.OptionPakage optionPakage = new TownItemDropDatabase.OptionPakage();
				int num = pdb.m_Params.Length;
				for (int i = 0; i < num; i++)
				{
					if (pdb.m_Params[i].m_LimitNum > 0)
					{
						TownItemDropDatabase.OptionPakageKey optionPakageKey = optionPakage.GetKey(pdb.m_Params[i].m_ID);
						optionPakageKey.m_TableID.Add(i);
					}
				}
				num = optionPakage.GetKeyNum();
				this.m_DataList = new TownItemDropDatabase.DropItemTool[optionPakage.GetKeyNum()];
				for (int i = 0; i < num; i++)
				{
					TownItemDropDatabase.OptionPakageKey optionPakageKey = optionPakage.GetIndexToKey(i);
					this.m_DataList[i] = new TownItemDropDatabase.DropItemTool();
					optionPakageKey.MakeOptionKeyData(pdb, ref TownItemDropDatabase.ms_DropItemDB.m_DataList[i]);
				}
			}

			// Token: 0x04001803 RID: 6147
			public TownItemDropDatabase.DropItemTool[] m_DataList;
		}

		// Token: 0x02000388 RID: 904
		public class OptionPakageKey
		{
			// Token: 0x06001120 RID: 4384 RVA: 0x00059DF4 File Offset: 0x000581F4
			public void MakeOptionKeyData(TownObjectLevelUpDB pdata, ref TownItemDropDatabase.DropItemTool pout)
			{
				int count = this.m_TableID.Count;
				pout.m_KeyID = this.m_KeyID;
				pout.m_Table = new TownItemDropDatabase.DropItemPakage[this.m_TableID.Count];
				for (int i = 0; i < count; i++)
				{
					int num = this.m_TableID[i];
					pout.m_Table[i].m_Level = pdata.m_Params[num].m_TargetLv;
					pout.m_Table[i].m_LimitNum = pdata.m_Params[num].m_LimitNum;
					pout.m_Table[i].m_ChkTime = pdata.m_Params[num].m_WakeTime * 1000;
					if (pout.m_Table[i].m_LimitNum != 0)
					{
						int num2 = 10;
						if (num2 > pdata.m_Params[num].m_Parsent.Length)
						{
							num2 = pdata.m_Params[num].m_Parsent.Length;
						}
						if (num2 > pdata.m_Params[num].m_ResultNo.Length)
						{
							num2 = pdata.m_Params[num].m_ResultNo.Length;
						}
						pout.m_Table[i].m_Table = new TownItemDropDatabase.DropItemData[num2];
						for (int j = 0; j < num2; j++)
						{
							pout.m_Table[i].m_Table[j].m_ResultNo = (int)pdata.m_Params[num].m_ResultNo[j];
							pout.m_Table[i].m_Table[j].m_WakeUp = (short)(pdata.m_Params[num].m_Parsent[j] * 100f);
						}
					}
				}
			}

			// Token: 0x04001804 RID: 6148
			public int m_KeyID;

			// Token: 0x04001805 RID: 6149
			public List<int> m_TableID = new List<int>();
		}

		// Token: 0x02000389 RID: 905
		public class OptionPakage
		{
			// Token: 0x06001122 RID: 4386 RVA: 0x00059FE4 File Offset: 0x000583E4
			public TownItemDropDatabase.OptionPakageKey GetKey(int fkey)
			{
				TownItemDropDatabase.OptionPakageKey optionPakageKey = null;
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_KeyID == fkey)
					{
						optionPakageKey = this.m_List[i];
						break;
					}
				}
				if (optionPakageKey == null)
				{
					optionPakageKey = new TownItemDropDatabase.OptionPakageKey();
					optionPakageKey.m_KeyID = fkey;
					this.m_List.Add(optionPakageKey);
				}
				return optionPakageKey;
			}

			// Token: 0x06001123 RID: 4387 RVA: 0x0005A05A File Offset: 0x0005845A
			public int GetKeyNum()
			{
				return this.m_List.Count;
			}

			// Token: 0x06001124 RID: 4388 RVA: 0x0005A067 File Offset: 0x00058467
			public TownItemDropDatabase.OptionPakageKey GetIndexToKey(int findex)
			{
				return this.m_List[findex];
			}

			// Token: 0x04001806 RID: 6150
			public List<TownItemDropDatabase.OptionPakageKey> m_List = new List<TownItemDropDatabase.OptionPakageKey>();
		}
	}
}
