﻿using System;

namespace Star
{
	// Token: 0x02000205 RID: 517
	public enum eSkillTargetType
	{
		// Token: 0x04000CB2 RID: 3250
		Self,
		// Token: 0x04000CB3 RID: 3251
		TgtSingle,
		// Token: 0x04000CB4 RID: 3252
		TgtAll,
		// Token: 0x04000CB5 RID: 3253
		MySingle,
		// Token: 0x04000CB6 RID: 3254
		MyAll,
		// Token: 0x04000CB7 RID: 3255
		Num
	}
}
