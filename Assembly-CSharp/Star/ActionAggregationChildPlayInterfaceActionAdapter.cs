﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000086 RID: 134
	[AddComponentMenu("ActionAggregation/PlayInterface")]
	[Serializable]
	public class ActionAggregationChildPlayInterfaceActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000419 RID: 1049 RVA: 0x00014051 File Offset: 0x00012451
		public override void Update()
		{
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00014053 File Offset: 0x00012453
		public override void PlayStartExtendProcess()
		{
			if (this.m_PlayInterface != null)
			{
				this.m_PlayInterface.Play();
			}
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x00014071 File Offset: 0x00012471
		public override void Skip()
		{
			if (this.m_PlayInterface != null)
			{
				this.m_PlayInterface.Stop();
			}
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x0001408F File Offset: 0x0001248F
		public override void RecoveryFinishToWaitExtendProcess()
		{
			if (this.m_PlayInterface != null)
			{
				this.m_PlayInterface.Stop();
			}
		}

		// Token: 0x0400022F RID: 559
		[SerializeField]
		[Tooltip("Play関数を持つシーン内オブジェクト.")]
		private PlayInterfaceMonoBehaviour m_PlayInterface;
	}
}
