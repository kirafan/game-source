﻿using System;

namespace Star
{
	// Token: 0x0200032A RID: 810
	public enum eCharaStay
	{
		// Token: 0x040016C4 RID: 5828
		Room,
		// Token: 0x040016C5 RID: 5829
		Sleep,
		// Token: 0x040016C6 RID: 5830
		Content,
		// Token: 0x040016C7 RID: 5831
		Buf,
		// Token: 0x040016C8 RID: 5832
		Menu
	}
}
