﻿using System;

namespace Star
{
	// Token: 0x02000192 RID: 402
	public static class ClassListDB_Ext
	{
		// Token: 0x06000AEC RID: 2796 RVA: 0x000412BB File Offset: 0x0003F6BB
		public static ClassListDB_Param GetParam(this ClassListDB self, eClassType classType)
		{
			return self.m_Params[(int)classType];
		}
	}
}
