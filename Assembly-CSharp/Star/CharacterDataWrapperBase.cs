﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x0200029F RID: 671
	public abstract class CharacterDataWrapperBase
	{
		// Token: 0x06000C64 RID: 3172 RVA: 0x00048646 File Offset: 0x00046A46
		protected CharacterDataWrapperBase(eCharacterDataType characterDataType)
		{
			this.m_CharacterDataType = characterDataType;
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x00048655 File Offset: 0x00046A55
		public eCharacterDataType GetCharacterDataType()
		{
			return this.m_CharacterDataType;
		}

		// Token: 0x06000C66 RID: 3174
		public abstract long GetMngId();

		// Token: 0x06000C67 RID: 3175
		public abstract int GetCharaId();

		// Token: 0x06000C68 RID: 3176 RVA: 0x0004865D File Offset: 0x00046A5D
		public virtual eTitleType GetCharaTitleType()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(this.GetCharaNamedType());
		}

		// Token: 0x06000C69 RID: 3177
		public abstract eCharaNamedType GetCharaNamedType();

		// Token: 0x06000C6A RID: 3178
		public abstract eClassType GetClassType();

		// Token: 0x06000C6B RID: 3179
		public abstract eElementType GetElementType();

		// Token: 0x06000C6C RID: 3180
		public abstract eRare GetRarity();

		// Token: 0x06000C6D RID: 3181
		public abstract int GetCost();

		// Token: 0x06000C6E RID: 3182
		public abstract int GetLv();

		// Token: 0x06000C6F RID: 3183
		public abstract int GetMaxLv();

		// Token: 0x06000C70 RID: 3184
		public abstract long GetExp();

		// Token: 0x06000C71 RID: 3185
		public abstract int GetLimitBreak();

		// Token: 0x06000C72 RID: 3186 RVA: 0x00048679 File Offset: 0x00046A79
		public virtual long GetNowExp()
		{
			return EditUtility.GetCharaNowExp(this.GetLv(), this.GetExp());
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x0004868C File Offset: 0x00046A8C
		public virtual long GetNowMaxExp()
		{
			return EditUtility.GetCharaNextMaxExp(this.GetLv());
		}

		// Token: 0x06000C74 RID: 3188
		public abstract int GetFriendship();

		// Token: 0x06000C75 RID: 3189
		public abstract int GetFriendshipMax();

		// Token: 0x06000C76 RID: 3190
		public abstract long GetFriendshipExp();

		// Token: 0x06000C77 RID: 3191
		public abstract SkillLearnData GetUniqueSkillLearnData();

		// Token: 0x06000C78 RID: 3192 RVA: 0x00048699 File Offset: 0x00046A99
		public virtual int GetUniqueSkillLv()
		{
			return this.GetSkillLevel(this.GetUniqueSkillLearnData());
		}

		// Token: 0x06000C79 RID: 3193 RVA: 0x000486A7 File Offset: 0x00046AA7
		public virtual int GetUniqueSkillMaxLv()
		{
			return this.GetSkillMaxLevel(this.GetUniqueSkillLearnData());
		}

		// Token: 0x06000C7A RID: 3194
		public abstract List<SkillLearnData> GetClassSkillLearnDatas();

		// Token: 0x06000C7B RID: 3195 RVA: 0x000486B8 File Offset: 0x00046AB8
		public virtual int GetClassSkillsCount()
		{
			List<SkillLearnData> classSkillLearnDatas = this.GetClassSkillLearnDatas();
			if (classSkillLearnDatas == null)
			{
				return 0;
			}
			return classSkillLearnDatas.Count;
		}

		// Token: 0x06000C7C RID: 3196 RVA: 0x000486DC File Offset: 0x00046ADC
		public SkillLearnData GetClassSkillLearnData(int index)
		{
			List<SkillLearnData> classSkillLearnDatas = this.GetClassSkillLearnDatas();
			if (classSkillLearnDatas == null || index < 0 || classSkillLearnDatas.Count <= index)
			{
				return null;
			}
			return classSkillLearnDatas[index];
		}

		// Token: 0x06000C7D RID: 3197 RVA: 0x00048714 File Offset: 0x00046B14
		public virtual int[] GetClassSkillLevels()
		{
			List<SkillLearnData> classSkillLearnDatas = this.GetClassSkillLearnDatas();
			if (classSkillLearnDatas == null)
			{
				return null;
			}
			int[] array = new int[classSkillLearnDatas.Count];
			for (int i = 0; i < classSkillLearnDatas.Count; i++)
			{
				array[i] = this.GetSkillLevel(classSkillLearnDatas[i]);
			}
			return array;
		}

		// Token: 0x06000C7E RID: 3198 RVA: 0x00048764 File Offset: 0x00046B64
		public virtual int GetClassSkillLevel(int index)
		{
			return this.GetSkillLevel(this.GetClassSkillLearnData(index));
		}

		// Token: 0x06000C7F RID: 3199 RVA: 0x00048773 File Offset: 0x00046B73
		protected int GetSkillLevel(SkillLearnData sld)
		{
			if (sld == null)
			{
				return 1;
			}
			return sld.SkillLv;
		}

		// Token: 0x06000C80 RID: 3200 RVA: 0x00048783 File Offset: 0x00046B83
		protected int GetSkillMaxLevel(SkillLearnData sld)
		{
			if (sld == null)
			{
				return 1;
			}
			return sld.SkillMaxLv;
		}

		// Token: 0x06000C81 RID: 3201 RVA: 0x00048793 File Offset: 0x00046B93
		public virtual EditUtility.OutputCharaParam GetCharaParamLvOnly()
		{
			return EditUtility.CalcCharaParamLevelOnly(this.GetCharaId(), this.GetLv());
		}

		// Token: 0x06000C82 RID: 3202 RVA: 0x000487A8 File Offset: 0x00046BA8
		public virtual int GetHpLvOnly()
		{
			return this.GetCharaParamLvOnly().Hp;
		}

		// Token: 0x06000C83 RID: 3203 RVA: 0x000487C4 File Offset: 0x00046BC4
		public virtual int GetAtkLvOnly()
		{
			return this.GetCharaParamLvOnly().Atk;
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x000487E0 File Offset: 0x00046BE0
		public virtual int GetDefLvOnly()
		{
			return this.GetCharaParamLvOnly().Mgc;
		}

		// Token: 0x06000C85 RID: 3205 RVA: 0x000487FC File Offset: 0x00046BFC
		public virtual int GetMDefLvOnly()
		{
			return this.GetCharaParamLvOnly().Def;
		}

		// Token: 0x06000C86 RID: 3206 RVA: 0x00048818 File Offset: 0x00046C18
		public virtual int GetMgcLvOnly()
		{
			return this.GetCharaParamLvOnly().MDef;
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x00048834 File Offset: 0x00046C34
		public virtual int GetSpdLvOnly()
		{
			return this.GetCharaParamLvOnly().Spd;
		}

		// Token: 0x06000C88 RID: 3208 RVA: 0x00048850 File Offset: 0x00046C50
		public virtual int GetLuckLvOnly()
		{
			return this.GetCharaParamLvOnly().Luck;
		}

		// Token: 0x06000C89 RID: 3209 RVA: 0x0004886B File Offset: 0x00046C6B
		public void Assert()
		{
		}

		// Token: 0x04001542 RID: 5442
		protected eCharacterDataType m_CharacterDataType;
	}
}
