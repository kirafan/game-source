﻿using System;

namespace Star
{
	// Token: 0x020001F9 RID: 505
	[Serializable]
	public struct RoomShopListDB_Param
	{
		// Token: 0x04000C42 RID: 3138
		public int m_Category;

		// Token: 0x04000C43 RID: 3139
		public int m_ID;

		// Token: 0x04000C44 RID: 3140
		public int m_BuyPrice;

		// Token: 0x04000C45 RID: 3141
		public int m_SellPrice;

		// Token: 0x04000C46 RID: 3142
		public string m_BaseDescription;

		// Token: 0x04000C47 RID: 3143
		public string m_BufDescription;

		// Token: 0x04000C48 RID: 3144
		public ushort m_ListUpID;
	}
}
