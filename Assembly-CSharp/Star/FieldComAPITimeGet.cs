﻿using System;
using AppResponseTypes;

namespace Star
{
	// Token: 0x02000347 RID: 839
	public class FieldComAPITimeGet : INetComHandle
	{
		// Token: 0x06001018 RID: 4120 RVA: 0x00055BDB File Offset: 0x00053FDB
		public FieldComAPITimeGet()
		{
			this.ApiName = "api/app/health";
			this.Request = false;
			this.ResponseType = typeof(Health);
		}
	}
}
