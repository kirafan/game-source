﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E0 RID: 1248
	public static class Easing
	{
		// Token: 0x060018CA RID: 6346 RVA: 0x000811FC File Offset: 0x0007F5FC
		public static float Linear(float t, float b, float c, float d)
		{
			return c * t / d + b;
		}

		// Token: 0x060018CB RID: 6347 RVA: 0x00081205 File Offset: 0x0007F605
		public static float EaseInQuad(float t, float b, float c, float d)
		{
			t /= d;
			return c * t * t + b;
		}

		// Token: 0x060018CC RID: 6348 RVA: 0x00081213 File Offset: 0x0007F613
		public static float EaseOutQuad(float t, float b, float c, float d)
		{
			t /= d;
			return -c * t * (t - 2f) + b;
		}

		// Token: 0x060018CD RID: 6349 RVA: 0x00081228 File Offset: 0x0007F628
		public static float EaseInOutQuad(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return c / 2f * t * t + b;
			}
			t -= 1f;
			return -c / 2f * (t * (t - 2f) - 1f) + b;
		}

		// Token: 0x060018CE RID: 6350 RVA: 0x0008127C File Offset: 0x0007F67C
		public static float EaseInCubic(float t, float b, float c, float d)
		{
			t /= d;
			return c * t * t * t + b;
		}

		// Token: 0x060018CF RID: 6351 RVA: 0x0008128C File Offset: 0x0007F68C
		public static float EaseOutCubic(float t, float b, float c, float d)
		{
			t /= d;
			t -= 1f;
			return c * (t * t * t + 1f) + b;
		}

		// Token: 0x060018D0 RID: 6352 RVA: 0x000812AC File Offset: 0x0007F6AC
		public static float EaseInOutCubic(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return c / 2f * t * t * t + b;
			}
			t -= 2f;
			return c / 2f * (t * t * t + 2f) + b;
		}

		// Token: 0x060018D1 RID: 6353 RVA: 0x000812FD File Offset: 0x0007F6FD
		public static float EaseInQuart(float t, float b, float c, float d)
		{
			t /= d;
			return c * t * t * t * t + b;
		}

		// Token: 0x060018D2 RID: 6354 RVA: 0x0008130F File Offset: 0x0007F70F
		public static float EaseOutQuart(float t, float b, float c, float d)
		{
			t /= d;
			t -= 1f;
			return -c * (t * t * t * t - 1f) + b;
		}

		// Token: 0x060018D3 RID: 6355 RVA: 0x00081334 File Offset: 0x0007F734
		public static float EaseInOutQuart(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return c / 2f * t * t * t * t + b;
			}
			t -= 2f;
			return -c / 2f * (t * t * t * t - 2f) + b;
		}

		// Token: 0x060018D4 RID: 6356 RVA: 0x0008138A File Offset: 0x0007F78A
		public static float EaseInQuint(float t, float b, float c, float d)
		{
			t /= d;
			return c * t * t * t * t * t + b;
		}

		// Token: 0x060018D5 RID: 6357 RVA: 0x0008139E File Offset: 0x0007F79E
		public static float EaseOutQuint(float t, float b, float c, float d)
		{
			t /= d;
			t -= 1f;
			return c * (t * t * t * t * t + 1f) + b;
		}

		// Token: 0x060018D6 RID: 6358 RVA: 0x000813C4 File Offset: 0x0007F7C4
		public static float EaseInOutQuint(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return c / 2f * t * t * t * t * t + b;
			}
			t -= 2f;
			return c / 2f * (t * t * t * t * t + 2f) + b;
		}

		// Token: 0x060018D7 RID: 6359 RVA: 0x0008141D File Offset: 0x0007F81D
		public static float EaseInSine(float t, float b, float c, float d)
		{
			return -c * Mathf.Cos(t / d * 1.5707964f) + c + b;
		}

		// Token: 0x060018D8 RID: 6360 RVA: 0x00081434 File Offset: 0x0007F834
		public static float EaseOutSine(float t, float b, float c, float d)
		{
			return c * Mathf.Sin(t / d * 1.5707964f) + b;
		}

		// Token: 0x060018D9 RID: 6361 RVA: 0x00081448 File Offset: 0x0007F848
		public static float EaseInOutSine(float t, float b, float c, float d)
		{
			return -c / 2f * (Mathf.Cos(3.1415927f * t / d) - 1f) + b;
		}

		// Token: 0x060018DA RID: 6362 RVA: 0x00081469 File Offset: 0x0007F869
		public static float EaseInExpo(float t, float b, float c, float d)
		{
			return c * Mathf.Pow(2f, 10f * (t / d - 1f)) + b;
		}

		// Token: 0x060018DB RID: 6363 RVA: 0x00081488 File Offset: 0x0007F888
		public static float EaseOutExpo(float t, float b, float c, float d)
		{
			return c * (-Mathf.Pow(2f, -10f * t / d) + 1f) + b;
		}

		// Token: 0x060018DC RID: 6364 RVA: 0x000814A8 File Offset: 0x0007F8A8
		public static float EaseInOutExpo(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return c / 2f * Mathf.Pow(2f, 10f * (t - 1f)) + b;
			}
			t -= 1f;
			return c / 2f * (-Mathf.Pow(2f, -10f * t) + 2f) + b;
		}

		// Token: 0x060018DD RID: 6365 RVA: 0x00081518 File Offset: 0x0007F918
		public static float EaseInCirc(float t, float b, float c, float d)
		{
			t /= d;
			return -c * (Mathf.Sqrt(1f - t * t) - 1f) + b;
		}

		// Token: 0x060018DE RID: 6366 RVA: 0x00081538 File Offset: 0x0007F938
		public static float EaseOutCirc(float t, float b, float c, float d)
		{
			t /= d;
			t -= 1f;
			return c * Mathf.Sqrt(1f - t * t) + b;
		}

		// Token: 0x060018DF RID: 6367 RVA: 0x0008155C File Offset: 0x0007F95C
		public static float EaseInOutCirc(float t, float b, float c, float d)
		{
			t /= d / 2f;
			if (t < 1f)
			{
				return -c / 2f * (Mathf.Sqrt(1f - t * t) - 1f) + b;
			}
			t -= 2f;
			return c / 2f * (Mathf.Sqrt(1f - t * t) + 1f) + b;
		}
	}
}
