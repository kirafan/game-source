﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000AC RID: 172
	public static class BattleDefine
	{
		// Token: 0x060004F4 RID: 1268 RVA: 0x0001A6E0 File Offset: 0x00018AE0
		// Note: this type is marked as 'beforefieldinit'.
		static BattleDefine()
		{
			string[,] array = new string[3, 3];
			array[0, 0] = "voice_413";
			array[0, 1] = "voice_413";
			array[0, 2] = "voice_413";
			array[1, 0] = "voice_413";
			array[1, 1] = "voice_414";
			array[1, 2] = "voice_414";
			array[2, 0] = "voice_413";
			array[2, 1] = "voice_414";
			array[2, 2] = "voice_416";
			BattleDefine.EN_VOICE_DAMAGES = array;
			BattleDefine.DAMAGE_VOICE_THRESHOLD = new float[]
			{
				1f,
				0.6f,
				0.2f
			};
			BattleDefine.BG_POS = new Vector3(0f, 0f, 70f);
		}

		// Token: 0x04000335 RID: 821
		public const int PARTY_NUM = 2;

		// Token: 0x04000336 RID: 822
		public const int PARTY_MEMBER_NUM = 5;

		// Token: 0x04000337 RID: 823
		public const int PARTY_MEMBER_NUM_INC_FRIEND = 6;

		// Token: 0x04000338 RID: 824
		public const int JOIN_NUM = 3;

		// Token: 0x04000339 RID: 825
		public const int BENCH_NUM = 2;

		// Token: 0x0400033A RID: 826
		public const int LEADERSKILL_MEMBER_INDEX = 0;

		// Token: 0x0400033B RID: 827
		public const int PL_SKILL_MAX = 4;

		// Token: 0x0400033C RID: 828
		public const string PLAYER_MEMBER_NAME_PREFIX = "P";

		// Token: 0x0400033D RID: 829
		public const string PLAYER_FRIEND_MEMBER_NAME = "PF";

		// Token: 0x0400033E RID: 830
		public const string ENEMY_MEMBER_NAME_PREFIX = "E";

		// Token: 0x0400033F RID: 831
		public const string BATTLE_ACT_KEY_IDLE = "idle";

		// Token: 0x04000340 RID: 832
		public const string BATTLE_ACT_KEY_ABNORMAL = "abnormal";

		// Token: 0x04000341 RID: 833
		public const string BATTLE_ACT_KEY_DAMAGE = "damage";

		// Token: 0x04000342 RID: 834
		public const string BATTLE_ACT_KEY_DEAD = "dead";

		// Token: 0x04000343 RID: 835
		public const string BATTLE_ACT_KEY_RUN = "battle_run";

		// Token: 0x04000344 RID: 836
		public const string BATTLE_ACT_KEY_IN = "battle_in";

		// Token: 0x04000345 RID: 837
		public const string BATTLE_ACT_KEY_OUT = "battle_out";

		// Token: 0x04000346 RID: 838
		public const string BATTLE_ACT_KEY_WALK = "walk";

		// Token: 0x04000347 RID: 839
		public const string BATTLE_ACT_KEY_ATK = "attack";

		// Token: 0x04000348 RID: 840
		public const string BATTLE_ACT_KEY_GUARD = "";

		// Token: 0x04000349 RID: 841
		public const string BATTLE_ACT_KEY_CLS_SKILL = "class_skill_";

		// Token: 0x0400034A RID: 842
		public const string BATTLE_ACT_KEY_EN_SKILL = "skill_";

		// Token: 0x0400034B RID: 843
		public const string BATTLE_ACT_KEY_EN_UNQ_SKILL = "charge_skill";

		// Token: 0x0400034C RID: 844
		public const string BATTLE_ACT_KEY_WIN_ST = "win_st_";

		// Token: 0x0400034D RID: 845
		public const string BATTLE_ACT_KEY_WIN_LP = "win_lp_";

		// Token: 0x0400034E RID: 846
		public const string BATTLE_ACT_KEY_KIRARA_JUMP = "kirarajump_";

		// Token: 0x0400034F RID: 847
		public const int HIT_STOP_FRAME = 1;

		// Token: 0x04000350 RID: 848
		public const int HIT_STOP_FRAME_STUN = 3;

		// Token: 0x04000351 RID: 849
		public const float CHANGE_COLOR_SKILL_TIME = 0.18f;

		// Token: 0x04000352 RID: 850
		public static readonly Color BG_COLOR_SKILL = new Color(0.5f, 0.5f, 0.5f);

		// Token: 0x04000353 RID: 851
		public static readonly Color CHARA_COLOR_SKILL = new Color(0.5f, 0.5f, 0.5f);

		// Token: 0x04000354 RID: 852
		public static readonly Color BG_COLOR_UNIQUESKILL = new Color(0.25f, 0.25f, 0.25f);

		// Token: 0x04000355 RID: 853
		public static readonly Color CHARA_COLOR_UNIQUESKILL = new Color(0.25f, 0.25f, 0.25f);

		// Token: 0x04000356 RID: 854
		public const float LAST_BLOW_TIME_SCALE = 0.5f;

		// Token: 0x04000357 RID: 855
		public const float LAST_BLOW_TIME_SCALE_SEC = 2.4f;

		// Token: 0x04000358 RID: 856
		public const float LAST_BLOW_TIME_SCALE_SEC_REVERT_RATIO = 1f;

		// Token: 0x04000359 RID: 857
		public const string BGM_DEFAULT = "bgm_battle_1";

		// Token: 0x0400035A RID: 858
		public const string BGM_WARNING = "bgm_battle_2";

		// Token: 0x0400035B RID: 859
		public const string EFFECT_PACK_ALLWAYS = "btl_always";

		// Token: 0x0400035C RID: 860
		public const float CHARA_WAVE_IN_FRAME = 6f;

		// Token: 0x0400035D RID: 861
		public const float CHARA_WAVE_IN_DELAY_INTERVAL = 0.15f;

		// Token: 0x0400035E RID: 862
		public const float CHARA_IDLE_DELAY_INTERVAL = 0.2f;

		// Token: 0x0400035F RID: 863
		public static readonly float[] SHADOW_OFFSET_BY_CLASS = new float[]
		{
			0f,
			-0.2f,
			0.28f,
			0.1f,
			0.1f
		};

		// Token: 0x04000360 RID: 864
		public const float SHADOW_OFFSET_Z = 0.3f;

		// Token: 0x04000361 RID: 865
		public static readonly float[] SHADOW_SCALE_RANGE = new float[]
		{
			1.3f,
			1.75f
		};

		// Token: 0x04000362 RID: 866
		public static readonly Vector3 PL_STUN_OFFSET = new Vector3(-0.1f, 0.92f, 0f);

		// Token: 0x04000363 RID: 867
		public const float COLLISION_RADIUS = 0.36f;

		// Token: 0x04000364 RID: 868
		public const float TOGETHER_GAUGE_MAX = 3f;

		// Token: 0x04000365 RID: 869
		public const int FACIAL_ID_PLUSEVENT = 9;

		// Token: 0x04000366 RID: 870
		public const int FACIAL_ID_MINUSEVENT = 17;

		// Token: 0x04000367 RID: 871
		public const eSoundVoiceListDB VOICE_START = eSoundVoiceListDB.voice_401;

		// Token: 0x04000368 RID: 872
		public const eSoundVoiceListDB VOICE_START_WARNING = eSoundVoiceListDB.voice_401;

		// Token: 0x04000369 RID: 873
		public const eSoundVoiceListDB VOICE_KIRARA_JUMP = eSoundVoiceListDB.voice_412;

		// Token: 0x0400036A RID: 874
		public static readonly string[] VOICE_DAMAGES = new string[]
		{
			"voice_413",
			"voice_413",
			"voice_413"
		};

		// Token: 0x0400036B RID: 875
		public const eSoundVoiceListDB VOICE_ABNORMAL = eSoundVoiceListDB.voice_415;

		// Token: 0x0400036C RID: 876
		public const eSoundVoiceListDB VOICE_DEAD = eSoundVoiceListDB.voice_416;

		// Token: 0x0400036D RID: 877
		public const eSoundVoiceListDB VOICE_BATTLE_CLEAR = eSoundVoiceListDB.voice_417;

		// Token: 0x0400036E RID: 878
		public const eSoundVoiceListDB VOICE_GAME_OVER = eSoundVoiceListDB.voice_419;

		// Token: 0x0400036F RID: 879
		public const eSoundVoiceListDB VOICE_CHARA_TOUCH = eSoundVoiceListDB.voice_420;

		// Token: 0x04000370 RID: 880
		public static readonly eSoundVoiceListDB[] VOICE_SKILLS = new eSoundVoiceListDB[]
		{
			eSoundVoiceListDB.voice_403,
			eSoundVoiceListDB.voice_403,
			eSoundVoiceListDB.voice_409,
			eSoundVoiceListDB.voice_406,
			eSoundVoiceListDB.voice_406,
			eSoundVoiceListDB.voice_409,
			eSoundVoiceListDB.voice_411,
			eSoundVoiceListDB.voice_406,
			eSoundVoiceListDB.voice_411
		};

		// Token: 0x04000371 RID: 881
		public const string EN_VOICE_START = "voice_400";

		// Token: 0x04000372 RID: 882
		public static readonly string[,] EN_VOICE_DAMAGES;

		// Token: 0x04000373 RID: 883
		public const string EN_VOICE_DEAD = "voice_418";

		// Token: 0x04000374 RID: 884
		public const string EN_VOICE_GAME_OVER = "voice_419";

		// Token: 0x04000375 RID: 885
		public static readonly float[] DAMAGE_VOICE_THRESHOLD;

		// Token: 0x04000376 RID: 886
		public const int HIT_REGIST = -1;

		// Token: 0x04000377 RID: 887
		public const int HIT_DEFAULT = 0;

		// Token: 0x04000378 RID: 888
		public const int HIT_WEAK = 1;

		// Token: 0x04000379 RID: 889
		public const int AI_FLAG_NUM = 5;

		// Token: 0x0400037A RID: 890
		public static readonly Vector3 BG_POS;

		// Token: 0x0400037B RID: 891
		public const float DEFAULT_CHARA_POS_INTERVAL_Z = 6f;

		// Token: 0x0400037C RID: 892
		public const float SCENE_LINE_CHARA = 0f;

		// Token: 0x0400037D RID: 893
		public const float SCENE_LINE_EFFECT_DEFAULT_FROM_NEARCLIP = 20f;

		// Token: 0x0400037E RID: 894
		public const float SCENE_LINE_EFFECT_DAMAGE_FROM_NEARCLIP = 10f;

		// Token: 0x0400037F RID: 895
		public const int SORTING_ORDER_CHARA_BASE = 0;

		// Token: 0x04000380 RID: 896
		public const int SORTING_ORDER_CHARA_INTERVAL = 10;

		// Token: 0x04000381 RID: 897
		public const int SORTING_ORDER_EFFECT_DEFAULT = 1000;

		// Token: 0x04000382 RID: 898
		public const int SORTING_ORDER_EFFECT_DAMAGE = 1500;

		// Token: 0x04000383 RID: 899
		public const int SORTING_ORDER_BG = -32768;

		// Token: 0x020000AD RID: 173
		public enum ePartyType
		{
			// Token: 0x04000385 RID: 901
			None = -1,
			// Token: 0x04000386 RID: 902
			Player,
			// Token: 0x04000387 RID: 903
			Enemy,
			// Token: 0x04000388 RID: 904
			Num
		}

		// Token: 0x020000AE RID: 174
		public enum eBattlePartySlot
		{
			// Token: 0x0400038A RID: 906
			None = -1,
			// Token: 0x0400038B RID: 907
			Slot_1,
			// Token: 0x0400038C RID: 908
			Slot_2,
			// Token: 0x0400038D RID: 909
			Slot_3,
			// Token: 0x0400038E RID: 910
			Slot_4,
			// Token: 0x0400038F RID: 911
			Slot_5,
			// Token: 0x04000390 RID: 912
			Num,
			// Token: 0x04000391 RID: 913
			Slot_Friend = 5,
			// Token: 0x04000392 RID: 914
			Num_IncludeFriend
		}

		// Token: 0x020000AF RID: 175
		public enum eJoinMember
		{
			// Token: 0x04000394 RID: 916
			None = -1,
			// Token: 0x04000395 RID: 917
			Join_1,
			// Token: 0x04000396 RID: 918
			Join_2,
			// Token: 0x04000397 RID: 919
			Join_3,
			// Token: 0x04000398 RID: 920
			Bench_1,
			// Token: 0x04000399 RID: 921
			Bench_2,
			// Token: 0x0400039A RID: 922
			Bench_Friend
		}

		// Token: 0x020000B0 RID: 176
		public enum eDeadDetail
		{
			// Token: 0x0400039C RID: 924
			Alive,
			// Token: 0x0400039D RID: 925
			Dead_Ready,
			// Token: 0x0400039E RID: 926
			Dead_Complete
		}

		// Token: 0x020000B1 RID: 177
		public enum eFluctuation
		{
			// Token: 0x040003A0 RID: 928
			Down = -1,
			// Token: 0x040003A1 RID: 929
			None,
			// Token: 0x040003A2 RID: 930
			Up
		}

		// Token: 0x020000B2 RID: 178
		public enum eStatus
		{
			// Token: 0x040003A4 RID: 932
			Atk,
			// Token: 0x040003A5 RID: 933
			Mgc,
			// Token: 0x040003A6 RID: 934
			Def,
			// Token: 0x040003A7 RID: 935
			MDef,
			// Token: 0x040003A8 RID: 936
			Spd,
			// Token: 0x040003A9 RID: 937
			Luck,
			// Token: 0x040003AA RID: 938
			Num
		}

		// Token: 0x020000B3 RID: 179
		public enum eFlagForUIStatusIcon
		{
			// Token: 0x040003AC RID: 940
			Stun,
			// Token: 0x040003AD RID: 941
			StatusChange_Atk,
			// Token: 0x040003AE RID: 942
			StatusChange_Mgc,
			// Token: 0x040003AF RID: 943
			StatusChange_Def,
			// Token: 0x040003B0 RID: 944
			StatusChange_MDef,
			// Token: 0x040003B1 RID: 945
			StatusChange_Spd,
			// Token: 0x040003B2 RID: 946
			StatusChange_Luck,
			// Token: 0x040003B3 RID: 947
			StateAbnormal_Confusion,
			// Token: 0x040003B4 RID: 948
			StateAbnormal_Paralysis,
			// Token: 0x040003B5 RID: 949
			StateAbnormal_Poison,
			// Token: 0x040003B6 RID: 950
			StateAbnormal_Bearish,
			// Token: 0x040003B7 RID: 951
			StateAbnormal_Sleep,
			// Token: 0x040003B8 RID: 952
			StateAbnormal_Unhappy,
			// Token: 0x040003B9 RID: 953
			StateAbnormal_Silence,
			// Token: 0x040003BA RID: 954
			StateAbnormal_Isolation,
			// Token: 0x040003BB RID: 955
			ResistElement_Fire,
			// Token: 0x040003BC RID: 956
			ResistElement_Water,
			// Token: 0x040003BD RID: 957
			ResistElement_Earth,
			// Token: 0x040003BE RID: 958
			ResistElement_Wind,
			// Token: 0x040003BF RID: 959
			ResistElement_Moon,
			// Token: 0x040003C0 RID: 960
			ResistElement_Sun,
			// Token: 0x040003C1 RID: 961
			StateAbnormalAdditionalProbability,
			// Token: 0x040003C2 RID: 962
			StateAbnormalDisable,
			// Token: 0x040003C3 RID: 963
			WeakElementBonus,
			// Token: 0x040003C4 RID: 964
			NextAtkUp,
			// Token: 0x040003C5 RID: 965
			NextMgcUp,
			// Token: 0x040003C6 RID: 966
			NextCritical,
			// Token: 0x040003C7 RID: 967
			Barrier,
			// Token: 0x040003C8 RID: 968
			HateChange,
			// Token: 0x040003C9 RID: 969
			Regene,
			// Token: 0x040003CA RID: 970
			Num
		}

		// Token: 0x020000B4 RID: 180
		[Serializable]
		public class BuffData
		{
			// Token: 0x060004F5 RID: 1269 RVA: 0x0001A885 File Offset: 0x00018C85
			public BuffData(eSkillTurnConsume turnConsume, int turn, float value)
			{
				this.Type = turnConsume;
				this.Turn = turn;
				this.Val = value;
			}

			// Token: 0x060004F6 RID: 1270 RVA: 0x0001A8A2 File Offset: 0x00018CA2
			public void Reset()
			{
				this.Turn = 0;
				this.Val = 0f;
			}

			// Token: 0x040003CB RID: 971
			public eSkillTurnConsume Type;

			// Token: 0x040003CC RID: 972
			public int Turn;

			// Token: 0x040003CD RID: 973
			public float Val;
		}

		// Token: 0x020000B5 RID: 181
		[Serializable]
		public class BuffStack
		{
			// Token: 0x060004F7 RID: 1271 RVA: 0x0001A8B6 File Offset: 0x00018CB6
			public BuffStack()
			{
				this.Stack = new List<BattleDefine.BuffData>();
			}

			// Token: 0x060004F8 RID: 1272 RVA: 0x0001A8C9 File Offset: 0x00018CC9
			public void Add(BattleDefine.BuffData buff)
			{
				this.Stack.Add(buff);
			}

			// Token: 0x060004F9 RID: 1273 RVA: 0x0001A8D7 File Offset: 0x00018CD7
			public void Clear()
			{
				this.Stack.Clear();
			}

			// Token: 0x060004FA RID: 1274 RVA: 0x0001A8E4 File Offset: 0x00018CE4
			public bool IsEnable()
			{
				return this.Stack.Count > 0;
			}

			// Token: 0x060004FB RID: 1275 RVA: 0x0001A8FC File Offset: 0x00018CFC
			public float GetValue(bool isSpd = false)
			{
				float num = 0f;
				if (!isSpd)
				{
					for (int i = 0; i < this.Stack.Count; i++)
					{
						if (this.Stack[i].Turn > 0)
						{
							num += this.Stack[i].Val;
						}
					}
				}
				else
				{
					float num2 = 0f;
					if (this.Stack.Count > 0)
					{
						for (int j = 0; j < this.Stack.Count; j++)
						{
							if (this.Stack[j].Turn > 0)
							{
								num2 += 1f - this.Stack[j].Val;
							}
						}
						num = 1f - num2;
					}
				}
				return num;
			}

			// Token: 0x060004FC RID: 1276 RVA: 0x0001A9D8 File Offset: 0x00018DD8
			public void DecreaseTurn(bool isMyTurn)
			{
				for (int i = this.Stack.Count - 1; i >= 0; i--)
				{
					if (this.Stack[i].Type != eSkillTurnConsume.Self || isMyTurn)
					{
						this.Stack[i].Turn--;
						if (this.Stack[i].Turn <= 0)
						{
							this.Stack.RemoveAt(i);
						}
					}
				}
			}

			// Token: 0x060004FD RID: 1277 RVA: 0x0001AA61 File Offset: 0x00018E61
			public int GetStackNum()
			{
				return this.Stack.Count;
			}

			// Token: 0x060004FE RID: 1278 RVA: 0x0001AA6E File Offset: 0x00018E6E
			public BattleDefine.BuffData GetStackAt(int index)
			{
				return this.Stack[index];
			}

			// Token: 0x040003CE RID: 974
			[SerializeField]
			private List<BattleDefine.BuffData> Stack;
		}

		// Token: 0x020000B6 RID: 182
		[Serializable]
		public class StateAbnormal
		{
			// Token: 0x060004FF RID: 1279 RVA: 0x0001AA7C File Offset: 0x00018E7C
			public StateAbnormal()
			{
			}

			// Token: 0x06000500 RID: 1280 RVA: 0x0001AA84 File Offset: 0x00018E84
			public StateAbnormal(bool isRegist, int turn)
			{
				this.IsRegist = isRegist;
				this.Turn = turn;
			}

			// Token: 0x06000501 RID: 1281 RVA: 0x0001AA9A File Offset: 0x00018E9A
			public void Reset()
			{
				this.Turn = 0;
			}

			// Token: 0x06000502 RID: 1282 RVA: 0x0001AAA3 File Offset: 0x00018EA3
			public bool IsEnable()
			{
				return this.Turn > 0;
			}

			// Token: 0x06000503 RID: 1283 RVA: 0x0001AAB4 File Offset: 0x00018EB4
			public void DecreaseTurn(bool isMyTurn)
			{
				if (this.Turn > 0)
				{
					this.Turn--;
				}
			}

			// Token: 0x040003CF RID: 975
			public bool IsRegist;

			// Token: 0x040003D0 RID: 976
			public int Turn;
		}

		// Token: 0x020000B7 RID: 183
		public enum eNextBuff
		{
			// Token: 0x040003D2 RID: 978
			Attack,
			// Token: 0x040003D3 RID: 979
			Magic,
			// Token: 0x040003D4 RID: 980
			Critical,
			// Token: 0x040003D5 RID: 981
			Num
		}

		// Token: 0x020000B8 RID: 184
		[Serializable]
		public class NextBuff
		{
			// Token: 0x06000505 RID: 1285 RVA: 0x0001AAD8 File Offset: 0x00018ED8
			public void Reset()
			{
				this.Step = BattleDefine.NextBuff.eStep.None;
				this.Val = 0f;
			}

			// Token: 0x06000506 RID: 1286 RVA: 0x0001AAEC File Offset: 0x00018EEC
			public void SetUsedIfAvailable()
			{
				if (this.Step == BattleDefine.NextBuff.eStep.Ready)
				{
					this.Step = BattleDefine.NextBuff.eStep.Used;
				}
			}

			// Token: 0x06000507 RID: 1287 RVA: 0x0001AB00 File Offset: 0x00018F00
			public void ResetIfUsed()
			{
				if (this.Step == BattleDefine.NextBuff.eStep.Used)
				{
					this.Reset();
				}
			}

			// Token: 0x06000508 RID: 1288 RVA: 0x0001AB14 File Offset: 0x00018F14
			public float GetCoef()
			{
				if (this.Step >= BattleDefine.NextBuff.eStep.Ready && this.Step <= BattleDefine.NextBuff.eStep.Used)
				{
					return this.Val;
				}
				return 0f;
			}

			// Token: 0x040003D6 RID: 982
			public BattleDefine.NextBuff.eStep Step;

			// Token: 0x040003D7 RID: 983
			public float Val;

			// Token: 0x020000B9 RID: 185
			public enum eStep
			{
				// Token: 0x040003D9 RID: 985
				None = -1,
				// Token: 0x040003DA RID: 986
				Ready,
				// Token: 0x040003DB RID: 987
				Used
			}
		}

		// Token: 0x020000BA RID: 186
		[Serializable]
		public class RegeneData
		{
			// Token: 0x06000509 RID: 1289 RVA: 0x0001AB3A File Offset: 0x00018F3A
			public RegeneData(string ownerName, int turn, int power)
			{
				this.OwnerName = ownerName;
				this.Turn = turn;
				this.Pow = power;
			}

			// Token: 0x0600050A RID: 1290 RVA: 0x0001AB57 File Offset: 0x00018F57
			public void Reset()
			{
				this.OwnerName = null;
				this.Turn = 0;
				this.Pow = 0;
			}

			// Token: 0x0600050B RID: 1291 RVA: 0x0001AB6E File Offset: 0x00018F6E
			public bool IsEnable()
			{
				return this.Turn > 0;
			}

			// Token: 0x0600050C RID: 1292 RVA: 0x0001AB7F File Offset: 0x00018F7F
			public void DecreaseTurn(bool isMyTurn)
			{
				if (!isMyTurn)
				{
					return;
				}
				this.Turn--;
				if (this.Turn <= 0)
				{
					this.Reset();
				}
			}

			// Token: 0x040003DC RID: 988
			public string OwnerName;

			// Token: 0x040003DD RID: 989
			public int Turn;

			// Token: 0x040003DE RID: 990
			public int Pow;
		}

		// Token: 0x020000BB RID: 187
		public enum eTurnPreEndEvent
		{
			// Token: 0x040003E0 RID: 992
			None = -1,
			// Token: 0x040003E1 RID: 993
			AwakeFromSleep,
			// Token: 0x040003E2 RID: 994
			AbnormalPoison,
			// Token: 0x040003E3 RID: 995
			Regene,
			// Token: 0x040003E4 RID: 996
			Num
		}
	}
}
