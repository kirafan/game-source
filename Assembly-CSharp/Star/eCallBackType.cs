﻿using System;

namespace Star
{
	// Token: 0x0200032B RID: 811
	public enum eCallBackType
	{
		// Token: 0x040016CA RID: 5834
		MoveBuild,
		// Token: 0x040016CB RID: 5835
		SwapBuild,
		// Token: 0x040016CC RID: 5836
		RemoveBuild,
		// Token: 0x040016CD RID: 5837
		ScheduleChange,
		// Token: 0x040016CE RID: 5838
		Event,
		// Token: 0x040016CF RID: 5839
		StayChange,
		// Token: 0x040016D0 RID: 5840
		CompBuild,
		// Token: 0x040016D1 RID: 5841
		LevelUp
	}
}
