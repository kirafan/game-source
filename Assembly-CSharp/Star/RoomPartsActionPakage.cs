﻿using System;

namespace Star
{
	// Token: 0x0200061B RID: 1563
	public class RoomPartsActionPakage
	{
		// Token: 0x06001E81 RID: 7809 RVA: 0x000A4F6D File Offset: 0x000A336D
		public RoomPartsActionPakage()
		{
			this.m_Table = new IRoomPartsAction[8];
		}

		// Token: 0x06001E82 RID: 7810 RVA: 0x000A4F84 File Offset: 0x000A3384
		public void EntryAction(IRoomPartsAction paction, uint fuid = 0U)
		{
			Type type = paction.GetType();
			bool flag = false;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].GetType() == type)
				{
					this.m_Table[i] = null;
					this.m_Table[i] = paction;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				if (this.m_Num < 8)
				{
					this.m_Table[this.m_Num] = paction;
					paction.m_UID = fuid;
					this.m_Num++;
				}
				else
				{
					paction = null;
				}
			}
		}

		// Token: 0x06001E83 RID: 7811 RVA: 0x000A501C File Offset: 0x000A341C
		public IRoomPartsAction GetAction(uint fuid)
		{
			IRoomPartsAction result = null;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].m_UID == fuid)
				{
					result = this.m_Table[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06001E84 RID: 7812 RVA: 0x000A5064 File Offset: 0x000A3464
		public IRoomPartsAction GetAction(Type ftype)
		{
			IRoomPartsAction result = null;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].GetType() == ftype)
				{
					result = this.m_Table[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06001E85 RID: 7813 RVA: 0x000A50AC File Offset: 0x000A34AC
		public void PakageUpdate()
		{
			int num = 0;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (!this.m_Table[i].PartsUpdate())
				{
					this.m_Table[i] = null;
					num++;
				}
			}
			if (num != 0)
			{
				int i = 0;
				while (i < this.m_Num)
				{
					if (this.m_Table[i] == null)
					{
						for (int j = i + 1; j < this.m_Num; j++)
						{
							this.m_Table[j - 1] = this.m_Table[j];
						}
						this.m_Num--;
						this.m_Table[this.m_Num] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x06001E86 RID: 7814 RVA: 0x000A5168 File Offset: 0x000A3568
		public void CancelAction()
		{
			for (int i = 0; i < this.m_Num; i++)
			{
				this.m_Table[i].m_Active = false;
				this.m_Table[i].PartsCancel();
				this.m_Table[i] = null;
			}
		}

		// Token: 0x06001E87 RID: 7815 RVA: 0x000A51B0 File Offset: 0x000A35B0
		public void Release()
		{
			for (int i = 0; i < 8; i++)
			{
				if (this.m_Table[i] != null)
				{
					this.m_Table[i].PartsCancel();
				}
				this.m_Table[i] = null;
			}
			this.m_Table = null;
		}

		// Token: 0x04002527 RID: 9511
		public int m_Num;

		// Token: 0x04002528 RID: 9512
		public IRoomPartsAction[] m_Table;
	}
}
