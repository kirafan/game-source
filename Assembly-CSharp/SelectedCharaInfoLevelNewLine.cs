﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x020007E0 RID: 2016
public class SelectedCharaInfoLevelNewLine : MonoBehaviour
{
	// Token: 0x06002989 RID: 10633 RVA: 0x000DBEAD File Offset: 0x000DA2AD
	private void Start()
	{
	}

	// Token: 0x0600298A RID: 10634 RVA: 0x000DBEB0 File Offset: 0x000DA2B0
	private void Update()
	{
		if (this.m_Source != null && (this.m_PreSource == null || this.m_PreSource != this.m_Source.text))
		{
			this.m_PreSource = this.m_Source.text;
			string[] array = this.m_PreSource.Split(new char[]
			{
				'/'
			});
			if (array.Length == 2)
			{
				string text = array[0];
				string text2 = array[1];
				if (this.m_NewLineType == SelectedCharaInfoLevelNewLine.eNewLineType.BeforeSlash)
				{
					text2 = "/" + text2;
				}
				else
				{
					text += "/";
				}
				if (this.m_BeforeDestination != null)
				{
					this.m_BeforeDestination.text = text;
				}
				if (this.m_AfterDestination != null)
				{
					this.m_AfterDestination.text = text2;
				}
			}
		}
	}

	// Token: 0x04003015 RID: 12309
	[SerializeField]
	private Text m_Source;

	// Token: 0x04003016 RID: 12310
	[SerializeField]
	private Text m_BeforeDestination;

	// Token: 0x04003017 RID: 12311
	[SerializeField]
	private Text m_AfterDestination;

	// Token: 0x04003018 RID: 12312
	[SerializeField]
	private SelectedCharaInfoLevelNewLine.eNewLineType m_NewLineType;

	// Token: 0x04003019 RID: 12313
	private string m_PreSource;

	// Token: 0x020007E1 RID: 2017
	public enum eNewLineType
	{
		// Token: 0x0400301B RID: 12315
		BeforeSlash,
		// Token: 0x0400301C RID: 12316
		AfterSlash
	}
}
