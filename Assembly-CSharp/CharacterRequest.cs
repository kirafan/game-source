﻿using System;
using CharacterRequestTypes;
using Meige;

// Token: 0x02000B5C RID: 2908
public static class CharacterRequest
{
	// Token: 0x06003DD5 RID: 15829 RVA: 0x001382A8 File Offset: 0x001366A8
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003DD6 RID: 15830 RVA: 0x001382CC File Offset: 0x001366CC
	public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003DD7 RID: 15831 RVA: 0x001382F0 File Offset: 0x001366F0
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("character/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("characterId", param.characterId);
		return meigewwwParam;
	}

	// Token: 0x06003DD8 RID: 15832 RVA: 0x00138324 File Offset: 0x00136724
	public static MeigewwwParam Get(string characterId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("character/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("characterId", characterId);
		return meigewwwParam;
	}
}
