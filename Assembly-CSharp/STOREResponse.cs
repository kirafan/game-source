﻿using System;
using System.Collections.Generic;
using Meige;
using STOREResponseTypes;
using WWWTypes;

// Token: 0x02000C0A RID: 3082
public static class STOREResponse
{
	// Token: 0x06003FBA RID: 16314 RVA: 0x0013D018 File Offset: 0x0013B418
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FBB RID: 16315 RVA: 0x0013D030 File Offset: 0x0013B430
	public static PurchaseAppStore PurchaseAppStore(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<PurchaseAppStore>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FBC RID: 16316 RVA: 0x0013D048 File Offset: 0x0013B448
	public static PurchaseGooglePlay PurchaseGooglePlay(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<PurchaseGooglePlay>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FBD RID: 16317 RVA: 0x0013D060 File Offset: 0x0013B460
	public static GetGooglePayload GetGooglePayload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetGooglePayload>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FBE RID: 16318 RVA: 0x0013D078 File Offset: 0x0013B478
	public static GetApplePayload GetApplePayload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetApplePayload>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FBF RID: 16319 RVA: 0x0013D090 File Offset: 0x0013B490
	public static GetPurchaseLog GetPurchaseLog(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetPurchaseLog>(param, dialogType, acceptableResultCodes);
	}
}
