﻿using System;
using Meige;
using QuestChapterRequestTypes;

// Token: 0x02000B68 RID: 2920
public static class QuestChapterRequest
{
	// Token: 0x06003E73 RID: 15987 RVA: 0x0013A87C File Offset: 0x00138C7C
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("quest_chapter/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("chapterId", param.chapterId);
		return meigewwwParam;
	}

	// Token: 0x06003E74 RID: 15988 RVA: 0x0013A8B0 File Offset: 0x00138CB0
	public static MeigewwwParam Get(string chapterId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("quest_chapter/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("chapterId", chapterId);
		return meigewwwParam;
	}

	// Token: 0x06003E75 RID: 15989 RVA: 0x0013A8E0 File Offset: 0x00138CE0
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("quest_chapter/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E76 RID: 15990 RVA: 0x0013A904 File Offset: 0x00138D04
	public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("quest_chapter/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}
}
