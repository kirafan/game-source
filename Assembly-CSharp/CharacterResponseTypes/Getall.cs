﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace CharacterResponseTypes
{
	// Token: 0x02000C13 RID: 3091
	public class Getall : CommonResponse
	{
		// Token: 0x06003FDE RID: 16350 RVA: 0x0013D448 File Offset: 0x0013B848
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.characters == null) ? string.Empty : this.characters.ToString());
		}

		// Token: 0x040045D0 RID: 17872
		public Character[] characters;
	}
}
