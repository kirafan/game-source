﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace CharacterResponseTypes
{
	// Token: 0x02000C14 RID: 3092
	public class Get : CommonResponse
	{
		// Token: 0x06003FE0 RID: 16352 RVA: 0x0013D48C File Offset: 0x0013B88C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.characters == null) ? string.Empty : this.characters.ToString());
		}

		// Token: 0x040045D1 RID: 17873
		public Character[] characters;
	}
}
