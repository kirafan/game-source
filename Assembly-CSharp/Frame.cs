﻿using System;
using UnityEngine;

// Token: 0x02000FA4 RID: 4004
public sealed class Frame
{
	// Token: 0x060052AC RID: 21164 RVA: 0x00171FF2 File Offset: 0x001703F2
	private Frame()
	{
	}

	// Token: 0x17000591 RID: 1425
	// (get) Token: 0x060052AD RID: 21165 RVA: 0x00171FFA File Offset: 0x001703FA
	public static Frame Instance
	{
		get
		{
			return Frame.m_Instance;
		}
	}

	// Token: 0x060052AE RID: 21166 RVA: 0x00172001 File Offset: 0x00170401
	public static void Create()
	{
		if (Frame.m_Instance == null)
		{
			Frame.m_Instance = new Frame();
		}
		Frame.m_Instance.Init();
	}

	// Token: 0x060052AF RID: 21167 RVA: 0x00172024 File Offset: 0x00170424
	public void Init()
	{
		this.m_DeltaOrg = 0f;
		this.m_Fps = 0f;
		this.m_GameFps = ((Application.targetFrameRate > 0) ? Application.targetFrameRate : 30);
		this.m_MaxSkip = Mathf.RoundToInt(1f / Time.maximumDeltaTime);
	}

	// Token: 0x17000592 RID: 1426
	// (get) Token: 0x060052B0 RID: 21168 RVA: 0x0017207A File Offset: 0x0017047A
	public float deltaOrg
	{
		get
		{
			return this.m_DeltaOrg;
		}
	}

	// Token: 0x17000593 RID: 1427
	// (get) Token: 0x060052B1 RID: 21169 RVA: 0x00172082 File Offset: 0x00170482
	public float updateDelta
	{
		get
		{
			return this.m_UpdateDelta;
		}
	}

	// Token: 0x17000594 RID: 1428
	// (get) Token: 0x060052B2 RID: 21170 RVA: 0x0017208A File Offset: 0x0017048A
	public float delta
	{
		get
		{
			return this.m_Delta;
		}
	}

	// Token: 0x17000595 RID: 1429
	// (get) Token: 0x060052B3 RID: 21171 RVA: 0x00172092 File Offset: 0x00170492
	public float deltaSecOrg
	{
		get
		{
			return this.m_DeltaSecOrg;
		}
	}

	// Token: 0x17000596 RID: 1430
	// (get) Token: 0x060052B4 RID: 21172 RVA: 0x0017209A File Offset: 0x0017049A
	public float deltaSec
	{
		get
		{
			return this.m_DeltaSec;
		}
	}

	// Token: 0x17000597 RID: 1431
	// (get) Token: 0x060052B5 RID: 21173 RVA: 0x001720A2 File Offset: 0x001704A2
	public float fps
	{
		get
		{
			return this.m_Fps;
		}
	}

	// Token: 0x060052B6 RID: 21174 RVA: 0x001720AA File Offset: 0x001704AA
	public int GetFps()
	{
		return this.mNowFrameRate;
	}

	// Token: 0x060052B7 RID: 21175 RVA: 0x001720B2 File Offset: 0x001704B2
	public float FrameToSecond(float frame)
	{
		return frame / (float)this.m_GameFps;
	}

	// Token: 0x060052B8 RID: 21176 RVA: 0x001720BE File Offset: 0x001704BE
	public int SecondToFrame(float sec)
	{
		return (int)(sec * (float)this.m_GameFps);
	}

	// Token: 0x060052B9 RID: 21177 RVA: 0x001720CC File Offset: 0x001704CC
	public void Update()
	{
		float deltaTime = Time.deltaTime;
		this.m_UpdateDelta = Mathf.Min(deltaTime * (float)this.m_GameFps, (float)this.m_MaxSkip);
	}

	// Token: 0x060052BA RID: 21178 RVA: 0x001720FC File Offset: 0x001704FC
	public void LateUpdate()
	{
		float deltaTime = Time.deltaTime;
		this.m_DeltaOrg = Mathf.Min(deltaTime * (float)this.m_GameFps, (float)this.m_MaxSkip);
		this.m_DeltaSecOrg = this.FrameToSecond(this.m_DeltaOrg);
		this.m_Fps = 1f / this.m_DeltaSecOrg;
		this.m_Delta = this.m_DeltaOrg;
		this.m_DeltaSec = this.m_DeltaSecOrg;
	}

	// Token: 0x04005418 RID: 21528
	private static Frame m_Instance;

	// Token: 0x04005419 RID: 21529
	private int m_GameFps;

	// Token: 0x0400541A RID: 21530
	private int m_MaxSkip;

	// Token: 0x0400541B RID: 21531
	private float m_Fps;

	// Token: 0x0400541C RID: 21532
	private float m_DeltaOrg;

	// Token: 0x0400541D RID: 21533
	private float m_DeltaSecOrg;

	// Token: 0x0400541E RID: 21534
	private float m_Delta;

	// Token: 0x0400541F RID: 21535
	private float m_DeltaSec;

	// Token: 0x04005420 RID: 21536
	private float m_UpdateDelta;

	// Token: 0x04005421 RID: 21537
	private int mNowFrameRate;
}
