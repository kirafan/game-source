﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace WeaponResponseTypes
{
	// Token: 0x02000C92 RID: 3218
	public class Get : CommonResponse
	{
		// Token: 0x060040DD RID: 16605 RVA: 0x0013FECC File Offset: 0x0013E2CC
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.weapons == null) ? string.Empty : this.weapons.ToString());
		}

		// Token: 0x04004699 RID: 18073
		public Weapon[] weapons;
	}
}
