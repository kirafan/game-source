﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace WeaponResponseTypes
{
	// Token: 0x02000C91 RID: 3217
	public class Getall : CommonResponse
	{
		// Token: 0x060040DB RID: 16603 RVA: 0x0013FE88 File Offset: 0x0013E288
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.weapons == null) ? string.Empty : this.weapons.ToString());
		}

		// Token: 0x04004698 RID: 18072
		public Weapon[] weapons;
	}
}
