﻿using System;
using UnityEngine;

// Token: 0x02000CD4 RID: 3284
public class AnimationTargetJointInfo : MonoBehaviour
{
	// Token: 0x06004123 RID: 16675 RVA: 0x001405D0 File Offset: 0x0013E9D0
	public void Make(SkinnedMeshRenderer smr)
	{
		this.m_NameArray = new string[smr.bones.Length];
		int num = 0;
		foreach (Transform transform in smr.bones)
		{
			this.m_NameArray[num] = transform.gameObject.name;
			num++;
		}
	}

	// Token: 0x06004124 RID: 16676 RVA: 0x00140628 File Offset: 0x0013EA28
	public void Attach(GameObject treeGO)
	{
		Transform[] componentsInChildren = treeGO.GetComponentsInChildren<Transform>();
		this.Attach(componentsInChildren);
	}

	// Token: 0x06004125 RID: 16677 RVA: 0x00140644 File Offset: 0x0013EA44
	public void Attach(Transform[] allBones)
	{
		if (this.m_NameArray == null || this.m_SMR == null)
		{
			return;
		}
		if (this.m_NameArray.Length <= 0)
		{
			return;
		}
		Transform[] array = new Transform[this.m_NameArray.Length];
		for (int i = 0; i < this.m_NameArray.Length; i++)
		{
			array[i] = null;
			for (int j = 0; j < allBones.Length; j++)
			{
				if (allBones[j].gameObject.name == this.m_NameArray[i])
				{
					array[i] = allBones[j];
					break;
				}
			}
			Helper.Break(array[i] != null, "Error : This bone isn't existed bones");
		}
		this.m_SMR.bones = array;
		this.m_SMR.updateWhenOffscreen = true;
	}

	// Token: 0x06004126 RID: 16678 RVA: 0x00140711 File Offset: 0x0013EB11
	private void Start()
	{
		this.m_SMR = base.GetComponentInChildren<SkinnedMeshRenderer>();
		this.m_BBTimer = 5;
	}

	// Token: 0x06004127 RID: 16679 RVA: 0x00140728 File Offset: 0x0013EB28
	private void Update()
	{
		if (this.m_BBTimer <= 0)
		{
			return;
		}
		if (this.m_BBTimer-- <= 0)
		{
			this.m_SMR.updateWhenOffscreen = false;
		}
	}

	// Token: 0x04004976 RID: 18806
	public string[] m_NameArray;

	// Token: 0x04004977 RID: 18807
	private SkinnedMeshRenderer m_SMR;

	// Token: 0x04004978 RID: 18808
	private int m_BBTimer;
}
