﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerResponseTypes;
using WWWTypes;

// Token: 0x02000C03 RID: 3075
public static class PlayerResponse
{
	// Token: 0x06003F7C RID: 16252 RVA: 0x0013C760 File Offset: 0x0013AB60
	public static Signup Signup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Signup>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F7D RID: 16253 RVA: 0x0013C778 File Offset: 0x0013AB78
	public static Login Login(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Login>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F7E RID: 16254 RVA: 0x0013C790 File Offset: 0x0013AB90
	public static Setpushtoken Setpushtoken(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Setpushtoken>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F7F RID: 16255 RVA: 0x0013C7A8 File Offset: 0x0013ABA8
	public static Moveget Moveget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Moveget>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F80 RID: 16256 RVA: 0x0013C7C0 File Offset: 0x0013ABC0
	public static Moveset Moveset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Moveset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F81 RID: 16257 RVA: 0x0013C7D8 File Offset: 0x0013ABD8
	public static Linkmoveset Linkmoveset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Linkmoveset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F82 RID: 16258 RVA: 0x0013C7F0 File Offset: 0x0013ABF0
	public static Reset Reset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Reset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F83 RID: 16259 RVA: 0x0013C808 File Offset: 0x0013AC08
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F84 RID: 16260 RVA: 0x0013C820 File Offset: 0x0013AC20
	public static Getbadgecount Getbadgecount(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getbadgecount>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F85 RID: 16261 RVA: 0x0013C838 File Offset: 0x0013AC38
	public static Resumeget Resumeget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Resumeget>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F86 RID: 16262 RVA: 0x0013C850 File Offset: 0x0013AC50
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F87 RID: 16263 RVA: 0x0013C868 File Offset: 0x0013AC68
	public static Setpushnotification Setpushnotification(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Setpushnotification>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F88 RID: 16264 RVA: 0x0013C880 File Offset: 0x0013AC80
	public static Setage Setage(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Setage>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F89 RID: 16265 RVA: 0x0013C898 File Offset: 0x0013AC98
	public static Loginbonus Loginbonus(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Loginbonus>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8A RID: 16266 RVA: 0x0013C8B0 File Offset: 0x0013ACB0
	public static Itemgetsummary Itemgetsummary(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Itemgetsummary>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8B RID: 16267 RVA: 0x0013C8C8 File Offset: 0x0013ACC8
	public static Itemsale Itemsale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Itemsale>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8C RID: 16268 RVA: 0x0013C8E0 File Offset: 0x0013ACE0
	public static Weapongetall Weapongetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Weapongetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8D RID: 16269 RVA: 0x0013C8F8 File Offset: 0x0013ACF8
	public static Weaponmake Weaponmake(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Weaponmake>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8E RID: 16270 RVA: 0x0013C910 File Offset: 0x0013AD10
	public static Weaponupgrade Weaponupgrade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Weaponupgrade>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F8F RID: 16271 RVA: 0x0013C928 File Offset: 0x0013AD28
	public static Weaponsale Weaponsale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Weaponsale>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F90 RID: 16272 RVA: 0x0013C940 File Offset: 0x0013AD40
	public static Weaponlimitadd Weaponlimitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Weaponlimitadd>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F91 RID: 16273 RVA: 0x0013C958 File Offset: 0x0013AD58
	public static Charactergetall Charactergetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Charactergetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F92 RID: 16274 RVA: 0x0013C970 File Offset: 0x0013AD70
	public static Charactersetshown Charactersetshown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Charactersetshown>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F93 RID: 16275 RVA: 0x0013C988 File Offset: 0x0013AD88
	public static Characterlimitbreak Characterlimitbreak(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Characterlimitbreak>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F94 RID: 16276 RVA: 0x0013C9A0 File Offset: 0x0013ADA0
	public static Characterupgrade Characterupgrade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Characterupgrade>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F95 RID: 16277 RVA: 0x0013C9B8 File Offset: 0x0013ADB8
	public static Characterevolution Characterevolution(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Characterevolution>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F96 RID: 16278 RVA: 0x0013C9D0 File Offset: 0x0013ADD0
	public static Battlepartysetname Battlepartysetname(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Battlepartysetname>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F97 RID: 16279 RVA: 0x0013C9E8 File Offset: 0x0013ADE8
	public static Battlepartysetall Battlepartysetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Battlepartysetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F98 RID: 16280 RVA: 0x0013CA00 File Offset: 0x0013AE00
	public static Battlepartygetall Battlepartygetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Battlepartygetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F99 RID: 16281 RVA: 0x0013CA18 File Offset: 0x0013AE18
	public static Masterorbgetall Masterorbgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Masterorbgetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9A RID: 16282 RVA: 0x0013CA30 File Offset: 0x0013AE30
	public static Masterorbset Masterorbset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Masterorbset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9B RID: 16283 RVA: 0x0013CA48 File Offset: 0x0013AE48
	public static Questgetall Questgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questgetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9C RID: 16284 RVA: 0x0013CA60 File Offset: 0x0013AE60
	public static Questlogadd Questlogadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogadd>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9D RID: 16285 RVA: 0x0013CA78 File Offset: 0x0013AE78
	public static Questlogretry Questlogretry(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogretry>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9E RID: 16286 RVA: 0x0013CA90 File Offset: 0x0013AE90
	public static Questlogset Questlogset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F9F RID: 16287 RVA: 0x0013CAA8 File Offset: 0x0013AEA8
	public static Questloggetall Questloggetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questloggetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA0 RID: 16288 RVA: 0x0013CAC0 File Offset: 0x0013AEC0
	public static Questlogsave Questlogsave(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogsave>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA1 RID: 16289 RVA: 0x0013CAD8 File Offset: 0x0013AED8
	public static Questlogload Questlogload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogload>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA2 RID: 16290 RVA: 0x0013CAF0 File Offset: 0x0013AEF0
	public static Questlogreset Questlogreset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Questlogreset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA3 RID: 16291 RVA: 0x0013CB08 File Offset: 0x0013AF08
	public static Presentgetall Presentgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Presentgetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA4 RID: 16292 RVA: 0x0013CB20 File Offset: 0x0013AF20
	public static Presentreceived Presentreceived(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Presentreceived>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA5 RID: 16293 RVA: 0x0013CB38 File Offset: 0x0013AF38
	public static Presentget Presentget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Presentget>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA6 RID: 16294 RVA: 0x0013CB50 File Offset: 0x0013AF50
	public static Staminaadd Staminaadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Staminaadd>(param, dialogType, acceptableResultCodes);
	}
}
