﻿using System;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D3F RID: 3391
	public interface Result
	{
		// Token: 0x06004438 RID: 17464
		Status getStatus();
	}
}
