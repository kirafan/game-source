﻿using System;
using Google.Developers;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D3D RID: 3389
	public class GoogleApiClient : JavaObjWrapper
	{
		// Token: 0x06004419 RID: 17433 RVA: 0x00145E75 File Offset: 0x00144275
		public GoogleApiClient(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x0600441A RID: 17434 RVA: 0x00145E7E File Offset: 0x0014427E
		public GoogleApiClient() : base("com.google.android.gms.common.api.GoogleApiClient")
		{
		}

		// Token: 0x0600441B RID: 17435 RVA: 0x00145E8B File Offset: 0x0014428B
		public object getContext()
		{
			return base.InvokeCall<object>("getContext", "()Landroid/content/Context;", new object[0]);
		}

		// Token: 0x0600441C RID: 17436 RVA: 0x00145EA3 File Offset: 0x001442A3
		public void connect()
		{
			base.InvokeCallVoid("connect", "()V", new object[0]);
		}

		// Token: 0x0600441D RID: 17437 RVA: 0x00145EBB File Offset: 0x001442BB
		public void disconnect()
		{
			base.InvokeCallVoid("disconnect", "()V", new object[0]);
		}

		// Token: 0x0600441E RID: 17438 RVA: 0x00145ED3 File Offset: 0x001442D3
		public void dump(string arg_string_1, object arg_object_2, object arg_object_3, string[] arg_string_4)
		{
			base.InvokeCallVoid("dump", "(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V", new object[]
			{
				arg_string_1,
				arg_object_2,
				arg_object_3,
				arg_string_4
			});
		}

		// Token: 0x0600441F RID: 17439 RVA: 0x00145EFC File Offset: 0x001442FC
		public ConnectionResult blockingConnect(long arg_long_1, object arg_object_2)
		{
			return base.InvokeCall<ConnectionResult>("blockingConnect", "(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;", new object[]
			{
				arg_long_1,
				arg_object_2
			});
		}

		// Token: 0x06004420 RID: 17440 RVA: 0x00145F21 File Offset: 0x00144321
		public ConnectionResult blockingConnect()
		{
			return base.InvokeCall<ConnectionResult>("blockingConnect", "()Lcom/google/android/gms/common/ConnectionResult;", new object[0]);
		}

		// Token: 0x06004421 RID: 17441 RVA: 0x00145F39 File Offset: 0x00144339
		public PendingResult<Status> clearDefaultAccountAndReconnect()
		{
			return base.InvokeCall<PendingResult<Status>>("clearDefaultAccountAndReconnect", "()Lcom/google/android/gms/common/api/PendingResult;", new object[0]);
		}

		// Token: 0x06004422 RID: 17442 RVA: 0x00145F51 File Offset: 0x00144351
		public ConnectionResult getConnectionResult(object arg_object_1)
		{
			return base.InvokeCall<ConnectionResult>("getConnectionResult", "(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/ConnectionResult;", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004423 RID: 17443 RVA: 0x00145F6D File Offset: 0x0014436D
		public int getSessionId()
		{
			return base.InvokeCall<int>("getSessionId", "()I", new object[0]);
		}

		// Token: 0x06004424 RID: 17444 RVA: 0x00145F85 File Offset: 0x00144385
		public bool isConnecting()
		{
			return base.InvokeCall<bool>("isConnecting", "()Z", new object[0]);
		}

		// Token: 0x06004425 RID: 17445 RVA: 0x00145F9D File Offset: 0x0014439D
		public bool isConnectionCallbacksRegistered(object arg_object_1)
		{
			return base.InvokeCall<bool>("isConnectionCallbacksRegistered", "(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Z", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004426 RID: 17446 RVA: 0x00145FB9 File Offset: 0x001443B9
		public bool isConnectionFailedListenerRegistered(object arg_object_1)
		{
			return base.InvokeCall<bool>("isConnectionFailedListenerRegistered", "(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Z", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004427 RID: 17447 RVA: 0x00145FD5 File Offset: 0x001443D5
		public void reconnect()
		{
			base.InvokeCallVoid("reconnect", "()V", new object[0]);
		}

		// Token: 0x06004428 RID: 17448 RVA: 0x00145FED File Offset: 0x001443ED
		public void registerConnectionCallbacks(object arg_object_1)
		{
			base.InvokeCallVoid("registerConnectionCallbacks", "(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004429 RID: 17449 RVA: 0x00146009 File Offset: 0x00144409
		public void registerConnectionFailedListener(object arg_object_1)
		{
			base.InvokeCallVoid("registerConnectionFailedListener", "(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x0600442A RID: 17450 RVA: 0x00146025 File Offset: 0x00144425
		public void stopAutoManage(object arg_object_1)
		{
			base.InvokeCallVoid("stopAutoManage", "(Landroid/support/v4/app/FragmentActivity;)V", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x0600442B RID: 17451 RVA: 0x00146041 File Offset: 0x00144441
		public void unregisterConnectionCallbacks(object arg_object_1)
		{
			base.InvokeCallVoid("unregisterConnectionCallbacks", "(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x0600442C RID: 17452 RVA: 0x0014605D File Offset: 0x0014445D
		public void unregisterConnectionFailedListener(object arg_object_1)
		{
			base.InvokeCallVoid("unregisterConnectionFailedListener", "(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x0600442D RID: 17453 RVA: 0x00146079 File Offset: 0x00144479
		public bool hasConnectedApi(object arg_object_1)
		{
			return base.InvokeCall<bool>("hasConnectedApi", "(Lcom/google/android/gms/common/api/Api;)Z", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x0600442E RID: 17454 RVA: 0x00146095 File Offset: 0x00144495
		public object getLooper()
		{
			return base.InvokeCall<object>("getLooper", "()Landroid/os/Looper;", new object[0]);
		}

		// Token: 0x0600442F RID: 17455 RVA: 0x001460AD File Offset: 0x001444AD
		public bool isConnected()
		{
			return base.InvokeCall<bool>("isConnected", "()Z", new object[0]);
		}

		// Token: 0x04004B7B RID: 19323
		private const string CLASS_NAME = "com/google/android/gms/common/api/GoogleApiClient";
	}
}
