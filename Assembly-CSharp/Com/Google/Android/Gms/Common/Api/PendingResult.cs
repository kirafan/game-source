﻿using System;
using Google.Developers;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D3E RID: 3390
	public class PendingResult<R> : JavaObjWrapper where R : Result
	{
		// Token: 0x06004430 RID: 17456 RVA: 0x001460C5 File Offset: 0x001444C5
		public PendingResult(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x06004431 RID: 17457 RVA: 0x001460CE File Offset: 0x001444CE
		public PendingResult() : base("com.google.android.gms.common.api.PendingResult")
		{
		}

		// Token: 0x06004432 RID: 17458 RVA: 0x001460DB File Offset: 0x001444DB
		public R await(long arg_long_1, object arg_object_2)
		{
			return base.InvokeCall<R>("await", "(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;", new object[]
			{
				arg_long_1,
				arg_object_2
			});
		}

		// Token: 0x06004433 RID: 17459 RVA: 0x00146100 File Offset: 0x00144500
		public R await()
		{
			return base.InvokeCall<R>("await", "()Lcom/google/android/gms/common/api/Result;", new object[0]);
		}

		// Token: 0x06004434 RID: 17460 RVA: 0x00146118 File Offset: 0x00144518
		public bool isCanceled()
		{
			return base.InvokeCall<bool>("isCanceled", "()Z", new object[0]);
		}

		// Token: 0x06004435 RID: 17461 RVA: 0x00146130 File Offset: 0x00144530
		public void cancel()
		{
			base.InvokeCallVoid("cancel", "()V", new object[0]);
		}

		// Token: 0x06004436 RID: 17462 RVA: 0x00146148 File Offset: 0x00144548
		public void setResultCallback(ResultCallback<R> arg_ResultCallback_1)
		{
			base.InvokeCallVoid("setResultCallback", "(Lcom/google/android/gms/common/api/ResultCallback;)V", new object[]
			{
				arg_ResultCallback_1
			});
		}

		// Token: 0x06004437 RID: 17463 RVA: 0x00146164 File Offset: 0x00144564
		public void setResultCallback(ResultCallback<R> arg_ResultCallback_1, long arg_long_2, object arg_object_3)
		{
			base.InvokeCallVoid("setResultCallback", "(Lcom/google/android/gms/common/api/ResultCallback;JLjava/util/concurrent/TimeUnit;)V", new object[]
			{
				arg_ResultCallback_1,
				arg_long_2,
				arg_object_3
			});
		}

		// Token: 0x04004B7C RID: 19324
		private const string CLASS_NAME = "com/google/android/gms/common/api/PendingResult";
	}
}
