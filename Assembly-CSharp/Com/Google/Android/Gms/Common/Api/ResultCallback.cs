﻿using System;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D40 RID: 3392
	public interface ResultCallback<R> where R : Result
	{
		// Token: 0x06004439 RID: 17465
		void onResult(R arg_Result_1);
	}
}
