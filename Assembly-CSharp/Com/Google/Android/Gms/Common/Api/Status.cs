﻿using System;
using Google.Developers;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D42 RID: 3394
	public class Status : JavaObjWrapper, Result
	{
		// Token: 0x0600443E RID: 17470 RVA: 0x0014618D File Offset: 0x0014458D
		public Status(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x0600443F RID: 17471 RVA: 0x00146196 File Offset: 0x00144596
		public Status(int arg_int_1, string arg_string_2, object arg_object_3)
		{
			base.CreateInstance("com/google/android/gms/common/api/Status", new object[]
			{
				arg_int_1,
				arg_string_2,
				arg_object_3
			});
		}

		// Token: 0x06004440 RID: 17472 RVA: 0x001461C0 File Offset: 0x001445C0
		public Status(int arg_int_1, string arg_string_2)
		{
			base.CreateInstance("com/google/android/gms/common/api/Status", new object[]
			{
				arg_int_1,
				arg_string_2
			});
		}

		// Token: 0x06004441 RID: 17473 RVA: 0x001461E6 File Offset: 0x001445E6
		public Status(int arg_int_1)
		{
			base.CreateInstance("com/google/android/gms/common/api/Status", new object[]
			{
				arg_int_1
			});
		}

		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x06004442 RID: 17474 RVA: 0x00146208 File Offset: 0x00144608
		public static object CREATOR
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/common/api/Status", "CREATOR", "Landroid/os/Parcelable$Creator;");
			}
		}

		// Token: 0x170004AE RID: 1198
		// (get) Token: 0x06004443 RID: 17475 RVA: 0x0014621E File Offset: 0x0014461E
		public static string NULL
		{
			get
			{
				return JavaObjWrapper.GetStaticStringField("com/google/android/gms/common/api/Status", "NULL");
			}
		}

		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x06004444 RID: 17476 RVA: 0x0014622F File Offset: 0x0014462F
		public static int CONTENTS_FILE_DESCRIPTOR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/api/Status", "CONTENTS_FILE_DESCRIPTOR");
			}
		}

		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x06004445 RID: 17477 RVA: 0x00146240 File Offset: 0x00144640
		public static int PARCELABLE_WRITE_RETURN_VALUE
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/api/Status", "PARCELABLE_WRITE_RETURN_VALUE");
			}
		}

		// Token: 0x06004446 RID: 17478 RVA: 0x00146251 File Offset: 0x00144651
		public bool equals(object arg_object_1)
		{
			return base.InvokeCall<bool>("equals", "(Ljava/lang/Object;)Z", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004447 RID: 17479 RVA: 0x0014626D File Offset: 0x0014466D
		public string toString()
		{
			return base.InvokeCall<string>("toString", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x06004448 RID: 17480 RVA: 0x00146285 File Offset: 0x00144685
		public int hashCode()
		{
			return base.InvokeCall<int>("hashCode", "()I", new object[0]);
		}

		// Token: 0x06004449 RID: 17481 RVA: 0x0014629D File Offset: 0x0014469D
		public bool isInterrupted()
		{
			return base.InvokeCall<bool>("isInterrupted", "()Z", new object[0]);
		}

		// Token: 0x0600444A RID: 17482 RVA: 0x001462B5 File Offset: 0x001446B5
		public Status getStatus()
		{
			return base.InvokeCall<Status>("getStatus", "()Lcom/google/android/gms/common/api/Status;", new object[0]);
		}

		// Token: 0x0600444B RID: 17483 RVA: 0x001462CD File Offset: 0x001446CD
		public bool isCanceled()
		{
			return base.InvokeCall<bool>("isCanceled", "()Z", new object[0]);
		}

		// Token: 0x0600444C RID: 17484 RVA: 0x001462E5 File Offset: 0x001446E5
		public int describeContents()
		{
			return base.InvokeCall<int>("describeContents", "()I", new object[0]);
		}

		// Token: 0x0600444D RID: 17485 RVA: 0x001462FD File Offset: 0x001446FD
		public object getResolution()
		{
			return base.InvokeCall<object>("getResolution", "()Landroid/app/PendingIntent;", new object[0]);
		}

		// Token: 0x0600444E RID: 17486 RVA: 0x00146315 File Offset: 0x00144715
		public int getStatusCode()
		{
			return base.InvokeCall<int>("getStatusCode", "()I", new object[0]);
		}

		// Token: 0x0600444F RID: 17487 RVA: 0x0014632D File Offset: 0x0014472D
		public string getStatusMessage()
		{
			return base.InvokeCall<string>("getStatusMessage", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x06004450 RID: 17488 RVA: 0x00146345 File Offset: 0x00144745
		public bool hasResolution()
		{
			return base.InvokeCall<bool>("hasResolution", "()Z", new object[0]);
		}

		// Token: 0x06004451 RID: 17489 RVA: 0x0014635D File Offset: 0x0014475D
		public void startResolutionForResult(object arg_object_1, int arg_int_2)
		{
			base.InvokeCallVoid("startResolutionForResult", "(Landroid/app/Activity;I)V", new object[]
			{
				arg_object_1,
				arg_int_2
			});
		}

		// Token: 0x06004452 RID: 17490 RVA: 0x00146382 File Offset: 0x00144782
		public void writeToParcel(object arg_object_1, int arg_int_2)
		{
			base.InvokeCallVoid("writeToParcel", "(Landroid/os/Parcel;I)V", new object[]
			{
				arg_object_1,
				arg_int_2
			});
		}

		// Token: 0x06004453 RID: 17491 RVA: 0x001463A7 File Offset: 0x001447A7
		public bool isSuccess()
		{
			return base.InvokeCall<bool>("isSuccess", "()Z", new object[0]);
		}

		// Token: 0x04004B7E RID: 19326
		private const string CLASS_NAME = "com/google/android/gms/common/api/Status";
	}
}
