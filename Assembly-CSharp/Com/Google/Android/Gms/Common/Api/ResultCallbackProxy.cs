﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Google.Developers;
using UnityEngine;

namespace Com.Google.Android.Gms.Common.Api
{
	// Token: 0x02000D41 RID: 3393
	public abstract class ResultCallbackProxy<R> : JavaInterfaceProxy, ResultCallback<R> where R : Result
	{
		// Token: 0x0600443A RID: 17466 RVA: 0x00144E21 File Offset: 0x00143221
		public ResultCallbackProxy() : base("com/google/android/gms/common/api/ResultCallback")
		{
		}

		// Token: 0x0600443B RID: 17467
		public abstract void OnResult(R arg_Result_1);

		// Token: 0x0600443C RID: 17468 RVA: 0x00144E2E File Offset: 0x0014322E
		public void onResult(R arg_Result_1)
		{
			this.OnResult(arg_Result_1);
		}

		// Token: 0x0600443D RID: 17469 RVA: 0x00144E38 File Offset: 0x00143238
		public void onResult(AndroidJavaObject arg_Result_1)
		{
			IntPtr rawObject = arg_Result_1.GetRawObject();
			ConstructorInfo constructor = typeof(R).GetConstructor(new Type[]
			{
				rawObject.GetType()
			});
			R r;
			if (constructor != null)
			{
				r = (R)((object)constructor.Invoke(new object[]
				{
					rawObject
				}));
			}
			else
			{
				ConstructorInfo constructor2 = typeof(R).GetConstructor(new Type[0]);
				r = (R)((object)constructor2.Invoke(new object[0]));
				Marshal.PtrToStructure(rawObject, r);
			}
			this.OnResult(r);
		}

		// Token: 0x04004B7D RID: 19325
		private const string CLASS_NAME = "com/google/android/gms/common/api/ResultCallback";
	}
}
