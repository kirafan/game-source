﻿using System;
using Google.Developers;

namespace Com.Google.Android.Gms.Common
{
	// Token: 0x02000D43 RID: 3395
	public class ConnectionResult : JavaObjWrapper
	{
		// Token: 0x06004454 RID: 17492 RVA: 0x001463BF File Offset: 0x001447BF
		public ConnectionResult(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x06004455 RID: 17493 RVA: 0x001463C8 File Offset: 0x001447C8
		public ConnectionResult(int arg_int_1, object arg_object_2, string arg_string_3)
		{
			base.CreateInstance("com/google/android/gms/common/ConnectionResult", new object[]
			{
				arg_int_1,
				arg_object_2,
				arg_string_3
			});
		}

		// Token: 0x06004456 RID: 17494 RVA: 0x001463F2 File Offset: 0x001447F2
		public ConnectionResult(int arg_int_1, object arg_object_2)
		{
			base.CreateInstance("com/google/android/gms/common/ConnectionResult", new object[]
			{
				arg_int_1,
				arg_object_2
			});
		}

		// Token: 0x06004457 RID: 17495 RVA: 0x00146418 File Offset: 0x00144818
		public ConnectionResult(int arg_int_1)
		{
			base.CreateInstance("com/google/android/gms/common/ConnectionResult", new object[]
			{
				arg_int_1
			});
		}

		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x06004458 RID: 17496 RVA: 0x0014643A File Offset: 0x0014483A
		public static int SUCCESS
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SUCCESS");
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x06004459 RID: 17497 RVA: 0x0014644B File Offset: 0x0014484B
		public static int SERVICE_MISSING
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_MISSING");
			}
		}

		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x0600445A RID: 17498 RVA: 0x0014645C File Offset: 0x0014485C
		public static int SERVICE_VERSION_UPDATE_REQUIRED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_VERSION_UPDATE_REQUIRED");
			}
		}

		// Token: 0x170004B4 RID: 1204
		// (get) Token: 0x0600445B RID: 17499 RVA: 0x0014646D File Offset: 0x0014486D
		public static int SERVICE_DISABLED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_DISABLED");
			}
		}

		// Token: 0x170004B5 RID: 1205
		// (get) Token: 0x0600445C RID: 17500 RVA: 0x0014647E File Offset: 0x0014487E
		public static int SIGN_IN_REQUIRED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SIGN_IN_REQUIRED");
			}
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x0600445D RID: 17501 RVA: 0x0014648F File Offset: 0x0014488F
		public static int INVALID_ACCOUNT
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "INVALID_ACCOUNT");
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x0600445E RID: 17502 RVA: 0x001464A0 File Offset: 0x001448A0
		public static int RESOLUTION_REQUIRED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "RESOLUTION_REQUIRED");
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x0600445F RID: 17503 RVA: 0x001464B1 File Offset: 0x001448B1
		public static int NETWORK_ERROR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "NETWORK_ERROR");
			}
		}

		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06004460 RID: 17504 RVA: 0x001464C2 File Offset: 0x001448C2
		public static int INTERNAL_ERROR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "INTERNAL_ERROR");
			}
		}

		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06004461 RID: 17505 RVA: 0x001464D3 File Offset: 0x001448D3
		public static int SERVICE_INVALID
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_INVALID");
			}
		}

		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x06004462 RID: 17506 RVA: 0x001464E4 File Offset: 0x001448E4
		public static int DEVELOPER_ERROR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "DEVELOPER_ERROR");
			}
		}

		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x06004463 RID: 17507 RVA: 0x001464F5 File Offset: 0x001448F5
		public static int LICENSE_CHECK_FAILED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "LICENSE_CHECK_FAILED");
			}
		}

		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x06004464 RID: 17508 RVA: 0x00146506 File Offset: 0x00144906
		public static int CANCELED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "CANCELED");
			}
		}

		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06004465 RID: 17509 RVA: 0x00146517 File Offset: 0x00144917
		public static int TIMEOUT
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "TIMEOUT");
			}
		}

		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06004466 RID: 17510 RVA: 0x00146528 File Offset: 0x00144928
		public static int INTERRUPTED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "INTERRUPTED");
			}
		}

		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06004467 RID: 17511 RVA: 0x00146539 File Offset: 0x00144939
		public static int API_UNAVAILABLE
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "API_UNAVAILABLE");
			}
		}

		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06004468 RID: 17512 RVA: 0x0014654A File Offset: 0x0014494A
		public static int SIGN_IN_FAILED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SIGN_IN_FAILED");
			}
		}

		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x06004469 RID: 17513 RVA: 0x0014655B File Offset: 0x0014495B
		public static int SERVICE_UPDATING
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_UPDATING");
			}
		}

		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x0600446A RID: 17514 RVA: 0x0014656C File Offset: 0x0014496C
		public static int SERVICE_MISSING_PERMISSION
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "SERVICE_MISSING_PERMISSION");
			}
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x0600446B RID: 17515 RVA: 0x0014657D File Offset: 0x0014497D
		public static int DRIVE_EXTERNAL_STORAGE_REQUIRED
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "DRIVE_EXTERNAL_STORAGE_REQUIRED");
			}
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x0600446C RID: 17516 RVA: 0x0014658E File Offset: 0x0014498E
		public static object CREATOR
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/common/ConnectionResult", "CREATOR", "Landroid/os/Parcelable$Creator;");
			}
		}

		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x0600446D RID: 17517 RVA: 0x001465A4 File Offset: 0x001449A4
		public static string NULL
		{
			get
			{
				return JavaObjWrapper.GetStaticStringField("com/google/android/gms/common/ConnectionResult", "NULL");
			}
		}

		// Token: 0x170004C7 RID: 1223
		// (get) Token: 0x0600446E RID: 17518 RVA: 0x001465B5 File Offset: 0x001449B5
		public static int CONTENTS_FILE_DESCRIPTOR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "CONTENTS_FILE_DESCRIPTOR");
			}
		}

		// Token: 0x170004C8 RID: 1224
		// (get) Token: 0x0600446F RID: 17519 RVA: 0x001465C6 File Offset: 0x001449C6
		public static int PARCELABLE_WRITE_RETURN_VALUE
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/common/ConnectionResult", "PARCELABLE_WRITE_RETURN_VALUE");
			}
		}

		// Token: 0x06004470 RID: 17520 RVA: 0x001465D7 File Offset: 0x001449D7
		public bool equals(object arg_object_1)
		{
			return base.InvokeCall<bool>("equals", "(Ljava/lang/Object;)Z", new object[]
			{
				arg_object_1
			});
		}

		// Token: 0x06004471 RID: 17521 RVA: 0x001465F3 File Offset: 0x001449F3
		public string toString()
		{
			return base.InvokeCall<string>("toString", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x06004472 RID: 17522 RVA: 0x0014660B File Offset: 0x00144A0B
		public int hashCode()
		{
			return base.InvokeCall<int>("hashCode", "()I", new object[0]);
		}

		// Token: 0x06004473 RID: 17523 RVA: 0x00146623 File Offset: 0x00144A23
		public int describeContents()
		{
			return base.InvokeCall<int>("describeContents", "()I", new object[0]);
		}

		// Token: 0x06004474 RID: 17524 RVA: 0x0014663B File Offset: 0x00144A3B
		public object getResolution()
		{
			return base.InvokeCall<object>("getResolution", "()Landroid/app/PendingIntent;", new object[0]);
		}

		// Token: 0x06004475 RID: 17525 RVA: 0x00146653 File Offset: 0x00144A53
		public bool hasResolution()
		{
			return base.InvokeCall<bool>("hasResolution", "()Z", new object[0]);
		}

		// Token: 0x06004476 RID: 17526 RVA: 0x0014666B File Offset: 0x00144A6B
		public void startResolutionForResult(object arg_object_1, int arg_int_2)
		{
			base.InvokeCallVoid("startResolutionForResult", "(Landroid/app/Activity;I)V", new object[]
			{
				arg_object_1,
				arg_int_2
			});
		}

		// Token: 0x06004477 RID: 17527 RVA: 0x00146690 File Offset: 0x00144A90
		public void writeToParcel(object arg_object_1, int arg_int_2)
		{
			base.InvokeCallVoid("writeToParcel", "(Landroid/os/Parcel;I)V", new object[]
			{
				arg_object_1,
				arg_int_2
			});
		}

		// Token: 0x06004478 RID: 17528 RVA: 0x001466B5 File Offset: 0x00144AB5
		public int getErrorCode()
		{
			return base.InvokeCall<int>("getErrorCode", "()I", new object[0]);
		}

		// Token: 0x06004479 RID: 17529 RVA: 0x001466CD File Offset: 0x00144ACD
		public string getErrorMessage()
		{
			return base.InvokeCall<string>("getErrorMessage", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x0600447A RID: 17530 RVA: 0x001466E5 File Offset: 0x00144AE5
		public bool isSuccess()
		{
			return base.InvokeCall<bool>("isSuccess", "()Z", new object[0]);
		}

		// Token: 0x04004B7F RID: 19327
		private const string CLASS_NAME = "com/google/android/gms/common/ConnectionResult";
	}
}
