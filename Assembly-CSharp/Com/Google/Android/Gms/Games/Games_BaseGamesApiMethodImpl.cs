﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Google.Developers;

namespace Com.Google.Android.Gms.Games
{
	// Token: 0x02000D45 RID: 3397
	public class Games_BaseGamesApiMethodImpl<R> : JavaObjWrapper where R : Result
	{
		// Token: 0x06004494 RID: 17556 RVA: 0x0014695F File Offset: 0x00144D5F
		public Games_BaseGamesApiMethodImpl(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x06004495 RID: 17557 RVA: 0x00146968 File Offset: 0x00144D68
		public Games_BaseGamesApiMethodImpl(GoogleApiClient arg_GoogleApiClient_1)
		{
			base.CreateInstance("com/google/android/gms/games/Games$BaseGamesApiMethodImpl", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x04004B81 RID: 19329
		private const string CLASS_NAME = "com/google/android/gms/games/Games$BaseGamesApiMethodImpl";
	}
}
