﻿using System;
using Com.Google.Android.Gms.Common.Api;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D4A RID: 3402
	public interface Stats_LoadPlayerStatsResult : Result
	{
		// Token: 0x060044B2 RID: 17586
		PlayerStats getPlayerStats();
	}
}
