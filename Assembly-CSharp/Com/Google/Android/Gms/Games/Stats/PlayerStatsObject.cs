﻿using System;
using Google.Developers;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D48 RID: 3400
	public class PlayerStatsObject : JavaObjWrapper, PlayerStats
	{
		// Token: 0x060044A3 RID: 17571 RVA: 0x001469E6 File Offset: 0x00144DE6
		public PlayerStatsObject(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x060044A4 RID: 17572 RVA: 0x001469EF File Offset: 0x00144DEF
		public static float UNSET_VALUE
		{
			get
			{
				return JavaObjWrapper.GetStaticFloatField("com/google/android/gms/games/stats/PlayerStats", "UNSET_VALUE");
			}
		}

		// Token: 0x170004DB RID: 1243
		// (get) Token: 0x060044A5 RID: 17573 RVA: 0x00146A00 File Offset: 0x00144E00
		public static int CONTENTS_FILE_DESCRIPTOR
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/games/stats/PlayerStats", "CONTENTS_FILE_DESCRIPTOR");
			}
		}

		// Token: 0x170004DC RID: 1244
		// (get) Token: 0x060044A6 RID: 17574 RVA: 0x00146A11 File Offset: 0x00144E11
		public static int PARCELABLE_WRITE_RETURN_VALUE
		{
			get
			{
				return JavaObjWrapper.GetStaticIntField("com/google/android/gms/games/stats/PlayerStats", "PARCELABLE_WRITE_RETURN_VALUE");
			}
		}

		// Token: 0x060044A7 RID: 17575 RVA: 0x00146A22 File Offset: 0x00144E22
		public float getAverageSessionLength()
		{
			return base.InvokeCall<float>("getAverageSessionLength", "()F", new object[0]);
		}

		// Token: 0x060044A8 RID: 17576 RVA: 0x00146A3A File Offset: 0x00144E3A
		public float getChurnProbability()
		{
			return base.InvokeCall<float>("getChurnProbability", "()F", new object[0]);
		}

		// Token: 0x060044A9 RID: 17577 RVA: 0x00146A52 File Offset: 0x00144E52
		public int getDaysSinceLastPlayed()
		{
			return base.InvokeCall<int>("getDaysSinceLastPlayed", "()I", new object[0]);
		}

		// Token: 0x060044AA RID: 17578 RVA: 0x00146A6A File Offset: 0x00144E6A
		public int getNumberOfPurchases()
		{
			return base.InvokeCall<int>("getNumberOfPurchases", "()I", new object[0]);
		}

		// Token: 0x060044AB RID: 17579 RVA: 0x00146A82 File Offset: 0x00144E82
		public int getNumberOfSessions()
		{
			return base.InvokeCall<int>("getNumberOfSessions", "()I", new object[0]);
		}

		// Token: 0x060044AC RID: 17580 RVA: 0x00146A9A File Offset: 0x00144E9A
		public float getSessionPercentile()
		{
			return base.InvokeCall<float>("getSessionPercentile", "()F", new object[0]);
		}

		// Token: 0x060044AD RID: 17581 RVA: 0x00146AB2 File Offset: 0x00144EB2
		public float getSpendPercentile()
		{
			return base.InvokeCall<float>("getSpendPercentile", "()F", new object[0]);
		}

		// Token: 0x060044AE RID: 17582 RVA: 0x00146ACA File Offset: 0x00144ECA
		public float getSpendProbability()
		{
			return base.InvokeCall<float>("getSpendProbability", "()F", new object[0]);
		}

		// Token: 0x060044AF RID: 17583 RVA: 0x00146AE2 File Offset: 0x00144EE2
		public float getHighSpenderProbability()
		{
			return base.InvokeCall<float>("getHighSpenderProbability", "()F", new object[0]);
		}

		// Token: 0x060044B0 RID: 17584 RVA: 0x00146AFA File Offset: 0x00144EFA
		public float getTotalSpendNext28Days()
		{
			return base.InvokeCall<float>("getTotalSpendNext28Days", "()F", new object[0]);
		}

		// Token: 0x04004B83 RID: 19331
		private const string CLASS_NAME = "com/google/android/gms/games/stats/PlayerStats";
	}
}
