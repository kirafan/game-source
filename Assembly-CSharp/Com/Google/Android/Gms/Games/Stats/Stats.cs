﻿using System;
using Com.Google.Android.Gms.Common.Api;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D49 RID: 3401
	public interface Stats
	{
		// Token: 0x060044B1 RID: 17585
		PendingResult<Stats_LoadPlayerStatsResultObject> loadPlayerStats(GoogleApiClient arg_GoogleApiClient_1, bool arg_bool_2);
	}
}
