﻿using System;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D47 RID: 3399
	public interface PlayerStats
	{
		// Token: 0x06004499 RID: 17561
		float getAverageSessionLength();

		// Token: 0x0600449A RID: 17562
		float getChurnProbability();

		// Token: 0x0600449B RID: 17563
		int getDaysSinceLastPlayed();

		// Token: 0x0600449C RID: 17564
		int getNumberOfPurchases();

		// Token: 0x0600449D RID: 17565
		int getNumberOfSessions();

		// Token: 0x0600449E RID: 17566
		float getSessionPercentile();

		// Token: 0x0600449F RID: 17567
		float getSpendPercentile();

		// Token: 0x060044A0 RID: 17568
		float getSpendProbability();

		// Token: 0x060044A1 RID: 17569
		float getHighSpenderProbability();

		// Token: 0x060044A2 RID: 17570
		float getTotalSpendNext28Days();
	}
}
