﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Google.Developers;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D4B RID: 3403
	public class StatsObject : JavaObjWrapper, Stats
	{
		// Token: 0x060044B3 RID: 17587 RVA: 0x00146B12 File Offset: 0x00144F12
		public StatsObject(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x060044B4 RID: 17588 RVA: 0x00146B1C File Offset: 0x00144F1C
		public PendingResult<Stats_LoadPlayerStatsResultObject> loadPlayerStats(GoogleApiClient arg_GoogleApiClient_1, bool arg_bool_2)
		{
			IntPtr ptr = base.InvokeCall<IntPtr>("loadPlayerStats", "(Lcom/google/android/gms/common/api/GoogleApiClient;Z)Lcom/google/android/gms/common/api/PendingResult;", new object[]
			{
				arg_GoogleApiClient_1,
				arg_bool_2
			});
			return new PendingResult<Stats_LoadPlayerStatsResultObject>(ptr);
		}

		// Token: 0x04004B84 RID: 19332
		private const string CLASS_NAME = "com/google/android/gms/games/stats/Stats";
	}
}
