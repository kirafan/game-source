﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Google.Developers;

namespace Com.Google.Android.Gms.Games.Stats
{
	// Token: 0x02000D46 RID: 3398
	public class Stats_LoadPlayerStatsResultObject : JavaObjWrapper, Stats_LoadPlayerStatsResult, Result
	{
		// Token: 0x06004496 RID: 17558 RVA: 0x00146985 File Offset: 0x00144D85
		public Stats_LoadPlayerStatsResultObject(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x06004497 RID: 17559 RVA: 0x00146990 File Offset: 0x00144D90
		public PlayerStats getPlayerStats()
		{
			IntPtr ptr = base.InvokeCall<IntPtr>("getPlayerStats", "()Lcom/google/android/gms/games/stats/PlayerStats;", new object[0]);
			return new PlayerStatsObject(ptr);
		}

		// Token: 0x06004498 RID: 17560 RVA: 0x001469BC File Offset: 0x00144DBC
		public Status getStatus()
		{
			IntPtr ptr = base.InvokeCall<IntPtr>("getStatus", "()Lcom/google/android/gms/common/api/Status;", new object[0]);
			return new Status(ptr);
		}

		// Token: 0x04004B82 RID: 19330
		private const string CLASS_NAME = "com/google/android/gms/games/stats/Stats$LoadPlayerStatsResult";
	}
}
