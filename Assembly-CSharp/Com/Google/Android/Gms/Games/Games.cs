﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Com.Google.Android.Gms.Games.Stats;
using Google.Developers;

namespace Com.Google.Android.Gms.Games
{
	// Token: 0x02000D44 RID: 3396
	public class Games : JavaObjWrapper
	{
		// Token: 0x0600447B RID: 17531 RVA: 0x001466FD File Offset: 0x00144AFD
		public Games(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x0600447C RID: 17532 RVA: 0x00146706 File Offset: 0x00144B06
		public static string EXTRA_PLAYER_IDS
		{
			get
			{
				return JavaObjWrapper.GetStaticStringField("com/google/android/gms/games/Games", "EXTRA_PLAYER_IDS");
			}
		}

		// Token: 0x170004CA RID: 1226
		// (get) Token: 0x0600447D RID: 17533 RVA: 0x00146717 File Offset: 0x00144B17
		public static string EXTRA_STATUS
		{
			get
			{
				return JavaObjWrapper.GetStaticStringField("com/google/android/gms/games/Games", "EXTRA_STATUS");
			}
		}

		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x0600447E RID: 17534 RVA: 0x00146728 File Offset: 0x00144B28
		public static object SCOPE_GAMES
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "SCOPE_GAMES", "Lcom/google/android/gms/common/api/Scope;");
			}
		}

		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x0600447F RID: 17535 RVA: 0x0014673E File Offset: 0x00144B3E
		public static object API
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "API", "Lcom/google/android/gms/common/api/Api;");
			}
		}

		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x06004480 RID: 17536 RVA: 0x00146754 File Offset: 0x00144B54
		public static object GamesMetadata
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "GamesMetadata", "Lcom/google/android/gms/games/GamesMetadata;");
			}
		}

		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x06004481 RID: 17537 RVA: 0x0014676A File Offset: 0x00144B6A
		public static object Achievements
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Achievements", "Lcom/google/android/gms/games/achievement/Achievements;");
			}
		}

		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x06004482 RID: 17538 RVA: 0x00146780 File Offset: 0x00144B80
		public static object Events
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Events", "Lcom/google/android/gms/games/event/Events;");
			}
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x06004483 RID: 17539 RVA: 0x00146796 File Offset: 0x00144B96
		public static object Leaderboards
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Leaderboards", "Lcom/google/android/gms/games/leaderboard/Leaderboards;");
			}
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x06004484 RID: 17540 RVA: 0x001467AC File Offset: 0x00144BAC
		public static object Invitations
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Invitations", "Lcom/google/android/gms/games/multiplayer/Invitations;");
			}
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x06004485 RID: 17541 RVA: 0x001467C2 File Offset: 0x00144BC2
		public static object TurnBasedMultiplayer
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "TurnBasedMultiplayer", "Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer;");
			}
		}

		// Token: 0x170004D3 RID: 1235
		// (get) Token: 0x06004486 RID: 17542 RVA: 0x001467D8 File Offset: 0x00144BD8
		public static object RealTimeMultiplayer
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "RealTimeMultiplayer", "Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer;");
			}
		}

		// Token: 0x170004D4 RID: 1236
		// (get) Token: 0x06004487 RID: 17543 RVA: 0x001467EE File Offset: 0x00144BEE
		public static object Players
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Players", "Lcom/google/android/gms/games/Players;");
			}
		}

		// Token: 0x170004D5 RID: 1237
		// (get) Token: 0x06004488 RID: 17544 RVA: 0x00146804 File Offset: 0x00144C04
		public static object Notifications
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Notifications", "Lcom/google/android/gms/games/Notifications;");
			}
		}

		// Token: 0x170004D6 RID: 1238
		// (get) Token: 0x06004489 RID: 17545 RVA: 0x0014681A File Offset: 0x00144C1A
		public static object Quests
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Quests", "Lcom/google/android/gms/games/quest/Quests;");
			}
		}

		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x0600448A RID: 17546 RVA: 0x00146830 File Offset: 0x00144C30
		public static object Requests
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Requests", "Lcom/google/android/gms/games/request/Requests;");
			}
		}

		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x0600448B RID: 17547 RVA: 0x00146846 File Offset: 0x00144C46
		public static object Snapshots
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<object>("com/google/android/gms/games/Games", "Snapshots", "Lcom/google/android/gms/games/snapshot/Snapshots;");
			}
		}

		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x0600448C RID: 17548 RVA: 0x0014685C File Offset: 0x00144C5C
		public static StatsObject Stats
		{
			get
			{
				return JavaObjWrapper.GetStaticObjectField<StatsObject>("com/google/android/gms/games/Games", "Stats", "Lcom/google/android/gms/games/stats/Stats;");
			}
		}

		// Token: 0x0600448D RID: 17549 RVA: 0x00146872 File Offset: 0x00144C72
		public static string getAppId(GoogleApiClient arg_GoogleApiClient_1)
		{
			return JavaObjWrapper.StaticInvokeCall<string>("com/google/android/gms/games/Games", "getAppId", "(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/lang/String;", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x0600448E RID: 17550 RVA: 0x00146892 File Offset: 0x00144C92
		public static string getCurrentAccountName(GoogleApiClient arg_GoogleApiClient_1)
		{
			return JavaObjWrapper.StaticInvokeCall<string>("com/google/android/gms/games/Games", "getCurrentAccountName", "(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/lang/String;", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x0600448F RID: 17551 RVA: 0x001468B2 File Offset: 0x00144CB2
		public static int getSdkVariant(GoogleApiClient arg_GoogleApiClient_1)
		{
			return JavaObjWrapper.StaticInvokeCall<int>("com/google/android/gms/games/Games", "getSdkVariant", "(Lcom/google/android/gms/common/api/GoogleApiClient;)I", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x06004490 RID: 17552 RVA: 0x001468D2 File Offset: 0x00144CD2
		public static object getSettingsIntent(GoogleApiClient arg_GoogleApiClient_1)
		{
			return JavaObjWrapper.StaticInvokeCall<object>("com/google/android/gms/games/Games", "getSettingsIntent", "(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/content/Intent;", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x06004491 RID: 17553 RVA: 0x001468F2 File Offset: 0x00144CF2
		public static void setGravityForPopups(GoogleApiClient arg_GoogleApiClient_1, int arg_int_2)
		{
			JavaObjWrapper.StaticInvokeCallVoid("com/google/android/gms/games/Games", "setGravityForPopups", "(Lcom/google/android/gms/common/api/GoogleApiClient;I)V", new object[]
			{
				arg_GoogleApiClient_1,
				arg_int_2
			});
		}

		// Token: 0x06004492 RID: 17554 RVA: 0x0014691B File Offset: 0x00144D1B
		public static void setViewForPopups(GoogleApiClient arg_GoogleApiClient_1, object arg_object_2)
		{
			JavaObjWrapper.StaticInvokeCallVoid("com/google/android/gms/games/Games", "setViewForPopups", "(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/view/View;)V", new object[]
			{
				arg_GoogleApiClient_1,
				arg_object_2
			});
		}

		// Token: 0x06004493 RID: 17555 RVA: 0x0014693F File Offset: 0x00144D3F
		public static PendingResult<Status> signOut(GoogleApiClient arg_GoogleApiClient_1)
		{
			return JavaObjWrapper.StaticInvokeCall<PendingResult<Status>>("com/google/android/gms/games/Games", "signOut", "(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;", new object[]
			{
				arg_GoogleApiClient_1
			});
		}

		// Token: 0x04004B80 RID: 19328
		private const string CLASS_NAME = "com/google/android/gms/games/Games";
	}
}
