﻿using System;
using Star;

// Token: 0x02000CDA RID: 3290
public class AssetServerSettings
{
	// Token: 0x170003DA RID: 986
	// (get) Token: 0x0600413B RID: 16699 RVA: 0x00140AE2 File Offset: 0x0013EEE2
	public static string assetServerURL
	{
		get
		{
			if (Star.Version.IsApplying)
			{
				return AssetServerSettings.m_assetServerReleaseURL;
			}
			return "https://asset-krr-prd.star-api.com/";
		}
	}

	// Token: 0x170003DB RID: 987
	// (get) Token: 0x0600413C RID: 16700 RVA: 0x00140AF9 File Offset: 0x0013EEF9
	public static bool awakeCleanCache
	{
		get
		{
			return false;
		}
	}

	// Token: 0x04004986 RID: 18822
	public static string m_assetServerReleaseURL = string.Empty;

	// Token: 0x04004987 RID: 18823
	private const string m_assetServerURL = "https://asset-krr-prd.star-api.com/";

	// Token: 0x04004988 RID: 18824
	private const bool m_awakeCleanCache = false;
}
