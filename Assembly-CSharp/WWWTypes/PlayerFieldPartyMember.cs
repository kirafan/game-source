﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB5 RID: 3253
	public class PlayerFieldPartyMember
	{
		// Token: 0x04004843 RID: 18499
		public long managedPartyMemberId;

		// Token: 0x04004844 RID: 18500
		public long managedCharacterId;

		// Token: 0x04004845 RID: 18501
		public int characterId;

		// Token: 0x04004846 RID: 18502
		public int scheduleId;

		// Token: 0x04004847 RID: 18503
		public int scheduleTag;

		// Token: 0x04004848 RID: 18504
		public long managedFacilityId;

		// Token: 0x04004849 RID: 18505
		public int roomId;

		// Token: 0x0400484A RID: 18506
		public int liveIdx;

		// Token: 0x0400484B RID: 18507
		public int touchItemResultNo;

		// Token: 0x0400484C RID: 18508
		public int flag;

		// Token: 0x0400484D RID: 18509
		public string scheduleTable;
	}
}
