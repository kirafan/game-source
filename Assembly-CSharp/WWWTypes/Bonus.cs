﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C98 RID: 3224
	public class Bonus
	{
		// Token: 0x0400470B RID: 18187
		public int bonusId;

		// Token: 0x0400470C RID: 18188
		public int type;

		// Token: 0x0400470D RID: 18189
		public int dayIndex;

		// Token: 0x0400470E RID: 18190
		public BonusDay[] bonusDays;
	}
}
