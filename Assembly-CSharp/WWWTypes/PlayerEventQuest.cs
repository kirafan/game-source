﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB4 RID: 3252
	public class PlayerEventQuest
	{
		// Token: 0x0400483F RID: 18495
		public EventQuest eventQuest;

		// Token: 0x04004840 RID: 18496
		public int clearRank;

		// Token: 0x04004841 RID: 18497
		public int orderTotal;

		// Token: 0x04004842 RID: 18498
		public int orderDaily;
	}
}
