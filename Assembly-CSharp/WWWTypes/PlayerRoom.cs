﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC0 RID: 3264
	public class PlayerRoom
	{
		// Token: 0x0400489C RID: 18588
		public long managedRoomId;

		// Token: 0x0400489D RID: 18589
		public int floorId;

		// Token: 0x0400489E RID: 18590
		public int groupId;

		// Token: 0x0400489F RID: 18591
		public PlayerRoomArrangement[] arrangeData;

		// Token: 0x040048A0 RID: 18592
		public int active;
	}
}
