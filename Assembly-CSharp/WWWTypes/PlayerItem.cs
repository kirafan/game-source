﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB7 RID: 3255
	public class PlayerItem
	{
		// Token: 0x04004853 RID: 18515
		public long managedItemId;

		// Token: 0x04004854 RID: 18516
		public int state;

		// Token: 0x04004855 RID: 18517
		public DateTime usedAt;

		// Token: 0x04004856 RID: 18518
		public DateTime soldAt;

		// Token: 0x04004857 RID: 18519
		public int itemId;
	}
}
