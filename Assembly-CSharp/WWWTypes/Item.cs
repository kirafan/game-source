﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAB RID: 3243
	public class Item
	{
		// Token: 0x040047DC RID: 18396
		public int id;

		// Token: 0x040047DD RID: 18397
		public string name;

		// Token: 0x040047DE RID: 18398
		public int rarity;

		// Token: 0x040047DF RID: 18399
		public int saleAmount;

		// Token: 0x040047E0 RID: 18400
		public int type;

		// Token: 0x040047E1 RID: 18401
		public int typeArg0;

		// Token: 0x040047E2 RID: 18402
		public int typeArg1;

		// Token: 0x040047E3 RID: 18403
		public int typeArg2;

		// Token: 0x040047E4 RID: 18404
		public int typeArg3;
	}
}
