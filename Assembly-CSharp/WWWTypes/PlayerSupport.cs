﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC3 RID: 3267
	public class PlayerSupport
	{
		// Token: 0x040048A9 RID: 18601
		public long managedSupportId;

		// Token: 0x040048AA RID: 18602
		public string name;

		// Token: 0x040048AB RID: 18603
		public int active;

		// Token: 0x040048AC RID: 18604
		public long[] managedCharacterIds;

		// Token: 0x040048AD RID: 18605
		public long[] managedWeaponIds;
	}
}
