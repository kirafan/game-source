﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC1 RID: 3265
	public class PlayerRoomArrangement
	{
		// Token: 0x040048A1 RID: 18593
		public long managedRoomObjectId;

		// Token: 0x040048A2 RID: 18594
		public int roomObjectId;

		// Token: 0x040048A3 RID: 18595
		public int roomNo;

		// Token: 0x040048A4 RID: 18596
		public int x;

		// Token: 0x040048A5 RID: 18597
		public int y;

		// Token: 0x040048A6 RID: 18598
		public int dir;
	}
}
