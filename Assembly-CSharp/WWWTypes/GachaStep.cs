﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA6 RID: 3238
	public class GachaStep
	{
		// Token: 0x040047BD RID: 18365
		public int id;

		// Token: 0x040047BE RID: 18366
		public string name;

		// Token: 0x040047BF RID: 18367
		public string bannerId;

		// Token: 0x040047C0 RID: 18368
		public string webViewUrl;

		// Token: 0x040047C1 RID: 18369
		public DateTime startAt;

		// Token: 0x040047C2 RID: 18370
		public DateTime endAt;

		// Token: 0x040047C3 RID: 18371
		public int reset;

		// Token: 0x040047C4 RID: 18372
		public int steps;

		// Token: 0x040047C5 RID: 18373
		public GachaOneStep[] gachaSteps;
	}
}
