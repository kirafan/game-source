﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAD RID: 3245
	public class MasterOrb
	{
		// Token: 0x040047F9 RID: 18425
		public int id;

		// Token: 0x040047FA RID: 18426
		public string name;

		// Token: 0x040047FB RID: 18427
		public int classType;

		// Token: 0x040047FC RID: 18428
		public int expTableId;

		// Token: 0x040047FD RID: 18429
		public int levelLimit;
	}
}
