﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAC RID: 3244
	public class ItemTradeRecipe
	{
		// Token: 0x040047E5 RID: 18405
		public int id;

		// Token: 0x040047E6 RID: 18406
		public int limitedFlag;

		// Token: 0x040047E7 RID: 18407
		public int dstType;

		// Token: 0x040047E8 RID: 18408
		public int dstId;

		// Token: 0x040047E9 RID: 18409
		public int dstAmount;

		// Token: 0x040047EA RID: 18410
		public int altType;

		// Token: 0x040047EB RID: 18411
		public int altId;

		// Token: 0x040047EC RID: 18412
		public int altAmount;

		// Token: 0x040047ED RID: 18413
		public int srcType1;

		// Token: 0x040047EE RID: 18414
		public int srcId1;

		// Token: 0x040047EF RID: 18415
		public int srcAmount1;

		// Token: 0x040047F0 RID: 18416
		public int srcType2;

		// Token: 0x040047F1 RID: 18417
		public int srcId2;

		// Token: 0x040047F2 RID: 18418
		public int srcAmount2;

		// Token: 0x040047F3 RID: 18419
		public DateTime startAt;

		// Token: 0x040047F4 RID: 18420
		public DateTime endAt;

		// Token: 0x040047F5 RID: 18421
		public int limitCount;

		// Token: 0x040047F6 RID: 18422
		public int resetFlag;

		// Token: 0x040047F7 RID: 18423
		public int totalTradeCount;

		// Token: 0x040047F8 RID: 18424
		public int monthlyTradeCount;
	}
}
