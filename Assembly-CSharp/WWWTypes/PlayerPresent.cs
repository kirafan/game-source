﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CBC RID: 3260
	public class PlayerPresent
	{
		// Token: 0x0400486C RID: 18540
		public long managedPresentId;

		// Token: 0x0400486D RID: 18541
		public int type;

		// Token: 0x0400486E RID: 18542
		public int objectId;

		// Token: 0x0400486F RID: 18543
		public int amount;

		// Token: 0x04004870 RID: 18544
		public string options;

		// Token: 0x04004871 RID: 18545
		public string title;

		// Token: 0x04004872 RID: 18546
		public string message;

		// Token: 0x04004873 RID: 18547
		public bool deadlineFlag;

		// Token: 0x04004874 RID: 18548
		public DateTime createdAt;

		// Token: 0x04004875 RID: 18549
		public DateTime deadlineAt;

		// Token: 0x04004876 RID: 18550
		public DateTime receivedAt;
	}
}
