﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CD2 RID: 3282
	public class WeaponExp
	{
		// Token: 0x04004966 RID: 18790
		public int tableId;

		// Token: 0x04004967 RID: 18791
		public int level;

		// Token: 0x04004968 RID: 18792
		public int exp;
	}
}
