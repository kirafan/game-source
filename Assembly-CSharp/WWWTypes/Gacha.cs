﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA4 RID: 3236
	public class Gacha
	{
		// Token: 0x0400479A RID: 18330
		public int id;

		// Token: 0x0400479B RID: 18331
		public string name;

		// Token: 0x0400479C RID: 18332
		public string bannerId;

		// Token: 0x0400479D RID: 18333
		public int type;

		// Token: 0x0400479E RID: 18334
		public int unlimitedGem;

		// Token: 0x0400479F RID: 18335
		public int gem1;

		// Token: 0x040047A0 RID: 18336
		public int gem10;

		// Token: 0x040047A1 RID: 18337
		public int first10;

		// Token: 0x040047A2 RID: 18338
		public int itemId;

		// Token: 0x040047A3 RID: 18339
		public int itemAmount;

		// Token: 0x040047A4 RID: 18340
		public DateTime startAt;

		// Token: 0x040047A5 RID: 18341
		public DateTime endAt;

		// Token: 0x040047A6 RID: 18342
		public int sun;

		// Token: 0x040047A7 RID: 18343
		public int mon;

		// Token: 0x040047A8 RID: 18344
		public int tue;

		// Token: 0x040047A9 RID: 18345
		public int wed;

		// Token: 0x040047AA RID: 18346
		public int thu;

		// Token: 0x040047AB RID: 18347
		public int fri;

		// Token: 0x040047AC RID: 18348
		public int sat;

		// Token: 0x040047AD RID: 18349
		public int redraw;

		// Token: 0x040047AE RID: 18350
		public int drawLimit;

		// Token: 0x040047AF RID: 18351
		public int box1Id;

		// Token: 0x040047B0 RID: 18352
		public int box2Id;

		// Token: 0x040047B1 RID: 18353
		public int box2Limit;

		// Token: 0x040047B2 RID: 18354
		public int box2Type;

		// Token: 0x040047B3 RID: 18355
		public string webViewUrl;
	}
}
