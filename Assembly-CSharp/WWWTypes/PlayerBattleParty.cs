﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB1 RID: 3249
	public class PlayerBattleParty
	{
		// Token: 0x04004828 RID: 18472
		public long managedBattlePartyId;

		// Token: 0x04004829 RID: 18473
		public string name;

		// Token: 0x0400482A RID: 18474
		public long[] managedCharacterIds;

		// Token: 0x0400482B RID: 18475
		public long[] managedWeaponIds;
	}
}
