﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA0 RID: 3232
	public class DropPresent
	{
		// Token: 0x0400475F RID: 18271
		public int itemNo;

		// Token: 0x04004760 RID: 18272
		public long presentAt;

		// Token: 0x04004761 RID: 18273
		public float magnification;
	}
}
