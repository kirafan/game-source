﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C99 RID: 3225
	public class BonusDay
	{
		// Token: 0x0400470F RID: 18191
		public int iconId;

		// Token: 0x04004710 RID: 18192
		public BonusItem[] bonusItems;
	}
}
