﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB0 RID: 3248
	public class Player
	{
		// Token: 0x04004804 RID: 18436
		public long id;

		// Token: 0x04004805 RID: 18437
		public string name;

		// Token: 0x04004806 RID: 18438
		public string comment;

		// Token: 0x04004807 RID: 18439
		public int age;

		// Token: 0x04004808 RID: 18440
		public string myCode;

		// Token: 0x04004809 RID: 18441
		public int level;

		// Token: 0x0400480A RID: 18442
		public long levelExp;

		// Token: 0x0400480B RID: 18443
		public long totalExp;

		// Token: 0x0400480C RID: 18444
		public long gold;

		// Token: 0x0400480D RID: 18445
		public long kirara;

		// Token: 0x0400480E RID: 18446
		public long unlimitedGem;

		// Token: 0x0400480F RID: 18447
		public long limitedGem;

		// Token: 0x04004810 RID: 18448
		public long lotteryTicket;

		// Token: 0x04004811 RID: 18449
		public int partyCost;

		// Token: 0x04004812 RID: 18450
		public long stamina;

		// Token: 0x04004813 RID: 18451
		public long staminaMax;

		// Token: 0x04004814 RID: 18452
		public DateTime staminaUpdatedAt;

		// Token: 0x04004815 RID: 18453
		public int recastTime;

		// Token: 0x04004816 RID: 18454
		public int recastTimeMax;

		// Token: 0x04004817 RID: 18455
		public long kiraraLimit;

		// Token: 0x04004818 RID: 18456
		public int weaponLimit;

		// Token: 0x04004819 RID: 18457
		public int weaponLimitCount;

		// Token: 0x0400481A RID: 18458
		public int facilityLimit;

		// Token: 0x0400481B RID: 18459
		public int facilityLimitCount;

		// Token: 0x0400481C RID: 18460
		public int roomObjectLimit;

		// Token: 0x0400481D RID: 18461
		public int roomObjectLimitCount;

		// Token: 0x0400481E RID: 18462
		public int characterLimit;

		// Token: 0x0400481F RID: 18463
		public int itemLimit;

		// Token: 0x04004820 RID: 18464
		public int friendLimit;

		// Token: 0x04004821 RID: 18465
		public int supportLimit;

		// Token: 0x04004822 RID: 18466
		public int loginCount;

		// Token: 0x04004823 RID: 18467
		public int loginDays;

		// Token: 0x04004824 RID: 18468
		public int continuousDays;

		// Token: 0x04004825 RID: 18469
		public DateTime lastLoginAt;

		// Token: 0x04004826 RID: 18470
		public string ipAddr;

		// Token: 0x04004827 RID: 18471
		public string userAgent;
	}
}
