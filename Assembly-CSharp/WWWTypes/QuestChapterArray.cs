﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC9 RID: 3273
	public class QuestChapterArray
	{
		// Token: 0x04004912 RID: 18706
		public int id;

		// Token: 0x04004913 RID: 18707
		public int difficulty;

		// Token: 0x04004914 RID: 18708
		public string name;

		// Token: 0x04004915 RID: 18709
		public int[] questIds;
	}
}
