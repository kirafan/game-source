﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA2 RID: 3234
	public class FriendList
	{
		// Token: 0x04004789 RID: 18313
		public long managedFriendId;

		// Token: 0x0400478A RID: 18314
		public int state;

		// Token: 0x0400478B RID: 18315
		public int direction;

		// Token: 0x0400478C RID: 18316
		public long playerId;

		// Token: 0x0400478D RID: 18317
		public string name;

		// Token: 0x0400478E RID: 18318
		public int supportLimit;

		// Token: 0x0400478F RID: 18319
		public string comment;

		// Token: 0x04004790 RID: 18320
		public string myCode;

		// Token: 0x04004791 RID: 18321
		public int level;

		// Token: 0x04004792 RID: 18322
		public long totalExp;

		// Token: 0x04004793 RID: 18323
		public DateTime lastLoginAt;

		// Token: 0x04004794 RID: 18324
		public string supportName;

		// Token: 0x04004795 RID: 18325
		public SupportCharacter[] supportCharacters;

		// Token: 0x04004796 RID: 18326
		public SupportCharacter fieldPartyMember;
	}
}
