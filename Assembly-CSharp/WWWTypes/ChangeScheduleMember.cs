﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C9B RID: 3227
	public class ChangeScheduleMember
	{
		// Token: 0x04004714 RID: 18196
		public long managedPartyMemberId;

		// Token: 0x04004715 RID: 18197
		public int scheduleId;

		// Token: 0x04004716 RID: 18198
		public int scheduleTag;

		// Token: 0x04004717 RID: 18199
		public long managedFacilityId;

		// Token: 0x04004718 RID: 18200
		public int touchItemResultNo;

		// Token: 0x04004719 RID: 18201
		public int flag;

		// Token: 0x0400471A RID: 18202
		public string scheduleTable;

		// Token: 0x0400471B RID: 18203
		public DropPresent[] dropPresent;
	}
}
