﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC6 RID: 3270
	public class PlayerWeapon
	{
		// Token: 0x040048B7 RID: 18615
		public long managedWeaponId;

		// Token: 0x040048B8 RID: 18616
		public int level;

		// Token: 0x040048B9 RID: 18617
		public long exp;

		// Token: 0x040048BA RID: 18618
		public int skillLevel;

		// Token: 0x040048BB RID: 18619
		public long skillExp;

		// Token: 0x040048BC RID: 18620
		public int weaponId;
	}
}
