﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCF RID: 3279
	public class TownFacilityBuyState
	{
		// Token: 0x04004949 RID: 18761
		public int facilityId;

		// Token: 0x0400494A RID: 18762
		public int actionNo;

		// Token: 0x0400494B RID: 18763
		public int openState;

		// Token: 0x0400494C RID: 18764
		public int nextLevel;

		// Token: 0x0400494D RID: 18765
		public int buildPointIndex;

		// Token: 0x0400494E RID: 18766
		public long actionTime;

		// Token: 0x0400494F RID: 18767
		public long buildTime;
	}
}
