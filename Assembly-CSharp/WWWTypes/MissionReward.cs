﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAF RID: 3247
	public class MissionReward
	{
		// Token: 0x04004801 RID: 18433
		public int RewardType;

		// Token: 0x04004802 RID: 18434
		public int RewardNo;

		// Token: 0x04004803 RID: 18435
		public int RewardNum;
	}
}
