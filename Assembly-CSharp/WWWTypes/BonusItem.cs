﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C9A RID: 3226
	public class BonusItem
	{
		// Token: 0x04004711 RID: 18193
		public int type;

		// Token: 0x04004712 RID: 18194
		public int objId;

		// Token: 0x04004713 RID: 18195
		public int amount;
	}
}
