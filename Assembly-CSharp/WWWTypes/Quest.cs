﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC7 RID: 3271
	public class Quest
	{
		// Token: 0x040048BD RID: 18621
		public int id;

		// Token: 0x040048BE RID: 18622
		public string name;

		// Token: 0x040048BF RID: 18623
		public int bgId;

		// Token: 0x040048C0 RID: 18624
		public int warn;

		// Token: 0x040048C1 RID: 18625
		public string bgmCue;

		// Token: 0x040048C2 RID: 18626
		public string lastBgmCue;

		// Token: 0x040048C3 RID: 18627
		public int isHideElement;

		// Token: 0x040048C4 RID: 18628
		public int stamina;

		// Token: 0x040048C5 RID: 18629
		public int rewardMoney;

		// Token: 0x040048C6 RID: 18630
		public int rewardUserExp;

		// Token: 0x040048C7 RID: 18631
		public int rewardMasterOrbExp;

		// Token: 0x040048C8 RID: 18632
		public int rewardCharacterExp;

		// Token: 0x040048C9 RID: 18633
		public int rewardFriendshipExp;

		// Token: 0x040048CA RID: 18634
		public int retryLimit;

		// Token: 0x040048CB RID: 18635
		public int loserBattle;

		// Token: 0x040048CC RID: 18636
		public long gold1;

		// Token: 0x040048CD RID: 18637
		public long gem1;

		// Token: 0x040048CE RID: 18638
		public int itemId1;

		// Token: 0x040048CF RID: 18639
		public int amount1;

		// Token: 0x040048D0 RID: 18640
		public int itemId2;

		// Token: 0x040048D1 RID: 18641
		public int amount2;

		// Token: 0x040048D2 RID: 18642
		public int itemId3;

		// Token: 0x040048D3 RID: 18643
		public int amount3;

		// Token: 0x040048D4 RID: 18644
		public int waveId1;

		// Token: 0x040048D5 RID: 18645
		public int waveId2;

		// Token: 0x040048D6 RID: 18646
		public int waveId3;

		// Token: 0x040048D7 RID: 18647
		public int waveId4;

		// Token: 0x040048D8 RID: 18648
		public int waveId5;

		// Token: 0x040048D9 RID: 18649
		public int advOnly;

		// Token: 0x040048DA RID: 18650
		public int advId1;

		// Token: 0x040048DB RID: 18651
		public int advId2;

		// Token: 0x040048DC RID: 18652
		public int advId3;

		// Token: 0x040048DD RID: 18653
		public int cpuId;

		// Token: 0x040048DE RID: 18654
		public int cpuLevel;

		// Token: 0x040048DF RID: 18655
		public int cpuLimitBreak;

		// Token: 0x040048E0 RID: 18656
		public int cpuSkillLevel;

		// Token: 0x040048E1 RID: 18657
		public int cpuWeaponId;

		// Token: 0x040048E2 RID: 18658
		public int cpuWeaponLevel;

		// Token: 0x040048E3 RID: 18659
		public string itemDropCharacters;

		// Token: 0x040048E4 RID: 18660
		public int itemDropAdd;

		// Token: 0x040048E5 RID: 18661
		public int storeReview;
	}
}
