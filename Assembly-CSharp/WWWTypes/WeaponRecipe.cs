﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CD3 RID: 3283
	public class WeaponRecipe
	{
		// Token: 0x04004969 RID: 18793
		public int id;

		// Token: 0x0400496A RID: 18794
		public int dstWeaponId;

		// Token: 0x0400496B RID: 18795
		public int amount;

		// Token: 0x0400496C RID: 18796
		public int itemId0;

		// Token: 0x0400496D RID: 18797
		public int num0;

		// Token: 0x0400496E RID: 18798
		public int itemId1;

		// Token: 0x0400496F RID: 18799
		public int num1;

		// Token: 0x04004970 RID: 18800
		public int itemId2;

		// Token: 0x04004971 RID: 18801
		public int num2;

		// Token: 0x04004972 RID: 18802
		public int itemId3;

		// Token: 0x04004973 RID: 18803
		public int num3;

		// Token: 0x04004974 RID: 18804
		public int itemId4;

		// Token: 0x04004975 RID: 18805
		public int num4;
	}
}
