﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB2 RID: 3250
	public class PlayerBattlePartyMember
	{
		// Token: 0x0400482C RID: 18476
		public long managedBattlePartyId;

		// Token: 0x0400482D RID: 18477
		public long[] managedCharacterIds;

		// Token: 0x0400482E RID: 18478
		public long[] managedWeaponIds;
	}
}
