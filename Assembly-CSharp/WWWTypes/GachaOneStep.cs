﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA7 RID: 3239
	public class GachaOneStep
	{
		// Token: 0x040047C6 RID: 18374
		public int limitedGem;

		// Token: 0x040047C7 RID: 18375
		public int unlimitedGem;

		// Token: 0x040047C8 RID: 18376
		public int lotteryBoxId;

		// Token: 0x040047C9 RID: 18377
		public int count;

		// Token: 0x040047CA RID: 18378
		public int exType;

		// Token: 0x040047CB RID: 18379
		public int exId;

		// Token: 0x040047CC RID: 18380
		public int exAmount;
	}
}
