﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC5 RID: 3269
	public class PlayerTownFacility
	{
		// Token: 0x040048B0 RID: 18608
		public long managedTownFacilityId;

		// Token: 0x040048B1 RID: 18609
		public int facilityId;

		// Token: 0x040048B2 RID: 18610
		public int buildPointIndex;

		// Token: 0x040048B3 RID: 18611
		public int level;

		// Token: 0x040048B4 RID: 18612
		public int openState;

		// Token: 0x040048B5 RID: 18613
		public long actionTime;

		// Token: 0x040048B6 RID: 18614
		public long buildTime;
	}
}
