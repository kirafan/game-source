﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA3 RID: 3235
	public class FriendshipExp
	{
		// Token: 0x04004797 RID: 18327
		public int tableId;

		// Token: 0x04004798 RID: 18328
		public int level;

		// Token: 0x04004799 RID: 18329
		public int exp;
	}
}
