﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C9C RID: 3228
	public class Character
	{
		// Token: 0x0400471C RID: 18204
		public int id;

		// Token: 0x0400471D RID: 18205
		public string name;

		// Token: 0x0400471E RID: 18206
		public int namedType;

		// Token: 0x0400471F RID: 18207
		public int titleType;

		// Token: 0x04004720 RID: 18208
		public int friendshipExpTableId;

		// Token: 0x04004721 RID: 18209
		public int rarity;

		// Token: 0x04004722 RID: 18210
		public int classType;

		// Token: 0x04004723 RID: 18211
		public int elementType;

		// Token: 0x04004724 RID: 18212
		public int cost;

		// Token: 0x04004725 RID: 18213
		public int level;

		// Token: 0x04004726 RID: 18214
		public int levelLimit;

		// Token: 0x04004727 RID: 18215
		public int hp;

		// Token: 0x04004728 RID: 18216
		public int attack;

		// Token: 0x04004729 RID: 18217
		public int magic;

		// Token: 0x0400472A RID: 18218
		public int defence;

		// Token: 0x0400472B RID: 18219
		public int magicDefence;

		// Token: 0x0400472C RID: 18220
		public int speed;

		// Token: 0x0400472D RID: 18221
		public int luck;

		// Token: 0x0400472E RID: 18222
		public int skillLevelLimit;

		// Token: 0x0400472F RID: 18223
		public int charSkillId;

		// Token: 0x04004730 RID: 18224
		public int charSkillExpTableId;

		// Token: 0x04004731 RID: 18225
		public int classSkillId1;

		// Token: 0x04004732 RID: 18226
		public int classSkillExpTableId1;

		// Token: 0x04004733 RID: 18227
		public int classSkillId2;

		// Token: 0x04004734 RID: 18228
		public int classSkillExpTableId2;
	}
}
