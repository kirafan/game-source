﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CBB RID: 3259
	public class PlayerNamedType
	{
		// Token: 0x04004867 RID: 18535
		public long managedNamedTypeId;

		// Token: 0x04004868 RID: 18536
		public int namedType;

		// Token: 0x04004869 RID: 18537
		public int titleType;

		// Token: 0x0400486A RID: 18538
		public int level;

		// Token: 0x0400486B RID: 18539
		public long exp;
	}
}
