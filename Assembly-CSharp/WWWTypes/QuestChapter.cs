﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CC8 RID: 3272
	public class QuestChapter
	{
		// Token: 0x040048E6 RID: 18662
		public int id;

		// Token: 0x040048E7 RID: 18663
		public int difficulty;

		// Token: 0x040048E8 RID: 18664
		public string name;

		// Token: 0x040048E9 RID: 18665
		public int questId0;

		// Token: 0x040048EA RID: 18666
		public int questId1;

		// Token: 0x040048EB RID: 18667
		public int questId2;

		// Token: 0x040048EC RID: 18668
		public int questId3;

		// Token: 0x040048ED RID: 18669
		public int questId4;

		// Token: 0x040048EE RID: 18670
		public int questId5;

		// Token: 0x040048EF RID: 18671
		public int questId6;

		// Token: 0x040048F0 RID: 18672
		public int questId7;

		// Token: 0x040048F1 RID: 18673
		public int questId8;

		// Token: 0x040048F2 RID: 18674
		public int questId9;

		// Token: 0x040048F3 RID: 18675
		public int questId10;

		// Token: 0x040048F4 RID: 18676
		public int questId11;

		// Token: 0x040048F5 RID: 18677
		public int questId12;

		// Token: 0x040048F6 RID: 18678
		public int questId13;

		// Token: 0x040048F7 RID: 18679
		public int questId14;

		// Token: 0x040048F8 RID: 18680
		public int questId15;

		// Token: 0x040048F9 RID: 18681
		public int questId16;

		// Token: 0x040048FA RID: 18682
		public int questId17;

		// Token: 0x040048FB RID: 18683
		public int questId18;

		// Token: 0x040048FC RID: 18684
		public int questId19;

		// Token: 0x040048FD RID: 18685
		public int questId20;

		// Token: 0x040048FE RID: 18686
		public int questId21;

		// Token: 0x040048FF RID: 18687
		public int questId22;

		// Token: 0x04004900 RID: 18688
		public int questId23;

		// Token: 0x04004901 RID: 18689
		public int questId24;

		// Token: 0x04004902 RID: 18690
		public int questId25;

		// Token: 0x04004903 RID: 18691
		public int questId26;

		// Token: 0x04004904 RID: 18692
		public int questId27;

		// Token: 0x04004905 RID: 18693
		public int questId28;

		// Token: 0x04004906 RID: 18694
		public int questId29;

		// Token: 0x04004907 RID: 18695
		public int questId30;

		// Token: 0x04004908 RID: 18696
		public int questId31;

		// Token: 0x04004909 RID: 18697
		public int questId32;

		// Token: 0x0400490A RID: 18698
		public int questId33;

		// Token: 0x0400490B RID: 18699
		public int questId34;

		// Token: 0x0400490C RID: 18700
		public int questId35;

		// Token: 0x0400490D RID: 18701
		public int questId36;

		// Token: 0x0400490E RID: 18702
		public int questId37;

		// Token: 0x0400490F RID: 18703
		public int questId38;

		// Token: 0x04004910 RID: 18704
		public int questId39;

		// Token: 0x04004911 RID: 18705
		public int questId40;
	}
}
