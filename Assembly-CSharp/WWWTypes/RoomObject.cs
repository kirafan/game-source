﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCB RID: 3275
	public class RoomObject
	{
		// Token: 0x0400491E RID: 18718
		public int id;

		// Token: 0x0400491F RID: 18719
		public string name;

		// Token: 0x04004920 RID: 18720
		public int type;

		// Token: 0x04004921 RID: 18721
		public int buyAmount;

		// Token: 0x04004922 RID: 18722
		public int saleAmount;

		// Token: 0x04004923 RID: 18723
		public int objectLimit;

		// Token: 0x04004924 RID: 18724
		public int shopFlag;

		// Token: 0x04004925 RID: 18725
		public DateTime dispStartAt;

		// Token: 0x04004926 RID: 18726
		public DateTime dispEndAt;

		// Token: 0x04004927 RID: 18727
		public int bargainFlag;

		// Token: 0x04004928 RID: 18728
		public DateTime bargainStartAt;

		// Token: 0x04004929 RID: 18729
		public DateTime bargainEndAt;

		// Token: 0x0400492A RID: 18730
		public int bargainBuyAmount;
	}
}
