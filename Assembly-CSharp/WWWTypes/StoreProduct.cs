﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCD RID: 3277
	public class StoreProduct
	{
		// Token: 0x0400492E RID: 18734
		public int id;

		// Token: 0x0400492F RID: 18735
		public int platform;

		// Token: 0x04004930 RID: 18736
		public int env;

		// Token: 0x04004931 RID: 18737
		public string sku;

		// Token: 0x04004932 RID: 18738
		public int amount1;

		// Token: 0x04004933 RID: 18739
		public int amount2;

		// Token: 0x04004934 RID: 18740
		public long price;

		// Token: 0x04004935 RID: 18741
		public int limitNum;

		// Token: 0x04004936 RID: 18742
		public int linkId;

		// Token: 0x04004937 RID: 18743
		public int uiPriority;

		// Token: 0x04004938 RID: 18744
		public int uiType;

		// Token: 0x04004939 RID: 18745
		public string name;

		// Token: 0x0400493A RID: 18746
		public string description;
	}
}
