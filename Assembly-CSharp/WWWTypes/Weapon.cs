﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CD1 RID: 3281
	public class Weapon
	{
		// Token: 0x04004955 RID: 18773
		public int id;

		// Token: 0x04004956 RID: 18774
		public string name;

		// Token: 0x04004957 RID: 18775
		public int classType;

		// Token: 0x04004958 RID: 18776
		public int cost;

		// Token: 0x04004959 RID: 18777
		public int expTableId;

		// Token: 0x0400495A RID: 18778
		public int levelLimit;

		// Token: 0x0400495B RID: 18779
		public int attack;

		// Token: 0x0400495C RID: 18780
		public int magic;

		// Token: 0x0400495D RID: 18781
		public int defense;

		// Token: 0x0400495E RID: 18782
		public int magicDefence;

		// Token: 0x0400495F RID: 18783
		public int attackMax;

		// Token: 0x04004960 RID: 18784
		public int magicMax;

		// Token: 0x04004961 RID: 18785
		public int defenseMax;

		// Token: 0x04004962 RID: 18786
		public int magicDefenceMax;

		// Token: 0x04004963 RID: 18787
		public int skillId;

		// Token: 0x04004964 RID: 18788
		public int saleAmount;

		// Token: 0x04004965 RID: 18789
		public int[] bonuses;
	}
}
