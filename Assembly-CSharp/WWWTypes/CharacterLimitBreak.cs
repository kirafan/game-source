﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C9F RID: 3231
	public class CharacterLimitBreak
	{
		// Token: 0x04004745 RID: 18245
		public int recipeId;

		// Token: 0x04004746 RID: 18246
		public int rarity;

		// Token: 0x04004747 RID: 18247
		public int levelUpMax1;

		// Token: 0x04004748 RID: 18248
		public int skillUpMax1;

		// Token: 0x04004749 RID: 18249
		public int amount1;

		// Token: 0x0400474A RID: 18250
		public int num10;

		// Token: 0x0400474B RID: 18251
		public int num11;

		// Token: 0x0400474C RID: 18252
		public int num12;

		// Token: 0x0400474D RID: 18253
		public int levelUpMax2;

		// Token: 0x0400474E RID: 18254
		public int skillUpMax2;

		// Token: 0x0400474F RID: 18255
		public int amount2;

		// Token: 0x04004750 RID: 18256
		public int num20;

		// Token: 0x04004751 RID: 18257
		public int num21;

		// Token: 0x04004752 RID: 18258
		public int num22;

		// Token: 0x04004753 RID: 18259
		public int levelUpMax3;

		// Token: 0x04004754 RID: 18260
		public int skillUpMax3;

		// Token: 0x04004755 RID: 18261
		public int amount3;

		// Token: 0x04004756 RID: 18262
		public int num30;

		// Token: 0x04004757 RID: 18263
		public int num31;

		// Token: 0x04004758 RID: 18264
		public int num32;

		// Token: 0x04004759 RID: 18265
		public int levelUpMax4;

		// Token: 0x0400475A RID: 18266
		public int skillUpMax4;

		// Token: 0x0400475B RID: 18267
		public int amount4;

		// Token: 0x0400475C RID: 18268
		public int num40;

		// Token: 0x0400475D RID: 18269
		public int num41;

		// Token: 0x0400475E RID: 18270
		public int num42;
	}
}
