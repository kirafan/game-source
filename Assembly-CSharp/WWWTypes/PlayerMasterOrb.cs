﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB9 RID: 3257
	public class PlayerMasterOrb
	{
		// Token: 0x0400485A RID: 18522
		public long managedMasterOrbId;

		// Token: 0x0400485B RID: 18523
		public int level;

		// Token: 0x0400485C RID: 18524
		public long exp;

		// Token: 0x0400485D RID: 18525
		public int equipment;

		// Token: 0x0400485E RID: 18526
		public int masterOrbId;
	}
}
