﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C9D RID: 3229
	public class CharacterEvolution
	{
		// Token: 0x04004735 RID: 18229
		public int recipeId;

		// Token: 0x04004736 RID: 18230
		public int srcCharacterId;

		// Token: 0x04004737 RID: 18231
		public int dstCharacterId;

		// Token: 0x04004738 RID: 18232
		public int amount;

		// Token: 0x04004739 RID: 18233
		public int itemId0;

		// Token: 0x0400473A RID: 18234
		public int num0;

		// Token: 0x0400473B RID: 18235
		public int itemId1;

		// Token: 0x0400473C RID: 18236
		public int num1;

		// Token: 0x0400473D RID: 18237
		public int itemId2;

		// Token: 0x0400473E RID: 18238
		public int num2;

		// Token: 0x0400473F RID: 18239
		public int itemId3;

		// Token: 0x04004740 RID: 18240
		public int num3;

		// Token: 0x04004741 RID: 18241
		public int itemId4;

		// Token: 0x04004742 RID: 18242
		public int num4;
	}
}
