﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA1 RID: 3233
	public class EventQuest
	{
		// Token: 0x04004762 RID: 18274
		public int id;

		// Token: 0x04004763 RID: 18275
		public int eventType;

		// Token: 0x04004764 RID: 18276
		public int groupId;

		// Token: 0x04004765 RID: 18277
		public string groupName;

		// Token: 0x04004766 RID: 18278
		public int groupSkip;

		// Token: 0x04004767 RID: 18279
		public Quest quest;

		// Token: 0x04004768 RID: 18280
		public DateTime startAt;

		// Token: 0x04004769 RID: 18281
		public DateTime endAt;

		// Token: 0x0400476A RID: 18282
		public int orderLimit;

		// Token: 0x0400476B RID: 18283
		public int freq;

		// Token: 0x0400476C RID: 18284
		public int interval;

		// Token: 0x0400476D RID: 18285
		public int month;

		// Token: 0x0400476E RID: 18286
		public int day;

		// Token: 0x0400476F RID: 18287
		public int sun;

		// Token: 0x04004770 RID: 18288
		public int mon;

		// Token: 0x04004771 RID: 18289
		public int tue;

		// Token: 0x04004772 RID: 18290
		public int wed;

		// Token: 0x04004773 RID: 18291
		public int thu;

		// Token: 0x04004774 RID: 18292
		public int fri;

		// Token: 0x04004775 RID: 18293
		public int sat;

		// Token: 0x04004776 RID: 18294
		public int exId1;

		// Token: 0x04004777 RID: 18295
		public int exId2;

		// Token: 0x04004778 RID: 18296
		public int ex2Amount;

		// Token: 0x04004779 RID: 18297
		public string exId3;

		// Token: 0x0400477A RID: 18298
		public int exTitle;

		// Token: 0x0400477B RID: 18299
		public int exName;

		// Token: 0x0400477C RID: 18300
		public int exRarity;

		// Token: 0x0400477D RID: 18301
		public int exCost;

		// Token: 0x0400477E RID: 18302
		public int exFire;

		// Token: 0x0400477F RID: 18303
		public int exWater;

		// Token: 0x04004780 RID: 18304
		public int exEarth;

		// Token: 0x04004781 RID: 18305
		public int exWind;

		// Token: 0x04004782 RID: 18306
		public int exMoon;

		// Token: 0x04004783 RID: 18307
		public int exSun;

		// Token: 0x04004784 RID: 18308
		public int exFighter;

		// Token: 0x04004785 RID: 18309
		public int exMagician;

		// Token: 0x04004786 RID: 18310
		public int exPriest;

		// Token: 0x04004787 RID: 18311
		public int exKnight;

		// Token: 0x04004788 RID: 18312
		public int exAlchemist;
	}
}
