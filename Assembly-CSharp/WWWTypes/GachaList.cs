﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA5 RID: 3237
	public class GachaList
	{
		// Token: 0x040047B4 RID: 18356
		public Gacha gacha;

		// Token: 0x040047B5 RID: 18357
		public int uGem1Total;

		// Token: 0x040047B6 RID: 18358
		public int uGem1Daily;

		// Token: 0x040047B7 RID: 18359
		public int gem1Total;

		// Token: 0x040047B8 RID: 18360
		public int gem1Daily;

		// Token: 0x040047B9 RID: 18361
		public int gem10Total;

		// Token: 0x040047BA RID: 18362
		public int gem10Daily;

		// Token: 0x040047BB RID: 18363
		public int itemTotal;

		// Token: 0x040047BC RID: 18364
		public int itemDaily;
	}
}
