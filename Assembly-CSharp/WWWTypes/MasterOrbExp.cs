﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAE RID: 3246
	public class MasterOrbExp
	{
		// Token: 0x040047FE RID: 18430
		public int tableId;

		// Token: 0x040047FF RID: 18431
		public int level;

		// Token: 0x04004800 RID: 18432
		public int exp;
	}
}
