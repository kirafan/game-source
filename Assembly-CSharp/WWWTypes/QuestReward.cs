﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCA RID: 3274
	public class QuestReward
	{
		// Token: 0x04004916 RID: 18710
		public long gold;

		// Token: 0x04004917 RID: 18711
		public long gem;

		// Token: 0x04004918 RID: 18712
		public int itemId1;

		// Token: 0x04004919 RID: 18713
		public int amount1;

		// Token: 0x0400491A RID: 18714
		public int itemId2;

		// Token: 0x0400491B RID: 18715
		public int amount2;

		// Token: 0x0400491C RID: 18716
		public int itemId3;

		// Token: 0x0400491D RID: 18717
		public int amount3;
	}
}
