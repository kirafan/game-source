﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA9 RID: 3241
	public class GachaResult
	{
		// Token: 0x040047D1 RID: 18385
		public int characterId;

		// Token: 0x040047D2 RID: 18386
		public int itemId;

		// Token: 0x040047D3 RID: 18387
		public int itemAmount;
	}
}
