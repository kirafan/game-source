﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CBD RID: 3261
	public class PlayerPurchase
	{
		// Token: 0x04004877 RID: 18551
		public long id;

		// Token: 0x04004878 RID: 18552
		public int platform;

		// Token: 0x04004879 RID: 18553
		public string receiptNo;

		// Token: 0x0400487A RID: 18554
		public string sku;

		// Token: 0x0400487B RID: 18555
		public long price;

		// Token: 0x0400487C RID: 18556
		public string name;

		// Token: 0x0400487D RID: 18557
		public int state;

		// Token: 0x0400487E RID: 18558
		public DateTime purchasedAt;
	}
}
