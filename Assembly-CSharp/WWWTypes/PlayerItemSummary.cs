﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB8 RID: 3256
	public class PlayerItemSummary
	{
		// Token: 0x04004858 RID: 18520
		public int id;

		// Token: 0x04004859 RID: 18521
		public int amount;
	}
}
