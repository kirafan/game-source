﻿using System;

namespace WWWTypes
{
	// Token: 0x02000C96 RID: 3222
	public enum ResultCode
	{
		// Token: 0x040046BA RID: 18106
		ERROR = -1,
		// Token: 0x040046BB RID: 18107
		SUCCESS,
		// Token: 0x040046BC RID: 18108
		OUTDATED,
		// Token: 0x040046BD RID: 18109
		APPLYING,
		// Token: 0x040046BE RID: 18110
		ACCESS_LIMITATION,
		// Token: 0x040046BF RID: 18111
		TERMS_UPDATED,
		// Token: 0x040046C0 RID: 18112
		MAINTENANCE = 10,
		// Token: 0x040046C1 RID: 18113
		UNAVAILABLE = 20,
		// Token: 0x040046C2 RID: 18114
		OUTDATED_AB_VERSION = 30,
		// Token: 0x040046C3 RID: 18115
		INVALID_REQUEST_HASH = 101,
		// Token: 0x040046C4 RID: 18116
		INVALID_PARAMETERS,
		// Token: 0x040046C5 RID: 18117
		INSUFFICIENT_PARAMETERS,
		// Token: 0x040046C6 RID: 18118
		INVALID_JSON_SCHEMA,
		// Token: 0x040046C7 RID: 18119
		DB_ERROR,
		// Token: 0x040046C8 RID: 18120
		NG_WORD,
		// Token: 0x040046C9 RID: 18121
		PLAYER_ALREADY_EXISTS = 201,
		// Token: 0x040046CA RID: 18122
		PLAYER_NOT_FOUND,
		// Token: 0x040046CB RID: 18123
		PLAYER_SESSION_EXPIRED,
		// Token: 0x040046CC RID: 18124
		PLAYER_SUSPENDED,
		// Token: 0x040046CD RID: 18125
		PLAYER_STOPPED,
		// Token: 0x040046CE RID: 18126
		PLAYER_DELETED,
		// Token: 0x040046CF RID: 18127
		INVALID_MOVE_PARAMETERS = 210,
		// Token: 0x040046D0 RID: 18128
		INVALID_LINK_PARAMETERS,
		// Token: 0x040046D1 RID: 18129
		UUID_NOT_FOUND,
		// Token: 0x040046D2 RID: 18130
		WEAPON_UPGRADE_LIMIT = 293,
		// Token: 0x040046D3 RID: 18131
		CHARACTER_UPGRADE_LIMIT,
		// Token: 0x040046D4 RID: 18132
		FACILITY_EXT_LIMIT,
		// Token: 0x040046D5 RID: 18133
		SUPPORT_CHAR_LIMIT,
		// Token: 0x040046D6 RID: 18134
		SUPPORT_LIMIT,
		// Token: 0x040046D7 RID: 18135
		ROOM_OBJECT_EXT_LIMIT,
		// Token: 0x040046D8 RID: 18136
		WEAPON_EXT_LIMIT,
		// Token: 0x040046D9 RID: 18137
		WEAPON_LIMIT = 301,
		// Token: 0x040046DA RID: 18138
		FACILITY_LIMIT,
		// Token: 0x040046DB RID: 18139
		ROOM_OBJECT_LIMIT,
		// Token: 0x040046DC RID: 18140
		CHARACTER_LIMIT,
		// Token: 0x040046DD RID: 18141
		ITEM_LIMIT,
		// Token: 0x040046DE RID: 18142
		TOWN_LIMIT,
		// Token: 0x040046DF RID: 18143
		ROOM_LIMIT,
		// Token: 0x040046E0 RID: 18144
		LIMIT_BREAK_LIMIT,
		// Token: 0x040046E1 RID: 18145
		GACHA_DRAW_LIMIT,
		// Token: 0x040046E2 RID: 18146
		FRIEND_LIMIT,
		// Token: 0x040046E3 RID: 18147
		FAVORITE_ITEM,
		// Token: 0x040046E4 RID: 18148
		FAVORITE_WEAPON,
		// Token: 0x040046E5 RID: 18149
		FAVORITE_CHARACTER,
		// Token: 0x040046E6 RID: 18150
		GACHA_STEP_DRAW_LIMIT,
		// Token: 0x040046E7 RID: 18151
		ALREADY_OWN_CHARACTER = 331,
		// Token: 0x040046E8 RID: 18152
		ALREADY_LOAD_WEAPON,
		// Token: 0x040046E9 RID: 18153
		ALREADY_OWN_ORB,
		// Token: 0x040046EA RID: 18154
		GOLD_IS_SHORT = 441,
		// Token: 0x040046EB RID: 18155
		ITEM_IS_SHORT,
		// Token: 0x040046EC RID: 18156
		UNLIMITED_GEM_IS_SHORT,
		// Token: 0x040046ED RID: 18157
		LIMITED_GEM_IS_SHORT,
		// Token: 0x040046EE RID: 18158
		GEM_IS_SHORT,
		// Token: 0x040046EF RID: 18159
		KIRARA_IS_SHORT,
		// Token: 0x040046F0 RID: 18160
		STAMINA_IS_SHORT = 451,
		// Token: 0x040046F1 RID: 18161
		COST_IS_SHORT,
		// Token: 0x040046F2 RID: 18162
		QUEST_OUT_OF_PERIOD = 461,
		// Token: 0x040046F3 RID: 18163
		GACHA_OUT_OF_PERIOD,
		// Token: 0x040046F4 RID: 18164
		TRADE_OUT_OF_PERIOD,
		// Token: 0x040046F5 RID: 18165
		BUY_OUT_OF_PERIOD,
		// Token: 0x040046F6 RID: 18166
		QUEST_RETRY_LIMIT,
		// Token: 0x040046F7 RID: 18167
		QUEST_ORDER_LIMIT,
		// Token: 0x040046F8 RID: 18168
		BARGAIN_OUT_OF_PERIOD,
		// Token: 0x040046F9 RID: 18169
		NOT_YET_FRIEND = 501,
		// Token: 0x040046FA RID: 18170
		ALREADY_FRIEND,
		// Token: 0x040046FB RID: 18171
		ALREADY_COMPLETE_MISSION = 510,
		// Token: 0x040046FC RID: 18172
		CURL_CONNECTION_TIMEOUT = 601,
		// Token: 0x040046FD RID: 18173
		CURL_CONNETION_ERROR,
		// Token: 0x040046FE RID: 18174
		APP_STORE_ERROR,
		// Token: 0x040046FF RID: 18175
		GOOGLE_PLAY_ERROR,
		// Token: 0x04004700 RID: 18176
		APP_STORE_INVALID_RECEIPT,
		// Token: 0x04004701 RID: 18177
		GOOGLE_PLAY_INVALID_STATUS,
		// Token: 0x04004702 RID: 18178
		STORE_ITEM_NOT_FOUND,
		// Token: 0x04004703 RID: 18179
		STORE_ALREADY_PURCHASED,
		// Token: 0x04004704 RID: 18180
		PURCHASE_GEM_OVERFLOW,
		// Token: 0x04004705 RID: 18181
		PROMOTION_CODE_NOT_FOUND = 801,
		// Token: 0x04004706 RID: 18182
		PROMOTION_CODE_ALREADY_USED,
		// Token: 0x04004707 RID: 18183
		PROMOTION_CODE_OF_SAME_CAMPAIGN,
		// Token: 0x04004708 RID: 18184
		PROMOTION_CAMPAIGN_NOT_OPEN,
		// Token: 0x04004709 RID: 18185
		PROMOTION_CAMPAIGN_ALREADY_OPEN,
		// Token: 0x0400470A RID: 18186
		UNKNOWN_ERROR = 999
	}
}
