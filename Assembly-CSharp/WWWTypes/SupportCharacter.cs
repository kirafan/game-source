﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCE RID: 3278
	public class SupportCharacter
	{
		// Token: 0x0400493B RID: 18747
		public long managedCharacterId;

		// Token: 0x0400493C RID: 18748
		public int characterId;

		// Token: 0x0400493D RID: 18749
		public int level;

		// Token: 0x0400493E RID: 18750
		public long exp;

		// Token: 0x0400493F RID: 18751
		public int levelBreak;

		// Token: 0x04004940 RID: 18752
		public int skillLevel1;

		// Token: 0x04004941 RID: 18753
		public int skillLevel2;

		// Token: 0x04004942 RID: 18754
		public int skillLevel3;

		// Token: 0x04004943 RID: 18755
		public int weaponId;

		// Token: 0x04004944 RID: 18756
		public int weaponLevel;

		// Token: 0x04004945 RID: 18757
		public int weaponSkillLevel;

		// Token: 0x04004946 RID: 18758
		public long weaponSkillExp;

		// Token: 0x04004947 RID: 18759
		public int namedLevel;

		// Token: 0x04004948 RID: 18760
		public long namedExp;
	}
}
