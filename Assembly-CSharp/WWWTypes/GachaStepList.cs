﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CA8 RID: 3240
	public class GachaStepList
	{
		// Token: 0x040047CD RID: 18381
		public GachaStep gachaStep;

		// Token: 0x040047CE RID: 18382
		public int step;

		// Token: 0x040047CF RID: 18383
		public int total;

		// Token: 0x040047D0 RID: 18384
		public int daily;
	}
}
