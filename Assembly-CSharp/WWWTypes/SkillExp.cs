﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CCC RID: 3276
	public class SkillExp
	{
		// Token: 0x0400492B RID: 18731
		public int tableId;

		// Token: 0x0400492C RID: 18732
		public int level;

		// Token: 0x0400492D RID: 18733
		public int exp;
	}
}
