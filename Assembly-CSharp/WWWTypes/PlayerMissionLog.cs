﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CBA RID: 3258
	public class PlayerMissionLog
	{
		// Token: 0x0400485F RID: 18527
		public long managedMissionId;

		// Token: 0x04004860 RID: 18528
		public int missionID;

		// Token: 0x04004861 RID: 18529
		public long subCode;

		// Token: 0x04004862 RID: 18530
		public int rate;

		// Token: 0x04004863 RID: 18531
		public int rateMax;

		// Token: 0x04004864 RID: 18532
		public int state;

		// Token: 0x04004865 RID: 18533
		public MissionReward[] reward;

		// Token: 0x04004866 RID: 18534
		public DateTime limitTime;
	}
}
