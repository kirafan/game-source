﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CAA RID: 3242
	public class Information
	{
		// Token: 0x040047D4 RID: 18388
		public int id;

		// Token: 0x040047D5 RID: 18389
		public string title;

		// Token: 0x040047D6 RID: 18390
		public string body;

		// Token: 0x040047D7 RID: 18391
		public string url;

		// Token: 0x040047D8 RID: 18392
		public string imgId;

		// Token: 0x040047D9 RID: 18393
		public int platform;

		// Token: 0x040047DA RID: 18394
		public DateTime startAt;

		// Token: 0x040047DB RID: 18395
		public DateTime endAt;
	}
}
