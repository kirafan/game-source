﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CD0 RID: 3280
	public class TownFacilitySetState
	{
		// Token: 0x04004950 RID: 18768
		public long managedTownFacilityId;

		// Token: 0x04004951 RID: 18769
		public int buildPointIndex;

		// Token: 0x04004952 RID: 18770
		public int openState;

		// Token: 0x04004953 RID: 18771
		public long buildTime;

		// Token: 0x04004954 RID: 18772
		public int actionNo;
	}
}
