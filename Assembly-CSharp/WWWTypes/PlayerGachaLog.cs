﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB6 RID: 3254
	public class PlayerGachaLog
	{
		// Token: 0x0400484E RID: 18510
		public long playerId;

		// Token: 0x0400484F RID: 18511
		public int gachaId;

		// Token: 0x04004850 RID: 18512
		public int drawType;

		// Token: 0x04004851 RID: 18513
		public DateTime drawAt;

		// Token: 0x04004852 RID: 18514
		public GachaResult[] gachaResults;
	}
}
