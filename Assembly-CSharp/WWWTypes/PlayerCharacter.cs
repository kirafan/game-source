﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CB3 RID: 3251
	public class PlayerCharacter
	{
		// Token: 0x0400482F RID: 18479
		public long managedCharacterId;

		// Token: 0x04004830 RID: 18480
		public int level;

		// Token: 0x04004831 RID: 18481
		public int levelLimit;

		// Token: 0x04004832 RID: 18482
		public long exp;

		// Token: 0x04004833 RID: 18483
		public int levelBreak;

		// Token: 0x04004834 RID: 18484
		public int skillLevel1;

		// Token: 0x04004835 RID: 18485
		public int skillLevelLimit1;

		// Token: 0x04004836 RID: 18486
		public long skillExp1;

		// Token: 0x04004837 RID: 18487
		public int skillLevel2;

		// Token: 0x04004838 RID: 18488
		public int skillLevelLimit2;

		// Token: 0x04004839 RID: 18489
		public long skillExp2;

		// Token: 0x0400483A RID: 18490
		public int skillLevel3;

		// Token: 0x0400483B RID: 18491
		public int skillLevelLimit3;

		// Token: 0x0400483C RID: 18492
		public long skillExp3;

		// Token: 0x0400483D RID: 18493
		public int characterId;

		// Token: 0x0400483E RID: 18494
		public int shown;
	}
}
