﻿using System;

namespace WWWTypes
{
	// Token: 0x02000CBF RID: 3263
	public class PlayerQuestLog
	{
		// Token: 0x04004881 RID: 18561
		public long id;

		// Token: 0x04004882 RID: 18562
		public int type;

		// Token: 0x04004883 RID: 18563
		public int questId;

		// Token: 0x04004884 RID: 18564
		public int state;

		// Token: 0x04004885 RID: 18565
		public DateTime orderReceivedAt;

		// Token: 0x04004886 RID: 18566
		public DateTime failedAt;

		// Token: 0x04004887 RID: 18567
		public DateTime retryAt;

		// Token: 0x04004888 RID: 18568
		public DateTime clearedAt;

		// Token: 0x04004889 RID: 18569
		public int clearRank;

		// Token: 0x0400488A RID: 18570
		public long managedPartyId;

		// Token: 0x0400488B RID: 18571
		public long managedCharacterId1;

		// Token: 0x0400488C RID: 18572
		public long managedCharacterId2;

		// Token: 0x0400488D RID: 18573
		public long managedCharacterId3;

		// Token: 0x0400488E RID: 18574
		public long managedCharacterId4;

		// Token: 0x0400488F RID: 18575
		public long managedCharacterId5;

		// Token: 0x04004890 RID: 18576
		public int rewardMoney;

		// Token: 0x04004891 RID: 18577
		public int rewardUserExp;

		// Token: 0x04004892 RID: 18578
		public int rewardMasterOrbExp;

		// Token: 0x04004893 RID: 18579
		public int rewardCharacterExp;

		// Token: 0x04004894 RID: 18580
		public int rewardFriendshipExp;

		// Token: 0x04004895 RID: 18581
		public string skillExp;

		// Token: 0x04004896 RID: 18582
		public string dropItem;

		// Token: 0x04004897 RID: 18583
		public string killedEnemy;

		// Token: 0x04004898 RID: 18584
		public string weaponSkillExp;

		// Token: 0x04004899 RID: 18585
		public int friendUseNum;

		// Token: 0x0400489A RID: 18586
		public int masterSkillUseNum;

		// Token: 0x0400489B RID: 18587
		public int uniqueSkillUseNum;
	}
}
