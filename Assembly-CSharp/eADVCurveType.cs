﻿using System;

// Token: 0x0200006C RID: 108
public enum eADVCurveType
{
	// Token: 0x040001B4 RID: 436
	Linear,
	// Token: 0x040001B5 RID: 437
	InQuad,
	// Token: 0x040001B6 RID: 438
	OutQuad,
	// Token: 0x040001B7 RID: 439
	InOutQuad,
	// Token: 0x040001B8 RID: 440
	InCubic,
	// Token: 0x040001B9 RID: 441
	OutCubic,
	// Token: 0x040001BA RID: 442
	InOutCubic
}
