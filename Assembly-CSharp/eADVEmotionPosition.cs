﻿using System;

// Token: 0x0200006A RID: 106
public enum eADVEmotionPosition
{
	// Token: 0x040001A4 RID: 420
	CharaDefault,
	// Token: 0x040001A5 RID: 421
	CharaFace,
	// Token: 0x040001A6 RID: 422
	CharaFaceLeft,
	// Token: 0x040001A7 RID: 423
	CharaFaceRight,
	// Token: 0x040001A8 RID: 424
	CharaBottom
}
