﻿using System;

// Token: 0x02000E91 RID: 3729
public interface IPropertyAccessor
{
	// Token: 0x06004D4A RID: 19786
	object GetValue(object target);

	// Token: 0x06004D4B RID: 19787
	void SetValue(object target, object value);
}
