﻿using System;
using System.Collections.Generic;
using Meige;
using QuestChapterResponseTypes;
using WWWTypes;

// Token: 0x02000C04 RID: 3076
public static class QuestChapterResponse
{
	// Token: 0x06003FA7 RID: 16295 RVA: 0x0013CB68 File Offset: 0x0013AF68
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FA8 RID: 16296 RVA: 0x0013CB80 File Offset: 0x0013AF80
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}
}
