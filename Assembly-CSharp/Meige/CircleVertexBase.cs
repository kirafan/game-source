﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F46 RID: 3910
	public static class CircleVertexBase
	{
		// Token: 0x060050EB RID: 20715 RVA: 0x0016A508 File Offset: 0x00168908
		static CircleVertexBase()
		{
			for (int i = 0; i < 1440; i++)
			{
				float f = (float)i / 1440f * 3.1415927f * 2f;
				CircleVertexBase.m_Vertices[i].x = Mathf.Sin(f);
				CircleVertexBase.m_Vertices[i].y = Mathf.Cos(f);
			}
		}

		// Token: 0x060050EC RID: 20716 RVA: 0x0016A57B File Offset: 0x0016897B
		public static int GetIndexFromAngle(float angle)
		{
			return (int)(Mathf.Repeat(angle, 360f) / 360f * 1439f);
		}

		// Token: 0x060050ED RID: 20717 RVA: 0x0016A595 File Offset: 0x00168995
		public static Vector2 GetVertexFromAngle(float angle)
		{
			return CircleVertexBase.m_Vertices[CircleVertexBase.GetIndexFromAngle(angle)];
		}

		// Token: 0x04005006 RID: 20486
		public const int CircleVertexNum = 1440;

		// Token: 0x04005007 RID: 20487
		public static Vector2[] m_Vertices = new Vector2[1440];
	}
}
