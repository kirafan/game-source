﻿using System;

namespace Meige
{
	// Token: 0x02000EF2 RID: 3826
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class TimelineFieldAttribute : Attribute
	{
		// Token: 0x06004EDB RID: 20187 RVA: 0x0015D407 File Offset: 0x0015B807
		public TimelineFieldAttribute(string name, string affiliation)
		{
			this.name = name;
			this.affiliation = affiliation;
		}

		// Token: 0x04004E92 RID: 20114
		public string name;

		// Token: 0x04004E93 RID: 20115
		public string affiliation;

		// Token: 0x04004E94 RID: 20116
		public int id;
	}
}
