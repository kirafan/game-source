﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000ED1 RID: 3793
	[Serializable]
	public class EffectRuleArrayParam_Vector2 : EffectRuleArrayParam<Vector2>
	{
	}
}
