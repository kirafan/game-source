﻿using System;

namespace Meige
{
	// Token: 0x02000F84 RID: 3972
	public enum eParticleEmitDirection : byte
	{
		// Token: 0x0400530B RID: 21259
		eParticleEmitDirection_ShapeAlong,
		// Token: 0x0400530C RID: 21260
		eParticleEmitDirection_Random
	}
}
