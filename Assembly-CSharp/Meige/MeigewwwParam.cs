﻿using System;
using Newtonsoft.Json;

namespace Meige
{
	// Token: 0x02000FDF RID: 4063
	public class MeigewwwParam
	{
		// Token: 0x060054A4 RID: 21668 RVA: 0x0017C28A File Offset: 0x0017A68A
		public MeigewwwParam()
		{
			this.Clear();
		}

		// Token: 0x060054A5 RID: 21669 RVA: 0x0017C298 File Offset: 0x0017A698
		public bool IsDone()
		{
			return this.m_status == MeigewwwParam.eStatus.Done;
		}

		// Token: 0x060054A6 RID: 21670 RVA: 0x0017C2A3 File Offset: 0x0017A6A3
		public MeigewwwParam.eStatus GetStatus()
		{
			return this.m_status;
		}

		// Token: 0x060054A7 RID: 21671 RVA: 0x0017C2AB File Offset: 0x0017A6AB
		public void SetStatus(MeigewwwParam.eStatus status)
		{
			this.m_status = status;
		}

		// Token: 0x060054A8 RID: 21672 RVA: 0x0017C2B4 File Offset: 0x0017A6B4
		public string GetAPI()
		{
			return this.m_API + this.m_requestParameter;
		}

		// Token: 0x060054A9 RID: 21673 RVA: 0x0017C2C7 File Offset: 0x0017A6C7
		public MeigewwwParam.eRequestMethod GetMethod()
		{
			return this.m_method;
		}

		// Token: 0x060054AA RID: 21674 RVA: 0x0017C2CF File Offset: 0x0017A6CF
		public string GetRequestJson()
		{
			return this.m_requestJson;
		}

		// Token: 0x060054AB RID: 21675 RVA: 0x0017C2D7 File Offset: 0x0017A6D7
		public void SetResponseJson(string json)
		{
			this.m_responseJson = json;
		}

		// Token: 0x060054AC RID: 21676 RVA: 0x0017C2E0 File Offset: 0x0017A6E0
		public string GetResponseJson()
		{
			return this.m_responseJson;
		}

		// Token: 0x060054AD RID: 21677 RVA: 0x0017C2E8 File Offset: 0x0017A6E8
		public bool IsSystemError()
		{
			return this.m_systemError != MeigewwwParam.eSystemError.None;
		}

		// Token: 0x060054AE RID: 21678 RVA: 0x0017C2F6 File Offset: 0x0017A6F6
		public MeigewwwParam.eSystemError GetSystemError()
		{
			return this.m_systemError;
		}

		// Token: 0x060054AF RID: 21679 RVA: 0x0017C2FE File Offset: 0x0017A6FE
		public void SetSystemError(MeigewwwParam.eSystemError systemError)
		{
			this.m_systemError = systemError;
		}

		// Token: 0x060054B0 RID: 21680 RVA: 0x0017C307 File Offset: 0x0017A707
		public void SetError(string json)
		{
			this.m_responseError = json;
		}

		// Token: 0x060054B1 RID: 21681 RVA: 0x0017C310 File Offset: 0x0017A710
		public string GetError()
		{
			return this.m_responseError;
		}

		// Token: 0x060054B2 RID: 21682 RVA: 0x0017C318 File Offset: 0x0017A718
		public void SetMaskFlag(int maskFlag)
		{
			this.m_maskFlag = maskFlag;
		}

		// Token: 0x060054B3 RID: 21683 RVA: 0x0017C321 File Offset: 0x0017A721
		public int GetMaskFlag()
		{
			return this.m_maskFlag;
		}

		// Token: 0x060054B4 RID: 21684 RVA: 0x0017C329 File Offset: 0x0017A729
		public void SetRetryCount(int retry)
		{
			this.m_retryCount = ((retry < 0) ? 0 : retry);
		}

		// Token: 0x060054B5 RID: 21685 RVA: 0x0017C33F File Offset: 0x0017A73F
		public int GetRetryCount()
		{
			return this.m_retryCount;
		}

		// Token: 0x060054B6 RID: 21686 RVA: 0x0017C347 File Offset: 0x0017A747
		public void SetPostDataType(int postDataType)
		{
			this.m_postDataType = postDataType;
		}

		// Token: 0x060054B7 RID: 21687 RVA: 0x0017C350 File Offset: 0x0017A750
		public bool IsPostCompress()
		{
			return (this.m_postDataType & 256) != 0;
		}

		// Token: 0x060054B8 RID: 21688 RVA: 0x0017C364 File Offset: 0x0017A764
		public bool IsPostCrypt()
		{
			return (this.m_postDataType & 1) != 0;
		}

		// Token: 0x060054B9 RID: 21689 RVA: 0x0017C374 File Offset: 0x0017A774
		public bool IsResponseCompress()
		{
			return (this.m_postDataType & 4096) != 0;
		}

		// Token: 0x060054BA RID: 21690 RVA: 0x0017C388 File Offset: 0x0017A788
		public bool IsResponseCrypt()
		{
			return (this.m_postDataType & 16) != 0;
		}

		// Token: 0x060054BB RID: 21691 RVA: 0x0017C39C File Offset: 0x0017A79C
		public void Clear()
		{
			this.m_API = string.Empty;
			this.m_requestParameter = string.Empty;
			this.m_method = MeigewwwParam.eRequestMethod.None;
			this.m_requestJson = string.Empty;
			this.m_responseJson = string.Empty;
			this.m_responseError = null;
			this.onRecivedCallback = null;
			this.m_postDataType = 0;
			this.m_maskFlag = 0;
			this.m_retryCount = 0;
			this.m_systemError = MeigewwwParam.eSystemError.None;
			this.m_parameterCount = 0;
			this.m_parameters = new MeigewwwParam.Parameter[100];
		}

		// Token: 0x060054BC RID: 21692 RVA: 0x0017C41A File Offset: 0x0017A81A
		public void Init(string api, MeigewwwParam.eRequestMethod method, MeigewwwParam.Callback callback = null)
		{
			this.m_API = api;
			this.m_method = method;
			this.onRecivedCallback = callback;
		}

		// Token: 0x060054BD RID: 21693 RVA: 0x0017C431 File Offset: 0x0017A831
		public void Setup()
		{
			if (this.m_method == MeigewwwParam.eRequestMethod.Get)
			{
				this.m_requestParameter = this.ParameterToHttpParam();
			}
			else if (this.m_method == MeigewwwParam.eRequestMethod.Post)
			{
				this.m_requestJson = this.ParameterToJson();
			}
		}

		// Token: 0x060054BE RID: 21694 RVA: 0x0017C468 File Offset: 0x0017A868
		public void OnRecived()
		{
			NetworkQueueManager.Done(this);
			if (this.onRecivedCallback != null)
			{
				this.onRecivedCallback(this);
			}
		}

		// Token: 0x060054BF RID: 21695 RVA: 0x0017C488 File Offset: 0x0017A888
		private string ParameterToHttpParam()
		{
			if (this.m_parameterCount <= 0)
			{
				return string.Empty;
			}
			string text = "?";
			for (int i = 0; i < this.m_parameterCount; i++)
			{
				if (i != 0)
				{
					text += "&";
				}
				text += this.m_parameters[i].paramName;
				text += "=";
				string text2 = this.m_parameters[i].value.ToString();
				text2 = NetworkQueueManager.escapeSpecialChars(text2);
				text += text2;
			}
			return text;
		}

		// Token: 0x060054C0 RID: 21696 RVA: 0x0017C524 File Offset: 0x0017A924
		private string ParameterToJson()
		{
			string str = "{";
			for (int i = 0; i < this.m_parameterCount; i++)
			{
				if (i != 0)
				{
					str += ",";
				}
				str = str + "\"" + this.m_parameters[i].paramName + "\"";
				str += ":";
				string str2 = JsonConvert.SerializeObject(this.m_parameters[i].value);
				str += str2;
			}
			return str + "}";
		}

		// Token: 0x060054C1 RID: 21697 RVA: 0x0017C5BC File Offset: 0x0017A9BC
		public void Add(string paramName, object value)
		{
			if (100 <= this.m_parameterCount)
			{
				throw new Exception("MeigewwwParam.Add Array outside the reference. [MeigeDefs.WWW_MAX_REQUEST_PARAM <= " + this.m_parameterCount + "]");
			}
			this.m_parameters[this.m_parameterCount].paramName = paramName;
			this.m_parameters[this.m_parameterCount].value = value;
			this.m_parameterCount++;
		}

		// Token: 0x060054C2 RID: 21698 RVA: 0x0017C632 File Offset: 0x0017AA32
		public int GetParameterNum()
		{
			return this.m_parameterCount;
		}

		// Token: 0x060054C3 RID: 21699 RVA: 0x0017C63A File Offset: 0x0017AA3A
		public void GetParameter(out MeigewwwParam.Parameter outparam, int index)
		{
			outparam = this.m_parameters[index];
		}

		// Token: 0x040055AF RID: 21935
		private string m_API;

		// Token: 0x040055B0 RID: 21936
		private string m_requestParameter;

		// Token: 0x040055B1 RID: 21937
		private MeigewwwParam.eRequestMethod m_method;

		// Token: 0x040055B2 RID: 21938
		private string m_requestJson;

		// Token: 0x040055B3 RID: 21939
		private MeigewwwParam.Callback onRecivedCallback;

		// Token: 0x040055B4 RID: 21940
		private int m_postDataType;

		// Token: 0x040055B5 RID: 21941
		private int m_retryCount;

		// Token: 0x040055B6 RID: 21942
		private MeigewwwParam.eStatus m_status;

		// Token: 0x040055B7 RID: 21943
		private string m_responseJson;

		// Token: 0x040055B8 RID: 21944
		private string m_responseError;

		// Token: 0x040055B9 RID: 21945
		private MeigewwwParam.eSystemError m_systemError;

		// Token: 0x040055BA RID: 21946
		private int m_maskFlag;

		// Token: 0x040055BB RID: 21947
		private int m_parameterCount;

		// Token: 0x040055BC RID: 21948
		private MeigewwwParam.Parameter[] m_parameters;

		// Token: 0x02000FE0 RID: 4064
		public enum eStatus
		{
			// Token: 0x040055BE RID: 21950
			Wait,
			// Token: 0x040055BF RID: 21951
			Progress,
			// Token: 0x040055C0 RID: 21952
			Done
		}

		// Token: 0x02000FE1 RID: 4065
		public enum eSystemError
		{
			// Token: 0x040055C2 RID: 21954
			None,
			// Token: 0x040055C3 RID: 21955
			Timeout,
			// Token: 0x040055C4 RID: 21956
			Unknown
		}

		// Token: 0x02000FE2 RID: 4066
		public enum eRequestMethod
		{
			// Token: 0x040055C6 RID: 21958
			None,
			// Token: 0x040055C7 RID: 21959
			Get,
			// Token: 0x040055C8 RID: 21960
			Post
		}

		// Token: 0x02000FE3 RID: 4067
		public struct Parameter
		{
			// Token: 0x040055C9 RID: 21961
			public string paramName;

			// Token: 0x040055CA RID: 21962
			public object value;
		}

		// Token: 0x02000FE4 RID: 4068
		// (Invoke) Token: 0x060054C5 RID: 21701
		public delegate void Callback(MeigewwwParam wwwParam);
	}
}
