﻿using System;

namespace Meige
{
	// Token: 0x02000F8B RID: 3979
	public enum eBlendFactor
	{
		// Token: 0x04005342 RID: 21314
		Zero,
		// Token: 0x04005343 RID: 21315
		One,
		// Token: 0x04005344 RID: 21316
		DstColor,
		// Token: 0x04005345 RID: 21317
		SrcColor,
		// Token: 0x04005346 RID: 21318
		OneMinusDstColor,
		// Token: 0x04005347 RID: 21319
		SrcAlpha,
		// Token: 0x04005348 RID: 21320
		OneMinusSrcColor,
		// Token: 0x04005349 RID: 21321
		DstAlpha,
		// Token: 0x0400534A RID: 21322
		OneMinusDstAlpha,
		// Token: 0x0400534B RID: 21323
		OneMinusSrcAlpha = 10
	}
}
