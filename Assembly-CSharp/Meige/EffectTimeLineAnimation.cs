﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EF0 RID: 3824
	[RequireComponent(typeof(Animation))]
	[DisallowMultipleComponent]
	public class EffectTimeLineAnimation : MonoBehaviour
	{
		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06004ECE RID: 20174 RVA: 0x0015D078 File Offset: 0x0015B478
		public Animation animation
		{
			get
			{
				return this.m_Animation;
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06004ECF RID: 20175 RVA: 0x0015D080 File Offset: 0x0015B480
		// (set) Token: 0x06004ED0 RID: 20176 RVA: 0x0015D088 File Offset: 0x0015B488
		public float animationSpeedScale
		{
			get
			{
				return this.m_AnimationSpeedScale;
			}
			set
			{
				this.m_AnimationSpeedScale = value;
			}
		}

		// Token: 0x06004ED1 RID: 20177 RVA: 0x0015D094 File Offset: 0x0015B494
		private void Awake()
		{
			this.m_Animation = base.gameObject.GetComponent<Animation>();
			this.m_Animation.Stop();
			this.m_Animation.playAutomatically = false;
			this.m_Animation.wrapMode = WrapMode.Once;
			this.m_AnimationTime = 0f;
		}

		// Token: 0x06004ED2 RID: 20178 RVA: 0x0015D0E0 File Offset: 0x0015B4E0
		public float GetTimeMax()
		{
			float num = 0f;
			if (this.m_CurveList != null)
			{
				for (int i = 0; i < this.m_CurveList.Length; i++)
				{
					for (int j = 0; j < this.m_CurveList[i].m_SourceCurve.m_KeyFrameArray.Length; j++)
					{
						if (num < this.m_CurveList[i].m_SourceCurve.m_KeyFrameArray[j].m_Time)
						{
							num = this.m_CurveList[i].m_SourceCurve.m_KeyFrameArray[j].m_Time;
						}
					}
				}
			}
			if (num < this.m_Animation.clip.length)
			{
				num = this.m_Animation.clip.length;
			}
			return num;
		}

		// Token: 0x06004ED3 RID: 20179 RVA: 0x0015D1A5 File Offset: 0x0015B5A5
		public void SetTimeNow(float value)
		{
			this.m_AnimationTime = value;
		}

		// Token: 0x06004ED4 RID: 20180 RVA: 0x0015D1AE File Offset: 0x0015B5AE
		public float GetTimeNow()
		{
			return this.m_AnimationTime;
		}

		// Token: 0x06004ED5 RID: 20181 RVA: 0x0015D1B6 File Offset: 0x0015B5B6
		public bool IsEndFrame()
		{
			return this.m_AnimationTime >= this.GetTimeMax();
		}

		// Token: 0x06004ED6 RID: 20182 RVA: 0x0015D1C9 File Offset: 0x0015B5C9
		public bool IsPlaying()
		{
			return this.m_isPlaying;
		}

		// Token: 0x06004ED7 RID: 20183 RVA: 0x0015D1D4 File Offset: 0x0015B5D4
		public bool Play()
		{
			if (this.IsPlaying())
			{
				return false;
			}
			IEnumerator enumerator = this.m_Animation.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					animationState.time = 0f;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.m_isPlaying = this.m_Animation.Play();
			this.m_isFirstFrame = true;
			return this.m_isPlaying;
		}

		// Token: 0x06004ED8 RID: 20184 RVA: 0x0015D268 File Offset: 0x0015B668
		public bool Stop()
		{
			IEnumerator enumerator = this.m_Animation.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					animationState.time = 0f;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.m_Animation.Stop();
			this.m_isPlaying = false;
			return true;
		}

		// Token: 0x06004ED9 RID: 20185 RVA: 0x0015D2E4 File Offset: 0x0015B6E4
		private void Update()
		{
			if (this.m_Animation == null)
			{
				return;
			}
			if (!this.IsPlaying())
			{
				return;
			}
			if (this.m_CurveList != null)
			{
				for (int i = 0; i < this.m_CurveList.Length; i++)
				{
					EffectTimeLineCurve effectTimeLineCurve = this.m_CurveList[i];
					effectTimeLineCurve.CalcValue(this.m_AnimationTime);
				}
			}
			AnimationState animationState = this.m_Animation[this.m_Animation.clip.name];
			animationState.time = Mathf.Clamp(this.m_AnimationTime, 0f, animationState.length);
			animationState.speed = this.animationSpeedScale;
			this.m_Animation.playAutomatically = false;
			if (!this.m_isFirstFrame)
			{
				this.m_AnimationTime += Time.deltaTime * this.animationSpeedScale;
			}
			this.m_isFirstFrame = false;
			if (this.m_AnimationTime > this.GetTimeMax())
			{
				this.m_AnimationTime = this.GetTimeMax();
				this.m_isPlaying = this.m_Animation.isPlaying;
			}
		}

		// Token: 0x04004E89 RID: 20105
		[SerializeField]
		private EffectTimeLineCurve[] m_CurveList;

		// Token: 0x04004E8A RID: 20106
		private Animation m_Animation;

		// Token: 0x04004E8B RID: 20107
		private float m_AnimationTime;

		// Token: 0x04004E8C RID: 20108
		private float m_AnimationSpeedScale = 1f;

		// Token: 0x04004E8D RID: 20109
		private bool m_isPlaying;

		// Token: 0x04004E8E RID: 20110
		private bool m_isFirstFrame;
	}
}
