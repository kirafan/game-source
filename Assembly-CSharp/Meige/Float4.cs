﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F0C RID: 3852
	[Serializable]
	public struct Float4 : IComparable, IComparable<Float4>, IOperatorFast<Float4>
	{
		// Token: 0x06004FF5 RID: 20469 RVA: 0x0015F238 File Offset: 0x0015D638
		public Float4(Float4 value)
		{
			this.m_Value = value.m_Value;
		}

		// Token: 0x06004FF6 RID: 20470 RVA: 0x0015F247 File Offset: 0x0015D647
		public Float4(Vector4 value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004FF7 RID: 20471 RVA: 0x0015F250 File Offset: 0x0015D650
		public Float4(float x, float y, float z, float w)
		{
			this.m_Value.x = x;
			this.m_Value.y = y;
			this.m_Value.z = z;
			this.m_Value.w = w;
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06004FF9 RID: 20473 RVA: 0x0015F28C File Offset: 0x0015D68C
		// (set) Token: 0x06004FF8 RID: 20472 RVA: 0x0015F283 File Offset: 0x0015D683
		public Vector4 value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06004FFB RID: 20475 RVA: 0x0015F2A2 File Offset: 0x0015D6A2
		// (set) Token: 0x06004FFA RID: 20474 RVA: 0x0015F294 File Offset: 0x0015D694
		public float x
		{
			get
			{
				return this.m_Value.x;
			}
			set
			{
				this.m_Value.x = value;
			}
		}

		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06004FFD RID: 20477 RVA: 0x0015F2BD File Offset: 0x0015D6BD
		// (set) Token: 0x06004FFC RID: 20476 RVA: 0x0015F2AF File Offset: 0x0015D6AF
		public float y
		{
			get
			{
				return this.m_Value.y;
			}
			set
			{
				this.m_Value.y = value;
			}
		}

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x06004FFF RID: 20479 RVA: 0x0015F2D8 File Offset: 0x0015D6D8
		// (set) Token: 0x06004FFE RID: 20478 RVA: 0x0015F2CA File Offset: 0x0015D6CA
		public float z
		{
			get
			{
				return this.m_Value.z;
			}
			set
			{
				this.m_Value.z = value;
			}
		}

		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x06005001 RID: 20481 RVA: 0x0015F2F3 File Offset: 0x0015D6F3
		// (set) Token: 0x06005000 RID: 20480 RVA: 0x0015F2E5 File Offset: 0x0015D6E5
		public float w
		{
			get
			{
				return this.m_Value.w;
			}
			set
			{
				this.m_Value.w = value;
			}
		}

		// Token: 0x17000552 RID: 1362
		public float this[int idx]
		{
			get
			{
				return this.m_Value[idx];
			}
			set
			{
				this.m_Value[idx] = value;
			}
		}

		// Token: 0x06005004 RID: 20484 RVA: 0x0015F31D File Offset: 0x0015D71D
		public static implicit operator Float4(Vector4 v)
		{
			return new Float4(v);
		}

		// Token: 0x06005005 RID: 20485 RVA: 0x0015F325 File Offset: 0x0015D725
		public static implicit operator Vector4(Float4 v)
		{
			return v.value;
		}

		// Token: 0x06005006 RID: 20486 RVA: 0x0015F32E File Offset: 0x0015D72E
		public void Add(Float4 a, Float4 b)
		{
			this = a + b;
		}

		// Token: 0x06005007 RID: 20487 RVA: 0x0015F33D File Offset: 0x0015D73D
		public void Sub(Float4 a, Float4 b)
		{
			this = a - b;
		}

		// Token: 0x06005008 RID: 20488 RVA: 0x0015F34C File Offset: 0x0015D74C
		public void Mul(Float4 a, Float4 b)
		{
			this = a * b;
		}

		// Token: 0x06005009 RID: 20489 RVA: 0x0015F35B File Offset: 0x0015D75B
		public void Mul(Float4 a, float b)
		{
			this = a * b;
		}

		// Token: 0x0600500A RID: 20490 RVA: 0x0015F36A File Offset: 0x0015D76A
		public void Div(Float4 a, Float4 b)
		{
			this = a / b;
		}

		// Token: 0x0600500B RID: 20491 RVA: 0x0015F379 File Offset: 0x0015D779
		public void Add(Float4 b)
		{
			this += b;
		}

		// Token: 0x0600500C RID: 20492 RVA: 0x0015F38D File Offset: 0x0015D78D
		public void Sub(Float4 b)
		{
			this -= b;
		}

		// Token: 0x0600500D RID: 20493 RVA: 0x0015F3A1 File Offset: 0x0015D7A1
		public void Mul(Float4 b)
		{
			this *= b;
		}

		// Token: 0x0600500E RID: 20494 RVA: 0x0015F3B5 File Offset: 0x0015D7B5
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x0600500F RID: 20495 RVA: 0x0015F3C9 File Offset: 0x0015D7C9
		public void Div(Float4 b)
		{
			this *= b;
		}

		// Token: 0x06005010 RID: 20496 RVA: 0x0015F3DD File Offset: 0x0015D7DD
		public void SetRowValue(object o)
		{
			this.m_Value = (Vector4)o;
		}

		// Token: 0x06005011 RID: 20497 RVA: 0x0015F3EB File Offset: 0x0015D7EB
		public void SetRowValue(float o, int componentIdx)
		{
			this.m_Value[componentIdx] = o;
		}

		// Token: 0x06005012 RID: 20498 RVA: 0x0015F3FC File Offset: 0x0015D7FC
		public int CompareTo(object obj)
		{
			Float4? @float = obj as Float4?;
			if (@float == null)
			{
				return 1;
			}
			return this.CompareTo(@float);
		}

		// Token: 0x06005013 RID: 20499 RVA: 0x0015F434 File Offset: 0x0015D834
		public int CompareTo(Float4 target)
		{
			if (target.m_Value.x == this.x && target.m_Value.y == this.y && target.m_Value.z == this.z && target.m_Value.w == this.w)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06005014 RID: 20500 RVA: 0x0015F4A0 File Offset: 0x0015D8A0
		public override bool Equals(object obj)
		{
			Float4? @float = obj as Float4?;
			return @float != null && this.Equals(@float);
		}

		// Token: 0x06005015 RID: 20501 RVA: 0x0015F4DC File Offset: 0x0015D8DC
		public bool Equals(Float4 obj)
		{
			return this == obj;
		}

		// Token: 0x06005016 RID: 20502 RVA: 0x0015F4EA File Offset: 0x0015D8EA
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06005017 RID: 20503 RVA: 0x0015F4FD File Offset: 0x0015D8FD
		public static Float4 operator +(Float4 v0)
		{
			return v0;
		}

		// Token: 0x06005018 RID: 20504 RVA: 0x0015F500 File Offset: 0x0015D900
		public static Float4 operator +(Float4 v0, Float4 v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x + v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y + v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z + v1.m_Value.z;
			result.m_Value.w = v0.m_Value.w + v1.m_Value.w;
			return result;
		}

		// Token: 0x06005019 RID: 20505 RVA: 0x0015F5AC File Offset: 0x0015D9AC
		public static Float4 operator -(Float4 v0)
		{
			Float4 result = default(Float4);
			result.m_Value.x = -v0.m_Value.x;
			result.m_Value.y = -v0.m_Value.y;
			result.m_Value.z = -v0.m_Value.z;
			result.m_Value.w = -v0.m_Value.w;
			return result;
		}

		// Token: 0x0600501A RID: 20506 RVA: 0x0015F628 File Offset: 0x0015DA28
		public static Float4 operator -(Float4 v0, Float4 v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x - v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y - v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z - v1.m_Value.z;
			result.m_Value.w = v0.m_Value.w - v1.m_Value.w;
			return result;
		}

		// Token: 0x0600501B RID: 20507 RVA: 0x0015F6D4 File Offset: 0x0015DAD4
		public static Float4 operator *(Float4 v0, Float4 v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x * v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y * v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z * v1.m_Value.z;
			result.m_Value.w = v0.m_Value.w * v1.m_Value.w;
			return result;
		}

		// Token: 0x0600501C RID: 20508 RVA: 0x0015F780 File Offset: 0x0015DB80
		public static Float4 operator *(Float4 v0, float v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x * v1;
			result.m_Value.y = v0.m_Value.y * v1;
			result.m_Value.z = v0.m_Value.z * v1;
			result.m_Value.w = v0.m_Value.w * v1;
			return result;
		}

		// Token: 0x0600501D RID: 20509 RVA: 0x0015F800 File Offset: 0x0015DC00
		public static Float4 operator /(Float4 v0, Float4 v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x / v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y / v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z / v1.m_Value.z;
			result.m_Value.w = v0.m_Value.w / v1.m_Value.w;
			return result;
		}

		// Token: 0x0600501E RID: 20510 RVA: 0x0015F8AC File Offset: 0x0015DCAC
		public static Float4 operator /(Float4 v0, float v1)
		{
			Float4 result = default(Float4);
			result.m_Value.x = v0.m_Value.x / v1;
			result.m_Value.y = v0.m_Value.y / v1;
			result.m_Value.z = v0.m_Value.z / v1;
			result.m_Value.w = v0.m_Value.w / v1;
			return result;
		}

		// Token: 0x0600501F RID: 20511 RVA: 0x0015F92C File Offset: 0x0015DD2C
		public static bool operator ==(Float4 v0, Float4 v1)
		{
			return v0.value == v1.value;
		}

		// Token: 0x06005020 RID: 20512 RVA: 0x0015F941 File Offset: 0x0015DD41
		public static bool operator !=(Float4 v0, Float4 v1)
		{
			return v0.value != v1.value;
		}

		// Token: 0x04004EB5 RID: 20149
		[SerializeField]
		public Vector4 m_Value;
	}
}
