﻿using System;

namespace Meige
{
	// Token: 0x02000F69 RID: 3945
	public enum eFieldSpaceType
	{
		// Token: 0x04005214 RID: 21012
		eFieldSpaceType_3D,
		// Token: 0x04005215 RID: 21013
		eFieldSpaceType_2D
	}
}
