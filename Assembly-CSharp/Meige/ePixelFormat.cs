﻿using System;

namespace Meige
{
	// Token: 0x02000F97 RID: 3991
	public enum ePixelFormat
	{
		// Token: 0x040053CA RID: 21450
		ePixelFormat_Invalid = -1,
		// Token: 0x040053CB RID: 21451
		ePixelFormat_R8G8B8A8,
		// Token: 0x040053CC RID: 21452
		ePixelFormat_R10G10B10A2,
		// Token: 0x040053CD RID: 21453
		ePixelFormat_R4G4B4A4,
		// Token: 0x040053CE RID: 21454
		ePixelFormat_R5G5B5A1,
		// Token: 0x040053CF RID: 21455
		ePixelFormat_L8,
		// Token: 0x040053D0 RID: 21456
		ePixelFormat_A8,
		// Token: 0x040053D1 RID: 21457
		ePixelFormat_P4,
		// Token: 0x040053D2 RID: 21458
		ePixelFormat_P8,
		// Token: 0x040053D3 RID: 21459
		ePixelFormat_DXT1,
		// Token: 0x040053D4 RID: 21460
		ePixelFormat_DXT3,
		// Token: 0x040053D5 RID: 21461
		ePixelFormat_DXT5,
		// Token: 0x040053D6 RID: 21462
		ePixelFormat_F32,
		// Token: 0x040053D7 RID: 21463
		ePixelFormat_S8D24,
		// Token: 0x040053D8 RID: 21464
		ePixelFormat_D32,
		// Token: 0x040053D9 RID: 21465
		ePixelFormat_YUV,
		// Token: 0x040053DA RID: 21466
		ePixelFormat_R16G16B16A16,
		// Token: 0x040053DB RID: 21467
		ePixelFormat_R8G8B8,
		// Token: 0x040053DC RID: 21468
		ePixelFormat_D16,
		// Token: 0x040053DD RID: 21469
		ePixelFormat_R5G6B5,
		// Token: 0x040053DE RID: 21470
		ePixelFormat_R32G32,
		// Token: 0x040053DF RID: 21471
		ePixelFormat_R16G16,
		// Token: 0x040053E0 RID: 21472
		ePixelForma_Cnt
	}
}
