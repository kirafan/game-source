﻿using System;

namespace Meige
{
	// Token: 0x02000EA2 RID: 3746
	public interface EffectRuleParamBase
	{
		// Token: 0x06004E42 RID: 20034
		eEffectAnimTypeCode GetTypeCode();

		// Token: 0x06004E43 RID: 20035
		Type GetOrType();

		// Token: 0x06004E44 RID: 20036
		object GetValue(int tgtIdx);

		// Token: 0x06004E45 RID: 20037
		void SetValue(int tgtIdx, object value);

		// Token: 0x06004E46 RID: 20038
		object GetValue(int arrayIdx, int tgtIdx);

		// Token: 0x06004E47 RID: 20039
		void SetValue(int arrayIdx, int tgtIdx, object value);

		// Token: 0x06004E48 RID: 20040
		bool IsArray();

		// Token: 0x06004E49 RID: 20041
		int GetArraySize();

		// Token: 0x06004E4A RID: 20042
		bool ChangeArraySize(int size);
	}
}
