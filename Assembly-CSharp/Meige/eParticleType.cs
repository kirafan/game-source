﻿using System;

namespace Meige
{
	// Token: 0x02000F80 RID: 3968
	public enum eParticleType
	{
		// Token: 0x040052F1 RID: 21233
		eParticleType_Invalid = -1,
		// Token: 0x040052F2 RID: 21234
		eParticleType_Billboard,
		// Token: 0x040052F3 RID: 21235
		eParticleType_Point,
		// Token: 0x040052F4 RID: 21236
		eParticleType_Line,
		// Token: 0x040052F5 RID: 21237
		eParticleType_PolyLine,
		// Token: 0x040052F6 RID: 21238
		eParticleType_Confetti,
		// Token: 0x040052F7 RID: 21239
		eParticleType_Ribbon
	}
}
