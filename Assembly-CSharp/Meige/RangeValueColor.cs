﻿using System;

namespace Meige
{
	// Token: 0x02000EFF RID: 3839
	[Serializable]
	public class RangeValueColor : RangeValue<RGBA>
	{
		// Token: 0x06004F26 RID: 20262 RVA: 0x0015DD79 File Offset: 0x0015C179
		public RangeValueColor()
		{
		}

		// Token: 0x06004F27 RID: 20263 RVA: 0x0015DD81 File Offset: 0x0015C181
		public RangeValueColor(int num) : base(num)
		{
		}

		// Token: 0x06004F28 RID: 20264 RVA: 0x0015DD8A File Offset: 0x0015C18A
		public RangeValueColor(RangeValueColor v) : base(v)
		{
		}

		// Token: 0x06004F29 RID: 20265 RVA: 0x0015DD94 File Offset: 0x0015C194
		public new RangeValueColor Clone()
		{
			return new RangeValueColor(this);
		}
	}
}
