﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Meige
{
	// Token: 0x02000F56 RID: 3926
	public class MLD_Writer
	{
		// Token: 0x060051A6 RID: 20902 RVA: 0x0016D919 File Offset: 0x0016BD19
		public void AddName(string name)
		{
			if (!this.m_NameDB.Contains(name))
			{
				this.m_NameDB.Add(name);
			}
		}

		// Token: 0x060051A7 RID: 20903 RVA: 0x0016D938 File Offset: 0x0016BD38
		public int MakeNameOffsDB(int bodyOffs)
		{
			this.m_NameOffsDB = new List<int>();
			int num = 0;
			for (int i = 0; i < this.m_NameDB.Count; i++)
			{
				this.m_NameOffsDB.Add(bodyOffs + num);
				num += this.m_NameDB[i].Length + 1;
			}
			return num;
		}

		// Token: 0x060051A8 RID: 20904 RVA: 0x0016D994 File Offset: 0x0016BD94
		public int GetOffs(string name)
		{
			int index = this.m_NameDB.FindIndex(new Predicate<string>(name.Equals));
			return this.m_NameOffsDB[index];
		}

		// Token: 0x040050B3 RID: 20659
		public BinaryWriter m_File;

		// Token: 0x040050B4 RID: 20660
		public List<string> m_NameDB;

		// Token: 0x040050B5 RID: 20661
		public List<int> m_NameOffsDB;
	}
}
