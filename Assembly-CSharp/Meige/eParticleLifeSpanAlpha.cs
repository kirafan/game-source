﻿using System;

namespace Meige
{
	// Token: 0x02000F85 RID: 3973
	public enum eParticleLifeSpanAlpha : byte
	{
		// Token: 0x0400530E RID: 21262
		eParticleLifeSpanAlpha_None,
		// Token: 0x0400530F RID: 21263
		eParticleLifeSpanAlpha_FadeInOut,
		// Token: 0x04005310 RID: 21264
		eParticleLifeSpanAlpha_FadeIn,
		// Token: 0x04005311 RID: 21265
		eParticleLifeSpanAlpha_FadeOut
	}
}
