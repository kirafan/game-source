﻿using System;

namespace Meige
{
	// Token: 0x02000F05 RID: 3845
	[Serializable]
	public class RangePareColor : RangePare<RGBA>
	{
		// Token: 0x06004F3E RID: 20286 RVA: 0x0015DE99 File Offset: 0x0015C299
		public RangePareColor()
		{
		}

		// Token: 0x06004F3F RID: 20287 RVA: 0x0015DEA1 File Offset: 0x0015C2A1
		public RangePareColor(RGBA min, RGBA max) : base(min, max)
		{
		}

		// Token: 0x06004F40 RID: 20288 RVA: 0x0015DEAB File Offset: 0x0015C2AB
		public RangePareColor(RangePareColor v) : base(v)
		{
		}

		// Token: 0x06004F41 RID: 20289 RVA: 0x0015DEB4 File Offset: 0x0015C2B4
		public new RangePareColor Clone()
		{
			return new RangePareColor(this);
		}
	}
}
