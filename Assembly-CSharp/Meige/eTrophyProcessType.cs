﻿using System;

namespace Meige
{
	// Token: 0x02000FED RID: 4077
	public enum eTrophyProcessType
	{
		// Token: 0x040055E5 RID: 21989
		Unlock,
		// Token: 0x040055E6 RID: 21990
		DetailsGroup,
		// Token: 0x040055E7 RID: 21991
		DetailsAll,
		// Token: 0x040055E8 RID: 21992
		UnlockedFlg
	}
}
