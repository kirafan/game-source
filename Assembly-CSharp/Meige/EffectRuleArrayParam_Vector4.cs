﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000ED3 RID: 3795
	[Serializable]
	public class EffectRuleArrayParam_Vector4 : EffectRuleArrayParam<Vector4>
	{
	}
}
