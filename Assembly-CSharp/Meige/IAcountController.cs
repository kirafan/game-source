﻿using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FF1 RID: 4081
	public class IAcountController : MonoBehaviour
	{
		// Token: 0x060054F7 RID: 21751 RVA: 0x0017CF32 File Offset: 0x0017B332
		public static void Create()
		{
			CGlobalInstance.CreateInstance();
			IAcountController.ms_Inst = CGlobalInstance.EntryClass<IAcountController>();
			IAcountController.ms_Inst.m_State = IAcountController.eState.Invalid;
		}

		// Token: 0x060054F8 RID: 21752 RVA: 0x0017CF50 File Offset: 0x0017B350
		private void Start()
		{
			Debug.Log(string.Format("Start Google PlayAcount", new object[0]));
			PlayGamesClientConfiguration configuration = new PlayGamesClientConfiguration.Builder().Build();
			PlayGamesPlatform.InitializeInstance(configuration);
			PlayGamesPlatform.DebugLogEnabled = true;
			PlayGamesPlatform.Activate();
			if (!Social.localUser.authenticated)
			{
				Debug.Log(string.Format("PlatformAccount LogIn Start", new object[0]));
				Social.localUser.Authenticate(new Action<bool, string>(this.CallbackAcount));
			}
			else
			{
				this.m_UserName = Social.localUser.userName;
				this.m_UserID = Social.localUser.id;
				this.m_State = IAcountController.eState.Online;
				Debug.Log(string.Format("PlatformAccount : {0} : {1}", this.m_UserName, this.m_UserID));
			}
		}

		// Token: 0x060054F9 RID: 21753 RVA: 0x0017D010 File Offset: 0x0017B410
		private void CallbackAcount(bool fsuccess, string str)
		{
			if (fsuccess)
			{
				this.m_UserName = Social.localUser.userName;
				this.m_UserID = Social.localUser.id;
				Debug.Log(string.Format("PlatformAccount : {0} : {1} : {2}", this.m_UserName, this.m_UserID, str));
				this.m_State = IAcountController.eState.Online;
			}
			else
			{
				Debug.LogFormat("PlatformAccount Non : {0}", new object[]
				{
					str
				});
				this.m_State = IAcountController.eState.SignOut;
			}
		}

		// Token: 0x060054FA RID: 21754 RVA: 0x0017D088 File Offset: 0x0017B488
		public static void ResetCallback()
		{
			if (!Social.localUser.authenticated && IAcountController.ms_Inst != null)
			{
				Debug.Log(string.Format("PlatformAccount LogIn Start", new object[0]));
				Social.localUser.Authenticate(new Action<bool, string>(IAcountController.ms_Inst.CallbackAcount));
			}
		}

		// Token: 0x060054FB RID: 21755 RVA: 0x0017D0E3 File Offset: 0x0017B4E3
		public static IAcountController.eState GetState()
		{
			return IAcountController.ms_Inst.m_State;
		}

		// Token: 0x060054FC RID: 21756 RVA: 0x0017D0EF File Offset: 0x0017B4EF
		public static string GetUserName()
		{
			return IAcountController.ms_Inst.m_UserName;
		}

		// Token: 0x060054FD RID: 21757 RVA: 0x0017D0FB File Offset: 0x0017B4FB
		public static string GetUserID()
		{
			return IAcountController.ms_Inst.m_UserID;
		}

		// Token: 0x060054FE RID: 21758 RVA: 0x0017D107 File Offset: 0x0017B507
		public static bool IsSetup()
		{
			return IAcountController.ms_Inst != null;
		}

		// Token: 0x040055F2 RID: 22002
		private static IAcountController ms_Inst;

		// Token: 0x040055F3 RID: 22003
		private IAcountController.eState m_State;

		// Token: 0x040055F4 RID: 22004
		private string m_UserName;

		// Token: 0x040055F5 RID: 22005
		private string m_UserID;

		// Token: 0x02000FF2 RID: 4082
		public enum eState
		{
			// Token: 0x040055F7 RID: 22007
			Invalid = -1,
			// Token: 0x040055F8 RID: 22008
			SignOut,
			// Token: 0x040055F9 RID: 22009
			SignIn,
			// Token: 0x040055FA RID: 22010
			Online
		}
	}
}
