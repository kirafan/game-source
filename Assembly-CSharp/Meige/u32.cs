﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F08 RID: 3848
	[Serializable]
	public struct u32 : IComparable, IComparable<u32>, IOperatorFast<u32>
	{
		// Token: 0x06004F63 RID: 20323 RVA: 0x0015E1A3 File Offset: 0x0015C5A3
		public u32(uint value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004F64 RID: 20324 RVA: 0x0015E1AC File Offset: 0x0015C5AC
		public u32(u32 value)
		{
			this.m_Value = value.value;
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06004F66 RID: 20326 RVA: 0x0015E1C4 File Offset: 0x0015C5C4
		// (set) Token: 0x06004F65 RID: 20325 RVA: 0x0015E1BB File Offset: 0x0015C5BB
		public uint value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x06004F67 RID: 20327 RVA: 0x0015E1CC File Offset: 0x0015C5CC
		public static implicit operator u32(uint v)
		{
			return new u32(v);
		}

		// Token: 0x06004F68 RID: 20328 RVA: 0x0015E1D4 File Offset: 0x0015C5D4
		public static implicit operator uint(u32 v)
		{
			return v.value;
		}

		// Token: 0x06004F69 RID: 20329 RVA: 0x0015E1DD File Offset: 0x0015C5DD
		public void Add(u32 a, u32 b)
		{
			this = a + b;
		}

		// Token: 0x06004F6A RID: 20330 RVA: 0x0015E1EC File Offset: 0x0015C5EC
		public void Sub(u32 a, u32 b)
		{
			this = a - b;
		}

		// Token: 0x06004F6B RID: 20331 RVA: 0x0015E1FB File Offset: 0x0015C5FB
		public void Mul(u32 a, u32 b)
		{
			this = a * b;
		}

		// Token: 0x06004F6C RID: 20332 RVA: 0x0015E20A File Offset: 0x0015C60A
		public void Mul(u32 a, float b)
		{
			this = a * b;
		}

		// Token: 0x06004F6D RID: 20333 RVA: 0x0015E219 File Offset: 0x0015C619
		public void Div(u32 a, u32 b)
		{
			this = a / b;
		}

		// Token: 0x06004F6E RID: 20334 RVA: 0x0015E228 File Offset: 0x0015C628
		public void Add(u32 b)
		{
			this += b;
		}

		// Token: 0x06004F6F RID: 20335 RVA: 0x0015E23C File Offset: 0x0015C63C
		public void Sub(u32 b)
		{
			this -= b;
		}

		// Token: 0x06004F70 RID: 20336 RVA: 0x0015E250 File Offset: 0x0015C650
		public void Mul(u32 b)
		{
			this *= b;
		}

		// Token: 0x06004F71 RID: 20337 RVA: 0x0015E264 File Offset: 0x0015C664
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x06004F72 RID: 20338 RVA: 0x0015E278 File Offset: 0x0015C678
		public void Div(u32 b)
		{
			this *= b;
		}

		// Token: 0x06004F73 RID: 20339 RVA: 0x0015E28C File Offset: 0x0015C68C
		public void SetRowValue(object o)
		{
			this.m_Value = (uint)o;
		}

		// Token: 0x06004F74 RID: 20340 RVA: 0x0015E29C File Offset: 0x0015C69C
		public int CompareTo(object obj)
		{
			float? num = obj as float?;
			if (num == null)
			{
				return 1;
			}
			return this.CompareTo(num);
		}

		// Token: 0x06004F75 RID: 20341 RVA: 0x0015E2D2 File Offset: 0x0015C6D2
		public int CompareTo(u32 target)
		{
			if (target.value == this.m_Value)
			{
				return 0;
			}
			if (target.value < this.m_Value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06004F76 RID: 20342 RVA: 0x0015E300 File Offset: 0x0015C700
		public override bool Equals(object obj)
		{
			float? num = obj as float?;
			return num != null && this.Equals(num);
		}

		// Token: 0x06004F77 RID: 20343 RVA: 0x0015E33C File Offset: 0x0015C73C
		public bool Equals(float obj)
		{
			return this.m_Value == obj;
		}

		// Token: 0x06004F78 RID: 20344 RVA: 0x0015E349 File Offset: 0x0015C749
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06004F79 RID: 20345 RVA: 0x0015E35C File Offset: 0x0015C75C
		public static u32 operator +(u32 v0)
		{
			return v0;
		}

		// Token: 0x06004F7A RID: 20346 RVA: 0x0015E360 File Offset: 0x0015C760
		public static u32 operator +(u32 v0, u32 v1)
		{
			u32 result;
			result.m_Value = v0.m_Value + v1.m_Value;
			return result;
		}

		// Token: 0x06004F7B RID: 20347 RVA: 0x0015E384 File Offset: 0x0015C784
		public static u32 operator -(u32 v0)
		{
			u32 result;
			result.m_Value = (uint)(-(uint)((ulong)v0.m_Value));
			return result;
		}

		// Token: 0x06004F7C RID: 20348 RVA: 0x0015E3A4 File Offset: 0x0015C7A4
		public static u32 operator -(u32 v0, u32 v1)
		{
			u32 result;
			result.m_Value = v0.m_Value - v1.m_Value;
			return result;
		}

		// Token: 0x06004F7D RID: 20349 RVA: 0x0015E3C8 File Offset: 0x0015C7C8
		public static u32 operator *(u32 v0, u32 v1)
		{
			u32 result;
			result.m_Value = v0.m_Value * v1.m_Value;
			return result;
		}

		// Token: 0x06004F7E RID: 20350 RVA: 0x0015E3EC File Offset: 0x0015C7EC
		public static u32 operator *(u32 v0, float v1)
		{
			u32 result;
			result.m_Value = (uint)(v0.m_Value * v1);
			return result;
		}

		// Token: 0x06004F7F RID: 20351 RVA: 0x0015E410 File Offset: 0x0015C810
		public static u32 operator /(u32 v0, u32 v1)
		{
			u32 result;
			result.m_Value = v0.m_Value / v1.m_Value;
			return result;
		}

		// Token: 0x06004F80 RID: 20352 RVA: 0x0015E434 File Offset: 0x0015C834
		public static u32 operator /(u32 v0, float v1)
		{
			u32 result;
			result.m_Value = (uint)(v0.m_Value / v1);
			return result;
		}

		// Token: 0x06004F81 RID: 20353 RVA: 0x0015E455 File Offset: 0x0015C855
		public static bool operator ==(u32 v0, u32 v1)
		{
			return v0.m_Value == v1.m_Value;
		}

		// Token: 0x06004F82 RID: 20354 RVA: 0x0015E467 File Offset: 0x0015C867
		public static bool operator !=(u32 v0, u32 v1)
		{
			return v0.m_Value != v1.m_Value;
		}

		// Token: 0x04004EB1 RID: 20145
		[SerializeField]
		public uint m_Value;
	}
}
