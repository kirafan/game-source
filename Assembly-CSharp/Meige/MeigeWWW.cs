﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

namespace Meige
{
	// Token: 0x02000FDE RID: 4062
	public class MeigeWWW : IDisposable
	{
		// Token: 0x06005490 RID: 21648 RVA: 0x0017B1E8 File Offset: 0x001795E8
		public MeigeWWW(MeigewwwParam wwwParam)
		{
			this.m_www = null;
			this.m_param = wwwParam;
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x06005491 RID: 21649 RVA: 0x0017B1FE File Offset: 0x001795FE
		// (set) Token: 0x06005492 RID: 21650 RVA: 0x0017B205 File Offset: 0x00179605
		public static string SessionId
		{
			get
			{
				return MeigeWWW.m_sessionID;
			}
			set
			{
				MeigeWWW.m_sessionID = value;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x06005493 RID: 21651 RVA: 0x0017B20D File Offset: 0x0017960D
		// (set) Token: 0x06005494 RID: 21652 RVA: 0x0017B214 File Offset: 0x00179614
		public static string AppVersion
		{
			get
			{
				return MeigeWWW.m_appversion;
			}
			set
			{
				MeigeWWW.m_appversion = value;
			}
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x06005495 RID: 21653 RVA: 0x0017B21C File Offset: 0x0017961C
		// (set) Token: 0x06005496 RID: 21654 RVA: 0x0017B223 File Offset: 0x00179623
		public static bool IsRelease
		{
			get
			{
				return MeigeWWW.m_isRelease;
			}
			set
			{
				MeigeWWW.m_isRelease = value;
			}
		}

		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x06005497 RID: 21655 RVA: 0x0017B22B File Offset: 0x0017962B
		// (set) Token: 0x06005498 RID: 21656 RVA: 0x0017B232 File Offset: 0x00179632
		public static int TimeoutSec
		{
			get
			{
				return MeigeWWW.m_timeoutSec;
			}
			set
			{
				MeigeWWW.m_timeoutSec = value;
			}
		}

		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x06005499 RID: 21657 RVA: 0x0017B23A File Offset: 0x0017963A
		// (set) Token: 0x0600549A RID: 21658 RVA: 0x0017B241 File Offset: 0x00179641
		public static Dictionary<string, string> HttpHeaderCommon
		{
			get
			{
				return MeigeWWW.m_HttpHeaderCommon;
			}
			set
			{
				MeigeWWW.m_HttpHeaderCommon = value;
			}
		}

		// Token: 0x0600549B RID: 21659 RVA: 0x0017B24C File Offset: 0x0017964C
		public static void SetUp()
		{
			if (MeigeWWW.m_HttpHeaderCommon == null)
			{
				MeigeWWW.m_HttpHeaderCommon = new Dictionary<string, string>();
				string text = "app/" + MeigeWWW.m_appversion;
				text = text + "; " + SystemInfo.operatingSystem.Replace("(", string.Empty).Replace(")", string.Empty);
				text = text + "; " + SystemInfo.deviceModel.Replace("(", string.Empty).Replace(")", string.Empty);
				MeigeWWW.m_HttpHeaderCommon.Add("Unity-User-Agent", text);
				MeigeWWW.m_HttpHeaderCommon.Add("Content-Type", "application/json; charset=UTF-8");
			}
		}

		// Token: 0x0600549C RID: 21660 RVA: 0x0017B2FF File Offset: 0x001796FF
		public void Dispose()
		{
			if (this.m_www != null)
			{
				this.m_www.Dispose();
				this.m_www = null;
			}
			if (this.m_param != null)
			{
				this.m_param = null;
			}
		}

		// Token: 0x0600549D RID: 21661 RVA: 0x0017B330 File Offset: 0x00179730
		public IEnumerator Kick()
		{
			this.m_param.Setup();
			Dictionary<string, string> headers = new Dictionary<string, string>();
			string json = string.Empty;
			byte[] postData = null;
			if (this.m_param.GetMethod() == MeigewwwParam.eRequestMethod.Post)
			{
				json = this.m_param.GetRequestJson();
				postData = Encoding.UTF8.GetBytes(json);
				if (this.m_param.IsPostCompress())
				{
					IEnumerator coroutine = this.Compress(postData);
					yield return coroutine;
					postData = (byte[])coroutine.Current;
					headers.Add("Post-Compress", "on");
				}
				if (this.m_param.IsPostCrypt())
				{
					IEnumerator coroutine2 = this.Encrypt(postData);
					yield return coroutine2;
					postData = (byte[])coroutine2.Current;
					headers.Add("Post-Crypt", "on");
				}
			}
			if (this.m_param.IsResponseCompress())
			{
				headers.Add("Request-Compress", "on");
			}
			if (this.m_param.IsResponseCrypt())
			{
				headers.Add("Request-Crypt", "on");
			}
			string basestring = MeigeWWW.m_sessionID;
			if (basestring.Length > 0)
			{
				basestring += " ";
			}
			basestring = basestring + "/api/" + this.m_param.GetAPI();
			if (!string.IsNullOrEmpty(json))
			{
				basestring = basestring + " " + json;
			}
			basestring = basestring + " " + ((!MeigeWWW.m_isRelease) ? ProjDepend.REQUESTHASH_SECRET : ProjDepend.REQUESTHASH_SECRET_APPLYING);
			SHA256 crypto = new SHA256CryptoServiceProvider();
			byte[] requesthash = crypto.ComputeHash(Encoding.UTF8.GetBytes(basestring));
			crypto.Clear();
			StringBuilder requesthashText = new StringBuilder();
			foreach (byte b in requesthash)
			{
				requesthashText.Append(b.ToString("x2"));
			}
			headers[ProjDepend.WHERHK] = requesthashText.ToString();
			if (!string.IsNullOrEmpty(MeigeWWW.m_sessionID))
			{
				headers[ProjDepend.WHSIK] = MeigeWWW.m_sessionID;
			}
			foreach (string key in MeigeWWW.m_HttpHeaderCommon.Keys)
			{
				if (!headers.ContainsKey(key))
				{
					headers.Add(key, MeigeWWW.m_HttpHeaderCommon[key]);
				}
			}
			string server_url = ((!MeigeWWW.m_isRelease) ? ProjDepend.SERVER_URL : ProjDepend.SERVER_APPLYING_URL) + this.m_param.GetAPI();
			int tryCnt = this.m_param.GetRetryCount() + 1;
			for (;;)
			{
				long startTicks = DateTime.Now.Ticks;
				this.m_www = new UnityWebRequest(server_url, this.m_param.GetMethod().ToString());
				if (this.m_param.GetMethod() == MeigewwwParam.eRequestMethod.Post)
				{
					this.m_www.uploadHandler = new UploadHandlerRaw(postData);
				}
				this.m_www.downloadHandler = new DownloadHandlerBuffer();
				foreach (KeyValuePair<string, string> keyValuePair in headers)
				{
					this.m_www.SetRequestHeader(keyValuePair.Key, keyValuePair.Value);
				}
				this.m_www.timeout = MeigeWWW.TimeoutSec;
				yield return this.m_www.Send();
				if (this.m_www != null && this.m_www.isDone && string.IsNullOrEmpty(this.m_www.error))
				{
					break;
				}
				if (--tryCnt <= 0)
				{
					goto Block_23;
				}
				if (this.m_www != null)
				{
					this.m_www.Dispose();
					this.m_www = null;
				}
				yield return new WaitForSeconds(5f);
			}
			DownloadHandlerBuffer handler = (DownloadHandlerBuffer)this.m_www.downloadHandler;
			string headerValue = this.m_www.GetResponseHeader(ProjDepend.WHRPK);
			string text = handler.text;
			byte[] bytes = handler.data;
			if (this.m_param.IsResponseCrypt())
			{
				bytes = Convert.FromBase64String(text);
				IEnumerator coroutine3 = this.Decrypt(bytes);
				yield return coroutine3;
				bytes = (byte[])coroutine3.Current;
				if (bytes != null)
				{
					text = Encoding.UTF8.GetString(bytes);
				}
			}
			if (this.m_param.IsResponseCompress())
			{
				IEnumerator coroutine4 = this.Uncompress(bytes);
				yield return coroutine4;
				bytes = (byte[])coroutine4.Current;
				if (bytes != null)
				{
					text = Encoding.UTF8.GetString(bytes);
				}
			}
			if (!this.CheckResponseBody(text, headerValue))
			{
				this.m_param.SetSystemError(MeigewwwParam.eSystemError.Unknown);
				this.m_param.SetResponseJson(string.Empty);
			}
			else
			{
				this.m_param.SetSystemError(MeigewwwParam.eSystemError.None);
				this.m_param.SetResponseJson(text);
			}
			Block_23:
			if (this.m_www != null && !string.IsNullOrEmpty(this.m_www.error))
			{
				Debug.Log("www system error : " + this.m_www.error);
				this.m_param.SetError(this.m_www.error);
				this.m_param.SetSystemError(MeigewwwParam.eSystemError.Unknown);
			}
			if (this.m_www != null)
			{
				this.m_www.Dispose();
				this.m_www = null;
			}
			this.m_param.OnRecived();
			this.Dispose();
			yield break;
		}

		// Token: 0x0600549E RID: 21662 RVA: 0x0017B34C File Offset: 0x0017974C
		private bool CheckResponseBody(string body, string headerValue)
		{
			if (string.IsNullOrEmpty(headerValue))
			{
				return false;
			}
			uint num = 0U;
			if (ProjDepend.RH_CRC_IS_HEXADECIMAL)
			{
				uint.TryParse(headerValue, NumberStyles.HexNumber, null, out num);
			}
			else
			{
				uint.TryParse(headerValue, out num);
			}
			string value = string.Empty;
			Regex regex = new Regex("\"serverTime\":\"([-+\\d:T]+?)\"", RegexOptions.None);
			Match match = regex.Match(body);
			if (match.Success)
			{
				value = match.Groups[1].ToString();
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(body);
			stringBuilder.Append(value);
			byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
			uint num2 = CRC32.Compute(bytes);
			return num == num2;
		}

		// Token: 0x0600549F RID: 21663 RVA: 0x0017B408 File Offset: 0x00179808
		private IEnumerator Encrypt(byte[] src)
		{
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				byte[] inArray = AesCryptUtil.encrypt(src);
				string s = Convert.ToBase64String(inArray);
				src = Encoding.UTF8.GetBytes(s);
			}, null);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			yield return src;
			yield break;
		}

		// Token: 0x060054A0 RID: 21664 RVA: 0x0017B424 File Offset: 0x00179824
		private IEnumerator Compress(byte[] src)
		{
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				src = ZlibUtil.compress(src, 6);
			}, null);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			yield return src;
			yield break;
		}

		// Token: 0x060054A1 RID: 21665 RVA: 0x0017B440 File Offset: 0x00179840
		private IEnumerator Decrypt(byte[] src)
		{
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				src = AesCryptUtil.decrypt(src);
			}, null);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			yield return src;
			yield break;
		}

		// Token: 0x060054A2 RID: 21666 RVA: 0x0017B45C File Offset: 0x0017985C
		private IEnumerator Uncompress(byte[] src)
		{
			Job uncompressJob = new Job();
			uncompressJob.SetFunction(delegate(object obj)
			{
				src = ZlibUtil.uncompress(src);
			}, null);
			JobQueue.Enqueue(uncompressJob);
			while (!uncompressJob.IsDone())
			{
				yield return null;
			}
			yield return src;
			yield break;
		}

		// Token: 0x040055A6 RID: 21926
		private const int DEFAULT_TIMEOUT_SEC = 20;

		// Token: 0x040055A7 RID: 21927
		private const string REGEX_SERVER_TIME = "\"serverTime\":\"([-+\\d:T]+?)\"";

		// Token: 0x040055A8 RID: 21928
		private static Dictionary<string, string> m_HttpHeaderCommon;

		// Token: 0x040055A9 RID: 21929
		private static string m_sessionID = string.Empty;

		// Token: 0x040055AA RID: 21930
		private static string m_appversion = "0.0.0";

		// Token: 0x040055AB RID: 21931
		private static bool m_isRelease;

		// Token: 0x040055AC RID: 21932
		private static int m_timeoutSec = 20;

		// Token: 0x040055AD RID: 21933
		private UnityWebRequest m_www;

		// Token: 0x040055AE RID: 21934
		private MeigewwwParam m_param;
	}
}
