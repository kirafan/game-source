﻿using System;
using System.Text;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FCB RID: 4043
	public class PDBUtility
	{
		// Token: 0x06005430 RID: 21552 RVA: 0x00179740 File Offset: 0x00177B40
		private static uint GetRand(uint s)
		{
			uint num = s ^ s << 11;
			return 724963956U ^ (num ^ num >> 8);
		}

		// Token: 0x06005431 RID: 21553 RVA: 0x00179760 File Offset: 0x00177B60
		public static string Serialize(byte[] binaryData)
		{
			uint s = BitConverter.ToUInt32(binaryData, 0);
			uint num = BitConverter.ToUInt32(binaryData, 8);
			uint rand = PDBUtility.GetRand(s);
			byte[] bytes = BitConverter.GetBytes(rand);
			byte[] array = new byte[num];
			int num2 = 0;
			while ((long)num2 < (long)((ulong)num))
			{
				array[num2] = binaryData[16 + num2] - bytes[num2 % 4];
				num2++;
			}
			return Encoding.GetEncoding("utf-8").GetString(array);
		}

		// Token: 0x06005432 RID: 21554 RVA: 0x001797D8 File Offset: 0x00177BD8
		public static byte[] SerializeBytes(byte[] binaryData)
		{
			uint s = BitConverter.ToUInt32(binaryData, 0);
			uint num = BitConverter.ToUInt32(binaryData, 8);
			uint rand = PDBUtility.GetRand(s);
			byte[] bytes = BitConverter.GetBytes(rand);
			byte[] array = new byte[num];
			int num2 = 0;
			while ((long)num2 < (long)((ulong)num))
			{
				array[num2] = binaryData[16 + num2] - bytes[num2 % 4];
				num2++;
			}
			return array;
		}

		// Token: 0x06005433 RID: 21555 RVA: 0x0017983C File Offset: 0x00177C3C
		public static byte[] Load(string path)
		{
			byte[] array = null;
			try
			{
				TextAsset textAsset = (TextAsset)Resources.Load(path);
				array = new byte[textAsset.bytes.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = textAsset.bytes[i];
				}
			}
			catch (Exception message)
			{
				Debug.Log("PDB File Read Error!!!!\n" + Application.dataPath + path);
				Debug.Log(message);
				return null;
			}
			return array;
		}
	}
}
