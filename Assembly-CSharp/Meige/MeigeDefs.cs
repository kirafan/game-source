﻿using System;

namespace Meige
{
	// Token: 0x02000F66 RID: 3942
	public class MeigeDefs
	{
		// Token: 0x040050EF RID: 20719
		public static byte[] MFF_MLD_MAGIC = new byte[]
		{
			77,
			76,
			68,
			32
		};

		// Token: 0x040050F0 RID: 20720
		public static byte[] MFF_MACT_MAGIC = new byte[]
		{
			77,
			65,
			67,
			84
		};

		// Token: 0x040050F1 RID: 20721
		public const uint MFF_VERSION = 2U;

		// Token: 0x040050F2 RID: 20722
		public const uint MLD_VERSION = 0U;

		// Token: 0x040050F3 RID: 20723
		public const uint MACT_VERSION = 0U;

		// Token: 0x040050F4 RID: 20724
		public const int MEIGE_INVALID_ID = -1;

		// Token: 0x040050F5 RID: 20725
		public const int USABLE_NUM_OF_ADDITIONAL_TEXTURE = 1;

		// Token: 0x040050F6 RID: 20726
		public const string XMLNODE_MSBHIERARCHYNODELIST = "HierarchyNodeList";

		// Token: 0x040050F7 RID: 20727
		public const string XMLNODE_MSBHIERARCHYNODE = "MsbHierarhy";

		// Token: 0x040050F8 RID: 20728
		public const string XMLELEM_MESHLIST = "MeshList";

		// Token: 0x040050F9 RID: 20729
		public const string XMLNODE_MSBOBJECT = "MsbObject";

		// Token: 0x040050FA RID: 20730
		public const string XMLNODE_MSBMATERIAL = "MsbMaterial";

		// Token: 0x040050FB RID: 20731
		public const string XMLNODE_MSBMATERIALTEXINFO = "MsbMaterialTexInfo";

		// Token: 0x040050FC RID: 20732
		public const string XMLNODE_PARTICLEEMITTERLIST = "ParticleEmitterList";

		// Token: 0x040050FD RID: 20733
		public const string XMLNODE_PARTICLEEMITTER = "ParticleEmitter";

		// Token: 0x040050FE RID: 20734
		public const string XMLNODE_CAMERANODELIST = "CameraNodeList";

		// Token: 0x040050FF RID: 20735
		public const string XMLNODE_MSBCAMERA = "MsbCamera";

		// Token: 0x04005100 RID: 20736
		public const string XMLNODE_MSBADNode = "MsbADNode";

		// Token: 0x04005101 RID: 20737
		public const string XMLNODE_MSBADJoint = "MsbADJoint";

		// Token: 0x04005102 RID: 20738
		public const string XMLNODE_MSBADLink = "MsbADLink";

		// Token: 0x04005103 RID: 20739
		public const string XMLNODE_NAME = "Name";

		// Token: 0x04005104 RID: 20740
		public const string XMLNODE_REF_ID = "m_RefID";

		// Token: 0x04005105 RID: 20741
		public const string XMLNODE_ATTR = "Attribute";

		// Token: 0x04005106 RID: 20742
		public const string XMLNODE_MSBHIERARCHYNODE_DISTANCE = "m_vDistance";

		// Token: 0x04005107 RID: 20743
		public const string XMLNODE_MSBHIERARCHYNODE_SEGMENTSCALE_COMPENSATE = "MSB_HIERARCHY_ATTR_SEGMENTSCALE_COMPENSATE";

		// Token: 0x04005108 RID: 20744
		public const string XMLNODE_MSBHIERARCHYNODE_SKELETON = "MSB_HIERARCHY_ATTR_SKELETON";

		// Token: 0x04005109 RID: 20745
		public const string XMLNODE_MSBHIERARCHYNODE_JUSTTRANS_AFFECTEDBYPARENT = "MSB_HIERARCHY_ATTR_JUSTTRANS_AFFECTEDBYPARENT";

		// Token: 0x0400510A RID: 20746
		public const string XMLNODE_MSBOBJECT_AABB = "m_AABB";

		// Token: 0x0400510B RID: 20747
		public const string XMLNODE_MSBOBJECT_AABB_MIN = "AABB.Min";

		// Token: 0x0400510C RID: 20748
		public const string XMLNODE_MSBOBJECT_AABB_MAX = "AABB.Max";

		// Token: 0x0400510D RID: 20749
		public const string XMLNODE_MSBOBJECT_COLOR = "m_MeshColor";

		// Token: 0x0400510E RID: 20750
		public const string XMLNODE_MSBOBJECT_HIEINDEX = "m_HieIndex";

		// Token: 0x0400510F RID: 20751
		public const string XMLNODE_MSBOBJECT_BOUNDING_REFER_HIEINDEX = "m_BoundingReferHieIndex";

		// Token: 0x04005110 RID: 20752
		public const string XMLNODE_MSBOBJECT_RENDER_STAGE = "m_RenderStage";

		// Token: 0x04005111 RID: 20753
		public const string XMLNODE_MSBOBJECT_RENDER_ORDER = "m_RenderOrder";

		// Token: 0x04005112 RID: 20754
		public const string XMLNODE_MSBOBJECT_BILLBOARDTYPE = "m_BillBoardType";

		// Token: 0x04005113 RID: 20755
		public const string XMLNODE_MSBOBJECT_VISIBILITY = "MSBOBJ_ATTR_VISIBILITY";

		// Token: 0x04005114 RID: 20756
		public const string XMLNODE_MSBOBJECT_HALF_VERTEX_COLOR = "MSBOBJ_ATTR_HALF_VERTEX_COLOR";

		// Token: 0x04005115 RID: 20757
		public const string XMLNODE_MSBOBJECT_FOG_ENABLE = "MSBOBJ_ATTR_FOG_ENABLE";

		// Token: 0x04005116 RID: 20758
		public const string XMLNODE_MSBOBJECT_ENABLE_OUTLINE = "MSBOBJ_ATTR_OUTLINE";

		// Token: 0x04005117 RID: 20759
		public const string XMLNODE_MSBOBJECT_OUTLINE_COLOR = "m_OutlineColor";

		// Token: 0x04005118 RID: 20760
		public const string XMLNODE_MSBOBJECT_OUTLINE_WIDTH = "m_OutlineWidth";

		// Token: 0x04005119 RID: 20761
		public const string XMLNODE_MSBOBJECT_OUTLINE_DEPTHOFFSET = "m_OutlineDepthOffset";

		// Token: 0x0400511A RID: 20762
		public const string XMLNODE_MSBOBJECT_OUTLINE_TYPE = "m_OutlineType";

		// Token: 0x0400511B RID: 20763
		public const string XMLNODE_MSBMATERIAL_SHADINGTYPE = "m_ShadingType";

		// Token: 0x0400511C RID: 20764
		public const string XMLNODE_MSBMATERIAL_DIFFUSE = "m_Diffuse";

		// Token: 0x0400511D RID: 20765
		public const string XMLNODE_MSBMATERIAL_BLENDMODE = "m_BlendMode";

		// Token: 0x0400511E RID: 20766
		public const string XMLNODE_MSBMATERIAL_HDRFACTOR = "m_HDRFactor";

		// Token: 0x0400511F RID: 20767
		public const string XMLNODE_MSBMATERIAL_CULL = "MSB_MATERIAL_ATTRFLAG_CULL";

		// Token: 0x04005120 RID: 20768
		public const string XMLNODE_MSBMATERIAL_HIGHLIGHT_MASK = "MSB_MATERIAL_ATTRFLAG_HIGHLIGHT_MASK";

		// Token: 0x04005121 RID: 20769
		public const string XMLNODE_MSBMATERIAL_ALPHATEST_REF_VALUE = "m_AlphaTestRefValue";

		// Token: 0x04005122 RID: 20770
		public const string XMLNODE_MSBMATERIAL_OUTLINE_BLENDMODE_VALUE = "m_OutlineBlendMode";

		// Token: 0x04005123 RID: 20771
		public const string XMLNODE_MSBTEXTURE_TYPE = "TextureType";

		// Token: 0x04005124 RID: 20772
		public const string XMLNODE_MSBTEXTURE_LAYER = "TextureLayer";

		// Token: 0x04005125 RID: 20773
		public const string XMLNODE_MSBTEXTURE_COVERAGE = "m_CoverageUV";

		// Token: 0x04005126 RID: 20774
		public const string XMLNODE_MSBTEXTURE_TRANSLATION = "m_TanslationUV";

		// Token: 0x04005127 RID: 20775
		public const string XMLNODE_MSBTEXTURE_OFFSET = "m_OffsetUV";

		// Token: 0x04005128 RID: 20776
		public const string XMLNODE_MSBTEXTURE_ROTATE = "m_RotateUV";

		// Token: 0x04005129 RID: 20777
		public const string XMLNODE_MSBTEXTURE_LAYERBLENDMODE = "m_LayerBlendMode";

		// Token: 0x0400512A RID: 20778
		public const string XMLNODE_MSBTEXTURE_LAYERBLENDMODE_ALPHA = "m_LayerBlendModeAlpha";

		// Token: 0x0400512B RID: 20779
		public const string XMLNODE_CAMERA_PROJECTIONTYPE = "m_ProjectionType";

		// Token: 0x0400512C RID: 20780
		public const string XMLNODE_CAMERA_HIEINDEX = "m_HieIndex";

		// Token: 0x0400512D RID: 20781
		public const string XMLNODE_CAMERA_FOCALLENGTH = "m_FocalLength";

		// Token: 0x0400512E RID: 20782
		public const string XMLNODE_CAMERA_APARTUREWIDTH = "m_ApartureWidth";

		// Token: 0x0400512F RID: 20783
		public const string XMLNODE_CAMERA_APARTUREHEIGHT = "m_ApartureHeight";

		// Token: 0x04005130 RID: 20784
		public const string XMLNODE_CAMERA_ZNEAR = "m_Znear";

		// Token: 0x04005131 RID: 20785
		public const string XMLNODE_CAMERA_ZFAR = "m_Zfar";

		// Token: 0x04005132 RID: 20786
		public const string XMLNODE_CAMERA_ORTHOGRAPHICSSIZE = "m_OrthographicSize";

		// Token: 0x04005133 RID: 20787
		public const string XMLNODE_CAMERA_PP_EXPOSURE_VALUE = "m_ExposureValue";

		// Token: 0x04005134 RID: 20788
		public const string XMLNODE_CAMERA_PP_TONEMAP_WHITEPOINT_TYPE = "m_ToneMapWhitePointType";

		// Token: 0x04005135 RID: 20789
		public const string XMLNODE_CAMERA_PP_TONEMAP_WHITEPOINT_SCALE = "m_ToneMapWhitePointScale";

		// Token: 0x04005136 RID: 20790
		public const string XMLNODE_CAMERA_PP_TONEMAP_WHITEPOINT_RANGE_MIN = "m_ToneMapWhitePointRangeMin";

		// Token: 0x04005137 RID: 20791
		public const string XMLNODE_CAMERA_PP_TONEMAP_WHITEPOINT_RANGE_MAX = "m_ToneMapWhitePointRangeMax";

		// Token: 0x04005138 RID: 20792
		public const string XMLNODE_CAMERA_PP_BLOOM_REFERENCE_TYPE = "m_BloomReferenceType";

		// Token: 0x04005139 RID: 20793
		public const string XMLNODE_CAMERA_PP_BLOOM_REFERENCE_THRESHOLD_OFFSET = "m_BloomReferenceThresholdOffset";

		// Token: 0x0400513A RID: 20794
		public const string XMLNODE_CAMERA_PP_BLOOM_OFFSET = "m_BloomOffset";

		// Token: 0x0400513B RID: 20795
		public const string XMLNODE_CAMERA_PP_CONTRAST_LEVEL = "m_ContrastLevel";

		// Token: 0x0400513C RID: 20796
		public const string XMLNODE_CAMERA_PP_BRIGHTNESS_LEVEL = "m_BrightnessLevel";

		// Token: 0x0400513D RID: 20797
		public const string XMLNODE_CAMERA_PP_CHROMA_LEVEL = "m_ChromaLevel";

		// Token: 0x0400513E RID: 20798
		public const string XMLNODE_CAMERA_PP_COLORBLEND_LEVEL = "m_ColorBlendLevel";

		// Token: 0x0400513F RID: 20799
		public const string XMLNODE_CAMERA_PP_COLOR_BLEND_COLOR = "m_ColorBlendColor";

		// Token: 0x04005140 RID: 20800
		public const string XMLNODE_CAMERA_PP_ADAPTATION_RATESPEED = "m_AdaptationRateSpeed";

		// Token: 0x04005141 RID: 20801
		public const string XMLNODE_CAMERA_PP_ENABLE_TONEMAP = "m_bEnableCorrectToneMap";

		// Token: 0x04005142 RID: 20802
		public const string XMLNODE_CAMERA_PP_ENABLE_BLOOM = "m_bEnableBloom";

		// Token: 0x04005143 RID: 20803
		public const string XMLNODE_CAMERA_PP_ENABLE_CONTRAST = "m_bEnableCorrectContrast";

		// Token: 0x04005144 RID: 20804
		public const string XMLNODE_CAMERA_PP_ENABLE_BRIGHTNESS = "m_bEnableCorrectBrightness";

		// Token: 0x04005145 RID: 20805
		public const string XMLNODE_CAMERA_PP_ENABLE_CHROMA = "m_bEnableCorrectChroma";

		// Token: 0x04005146 RID: 20806
		public const string XMLNODE_CAMERA_PP_ENABLE_COLORBLEND = "m_bEnableCorrectColorBlend";

		// Token: 0x04005147 RID: 20807
		public const string XMLNODE_MIBHEADER = "MibHeader";

		// Token: 0x04005148 RID: 20808
		public const string XMLNODE_MIBHEADER_PIXELFORMAT = "PixelFormat";

		// Token: 0x04005149 RID: 20809
		public const string XMLNODE_MIBHEADER_ATTR_REPEAT_U = "MIBTEX_ATTR_REPEAT_U";

		// Token: 0x0400514A RID: 20810
		public const string XMLNODE_MIBHEADER_ATTR_REPEAT_V = "MIBTEX_ATTR_REPEAT_V";

		// Token: 0x0400514B RID: 20811
		public const string XMLNODE_MABHEADER = "MabHeader";

		// Token: 0x0400514C RID: 20812
		public const string XMLNODE_MABHEADER_KEYFRAME_NUM = "m_Keyframe";

		// Token: 0x0400514D RID: 20813
		public const string XMLNODE_MABHEADER_BASEFPS = "m_BaseFPS";

		// Token: 0x0400514E RID: 20814
		public const string XMLNODE_MABANIMNODELIST = "AnimNodeList";

		// Token: 0x0400514F RID: 20815
		public const string XMLNODE_MABANIMNODEHEADER = "MabAnimNodeHeader";

		// Token: 0x04005150 RID: 20816
		public const string XMLNODE_MABANIMNODE_TYPE = "m_TgtType";

		// Token: 0x04005151 RID: 20817
		public const string XMLNODE_MABANIMNODE_TARGET = "TargetInfo";

		// Token: 0x04005152 RID: 20818
		public const string XMLNODE_MABANIMNODE_TARGET_HIE_INDEX = "m_HieIndex";

		// Token: 0x04005153 RID: 20819
		public const string XMLNODE_MABANIMNODE_TARGET_MESH_INDEX = "m_MeshIndex";

		// Token: 0x04005154 RID: 20820
		public const string XMLNODE_MABANIMNODE_TARGET_MAT_INDEX = "m_MatIndex";

		// Token: 0x04005155 RID: 20821
		public const string XMLNODE_MABANIMNODE_TARGET_MAT_TEXTYPE = "m_TexType";

		// Token: 0x04005156 RID: 20822
		public const string XMLNODE_MABANIMNODE_TARGET_MAT_LAYERID = "m_LayerID";

		// Token: 0x04005157 RID: 20823
		public const string XMLNODE_MABANIMNODE_TARGET_CAMERA_INDEX = "m_CamIndex";

		// Token: 0x04005158 RID: 20824
		public const string XMLNODE_MABANIMNODE_TARGET_PE_INDEX = "m_PEIndex";

		// Token: 0x04005159 RID: 20825
		public const string XMLNODE_MABANIMNODE_CURVENUM = "m_CurveInfoNum";

		// Token: 0x0400515A RID: 20826
		public const string XMLNODE_MABANIMNODE_CURVE = "MabCurveInfo";

		// Token: 0x0400515B RID: 20827
		public const string XMLNODE_MABANIMNODE_CURVE_ATTR_HASDERIVATIVE = "MAB_CURVE_ATTRFLAG_HAS_DERIVATIVE";

		// Token: 0x0400515C RID: 20828
		public const string XMLNODE_MABANIMNODE_CURVE_KEYDATA_NUM = "m_KeyDataNum";

		// Token: 0x0400515D RID: 20829
		public const string XMLNODE_MABANIMNODE_CURVE_COMPONENT_NUM = "m_ComponentNum";

		// Token: 0x0400515E RID: 20830
		public const string XMLNODE_MABANIMNODE_CURVE_KEYFDATA = "Keyframe";

		// Token: 0x0400515F RID: 20831
		public const string XMLNODE_MABANIMEVENTLIST = "AnimEventList";

		// Token: 0x04005160 RID: 20832
		public const string XMLNODE_MABANIMEVENT = "AnimEvent";

		// Token: 0x04005161 RID: 20833
		public const string XMLNODE_MABANIMEVENT_FRAME = "m_Frame";

		// Token: 0x04005162 RID: 20834
		public const string XMLNODE_MABANIMEVENT_IVALUE = "m_iValue";

		// Token: 0x04005163 RID: 20835
		public const string XMLNODE_MSBADHANDLER_ANIMBLENDRATIO = "m_AnimBlendRatio";

		// Token: 0x04005164 RID: 20836
		public const string XMLNODE_MSBADHANDLER_KEEPSHAPECOEF = "m_KeepShapeCoef";

		// Token: 0x04005165 RID: 20837
		public const string XMLNODE_MSBADHANDLER_LINKCONSTRAINTFORCE = "m_LinkConstraintForce";

		// Token: 0x04005166 RID: 20838
		public const string XMLNODE_MSBADHANDLER_LIMITATIONANGLE = "m_LimitationAngle";

		// Token: 0x04005167 RID: 20839
		public const string XMLNODE_MSBADHANDLER_AIRFRICTIONCOEF = "m_AirFrictionCoef";

		// Token: 0x04005168 RID: 20840
		public const string XMLNODE_MSBADHANDLER_JNTNUM = "m_JntNum";

		// Token: 0x04005169 RID: 20841
		public const string XMLNODE_MSBADHANDLER_LINKNUM = "m_LinkNum";

		// Token: 0x0400516A RID: 20842
		public const string XMLNODE_MSBADHANDLER_ROOTJNTNUM = "m_RootJntNum";

		// Token: 0x0400516B RID: 20843
		public const string XMLNODE_MSBADHANDLER_HIERARCHYDEPTH = "m_HierachyDepth";

		// Token: 0x0400516C RID: 20844
		public const string XMLNODE_MSBADHANDLER_COLLISIONNUM = "m_CollisionNum";

		// Token: 0x0400516D RID: 20845
		public const string XMLNODE_MSBADJOINT_HIERARCHYNAME = "HierarchyName";

		// Token: 0x0400516E RID: 20846
		public const string XMLNODE_MSBADJOINT_HIEINDEX = "m_HieIndex";

		// Token: 0x0400516F RID: 20847
		public const string XMLNODE_MSBADJOINT_PARENTIDXOFADHIERARCHY = "m_ParentIdxOfADHierarchy";

		// Token: 0x04005170 RID: 20848
		public const string XMLNODE_MSBADJOINT_NUMOFIKGROUP = "m_NumOfIKGroup";

		// Token: 0x04005171 RID: 20849
		public const string XMLNODE_MSBADJOINT_LOCK = "ADJOINT_ATTRFLAG_LOCK";

		// Token: 0x04005172 RID: 20850
		public const string XMLNODE_MSBADJOINT_ROOT = "ADJOINT_ATTRFLAG_ROOT";

		// Token: 0x04005173 RID: 20851
		public const string XMLNODE_MSBADJOINT_GRAVITY = "ADJOINT_ATTRFLAG_GRAVITY";

		// Token: 0x04005174 RID: 20852
		public const string XMLNODE_MSBADJOINT_AIRFRICTION = "ADJOINT_ATTRFLAG_AIRFRICTION";

		// Token: 0x04005175 RID: 20853
		public const string XMLNODE_MSBADLINK_SRCADJOINTIDX = "m_SrcADJointIdx";

		// Token: 0x04005176 RID: 20854
		public const string XMLNODE_MSBADLINK_DSTADJOINTIDX = "m_DstADJointIdx";

		// Token: 0x04005177 RID: 20855
		public const string XMLNODE_MSBADLINK_RADIUS = "m_Radius";

		// Token: 0x04005178 RID: 20856
		public const string XMLNODE_MSBADLINK_LENGTH = "m_Length";

		// Token: 0x04005179 RID: 20857
		public const string XMLNODE_MSBADLINK_LINKTYPE = "m_LinkType";

		// Token: 0x0400517A RID: 20858
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE = "ParticleType";

		// Token: 0x0400517B RID: 20859
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_DEF = "m_particleType";

		// Token: 0x0400517C RID: 20860
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_BILLBOARD_PARAM_WIDTH_RANGE_MIN = "m_widthRange_Min";

		// Token: 0x0400517D RID: 20861
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_BILLBOARD_PARAM_WIDTH_RANGE_MAX = "m_widthRange_Max";

		// Token: 0x0400517E RID: 20862
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_BILLBOARD_PARAM_HEIGHT_RANGE_MIN = "m_heightRange_Min";

		// Token: 0x0400517F RID: 20863
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_BILLBOARD_PARAM_HEIGHT_RANGE_MAX = "m_heightRange_Max";

		// Token: 0x04005180 RID: 20864
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POINT_PARAM_SIZE_RANGE_MIN = "m_sizeRange_Min";

		// Token: 0x04005181 RID: 20865
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POINT_PARAM_SIZE_RANGE_MAX = "m_sizeRange_Max";

		// Token: 0x04005182 RID: 20866
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_LINE_PARAM_WIDTH = "m_width";

		// Token: 0x04005183 RID: 20867
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_LINE_PARAM_JOINT_NUM = "m_jointNum";

		// Token: 0x04005184 RID: 20868
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POLYLINE_PARAM_TOP_WIDTH_RANGE_MIN = "m_topWidthRange_Min";

		// Token: 0x04005185 RID: 20869
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POLYLINE_PARAM_TOP_WIDTH_RANGE_MAX = "m_topWidthRange_Max";

		// Token: 0x04005186 RID: 20870
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POLYLINE_PARAM_END_WIDTH_RANGE_MIN = "m_endWidthRange_Min";

		// Token: 0x04005187 RID: 20871
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POLYLINE_PARAM_END_WIDTH_RANGE_MAX = "m_endWidthRange_Max";

		// Token: 0x04005188 RID: 20872
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_POLYLINE_PARAM_JOINT_NUM = "m_jointNum";

		// Token: 0x04005189 RID: 20873
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_CONFETTI_PARAM_WIDTH_RANGE_MIN = "m_widthRange_Min";

		// Token: 0x0400518A RID: 20874
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_CONFETTI_PARAM_WIDTH_RANGE_MAX = "m_widthRange_Max";

		// Token: 0x0400518B RID: 20875
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_CONFETTI_PARAM_HEIGHT_RANGE_MIN = "m_heightRange_Min";

		// Token: 0x0400518C RID: 20876
		public const string XMLNODE_PTCLEMITTER_PARTICLE_TYPE_CONFETTI_PARAM_HEIGHT_RANGE_MAX = "m_heightRange_Max";

		// Token: 0x0400518D RID: 20877
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE = "EmissionType";

		// Token: 0x0400518E RID: 20878
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_DEF = "m_emitionType";

		// Token: 0x0400518F RID: 20879
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_POINT_PARAM_ANGLE_RANGE_MIN = "m_angleRange_Min";

		// Token: 0x04005190 RID: 20880
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_POINT_PARAM_ANGLE_RANGE_MAX = "m_angleRange_Max";

		// Token: 0x04005191 RID: 20881
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_WIDTH_RANGE_MIN = "m_widthRange_Min";

		// Token: 0x04005192 RID: 20882
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_WIDTH_RANGE_MAX = "m_widthRange_Max";

		// Token: 0x04005193 RID: 20883
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_HEIGHT_RANGE_MIN = "m_heightRange_Min";

		// Token: 0x04005194 RID: 20884
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_HEIGHT_RANGE_MAX = "m_heightRange_Max";

		// Token: 0x04005195 RID: 20885
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_DEPTH_RANGE_MIN = "m_depthRange_Min";

		// Token: 0x04005196 RID: 20886
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_BOX_PARAM_DEPTH_RANGE_MAX = "m_depthRange_Max";

		// Token: 0x04005197 RID: 20887
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANEQUAD_PARAM_WIDTH_RANGE_MIN = "m_widthRange_Min";

		// Token: 0x04005198 RID: 20888
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANEQUAD_PARAM_WIDTH_RANGE_MAX = "m_widthRange_Max";

		// Token: 0x04005199 RID: 20889
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANEQUAD_PARAM_HEIGHT_RANGE_MIN = "m_heightRange_Min";

		// Token: 0x0400519A RID: 20890
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANEQUAD_PARAM_HEIGHT_RANGE_MAX = "m_heightRange_Max";

		// Token: 0x0400519B RID: 20891
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANECIRCLE_PARAM_WIDTH_RANGE_MIN = "m_radiusRange_Min";

		// Token: 0x0400519C RID: 20892
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_PLANECIRCLE_PARAM_WIDTH_RANGE_MAX = "m_radiusRange_Max";

		// Token: 0x0400519D RID: 20893
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_SPHERE_PARAM_ANGLE_RANGE_MIN = "m_angleRange_Min";

		// Token: 0x0400519E RID: 20894
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_SPHERE_PARAM_ANGLE_RANGE_MAX = "m_angleRange_Max";

		// Token: 0x0400519F RID: 20895
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_SPHERE_PARAM_RADIUS_RANGE_MIN = "m_radiusRange_Min";

		// Token: 0x040051A0 RID: 20896
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_SPHERE_PARAM_RADIUS_RANGE_MAX = "m_radiusRange_Max";

		// Token: 0x040051A1 RID: 20897
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_TORUS_PARAM_ANGLE_RANGE_MIN = "m_angleRange_Min";

		// Token: 0x040051A2 RID: 20898
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_TORUS_PARAM_ANGLE_RANGE_MAX = "m_angleRange_Max";

		// Token: 0x040051A3 RID: 20899
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_TORUS_PARAM_BIGRADIUS = "m_bigRadiusRange";

		// Token: 0x040051A4 RID: 20900
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_TORUS_PARAM_SMALLRADIUS_RANGE_MIN = "m_smallRadiusRange_Min";

		// Token: 0x040051A5 RID: 20901
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_TORUS_PARAM_SMALLRADIUS_RANGE_MAX = "m_smallRadiusRange_Max";

		// Token: 0x040051A6 RID: 20902
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_CYLINDER_PARAM_RADIUS_RANGE_MIN = "m_RadiusRange_Min";

		// Token: 0x040051A7 RID: 20903
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_CYLINDER_PARAM_RADIUS_RANGE_MAX = "m_RadiusRange_Max";

		// Token: 0x040051A8 RID: 20904
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_CYLINDER_PARAM_HEIGHT_RANGE_MIN = "m_HeightRange_Min";

		// Token: 0x040051A9 RID: 20905
		public const string XMLNODE_PTCLEMITTER_EMISSION_TYPE_CYLINDER_PARAM_HEIGHT_RANGE_MAX = "m_HeightRange_Max";

		// Token: 0x040051AA RID: 20906
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESCALE_RANGE_MIN = "m_lifeScaleRange_Min";

		// Token: 0x040051AB RID: 20907
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESCALE_RANGE_MAX = "m_lifeScaleRange_Max";

		// Token: 0x040051AC RID: 20908
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_ACCELERATION_FLG = "m_usingAccelerationFlg";

		// Token: 0x040051AD RID: 20909
		public const string XMLNODE_PTCLEMITTER_PARAM_ACCELERATION_RANGE_MIN = "m_accelerationRange_Min";

		// Token: 0x040051AE RID: 20910
		public const string XMLNODE_PTCLEMITTER_PARAM_ACCELERATION_RANGE_MAX = "m_accelerationRange_Max";

		// Token: 0x040051AF RID: 20911
		public const string XMLNODE_PTCLEMITTER_PARAM_DRAGFORCE_RANGE_MIN = "m_dragForceRange_Min";

		// Token: 0x040051B0 RID: 20912
		public const string XMLNODE_PTCLEMITTER_PARAM_DRAGFORCE_RANGE_MAX = "m_dragForceRange_Max";

		// Token: 0x040051B1 RID: 20913
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_ROTATION_FLG = "m_usingRotationFlg";

		// Token: 0x040051B2 RID: 20914
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTSPEED_RANGE_MIN = "m_rotSpeedRange_Min";

		// Token: 0x040051B3 RID: 20915
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTSPEED_RANGE_MAX = "m_rotSpeedRange_Max";

		// Token: 0x040051B4 RID: 20916
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTACCELERATION_RANGE_MIN = "m_rotAccelerationRange_Min";

		// Token: 0x040051B5 RID: 20917
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTACCELERATION_RANGE_MAX = "m_rotAccelerationRange_Max";

		// Token: 0x040051B6 RID: 20918
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTDRAGFORCE_RANGE_MIN = "m_rotDragForceRange_Min";

		// Token: 0x040051B7 RID: 20919
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTDRAGFORCE_RANGE_MAX = "m_rotDragForceRange_Max";

		// Token: 0x040051B8 RID: 20920
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTANCHOROFFSET_RANGE_MIN = "m_rotAnchorOffsetRange_Min";

		// Token: 0x040051B9 RID: 20921
		public const string XMLNODE_PTCLEMITTER_PARAM_ROTANCHOROFFSET_RANGE_MAX = "m_rotAnchorOffsetRange_Max";

		// Token: 0x040051BA RID: 20922
		public const string XMLNODE_PTCLEMITTER_PARAM_ROT_RANGE_MIN = "m_rotRange_Min";

		// Token: 0x040051BB RID: 20923
		public const string XMLNODE_PTCLEMITTER_PARAM_ROT_RANGE_MAX = "m_rotRange_Max";

		// Token: 0x040051BC RID: 20924
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_UVANIM_FLG = "m_usingUVAnimationFlg";

		// Token: 0x040051BD RID: 20925
		public const string XMLNODE_PTCLEMITTER_PARAM_UVANIM_TYPE = "m_uvAnimeType";

		// Token: 0x040051BE RID: 20926
		public const string XMLNODE_PTCLEMITTER_PARAM_UVANIM_SWITCHBLOCKSEC_RANGE_MIN = "m_uvAnimeSwitchBlockSecRange_Min";

		// Token: 0x040051BF RID: 20927
		public const string XMLNODE_PTCLEMITTER_PARAM_UVANIM_SWITCHBLOCKSEC_RANGE_MAX = "m_uvAnimeSwitchBlockSecRange_Max";

		// Token: 0x040051C0 RID: 20928
		public const string XMLNODE_PTCLEMITTER_PARAM_UVANIM_RANDOMSTARTBLOCK_FLG = "m_uvAnimeRandomStartBlockFlg";

		// Token: 0x040051C1 RID: 20929
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_COLORCURVE_FLG = "m_usingColorCurveFlg";

		// Token: 0x040051C2 RID: 20930
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_COLORCURVE_NUM = "m_colorCurveNum";

		// Token: 0x040051C3 RID: 20931
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM = "ColorCurveParam";

		// Token: 0x040051C4 RID: 20932
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_COLOR0 = "m_colorCurveArray_0";

		// Token: 0x040051C5 RID: 20933
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_COLOR1 = "m_colorCurveArray_1";

		// Token: 0x040051C6 RID: 20934
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_COLOR2 = "m_colorCurveArray_2";

		// Token: 0x040051C7 RID: 20935
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_COLOR3 = "m_colorCurveArray_3";

		// Token: 0x040051C8 RID: 20936
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_RATIO0 = "m_colorCurveRatio_0";

		// Token: 0x040051C9 RID: 20937
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_RATIO1 = "m_colorCurveRatio_1";

		// Token: 0x040051CA RID: 20938
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_RATIO2 = "m_colorCurveRatio_2";

		// Token: 0x040051CB RID: 20939
		public const string XMLNODE_PTCLEMITTER_PARAM_COLORCURVE_PARAM_RATIO3 = "m_colorCurveRatio_3";

		// Token: 0x040051CC RID: 20940
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_LIGHT_FLG = "m_usingLightFlg";

		// Token: 0x040051CD RID: 20941
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_TAIL_FLG = "m_usingTailFlg";

		// Token: 0x040051CE RID: 20942
		public const string XMLNODE_PTCLEMITTER_PARAM_TAIL_JOINT_NUM = "m_tailJointNum";

		// Token: 0x040051CF RID: 20943
		public const string XMLNODE_PTCLEMITTER_PARAM_TAIL_UVRECT = "m_tailUVRect";

		// Token: 0x040051D0 RID: 20944
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_BLINK_FLG = "m_usingBlinkFlg";

		// Token: 0x040051D1 RID: 20945
		public const string XMLNODE_PTCLEMITTER_PARAM_BLINK_RANGE_MIN = "m_blinkSpanSecRange_Min";

		// Token: 0x040051D2 RID: 20946
		public const string XMLNODE_PTCLEMITTER_PARAM_BLINK_RANGE_MAX = "m_blinkSpanSecRange_Max";

		// Token: 0x040051D3 RID: 20947
		public const string XMLNODE_PTCLEMITTER_PARAM_USING_PATHMOVE_FLG = "m_usingPathMoveFlg";

		// Token: 0x040051D4 RID: 20948
		public const string XMLNODE_PTCLEMITTER_PARAM_PATHMOVE_START_FOCUS_RADIUS_RANGE_MIN = "m_startFocusRadiusRange_Min";

		// Token: 0x040051D5 RID: 20949
		public const string XMLNODE_PTCLEMITTER_PARAM_PATHMOVE_START_FOCUS_RADIUS_RANGE_MAX = "m_startFocusRadiusRange_Max";

		// Token: 0x040051D6 RID: 20950
		public const string XMLNODE_PTCLEMITTER_PARAM_PATHMOVE_END_FOCUS_RADIUS_RANGE_MIN = "m_endFocusRadiusRange_Min";

		// Token: 0x040051D7 RID: 20951
		public const string XMLNODE_PTCLEMITTER_PARAM_PATHMOVE_END_FOCUS_RADIUS_RANGE_MAX = "m_endFocusRadiusRange_Max";

		// Token: 0x040051D8 RID: 20952
		public const string XMLNODE_PTCLEMITTER_PARAM_UVBLOCK_NUM = "m_uvBlockNum";

		// Token: 0x040051D9 RID: 20953
		public const string XMLNODE_PTCLEMITTER_PARAM_UVBLOCK_TOPBLOCK = "m_uvRect_TopBlock";

		// Token: 0x040051DA RID: 20954
		public const string XMLNODE_PTCLEMITTER_PARAM_GRAVITY_DIR = "m_gravityDir";

		// Token: 0x040051DB RID: 20955
		public const string XMLNODE_PTCLEMITTER_PARAM_GRAVITY_FORCE_RANGE_MIN = "m_gravityForceRange_Min";

		// Token: 0x040051DC RID: 20956
		public const string XMLNODE_PTCLEMITTER_PARAM_GRAVITY_FORCE_RANGE_MAX = "m_gravityForceRange_Max";

		// Token: 0x040051DD RID: 20957
		public const string XMLNODE_PTCLEMITTER_PARAM_SPEED_RANGE_MIN = "m_speedRange_Min";

		// Token: 0x040051DE RID: 20958
		public const string XMLNODE_PTCLEMITTER_PARAM_SPEED_RANGE_MAX = "m_speedRange_Max";

		// Token: 0x040051DF RID: 20959
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE = "LifeSpanType";

		// Token: 0x040051E0 RID: 20960
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_DEF = "m_lifeSpanType";

		// Token: 0x040051E1 RID: 20961
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_ALPHA = "m_lifeSpanAlpha";

		// Token: 0x040051E2 RID: 20962
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_TIME_RANGE_MIN = "m_lifeSpanSecRange_Min";

		// Token: 0x040051E3 RID: 20963
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_TIME_RANGE_MAX = "m_lifeSpanSecRange_Max";

		// Token: 0x040051E4 RID: 20964
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_DISTANCE_RANGE_MIN = "m_lifeSpanDistanceMaxRange_Min";

		// Token: 0x040051E5 RID: 20965
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_DISTANCE_RANGE_MAX = "m_lifeSpanDistanceMaxRange_Max";

		// Token: 0x040051E6 RID: 20966
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_HEIGHT_RANGE_MIN = "m_lifeSpanHeightRange_Min";

		// Token: 0x040051E7 RID: 20967
		public const string XMLNODE_PTCLEMITTER_PARAM_LIFESPAN_TYPE_HEIGHT_RANGE_MAX = "m_lifeSpanHeightRange_Max";

		// Token: 0x040051E8 RID: 20968
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_TYPE = "CollisionType";

		// Token: 0x040051E9 RID: 20969
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_TYPE_DEF = "m_collisionType";

		// Token: 0x040051EA RID: 20970
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_HEIGHT_HEIGHT = "m_height";

		// Token: 0x040051EB RID: 20971
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_HEIGHT_REFLECTIONCOEFFICIENT = "m_reflectionCoefficient";

		// Token: 0x040051EC RID: 20972
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_HEIGHT_FRICTIONCOEFFICIENT = "m_frictionCoefficient";

		// Token: 0x040051ED RID: 20973
		public const string XMLNODE_PTCLEMITTER_PARAM_COLLISION_HEIGHT_LOCALHEIGHT = "m_isLocalHeight";

		// Token: 0x040051EE RID: 20974
		public const string XMLNODE_PTCLEMITTER_PARAM_PARTICLE_NUM = "m_particleNum";

		// Token: 0x040051EF RID: 20975
		public const string XMLNODE_PTCLEMITTER_PARAM_INCIDENTNUMBERPERSEC = "m_incidentNumberPerSec";

		// Token: 0x040051F0 RID: 20976
		public const string XMLNODE_PTCLEMITTER_PARAM_INCIDENTRANDOMLEVEL = "m_incidentRandomLevel";

		// Token: 0x040051F1 RID: 20977
		public const string XMLNODE_PTCLEMITTER_PARAM_BASEFRAMESPEEDSCALE = "m_baseFrameSpeedScale";

		// Token: 0x040051F2 RID: 20978
		public const string XMLNODE_PTCLEMITTER_PARAM_HDRFACTOR = "m_HDR_Factor";

		// Token: 0x040051F3 RID: 20979
		public const string XMLNODE_PTCLEMITTER_PARAM_IS_LOCALTRANS = "m_isLocalTrans";

		// Token: 0x040051F4 RID: 20980
		public const string XMLNODE_PTCLEMITTER_PARAM_IS_ORTHOGRAPH = "m_isOrthoGraph";

		// Token: 0x040051F5 RID: 20981
		public const string XMLNODE_PTCLEMITTER_PARAM_LOCALROTATION = "m_LocalRotation";

		// Token: 0x040051F6 RID: 20982
		public const string XMLNODE_PTCLEMITTER_PARAM_RANDOM_EMIT_DIR_FLG = "m_isRandomEmitDir";

		// Token: 0x040051F7 RID: 20983
		public const string XMLNODE_PTCLEMITTER_PARAM_IS_ACTIVE = "m_bActive";

		// Token: 0x040051F8 RID: 20984
		public const int STRIDE_FROM_RENDERSTAGE_TO_RENDERQUEUE = 125;

		// Token: 0x040051F9 RID: 20985
		public const int OFFSET_TO_ALPHA_RENDERQUEUE = 1;

		// Token: 0x040051FA RID: 20986
		public const int JOB_QUEUE_THREAD_NUM = 2;

		// Token: 0x040051FB RID: 20987
		public const int JOB_QUEUE_JOB_MAX = 100;

		// Token: 0x040051FC RID: 20988
		public const int JOB_QUEUE_MASK_BIT_FLAG_NUM = 16;

		// Token: 0x040051FD RID: 20989
		public const int WWW_QUEUE_MASK_BIT_FLAG_NUM = 16;

		// Token: 0x040051FE RID: 20990
		public const int WWW_MAX_CONCURRENT_CONNECTION = 10;

		// Token: 0x040051FF RID: 20991
		public const int WWW_MAX_REQUEST_PARAM = 100;

		// Token: 0x04005200 RID: 20992
		public const int WWW_POST_TYPE_NONE = 0;

		// Token: 0x04005201 RID: 20993
		public const int WWW_POST_TYPE_REQUEST_CRYPT = 1;

		// Token: 0x04005202 RID: 20994
		public const int WWW_POST_TYPE_RESPONSE_CRYPT = 16;

		// Token: 0x04005203 RID: 20995
		public const int WWW_POST_TYPE_REQUEST_COMPRESS = 256;

		// Token: 0x04005204 RID: 20996
		public const int WWW_POST_TYPE_RESPONSE_COMPRESS = 4096;

		// Token: 0x04005205 RID: 20997
		public const int WWW_POST_TYPE_CRYPT = 17;

		// Token: 0x04005206 RID: 20998
		public const int WWW_POST_TYPE_COMPRESS = 4352;

		// Token: 0x04005207 RID: 20999
		public const int WWW_POST_TYPE_ALL = 4369;

		// Token: 0x04005208 RID: 21000
		public const float F_EPSILON6 = 1E-06f;

		// Token: 0x04005209 RID: 21001
		public const float F_EPSILON5 = 1E-05f;

		// Token: 0x0400520A RID: 21002
		public const float F_EPSILON4 = 0.0001f;

		// Token: 0x0400520B RID: 21003
		public const float F_EPSILON3 = 0.001f;

		// Token: 0x0400520C RID: 21004
		public const float MEIGE_GRAVITY = 9.80665f;
	}
}
