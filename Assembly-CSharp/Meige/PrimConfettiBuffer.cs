﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F49 RID: 3913
	public class PrimConfettiBuffer : MonoBehaviour
	{
		// Token: 0x06005107 RID: 20743 RVA: 0x0016AF65 File Offset: 0x00169365
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x06005108 RID: 20744 RVA: 0x0016AF8C File Offset: 0x0016938C
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				IEnumerator enumerator = this.m_ConfettiBufferList.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						PrimConfettiBuffer.ConfettiBuffer confettiBuffer = (PrimConfettiBuffer.ConfettiBuffer)obj;
						this.m_EffectMeshBuffer.RemoveBuffer(confettiBuffer.meshBuffer);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x06005109 RID: 20745 RVA: 0x0016B020 File Offset: 0x00169420
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x0600510A RID: 20746 RVA: 0x0016B028 File Offset: 0x00169428
		internal void AddActiveBufferList(PrimConfettiBuffer.ConfettiBuffer meshBuffer)
		{
			this.m_activeConfettiBufferList.Add(meshBuffer);
		}

		// Token: 0x0600510B RID: 20747 RVA: 0x0016B037 File Offset: 0x00169437
		internal void RemoveActiveBufferList(PrimConfettiBuffer.ConfettiBuffer meshBuffer)
		{
			this.m_activeConfettiBufferList.Remove(meshBuffer);
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x0600510D RID: 20749 RVA: 0x0016B04E File Offset: 0x0016944E
		// (set) Token: 0x0600510C RID: 20748 RVA: 0x0016B045 File Offset: 0x00169445
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x0600510F RID: 20751 RVA: 0x0016B05F File Offset: 0x0016945F
		// (set) Token: 0x0600510E RID: 20750 RVA: 0x0016B056 File Offset: 0x00169456
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06005111 RID: 20753 RVA: 0x0016B070 File Offset: 0x00169470
		// (set) Token: 0x06005110 RID: 20752 RVA: 0x0016B067 File Offset: 0x00169467
		public bool bufferChangeFlg { get; set; }

		// Token: 0x06005112 RID: 20754 RVA: 0x0016B078 File Offset: 0x00169478
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x06005113 RID: 20755 RVA: 0x0016B080 File Offset: 0x00169480
		private void Setup()
		{
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
				this.m_MeshBufferIsSelf = true;
			}
		}

		// Token: 0x06005114 RID: 20756 RVA: 0x0016B0D0 File Offset: 0x001694D0
		public virtual PrimConfettiBuffer.ConfettiBuffer AddBuffer(int confettiNum)
		{
			this.Setup();
			PrimConfettiBuffer.ConfettiBuffer confettiBuffer = new PrimConfettiBuffer.ConfettiBuffer();
			confettiBuffer.positions = new Vector3[confettiNum];
			confettiBuffer.sizes = new Vector2[confettiNum];
			confettiBuffer.rots = new Vector3[confettiNum];
			confettiBuffer.UVs = new Rect[confettiNum];
			confettiBuffer.colors = new Color[confettiNum];
			confettiBuffer.confettiNum = confettiNum;
			confettiBuffer.parent = this;
			confettiBuffer.enabled = false;
			this.m_ConfettiBufferList.Add(confettiBuffer);
			confettiBuffer.UpdatePositions();
			confettiBuffer.UpdateSizes();
			confettiBuffer.UpdateRots();
			confettiBuffer.UpdateUVs();
			confettiBuffer.UpdateColors();
			this.bufferChangeFlg = true;
			confettiBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(confettiNum * 4, confettiNum * 4);
			for (int i = 0; i < confettiNum; i++)
			{
				confettiBuffer.meshBuffer.indices[i * 4] = i * 4;
				confettiBuffer.meshBuffer.indices[i * 4 + 1] = i * 4 + 3;
				confettiBuffer.meshBuffer.indices[i * 4 + 2] = i * 4 + 2;
				confettiBuffer.meshBuffer.indices[i * 4 + 3] = i * 4 + 1;
			}
			confettiBuffer.meshBuffer.UpdateIndices();
			return confettiBuffer;
		}

		// Token: 0x06005115 RID: 20757 RVA: 0x0016B1F8 File Offset: 0x001695F8
		public virtual void RemoveBuffer(PrimConfettiBuffer.ConfettiBuffer confettiBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			confettiBuffer.enabled = false;
			int num = this.m_ConfettiBufferList.IndexOf(confettiBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(confettiBuffer.meshBuffer);
				confettiBuffer.meshBuffer = null;
				this.m_ConfettiBufferList.RemoveAt(num);
			}
		}

		// Token: 0x06005116 RID: 20758 RVA: 0x0016B258 File Offset: 0x00169658
		private void UpdateMesh()
		{
			if (this.m_ConfettiBufferList.Count == 0)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < this.m_ConfettiBufferList.Count; i++)
			{
				PrimConfettiBuffer.ConfettiBuffer confettiBuffer = this.m_ConfettiBufferList[i] as PrimConfettiBuffer.ConfettiBuffer;
				if (confettiBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = confettiBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (confettiBuffer.enabled)
						{
							for (int j = 0; j < confettiBuffer.confettiNum; j++)
							{
								if (confettiBuffer.isDirtyPositions || confettiBuffer.isDirtySizes || confettiBuffer.isDirtyRots)
								{
									meshBuffer.vertices[j * 4].x = confettiBuffer.sizes[j].x * -0.5f;
									meshBuffer.vertices[j * 4].y = confettiBuffer.sizes[j].y * -0.5f;
									meshBuffer.vertices[j * 4].z = 0f;
									meshBuffer.vertices[j * 4 + 1].x = confettiBuffer.sizes[j].x * 0.5f;
									meshBuffer.vertices[j * 4 + 1].y = confettiBuffer.sizes[j].y * -0.5f;
									meshBuffer.vertices[j * 4 + 1].z = 0f;
									meshBuffer.vertices[j * 4 + 2].x = confettiBuffer.sizes[j].x * 0.5f;
									meshBuffer.vertices[j * 4 + 2].y = confettiBuffer.sizes[j].y * 0.5f;
									meshBuffer.vertices[j * 4 + 2].z = 0f;
									meshBuffer.vertices[j * 4 + 3].x = confettiBuffer.sizes[j].x * -0.5f;
									meshBuffer.vertices[j * 4 + 3].y = confettiBuffer.sizes[j].y * 0.5f;
									meshBuffer.vertices[j * 4 + 3].z = 0f;
									Quaternion rotation = Quaternion.Euler(confettiBuffer.rots[j]);
									meshBuffer.vertices[j * 4] = rotation * meshBuffer.vertices[j * 4] + confettiBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 1] = rotation * meshBuffer.vertices[j * 4 + 1] + confettiBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 2] = rotation * meshBuffer.vertices[j * 4 + 2] + confettiBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 3] = rotation * meshBuffer.vertices[j * 4 + 3] + confettiBuffer.positions[j];
								}
								if (confettiBuffer.isDirtyUVs)
								{
									meshBuffer.UVs[j * 4].x = confettiBuffer.UVs[j].x;
									meshBuffer.UVs[j * 4].y = confettiBuffer.UVs[j].y;
									meshBuffer.UVs[j * 4 + 1].x = confettiBuffer.UVs[j].x + confettiBuffer.UVs[j].width;
									meshBuffer.UVs[j * 4 + 1].y = confettiBuffer.UVs[j].y;
									meshBuffer.UVs[j * 4 + 2].x = confettiBuffer.UVs[j].x + confettiBuffer.UVs[j].width;
									meshBuffer.UVs[j * 4 + 2].y = confettiBuffer.UVs[j].y + confettiBuffer.UVs[j].height;
									meshBuffer.UVs[j * 4 + 3].x = confettiBuffer.UVs[j].x;
									meshBuffer.UVs[j * 4 + 3].y = confettiBuffer.UVs[j].y + confettiBuffer.UVs[j].height;
								}
								if (confettiBuffer.isDirtyColors)
								{
									meshBuffer.colors[j * 4] = confettiBuffer.colors[j];
									meshBuffer.colors[j * 4 + 1] = confettiBuffer.colors[j];
									meshBuffer.colors[j * 4 + 2] = confettiBuffer.colors[j];
									meshBuffer.colors[j * 4 + 3] = confettiBuffer.colors[j];
								}
							}
							if (confettiBuffer.isDirtyPositions || confettiBuffer.isDirtySizes || confettiBuffer.isDirtyRots)
							{
								meshBuffer.UpdateVertices();
								confettiBuffer.isDirtyPositions = false;
								confettiBuffer.isDirtySizes = false;
								confettiBuffer.isDirtyRots = false;
							}
							if (confettiBuffer.isDirtyUVs)
							{
								meshBuffer.UpdateUVs();
								confettiBuffer.isDirtyUVs = false;
							}
							if (confettiBuffer.isDirtyColors)
							{
								meshBuffer.UpdateColors();
								confettiBuffer.isDirtyColors = false;
							}
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x06005117 RID: 20759 RVA: 0x0016B90B File Offset: 0x00169D0B
		private void Update()
		{
			this.UpdateMesh();
		}

		// Token: 0x06005118 RID: 20760 RVA: 0x0016B913 File Offset: 0x00169D13
		private void OnWillRenderObject()
		{
		}

		// Token: 0x04005037 RID: 20535
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04005038 RID: 20536
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04005039 RID: 20537
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x0400503A RID: 20538
		protected ArrayList m_ConfettiBufferList = new ArrayList();

		// Token: 0x0400503B RID: 20539
		protected ArrayList m_activeConfettiBufferList = new ArrayList(32);

		// Token: 0x02000F4A RID: 3914
		public class ConfettiBuffer
		{
			// Token: 0x0600511A RID: 20762 RVA: 0x0016B91D File Offset: 0x00169D1D
			public void Remove()
			{
			}

			// Token: 0x0600511B RID: 20763 RVA: 0x0016B91F File Offset: 0x00169D1F
			public void UpdatePositions()
			{
				this.isDirtyPositions = true;
			}

			// Token: 0x0600511C RID: 20764 RVA: 0x0016B928 File Offset: 0x00169D28
			public void UpdateSizes()
			{
				this.isDirtySizes = true;
			}

			// Token: 0x0600511D RID: 20765 RVA: 0x0016B931 File Offset: 0x00169D31
			public void UpdateRots()
			{
				this.isDirtyRots = true;
			}

			// Token: 0x0600511E RID: 20766 RVA: 0x0016B93A File Offset: 0x00169D3A
			public void UpdateUVs()
			{
				this.isDirtyUVs = true;
			}

			// Token: 0x0600511F RID: 20767 RVA: 0x0016B943 File Offset: 0x00169D43
			public void UpdateColors()
			{
				this.isDirtyColors = true;
			}

			// Token: 0x06005120 RID: 20768 RVA: 0x0016B94C File Offset: 0x00169D4C
			public void Minimize()
			{
			}

			// Token: 0x1700056A RID: 1386
			// (get) Token: 0x06005121 RID: 20769 RVA: 0x0016B94E File Offset: 0x00169D4E
			// (set) Token: 0x06005122 RID: 20770 RVA: 0x0016B958 File Offset: 0x00169D58
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateSizes();
					this.UpdateRots();
					this.UpdateColors();
				}
			}

			// Token: 0x0400503E RID: 20542
			internal PrimConfettiBuffer parent;

			// Token: 0x0400503F RID: 20543
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04005040 RID: 20544
			public Vector3[] positions;

			// Token: 0x04005041 RID: 20545
			public Vector2[] sizes;

			// Token: 0x04005042 RID: 20546
			public Vector3[] rots;

			// Token: 0x04005043 RID: 20547
			public Rect[] UVs;

			// Token: 0x04005044 RID: 20548
			public Color[] colors;

			// Token: 0x04005045 RID: 20549
			public int confettiNum;

			// Token: 0x04005046 RID: 20550
			internal bool isDirtyPositions;

			// Token: 0x04005047 RID: 20551
			internal bool isDirtySizes;

			// Token: 0x04005048 RID: 20552
			internal bool isDirtyRots;

			// Token: 0x04005049 RID: 20553
			internal bool isDirtyUVs;

			// Token: 0x0400504A RID: 20554
			internal bool isDirtyColors;

			// Token: 0x0400504B RID: 20555
			private bool m_enabled;
		}
	}
}
