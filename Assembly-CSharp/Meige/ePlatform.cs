﻿using System;

namespace Meige
{
	// Token: 0x02000F7D RID: 3965
	public enum ePlatform
	{
		// Token: 0x040052B5 RID: 21173
		ePlatform_Invalid = -1,
		// Token: 0x040052B6 RID: 21174
		ePlatform_WIN,
		// Token: 0x040052B7 RID: 21175
		ePlatform_VITA,
		// Token: 0x040052B8 RID: 21176
		ePlatform_PSP,
		// Token: 0x040052B9 RID: 21177
		ePlatform_Cnt
	}
}
