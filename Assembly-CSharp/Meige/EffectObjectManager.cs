﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EA0 RID: 3744
	public class EffectObjectManager : MonoBehaviour
	{
		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06004E32 RID: 20018 RVA: 0x0015BE69 File Offset: 0x0015A269
		public static EffectObjectManager Instance
		{
			get
			{
				return EffectObjectManager.m_Instance;
			}
		}

		// Token: 0x06004E33 RID: 20019 RVA: 0x0015BE70 File Offset: 0x0015A270
		private void Awake()
		{
			if (EffectObjectManager.m_Instance == null)
			{
				EffectObjectManager.m_Instance = this;
				this.m_ThisTransform = base.transform;
				this.m_ThisTransform.position = Vector3.zero;
				this.m_ThisTransform.eulerAngles = Vector3.zero;
				this.m_ThisTransform.localScale = Vector3.one;
				if (this.m_IsDontDestroyOnLoad)
				{
					UnityEngine.Object.DontDestroyOnLoad(this);
				}
			}
			else
			{
				Helper.DestroyAll(base.gameObject);
			}
		}

		// Token: 0x06004E34 RID: 20020 RVA: 0x0015BEF0 File Offset: 0x0015A2F0
		private void Update()
		{
			foreach (EffectMeshBuffer effectMeshBuffer in this.m_MeshBufferList)
			{
			}
		}

		// Token: 0x06004E35 RID: 20021 RVA: 0x0015BF48 File Offset: 0x0015A348
		public bool RegisterMeshBufferComponent(EffectMeshBuffer meshBuffer)
		{
			this.m_MeshBufferList.Add(meshBuffer);
			return true;
		}

		// Token: 0x06004E36 RID: 20022 RVA: 0x0015BF58 File Offset: 0x0015A358
		public bool RemoveMeshBufferComponent(EffectMeshBuffer meshBuffer)
		{
			int num = this.m_MeshBufferList.IndexOf(meshBuffer);
			if (num < 0)
			{
				return false;
			}
			this.m_MeshBufferList.RemoveAt(num);
			return true;
		}

		// Token: 0x06004E37 RID: 20023 RVA: 0x0015BF88 File Offset: 0x0015A388
		public bool RegisterRendererComponent(EffectRenderer renderer)
		{
			this.m_RendererList.Add(renderer);
			return true;
		}

		// Token: 0x06004E38 RID: 20024 RVA: 0x0015BF98 File Offset: 0x0015A398
		public bool RemoveRendererComponent(EffectRenderer renderer)
		{
			int num = this.m_RendererList.IndexOf(renderer);
			if (num < 0)
			{
				return false;
			}
			this.m_RendererList.RemoveAt(num);
			return true;
		}

		// Token: 0x04004E52 RID: 20050
		private static EffectObjectManager m_Instance;

		// Token: 0x04004E53 RID: 20051
		private List<EffectMeshBuffer> m_MeshBufferList = new List<EffectMeshBuffer>();

		// Token: 0x04004E54 RID: 20052
		private List<EffectRenderer> m_RendererList = new List<EffectRenderer>();

		// Token: 0x04004E55 RID: 20053
		[NonSerialized]
		public Transform m_ThisTransform;

		// Token: 0x04004E56 RID: 20054
		[SerializeField]
		private bool m_IsDontDestroyOnLoad = true;
	}
}
