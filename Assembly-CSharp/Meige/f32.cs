﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F09 RID: 3849
	[Serializable]
	public struct f32 : IComparable, IComparable<f32>, IOperatorFast<f32>
	{
		// Token: 0x06004F83 RID: 20355 RVA: 0x0015E47C File Offset: 0x0015C87C
		public f32(float value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004F84 RID: 20356 RVA: 0x0015E485 File Offset: 0x0015C885
		public f32(f32 value)
		{
			this.m_Value = value.value;
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06004F86 RID: 20358 RVA: 0x0015E49D File Offset: 0x0015C89D
		// (set) Token: 0x06004F85 RID: 20357 RVA: 0x0015E494 File Offset: 0x0015C894
		public float value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x06004F87 RID: 20359 RVA: 0x0015E4A5 File Offset: 0x0015C8A5
		public static implicit operator f32(float v)
		{
			return new f32(v);
		}

		// Token: 0x06004F88 RID: 20360 RVA: 0x0015E4AD File Offset: 0x0015C8AD
		public static implicit operator float(f32 v)
		{
			return v.value;
		}

		// Token: 0x06004F89 RID: 20361 RVA: 0x0015E4B6 File Offset: 0x0015C8B6
		public void Add(f32 a, f32 b)
		{
			this = a + b;
		}

		// Token: 0x06004F8A RID: 20362 RVA: 0x0015E4C5 File Offset: 0x0015C8C5
		public void Sub(f32 a, f32 b)
		{
			this = a - b;
		}

		// Token: 0x06004F8B RID: 20363 RVA: 0x0015E4D4 File Offset: 0x0015C8D4
		public void Mul(f32 a, f32 b)
		{
			this = a * b;
		}

		// Token: 0x06004F8C RID: 20364 RVA: 0x0015E4E3 File Offset: 0x0015C8E3
		public void Mul(f32 a, float b)
		{
			this = a * b;
		}

		// Token: 0x06004F8D RID: 20365 RVA: 0x0015E4F2 File Offset: 0x0015C8F2
		public void Div(f32 a, f32 b)
		{
			this = a / b;
		}

		// Token: 0x06004F8E RID: 20366 RVA: 0x0015E501 File Offset: 0x0015C901
		public void Add(f32 b)
		{
			this += b;
		}

		// Token: 0x06004F8F RID: 20367 RVA: 0x0015E515 File Offset: 0x0015C915
		public void Sub(f32 b)
		{
			this -= b;
		}

		// Token: 0x06004F90 RID: 20368 RVA: 0x0015E529 File Offset: 0x0015C929
		public void Mul(f32 b)
		{
			this *= b;
		}

		// Token: 0x06004F91 RID: 20369 RVA: 0x0015E53D File Offset: 0x0015C93D
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x06004F92 RID: 20370 RVA: 0x0015E551 File Offset: 0x0015C951
		public void Div(f32 b)
		{
			this *= b;
		}

		// Token: 0x06004F93 RID: 20371 RVA: 0x0015E565 File Offset: 0x0015C965
		public void SetRowValue(object o)
		{
			this.m_Value = (float)o;
		}

		// Token: 0x06004F94 RID: 20372 RVA: 0x0015E574 File Offset: 0x0015C974
		public int CompareTo(object obj)
		{
			float? num = obj as float?;
			if (num == null)
			{
				return 1;
			}
			return this.CompareTo(num);
		}

		// Token: 0x06004F95 RID: 20373 RVA: 0x0015E5AA File Offset: 0x0015C9AA
		public int CompareTo(f32 target)
		{
			if (target.value == this.m_Value)
			{
				return 0;
			}
			if (target.value < this.m_Value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06004F96 RID: 20374 RVA: 0x0015E5D8 File Offset: 0x0015C9D8
		public override bool Equals(object obj)
		{
			float? num = obj as float?;
			return num != null && this.Equals(num);
		}

		// Token: 0x06004F97 RID: 20375 RVA: 0x0015E614 File Offset: 0x0015CA14
		public bool Equals(float obj)
		{
			return this.m_Value == obj;
		}

		// Token: 0x06004F98 RID: 20376 RVA: 0x0015E61F File Offset: 0x0015CA1F
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06004F99 RID: 20377 RVA: 0x0015E632 File Offset: 0x0015CA32
		public static f32 operator +(f32 v0)
		{
			return v0;
		}

		// Token: 0x06004F9A RID: 20378 RVA: 0x0015E638 File Offset: 0x0015CA38
		public static f32 operator +(f32 v0, f32 v1)
		{
			f32 result;
			result.m_Value = v0.m_Value + v1.m_Value;
			return result;
		}

		// Token: 0x06004F9B RID: 20379 RVA: 0x0015E660 File Offset: 0x0015CA60
		public static f32 operator -(f32 v0)
		{
			f32 result;
			result.m_Value = -v0.m_Value;
			return result;
		}

		// Token: 0x06004F9C RID: 20380 RVA: 0x0015E680 File Offset: 0x0015CA80
		public static f32 operator -(f32 v0, f32 v1)
		{
			f32 result;
			result.m_Value = v0.m_Value - v1.m_Value;
			return result;
		}

		// Token: 0x06004F9D RID: 20381 RVA: 0x0015E6A8 File Offset: 0x0015CAA8
		public static f32 operator *(f32 v0, f32 v1)
		{
			f32 result;
			result.m_Value = v0.m_Value * v1.m_Value;
			return result;
		}

		// Token: 0x06004F9E RID: 20382 RVA: 0x0015E6D0 File Offset: 0x0015CAD0
		public static f32 operator *(f32 v0, float v1)
		{
			f32 result;
			result.m_Value = v0.m_Value * v1;
			return result;
		}

		// Token: 0x06004F9F RID: 20383 RVA: 0x0015E6F0 File Offset: 0x0015CAF0
		public static f32 operator /(f32 v0, f32 v1)
		{
			f32 result;
			result.m_Value = v0.m_Value / v1.m_Value;
			return result;
		}

		// Token: 0x06004FA0 RID: 20384 RVA: 0x0015E718 File Offset: 0x0015CB18
		public static f32 operator /(f32 v0, float v1)
		{
			f32 result;
			result.m_Value = v0.m_Value / v1;
			return result;
		}

		// Token: 0x06004FA1 RID: 20385 RVA: 0x0015E737 File Offset: 0x0015CB37
		public static bool operator ==(f32 v0, f32 v1)
		{
			return v0.m_Value == v1.m_Value;
		}

		// Token: 0x06004FA2 RID: 20386 RVA: 0x0015E749 File Offset: 0x0015CB49
		public static bool operator !=(f32 v0, f32 v1)
		{
			return v0.m_Value != v1.m_Value;
		}

		// Token: 0x04004EB2 RID: 20146
		[SerializeField]
		public float m_Value;
	}
}
