﻿using System;

namespace Meige
{
	// Token: 0x02000F9C RID: 3996
	public struct MFFChunk
	{
		// Token: 0x04005406 RID: 21510
		public uint m_HashKey;

		// Token: 0x04005407 RID: 21511
		public uint m_BodySize;

		// Token: 0x04005408 RID: 21512
		public uint m_OffsetToBody;

		// Token: 0x04005409 RID: 21513
		public byte m_Type;

		// Token: 0x0400540A RID: 21514
		public byte m_cPad;

		// Token: 0x0400540B RID: 21515
		public ushort m_ElementNum;
	}
}
