﻿using System;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace Meige.GenericOperator
{
	// Token: 0x02000EF5 RID: 3829
	public static class Operator<T>
	{
		// Token: 0x06004EE0 RID: 20192 RVA: 0x0015D459 File Offset: 0x0015B859
		public static Func<T, T, T> Lambda(Func<ParameterExpression, ParameterExpression, BinaryExpression> op)
		{
			return Expression.Lambda<Func<T, T, T>>(op(Operator<T>.x, Operator<T>.y), new ParameterExpression[]
			{
				Operator<T>.x,
				Operator<T>.y
			}).Compile();
		}

		// Token: 0x06004EE1 RID: 20193 RVA: 0x0015D48B File Offset: 0x0015B88B
		public static Func<T, float, T> LambdaFloat(Func<ParameterExpression, ParameterExpression, BinaryExpression> op)
		{
			return Expression.Lambda<Func<T, float, T>>(op(Operator<T>.x, Operator<T>.y), new ParameterExpression[]
			{
				Operator<T>.x,
				Operator<T>.y
			}).Compile();
		}

		// Token: 0x06004EE2 RID: 20194 RVA: 0x0015D4C0 File Offset: 0x0015B8C0
		// Note: this type is marked as 'beforefieldinit'.
		static Operator()
		{
			if (Operator<T>.<>f__mg$cache0 == null)
			{
				Operator<T>.<>f__mg$cache0 = new Func<ParameterExpression, ParameterExpression, BinaryExpression>(Expression.Add);
			}
			Operator<T>.Add = Operator<T>.Lambda(Operator<T>.<>f__mg$cache0);
			if (Operator<T>.<>f__mg$cache1 == null)
			{
				Operator<T>.<>f__mg$cache1 = new Func<ParameterExpression, ParameterExpression, BinaryExpression>(Expression.Subtract);
			}
			Operator<T>.Sub = Operator<T>.Lambda(Operator<T>.<>f__mg$cache1);
			if (Operator<T>.<>f__mg$cache2 == null)
			{
				Operator<T>.<>f__mg$cache2 = new Func<ParameterExpression, ParameterExpression, BinaryExpression>(Expression.Multiply);
			}
			Operator<T>.Mul = Operator<T>.Lambda(Operator<T>.<>f__mg$cache2);
			if (Operator<T>.<>f__mg$cache3 == null)
			{
				Operator<T>.<>f__mg$cache3 = new Func<ParameterExpression, ParameterExpression, BinaryExpression>(Expression.Divide);
			}
			Operator<T>.Div = Operator<T>.Lambda(Operator<T>.<>f__mg$cache3);
			if (Operator<T>.<>f__mg$cache4 == null)
			{
				Operator<T>.<>f__mg$cache4 = new Func<ParameterExpression, ParameterExpression, BinaryExpression>(Expression.Multiply);
			}
			Operator<T>.MulFloat = Operator<T>.LambdaFloat(Operator<T>.<>f__mg$cache4);
			Operator<T>.ProductSum = Expression.Lambda<Func<T, T, T, T, T>>(Expression.Add(Expression.Multiply(Operator<T>.x, Operator<T>.y), Expression.Multiply(Operator<T>.z, Operator<T>.w)), new ParameterExpression[]
			{
				Operator<T>.x,
				Operator<T>.y,
				Operator<T>.z,
				Operator<T>.w
			}).Compile();
			Operator<T>.ProductDifference = Expression.Lambda<Func<T, T, T, T, T>>(Expression.Subtract(Expression.Multiply(Operator<T>.x, Operator<T>.y), Expression.Multiply(Operator<T>.z, Operator<T>.w)), new ParameterExpression[]
			{
				Operator<T>.x,
				Operator<T>.y,
				Operator<T>.z,
				Operator<T>.w
			}).Compile();
		}

		// Token: 0x04004E99 RID: 20121
		private static readonly ParameterExpression x = Expression.Parameter(typeof(T), "x");

		// Token: 0x04004E9A RID: 20122
		private static readonly ParameterExpression y = Expression.Parameter(typeof(T), "y");

		// Token: 0x04004E9B RID: 20123
		private static readonly ParameterExpression z = Expression.Parameter(typeof(T), "z");

		// Token: 0x04004E9C RID: 20124
		private static readonly ParameterExpression w = Expression.Parameter(typeof(T), "w");

		// Token: 0x04004E9D RID: 20125
		public static readonly Func<T, T, T> Add;

		// Token: 0x04004E9E RID: 20126
		public static readonly Func<T, T, T> Sub;

		// Token: 0x04004E9F RID: 20127
		public static readonly Func<T, T, T> Mul;

		// Token: 0x04004EA0 RID: 20128
		public static readonly Func<T, T, T> Div;

		// Token: 0x04004EA1 RID: 20129
		public static readonly Func<T, float, T> MulFloat;

		// Token: 0x04004EA2 RID: 20130
		public static readonly Func<T, T, T, T, T> ProductSum;

		// Token: 0x04004EA3 RID: 20131
		public static readonly Func<T, T, T, T, T> ProductDifference;

		// Token: 0x04004EA4 RID: 20132
		[CompilerGenerated]
		private static Func<ParameterExpression, ParameterExpression, BinaryExpression> <>f__mg$cache0;

		// Token: 0x04004EA5 RID: 20133
		[CompilerGenerated]
		private static Func<ParameterExpression, ParameterExpression, BinaryExpression> <>f__mg$cache1;

		// Token: 0x04004EA6 RID: 20134
		[CompilerGenerated]
		private static Func<ParameterExpression, ParameterExpression, BinaryExpression> <>f__mg$cache2;

		// Token: 0x04004EA7 RID: 20135
		[CompilerGenerated]
		private static Func<ParameterExpression, ParameterExpression, BinaryExpression> <>f__mg$cache3;

		// Token: 0x04004EA8 RID: 20136
		[CompilerGenerated]
		private static Func<ParameterExpression, ParameterExpression, BinaryExpression> <>f__mg$cache4;
	}
}
