﻿using System;

namespace Meige.GenericOperator
{
	// Token: 0x02000EF6 RID: 3830
	public interface IOperatorFast<T>
	{
		// Token: 0x06004EE3 RID: 20195
		void Add(T a, T b);

		// Token: 0x06004EE4 RID: 20196
		void Sub(T a, T b);

		// Token: 0x06004EE5 RID: 20197
		void Mul(T a, T b);

		// Token: 0x06004EE6 RID: 20198
		void Mul(T a, float b);

		// Token: 0x06004EE7 RID: 20199
		void Div(T a, T b);

		// Token: 0x06004EE8 RID: 20200
		void Add(T b);

		// Token: 0x06004EE9 RID: 20201
		void Sub(T b);

		// Token: 0x06004EEA RID: 20202
		void Mul(T b);

		// Token: 0x06004EEB RID: 20203
		void Mul(float b);

		// Token: 0x06004EEC RID: 20204
		void Div(T b);

		// Token: 0x06004EED RID: 20205
		void SetRowValue(object o);
	}
}
