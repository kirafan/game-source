﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EF9 RID: 3833
	[Serializable]
	public class RangePare<Value> where Value : struct, IOperatorFast<Value>, IComparable<Value>
	{
		// Token: 0x06004EFF RID: 20223 RVA: 0x0015DA30 File Offset: 0x0015BE30
		public RangePare()
		{
		}

		// Token: 0x06004F00 RID: 20224 RVA: 0x0015DA38 File Offset: 0x0015BE38
		public RangePare(Value min, Value max)
		{
			this.m_Min = min;
			this.m_Max = max;
		}

		// Token: 0x06004F01 RID: 20225 RVA: 0x0015DA4E File Offset: 0x0015BE4E
		public RangePare(RangePare<Value> value)
		{
			this.m_Min = value.min;
			this.m_Max = value.max;
		}

		// Token: 0x1700053C RID: 1340
		public Value this[int idx]
		{
			get
			{
				if (idx == 0)
				{
					return this.m_Min;
				}
				if (idx == 1)
				{
					return this.m_Max;
				}
				return default(Value);
			}
			set
			{
				if (idx == 0)
				{
					this.m_Min = value;
				}
				else if (idx == 1)
				{
					this.m_Max = value;
				}
			}
		}

		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06004F05 RID: 20229 RVA: 0x0015DACA File Offset: 0x0015BECA
		// (set) Token: 0x06004F04 RID: 20228 RVA: 0x0015DAC1 File Offset: 0x0015BEC1
		public Value min
		{
			get
			{
				return this.m_Min;
			}
			set
			{
				this.m_Min = value;
			}
		}

		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06004F07 RID: 20231 RVA: 0x0015DADB File Offset: 0x0015BEDB
		// (set) Token: 0x06004F06 RID: 20230 RVA: 0x0015DAD2 File Offset: 0x0015BED2
		public Value max
		{
			get
			{
				return this.m_Max;
			}
			set
			{
				this.m_Max = value;
			}
		}

		// Token: 0x06004F08 RID: 20232 RVA: 0x0015DAE3 File Offset: 0x0015BEE3
		public void Set(RangePare<Value> v)
		{
			this.m_Min = v.min;
			this.m_Max = v.max;
		}

		// Token: 0x06004F09 RID: 20233 RVA: 0x0015DAFD File Offset: 0x0015BEFD
		public void SetValue(Value min, Value max)
		{
			this.m_Min = min;
			this.m_Max = max;
		}

		// Token: 0x06004F0A RID: 20234 RVA: 0x0015DB0D File Offset: 0x0015BF0D
		public void SetValueRow(object min, object max)
		{
			this.m_Min.SetRowValue(min);
			this.m_Max.SetRowValue(max);
		}

		// Token: 0x06004F0B RID: 20235 RVA: 0x0015DB34 File Offset: 0x0015BF34
		public static RangePare<Value> Clone(RangePare<Value> target)
		{
			return new RangePare<Value>
			{
				m_Min = target.m_Min,
				m_Max = target.m_Max
			};
		}

		// Token: 0x06004F0C RID: 20236 RVA: 0x0015DB60 File Offset: 0x0015BF60
		public virtual RangePare<Value> Clone()
		{
			return RangePare<Value>.Clone(this);
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06004F0D RID: 20237 RVA: 0x0015DB68 File Offset: 0x0015BF68
		public Value ValueMin
		{
			get
			{
				return (this.m_Min.CompareTo(this.m_Max) < 0) ? this.m_Min : this.m_Max;
			}
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06004F0E RID: 20238 RVA: 0x0015DB98 File Offset: 0x0015BF98
		public Value ValueMax
		{
			get
			{
				return (this.m_Min.CompareTo(this.m_Max) > 0) ? this.m_Min : this.m_Max;
			}
		}

		// Token: 0x06004F0F RID: 20239 RVA: 0x0015DBC8 File Offset: 0x0015BFC8
		public Value Random()
		{
			return this.Lerp(UnityEngine.Random.Range(0f, 1f));
		}

		// Token: 0x06004F10 RID: 20240 RVA: 0x0015DBE0 File Offset: 0x0015BFE0
		public Value Lerp(float rate)
		{
			Value value = default(Value);
			value.Sub(this.max, this.min);
			value.Mul(value, rate);
			value.Add(value, this.min);
			return value;
		}

		// Token: 0x06004F11 RID: 20241 RVA: 0x0015DC34 File Offset: 0x0015C034
		public Value ReverseLerp(float rate)
		{
			Value value = default(Value);
			value.Sub(this.min, this.max);
			value.Mul(value, rate);
			value.Add(value, this.max);
			return value;
		}

		// Token: 0x04004EAD RID: 20141
		[SerializeField]
		public Value m_Min;

		// Token: 0x04004EAE RID: 20142
		[SerializeField]
		public Value m_Max;
	}
}
