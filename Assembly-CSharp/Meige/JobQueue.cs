﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Meige
{
	// Token: 0x02000FDC RID: 4060
	public class JobQueue
	{
		// Token: 0x0600547D RID: 21629 RVA: 0x0017ABCE File Offset: 0x00178FCE
		public static void Create()
		{
			if (JobQueue.m_Instance == null)
			{
				JobQueue.m_Instance = new JobQueue();
			}
			JobQueue.m_Instance.Init();
		}

		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x0600547E RID: 21630 RVA: 0x0017ABF0 File Offset: 0x00178FF0
		public static JobQueue InstanceLock
		{
			get
			{
				object @lock = JobQueue.m_Lock;
				JobQueue instance;
				lock (@lock)
				{
					instance = JobQueue.m_Instance;
				}
				return instance;
			}
		}

		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x0600547F RID: 21631 RVA: 0x0017AC2C File Offset: 0x0017902C
		public static JobQueue Instance
		{
			get
			{
				return JobQueue.m_Instance;
			}
		}

		// Token: 0x06005480 RID: 21632 RVA: 0x0017AC33 File Offset: 0x00179033
		public static void Enqueue(Job job)
		{
			if (JobQueue.Instance == null)
			{
				return;
			}
			JobQueue.Instance.EnqueueJob(job);
		}

		// Token: 0x06005481 RID: 21633 RVA: 0x0017AC4B File Offset: 0x0017904B
		public static Job Dequeue()
		{
			if (JobQueue.Instance == null)
			{
				return null;
			}
			return JobQueue.Instance.DequeueJob();
		}

		// Token: 0x06005482 RID: 21634 RVA: 0x0017AC63 File Offset: 0x00179063
		public static void Done(Job job)
		{
			if (JobQueue.Instance == null)
			{
				return;
			}
			JobQueue.Instance.DoneJob(job);
		}

		// Token: 0x06005483 RID: 21635 RVA: 0x0017AC7B File Offset: 0x0017907B
		public static void Release()
		{
			if (JobQueue.Instance == null)
			{
				return;
			}
			JobQueue.Instance.ReleaseJobQueue();
		}

		// Token: 0x06005484 RID: 21636 RVA: 0x0017AC94 File Offset: 0x00179094
		private void Init()
		{
			this.m_workerThreads = null;
			this.m_event = null;
			this.m_exit = false;
			this.m_barrierJobFlagMask = 0;
			this.m_doingJobCounter = new int[16];
			int num = 2;
			if (num > 0)
			{
				this.m_event = new AutoResetEvent(false);
				this.m_jobs = new List<Job>();
				this.m_workerThreads = new WorkerThread[num];
				for (int i = 0; i < num; i++)
				{
					this.m_workerThreads[i] = new WorkerThread();
				}
			}
		}

		// Token: 0x06005485 RID: 21637 RVA: 0x0017AD18 File Offset: 0x00179118
		public void ReleaseJobQueue()
		{
			this.m_exit = true;
			if (this.m_workerThreads != null)
			{
				if (this.m_event != null)
				{
					this.m_event.Set();
					this.m_event.Close();
					this.m_event = null;
				}
				foreach (WorkerThread workerThread in this.m_workerThreads)
				{
					workerThread.Release();
				}
				this.m_workerThreads = null;
				foreach (Job job in this.m_jobs)
				{
					job.Invoke();
				}
				this.m_jobs.Clear();
			}
		}

		// Token: 0x06005486 RID: 21638 RVA: 0x0017ADE8 File Offset: 0x001791E8
		private void WakeUp()
		{
			for (int i = 0; i < 2; i++)
			{
				this.m_workerThreads[i].Signal();
			}
		}

		// Token: 0x06005487 RID: 21639 RVA: 0x0017AE14 File Offset: 0x00179214
		private void EnqueueJob(Job job)
		{
			if (this.m_workerThreads == null || this.m_exit)
			{
				job.Invoke();
				return;
			}
			while (100 <= this.m_jobs.Count)
			{
				this.m_event.WaitOne();
			}
			object jobs = this.m_jobs;
			lock (jobs)
			{
				this.m_jobs.Add(job);
			}
			this.WakeUp();
		}

		// Token: 0x06005488 RID: 21640 RVA: 0x0017AE9C File Offset: 0x0017929C
		private Job DequeueJob()
		{
			Job job = null;
			object jobs = this.m_jobs;
			lock (jobs)
			{
				foreach (Job job2 in this.m_jobs)
				{
					int jobFlagMask = job2.GetJobFlagMask();
					if (jobFlagMask == 0 || (this.m_barrierJobFlagMask & jobFlagMask) == 0)
					{
						job = job2;
						break;
					}
				}
			}
			if (job != null)
			{
				int jobFlagMask2 = job.GetJobFlagMask();
				int barrierJobFlagMask = this.m_barrierJobFlagMask;
				int value = this.m_barrierJobFlagMask | jobFlagMask2;
				Interlocked.CompareExchange(ref this.m_barrierJobFlagMask, value, barrierJobFlagMask);
				for (int i = 0; i < 16; i++)
				{
					if ((jobFlagMask2 & 1 << i) != 0)
					{
						this.m_doingJobCounter[i]++;
					}
				}
				object jobs2 = this.m_jobs;
				lock (jobs2)
				{
					this.m_jobs.Remove(job);
				}
				this.m_event.Set();
			}
			return job;
		}

		// Token: 0x06005489 RID: 21641 RVA: 0x0017AFEC File Offset: 0x001793EC
		private void DoneJob(Job job)
		{
			int jobFlagMask = job.GetJobFlagMask();
			if (jobFlagMask != 0)
			{
				for (int i = 0; i < 16; i++)
				{
					if ((jobFlagMask & 1 << i) != 0)
					{
						this.m_doingJobCounter[i]--;
						if (this.m_doingJobCounter[i] <= 0 && (this.m_barrierJobFlagMask & 1 << i) != 0)
						{
							this.m_barrierJobFlagMask &= ~(1 << i);
						}
					}
				}
				this.WakeUp();
			}
		}

		// Token: 0x0400559B RID: 21915
		private static JobQueue m_Instance = null;

		// Token: 0x0400559C RID: 21916
		private static readonly object m_Lock = new object();

		// Token: 0x0400559D RID: 21917
		private WorkerThread[] m_workerThreads;

		// Token: 0x0400559E RID: 21918
		private AutoResetEvent m_event;

		// Token: 0x0400559F RID: 21919
		private List<Job> m_jobs;

		// Token: 0x040055A0 RID: 21920
		private bool m_exit;

		// Token: 0x040055A1 RID: 21921
		private int m_barrierJobFlagMask;

		// Token: 0x040055A2 RID: 21922
		private int[] m_doingJobCounter;
	}
}
