﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F43 RID: 3907
	public class PrimBillboardBuffer : MonoBehaviour
	{
		// Token: 0x060050CD RID: 20685 RVA: 0x0016960B File Offset: 0x00167A0B
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x060050CE RID: 20686 RVA: 0x00169634 File Offset: 0x00167A34
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer.RemoveHook_OnWillRender(new EffectMeshBuffer.HookUpdate(this.UpdateMesh));
				foreach (PrimBillboardBuffer.BillboardBuffer billboardBuffer in this.m_BillboardBufferList)
				{
					this.m_EffectMeshBuffer.RemoveBuffer(billboardBuffer.meshBuffer);
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x060050CF RID: 20687 RVA: 0x001696D4 File Offset: 0x00167AD4
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x060050D0 RID: 20688 RVA: 0x001696DC File Offset: 0x00167ADC
		internal void AddActiveBufferList(PrimBillboardBuffer.BillboardBuffer meshBuffer)
		{
			this.m_activeBillboardBufferList.Add(meshBuffer);
		}

		// Token: 0x060050D1 RID: 20689 RVA: 0x001696EA File Offset: 0x00167AEA
		internal void RemoveActiveBufferList(PrimBillboardBuffer.BillboardBuffer meshBuffer)
		{
			this.m_activeBillboardBufferList.Remove(meshBuffer);
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x060050D3 RID: 20691 RVA: 0x00169702 File Offset: 0x00167B02
		// (set) Token: 0x060050D2 RID: 20690 RVA: 0x001696F9 File Offset: 0x00167AF9
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x060050D5 RID: 20693 RVA: 0x00169713 File Offset: 0x00167B13
		// (set) Token: 0x060050D4 RID: 20692 RVA: 0x0016970A File Offset: 0x00167B0A
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x060050D7 RID: 20695 RVA: 0x00169724 File Offset: 0x00167B24
		// (set) Token: 0x060050D6 RID: 20694 RVA: 0x0016971B File Offset: 0x00167B1B
		public bool bufferChangeFlg { get; set; }

		// Token: 0x060050D8 RID: 20696 RVA: 0x0016972C File Offset: 0x00167B2C
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x060050D9 RID: 20697 RVA: 0x00169734 File Offset: 0x00167B34
		private void Setup()
		{
			if (this.m_SetupIsEnd)
			{
				return;
			}
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
				this.m_MeshBufferIsSelf = true;
			}
			if (this.m_EffectMeshBuffer != null)
			{
				this.m_EffectMeshBuffer.AddHook_OnWillRender(new EffectMeshBuffer.HookUpdate(this.UpdateMesh));
			}
			this.m_SetupIsEnd = true;
		}

		// Token: 0x060050DA RID: 20698 RVA: 0x001697BC File Offset: 0x00167BBC
		public virtual PrimBillboardBuffer.BillboardBuffer AddBuffer(int billboardNum, bool isShareVetexColor = true)
		{
			this.Setup();
			PrimBillboardBuffer.BillboardBuffer billboardBuffer = new PrimBillboardBuffer.BillboardBuffer();
			billboardBuffer.positions = new Vector3[billboardNum];
			billboardBuffer.sizes = new Vector2[billboardNum];
			billboardBuffer.rots = new float[billboardNum];
			billboardBuffer.UVs = new Rect[billboardNum];
			billboardBuffer.colors = new Color[billboardNum];
			if (this.m_EffectMeshBuffer.isUsableSubUVsFix)
			{
				billboardBuffer.SubUVs = new Rect[billboardNum];
			}
			if (isShareVetexColor)
			{
				billboardBuffer.colors1 = billboardBuffer.colors;
				billboardBuffer.colors2 = billboardBuffer.colors;
				billboardBuffer.colors3 = billboardBuffer.colors;
			}
			else
			{
				billboardBuffer.colors1 = new Color[billboardNum];
				billboardBuffer.colors2 = new Color[billboardNum];
				billboardBuffer.colors3 = new Color[billboardNum];
			}
			billboardBuffer.billboardNum = billboardNum;
			billboardBuffer.parent = this;
			billboardBuffer.enabled = false;
			this.m_BillboardBufferList.Add(billboardBuffer);
			billboardBuffer.UpdatePositions();
			billboardBuffer.UpdateSizes();
			billboardBuffer.UpdateRots();
			billboardBuffer.UpdateUVs();
			billboardBuffer.UpdateColors();
			if (this.m_EffectMeshBuffer.isUsableSubUVsFix)
			{
				billboardBuffer.UpdateSubUVs();
			}
			this.bufferChangeFlg = true;
			billboardBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(billboardNum * 4, billboardNum * 4);
			for (int i = 0; i < billboardNum; i++)
			{
				billboardBuffer.meshBuffer.indices[i * 4] = i * 4;
				billboardBuffer.meshBuffer.indices[i * 4 + 1] = i * 4 + 3;
				billboardBuffer.meshBuffer.indices[i * 4 + 2] = i * 4 + 2;
				billboardBuffer.meshBuffer.indices[i * 4 + 3] = i * 4 + 1;
			}
			billboardBuffer.meshBuffer.UpdateIndices();
			return billboardBuffer;
		}

		// Token: 0x060050DB RID: 20699 RVA: 0x00169968 File Offset: 0x00167D68
		public virtual void RemoveBuffer(PrimBillboardBuffer.BillboardBuffer billboardBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			billboardBuffer.enabled = false;
			int num = this.m_BillboardBufferList.IndexOf(billboardBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(billboardBuffer.meshBuffer);
				billboardBuffer.meshBuffer = null;
				this.m_BillboardBufferList.RemoveAt(num);
			}
		}

		// Token: 0x060050DC RID: 20700 RVA: 0x001699C8 File Offset: 0x00167DC8
		private void UpdateMesh()
		{
			if (this.m_BillboardBufferList.Count == 0)
			{
				return;
			}
			Camera current = Camera.current;
			if (current == null)
			{
				return;
			}
			Transform transform = current.transform;
			for (int i = 0; i < this.m_BillboardBufferList.Count; i++)
			{
				PrimBillboardBuffer.BillboardBuffer billboardBuffer = this.m_BillboardBufferList[i];
				if (billboardBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = billboardBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						Vector3 vector = default(Vector3);
						if (billboardBuffer.enabled)
						{
							Quaternion lhs;
							switch (billboardBuffer.rotateType)
							{
							default:
								lhs = transform.rotation;
								break;
							case PrimBillboardBuffer.eRotateType.BillboardX:
							{
								Vector3 forward = transform.forward;
								forward.x = 0f;
								forward.Normalize();
								lhs = Quaternion.LookRotation(forward);
								break;
							}
							case PrimBillboardBuffer.eRotateType.BillboardY:
							{
								Vector3 forward = transform.forward;
								forward.y = 0f;
								forward.Normalize();
								lhs = Quaternion.LookRotation(forward);
								break;
							}
							case PrimBillboardBuffer.eRotateType.BillboardZ:
							{
								Vector3 forward = transform.forward;
								forward.z = 0f;
								forward.Normalize();
								lhs = Quaternion.LookRotation(forward);
								break;
							}
							case PrimBillboardBuffer.eRotateType.X:
								lhs = Quaternion.Euler(0f, 90f, 0f);
								break;
							case PrimBillboardBuffer.eRotateType.Y:
								lhs = Quaternion.Euler(90f, 0f, 0f);
								break;
							case PrimBillboardBuffer.eRotateType.Z:
								lhs = Quaternion.Euler(0f, 0f, 0f);
								break;
							}
							for (int j = 0; j < billboardBuffer.billboardNum; j++)
							{
								if (billboardBuffer.isDirtyPositions || billboardBuffer.isDirtySizes || billboardBuffer.isDirtyRots)
								{
									meshBuffer.vertices[j * 4].x = billboardBuffer.sizes[j].x * -0.5f;
									meshBuffer.vertices[j * 4].y = billboardBuffer.sizes[j].y * -0.5f;
									meshBuffer.vertices[j * 4].z = 0f;
									meshBuffer.vertices[j * 4 + 1].x = billboardBuffer.sizes[j].x * 0.5f;
									meshBuffer.vertices[j * 4 + 1].y = billboardBuffer.sizes[j].y * -0.5f;
									meshBuffer.vertices[j * 4 + 1].z = 0f;
									meshBuffer.vertices[j * 4 + 2].x = billboardBuffer.sizes[j].x * 0.5f;
									meshBuffer.vertices[j * 4 + 2].y = billboardBuffer.sizes[j].y * 0.5f;
									meshBuffer.vertices[j * 4 + 2].z = 0f;
									meshBuffer.vertices[j * 4 + 3].x = billboardBuffer.sizes[j].x * -0.5f;
									meshBuffer.vertices[j * 4 + 3].y = billboardBuffer.sizes[j].y * 0.5f;
									meshBuffer.vertices[j * 4 + 3].z = 0f;
									Quaternion rhs = Quaternion.Euler(0f, 0f, billboardBuffer.rots[j]);
									meshBuffer.vertices[j * 4] = lhs * rhs * meshBuffer.vertices[j * 4] + billboardBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 1] = lhs * rhs * meshBuffer.vertices[j * 4 + 1] + billboardBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 2] = lhs * rhs * meshBuffer.vertices[j * 4 + 2] + billboardBuffer.positions[j];
									meshBuffer.vertices[j * 4 + 3] = lhs * rhs * meshBuffer.vertices[j * 4 + 3] + billboardBuffer.positions[j];
								}
								if (billboardBuffer.isDirtyUVs)
								{
									meshBuffer.UVs[j * 4].x = billboardBuffer.UVs[j].x;
									meshBuffer.UVs[j * 4].y = billboardBuffer.UVs[j].y;
									meshBuffer.UVs[j * 4 + 1].x = billboardBuffer.UVs[j].x + billboardBuffer.UVs[j].width;
									meshBuffer.UVs[j * 4 + 1].y = billboardBuffer.UVs[j].y;
									meshBuffer.UVs[j * 4 + 2].x = billboardBuffer.UVs[j].x + billboardBuffer.UVs[j].width;
									meshBuffer.UVs[j * 4 + 2].y = billboardBuffer.UVs[j].y + billboardBuffer.UVs[j].height;
									meshBuffer.UVs[j * 4 + 3].x = billboardBuffer.UVs[j].x;
									meshBuffer.UVs[j * 4 + 3].y = billboardBuffer.UVs[j].y + billboardBuffer.UVs[j].height;
								}
								if (this.m_EffectMeshBuffer.isUsableSubUVsFix)
								{
									meshBuffer.SubUVs[j * 4].x = billboardBuffer.SubUVs[j].x;
									meshBuffer.SubUVs[j * 4].y = billboardBuffer.SubUVs[j].y;
									meshBuffer.SubUVs[j * 4 + 1].x = billboardBuffer.SubUVs[j].x + billboardBuffer.SubUVs[j].width;
									meshBuffer.SubUVs[j * 4 + 1].y = billboardBuffer.SubUVs[j].y;
									meshBuffer.SubUVs[j * 4 + 2].x = billboardBuffer.SubUVs[j].x + billboardBuffer.SubUVs[j].width;
									meshBuffer.SubUVs[j * 4 + 2].y = billboardBuffer.SubUVs[j].y + billboardBuffer.SubUVs[j].height;
									meshBuffer.SubUVs[j * 4 + 3].x = billboardBuffer.SubUVs[j].x;
									meshBuffer.SubUVs[j * 4 + 3].y = billboardBuffer.SubUVs[j].y + billboardBuffer.SubUVs[j].height;
								}
								if (billboardBuffer.isDirtyColors)
								{
									meshBuffer.colors[j * 4] = billboardBuffer.colors[j];
									meshBuffer.colors[j * 4 + 1] = billboardBuffer.colors1[j];
									meshBuffer.colors[j * 4 + 2] = billboardBuffer.colors2[j];
									meshBuffer.colors[j * 4 + 3] = billboardBuffer.colors3[j];
								}
							}
							if (billboardBuffer.isDirtyPositions || billboardBuffer.isDirtySizes || billboardBuffer.isDirtyRots)
							{
								meshBuffer.UpdateVertices();
								billboardBuffer.isDirtyPositions = false;
								billboardBuffer.isDirtySizes = false;
								billboardBuffer.isDirtyRots = false;
							}
							if (billboardBuffer.isDirtyUVs)
							{
								meshBuffer.UpdateUVs();
								billboardBuffer.isDirtyUVs = false;
							}
							if (billboardBuffer.isDirtySubUVs)
							{
								meshBuffer.UpdateSubUVs();
								billboardBuffer.isDirtyUVs = false;
							}
							if (billboardBuffer.isDirtyColors)
							{
								meshBuffer.UpdateColors();
								billboardBuffer.isDirtyColors = false;
							}
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
		}

		// Token: 0x060050DD RID: 20701 RVA: 0x0016A3A4 File Offset: 0x001687A4
		private void UpdateAlways()
		{
			int num = 0;
			for (int i = 0; i < this.m_BillboardBufferList.Count; i++)
			{
				PrimBillboardBuffer.BillboardBuffer billboardBuffer = this.m_BillboardBufferList[i];
				if (billboardBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = billboardBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (billboardBuffer.enabled)
						{
							meshBuffer.enabled = true;
							num++;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x060050DE RID: 20702 RVA: 0x0016A428 File Offset: 0x00168828
		private void Update()
		{
			this.UpdateAlways();
		}

		// Token: 0x04004FE0 RID: 20448
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04004FE1 RID: 20449
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04004FE2 RID: 20450
		protected bool m_SetupIsEnd;

		// Token: 0x04004FE3 RID: 20451
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x04004FE4 RID: 20452
		protected List<PrimBillboardBuffer.BillboardBuffer> m_BillboardBufferList = new List<PrimBillboardBuffer.BillboardBuffer>();

		// Token: 0x04004FE5 RID: 20453
		protected List<PrimBillboardBuffer.BillboardBuffer> m_activeBillboardBufferList = new List<PrimBillboardBuffer.BillboardBuffer>(32);

		// Token: 0x02000F44 RID: 3908
		public enum eRotateType : byte
		{
			// Token: 0x04004FE9 RID: 20457
			BillboardXYZ,
			// Token: 0x04004FEA RID: 20458
			BillboardX,
			// Token: 0x04004FEB RID: 20459
			BillboardY,
			// Token: 0x04004FEC RID: 20460
			BillboardZ,
			// Token: 0x04004FED RID: 20461
			X,
			// Token: 0x04004FEE RID: 20462
			Y,
			// Token: 0x04004FEF RID: 20463
			Z,
			// Token: 0x04004FF0 RID: 20464
			Max
		}

		// Token: 0x02000F45 RID: 3909
		public class BillboardBuffer
		{
			// Token: 0x17000561 RID: 1377
			// (get) Token: 0x060050E0 RID: 20704 RVA: 0x0016A43F File Offset: 0x0016883F
			public bool isShareVertexColor
			{
				get
				{
					return this.m_isShareVertexColor;
				}
			}

			// Token: 0x060050E1 RID: 20705 RVA: 0x0016A447 File Offset: 0x00168847
			public void Remove()
			{
			}

			// Token: 0x060050E2 RID: 20706 RVA: 0x0016A449 File Offset: 0x00168849
			public void UpdatePositions()
			{
				this.isDirtyPositions = true;
			}

			// Token: 0x060050E3 RID: 20707 RVA: 0x0016A452 File Offset: 0x00168852
			public void UpdateSizes()
			{
				this.isDirtySizes = true;
			}

			// Token: 0x060050E4 RID: 20708 RVA: 0x0016A45B File Offset: 0x0016885B
			public void UpdateRots()
			{
				this.isDirtyRots = true;
			}

			// Token: 0x060050E5 RID: 20709 RVA: 0x0016A464 File Offset: 0x00168864
			public void UpdateUVs()
			{
				this.isDirtyUVs = true;
			}

			// Token: 0x060050E6 RID: 20710 RVA: 0x0016A46D File Offset: 0x0016886D
			public void UpdateSubUVs()
			{
				this.isDirtySubUVs = true;
			}

			// Token: 0x060050E7 RID: 20711 RVA: 0x0016A476 File Offset: 0x00168876
			public void UpdateColors()
			{
				this.isDirtyColors = true;
			}

			// Token: 0x060050E8 RID: 20712 RVA: 0x0016A47F File Offset: 0x0016887F
			public void Minimize()
			{
			}

			// Token: 0x17000562 RID: 1378
			// (get) Token: 0x060050E9 RID: 20713 RVA: 0x0016A481 File Offset: 0x00168881
			// (set) Token: 0x060050EA RID: 20714 RVA: 0x0016A48C File Offset: 0x0016888C
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateSizes();
					this.UpdateRots();
					this.UpdateColors();
				}
			}

			// Token: 0x04004FF1 RID: 20465
			internal PrimBillboardBuffer parent;

			// Token: 0x04004FF2 RID: 20466
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04004FF3 RID: 20467
			public Vector3[] positions;

			// Token: 0x04004FF4 RID: 20468
			public Vector2[] sizes;

			// Token: 0x04004FF5 RID: 20469
			public float[] rots;

			// Token: 0x04004FF6 RID: 20470
			public Rect[] UVs;

			// Token: 0x04004FF7 RID: 20471
			public Rect[] SubUVs;

			// Token: 0x04004FF8 RID: 20472
			public Color[] colors;

			// Token: 0x04004FF9 RID: 20473
			public Color[] colors1;

			// Token: 0x04004FFA RID: 20474
			public Color[] colors2;

			// Token: 0x04004FFB RID: 20475
			public Color[] colors3;

			// Token: 0x04004FFC RID: 20476
			public PrimBillboardBuffer.eRotateType rotateType;

			// Token: 0x04004FFD RID: 20477
			public int billboardNum;

			// Token: 0x04004FFE RID: 20478
			internal bool isDirtyPositions;

			// Token: 0x04004FFF RID: 20479
			internal bool isDirtySizes;

			// Token: 0x04005000 RID: 20480
			internal bool isDirtyRots;

			// Token: 0x04005001 RID: 20481
			internal bool isDirtyUVs;

			// Token: 0x04005002 RID: 20482
			internal bool isDirtySubUVs;

			// Token: 0x04005003 RID: 20483
			internal bool isDirtyColors;

			// Token: 0x04005004 RID: 20484
			internal bool m_isShareVertexColor = true;

			// Token: 0x04005005 RID: 20485
			private bool m_enabled;
		}
	}
}
