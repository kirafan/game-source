﻿using System;
using System.Linq;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FB8 RID: 4024
	[Serializable]
	public class MsbArticulatedDynamicsHandler
	{
		// Token: 0x060053C1 RID: 21441 RVA: 0x00176BBC File Offset: 0x00174FBC
		public void Init(MsbHandler msbHandler)
		{
			this.m_Owner = msbHandler;
			this.m_Work = new MsbArticulatedDynamicsHandler.MsbADParam(this.m_Src);
			this.m_OldRootPosition = new Vector3[this.m_Work.m_RootJntNum];
			this.m_RootIndexArray = new int[this.m_Work.m_RootJntNum];
			this.m_DiscardTranslateFlags = 0;
			this.m_bStartFromScratch = true;
			this.m_LimitationAngleMode = MsbArticulatedDynamicsHandler.eLimitationAngleMode.eLimitationAngleMode_Standard;
			s32 v = 0;
			this.m_MsbADJointArray = new MsbADJoint[this.m_SrcMsbADJointParamArray.Count<MsbADJointParam>()];
			for (int i = 0; i < this.m_SrcMsbADJointParamArray.Count<MsbADJointParam>(); i++)
			{
				MsbADJoint msbADJoint = new MsbADJoint();
				msbADJoint.Init(this, this.m_SrcMsbADJointParamArray[i]);
				if (msbADJoint.IsRoot())
				{
					this.m_RootIndexArray[v] = i;
					this.m_OldRootPosition[v] = Vector3.zero;
					v++;
				}
				this.m_MsbADJointArray[i] = msbADJoint;
			}
			foreach (MsbADJoint msbADJoint2 in this.m_MsbADJointArray)
			{
				MsbADJoint.AD_HIE_CALC adhieCalc = msbADJoint2.GetADHieCalc();
				int parentIdxOfADHierarchy = msbADJoint2.GetParentIdxOfADHierarchy();
				if (parentIdxOfADHierarchy != -1)
				{
					msbADJoint2.SetParent(this.m_MsbADJointArray[parentIdxOfADHierarchy]);
				}
				adhieCalc.MakeMatrix();
				msbADJoint2.SetWorldPosition(adhieCalc.m_Matrix.GetColumn(3));
				msbADJoint2.SetOldWorldPosition(adhieCalc.m_Matrix.GetColumn(3));
			}
			this.m_MsbADLinkArray = new MsbADLink[this.m_SrcMsbADLinkParamArray.Count<MsbADLinkParam>()];
			for (int k = 0; k < this.m_SrcMsbADLinkParamArray.Count<MsbADLinkParam>(); k++)
			{
				MsbADLink msbADLink = new MsbADLink();
				msbADLink.Init(this, this.m_SrcMsbADLinkParamArray[k]);
				this.m_MsbADLinkArray[k] = msbADLink;
			}
		}

		// Token: 0x060053C2 RID: 21442 RVA: 0x00176D9E File Offset: 0x0017519E
		public MsbHandler GetOwner()
		{
			return this.m_Owner;
		}

		// Token: 0x060053C3 RID: 21443 RVA: 0x00176DA6 File Offset: 0x001751A6
		public int GetJointNum()
		{
			return (this.m_MsbADJointArray != null) ? this.m_MsbADJointArray.Count<MsbADJoint>() : 0;
		}

		// Token: 0x060053C4 RID: 21444 RVA: 0x00176DC4 File Offset: 0x001751C4
		public MsbADJoint GetJoint(int index)
		{
			return this.m_MsbADJointArray[index];
		}

		// Token: 0x060053C5 RID: 21445 RVA: 0x00176DCE File Offset: 0x001751CE
		public int GetLinkNum()
		{
			return (this.m_MsbADLinkArray != null) ? this.m_MsbADLinkArray.Count<MsbADLink>() : 0;
		}

		// Token: 0x060053C6 RID: 21446 RVA: 0x00176DEC File Offset: 0x001751EC
		public MsbADLink GetLink(int index)
		{
			return this.m_MsbADLinkArray[index];
		}

		// Token: 0x060053C7 RID: 21447 RVA: 0x00176DF6 File Offset: 0x001751F6
		public int GetRootNum()
		{
			return (this.m_RootIndexArray != null) ? this.m_RootIndexArray.Count<int>() : 0;
		}

		// Token: 0x060053C8 RID: 21448 RVA: 0x00176E14 File Offset: 0x00175214
		public MsbADJoint GetRoot(int index)
		{
			return this.GetJoint(this.m_RootIndexArray[index]);
		}

		// Token: 0x060053C9 RID: 21449 RVA: 0x00176E24 File Offset: 0x00175224
		public float GetAnimBlendRatio()
		{
			return this.m_Work.m_AnimBlendRatio;
		}

		// Token: 0x060053CA RID: 21450 RVA: 0x00176E31 File Offset: 0x00175231
		public void SetAnimBlendRatio(float ratio)
		{
			this.m_Work.m_AnimBlendRatio = ratio;
		}

		// Token: 0x060053CB RID: 21451 RVA: 0x00176E3F File Offset: 0x0017523F
		public float GetKeepShapeCoef()
		{
			return this.m_Work.m_KeepShapeCoef;
		}

		// Token: 0x060053CC RID: 21452 RVA: 0x00176E4C File Offset: 0x0017524C
		public void SetKeepShapeCoef(float coef)
		{
			this.m_Work.m_KeepShapeCoef = coef;
		}

		// Token: 0x060053CD RID: 21453 RVA: 0x00176E5A File Offset: 0x0017525A
		public float GetLinkConstraintForce()
		{
			return this.m_Work.m_LinkConstraintForce;
		}

		// Token: 0x060053CE RID: 21454 RVA: 0x00176E67 File Offset: 0x00175267
		public float GetLimitationAngle()
		{
			return this.m_Work.m_LimitationAngle;
		}

		// Token: 0x060053CF RID: 21455 RVA: 0x00176E74 File Offset: 0x00175274
		public void SetDiscardTranslate(bool bX, bool bY, bool bZ)
		{
			this.m_DiscardTranslateFlags = (((!bX) ? 0 : 1) | ((!bY) ? 0 : 2) | ((!bZ) ? 0 : 4));
		}

		// Token: 0x060053D0 RID: 21456 RVA: 0x00176EA5 File Offset: 0x001752A5
		public bool IsDiscardTranslateX()
		{
			return (this.m_DiscardTranslateFlags & 1) != 0;
		}

		// Token: 0x060053D1 RID: 21457 RVA: 0x00176EB5 File Offset: 0x001752B5
		public bool IsDiscardTranslateY()
		{
			return (this.m_DiscardTranslateFlags & 2) != 0;
		}

		// Token: 0x060053D2 RID: 21458 RVA: 0x00176EC5 File Offset: 0x001752C5
		public bool IsDiscardTranslateZ()
		{
			return (this.m_DiscardTranslateFlags & 4) != 0;
		}

		// Token: 0x060053D3 RID: 21459 RVA: 0x00176ED5 File Offset: 0x001752D5
		public void SetLimitationAngleMode(MsbArticulatedDynamicsHandler.eLimitationAngleMode limitAngleMode)
		{
			this.m_LimitationAngleMode = limitAngleMode;
		}

		// Token: 0x060053D4 RID: 21460 RVA: 0x00176EDE File Offset: 0x001752DE
		public MsbArticulatedDynamicsHandler.eLimitationAngleMode GetLimitationAngleMode()
		{
			return this.m_LimitationAngleMode;
		}

		// Token: 0x060053D5 RID: 21461 RVA: 0x00176EE6 File Offset: 0x001752E6
		public float GetAirFrictionCoef()
		{
			return this.m_Work.m_AirFrictionCoef;
		}

		// Token: 0x060053D6 RID: 21462 RVA: 0x00176EF3 File Offset: 0x001752F3
		public string GetName()
		{
			return this.m_Name;
		}

		// Token: 0x060053D7 RID: 21463 RVA: 0x00176EFB File Offset: 0x001752FB
		public int GetRefID()
		{
			return this.m_Work.m_RefID;
		}

		// Token: 0x060053D8 RID: 21464 RVA: 0x00176F08 File Offset: 0x00175308
		private bool IsAnimationBlend()
		{
			return this.GetAnimBlendRatio() > 1E-06f;
		}

		// Token: 0x060053D9 RID: 21465 RVA: 0x00176F18 File Offset: 0x00175318
		public void CalculateHierarchyMatrix()
		{
			if (this.m_bStartFromScratch)
			{
			}
			for (int i = 0; i < this.GetJointNum(); i++)
			{
				MsbADJoint joint = this.GetJoint(i);
				if (this.m_bStartFromScratch)
				{
					joint.StartFromScratch();
				}
			}
			if (this.m_bStartFromScratch)
			{
				s32 v = 0;
				while (v < this.GetRootNum())
				{
					MsbADJoint root = this.GetRoot(v);
					this.m_OldRootPosition[v] = root.GetHierarchyNode().transform.position;
					v++;
				}
			}
		}

		// Token: 0x060053DA RID: 21466 RVA: 0x00176FCC File Offset: 0x001753CC
		public void DoWork()
		{
			int num = 2;
			int num2 = 1;
			int linkConstraintForceRep = 1;
			float num3 = 0.016666668f;
			float fDtRepci = 1f / num3;
			bool bAnimBlend = false;
			for (int i = 0; i < num; i++)
			{
				this.Prepare();
				for (int j = 0; j < num2; j++)
				{
					for (int k = 0; k < this.GetJointNum(); k++)
					{
						MsbADJoint joint = this.GetJoint(k);
						joint.ExternalForce(num3);
					}
					this.LinkConstraintForce(num3, linkConstraintForceRep);
				}
				this.IKSolve(false, bAnimBlend);
				this.Finalyze(fDtRepci, true);
				this.m_bStartFromScratch = false;
			}
			for (int l = 0; l < this.GetRootNum(); l++)
			{
				MsbADJoint root = this.GetRoot(l);
				this.m_OldRootPosition[l] = root.GetWorldPosition();
			}
		}

		// Token: 0x060053DB RID: 21467 RVA: 0x001770B0 File Offset: 0x001754B0
		private void Prepare()
		{
			for (int i = 0; i < this.GetRootNum(); i++)
			{
				MsbADJoint root = this.GetRoot(i);
				Vector3 translateToDiscard = Vector3.zero;
				root.Prepare(translateToDiscard, this.m_bStartFromScratch);
				translateToDiscard = root.GetHierarchyNode().GetTransform().position - this.m_OldRootPosition[i];
				if (!this.IsDiscardTranslateX())
				{
					translateToDiscard.x = 0f;
				}
				if (!this.IsDiscardTranslateY())
				{
					translateToDiscard.y = 0f;
				}
				if (!this.IsDiscardTranslateZ())
				{
					translateToDiscard.z = 0f;
				}
				for (int j = 1; j < root.GetNumberOfIKGroup(); j++)
				{
					MsbADJoint joint = this.GetJoint(this.m_RootIndexArray[i] + j);
					joint.Prepare(translateToDiscard, this.m_bStartFromScratch);
				}
			}
			for (int k = 0; k < this.GetLinkNum(); k++)
			{
				MsbADLink link = this.GetLink(k);
				link.Prepare();
			}
		}

		// Token: 0x060053DC RID: 21468 RVA: 0x001771C0 File Offset: 0x001755C0
		private void LinkConstraintForce(float fDt, int linkConstraintForceRep)
		{
			int num = 0;
			int num2 = 0;
			do
			{
				bool flag = true;
				for (int i = num; i < this.GetLinkNum(); i++)
				{
					MsbADLink link = this.GetLink(i);
					MsbADJoint srcJoint = link.GetSrcJoint();
					MsbADJoint tgtJoint = link.GetTgtJoint();
					if (!srcJoint.IsLock() || !tgtJoint.IsLock())
					{
						if (link.ConstraintForce(fDt) >= 1E-06f)
						{
							flag = false;
						}
					}
				}
				if (linkConstraintForceRep <= 0)
				{
					break;
				}
				if (flag)
				{
					break;
				}
				num = 1;
				for (int j = this.GetLinkNum() - (num + 1); j >= 0; j--)
				{
					MsbADLink link2 = this.GetLink(j);
					MsbADJoint srcJoint2 = link2.GetSrcJoint();
					MsbADJoint tgtJoint2 = link2.GetTgtJoint();
					if (!srcJoint2.IsLock() || !tgtJoint2.IsLock())
					{
						if (link2.ConstraintForce(fDt) >= 1E-06f)
						{
							flag = false;
						}
					}
				}
				if (flag)
				{
					break;
				}
			}
			while (++num2 < linkConstraintForceRep);
		}

		// Token: 0x060053DD RID: 21469 RVA: 0x001772D4 File Offset: 0x001756D4
		private void Finalyze(float fDtRepci, bool bAfterIKSolve)
		{
			for (int i = 0; i < this.GetJointNum(); i++)
			{
				MsbADJoint joint = this.GetJoint(i);
				joint.Finalyze(fDtRepci, bAfterIKSolve);
			}
		}

		// Token: 0x060053DE RID: 21470 RVA: 0x00177308 File Offset: 0x00175708
		private void IKSolve(bool bEnableIKLinkStretch, bool bAnimBlend)
		{
			for (int i = 0; i < this.GetRootNum(); i++)
			{
				MsbADJoint root = this.GetRoot(i);
				root.GetADHieCalc().MakeMatrix();
				for (int j = 1; j < root.GetNumberOfIKGroup(); j++)
				{
					MsbADJoint joint = this.GetJoint(this.m_RootIndexArray[i] + j);
					MsbADJoint joint2 = this.GetJoint(joint.GetParentIdxOfADHierarchy());
					MsbADJoint.AD_HIE_CALC adhieCalc = joint.GetADHieCalc();
					MsbADJoint.AD_HIE_CALC adhieCalc2 = joint2.GetADHieCalc();
					adhieCalc.MakeMatrix();
					Vector3 a = adhieCalc.m_Matrix.GetColumn(3);
					Vector3 b = adhieCalc2.m_Matrix.GetColumn(3);
					Vector3 worldPosition = joint.GetWorldPosition();
					Vector3 a2 = a - b;
					Vector3 a3 = worldPosition - b;
					float magnitude = a2.magnitude;
					float magnitude2 = a3.magnitude;
					if (!float.IsNaN(magnitude) && !float.IsNaN(magnitude2))
					{
						if (magnitude > 0f && magnitude2 > 0f)
						{
							Vector3 lhs = a2 / magnitude;
							Vector3 rhs = a3 / magnitude2;
							float num = Vector3.Dot(lhs, rhs);
							if (num >= 0.9999f)
							{
								if (bEnableIKLinkStretch || joint.IsIKLinkStretch())
								{
									adhieCalc.m_ScaleOfDistance = magnitude2 / magnitude;
									adhieCalc.MakeMatrix();
								}
							}
							else
							{
								Vector3 vector = Vector3.Cross(lhs, rhs);
								if (Mathf.Abs(vector.x) <= 0f && Mathf.Abs(vector.y) <= 0f && Mathf.Abs(vector.z) <= 0f)
								{
									if (bEnableIKLinkStretch || joint.IsIKLinkStretch())
									{
										adhieCalc.m_ScaleOfDistance = magnitude2 / magnitude;
										adhieCalc.MakeMatrix();
									}
								}
								else
								{
									vector = Matrix4x4.Inverse(adhieCalc2.m_Matrix).MultiplyVector(vector);
									vector.Normalize();
									float num2 = Mathf.Acos(num);
									if (!joint.IsCollided())
									{
										float num3 = Mathf.Abs(num2);
										if (num3 > this.GetLimitationAngle())
										{
											float num4;
											switch (this.GetLimitationAngleMode())
											{
											case MsbArticulatedDynamicsHandler.eLimitationAngleMode.eLimitationAngleMode_Disable:
												num4 = Mathf.Lerp(this.GetLimitationAngle(), num3, 0.5f);
												break;
											case MsbArticulatedDynamicsHandler.eLimitationAngleMode.eLimitationAngleMode_Standard:
												goto IL_239;
											case MsbArticulatedDynamicsHandler.eLimitationAngleMode.eLimitationAngleMode_ExceptCollide:
												num4 = (joint.IsCollided() ? Mathf.Min(num3, 0.7853982f) : this.GetLimitationAngle());
												break;
											default:
												goto IL_239;
											}
											IL_26F:
											if (num2 < 0f)
											{
												num2 = -num4;
												goto IL_289;
											}
											num2 = num4;
											goto IL_289;
											IL_239:
											num4 = this.GetLimitationAngle();
											goto IL_26F;
										}
									}
									IL_289:
									Quaternion quaternion = Quaternion.AngleAxis(57.29578f * num2, vector);
									if (bAnimBlend)
									{
										Quaternion a4 = quaternion;
										quaternion = Quaternion.Slerp(a4, adhieCalc2.m_HierarchyNode.GetTransform().localRotation, this.GetAnimBlendRatio());
									}
									adhieCalc2.m_Rotation *= quaternion;
									adhieCalc2.MakeMatrix();
									if (bEnableIKLinkStretch || joint.IsIKLinkStretch())
									{
										adhieCalc.m_ScaleOfDistance = magnitude2 / magnitude;
									}
									adhieCalc.MakeMatrix();
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x060053DF RID: 21471 RVA: 0x00177638 File Offset: 0x00175A38
		public void Flush()
		{
			bool bAnimBlend = this.IsAnimationBlend();
			for (int i = 0; i < this.GetJointNum(); i++)
			{
				MsbADJoint joint = this.GetJoint(i);
				joint.Flush(bAnimBlend);
			}
			this.m_bStartFromScratch = false;
		}

		// Token: 0x0400548E RID: 21646
		public string m_Name;

		// Token: 0x0400548F RID: 21647
		public MsbArticulatedDynamicsHandler.MsbADParam m_Src;

		// Token: 0x04005490 RID: 21648
		public MsbADJointParam[] m_SrcMsbADJointParamArray;

		// Token: 0x04005491 RID: 21649
		public MsbADLinkParam[] m_SrcMsbADLinkParamArray;

		// Token: 0x04005492 RID: 21650
		private MsbADJoint[] m_MsbADJointArray;

		// Token: 0x04005493 RID: 21651
		private MsbADLink[] m_MsbADLinkArray;

		// Token: 0x04005494 RID: 21652
		private MsbHandler m_Owner;

		// Token: 0x04005495 RID: 21653
		private MsbArticulatedDynamicsHandler.MsbADParam m_Work;

		// Token: 0x04005496 RID: 21654
		private Vector3[] m_OldRootPosition;

		// Token: 0x04005497 RID: 21655
		private int[] m_RootIndexArray;

		// Token: 0x04005498 RID: 21656
		private int m_DiscardTranslateFlags;

		// Token: 0x04005499 RID: 21657
		private bool m_bStartFromScratch;

		// Token: 0x0400549A RID: 21658
		private MsbArticulatedDynamicsHandler.eLimitationAngleMode m_LimitationAngleMode;

		// Token: 0x02000FB9 RID: 4025
		[Serializable]
		public class MsbADParam
		{
			// Token: 0x060053E0 RID: 21472 RVA: 0x00177679 File Offset: 0x00175A79
			public MsbADParam()
			{
			}

			// Token: 0x060053E1 RID: 21473 RVA: 0x00177684 File Offset: 0x00175A84
			public MsbADParam(MsbArticulatedDynamicsHandler.MsbADParam src)
			{
				this.m_AnimBlendRatio = src.m_AnimBlendRatio;
				this.m_KeepShapeCoef = src.m_KeepShapeCoef;
				this.m_LinkConstraintForce = src.m_LinkConstraintForce;
				this.m_LimitationAngle = src.m_LimitationAngle;
				this.m_AirFrictionCoef = src.m_AirFrictionCoef;
				this.m_JntNum = src.m_JntNum;
				this.m_LinkNum = src.m_LinkNum;
				this.m_RootJntNum = src.m_RootJntNum;
				this.m_HierachyDepth = src.m_HierachyDepth;
				this.m_RefID = src.m_RefID;
				this.m_CollisionNum = src.m_CollisionNum;
			}

			// Token: 0x0400549B RID: 21659
			public float m_AnimBlendRatio;

			// Token: 0x0400549C RID: 21660
			public float m_KeepShapeCoef;

			// Token: 0x0400549D RID: 21661
			public float m_LinkConstraintForce;

			// Token: 0x0400549E RID: 21662
			public float m_LimitationAngle;

			// Token: 0x0400549F RID: 21663
			public float m_AirFrictionCoef;

			// Token: 0x040054A0 RID: 21664
			public int m_JntNum;

			// Token: 0x040054A1 RID: 21665
			public int m_LinkNum;

			// Token: 0x040054A2 RID: 21666
			public int m_RootJntNum;

			// Token: 0x040054A3 RID: 21667
			public int m_HierachyDepth;

			// Token: 0x040054A4 RID: 21668
			public int m_RefID;

			// Token: 0x040054A5 RID: 21669
			public int m_CollisionNum;
		}

		// Token: 0x02000FBA RID: 4026
		public enum eLimitationAngleMode
		{
			// Token: 0x040054A7 RID: 21671
			eLimitationAngleMode_Disable,
			// Token: 0x040054A8 RID: 21672
			eLimitationAngleMode_Standard,
			// Token: 0x040054A9 RID: 21673
			eLimitationAngleMode_ExceptCollide
		}
	}
}
