﻿using System;

namespace Meige
{
	// Token: 0x02000F82 RID: 3970
	public enum eParticleLifeScaleType : byte
	{
		// Token: 0x040052FE RID: 21246
		eParticleLifeScaleType_Linear,
		// Token: 0x040052FF RID: 21247
		eParticleLifeScaleType_CurveAcceleration,
		// Token: 0x04005300 RID: 21248
		eParticleLifeScaleType_CurveSlowdonw
	}
}
