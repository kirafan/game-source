﻿using System;

namespace Meige
{
	// Token: 0x02000F00 RID: 3840
	[Serializable]
	public class RangePareInt : RangePare<s32>
	{
		// Token: 0x06004F2A RID: 20266 RVA: 0x0015DDA9 File Offset: 0x0015C1A9
		public RangePareInt()
		{
		}

		// Token: 0x06004F2B RID: 20267 RVA: 0x0015DDB1 File Offset: 0x0015C1B1
		public RangePareInt(s32 min, s32 max) : base(min, max)
		{
		}

		// Token: 0x06004F2C RID: 20268 RVA: 0x0015DDBB File Offset: 0x0015C1BB
		public RangePareInt(RangePareInt v) : base(v)
		{
		}

		// Token: 0x06004F2D RID: 20269 RVA: 0x0015DDC4 File Offset: 0x0015C1C4
		public new RangePareInt Clone()
		{
			return new RangePareInt(this);
		}
	}
}
