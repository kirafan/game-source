﻿using System;

namespace Meige
{
	// Token: 0x02000EE8 RID: 3816
	[Serializable]
	public class EffectRule_Example : EffectRuleBase
	{
		// Token: 0x06004EAC RID: 20140 RVA: 0x0015C559 File Offset: 0x0015A959
		public override int GetPropertyNum()
		{
			return 10;
		}

		// Token: 0x06004EAD RID: 20141 RVA: 0x0015C560 File Offset: 0x0015A960
		public override EffectRuleParamBase GetParamBase(int propertyIdx)
		{
			switch (propertyIdx)
			{
			case 0:
				return this.m_TestParam_Int;
			case 1:
				return this.m_TestParam_Float;
			case 2:
				return this.m_TestParam_S32;
			case 3:
				return this.m_TestParam_F32;
			case 4:
				return this.m_TestParam_Vector4;
			case 5:
				return this.m_TestParam_Float4;
			case 6:
				return this.m_TestParam_RGBA;
			case 7:
				return this.m_TestParam_RangePareVector4;
			case 8:
				return this.m_TestParam_RangeValueVector4;
			case 9:
				return this.m_TestParam_TestPropertyID;
			default:
				return null;
			}
		}

		// Token: 0x06004EAE RID: 20142 RVA: 0x0015C5E9 File Offset: 0x0015A9E9
		private void Update()
		{
		}

		// Token: 0x04004E61 RID: 20065
		[EffectRuleField("TestParam_Int", "m_TestParam_Int")]
		private EffectRuleParam_Int m_TestParam_Int = new EffectRuleParam_Int();

		// Token: 0x04004E62 RID: 20066
		[EffectRuleField("Check/TestParam_Float", "m_TestParam_Float")]
		private EffectRuleParam_Float m_TestParam_Float = new EffectRuleParam_Float();

		// Token: 0x04004E63 RID: 20067
		[EffectRuleField("Check/TestParam_S32", "m_TestParam_S32")]
		private EffectRuleParam_S32 m_TestParam_S32 = new EffectRuleParam_S32();

		// Token: 0x04004E64 RID: 20068
		[EffectRuleField("Check/TestParam_F32", "m_TestParam_F32")]
		private EffectRuleParam_F32 m_TestParam_F32 = new EffectRuleParam_F32();

		// Token: 0x04004E65 RID: 20069
		[EffectRuleField("Check/Test/TestParam_Vector4", "m_TestParam_Vector4")]
		private EffectRuleParam_Vector4 m_TestParam_Vector4 = new EffectRuleParam_Vector4();

		// Token: 0x04004E66 RID: 20070
		[EffectRuleField("Check/Test/TestParam_Float4", "m_TestParam_Float4")]
		private EffectRuleParam_Float4 m_TestParam_Float4 = new EffectRuleParam_Float4();

		// Token: 0x04004E67 RID: 20071
		[EffectRuleField("Check/Test/TestParam_RGBA", "m_TestParam_RGBA")]
		private EffectRuleParam_RGBA m_TestParam_RGBA = new EffectRuleParam_RGBA();

		// Token: 0x04004E68 RID: 20072
		[EffectRuleField("Check/Test/TestParam_RangePareVector4", "m_TestParam_RangePareVector4")]
		private EffectRuleParam_RangePareVector4 m_TestParam_RangePareVector4 = new EffectRuleParam_RangePareVector4();

		// Token: 0x04004E69 RID: 20073
		[EffectRuleField("TestParam_RangeValueVector4", "m_TestParam_RangeValueVector4")]
		private EffectRuleParam_RangeValueVector4 m_TestParam_RangeValueVector4 = new EffectRuleParam_RangeValueVector4();

		// Token: 0x04004E6A RID: 20074
		[EffectRuleField("TestParam_TestPropertyID", "m_TestParam_TestPropertyID")]
		private EffectRule_Example.EffectRuleParam_TestPropertyID m_TestParam_TestPropertyID = new EffectRule_Example.EffectRuleParam_TestPropertyID();

		// Token: 0x02000EE9 RID: 3817
		public enum ePropertyID
		{
			// Token: 0x04004E6C RID: 20076
			TestParam_Int,
			// Token: 0x04004E6D RID: 20077
			TestParam_Float,
			// Token: 0x04004E6E RID: 20078
			TestParam_S32,
			// Token: 0x04004E6F RID: 20079
			TestParam_F32,
			// Token: 0x04004E70 RID: 20080
			TestParam_Vector4,
			// Token: 0x04004E71 RID: 20081
			TestParam_Float4,
			// Token: 0x04004E72 RID: 20082
			TestParam_RGBA,
			// Token: 0x04004E73 RID: 20083
			TestParam_RangePareVector4,
			// Token: 0x04004E74 RID: 20084
			TestParam_RangeValueVector4,
			// Token: 0x04004E75 RID: 20085
			TestParam_TestPropertyID,
			// Token: 0x04004E76 RID: 20086
			Max
		}

		// Token: 0x02000EEA RID: 3818
		[Serializable]
		public class EffectRuleParam_TestPropertyID : EffectRuleParam<EffectRule_Example.ePropertyID>
		{
		}
	}
}
