﻿using System;

namespace Meige.AssetBundles
{
	// Token: 0x02000E83 RID: 3715
	public enum ErrorType
	{
		// Token: 0x04004D9D RID: 19869
		None,
		// Token: 0x04004D9E RID: 19870
		Timeout,
		// Token: 0x04004D9F RID: 19871
		Unknown
	}
}
