﻿using System;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7F RID: 3711
	public class AssetBundleLoadSubAssetOperationFull : AssetBundleLoadAssetOperationFull
	{
		// Token: 0x06004CB7 RID: 19639 RVA: 0x00155C47 File Offset: 0x00154047
		public AssetBundleLoadSubAssetOperationFull(string bundleName, string assetName, Type type) : base(bundleName, assetName, type)
		{
		}

		// Token: 0x06004CB8 RID: 19640 RVA: 0x00155C54 File Offset: 0x00154054
		public override bool Update()
		{
			if (this.m_Request != null)
			{
				base.MoveNext();
				return base.callback != null && !this.m_Request.isDone;
			}
			LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(this.m_AssetBundleName, out this.m_DownloadingError);
			if (loadedAssetBundle != null)
			{
				if (string.IsNullOrEmpty(this.m_AssetName))
				{
					this.m_Request = loadedAssetBundle.m_AssetBundle.LoadAllAssetsAsync();
				}
				else
				{
					this.m_Request = loadedAssetBundle.m_AssetBundle.LoadAssetWithSubAssetsAsync(this.m_AssetName, this.m_Type);
				}
				return base.callback != null;
			}
			return true;
		}
	}
}
