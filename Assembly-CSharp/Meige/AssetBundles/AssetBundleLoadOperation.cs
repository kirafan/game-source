﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7A RID: 3706
	public abstract class AssetBundleLoadOperation : IEnumerator
	{
		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x06004C97 RID: 19607 RVA: 0x00155846 File Offset: 0x00153C46
		// (set) Token: 0x06004C98 RID: 19608 RVA: 0x0015584E File Offset: 0x00153C4E
		public Action<AssetBundleLoadOperation> callback { get; set; }

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06004C99 RID: 19609 RVA: 0x00155857 File Offset: 0x00153C57
		public string assetBundleName
		{
			get
			{
				return this.m_AssetBundleName;
			}
		}

		// Token: 0x06004C9A RID: 19610 RVA: 0x0015585F File Offset: 0x00153C5F
		public ErrorType GetError()
		{
			return this.m_DownloadingError;
		}

		// Token: 0x06004C9B RID: 19611 RVA: 0x00155867 File Offset: 0x00153C67
		public bool IsError()
		{
			return this.m_DownloadingError != ErrorType.None;
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06004C9C RID: 19612 RVA: 0x00155875 File Offset: 0x00153C75
		public AsyncOperation asyncOperation
		{
			get
			{
				return this.m_asyncOperation;
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06004C9D RID: 19613 RVA: 0x0015587D File Offset: 0x00153C7D
		public object Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06004C9E RID: 19614 RVA: 0x00155880 File Offset: 0x00153C80
		public bool MoveNext()
		{
			bool flag = this.IsDone();
			if (flag && this.callback != null)
			{
				this.callback(this);
			}
			return !flag;
		}

		// Token: 0x06004C9F RID: 19615 RVA: 0x001558B5 File Offset: 0x00153CB5
		public void Reset()
		{
		}

		// Token: 0x06004CA0 RID: 19616
		public abstract bool Update();

		// Token: 0x06004CA1 RID: 19617
		public abstract bool IsDone();

		// Token: 0x04004D91 RID: 19857
		protected string m_AssetBundleName;

		// Token: 0x04004D92 RID: 19858
		protected ErrorType m_DownloadingError;

		// Token: 0x04004D93 RID: 19859
		protected AsyncOperation m_asyncOperation;
	}
}
