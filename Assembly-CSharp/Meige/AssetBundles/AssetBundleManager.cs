﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Meige.AssetBundles
{
	// Token: 0x02000E85 RID: 3717
	public class AssetBundleManager : SingletonMonoBehaviour<AssetBundleManager>
	{
		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06004CC5 RID: 19653 RVA: 0x00155E1D File Offset: 0x0015421D
		// (set) Token: 0x06004CC6 RID: 19654 RVA: 0x00155E24 File Offset: 0x00154224
		public static AssetBundleManager.LogMode logMode
		{
			get
			{
				return AssetBundleManager.m_LogMode;
			}
			set
			{
				AssetBundleManager.m_LogMode = value;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06004CC7 RID: 19655 RVA: 0x00155E2C File Offset: 0x0015422C
		// (set) Token: 0x06004CC8 RID: 19656 RVA: 0x00155E33 File Offset: 0x00154233
		public static string BaseDownloadingURL
		{
			get
			{
				return AssetBundleManager.m_BaseDownloadingURL;
			}
			set
			{
				AssetBundleManager.m_BaseDownloadingURL = value;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06004CC9 RID: 19657 RVA: 0x00155E3B File Offset: 0x0015423B
		// (set) Token: 0x06004CCA RID: 19658 RVA: 0x00155E42 File Offset: 0x00154242
		public static string[] ActiveVariants
		{
			get
			{
				return AssetBundleManager.m_ActiveVariants;
			}
			set
			{
				AssetBundleManager.m_ActiveVariants = value;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (set) Token: 0x06004CCB RID: 19659 RVA: 0x00155E4A File Offset: 0x0015424A
		public static AssetBundleManifest AssetBundleManifestObject
		{
			set
			{
				AssetBundleManager.m_AssetBundleManifest = value;
			}
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06004CCC RID: 19660 RVA: 0x00155E52 File Offset: 0x00154252
		public static Dictionary<string, AssetBundleWWW> downloadingWWWs
		{
			get
			{
				return AssetBundleManager.m_DownloadingWWWs;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06004CCD RID: 19661 RVA: 0x00155E59 File Offset: 0x00154259
		// (set) Token: 0x06004CCE RID: 19662 RVA: 0x00155E60 File Offset: 0x00154260
		public static bool isReady { get; private set; }

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06004CD0 RID: 19664 RVA: 0x00155E70 File Offset: 0x00154270
		// (set) Token: 0x06004CCF RID: 19663 RVA: 0x00155E68 File Offset: 0x00154268
		public static float timeoutSec
		{
			get
			{
				return AssetBundleWWW.timeOutSec;
			}
			set
			{
				AssetBundleWWW.timeOutSec = value;
			}
		}

		// Token: 0x06004CD1 RID: 19665 RVA: 0x00155E77 File Offset: 0x00154277
		private static void Log(AssetBundleManager.LogType logType, string text)
		{
			if (logType == AssetBundleManager.LogType.Error)
			{
				Debug.LogError("[AssetBundleManager] " + text);
			}
		}

		// Token: 0x06004CD2 RID: 19666 RVA: 0x00155E90 File Offset: 0x00154290
		public static string GetBundleVersion(string assetBundleName)
		{
			if (AssetBundleManager.m_IndividualVersion != null && AssetBundleManager.m_IndividualVersion.ContainsKey(assetBundleName))
			{
				return AssetBundleManager.m_IndividualVersion[assetBundleName];
			}
			return string.Empty;
		}

		// Token: 0x06004CD3 RID: 19667 RVA: 0x00155EC0 File Offset: 0x001542C0
		private static string GetStreamingAssetsPath()
		{
			if (Application.isEditor)
			{
				return "file://" + Environment.CurrentDirectory.Replace("\\", "/");
			}
			if (Application.isWebPlayer)
			{
				return Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/") + "/StreamingAssets";
			}
			if (Application.isMobilePlatform || Application.isConsolePlatform)
			{
				return Application.streamingAssetsPath;
			}
			return "file://" + Application.streamingAssetsPath;
		}

		// Token: 0x06004CD4 RID: 19668 RVA: 0x00155F4D File Offset: 0x0015434D
		public static void SetSourceAssetBundleDirectory(string relativePath)
		{
			AssetBundleManager.BaseDownloadingURL = AssetBundleManager.GetStreamingAssetsPath() + relativePath;
		}

		// Token: 0x06004CD5 RID: 19669 RVA: 0x00155F5F File Offset: 0x0015435F
		public static void SetSourceAssetBundleURL(string absolutePath)
		{
			AssetBundleManager.BaseDownloadingURL = absolutePath + Utility.GetPlatformName() + "/";
		}

		// Token: 0x06004CD6 RID: 19670 RVA: 0x00155F78 File Offset: 0x00154378
		public static void SetDevelopmentAssetBundleServer(string _url)
		{
			TextAsset textAsset = Resources.Load("AssetBundleServerURL") as TextAsset;
			string text = (!(textAsset != null)) ? null : textAsset.text.Trim();
			if (text == null || text.Length == 0)
			{
				AssetBundleManager.SetSourceAssetBundleURL(_url);
			}
			else
			{
				AssetBundleManager.SetSourceAssetBundleURL(text);
			}
		}

		// Token: 0x06004CD7 RID: 19671 RVA: 0x00155FD8 File Offset: 0x001543D8
		public static LoadedAssetBundle GetLoadedAssetBundle(string assetBundleName, out ErrorType error)
		{
			bool flag = false;
			if (AssetBundleManager.m_DownloadingErrors.TryGetValue(assetBundleName, out error))
			{
				return null;
			}
			LoadedAssetBundle loadedAssetBundle = null;
			AssetBundleManager.m_LoadedAssetBundles.TryGetValue(assetBundleName, out loadedAssetBundle);
			if (loadedAssetBundle == null && AssetBundleManager.m_DownloadingWWWs.ContainsKey(assetBundleName))
			{
				flag = true;
			}
			string[] array = null;
			if (!AssetBundleManager.m_Dependencies.TryGetValue(assetBundleName, out array))
			{
				return loadedAssetBundle;
			}
			foreach (string key in array)
			{
				if (AssetBundleManager.m_DownloadingErrors.TryGetValue(key, out error))
				{
					return null;
				}
				LoadedAssetBundle loadedAssetBundle2;
				AssetBundleManager.m_LoadedAssetBundles.TryGetValue(key, out loadedAssetBundle2);
				if (loadedAssetBundle2 == null && AssetBundleManager.m_DownloadingWWWs.ContainsKey(key))
				{
					flag = true;
				}
			}
			if (flag)
			{
				error = ErrorType.None;
				return null;
			}
			return loadedAssetBundle;
		}

		// Token: 0x06004CD8 RID: 19672 RVA: 0x001560A0 File Offset: 0x001544A0
		public static bool GetLoadedAssetBundleFlag(string assetBundleName, out ErrorType error)
		{
			bool flag = false;
			if (AssetBundleManager.m_DownloadingErrors.TryGetValue(assetBundleName, out error))
			{
				return false;
			}
			bool flag2 = false;
			AssetBundleManager.m_AssetBundleFlags.TryGetValue(assetBundleName, out flag2);
			if (!flag2)
			{
				if (AssetBundleManager.m_DownloadingWWWs.ContainsKey(assetBundleName))
				{
				}
				return false;
			}
			string[] array = null;
			if (!AssetBundleManager.m_Dependencies.TryGetValue(assetBundleName, out array))
			{
				return true;
			}
			foreach (string key in array)
			{
				if (AssetBundleManager.m_DownloadingErrors.TryGetValue(key, out error))
				{
					return false;
				}
				bool flag3;
				AssetBundleManager.m_AssetBundleFlags.TryGetValue(key, out flag3);
				if (!flag3)
				{
					if (AssetBundleManager.m_DownloadingWWWs.ContainsKey(key))
					{
					}
					return false;
				}
			}
			if (flag)
			{
				error = ErrorType.None;
				return false;
			}
			return true;
		}

		// Token: 0x06004CD9 RID: 19673 RVA: 0x0015616C File Offset: 0x0015456C
		public static void Initialize(string assetServerURL)
		{
			if (AssetBundleManager.isReady)
			{
				return;
			}
			AssetBundleManager.SetSourceAssetBundleURL(assetServerURL);
			SingletonMonoBehaviour<AssetBundleManager>.Instance.StartCoroutine("UpdateManifest");
		}

		// Token: 0x06004CDA RID: 19674 RVA: 0x00156190 File Offset: 0x00154590
		public IEnumerator UpdateManifest()
		{
			if (this.m_isUpdate)
			{
				yield break;
			}
			this.m_isUpdate = true;
			AssetBundleManager.isReady = false;
			string manifestAssetBundleName = Utility.GetPlatformName();
			for (;;)
			{
				AssetBundleManager.LoadAssetBundle(manifestAssetBundleName, true, false);
				AssetBundleLoadManifestOperation operation = new AssetBundleLoadManifestOperation(manifestAssetBundleName, "AssetBundleManifest", typeof(AssetBundleManifest));
				AssetBundleManager.m_InProgressOperations.Add(operation);
				yield return operation;
				bool isSuccess = false;
				if (!operation.IsError())
				{
					isSuccess = true;
				}
				AssetBundleManager.UnloadAssetBundle(manifestAssetBundleName);
				AssetBundleManager.UnloadErrors(manifestAssetBundleName);
				if (isSuccess)
				{
					break;
				}
				yield return new WaitForSeconds(1f);
			}
			UnityWebRequest www;
			Utility.AssetBundleMD5Version assetBundleMD5Version;
			for (;;)
			{
				string url = AssetBundleManager.m_BaseDownloadingURL + "v.b?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
				www = UnityWebRequest.Get(url);
				www.timeout = (int)AssetBundleManager.timeoutSec;
				yield return www.Send();
				if ((www != null || string.IsNullOrEmpty(www.error)) && www.responseCode == 200L)
				{
					assetBundleMD5Version = null;
					try
					{
						byte[] array = www.downloadHandler.data;
						array = ZlibUtil.uncompress(array);
						string @string = Encoding.UTF8.GetString(array);
						assetBundleMD5Version = JsonUtility.FromJson<Utility.AssetBundleMD5Version>(@string);
					}
					catch
					{
					}
					if (assetBundleMD5Version != null && assetBundleMD5Version.list != null && assetBundleMD5Version.list.Count > 0)
					{
						break;
					}
				}
				www.Dispose();
				yield return new WaitForSeconds(1f);
			}
			if (AssetBundleManager.m_IndividualVersion == null)
			{
				AssetBundleManager.m_IndividualVersion = new Dictionary<string, string>();
			}
			AssetBundleManager.m_IndividualVersion.Clear();
			foreach (Utility.AssetBundleMD5VersionParam assetBundleMD5VersionParam in assetBundleMD5Version.list)
			{
				AssetBundleManager.m_IndividualVersion[assetBundleMD5VersionParam.name] = assetBundleMD5VersionParam.version;
			}
			www.Dispose();
			AssetBundleManager.isReady = true;
			this.m_isUpdate = false;
			yield break;
		}

		// Token: 0x06004CDB RID: 19675 RVA: 0x001561AC File Offset: 0x001545AC
		protected static void LoadAssetBundle(string assetBundleName, bool isLoadingAssetBundleManifest = false, bool isDownloadOnly = false)
		{
			AssetBundleManager.Log(AssetBundleManager.LogType.Info, "Loading Asset Bundle " + ((!isLoadingAssetBundleManifest) ? ": " : "Manifest: ") + assetBundleName);
			if (!isLoadingAssetBundleManifest && AssetBundleManager.m_AssetBundleManifest == null)
			{
				Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
				return;
			}
			if (!SingletonMonoBehaviour<AssetBundleManager>.instance.LoadAssetBundleInternal(assetBundleName, isLoadingAssetBundleManifest, isDownloadOnly) && !isLoadingAssetBundleManifest)
			{
				AssetBundleManager.LoadDependencies(assetBundleName, isDownloadOnly);
			}
		}

		// Token: 0x06004CDC RID: 19676 RVA: 0x00156224 File Offset: 0x00154624
		protected static string RemapVariantName(string assetBundleName)
		{
			if (!AssetBundleManager.isReady)
			{
				Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
				Debug.Break();
				return null;
			}
			string[] allAssetBundlesWithVariant = AssetBundleManager.m_AssetBundleManifest.GetAllAssetBundlesWithVariant();
			string[] array = assetBundleName.Split(new char[]
			{
				'.'
			});
			int num = int.MaxValue;
			int num2 = -1;
			for (int i = 0; i < allAssetBundlesWithVariant.Length; i++)
			{
				string[] array2 = allAssetBundlesWithVariant[i].Split(new char[]
				{
					'.'
				});
				if (!(array2[0] != array[0]))
				{
					int num3 = Array.IndexOf<string>(AssetBundleManager.m_ActiveVariants, array2[1]);
					if (num3 == -1)
					{
						num3 = 2147483646;
					}
					if (num3 < num)
					{
						num = num3;
						num2 = i;
					}
				}
			}
			if (num == 2147483646)
			{
				Debug.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + allAssetBundlesWithVariant[num2]);
			}
			if (num2 != -1)
			{
				return allAssetBundlesWithVariant[num2];
			}
			return assetBundleName;
		}

		// Token: 0x06004CDD RID: 19677 RVA: 0x0015630C File Offset: 0x0015470C
		protected bool LoadAssetBundleInternal(string assetBundleName, bool isLoadingAssetBundleManifest, bool isDownloadOnly = false)
		{
			if (!isDownloadOnly)
			{
				if (AssetBundleManager.m_ReferencedCounts.ContainsKey(assetBundleName))
				{
					Dictionary<string, int> referencedCounts;
					(referencedCounts = AssetBundleManager.m_ReferencedCounts)[assetBundleName] = referencedCounts[assetBundleName] + 1;
					return true;
				}
				AssetBundleManager.m_ReferencedCounts.Add(assetBundleName, 1);
			}
			if (AssetBundleManager.m_DownloadingWWWs.ContainsKey(assetBundleName))
			{
				return true;
			}
			if (isDownloadOnly && !AssetBundleManager.m_AssetBundleFlags.ContainsKey(assetBundleName))
			{
				AssetBundleManager.m_AssetBundleFlags.Add(assetBundleName, false);
			}
			AssetBundleWWW assetBundleWWW;
			if (isLoadingAssetBundleManifest)
			{
				assetBundleWWW = new AssetBundleWWW(AssetBundleManager.m_BaseDownloadingURL, true, assetBundleName);
			}
			else
			{
				string assetBundleName2 = AssetBundleNameUtility.Encode(assetBundleName);
				assetBundleWWW = new AssetBundleWWW(AssetBundleManager.m_BaseDownloadingURL, false, assetBundleName2);
				assetBundleWWW.isDownloadOnly = isDownloadOnly;
			}
			base.StartCoroutine(assetBundleWWW.Update());
			AssetBundleManager.m_DownloadingWWWs.Add(assetBundleName, assetBundleWWW);
			return false;
		}

		// Token: 0x06004CDE RID: 19678 RVA: 0x001563E0 File Offset: 0x001547E0
		protected static void LoadDependencies(string assetBundleName, bool isDownloadOnly = false)
		{
			if (AssetBundleManager.m_AssetBundleManifest == null)
			{
				Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
				return;
			}
			string[] allDependencies = AssetBundleManager.m_AssetBundleManifest.GetAllDependencies(AssetBundleNameUtility.Encode(assetBundleName));
			if (allDependencies.Length == 0)
			{
				return;
			}
			for (int i = 0; i < allDependencies.Length; i++)
			{
				allDependencies[i] = AssetBundleNameUtility.Decode(allDependencies[i]);
			}
			for (int j = 0; j < allDependencies.Length; j++)
			{
				allDependencies[j] = AssetBundleManager.RemapVariantName(allDependencies[j]);
			}
			if (!AssetBundleManager.m_Dependencies.ContainsKey(assetBundleName))
			{
				AssetBundleManager.m_Dependencies.Add(assetBundleName, allDependencies);
			}
			for (int k = 0; k < allDependencies.Length; k++)
			{
				SingletonMonoBehaviour<AssetBundleManager>.instance.LoadAssetBundleInternal(allDependencies[k], false, isDownloadOnly);
			}
		}

		// Token: 0x06004CDF RID: 19679 RVA: 0x001564A0 File Offset: 0x001548A0
		public static void UnloadAssetBundle(string assetBundleName)
		{
			AssetBundleManager.UnloadAssetBundleInternal(assetBundleName);
			AssetBundleManager.UnloadDependencies(assetBundleName);
		}

		// Token: 0x06004CE0 RID: 19680 RVA: 0x001564B0 File Offset: 0x001548B0
		protected static void UnloadDependencies(string assetBundleName)
		{
			string[] array = null;
			if (!AssetBundleManager.m_Dependencies.TryGetValue(assetBundleName, out array))
			{
				return;
			}
			foreach (string assetBundleName2 in array)
			{
				AssetBundleManager.UnloadAssetBundleInternal(assetBundleName2);
			}
			AssetBundleManager.m_Dependencies.Remove(assetBundleName);
		}

		// Token: 0x06004CE1 RID: 19681 RVA: 0x001564FE File Offset: 0x001548FE
		protected static void UnloadErrors(string assetBundleName)
		{
			if (AssetBundleManager.m_DownloadingErrors.ContainsKey(assetBundleName))
			{
				AssetBundleManager.m_DownloadingErrors.Remove(assetBundleName);
			}
		}

		// Token: 0x06004CE2 RID: 19682 RVA: 0x0015651C File Offset: 0x0015491C
		public static void ClearFlag()
		{
			AssetBundleManager.m_AssetBundleFlags.Clear();
		}

		// Token: 0x06004CE3 RID: 19683 RVA: 0x00156528 File Offset: 0x00154928
		protected static void UnloadFlag(string assetBundleName)
		{
			if (AssetBundleManager.m_AssetBundleFlags.ContainsKey(assetBundleName))
			{
				AssetBundleManager.m_AssetBundleFlags.Remove(assetBundleName);
			}
		}

		// Token: 0x06004CE4 RID: 19684 RVA: 0x00156548 File Offset: 0x00154948
		protected static void UnloadAssetBundleInternal(string assetBundleName)
		{
			AssetBundleManager.UnloadFlag(assetBundleName);
			int num = 1;
			if (AssetBundleManager.m_ReferencedCounts.ContainsKey(assetBundleName))
			{
				Dictionary<string, int> referencedCounts;
				(referencedCounts = AssetBundleManager.m_ReferencedCounts)[assetBundleName] = referencedCounts[assetBundleName] - 1;
				num = AssetBundleManager.m_ReferencedCounts[assetBundleName];
			}
			ErrorType errorType;
			LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(assetBundleName, out errorType);
			if (num <= 0)
			{
				AssetBundleManager.m_ReferencedCounts.Remove(assetBundleName);
				if (loadedAssetBundle != null)
				{
					loadedAssetBundle.m_AssetBundle.Unload(false);
				}
				AssetBundleManager.m_LoadedAssetBundles.Remove(assetBundleName);
				AssetBundleManager.Log(AssetBundleManager.LogType.Info, assetBundleName + " has been unloaded successfully");
			}
		}

		// Token: 0x06004CE5 RID: 19685 RVA: 0x001565E0 File Offset: 0x001549E0
		public static bool IsDependence(string assetBundleName)
		{
			string[] allDependencies = AssetBundleManager.m_AssetBundleManifest.GetAllDependencies(assetBundleName);
			bool result = false;
			if (allDependencies != null && allDependencies.Length > 0)
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06004CE6 RID: 19686 RVA: 0x0015660D File Offset: 0x00154A0D
		private void Update()
		{
			this.UpdateProcess();
		}

		// Token: 0x06004CE7 RID: 19687 RVA: 0x00156618 File Offset: 0x00154A18
		private void UpdateProcess()
		{
			List<string> list = new List<string>();
			foreach (KeyValuePair<string, AssetBundleWWW> keyValuePair in AssetBundleManager.m_DownloadingWWWs)
			{
				string key = keyValuePair.Key;
				AssetBundleWWW value = keyValuePair.Value;
				if (value.error != null)
				{
					if (!AssetBundleManager.m_DownloadingErrors.ContainsKey(keyValuePair.Key))
					{
						AssetBundleManager.m_DownloadingErrors.Add(keyValuePair.Key, ErrorType.Unknown);
					}
					Debug.LogWarning(string.Format("Failed downloading bundle {0} from {1}: {2}", keyValuePair.Key, value.url, value.error));
					list.Add(keyValuePair.Key);
				}
				else if (value.TimeoutCheckUpdate())
				{
					if (!AssetBundleManager.m_DownloadingErrors.ContainsKey(keyValuePair.Key))
					{
						AssetBundleManager.m_DownloadingErrors.Add(keyValuePair.Key, ErrorType.Timeout);
					}
					Debug.LogWarning(string.Format("Timeout downloading bundle {0} from {1}: {2}", keyValuePair.Key, value.url, "Timeout"));
					list.Add(keyValuePair.Key);
				}
				else if (value.isDone)
				{
					AssetBundle assetBundle = value.assetBundle;
					if (!value.isDownloadOnly)
					{
						if (assetBundle == null)
						{
							if (!AssetBundleManager.m_DownloadingErrors.ContainsKey(keyValuePair.Key))
							{
								AssetBundleManager.m_DownloadingErrors.Add(keyValuePair.Key, ErrorType.Unknown);
							}
							Debug.LogWarning(string.Format("{0} is not a valid asset bundle.", keyValuePair.Key));
							list.Add(keyValuePair.Key);
							continue;
						}
						if (!AssetBundleManager.m_LoadedAssetBundles.ContainsKey(keyValuePair.Key))
						{
							AssetBundleManager.m_LoadedAssetBundles.Add(keyValuePair.Key, new LoadedAssetBundle(assetBundle));
						}
					}
					else if (AssetBundleManager.m_AssetBundleFlags.ContainsKey(key))
					{
						AssetBundleManager.m_AssetBundleFlags[key] = true;
					}
					list.Add(keyValuePair.Key);
				}
			}
			foreach (string key2 in list)
			{
				AssetBundleWWW assetBundleWWW = AssetBundleManager.m_DownloadingWWWs[key2];
				base.StopCoroutine(assetBundleWWW.Update());
				assetBundleWWW.Dispose();
				if (AssetBundleManager.m_DownloadingWWWs.ContainsKey(key2))
				{
					AssetBundleManager.m_DownloadingWWWs.Remove(key2);
				}
			}
			int i = 0;
			while (i < AssetBundleManager.m_InProgressOperations.Count)
			{
				if (!AssetBundleManager.m_InProgressOperations[i].Update())
				{
					string assetBundleName = AssetBundleManager.m_InProgressOperations[i].assetBundleName;
					AssetBundleManager.m_InProgressOperations.RemoveAt(i);
					if (AssetBundleManager.m_DownloadingErrors.ContainsKey(assetBundleName))
					{
						AssetBundleManager.m_DownloadingErrors.Remove(assetBundleName);
					}
					string[] array;
					if (AssetBundleManager.m_Dependencies.TryGetValue(assetBundleName, out array))
					{
						foreach (string key3 in array)
						{
							if (AssetBundleManager.m_DownloadingErrors.ContainsKey(key3))
							{
								AssetBundleManager.m_DownloadingErrors.Remove(key3);
							}
						}
					}
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x06004CE8 RID: 19688 RVA: 0x00156994 File Offset: 0x00154D94
		public static AssetBundleLoadAssetOperation LoadAssetAsync(string assetBundleName, string assetName, Type type, Action<AssetBundleLoadOperation> callback = null)
		{
			AssetBundleManager.Log(AssetBundleManager.LogType.Info, string.Format("Loading {0} from {1} bundle", assetName, assetBundleName));
			assetBundleName = AssetBundleManager.RemapVariantName(assetBundleName);
			AssetBundleManager.LoadAssetBundle(assetBundleName, false, false);
			AssetBundleLoadAssetOperation assetBundleLoadAssetOperation = new AssetBundleLoadAssetOperationFull(assetBundleName, assetName, type);
			assetBundleLoadAssetOperation.callback = callback;
			AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadAssetOperation);
			return assetBundleLoadAssetOperation;
		}

		// Token: 0x06004CE9 RID: 19689 RVA: 0x001569E4 File Offset: 0x00154DE4
		public static AssetBundleLoadAssetOperation LoadSubAssetAsync(string assetBundleName, string assetName, Type type, Action<AssetBundleLoadOperation> callback = null)
		{
			AssetBundleManager.Log(AssetBundleManager.LogType.Info, string.Format("Loading {0} from {1} bundle", assetName, assetBundleName));
			assetBundleName = AssetBundleManager.RemapVariantName(assetBundleName);
			AssetBundleManager.LoadAssetBundle(assetBundleName, false, false);
			AssetBundleLoadAssetOperation assetBundleLoadAssetOperation = new AssetBundleLoadSubAssetOperationFull(assetBundleName, assetName, type);
			assetBundleLoadAssetOperation.callback = callback;
			AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadAssetOperation);
			return assetBundleLoadAssetOperation;
		}

		// Token: 0x06004CEA RID: 19690 RVA: 0x00156A34 File Offset: 0x00154E34
		public static AssetBundleLoadOperation LoadLevelAsync(string assetBundleName, string levelName, bool isAdditive, bool allowSceneActivation = true)
		{
			AssetBundleManager.Log(AssetBundleManager.LogType.Info, string.Format("Loading {0} from {1} bundle", levelName, assetBundleName));
			assetBundleName = AssetBundleManager.RemapVariantName(assetBundleName);
			AssetBundleManager.LoadAssetBundle(assetBundleName, false, false);
			AssetBundleLoadOperation assetBundleLoadOperation = new AssetBundleLoadLevelOperation(assetBundleName, levelName, isAdditive, allowSceneActivation);
			AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadOperation);
			return assetBundleLoadOperation;
		}

		// Token: 0x06004CEB RID: 19691 RVA: 0x00156A7C File Offset: 0x00154E7C
		public static AssetBundleLoadOperation DownloadOnlyAsync(string assetBundleName, Action<AssetBundleLoadOperation> callback = null)
		{
			AssetBundleManager.Log(AssetBundleManager.LogType.Info, string.Format("Loading {0} bundle", assetBundleName));
			assetBundleName = AssetBundleManager.RemapVariantName(assetBundleName);
			AssetBundleManager.LoadAssetBundle(assetBundleName, false, true);
			AssetBundleLoadOperation assetBundleLoadOperation = new AssetBundleLoadAssetOperationDownloadOnly(assetBundleName);
			assetBundleLoadOperation.callback = callback;
			AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadOperation);
			return assetBundleLoadOperation;
		}

		// Token: 0x06004CEC RID: 19692 RVA: 0x00156AC8 File Offset: 0x00154EC8
		public static AssetBundleLoadOperation Retry(AssetBundleLoadOperation assetOperation)
		{
			string text = assetOperation.assetBundleName;
			AssetBundleLoadOperation assetBundleLoadOperation = null;
			if (assetOperation is AssetBundleLoadAssetOperationFull)
			{
				AssetBundleLoadAssetOperationFull assetBundleLoadAssetOperationFull = assetOperation as AssetBundleLoadAssetOperationFull;
				string assetName = assetBundleLoadAssetOperationFull.assetName;
				Type objectType = assetBundleLoadAssetOperationFull.GetObjectType();
				text = AssetBundleManager.RemapVariantName(text);
				AssetBundleManager.UnloadDependencies(text);
				AssetBundleManager.UnloadErrors(text);
				AssetBundleManager.LoadAssetBundle(text, false, false);
				assetBundleLoadOperation = new AssetBundleLoadAssetOperationFull(text, assetName, objectType);
				assetBundleLoadOperation.callback = assetOperation.callback;
				AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadOperation);
			}
			else if (assetOperation is AssetBundleLoadSubAssetOperationFull)
			{
				AssetBundleLoadSubAssetOperationFull assetBundleLoadSubAssetOperationFull = assetOperation as AssetBundleLoadSubAssetOperationFull;
				string assetName2 = assetBundleLoadSubAssetOperationFull.assetName;
				Type objectType2 = assetBundleLoadSubAssetOperationFull.GetObjectType();
				text = AssetBundleManager.RemapVariantName(text);
				AssetBundleManager.UnloadDependencies(text);
				AssetBundleManager.UnloadErrors(text);
				AssetBundleManager.LoadAssetBundle(text, false, false);
				assetBundleLoadOperation = new AssetBundleLoadSubAssetOperationFull(text, assetName2, objectType2);
				assetBundleLoadOperation.callback = assetOperation.callback;
				AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadOperation);
			}
			else if (assetOperation is AssetBundleLoadLevelOperation)
			{
				AssetBundleLoadLevelOperation assetBundleLoadLevelOperation = assetOperation as AssetBundleLoadLevelOperation;
				string levelName = assetBundleLoadLevelOperation.GetLevelName();
				bool isAdditive = assetBundleLoadLevelOperation.IsAdditive();
				bool allowSceneActivation = assetBundleLoadLevelOperation.allowSceneActivation();
				text = AssetBundleManager.RemapVariantName(text);
				AssetBundleManager.UnloadDependencies(text);
				AssetBundleManager.UnloadErrors(text);
				AssetBundleManager.LoadAssetBundle(text, false, false);
				assetBundleLoadOperation = new AssetBundleLoadLevelOperation(text, levelName, isAdditive, allowSceneActivation);
				AssetBundleManager.m_InProgressOperations.Add(assetBundleLoadOperation);
			}
			return assetBundleLoadOperation;
		}

		// Token: 0x04004DA1 RID: 19873
		private static AssetBundleManager.LogMode m_LogMode = AssetBundleManager.LogMode.All;

		// Token: 0x04004DA2 RID: 19874
		private static string m_BaseDownloadingURL = string.Empty;

		// Token: 0x04004DA3 RID: 19875
		private static string[] m_ActiveVariants = new string[0];

		// Token: 0x04004DA4 RID: 19876
		private static AssetBundleManifest m_AssetBundleManifest = null;

		// Token: 0x04004DA5 RID: 19877
		private static Dictionary<string, string> m_IndividualVersion = null;

		// Token: 0x04004DA6 RID: 19878
		private static Dictionary<string, LoadedAssetBundle> m_LoadedAssetBundles = new Dictionary<string, LoadedAssetBundle>();

		// Token: 0x04004DA7 RID: 19879
		private static Dictionary<string, bool> m_AssetBundleFlags = new Dictionary<string, bool>();

		// Token: 0x04004DA8 RID: 19880
		private static Dictionary<string, AssetBundleWWW> m_DownloadingWWWs = new Dictionary<string, AssetBundleWWW>();

		// Token: 0x04004DA9 RID: 19881
		private static Dictionary<string, ErrorType> m_DownloadingErrors = new Dictionary<string, ErrorType>();

		// Token: 0x04004DAA RID: 19882
		private static List<AssetBundleLoadOperation> m_InProgressOperations = new List<AssetBundleLoadOperation>();

		// Token: 0x04004DAB RID: 19883
		private static Dictionary<string, int> m_ReferencedCounts = new Dictionary<string, int>();

		// Token: 0x04004DAC RID: 19884
		private static Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]>();

		// Token: 0x04004DAE RID: 19886
		private bool m_isUpdate;

		// Token: 0x02000E86 RID: 3718
		public enum LogMode
		{
			// Token: 0x04004DB0 RID: 19888
			All,
			// Token: 0x04004DB1 RID: 19889
			JustErrors
		}

		// Token: 0x02000E87 RID: 3719
		public enum LogType
		{
			// Token: 0x04004DB3 RID: 19891
			Info,
			// Token: 0x04004DB4 RID: 19892
			Warning,
			// Token: 0x04004DB5 RID: 19893
			Error
		}
	}
}
