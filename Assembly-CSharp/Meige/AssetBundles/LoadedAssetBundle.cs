﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E84 RID: 3716
	public class LoadedAssetBundle
	{
		// Token: 0x06004CC3 RID: 19651 RVA: 0x00155E06 File Offset: 0x00154206
		public LoadedAssetBundle(AssetBundle assetBundle)
		{
			this.m_AssetBundle = assetBundle;
		}

		// Token: 0x04004DA0 RID: 19872
		public AssetBundle m_AssetBundle;
	}
}
