﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Meige.AssetBundles
{
	// Token: 0x02000E89 RID: 3721
	public class AssetBundleWWW : IDisposable
	{
		// Token: 0x06004CF3 RID: 19699 RVA: 0x0015703C File Offset: 0x0015543C
		public AssetBundleWWW(string url, bool isManifest, string assetBundleName = "")
		{
			this.Clear();
			url += assetBundleName;
			this.m_url = url;
			this.m_isManifest = isManifest;
			this.m_assetBundleName = assetBundleName;
			this.m_oldProgress = 0f;
			this.m_timeElapsed = 0f;
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06004CF5 RID: 19701 RVA: 0x00157091 File Offset: 0x00155491
		// (set) Token: 0x06004CF4 RID: 19700 RVA: 0x00157089 File Offset: 0x00155489
		public static float timeOutSec
		{
			get
			{
				return AssetBundleWWW.m_timeOut;
			}
			set
			{
				AssetBundleWWW.m_timeOut = value;
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06004CF6 RID: 19702 RVA: 0x00157098 File Offset: 0x00155498
		public string url
		{
			get
			{
				return this.m_url;
			}
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06004CF7 RID: 19703 RVA: 0x001570A0 File Offset: 0x001554A0
		public bool isManifest
		{
			get
			{
				return this.m_isManifest;
			}
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06004CF8 RID: 19704 RVA: 0x001570A8 File Offset: 0x001554A8
		public string assetBundleName
		{
			get
			{
				return this.m_assetBundleName;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06004CFA RID: 19706 RVA: 0x001570B9 File Offset: 0x001554B9
		// (set) Token: 0x06004CF9 RID: 19705 RVA: 0x001570B0 File Offset: 0x001554B0
		public bool isDownloadOnly
		{
			get
			{
				return this.m_isDownloadOnly;
			}
			set
			{
				this.m_isDownloadOnly = value;
			}
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06004CFB RID: 19707 RVA: 0x001570C1 File Offset: 0x001554C1
		public float progress
		{
			get
			{
				return (this.m_www == null) ? 0f : this.m_www.downloadProgress;
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06004CFC RID: 19708 RVA: 0x001570E3 File Offset: 0x001554E3
		public bool isDone
		{
			get
			{
				return this.m_isDone;
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06004CFD RID: 19709 RVA: 0x001570EB File Offset: 0x001554EB
		public bool isError
		{
			get
			{
				return !string.IsNullOrEmpty(this.m_error);
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06004CFE RID: 19710 RVA: 0x001570FB File Offset: 0x001554FB
		public string error
		{
			get
			{
				return this.m_error;
			}
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06004CFF RID: 19711 RVA: 0x00157103 File Offset: 0x00155503
		public AssetBundle assetBundle
		{
			get
			{
				return this.m_assetBundle;
			}
		}

		// Token: 0x06004D00 RID: 19712 RVA: 0x0015710C File Offset: 0x0015550C
		~AssetBundleWWW()
		{
			this.DecrementLoadCount();
			this.Dispose();
		}

		// Token: 0x06004D01 RID: 19713 RVA: 0x00157144 File Offset: 0x00155544
		public void Dispose()
		{
			if (this.m_www != null)
			{
				if (this.isError && this.m_assetBundle)
				{
					this.m_assetBundle.Unload(false);
				}
				this.m_www.Dispose();
				this.m_www = null;
			}
		}

		// Token: 0x06004D02 RID: 19714 RVA: 0x00157198 File Offset: 0x00155598
		protected void Clear()
		{
			this.m_www = null;
			this.m_url = null;
			this.m_assetBundleName = null;
			this.m_isDownloadOnly = false;
			this.m_assetBundle = null;
			this.m_isDone = false;
			this.m_error = null;
			this.m_md5 = null;
			this.m_isCurrentLoad = false;
			this.m_isCurrentDownload = false;
		}

		// Token: 0x06004D03 RID: 19715 RVA: 0x001571EC File Offset: 0x001555EC
		public bool TimeoutCheckUpdate()
		{
			if (this.m_www == null)
			{
				return false;
			}
			if (this.m_www.isDone)
			{
				return false;
			}
			if (this.m_www.isError)
			{
				return false;
			}
			if (this.m_www.downloadProgress != this.m_oldProgress)
			{
				this.m_oldProgress = this.m_www.downloadProgress;
				return false;
			}
			this.m_timeElapsed += Time.deltaTime;
			return this.m_timeElapsed > AssetBundleWWW.m_timeOut;
		}

		// Token: 0x06004D04 RID: 19716 RVA: 0x00157278 File Offset: 0x00155678
		public IEnumerator Update()
		{
			if (this.m_isManifest)
			{
				yield return this.LoadManifest();
			}
			else
			{
				yield return this.LoadFile();
			}
			yield break;
		}

		// Token: 0x06004D05 RID: 19717 RVA: 0x00157294 File Offset: 0x00155694
		protected IEnumerator LoadFile()
		{
			AssetbundleCache cache = new AssetbundleCache(this.m_assetBundleName, AssetBundleManager.GetBundleVersion(this.m_assetBundleName));
			while (!AssetBundleManager.isReady || AssetBundleWWW.m_currentLoadCount >= ProjDepend.ASSETBUNDLE_MAX_LOADING_COUNT || MeigeResourceManager.isError)
			{
				yield return null;
			}
			AssetBundleWWW.m_currentLoadCount++;
			this.m_isCurrentLoad = true;
			cache.version = AssetBundleManager.GetBundleVersion(this.m_assetBundleName);
			if (cache.ExistAssetbundle())
			{
				if (this.m_isDownloadOnly)
				{
					this.m_isDone = true;
					this.DecrementLoadCount();
					yield break;
				}
				if (!cache.isEncrypted)
				{
					yield return this.LoadFromFileAsync(cache.GetFilePath());
				}
				else
				{
					yield return cache.LoadAssetbundle();
					if (cache.bytes != null)
					{
						yield return this.Decrypted(cache.bytes);
					}
				}
				if (this.m_assetBundle != null)
				{
					this.m_isDone = true;
					this.DecrementLoadCount();
					yield break;
				}
			}
			AssetbundleCache.CleanCache(this.m_assetBundleName);
			this.DecrementLoadCount();
			while (!AssetBundleManager.isReady || AssetBundleWWW.m_currentDownloadCount >= ProjDepend.ASSETBUNDLE_MAX_DOWNLOADING_COUNT || MeigeResourceManager.isError)
			{
				yield return null;
			}
			AssetBundleWWW.m_currentDownloadCount++;
			this.m_isCurrentDownload = true;
			string url = this.m_url + "?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
			this.m_www = UnityWebRequest.Get(url);
			this.m_www.downloadHandler = new FileDownloadHandler(cache.GetFilePath());
			if (this.m_www == null)
			{
				this.SetError("m_www is null.");
				yield break;
			}
			yield return this.m_www.Send();
			if (this.m_www == null)
			{
				this.SetError("m_www is null.");
				yield break;
			}
			if (this.m_www.isError)
			{
				this.SetError("UnityWebRequest error : " + this.m_www.error);
				yield break;
			}
			long status = this.m_www.responseCode;
			if (status != 200L)
			{
				this.SetError("UnityWebRequest status : " + status);
				yield break;
			}
			if (this.m_www.isDone)
			{
				bool isAssetbundle = false;
				FileDownloadHandler handler = (FileDownloadHandler)this.m_www.downloadHandler;
				isAssetbundle = !handler.isEncrypted;
				yield return this.GetMD5(cache.GetFilePath());
				bool isUpdate = false;
				string version;
				for (;;)
				{
					version = AssetBundleManager.GetBundleVersion(this.m_assetBundleName);
					cache.version = version;
					if (!string.IsNullOrEmpty(this.m_md5) && this.m_md5 == version)
					{
						break;
					}
					if (isUpdate)
					{
						goto Block_19;
					}
					yield return SingletonMonoBehaviour<AssetBundleManager>.Instance.UpdateManifest();
					yield return new WaitForSeconds(0.5f);
					isUpdate = true;
				}
				cache.SetVersion(!isAssetbundle);
				if (this.m_isDownloadOnly)
				{
					goto IL_665;
				}
				if (isAssetbundle)
				{
					yield return this.LoadFromFileAsync(cache.GetFilePath());
				}
				else
				{
					yield return cache.LoadAssetbundle();
					if (cache.bytes != null)
					{
						yield return this.Decrypted(cache.bytes);
					}
				}
				if (this.assetBundle == null)
				{
					AssetbundleCache.CleanCache(this.m_assetBundleName);
					this.SetError(string.Format("{0} is not a valid asset bundle.", this.m_assetBundleName));
					yield break;
				}
				goto IL_665;
				Block_19:
				this.SetError("Files of different versions." + this.m_md5 + " <>" + version);
				yield break;
			}
			IL_665:
			this.m_isDone = true;
			this.DecrementLoadCount();
			yield break;
		}

		// Token: 0x06004D06 RID: 19718 RVA: 0x001572B0 File Offset: 0x001556B0
		protected IEnumerator LoadManifest()
		{
			string url = this.m_url + "?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
			this.m_www = UnityWebRequest.GetAssetBundle(url);
			if (this.m_www == null)
			{
				this.SetError("m_www is null.");
				yield break;
			}
			yield return this.m_www.Send();
			if (this.m_www == null)
			{
				this.SetError("m_www is null.");
				yield break;
			}
			if (this.m_www.isError)
			{
				this.SetError("UnityWebRequest error : " + this.m_www.error);
				yield break;
			}
			long status = this.m_www.responseCode;
			if (status != 200L)
			{
				this.SetError("UnityWebRequest status : " + status);
				yield break;
			}
			if (this.m_www.isDone)
			{
				this.m_assetBundle = ((DownloadHandlerAssetBundle)this.m_www.downloadHandler).assetBundle;
				this.m_isDone = true;
				this.DecrementLoadCount();
			}
			yield break;
		}

		// Token: 0x06004D07 RID: 19719 RVA: 0x001572CB File Offset: 0x001556CB
		protected void SetError(string error)
		{
			this.m_error = error;
			this.m_isDone = true;
			this.DecrementLoadCount();
		}

		// Token: 0x06004D08 RID: 19720 RVA: 0x001572E1 File Offset: 0x001556E1
		protected void DecrementLoadCount()
		{
			if (this.m_isCurrentLoad)
			{
				AssetBundleWWW.m_currentLoadCount--;
				this.m_isCurrentLoad = false;
			}
			if (this.m_isCurrentDownload)
			{
				AssetBundleWWW.m_currentDownloadCount--;
				this.m_isCurrentDownload = false;
			}
		}

		// Token: 0x06004D09 RID: 19721 RVA: 0x00157320 File Offset: 0x00155720
		protected IEnumerator LoadFromFileAsync(string path)
		{
			AssetBundleCreateRequest req = null;
			req = AssetBundle.LoadFromFileAsync(path);
			yield return req;
			this.m_assetBundle = req.assetBundle;
			yield break;
		}

		// Token: 0x06004D0A RID: 19722 RVA: 0x00157344 File Offset: 0x00155744
		protected IEnumerator LoadFromMemoryAsync(byte[] bytes)
		{
			if (bytes != null && bytes.Length > 0 && Utility.IsAssetbundle(bytes))
			{
				AssetBundleCreateRequest req = null;
				req = AssetBundle.LoadFromMemoryAsync(bytes);
				yield return req;
				this.m_assetBundle = req.assetBundle;
			}
			yield break;
		}

		// Token: 0x06004D0B RID: 19723 RVA: 0x00157368 File Offset: 0x00155768
		protected IEnumerator GetMD5(string path)
		{
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				Utility utility = new Utility();
				this.m_md5 = utility.GetMD5(path);
			}, null);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			yield break;
		}

		// Token: 0x06004D0C RID: 19724 RVA: 0x0015738C File Offset: 0x0015578C
		protected IEnumerator Decrypted(byte[] src)
		{
			byte[] dst = null;
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				byte[] encryptData = (byte[])obj;
				AssetBundleCryptUtility assetBundleCryptUtility = new AssetBundleCryptUtility();
				assetBundleCryptUtility.DecryptAsset(encryptData, out dst);
			}, src);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			if (!job.IsError())
			{
				yield return this.LoadFromMemoryAsync(dst);
			}
			yield break;
		}

		// Token: 0x04004DB6 RID: 19894
		protected static float m_timeOut = 10f;

		// Token: 0x04004DB7 RID: 19895
		protected static int m_currentLoadCount;

		// Token: 0x04004DB8 RID: 19896
		protected static int m_currentDownloadCount;

		// Token: 0x04004DB9 RID: 19897
		protected UnityWebRequest m_www;

		// Token: 0x04004DBA RID: 19898
		protected string m_url;

		// Token: 0x04004DBB RID: 19899
		protected bool m_isManifest;

		// Token: 0x04004DBC RID: 19900
		protected string m_assetBundleName;

		// Token: 0x04004DBD RID: 19901
		protected bool m_isDownloadOnly;

		// Token: 0x04004DBE RID: 19902
		protected AssetBundle m_assetBundle;

		// Token: 0x04004DBF RID: 19903
		protected bool m_isDone;

		// Token: 0x04004DC0 RID: 19904
		protected string m_error;

		// Token: 0x04004DC1 RID: 19905
		protected string m_md5;

		// Token: 0x04004DC2 RID: 19906
		protected float m_oldProgress;

		// Token: 0x04004DC3 RID: 19907
		protected float m_timeElapsed;

		// Token: 0x04004DC4 RID: 19908
		protected bool m_isCurrentLoad;

		// Token: 0x04004DC5 RID: 19909
		protected bool m_isCurrentDownload;
	}
}
