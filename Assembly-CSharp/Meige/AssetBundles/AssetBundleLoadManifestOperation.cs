﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E80 RID: 3712
	public class AssetBundleLoadManifestOperation : AssetBundleLoadAssetOperationFull
	{
		// Token: 0x06004CB9 RID: 19641 RVA: 0x00155CFD File Offset: 0x001540FD
		public AssetBundleLoadManifestOperation(string bundleName, string assetName, Type type) : base(bundleName, assetName, type)
		{
		}

		// Token: 0x06004CBA RID: 19642 RVA: 0x00155D08 File Offset: 0x00154108
		public override bool Update()
		{
			base.Update();
			if (this.m_Request != null && this.m_Request.isDone)
			{
				AssetBundleManager.AssetBundleManifestObject = this.GetAsset<AssetBundleManifest>();
				return false;
			}
			return true;
		}
	}
}
