﻿using System;
using System.IO;

namespace Meige.AssetBundles
{
	// Token: 0x02000E78 RID: 3704
	public class AssetBundleCryptUtility
	{
		// Token: 0x06004C8E RID: 19598 RVA: 0x00155468 File Offset: 0x00153868
		public void EncryptAsset(byte[] source, string exportPath)
		{
			byte[] array = null;
			AssetBundleCryptography assetBundleCryptography = new AssetBundleCryptography();
			byte[] array2;
			assetBundleCryptography.EncryptAes(source, AssetBundleCryptUtility.EncryptKey, AssetBundleCryptUtility.EncryptPasswordCount, out array2, out array);
			using (FileStream fileStream = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
			{
				using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
				{
					binaryWriter.Write((byte)array2.Length);
					binaryWriter.Write(array2);
					binaryWriter.Write(array.Length);
					binaryWriter.Write(array);
				}
			}
		}

		// Token: 0x06004C8F RID: 19599 RVA: 0x0015550C File Offset: 0x0015390C
		public void DecryptAsset(byte[] encryptData, out byte[] decryptData)
		{
			decryptData = null;
			int num = encryptData.Length;
			using (MemoryStream memoryStream = new MemoryStream(encryptData))
			{
				using (BinaryReader binaryReader = new BinaryReader(memoryStream))
				{
					int num2 = (int)binaryReader.ReadByte();
					if (num2 > 0 && num2 <= num)
					{
						byte[] pw = binaryReader.ReadBytes(num2);
						int num3 = binaryReader.ReadInt32();
						if (num3 > 0 && num3 <= num)
						{
							byte[] src = binaryReader.ReadBytes(num3);
							AssetBundleCryptography assetBundleCryptography = new AssetBundleCryptography();
							assetBundleCryptography.DecryptAes(src, AssetBundleCryptUtility.EncryptKey, pw, out decryptData);
						}
					}
				}
			}
		}

		// Token: 0x04004D8C RID: 19852
		public const int FileHeaderLength = 7;

		// Token: 0x04004D8D RID: 19853
		private static readonly byte[] EncryptKey = new byte[]
		{
			110,
			78,
			78,
			77,
			70,
			117,
			51,
			119,
			83,
			119,
			88,
			119,
			90,
			65,
			86,
			109,
			98,
			69,
			120,
			113,
			67,
			51,
			109,
			102,
			120,
			121,
			106,
			48,
			109,
			57,
			88,
			66
		};

		// Token: 0x04004D8E RID: 19854
		private static readonly int EncryptPasswordCount = 16;
	}
}
