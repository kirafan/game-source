﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E82 RID: 3714
	public class AssetBundleLoadAssetOperationDownloadOnly : AssetBundleLoadOperation
	{
		// Token: 0x06004CC0 RID: 19648 RVA: 0x00155D5A File Offset: 0x0015415A
		public AssetBundleLoadAssetOperationDownloadOnly(string bundleName)
		{
			this.m_AssetBundleName = bundleName;
		}

		// Token: 0x06004CC1 RID: 19649 RVA: 0x00155D6C File Offset: 0x0015416C
		public override bool Update()
		{
			if (this.m_isDownloaded)
			{
				if (base.callback != null)
				{
					base.MoveNext();
				}
				return false;
			}
			this.m_isDownloaded = AssetBundleManager.GetLoadedAssetBundleFlag(this.m_AssetBundleName, out this.m_DownloadingError);
			if (this.m_isDownloaded)
			{
				return false;
			}
			if (this.m_DownloadingError != ErrorType.None)
			{
				base.MoveNext();
				return false;
			}
			return true;
		}

		// Token: 0x06004CC2 RID: 19650 RVA: 0x00155DD1 File Offset: 0x001541D1
		public override bool IsDone()
		{
			if (this.m_DownloadingError != ErrorType.None)
			{
				Debug.Log(this.m_DownloadingError + " : " + this.m_AssetBundleName);
				return true;
			}
			return this.m_isDownloaded;
		}

		// Token: 0x04004D9B RID: 19867
		protected bool m_isDownloaded;
	}
}
