﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7C RID: 3708
	public abstract class AssetBundleLoadAssetOperation : AssetBundleLoadOperation
	{
		// Token: 0x06004CA9 RID: 19625
		public abstract T GetAsset<T>() where T : UnityEngine.Object;

		// Token: 0x06004CAA RID: 19626
		public abstract T[] GetAllAsset<T>() where T : UnityEngine.Object;

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06004CAB RID: 19627 RVA: 0x001559FB File Offset: 0x00153DFB
		public string assetName
		{
			get
			{
				return this.m_AssetName;
			}
		}

		// Token: 0x04004D97 RID: 19863
		protected string m_AssetName;

		// Token: 0x04004D98 RID: 19864
		protected Type m_Type;
	}
}
