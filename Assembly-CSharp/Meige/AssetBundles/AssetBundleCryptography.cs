﻿using System;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E79 RID: 3705
	public class AssetBundleCryptography
	{
		// Token: 0x06004C92 RID: 19602 RVA: 0x001555F8 File Offset: 0x001539F8
		public void EncryptAes(byte[] src, byte[] key, int pwSize, out byte[] pw, out byte[] dst)
		{
			pw = this.CreatePassword(pwSize);
			dst = null;
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				byte[] rgbIV = pw;
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(key, rgbIV))
				{
					using (MemoryStream memoryStream = new MemoryStream())
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
						{
							cryptoStream.Write(src, 0, src.Length);
							cryptoStream.FlushFinalBlock();
							dst = memoryStream.ToArray();
						}
					}
				}
			}
		}

		// Token: 0x06004C93 RID: 19603 RVA: 0x001556F4 File Offset: 0x00153AF4
		public void DecryptAes(byte[] src, byte[] key, byte[] pw, out byte[] dst)
		{
			dst = new byte[src.Length];
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(key, pw))
				{
					using (MemoryStream memoryStream = new MemoryStream(src))
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read))
						{
							cryptoStream.Read(dst, 0, dst.Length);
						}
					}
				}
			}
		}

		// Token: 0x06004C94 RID: 19604 RVA: 0x001557E4 File Offset: 0x00153BE4
		public byte[] CreatePassword(int count)
		{
			byte[] array = new byte[count];
			for (int i = count - 1; i >= 0; i--)
			{
				byte b = AssetBundleCryptography.PasswordChars[UnityEngine.Random.Range(0, AssetBundleCryptography.PasswordChars.Length)];
				array[i] = b;
			}
			return array;
		}

		// Token: 0x04004D8F RID: 19855
		private static byte[] PasswordChars = new byte[]
		{
			48,
			49,
			50,
			51,
			52,
			53,
			54,
			55,
			56,
			57,
			97,
			98,
			99,
			100,
			101,
			102,
			103,
			104,
			105,
			106,
			107,
			108,
			109,
			110,
			111,
			112,
			113,
			114,
			115,
			116,
			117,
			118,
			119,
			120,
			121,
			122,
			65,
			66,
			67,
			68,
			69,
			70,
			71,
			72,
			73,
			74,
			75,
			76,
			77,
			78,
			79,
			80,
			81,
			82,
			83,
			84,
			85,
			86,
			87,
			88,
			89,
			90
		};
	}
}
