﻿using System;
using System.IO;
using UnityEngine.Networking;

namespace Meige.AssetBundles
{
	// Token: 0x02000E8A RID: 3722
	internal class FileDownloadHandler : DownloadHandlerScript
	{
		// Token: 0x06004D0E RID: 19726 RVA: 0x00158174 File Offset: 0x00156574
		public FileDownloadHandler(string path)
		{
			string directoryName = Path.GetDirectoryName(path);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			this.m_fs = new FileStream(path, FileMode.Create, FileAccess.Write);
			this.m_headers = new byte[7];
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06004D0F RID: 19727 RVA: 0x001581CB File Offset: 0x001565CB
		public bool isEncrypted
		{
			get
			{
				return this.m_isEncrypted;
			}
		}

		// Token: 0x06004D10 RID: 19728 RVA: 0x001581D4 File Offset: 0x001565D4
		protected override bool ReceiveData(byte[] data, int dataLength)
		{
			if (this.m_fs != null)
			{
				this.m_fs.Write(data, 0, dataLength);
			}
			this.m_offset += dataLength;
			if (this.m_headers != null && this.m_headerLength < this.m_headers.Length)
			{
				for (int i = 0; i < data.Length; i++)
				{
					this.m_headers[this.m_headerLength] = data[i];
					this.m_headerLength++;
					if (this.m_headerLength >= this.m_headers.Length)
					{
						break;
					}
				}
			}
			return true;
		}

		// Token: 0x06004D11 RID: 19729 RVA: 0x00158274 File Offset: 0x00156674
		protected override void CompleteContent()
		{
			if (this.m_fs != null)
			{
				this.m_fs.Flush();
				this.m_fs.Close();
				this.m_fs = null;
			}
			if (this.m_headers != null)
			{
				this.m_isEncrypted = !Utility.IsAssetbundle(this.m_headers);
			}
		}

		// Token: 0x06004D12 RID: 19730 RVA: 0x001582C8 File Offset: 0x001566C8
		protected override void ReceiveContentLength(int contentLength)
		{
			this.m_length = contentLength;
		}

		// Token: 0x06004D13 RID: 19731 RVA: 0x001582D1 File Offset: 0x001566D1
		protected override float GetProgress()
		{
			if (this.m_length == 0)
			{
				return 0f;
			}
			return (float)this.m_offset / (float)this.m_length;
		}

		// Token: 0x04004DC6 RID: 19910
		private FileStream m_fs;

		// Token: 0x04004DC7 RID: 19911
		private int m_offset;

		// Token: 0x04004DC8 RID: 19912
		private int m_length;

		// Token: 0x04004DC9 RID: 19913
		private byte[] m_headers;

		// Token: 0x04004DCA RID: 19914
		private int m_headerLength;

		// Token: 0x04004DCB RID: 19915
		private bool m_isEncrypted;
	}
}
