﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7E RID: 3710
	public class AssetBundleLoadAssetOperationFull : AssetBundleLoadAssetOperation
	{
		// Token: 0x06004CB1 RID: 19633 RVA: 0x00155A76 File Offset: 0x00153E76
		public AssetBundleLoadAssetOperationFull(string bundleName, string assetName, Type type)
		{
			this.m_AssetBundleName = bundleName;
			this.m_AssetName = assetName;
			this.m_Type = type;
		}

		// Token: 0x06004CB2 RID: 19634 RVA: 0x00155A93 File Offset: 0x00153E93
		public Type GetObjectType()
		{
			return this.m_Type;
		}

		// Token: 0x06004CB3 RID: 19635 RVA: 0x00155A9B File Offset: 0x00153E9B
		public override T GetAsset<T>()
		{
			if (this.m_Request != null && this.m_Request.isDone)
			{
				return this.m_Request.asset as T;
			}
			return (T)((object)null);
		}

		// Token: 0x06004CB4 RID: 19636 RVA: 0x00155AD4 File Offset: 0x00153ED4
		public override T[] GetAllAsset<T>()
		{
			if (this.m_Request != null && this.m_Request.isDone)
			{
				T[] array = new T[this.m_Request.allAssets.Length];
				this.m_Request.allAssets.CopyTo(array, 0);
				return array;
			}
			return null;
		}

		// Token: 0x06004CB5 RID: 19637 RVA: 0x00155B24 File Offset: 0x00153F24
		public override bool Update()
		{
			if (this.m_Request != null)
			{
				base.MoveNext();
				return base.callback != null && !this.m_Request.isDone;
			}
			LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(this.m_AssetBundleName, out this.m_DownloadingError);
			if (loadedAssetBundle != null)
			{
				if (string.IsNullOrEmpty(this.m_AssetName))
				{
					this.m_Request = loadedAssetBundle.m_AssetBundle.LoadAllAssetsAsync();
				}
				else
				{
					this.m_Request = loadedAssetBundle.m_AssetBundle.LoadAssetAsync(this.m_AssetName, this.m_Type);
				}
				return base.callback != null;
			}
			if (this.m_DownloadingError != ErrorType.None)
			{
				base.MoveNext();
				return false;
			}
			return true;
		}

		// Token: 0x06004CB6 RID: 19638 RVA: 0x00155BE4 File Offset: 0x00153FE4
		public override bool IsDone()
		{
			if (this.m_Request == null && this.m_DownloadingError != ErrorType.None)
			{
				Debug.Log(this.m_DownloadingError + " : " + this.m_AssetBundleName);
				return true;
			}
			return this.m_Request != null && this.m_Request.isDone;
		}

		// Token: 0x04004D9A RID: 19866
		protected AssetBundleRequest m_Request;
	}
}
