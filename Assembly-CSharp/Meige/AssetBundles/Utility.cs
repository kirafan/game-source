﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E8C RID: 3724
	public class Utility
	{
		// Token: 0x06004D25 RID: 19749 RVA: 0x00158AB0 File Offset: 0x00156EB0
		public static string GetPlatformName()
		{
			return Utility.GetPlatformForAssetBundles(Application.platform);
		}

		// Token: 0x06004D26 RID: 19750 RVA: 0x00158ABC File Offset: 0x00156EBC
		private static string GetPlatformForAssetBundles(RuntimePlatform platform)
		{
			if (platform == RuntimePlatform.OSXPlayer)
			{
				return "OSX";
			}
			if (platform == RuntimePlatform.WindowsPlayer)
			{
				return "Windows";
			}
			switch (platform)
			{
			case RuntimePlatform.IPhonePlayer:
				return "iOS";
			default:
				if (platform != RuntimePlatform.WebGLPlayer)
				{
					return null;
				}
				return "WebGL";
			case RuntimePlatform.Android:
				return "Android";
			}
		}

		// Token: 0x06004D27 RID: 19751 RVA: 0x00158B1C File Offset: 0x00156F1C
		public static bool IsAssetbundle(byte[] src)
		{
			return src != null && (src[0] == 85 && src[1] == 110 && src[2] == 105 && src[3] == 116 && src[4] == 121 && src[5] == 70) && src[6] == 83;
		}

		// Token: 0x06004D28 RID: 19752 RVA: 0x00158B78 File Offset: 0x00156F78
		public string GetMD5(string path)
		{
			string result = string.Empty;
			using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				using (MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider())
				{
					byte[] value = md5CryptoServiceProvider.ComputeHash(fileStream);
					md5CryptoServiceProvider.Clear();
					fileStream.Close();
					result = BitConverter.ToString(value).ToLower().Replace("-", string.Empty);
				}
			}
			return result;
		}

		// Token: 0x04004DD3 RID: 19923
		public const string AssetBundlesOutputPath = "AssetBundles";

		// Token: 0x04004DD4 RID: 19924
		public const string VersionFileName = "v.b";

		// Token: 0x04004DD5 RID: 19925
		public const int AssetBundleSystemVersion = 2;

		// Token: 0x02000E8D RID: 3725
		[Serializable]
		public class AssetBundleMD5Version
		{
			// Token: 0x04004DD6 RID: 19926
			public List<Utility.AssetBundleMD5VersionParam> list;
		}

		// Token: 0x02000E8E RID: 3726
		[Serializable]
		public class AssetBundleMD5VersionParam
		{
			// Token: 0x04004DD7 RID: 19927
			public string name;

			// Token: 0x04004DD8 RID: 19928
			public string version;
		}
	}
}
