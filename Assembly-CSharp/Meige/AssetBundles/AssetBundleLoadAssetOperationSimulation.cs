﻿using System;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7D RID: 3709
	public class AssetBundleLoadAssetOperationSimulation : AssetBundleLoadAssetOperation
	{
		// Token: 0x06004CAC RID: 19628 RVA: 0x00155A03 File Offset: 0x00153E03
		public AssetBundleLoadAssetOperationSimulation(string bundleName, string assetName, Type type)
		{
		}

		// Token: 0x06004CAD RID: 19629 RVA: 0x00155A0B File Offset: 0x00153E0B
		public override T GetAsset<T>()
		{
			return this.m_SimulatedObjects[0] as T;
		}

		// Token: 0x06004CAE RID: 19630 RVA: 0x00155A20 File Offset: 0x00153E20
		public override T[] GetAllAsset<T>()
		{
			T[] array = new T[this.m_SimulatedObjects.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (this.m_SimulatedObjects[i] as T);
			}
			return array;
		}

		// Token: 0x06004CAF RID: 19631 RVA: 0x00155A69 File Offset: 0x00153E69
		public override bool Update()
		{
			base.MoveNext();
			return false;
		}

		// Token: 0x06004CB0 RID: 19632 RVA: 0x00155A73 File Offset: 0x00153E73
		public override bool IsDone()
		{
			return true;
		}

		// Token: 0x04004D99 RID: 19865
		private UnityEngine.Object[] m_SimulatedObjects;
	}
}
