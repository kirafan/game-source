﻿using System;

namespace Meige.AssetBundles
{
	// Token: 0x02000E81 RID: 3713
	public class AssetBundleLoadAssetOperationDownloadOnlySimulation : AssetBundleLoadAssetOperation
	{
		// Token: 0x06004CBB RID: 19643 RVA: 0x00155D3A File Offset: 0x0015413A
		public AssetBundleLoadAssetOperationDownloadOnlySimulation(string bundleName)
		{
		}

		// Token: 0x06004CBC RID: 19644 RVA: 0x00155D42 File Offset: 0x00154142
		public override T GetAsset<T>()
		{
			return (T)((object)null);
		}

		// Token: 0x06004CBD RID: 19645 RVA: 0x00155D4A File Offset: 0x0015414A
		public override T[] GetAllAsset<T>()
		{
			return null;
		}

		// Token: 0x06004CBE RID: 19646 RVA: 0x00155D4D File Offset: 0x0015414D
		public override bool Update()
		{
			base.MoveNext();
			return false;
		}

		// Token: 0x06004CBF RID: 19647 RVA: 0x00155D57 File Offset: 0x00154157
		public override bool IsDone()
		{
			return true;
		}
	}
}
