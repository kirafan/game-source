﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PreviewLabs;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x02000E8B RID: 3723
	public class AssetbundleCache
	{
		// Token: 0x06004D14 RID: 19732 RVA: 0x001582F3 File Offset: 0x001566F3
		public AssetbundleCache(string fileName, string version)
		{
			this.m_fileName = fileName;
			this.m_version = version;
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06004D15 RID: 19733 RVA: 0x00158309 File Offset: 0x00156709
		public byte[] bytes
		{
			get
			{
				return this.m_bytes;
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06004D16 RID: 19734 RVA: 0x00158311 File Offset: 0x00156711
		public bool isEncrypted
		{
			get
			{
				return this.m_isEncrypted;
			}
		}

		// Token: 0x1700051C RID: 1308
		// (set) Token: 0x06004D17 RID: 19735 RVA: 0x00158319 File Offset: 0x00156719
		public string version
		{
			set
			{
				this.m_version = value;
			}
		}

		// Token: 0x06004D18 RID: 19736 RVA: 0x00158324 File Offset: 0x00156724
		~AssetbundleCache()
		{
			this.m_bytes = null;
		}

		// Token: 0x06004D19 RID: 19737 RVA: 0x00158354 File Offset: 0x00156754
		public static bool CleanCache(string fileName = "")
		{
			try
			{
				if (!string.IsNullOrEmpty(fileName))
				{
					AssetbundleCache.DeleteVersion(fileName);
					string fileName2 = Path.Combine(AssetbundleCache.SavePath, fileName);
					FileInfo fileInfo = new FileInfo(fileName2);
					fileInfo.Delete();
				}
				else
				{
					DirectoryInfo dirInfo = new DirectoryInfo(AssetbundleCache.SavePath);
					AssetbundleCache.CleanCacheProc(dirInfo, string.Empty);
				}
			}
			catch (Exception ex)
			{
				Debug.LogWarning("AssetbundleCache.CleanCache Exception : " + ex.ToString());
				return false;
			}
			return true;
		}

		// Token: 0x06004D1A RID: 19738 RVA: 0x001583E0 File Offset: 0x001567E0
		private static void CleanCacheProc(DirectoryInfo dirInfo, string path = "")
		{
			foreach (FileInfo fileInfo in dirInfo.GetFiles())
			{
				string fileName = Path.Combine(path, fileInfo.Name);
				AssetbundleCache.DeleteVersion(fileName);
				fileInfo.Delete();
			}
			foreach (DirectoryInfo directoryInfo in dirInfo.GetDirectories())
			{
				AssetbundleCache.CleanCacheProc(directoryInfo, Path.Combine(path, directoryInfo.Name));
			}
		}

		// Token: 0x06004D1B RID: 19739 RVA: 0x00158464 File Offset: 0x00156864
		public bool ExistAssetbundle()
		{
			string a;
			this.GetVersion(out a, out this.m_isEncrypted);
			if (a != this.m_version)
			{
				return false;
			}
			string filePath = this.GetFilePath();
			return File.Exists(filePath);
		}

		// Token: 0x06004D1C RID: 19740 RVA: 0x001584A8 File Offset: 0x001568A8
		public IEnumerator LoadAssetbundle()
		{
			this.m_bytes = null;
			if (!this.ExistAssetbundle())
			{
				yield break;
			}
			string saveFile = this.GetFilePath();
			if (File.Exists(saveFile))
			{
				while (AssetbundleCache.m_accessLists.Contains(this.m_fileName))
				{
					yield return null;
				}
				AssetbundleCache.m_accessLists.Add(this.m_fileName);
				Job job = new Job();
				job.SetFunction(delegate(object obj)
				{
					using (FileStream fileStream = new FileStream(saveFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
					{
						this.m_bytes = new byte[fileStream.Length];
						fileStream.Read(this.m_bytes, 0, this.m_bytes.Length);
					}
				}, null);
				JobQueue.Enqueue(job);
				while (!job.IsDone())
				{
					yield return null;
				}
				AssetbundleCache.m_accessLists.Remove(this.m_fileName);
			}
			else
			{
				AssetbundleCache.DeleteVersion(this.m_fileName);
			}
			yield break;
		}

		// Token: 0x06004D1D RID: 19741 RVA: 0x001584C4 File Offset: 0x001568C4
		public IEnumerator SaveAssetbundle(byte[] bytes)
		{
			string saveFile = this.GetFilePath();
			string path = Path.GetDirectoryName(saveFile);
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			while (AssetbundleCache.m_accessLists.Contains(this.m_fileName))
			{
				yield return null;
			}
			AssetbundleCache.m_accessLists.Add(this.m_fileName);
			Job job = new Job();
			job.SetFunction(delegate(object obj)
			{
				using (FileStream fileStream = new FileStream(saveFile, FileMode.Create, FileAccess.Write, FileShare.Write))
				{
					fileStream.Write(bytes, 0, bytes.Length);
				}
			}, null);
			JobQueue.Enqueue(job);
			while (!job.IsDone())
			{
				yield return null;
			}
			AssetbundleCache.m_accessLists.Remove(this.m_fileName);
			yield break;
		}

		// Token: 0x06004D1E RID: 19742 RVA: 0x001584E8 File Offset: 0x001568E8
		public string GetFilePath()
		{
			return Path.Combine(AssetbundleCache.SavePath, this.m_fileName);
		}

		// Token: 0x06004D1F RID: 19743 RVA: 0x00158508 File Offset: 0x00156908
		public void SetVersion(bool isEncrypted)
		{
			this.m_isEncrypted = isEncrypted;
			string key = "AssetbundleVersion_" + this.m_fileName;
			string arg = (!this.m_isEncrypted) ? "0" : "1";
			string arg2 = 2.ToString();
			string value = string.Format("{0}_{1}_{2}", this.m_version, arg, arg2);
			PreviewLabs.PlayerPrefs.SetString(key, value);
			this.SaveVersion();
		}

		// Token: 0x06004D20 RID: 19744 RVA: 0x0015857C File Offset: 0x0015697C
		private void GetVersion(out string oldVersion, out bool isEncrypted)
		{
			string key = "AssetbundleVersion_" + this.m_fileName;
			oldVersion = string.Empty;
			isEncrypted = false;
			if (PreviewLabs.PlayerPrefs.HasKey(key))
			{
				string @string = PreviewLabs.PlayerPrefs.GetString(key);
				string[] array = @string.Split(new char[]
				{
					'_'
				});
				if (array.Length == 3)
				{
					bool flag = array[2] == 2.ToString();
					if (flag)
					{
						oldVersion = array[0];
						isEncrypted = (array[1] == "1");
					}
				}
			}
		}

		// Token: 0x06004D21 RID: 19745 RVA: 0x0015861C File Offset: 0x00156A1C
		private static void DeleteVersion(string fileName)
		{
			string key = "AssetbundleVersion_" + fileName;
			if (PreviewLabs.PlayerPrefs.HasKey(key))
			{
				PreviewLabs.PlayerPrefs.DeleteKey(key);
			}
		}

		// Token: 0x06004D22 RID: 19746 RVA: 0x00158646 File Offset: 0x00156A46
		private void SaveVersion()
		{
			PreviewLabs.PlayerPrefs.Flush();
		}

		// Token: 0x04004DCC RID: 19916
		private const string PREFS_VERSION_KEY = "AssetbundleVersion_";

		// Token: 0x04004DCD RID: 19917
		private static readonly string SavePath = Path.Combine(Application.temporaryCachePath, "a.b");

		// Token: 0x04004DCE RID: 19918
		private static List<string> m_accessLists = new List<string>();

		// Token: 0x04004DCF RID: 19919
		private string m_fileName;

		// Token: 0x04004DD0 RID: 19920
		private string m_version;

		// Token: 0x04004DD1 RID: 19921
		private byte[] m_bytes;

		// Token: 0x04004DD2 RID: 19922
		private bool m_isEncrypted;
	}
}
