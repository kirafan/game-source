﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Meige.AssetBundles
{
	// Token: 0x02000E7B RID: 3707
	public class AssetBundleLoadLevelOperation : AssetBundleLoadOperation
	{
		// Token: 0x06004CA2 RID: 19618 RVA: 0x001558B7 File Offset: 0x00153CB7
		public AssetBundleLoadLevelOperation(string assetbundleName, string levelName, bool isAdditive, bool allowSceneActivation)
		{
			this.m_AssetBundleName = assetbundleName;
			this.m_LevelName = levelName;
			this.m_IsAdditive = isAdditive;
			this.m_allowSceneActivation = allowSceneActivation;
		}

		// Token: 0x06004CA3 RID: 19619 RVA: 0x001558DC File Offset: 0x00153CDC
		public string GetLevelName()
		{
			return this.m_LevelName;
		}

		// Token: 0x06004CA4 RID: 19620 RVA: 0x001558E4 File Offset: 0x00153CE4
		public bool IsAdditive()
		{
			return this.m_IsAdditive;
		}

		// Token: 0x06004CA5 RID: 19621 RVA: 0x001558EC File Offset: 0x00153CEC
		public bool allowSceneActivation()
		{
			return this.m_allowSceneActivation;
		}

		// Token: 0x06004CA6 RID: 19622 RVA: 0x001558F4 File Offset: 0x00153CF4
		public override bool Update()
		{
			if (this.m_asyncOperation != null)
			{
				return false;
			}
			LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(this.m_AssetBundleName, out this.m_DownloadingError);
			if (loadedAssetBundle != null)
			{
				if (this.m_IsAdditive)
				{
					this.m_asyncOperation = SceneManager.LoadSceneAsync(this.m_LevelName, LoadSceneMode.Additive);
				}
				else
				{
					this.m_asyncOperation = SceneManager.LoadSceneAsync(this.m_LevelName);
				}
				this.m_asyncOperation.allowSceneActivation = this.m_allowSceneActivation;
				return false;
			}
			return true;
		}

		// Token: 0x06004CA7 RID: 19623 RVA: 0x00155970 File Offset: 0x00153D70
		public override bool IsDone()
		{
			if (this.m_asyncOperation == null && this.m_DownloadingError != ErrorType.None)
			{
				Debug.Log(this.m_DownloadingError + " : " + this.m_AssetBundleName);
				return true;
			}
			if (this.m_asyncOperation == null)
			{
				return false;
			}
			if (this.m_asyncOperation.allowSceneActivation)
			{
				return this.m_asyncOperation.isDone;
			}
			return this.m_asyncOperation.progress >= 0.9f;
		}

		// Token: 0x04004D94 RID: 19860
		protected string m_LevelName;

		// Token: 0x04004D95 RID: 19861
		protected bool m_IsAdditive;

		// Token: 0x04004D96 RID: 19862
		protected bool m_allowSceneActivation;
	}
}
