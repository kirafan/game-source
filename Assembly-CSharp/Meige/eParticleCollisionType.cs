﻿using System;

namespace Meige
{
	// Token: 0x02000F87 RID: 3975
	public enum eParticleCollisionType : byte
	{
		// Token: 0x04005316 RID: 21270
		eParticleCollisionType_None,
		// Token: 0x04005317 RID: 21271
		eParticleCollisionType_Height,
		// Token: 0x04005318 RID: 21272
		eParticleCollisionType_Collision
	}
}
