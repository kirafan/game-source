﻿using System;

namespace Meige
{
	// Token: 0x02000F90 RID: 3984
	public enum eTextureMapType
	{
		// Token: 0x04005364 RID: 21348
		eTextureMapType_Std,
		// Token: 0x04005365 RID: 21349
		eTextureMapType_SSDistorsion,
		// Token: 0x04005366 RID: 21350
		eTextureMapType_Max
	}
}
