﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FB4 RID: 4020
	public class MsbADJoint
	{
		// Token: 0x06005388 RID: 21384 RVA: 0x00175F00 File Offset: 0x00174300
		public void Init(MsbArticulatedDynamicsHandler owner, MsbADJointParam src)
		{
			this.m_Owner = owner;
			this.m_Work = new MsbADJointParam(src);
			this.m_Velocity = Vector3.zero;
			this.m_fVelocity = 0f;
			this.m_WorldPosition = Vector3.zero;
			this.m_OldWorldPosition = Vector3.zero;
			MsbHierarchyNode msbHierarchyNodeByName = owner.GetOwner().GetMsbHierarchyNodeByName(this.GetHieName());
			this.m_ADHieCalc = new MsbADJoint.AD_HIE_CALC();
			this.m_ADHieCalc.Init(msbHierarchyNodeByName);
			this.m_KeepShapeAccel = this.m_ADHieCalc.m_Position;
			float num = Vector3.Dot(this.m_KeepShapeAccel, this.m_KeepShapeAccel);
			if (num > 1E-06f)
			{
				this.m_KeepShapeAccel.Normalize();
				this.m_KeepShapeAccel *= 9.80665f;
			}
			else
			{
				this.m_KeepShapeAccel = Vector3.zero;
			}
			this.m_Gravity.Set(0f, -9.80665f, 0f);
			this.m_Mass = 1f;
			this.m_AirFrictionCoef = this.m_Owner.GetAirFrictionCoef();
			this.m_AirFrictionCoefDivideByMass = this.m_AirFrictionCoef / this.m_Mass;
		}

		// Token: 0x06005389 RID: 21385 RVA: 0x0017601D File Offset: 0x0017441D
		public void SetParent(MsbADJoint parent)
		{
			this.m_Parent = parent;
			this.m_ADHieCalc.m_HieCalcParent = this.m_Parent.GetADHieCalc();
		}

		// Token: 0x0600538A RID: 21386 RVA: 0x0017603C File Offset: 0x0017443C
		public void StartFromScratch()
		{
			this.m_ADHieCalc.StartFromScratch();
		}

		// Token: 0x0600538B RID: 21387 RVA: 0x0017604C File Offset: 0x0017444C
		public void CopyFromHierarchy()
		{
			MsbHierarchyNode hierarchyNode = this.m_ADHieCalc.m_HierarchyNode;
			this.m_ADHieCalc.m_Rotation = hierarchyNode.GetTransform().localRotation;
			this.m_ADHieCalc.m_ScaleOfDistance = hierarchyNode.GetScaleOfDistance();
			this.m_ADHieCalc.MakeMatrix();
			this.m_WorldPosition = this.m_ADHieCalc.m_Matrix.GetColumn(3);
			this.m_OldWorldPosition = this.m_WorldPosition;
		}

		// Token: 0x0600538C RID: 21388 RVA: 0x001760C0 File Offset: 0x001744C0
		public void Prepare(Vector3 translateToDiscard, bool bStartFromScratch)
		{
			this.m_ADHieCalc.m_Rotation = Quaternion.identity;
			this.m_ADHieCalc.m_ScaleOfDistance = 1f;
			this.m_ADHieCalc.MakeMatrix();
			if (bStartFromScratch)
			{
				this.m_Velocity = Vector3.zero;
				this.m_fVelocity = 0f;
				this.m_WorldPosition = this.m_ADHieCalc.m_Matrix.GetColumn(3);
				this.m_OldWorldPosition = this.m_WorldPosition;
			}
			else if (this.IsLock())
			{
				this.m_Velocity = Vector3.zero;
				this.m_fVelocity = 0f;
				this.m_WorldPosition = this.GetHierarchyNode().GetTransform().localToWorldMatrix.GetColumn(3);
				this.m_OldWorldPosition = this.m_WorldPosition;
			}
			else
			{
				this.m_WorldPosition += translateToDiscard;
				this.m_OldWorldPosition += translateToDiscard;
			}
		}

		// Token: 0x0600538D RID: 21389 RVA: 0x001761BC File Offset: 0x001745BC
		public void ExternalForce(float fDt)
		{
			this.Collided(false);
			if (this.IsLock())
			{
				return;
			}
			Vector3 a = Vector3.zero;
			float keepShapeCoef = this.m_Owner.GetKeepShapeCoef();
			if (!this.IsRoot() && keepShapeCoef > 0f)
			{
				Vector3 a2 = this.m_ADHieCalc.m_Matrix.MultiplyVector(this.m_KeepShapeAccel);
				a += a2 * keepShapeCoef;
				if (this.IsHasGravity())
				{
					a += this.m_Gravity * (1f - keepShapeCoef);
				}
			}
			else if (this.IsHasGravity())
			{
				a += this.m_Gravity;
			}
			if (this.IsHasAirFriction())
			{
				a -= this.m_Velocity * this.m_AirFrictionCoefDivideByMass;
			}
			Vector3 b = a * fDt;
			this.m_Velocity += b;
			float num = Vector3.Dot(this.m_Velocity, this.m_Velocity);
			if (num > 1E-06f)
			{
				this.m_fVelocity = Mathf.Sqrt(num);
			}
			else
			{
				this.m_fVelocity = 0f;
				this.m_Velocity = Vector3.zero;
			}
			Vector3 b2 = this.m_Velocity * fDt;
			this.m_WorldPosition += b2;
		}

		// Token: 0x0600538E RID: 21390 RVA: 0x00176310 File Offset: 0x00174710
		public void Finalyze(float fDtRepci, bool bAfterIKSolve)
		{
			if (bAfterIKSolve)
			{
				this.m_ADHieCalc.MakeMatrix();
				this.m_WorldPosition = this.m_ADHieCalc.m_Matrix.GetColumn(3);
			}
			Vector3 a = this.m_WorldPosition - this.m_OldWorldPosition;
			Vector3 vector = a * fDtRepci;
			float num = Vector3.Dot(vector, vector);
			float num2 = (num <= 1E-06f) ? 0f : Mathf.Sqrt(num);
			if (num2 > 1E-06f)
			{
				this.m_fVelocity = Mathf.Min(this.m_fVelocity, num2);
				vector *= this.m_fVelocity / num2;
				this.m_Velocity = vector;
			}
			else
			{
				this.m_fVelocity = 0f;
				this.m_Velocity = Vector3.zero;
			}
			this.m_OldWorldPosition = this.m_WorldPosition;
		}

		// Token: 0x0600538F RID: 21391 RVA: 0x001763E4 File Offset: 0x001747E4
		public void Flush(bool bAnimBlend)
		{
			MsbHierarchyNode hierarchyNode = this.GetHierarchyNode();
			hierarchyNode.GetTransform().localRotation = this.m_ADHieCalc.m_Rotation;
			hierarchyNode.SetScaleOfDistance(this.m_ADHieCalc.m_ScaleOfDistance);
			if (bAnimBlend)
			{
				this.m_WorldPosition = this.m_ADHieCalc.m_Matrix.GetColumn(3);
				this.m_OldWorldPosition = this.m_WorldPosition;
			}
		}

		// Token: 0x06005390 RID: 21392 RVA: 0x0017644D File Offset: 0x0017484D
		public string GetHieName()
		{
			return this.m_Work.m_HieName;
		}

		// Token: 0x06005391 RID: 21393 RVA: 0x0017645A File Offset: 0x0017485A
		public bool IsLock()
		{
			return this.m_Work.m_bLock;
		}

		// Token: 0x06005392 RID: 21394 RVA: 0x00176467 File Offset: 0x00174867
		public bool IsRoot()
		{
			return this.m_Work.m_bRoot;
		}

		// Token: 0x06005393 RID: 21395 RVA: 0x00176474 File Offset: 0x00174874
		public bool IsHasGravity()
		{
			return this.m_Work.m_bGravity;
		}

		// Token: 0x06005394 RID: 21396 RVA: 0x00176481 File Offset: 0x00174881
		public bool IsHasAirFriction()
		{
			return this.m_Work.m_bAirFrictiion;
		}

		// Token: 0x06005395 RID: 21397 RVA: 0x0017648E File Offset: 0x0017488E
		public bool IsIKLinkStretch()
		{
			return this.m_Work.m_bILKinkStretch;
		}

		// Token: 0x06005396 RID: 21398 RVA: 0x0017649B File Offset: 0x0017489B
		public void SetLock(bool bLock)
		{
			this.m_Work.m_bLock = bLock;
		}

		// Token: 0x06005397 RID: 21399 RVA: 0x001764A9 File Offset: 0x001748A9
		public void EnableIKLinkStrecth(bool bStretch)
		{
			this.m_Work.m_bILKinkStretch = bStretch;
		}

		// Token: 0x06005398 RID: 21400 RVA: 0x001764B7 File Offset: 0x001748B7
		public Vector3 GetWorldPosition()
		{
			return this.m_WorldPosition;
		}

		// Token: 0x06005399 RID: 21401 RVA: 0x001764BF File Offset: 0x001748BF
		public void SetWorldPosition(Vector3 vPos)
		{
			this.m_WorldPosition = vPos;
		}

		// Token: 0x0600539A RID: 21402 RVA: 0x001764C8 File Offset: 0x001748C8
		public Vector3 GetOldWorldPosition()
		{
			return this.m_OldWorldPosition;
		}

		// Token: 0x0600539B RID: 21403 RVA: 0x001764D0 File Offset: 0x001748D0
		public void SetOldWorldPosition(Vector3 vPos)
		{
			this.m_OldWorldPosition = vPos;
		}

		// Token: 0x0600539C RID: 21404 RVA: 0x001764D9 File Offset: 0x001748D9
		public Vector3 GetVelocity()
		{
			return this.m_Velocity;
		}

		// Token: 0x0600539D RID: 21405 RVA: 0x001764E1 File Offset: 0x001748E1
		public void SetVelocity(Vector3 vVelocity)
		{
			this.m_Velocity = vVelocity;
		}

		// Token: 0x0600539E RID: 21406 RVA: 0x001764EC File Offset: 0x001748EC
		public bool IsStable()
		{
			return Mathf.Abs(this.m_Velocity.x) <= 0.05f && Mathf.Abs(this.m_Velocity.y) <= 0.05f && Mathf.Abs(this.m_Velocity.z) <= 0.05f;
		}

		// Token: 0x0600539F RID: 21407 RVA: 0x0017654A File Offset: 0x0017494A
		public MsbArticulatedDynamicsHandler GetOwner()
		{
			return this.m_Owner;
		}

		// Token: 0x060053A0 RID: 21408 RVA: 0x00176552 File Offset: 0x00174952
		public MsbHierarchyNode GetHierarchyNode()
		{
			return this.m_ADHieCalc.m_HierarchyNode;
		}

		// Token: 0x060053A1 RID: 21409 RVA: 0x0017655F File Offset: 0x0017495F
		public MsbADJoint.AD_HIE_CALC GetADHieCalc()
		{
			return this.m_ADHieCalc;
		}

		// Token: 0x060053A2 RID: 21410 RVA: 0x00176567 File Offset: 0x00174967
		public Vector3 GetGravity()
		{
			return this.m_Gravity;
		}

		// Token: 0x060053A3 RID: 21411 RVA: 0x0017656F File Offset: 0x0017496F
		public void SetGravity(ref Vector3 gravity)
		{
			this.m_Gravity = gravity;
		}

		// Token: 0x060053A4 RID: 21412 RVA: 0x0017657D File Offset: 0x0017497D
		public float GetMass()
		{
			return this.m_Mass;
		}

		// Token: 0x060053A5 RID: 21413 RVA: 0x00176585 File Offset: 0x00174985
		public float GetAirfrictionCoef()
		{
			return this.m_AirFrictionCoef;
		}

		// Token: 0x060053A6 RID: 21414 RVA: 0x0017658D File Offset: 0x0017498D
		public float GetAirfrictionCoefDivideByMass()
		{
			return this.m_AirFrictionCoefDivideByMass;
		}

		// Token: 0x060053A7 RID: 21415 RVA: 0x00176595 File Offset: 0x00174995
		public int GetNumberOfIKGroup()
		{
			return this.m_Work.m_NumOfIKGroup;
		}

		// Token: 0x060053A8 RID: 21416 RVA: 0x001765A2 File Offset: 0x001749A2
		public int GetParentIdxOfADHierarchy()
		{
			return this.m_Work.m_ParentIdxOfADHierarchy;
		}

		// Token: 0x060053A9 RID: 21417 RVA: 0x001765AF File Offset: 0x001749AF
		public void Collided(bool bCollided)
		{
			this.m_bCollided = bCollided;
		}

		// Token: 0x060053AA RID: 21418 RVA: 0x001765B8 File Offset: 0x001749B8
		public bool IsCollided()
		{
			return this.m_bCollided;
		}

		// Token: 0x0400546D RID: 21613
		private MsbArticulatedDynamicsHandler m_Owner;

		// Token: 0x0400546E RID: 21614
		private MsbADJoint m_Parent;

		// Token: 0x0400546F RID: 21615
		private MsbADJointParam m_Work;

		// Token: 0x04005470 RID: 21616
		private MsbADJoint.AD_HIE_CALC m_ADHieCalc;

		// Token: 0x04005471 RID: 21617
		private Vector3 m_WorldPosition;

		// Token: 0x04005472 RID: 21618
		private Vector3 m_OldWorldPosition;

		// Token: 0x04005473 RID: 21619
		private Vector3 m_Velocity;

		// Token: 0x04005474 RID: 21620
		private Vector3 m_Gravity;

		// Token: 0x04005475 RID: 21621
		private Vector3 m_KeepShapeAccel;

		// Token: 0x04005476 RID: 21622
		private float m_Mass;

		// Token: 0x04005477 RID: 21623
		private float m_AirFrictionCoef;

		// Token: 0x04005478 RID: 21624
		private float m_AirFrictionCoefDivideByMass;

		// Token: 0x04005479 RID: 21625
		private float m_fVelocity;

		// Token: 0x0400547A RID: 21626
		private bool m_bCollided;

		// Token: 0x0400547B RID: 21627
		private bool m_bAvailable;

		// Token: 0x02000FB5 RID: 4021
		public class AD_HIE_CALC
		{
			// Token: 0x060053AC RID: 21420 RVA: 0x001765C8 File Offset: 0x001749C8
			public void Init(MsbHierarchyNode msbHieNode)
			{
				this.m_HierarchyNode = msbHieNode;
				this.StartFromScratch();
				this.m_Position = this.m_HierarchyNode.GetTransform().localPosition;
				this.m_Rotation = this.m_HierarchyNode.GetTransform().localRotation;
				this.m_Scale = this.m_HierarchyNode.GetTransform().localScale;
			}

			// Token: 0x060053AD RID: 21421 RVA: 0x00176624 File Offset: 0x00174A24
			public void StartFromScratch()
			{
				this.m_HierarchyNode.Revert();
				this.m_ScaleOfDistance = 1f;
			}

			// Token: 0x060053AE RID: 21422 RVA: 0x0017663C File Offset: 0x00174A3C
			public void GetParentMatrix(out Matrix4x4 parentMtx)
			{
				if (this.m_HieCalcParent != null)
				{
					parentMtx = this.m_HieCalcParent.GetMatrix();
				}
				else if (this.m_HierarchyNode != null)
				{
					parentMtx = this.m_HierarchyNode.GetTransform().parent.localToWorldMatrix;
				}
				else
				{
					parentMtx = Matrix4x4.identity;
				}
			}

			// Token: 0x060053AF RID: 21423 RVA: 0x001766A8 File Offset: 0x00174AA8
			public void MakeMatrix()
			{
				if (this.m_HieCalcParent != null)
				{
					MsbHierarchyNode.EzMatrixCalcFunction(out this.m_Matrix, ref this.m_Position, ref this.m_Rotation, ref this.m_Scale, this.m_ScaleOfDistance, this.m_HieCalcParent.m_Matrix);
				}
				else if (this.m_HierarchyNode != null)
				{
					MsbHierarchyNode.EzMatrixCalcFunction(out this.m_Matrix, ref this.m_Position, ref this.m_Rotation, ref this.m_Scale, this.m_ScaleOfDistance, this.m_HierarchyNode.GetTransform().parent.localToWorldMatrix);
				}
				else
				{
					MsbHierarchyNode.EzMatrixCalcFunction(out this.m_Matrix, ref this.m_Position, ref this.m_Rotation, ref this.m_Scale, this.m_ScaleOfDistance, Matrix4x4.identity);
				}
			}

			// Token: 0x060053B0 RID: 21424 RVA: 0x00176769 File Offset: 0x00174B69
			public Matrix4x4 GetMatrix()
			{
				return this.m_Matrix;
			}

			// Token: 0x0400547C RID: 21628
			public MsbHierarchyNode m_HierarchyNode;

			// Token: 0x0400547D RID: 21629
			public MsbADJoint.AD_HIE_CALC m_HieCalcParent;

			// Token: 0x0400547E RID: 21630
			public float m_ScaleOfDistance;

			// Token: 0x0400547F RID: 21631
			public Vector3 m_Position;

			// Token: 0x04005480 RID: 21632
			public Quaternion m_Rotation;

			// Token: 0x04005481 RID: 21633
			public Vector3 m_Scale;

			// Token: 0x04005482 RID: 21634
			public Matrix4x4 m_Matrix;
		}
	}
}
