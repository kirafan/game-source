﻿using System;

namespace Meige
{
	// Token: 0x02000EEE RID: 3822
	[Serializable]
	public class EffectTimeLineSourceCurve
	{
		// Token: 0x04004E80 RID: 20096
		public EffectComponentBase m_Target;

		// Token: 0x04004E81 RID: 20097
		public string m_ParamPropertyName;

		// Token: 0x04004E82 RID: 20098
		public int m_ArrayIdx;

		// Token: 0x04004E83 RID: 20099
		public int m_TargetIdx;

		// Token: 0x04004E84 RID: 20100
		public int m_ComponentIdx;

		// Token: 0x04004E85 RID: 20101
		public EffectTimeLineKeyFrame[] m_KeyFrameArray;
	}
}
