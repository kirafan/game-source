﻿using System;

namespace Meige
{
	// Token: 0x02000F8D RID: 3981
	public enum eTextureType
	{
		// Token: 0x04005353 RID: 21331
		eTextureType_Invalid = -1,
		// Token: 0x04005354 RID: 21332
		eTextureType_Albedo,
		// Token: 0x04005355 RID: 21333
		eTextureType_Normal,
		// Token: 0x04005356 RID: 21334
		eTextureType_Specular,
		// Token: 0x04005357 RID: 21335
		eTextureType_Additional_Start,
		// Token: 0x04005358 RID: 21336
		eTextureType_Additional0 = 3,
		// Token: 0x04005359 RID: 21337
		eTextureType_Max
	}
}
