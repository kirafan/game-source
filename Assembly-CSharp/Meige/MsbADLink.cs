﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FB7 RID: 4023
	public class MsbADLink
	{
		// Token: 0x060053B4 RID: 21428 RVA: 0x001767D4 File Offset: 0x00174BD4
		public void Init(MsbArticulatedDynamicsHandler owner, MsbADLinkParam src)
		{
			this.m_Owner = owner;
			this.m_Work = new MsbADLinkParam(src);
			this.m_SrcJoint = this.m_Owner.GetJoint(this.GetSrcJointIndex());
			this.m_TgtJoint = this.m_Owner.GetJoint(this.GetTgtJointIndex());
			this.m_LengthForConstraint = this.GetLength();
			if (this.m_TgtJoint.GetADHieCalc().m_HieCalcParent == this.m_SrcJoint.GetADHieCalc())
			{
				this.m_InternalForceCoef = 1f;
				if (this.GetLinkType() == eADLinkType.eADLinkType_Spring)
				{
					this.m_TgtJoint.EnableIKLinkStrecth(true);
				}
			}
			else
			{
				this.m_InternalForceCoef = this.m_Owner.GetLinkConstraintForce();
			}
		}

		// Token: 0x060053B5 RID: 21429 RVA: 0x00176888 File Offset: 0x00174C88
		public void Prepare()
		{
			Vector3 one = Vector3.one;
			float magnitude = one.magnitude;
			float magnitude2 = this.m_SrcJoint.GetADHieCalc().m_Matrix.MultiplyVector(one).magnitude;
			float num = magnitude2 / magnitude;
			this.m_LengthForConstraint = this.GetLength() * num;
		}

		// Token: 0x060053B6 RID: 21430 RVA: 0x001768D4 File Offset: 0x00174CD4
		public float ConstraintForce(float fDt)
		{
			eADLinkType linkType = this.GetLinkType();
			if (linkType == eADLinkType.eADLinkType_Standard)
			{
				return this.ConstraintForceStandard();
			}
			if (linkType != eADLinkType.eADLinkType_Spring)
			{
				return 0f;
			}
			return this.ConstraintForceSpring(fDt);
		}

		// Token: 0x060053B7 RID: 21431 RVA: 0x0017690E File Offset: 0x00174D0E
		public eADLinkType GetLinkType()
		{
			return this.m_Work.m_LinkType;
		}

		// Token: 0x060053B8 RID: 21432 RVA: 0x0017691B File Offset: 0x00174D1B
		public int GetSrcJointIndex()
		{
			return this.m_Work.m_SrcADJointIdx;
		}

		// Token: 0x060053B9 RID: 21433 RVA: 0x00176928 File Offset: 0x00174D28
		public int GetTgtJointIndex()
		{
			return this.m_Work.m_DstADJointIdx;
		}

		// Token: 0x060053BA RID: 21434 RVA: 0x00176935 File Offset: 0x00174D35
		public float GetLength()
		{
			return this.m_Work.m_Length;
		}

		// Token: 0x060053BB RID: 21435 RVA: 0x00176942 File Offset: 0x00174D42
		public float GetRadius()
		{
			return this.m_Work.m_Radius;
		}

		// Token: 0x060053BC RID: 21436 RVA: 0x0017694F File Offset: 0x00174D4F
		public MsbADJoint GetSrcJoint()
		{
			return this.m_SrcJoint;
		}

		// Token: 0x060053BD RID: 21437 RVA: 0x00176957 File Offset: 0x00174D57
		public MsbADJoint GetTgtJoint()
		{
			return this.m_TgtJoint;
		}

		// Token: 0x060053BE RID: 21438 RVA: 0x00176960 File Offset: 0x00174D60
		public float ConstraintForceStandard()
		{
			Vector3 worldPosition = this.m_SrcJoint.GetWorldPosition();
			Vector3 worldPosition2 = this.m_TgtJoint.GetWorldPosition();
			float num;
			float num2;
			if (this.m_SrcJoint.IsLock())
			{
				num = 1f;
				num2 = 0f;
			}
			else if (this.m_TgtJoint.IsLock())
			{
				num = 0f;
				num2 = 1f;
			}
			else
			{
				num = this.m_SrcJoint.GetMass();
				num2 = this.m_TgtJoint.GetMass();
			}
			Vector3 a = worldPosition2 - worldPosition;
			float magnitude = a.magnitude;
			float num3 = magnitude - this.m_LengthForConstraint;
			float num4 = num3 * this.m_InternalForceCoef;
			if (num4 <= 0f)
			{
				return 0f;
			}
			Vector3 a2 = a / magnitude;
			float num5 = num + num2;
			float num6 = num4 / num5;
			float d = num6 * num2;
			float d2 = num6 * num;
			Vector3 b = a2 * d;
			Vector3 b2 = a2 * d2;
			Vector3 worldPosition3 = worldPosition + b;
			Vector3 worldPosition4 = worldPosition2 - b2;
			this.m_SrcJoint.SetWorldPosition(worldPosition3);
			this.m_TgtJoint.SetWorldPosition(worldPosition4);
			return num4;
		}

		// Token: 0x060053BF RID: 21439 RVA: 0x00176A84 File Offset: 0x00174E84
		public float ConstraintForceSpring(float fDt)
		{
			Vector3 worldPosition = this.m_SrcJoint.GetWorldPosition();
			Vector3 worldPosition2 = this.m_TgtJoint.GetWorldPosition();
			float num;
			float num2;
			if (this.m_SrcJoint.IsLock())
			{
				num = 1f;
				num2 = 0f;
			}
			else if (this.m_TgtJoint.IsLock())
			{
				num = 0f;
				num2 = 1f;
			}
			else
			{
				num = this.m_SrcJoint.GetMass();
				num2 = this.m_TgtJoint.GetMass();
			}
			Vector3 a = worldPosition2 - worldPosition;
			float magnitude = a.magnitude;
			float num3 = magnitude - this.m_LengthForConstraint;
			if (num3 <= 1E-06f)
			{
				return 0f;
			}
			Vector3 a2 = a / magnitude;
			float num4 = num + num2;
			float num5 = num3 * this.m_InternalForceCoef / num4;
			float num6 = num5 * fDt;
			Vector3 a3 = a2 * (num6 * num2 / num4);
			Vector3 a4 = a2 * (num6 * num / num4);
			Vector3 worldPosition3 = worldPosition + a3 * fDt;
			Vector3 worldPosition4 = worldPosition2 - a4 * fDt;
			this.m_SrcJoint.SetWorldPosition(worldPosition3);
			this.m_TgtJoint.SetWorldPosition(worldPosition4);
			return num6;
		}

		// Token: 0x04005488 RID: 21640
		private MsbArticulatedDynamicsHandler m_Owner;

		// Token: 0x04005489 RID: 21641
		private MsbADLinkParam m_Work;

		// Token: 0x0400548A RID: 21642
		private MsbADJoint m_SrcJoint;

		// Token: 0x0400548B RID: 21643
		private MsbADJoint m_TgtJoint;

		// Token: 0x0400548C RID: 21644
		private float m_InternalForceCoef;

		// Token: 0x0400548D RID: 21645
		private float m_LengthForConstraint;
	}
}
