﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EA1 RID: 3745
	public class EffectRenderer : MonoBehaviour
	{
		// Token: 0x06004E3B RID: 20027 RVA: 0x0015BFDC File Offset: 0x0015A3DC
		private void Start()
		{
			EffectObjectManager.Instance.RegisterRendererComponent(this);
			if (this.m_MeshRenderer == null)
			{
				this.m_MeshRenderer = base.gameObject.AddComponent<MeshRenderer>();
				this.m_MeshRenderer.sortingOrder = this.m_SortingOrder;
				this.m_MeshRenderer.enabled = this.m_isEnabled;
			}
			base.enabled = this.m_isEnabled;
		}

		// Token: 0x06004E3C RID: 20028 RVA: 0x0015C045 File Offset: 0x0015A445
		private void OnDestroy()
		{
			if (EffectObjectManager.Instance != null)
			{
				EffectObjectManager.Instance.RemoveRendererComponent(this);
			}
		}

		// Token: 0x06004E3D RID: 20029 RVA: 0x0015C063 File Offset: 0x0015A463
		private void OnEnable()
		{
			if (this.m_MeshRenderer != null)
			{
				this.m_MeshRenderer.enabled = true;
			}
			this.m_isEnabled = true;
		}

		// Token: 0x06004E3E RID: 20030 RVA: 0x0015C089 File Offset: 0x0015A489
		private void OnDisable()
		{
			if (this.m_MeshRenderer != null)
			{
				this.m_MeshRenderer.enabled = false;
			}
			this.m_isEnabled = false;
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06004E40 RID: 20032 RVA: 0x0015C108 File Offset: 0x0015A508
		// (set) Token: 0x06004E3F RID: 20031 RVA: 0x0015C0B0 File Offset: 0x0015A4B0
		public Material material
		{
			get
			{
				return this.m_Material;
			}
			set
			{
				this.m_Material = value;
				if (this.m_MeshRenderer == null)
				{
					this.m_MeshRenderer = base.gameObject.AddComponent<MeshRenderer>();
					this.m_MeshRenderer.sortingOrder = this.m_SortingOrder;
				}
				this.m_MeshRenderer.material = this.m_Material;
			}
		}

		// Token: 0x06004E41 RID: 20033 RVA: 0x0015C110 File Offset: 0x0015A510
		public void SetSortingOrder(int sortingOrder)
		{
			this.m_SortingOrder = sortingOrder;
			if (this.m_MeshRenderer == null)
			{
				return;
			}
			this.m_MeshRenderer.sortingOrder = this.m_SortingOrder;
		}

		// Token: 0x04004E57 RID: 20055
		[SerializeField]
		private Material m_Material;

		// Token: 0x04004E58 RID: 20056
		private MeshRenderer m_MeshRenderer;

		// Token: 0x04004E59 RID: 20057
		private bool m_isEnabled = true;

		// Token: 0x04004E5A RID: 20058
		private int m_SortingOrder;
	}
}
