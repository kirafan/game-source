﻿using System;

namespace Meige
{
	// Token: 0x02000FCF RID: 4047
	public enum eBloomReferenceType
	{
		// Token: 0x04005538 RID: 21816
		eBloomReferenceType_ToneLuminanceValue,
		// Token: 0x04005539 RID: 21817
		eBloomReferenceType_Fix
	}
}
