﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F06 RID: 3846
	[Serializable]
	public class ParticleColorCurve
	{
		// Token: 0x04004EAF RID: 20143
		public Color[] m_Color;
	}
}
