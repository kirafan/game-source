﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F07 RID: 3847
	[Serializable]
	public struct s32 : IComparable, IComparable<s32>, IOperatorFast<s32>
	{
		// Token: 0x06004F43 RID: 20291 RVA: 0x0015DED1 File Offset: 0x0015C2D1
		public s32(int value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004F44 RID: 20292 RVA: 0x0015DEDA File Offset: 0x0015C2DA
		public s32(s32 value)
		{
			this.m_Value = value.value;
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06004F46 RID: 20294 RVA: 0x0015DEF2 File Offset: 0x0015C2F2
		// (set) Token: 0x06004F45 RID: 20293 RVA: 0x0015DEE9 File Offset: 0x0015C2E9
		public int value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x06004F47 RID: 20295 RVA: 0x0015DEFA File Offset: 0x0015C2FA
		public static implicit operator s32(int v)
		{
			return new s32(v);
		}

		// Token: 0x06004F48 RID: 20296 RVA: 0x0015DF02 File Offset: 0x0015C302
		public static implicit operator int(s32 v)
		{
			return v.value;
		}

		// Token: 0x06004F49 RID: 20297 RVA: 0x0015DF0B File Offset: 0x0015C30B
		public void Add(s32 a, s32 b)
		{
			this = a + b;
		}

		// Token: 0x06004F4A RID: 20298 RVA: 0x0015DF1A File Offset: 0x0015C31A
		public void Sub(s32 a, s32 b)
		{
			this = a - b;
		}

		// Token: 0x06004F4B RID: 20299 RVA: 0x0015DF29 File Offset: 0x0015C329
		public void Mul(s32 a, s32 b)
		{
			this = a * b;
		}

		// Token: 0x06004F4C RID: 20300 RVA: 0x0015DF38 File Offset: 0x0015C338
		public void Mul(s32 a, float b)
		{
			this = a * b;
		}

		// Token: 0x06004F4D RID: 20301 RVA: 0x0015DF47 File Offset: 0x0015C347
		public void Div(s32 a, s32 b)
		{
			this = a / b;
		}

		// Token: 0x06004F4E RID: 20302 RVA: 0x0015DF56 File Offset: 0x0015C356
		public void Add(s32 b)
		{
			this += b;
		}

		// Token: 0x06004F4F RID: 20303 RVA: 0x0015DF6A File Offset: 0x0015C36A
		public void Sub(s32 b)
		{
			this -= b;
		}

		// Token: 0x06004F50 RID: 20304 RVA: 0x0015DF7E File Offset: 0x0015C37E
		public void Mul(s32 b)
		{
			this *= b;
		}

		// Token: 0x06004F51 RID: 20305 RVA: 0x0015DF92 File Offset: 0x0015C392
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x06004F52 RID: 20306 RVA: 0x0015DFA6 File Offset: 0x0015C3A6
		public void Div(s32 b)
		{
			this *= b;
		}

		// Token: 0x06004F53 RID: 20307 RVA: 0x0015DFBA File Offset: 0x0015C3BA
		public void SetRowValue(object o)
		{
			this.m_Value = (int)o;
		}

		// Token: 0x06004F54 RID: 20308 RVA: 0x0015DFC8 File Offset: 0x0015C3C8
		public int CompareTo(object obj)
		{
			float? num = obj as float?;
			if (num == null)
			{
				return 1;
			}
			return this.CompareTo(num);
		}

		// Token: 0x06004F55 RID: 20309 RVA: 0x0015DFFE File Offset: 0x0015C3FE
		public int CompareTo(s32 target)
		{
			if (target.value == this.m_Value)
			{
				return 0;
			}
			if (target.value < this.m_Value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06004F56 RID: 20310 RVA: 0x0015E02C File Offset: 0x0015C42C
		public override bool Equals(object obj)
		{
			float? num = obj as float?;
			return num != null && this.Equals(num);
		}

		// Token: 0x06004F57 RID: 20311 RVA: 0x0015E068 File Offset: 0x0015C468
		public bool Equals(float obj)
		{
			return (float)this.m_Value == obj;
		}

		// Token: 0x06004F58 RID: 20312 RVA: 0x0015E074 File Offset: 0x0015C474
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06004F59 RID: 20313 RVA: 0x0015E087 File Offset: 0x0015C487
		public static s32 operator +(s32 v0)
		{
			return v0;
		}

		// Token: 0x06004F5A RID: 20314 RVA: 0x0015E08C File Offset: 0x0015C48C
		public static s32 operator +(s32 v0, s32 v1)
		{
			s32 result;
			result.m_Value = v0.m_Value + v1.m_Value;
			return result;
		}

		// Token: 0x06004F5B RID: 20315 RVA: 0x0015E0B0 File Offset: 0x0015C4B0
		public static s32 operator -(s32 v0)
		{
			s32 result;
			result.m_Value = -v0.m_Value;
			return result;
		}

		// Token: 0x06004F5C RID: 20316 RVA: 0x0015E0D0 File Offset: 0x0015C4D0
		public static s32 operator -(s32 v0, s32 v1)
		{
			s32 result;
			result.m_Value = v0.m_Value - v1.m_Value;
			return result;
		}

		// Token: 0x06004F5D RID: 20317 RVA: 0x0015E0F4 File Offset: 0x0015C4F4
		public static s32 operator *(s32 v0, s32 v1)
		{
			s32 result;
			result.m_Value = v0.m_Value * v1.m_Value;
			return result;
		}

		// Token: 0x06004F5E RID: 20318 RVA: 0x0015E118 File Offset: 0x0015C518
		public static s32 operator *(s32 v0, float v1)
		{
			s32 result;
			result.m_Value = (int)((float)v0.m_Value * v1);
			return result;
		}

		// Token: 0x06004F5F RID: 20319 RVA: 0x0015E138 File Offset: 0x0015C538
		public static s32 operator /(s32 v0, s32 v1)
		{
			s32 result;
			result.m_Value = v0.m_Value / v1.m_Value;
			return result;
		}

		// Token: 0x06004F60 RID: 20320 RVA: 0x0015E15C File Offset: 0x0015C55C
		public static s32 operator /(s32 v0, float v1)
		{
			s32 result;
			result.m_Value = (int)((float)v0.m_Value / v1);
			return result;
		}

		// Token: 0x06004F61 RID: 20321 RVA: 0x0015E17C File Offset: 0x0015C57C
		public static bool operator ==(s32 v0, s32 v1)
		{
			return v0.m_Value == v1.m_Value;
		}

		// Token: 0x06004F62 RID: 20322 RVA: 0x0015E18E File Offset: 0x0015C58E
		public static bool operator !=(s32 v0, s32 v1)
		{
			return v0.m_Value != v1.m_Value;
		}

		// Token: 0x04004EB0 RID: 20144
		[SerializeField]
		public int m_Value;
	}
}
