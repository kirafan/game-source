﻿using System;

namespace Meige
{
	// Token: 0x02000FEF RID: 4079
	public class TrophyHandlerBase
	{
		// Token: 0x040055EE RID: 21998
		public eTrophyProcessType m_processType;

		// Token: 0x040055EF RID: 21999
		public eTrophyResult m_result;

		// Token: 0x040055F0 RID: 22000
		public bool m_isAvailable;
	}
}
