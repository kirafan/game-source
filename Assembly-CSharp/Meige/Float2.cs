﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F0A RID: 3850
	[Serializable]
	public struct Float2 : IComparable, IComparable<Float2>, IOperatorFast<Float2>
	{
		// Token: 0x06004FA3 RID: 20387 RVA: 0x0015E75E File Offset: 0x0015CB5E
		public Float2(Float2 value)
		{
			this.m_Value = value.m_Value;
		}

		// Token: 0x06004FA4 RID: 20388 RVA: 0x0015E76D File Offset: 0x0015CB6D
		public Float2(Vector2 value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004FA5 RID: 20389 RVA: 0x0015E776 File Offset: 0x0015CB76
		public Float2(float x, float y)
		{
			this.m_Value.x = x;
			this.m_Value.y = y;
		}

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06004FA7 RID: 20391 RVA: 0x0015E799 File Offset: 0x0015CB99
		// (set) Token: 0x06004FA6 RID: 20390 RVA: 0x0015E790 File Offset: 0x0015CB90
		public Vector2 value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06004FA9 RID: 20393 RVA: 0x0015E7AF File Offset: 0x0015CBAF
		// (set) Token: 0x06004FA8 RID: 20392 RVA: 0x0015E7A1 File Offset: 0x0015CBA1
		public float x
		{
			get
			{
				return this.m_Value.x;
			}
			set
			{
				this.m_Value.x = value;
			}
		}

		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06004FAB RID: 20395 RVA: 0x0015E7CA File Offset: 0x0015CBCA
		// (set) Token: 0x06004FAA RID: 20394 RVA: 0x0015E7BC File Offset: 0x0015CBBC
		public float y
		{
			get
			{
				return this.m_Value.y;
			}
			set
			{
				this.m_Value.y = value;
			}
		}

		// Token: 0x17000547 RID: 1351
		public float this[int idx]
		{
			get
			{
				return this.m_Value[idx];
			}
			set
			{
				this.m_Value[idx] = value;
			}
		}

		// Token: 0x06004FAE RID: 20398 RVA: 0x0015E7F4 File Offset: 0x0015CBF4
		public static implicit operator Float2(Vector2 v)
		{
			return new Float2(v);
		}

		// Token: 0x06004FAF RID: 20399 RVA: 0x0015E7FC File Offset: 0x0015CBFC
		public static implicit operator Vector2(Float2 v)
		{
			return v.value;
		}

		// Token: 0x06004FB0 RID: 20400 RVA: 0x0015E805 File Offset: 0x0015CC05
		public void Add(Float2 a, Float2 b)
		{
			this = a + b;
		}

		// Token: 0x06004FB1 RID: 20401 RVA: 0x0015E814 File Offset: 0x0015CC14
		public void Sub(Float2 a, Float2 b)
		{
			this = a - b;
		}

		// Token: 0x06004FB2 RID: 20402 RVA: 0x0015E823 File Offset: 0x0015CC23
		public void Mul(Float2 a, Float2 b)
		{
			this = a * b;
		}

		// Token: 0x06004FB3 RID: 20403 RVA: 0x0015E832 File Offset: 0x0015CC32
		public void Mul(Float2 a, float b)
		{
			this = a * b;
		}

		// Token: 0x06004FB4 RID: 20404 RVA: 0x0015E841 File Offset: 0x0015CC41
		public void Div(Float2 a, Float2 b)
		{
			this = a / b;
		}

		// Token: 0x06004FB5 RID: 20405 RVA: 0x0015E850 File Offset: 0x0015CC50
		public void Add(Float2 b)
		{
			this += b;
		}

		// Token: 0x06004FB6 RID: 20406 RVA: 0x0015E864 File Offset: 0x0015CC64
		public void Sub(Float2 b)
		{
			this -= b;
		}

		// Token: 0x06004FB7 RID: 20407 RVA: 0x0015E878 File Offset: 0x0015CC78
		public void Mul(Float2 b)
		{
			this *= b;
		}

		// Token: 0x06004FB8 RID: 20408 RVA: 0x0015E88C File Offset: 0x0015CC8C
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x06004FB9 RID: 20409 RVA: 0x0015E8A0 File Offset: 0x0015CCA0
		public void Div(Float2 b)
		{
			this *= b;
		}

		// Token: 0x06004FBA RID: 20410 RVA: 0x0015E8B4 File Offset: 0x0015CCB4
		public void SetRowValue(object o)
		{
			this.m_Value = (Vector2)o;
		}

		// Token: 0x06004FBB RID: 20411 RVA: 0x0015E8C2 File Offset: 0x0015CCC2
		public void SetRowValue(float o, int componentIdx)
		{
			this.m_Value[componentIdx] = o;
		}

		// Token: 0x06004FBC RID: 20412 RVA: 0x0015E8D4 File Offset: 0x0015CCD4
		public int CompareTo(object obj)
		{
			Float2? @float = obj as Float2?;
			if (@float == null)
			{
				return 1;
			}
			return this.CompareTo(@float);
		}

		// Token: 0x06004FBD RID: 20413 RVA: 0x0015E90A File Offset: 0x0015CD0A
		public int CompareTo(Float2 target)
		{
			if (target.x == this.x && target.y == this.y)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06004FBE RID: 20414 RVA: 0x0015E934 File Offset: 0x0015CD34
		public override bool Equals(object obj)
		{
			Float2? @float = obj as Float2?;
			return @float != null && this.Equals(@float);
		}

		// Token: 0x06004FBF RID: 20415 RVA: 0x0015E970 File Offset: 0x0015CD70
		public bool Equals(Float2 obj)
		{
			return this == obj;
		}

		// Token: 0x06004FC0 RID: 20416 RVA: 0x0015E97E File Offset: 0x0015CD7E
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06004FC1 RID: 20417 RVA: 0x0015E991 File Offset: 0x0015CD91
		public static Float2 operator +(Float2 v0)
		{
			return v0;
		}

		// Token: 0x06004FC2 RID: 20418 RVA: 0x0015E994 File Offset: 0x0015CD94
		public static Float2 operator +(Float2 v0, Float2 v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x + v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y + v1.m_Value.y;
			return result;
		}

		// Token: 0x06004FC3 RID: 20419 RVA: 0x0015E9F8 File Offset: 0x0015CDF8
		public static Float2 operator -(Float2 v0)
		{
			Float2 result = default(Float2);
			result.m_Value.x = -v0.m_Value.x;
			result.m_Value.y = -v0.m_Value.y;
			return result;
		}

		// Token: 0x06004FC4 RID: 20420 RVA: 0x0015EA44 File Offset: 0x0015CE44
		public static Float2 operator -(Float2 v0, Float2 v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x - v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y - v1.m_Value.y;
			return result;
		}

		// Token: 0x06004FC5 RID: 20421 RVA: 0x0015EAA8 File Offset: 0x0015CEA8
		public static Float2 operator *(Float2 v0, Float2 v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x * v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y * v1.m_Value.y;
			return result;
		}

		// Token: 0x06004FC6 RID: 20422 RVA: 0x0015EB0C File Offset: 0x0015CF0C
		public static Float2 operator *(Float2 v0, float v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x * v1;
			result.m_Value.y = v0.m_Value.y * v1;
			return result;
		}

		// Token: 0x06004FC7 RID: 20423 RVA: 0x0015EB58 File Offset: 0x0015CF58
		public static Float2 operator /(Float2 v0, Float2 v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x / v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y / v1.m_Value.y;
			return result;
		}

		// Token: 0x06004FC8 RID: 20424 RVA: 0x0015EBBC File Offset: 0x0015CFBC
		public static Float2 operator /(Float2 v0, float v1)
		{
			Float2 result = default(Float2);
			result.m_Value.x = v0.m_Value.x / v1;
			result.m_Value.y = v0.m_Value.y / v1;
			return result;
		}

		// Token: 0x06004FC9 RID: 20425 RVA: 0x0015EC08 File Offset: 0x0015D008
		public static bool operator ==(Float2 v0, Float2 v1)
		{
			return v0.value == v1.value;
		}

		// Token: 0x06004FCA RID: 20426 RVA: 0x0015EC1D File Offset: 0x0015D01D
		public static bool operator !=(Float2 v0, Float2 v1)
		{
			return v0.value != v1.value;
		}

		// Token: 0x04004EB3 RID: 20147
		public Vector2 m_Value;
	}
}
