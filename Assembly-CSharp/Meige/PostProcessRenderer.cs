﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FD3 RID: 4051
	public class PostProcessRenderer : MonoBehaviour
	{
		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06005436 RID: 21558 RVA: 0x001799C8 File Offset: 0x00177DC8
		// (set) Token: 0x06005435 RID: 21557 RVA: 0x001799BF File Offset: 0x00177DBF
		public bool enableCorrectToneCurve
		{
			get
			{
				return this.m_enableCorrectToneCurve;
			}
			set
			{
				this.m_enableCorrectToneCurve = value;
			}
		}

		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x06005438 RID: 21560 RVA: 0x001799D9 File Offset: 0x00177DD9
		// (set) Token: 0x06005437 RID: 21559 RVA: 0x001799D0 File Offset: 0x00177DD0
		public bool enableCorrectContrast
		{
			get
			{
				return this.m_enableCorrectContrast;
			}
			set
			{
				this.m_enableCorrectContrast = value;
			}
		}

		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x0600543A RID: 21562 RVA: 0x001799EA File Offset: 0x00177DEA
		// (set) Token: 0x06005439 RID: 21561 RVA: 0x001799E1 File Offset: 0x00177DE1
		public bool enableCorrectBrightness
		{
			get
			{
				return this.m_enableCorrectBrightness;
			}
			set
			{
				this.m_enableCorrectBrightness = value;
			}
		}

		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x0600543C RID: 21564 RVA: 0x001799FB File Offset: 0x00177DFB
		// (set) Token: 0x0600543B RID: 21563 RVA: 0x001799F2 File Offset: 0x00177DF2
		public bool enableCorrectChroma
		{
			get
			{
				return this.m_enableCorrectChroma;
			}
			set
			{
				this.m_enableCorrectChroma = value;
			}
		}

		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x0600543E RID: 21566 RVA: 0x00179A0C File Offset: 0x00177E0C
		// (set) Token: 0x0600543D RID: 21565 RVA: 0x00179A03 File Offset: 0x00177E03
		public bool enableCorrectColorBlend
		{
			get
			{
				return this.m_enableCorrectColorBlend;
			}
			set
			{
				this.m_enableCorrectColorBlend = value;
			}
		}

		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06005440 RID: 21568 RVA: 0x00179A1D File Offset: 0x00177E1D
		// (set) Token: 0x0600543F RID: 21567 RVA: 0x00179A14 File Offset: 0x00177E14
		public bool enableBloom
		{
			get
			{
				return this.m_enableBloom;
			}
			set
			{
				this.m_enableBloom = value;
			}
		}

		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x06005442 RID: 21570 RVA: 0x00179A2E File Offset: 0x00177E2E
		// (set) Token: 0x06005441 RID: 21569 RVA: 0x00179A25 File Offset: 0x00177E25
		public eToneMapProc toneMapProc
		{
			get
			{
				return this.m_ToneMapProc;
			}
			set
			{
				this.m_ToneMapProc = value;
			}
		}

		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x06005444 RID: 21572 RVA: 0x00179A3F File Offset: 0x00177E3F
		// (set) Token: 0x06005443 RID: 21571 RVA: 0x00179A36 File Offset: 0x00177E36
		public eToneMapWhitePoint toneMapWhitePoint
		{
			get
			{
				return this.m_ToneMapWhitePoint;
			}
			set
			{
				this.m_ToneMapWhitePoint = value;
			}
		}

		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x06005446 RID: 21574 RVA: 0x00179A50 File Offset: 0x00177E50
		// (set) Token: 0x06005445 RID: 21573 RVA: 0x00179A47 File Offset: 0x00177E47
		public float toneMapWhitePointScale
		{
			get
			{
				return this.m_ToneMapWhitePointScale;
			}
			set
			{
				this.m_ToneMapWhitePointScale = value;
			}
		}

		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06005448 RID: 21576 RVA: 0x00179A61 File Offset: 0x00177E61
		// (set) Token: 0x06005447 RID: 21575 RVA: 0x00179A58 File Offset: 0x00177E58
		public RangePareFloat toneMapWhitePointRange
		{
			get
			{
				return this.m_ToneMapWhitePointRange;
			}
			set
			{
				this.m_ToneMapWhitePointRange = value;
			}
		}

		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x0600544A RID: 21578 RVA: 0x00179A72 File Offset: 0x00177E72
		// (set) Token: 0x06005449 RID: 21577 RVA: 0x00179A69 File Offset: 0x00177E69
		public float exposureCorrectionValue
		{
			get
			{
				return this.m_ExposureCorrectionValue;
			}
			set
			{
				this.m_ExposureCorrectionValue = value;
			}
		}

		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x0600544C RID: 21580 RVA: 0x00179A83 File Offset: 0x00177E83
		// (set) Token: 0x0600544B RID: 21579 RVA: 0x00179A7A File Offset: 0x00177E7A
		public float adaptationRateSpeed
		{
			get
			{
				return this.m_AdaptationRateSpeed;
			}
			set
			{
				this.m_AdaptationRateSpeed = value;
			}
		}

		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x0600544E RID: 21582 RVA: 0x00179A94 File Offset: 0x00177E94
		// (set) Token: 0x0600544D RID: 21581 RVA: 0x00179A8B File Offset: 0x00177E8B
		public eBloomReferenceType bloomReferenceType
		{
			get
			{
				return this.m_BloomReferenceType;
			}
			set
			{
				this.m_BloomReferenceType = value;
			}
		}

		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x06005450 RID: 21584 RVA: 0x00179AA5 File Offset: 0x00177EA5
		// (set) Token: 0x0600544F RID: 21583 RVA: 0x00179A9C File Offset: 0x00177E9C
		public float bloomBrightThresholdOffset
		{
			get
			{
				return this.m_BloomBrightThresholdOffset;
			}
			set
			{
				this.m_BloomBrightThresholdOffset = value;
			}
		}

		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x06005452 RID: 21586 RVA: 0x00179AB6 File Offset: 0x00177EB6
		// (set) Token: 0x06005451 RID: 21585 RVA: 0x00179AAD File Offset: 0x00177EAD
		public float bloomOffset
		{
			get
			{
				return this.m_BloomOffset;
			}
			set
			{
				this.m_BloomOffset = value;
			}
		}

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x06005454 RID: 21588 RVA: 0x00179AC7 File Offset: 0x00177EC7
		// (set) Token: 0x06005453 RID: 21587 RVA: 0x00179ABE File Offset: 0x00177EBE
		public eGaussFilterType gaussFilterType
		{
			get
			{
				return this.m_GaussFilterType;
			}
			set
			{
				this.m_GaussFilterType = value;
			}
		}

		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06005456 RID: 21590 RVA: 0x00179AD8 File Offset: 0x00177ED8
		// (set) Token: 0x06005455 RID: 21589 RVA: 0x00179ACF File Offset: 0x00177ECF
		public int blurIterationNum
		{
			get
			{
				return this.m_BlurIterationNum;
			}
			set
			{
				this.m_BlurIterationNum = value;
			}
		}

		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x06005458 RID: 21592 RVA: 0x00179AE9 File Offset: 0x00177EE9
		// (set) Token: 0x06005457 RID: 21591 RVA: 0x00179AE0 File Offset: 0x00177EE0
		public float bloomIntensity
		{
			get
			{
				return this.m_BloomIntensity;
			}
			set
			{
				this.m_BloomIntensity = value;
			}
		}

		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x0600545A RID: 21594 RVA: 0x00179AFA File Offset: 0x00177EFA
		// (set) Token: 0x06005459 RID: 21593 RVA: 0x00179AF1 File Offset: 0x00177EF1
		public float correctContrastLevel
		{
			get
			{
				return this.m_CorrectContrastLevel;
			}
			set
			{
				this.m_CorrectContrastLevel = value;
			}
		}

		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x0600545C RID: 21596 RVA: 0x00179B0B File Offset: 0x00177F0B
		// (set) Token: 0x0600545B RID: 21595 RVA: 0x00179B02 File Offset: 0x00177F02
		public float correctBrightnessLevel
		{
			get
			{
				return this.m_CorrectBrightnessLevel;
			}
			set
			{
				this.m_CorrectBrightnessLevel = value;
			}
		}

		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x0600545E RID: 21598 RVA: 0x00179B1C File Offset: 0x00177F1C
		// (set) Token: 0x0600545D RID: 21597 RVA: 0x00179B13 File Offset: 0x00177F13
		public float correctChromaLevel
		{
			get
			{
				return this.m_CorrectChromaLevel;
			}
			set
			{
				this.m_CorrectChromaLevel = value;
			}
		}

		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x06005460 RID: 21600 RVA: 0x00179B2D File Offset: 0x00177F2D
		// (set) Token: 0x0600545F RID: 21599 RVA: 0x00179B24 File Offset: 0x00177F24
		public float correctBlendLevel
		{
			get
			{
				return this.m_CorrectBlendLevel;
			}
			set
			{
				this.m_CorrectBlendLevel = value;
			}
		}

		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x06005462 RID: 21602 RVA: 0x00179B3E File Offset: 0x00177F3E
		// (set) Token: 0x06005461 RID: 21601 RVA: 0x00179B35 File Offset: 0x00177F35
		public Color correctBlendColor
		{
			get
			{
				return this.m_CorrectBlendColor;
			}
			set
			{
				this.m_CorrectBlendColor = value;
			}
		}

		// Token: 0x06005463 RID: 21603 RVA: 0x00179B48 File Offset: 0x00177F48
		private void Start()
		{
			this.m_CorrectMaterial = new Material(Shader.Find("Hidden/Meige/MeigePostProcessShader"));
			this.m_BrightPassFilterMaterial = new Material(Shader.Find("Hidden/Meige/MeigeBrightPassFilterShader"));
			this.m_BloomMaterial = new Material(Shader.Find("Hidden/Meige/MeigeBloomShader"));
			this.m_LuminunceMaterial = new Material(Shader.Find("Hidden/Meige/MeigeLuminunceShader"));
			for (int i = 0; i < this.m_AdaptRenderTex.Length; i++)
			{
				this.m_AdaptRenderTex[i] = new RenderTexture(1, 1, 0, RenderTextureFormat.DefaultHDR);
			}
			this.m_AdaptedWhitePointScale = this.m_ToneMapWhitePointScale;
			this.m_isAvailable = true;
		}

		// Token: 0x06005464 RID: 21604 RVA: 0x00179BE7 File Offset: 0x00177FE7
		private void Update()
		{
		}

		// Token: 0x06005465 RID: 21605 RVA: 0x00179BEC File Offset: 0x00177FEC
		private void CalcLuminunceValue(RenderTexture src)
		{
			int num = Mathf.Min(1024, 1024);
			RenderTexture renderTexture = RenderTexture.GetTemporary(num / 2, num / 2, 0, src.format, RenderTextureReadWrite.Default, 1);
			renderTexture.filterMode = FilterMode.Point;
			renderTexture.wrapMode = TextureWrapMode.Repeat;
			Graphics.Blit(src, renderTexture, this.m_LuminunceMaterial, 0);
			while (renderTexture.width > 1 || renderTexture.height > 1)
			{
				int num2 = renderTexture.width / 2;
				if (num2 < 1)
				{
					num2 = 1;
				}
				int num3 = renderTexture.height / 2;
				if (num3 < 1)
				{
					num3 = 1;
				}
				RenderTexture temporary = RenderTexture.GetTemporary(num2, num3, 0, src.format, RenderTextureReadWrite.Default, 1);
				renderTexture.filterMode = FilterMode.Point;
				renderTexture.wrapMode = TextureWrapMode.Repeat;
				temporary.filterMode = FilterMode.Point;
				temporary.wrapMode = TextureWrapMode.Repeat;
				Graphics.Blit(renderTexture, temporary, this.m_LuminunceMaterial, 1);
				RenderTexture.ReleaseTemporary(renderTexture);
				renderTexture = temporary;
			}
			int curAdaptIndex = this.m_CurAdaptIndex;
			this.m_CurAdaptIndex = (this.m_CurAdaptIndex + 1) % 2;
			float num4 = 1f - Mathf.Pow(1f - this.m_AdaptationRateSpeed, 30f * Time.deltaTime);
			num4 = Mathf.Clamp(num4, 0.01f, 1f);
			this.m_LuminunceMaterial.SetTexture("_CurTex", renderTexture);
			this.m_LuminunceMaterial.SetVector("_AdaptParams", new Vector4(num4, this.m_ToneMapWhitePointRange.min, this.m_ToneMapWhitePointRange.max, 0f));
			Graphics.SetRenderTarget(this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
			GL.Clear(false, true, Color.black);
			Graphics.Blit(this.m_AdaptRenderTex[curAdaptIndex], this.m_AdaptRenderTex[this.m_CurAdaptIndex], this.m_LuminunceMaterial, 2);
		}

		// Token: 0x06005466 RID: 21606 RVA: 0x00179DA0 File Offset: 0x001781A0
		private void OnRenderImage(RenderTexture src, RenderTexture dst)
		{
			bool flag = false;
			if (this.m_isAvailable)
			{
				if ((this.m_enableCorrectToneCurve || this.m_enableBloom) && ((this.m_enableCorrectToneCurve && this.m_ToneMapWhitePoint != eToneMapWhitePoint.eToneMapWhitePoint_Fix) || (this.m_enableBloom && this.m_BloomReferenceType != eBloomReferenceType.eBloomReferenceType_Fix)))
				{
					this.CalcLuminunceValue(src);
				}
				if (this.m_BackupEnableCorrectToneCurve != this.m_enableCorrectToneCurve)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableToneCurve, (!this.m_enableCorrectToneCurve) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableToneCurve", (!this.m_enableCorrectToneCurve) ? 0f : 1f);
					this.m_BackupEnableCorrectToneCurve = this.m_enableCorrectToneCurve;
				}
				if (this.m_BackupEnableCorrectContrast != this.m_enableCorrectContrast)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableContrast, (!this.m_enableCorrectContrast) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableContrast", (!this.m_enableCorrectContrast) ? 0f : 1f);
					this.m_BackupEnableCorrectContrast = this.m_enableCorrectContrast;
				}
				if (this.m_BackupEnableCorrectBrightness != this.m_enableCorrectBrightness)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableBrightness, (!this.m_enableCorrectBrightness) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableBrightness", (!this.m_enableCorrectBrightness) ? 0f : 1f);
					this.m_BackupEnableCorrectBrightness = this.m_enableCorrectBrightness;
				}
				if (this.m_BackupEnableCorrectChroma != this.m_enableCorrectChroma)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableChroma, (!this.m_enableCorrectChroma) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableChroma", (!this.m_enableCorrectChroma) ? 0f : 1f);
					this.m_BackupEnableCorrectChroma = this.m_enableCorrectChroma;
				}
				if (this.m_BackupEnableCorrectColorBlend != this.m_enableCorrectColorBlend)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableColorBlend, (!this.m_enableCorrectColorBlend) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableColorBlend", (!this.m_enableCorrectColorBlend) ? 0f : 1f);
					this.m_BackupEnableCorrectColorBlend = this.m_enableCorrectColorBlend;
				}
				if (this.m_BackupEnableBloom != this.m_enableBloom)
				{
					MeigeShaderUtility.EnableKeyword(this.m_CorrectMaterial, PostProcessRenderer.m_ShaderKeywordName_EnableBloom, (!this.m_enableBloom) ? 0 : 1);
					this.m_CorrectMaterial.SetFloat("_EnableBloom", (!this.m_enableBloom) ? 0f : 1f);
					this.m_BackupEnableBloom = this.m_enableBloom;
				}
				Vector4 vector = default(Vector4);
				this.m_AdaptedWhitePointScale = Mathf.Lerp(this.m_AdaptedWhitePointScale, this.m_ToneMapWhitePointScale, this.m_AdaptationRateSpeed);
				vector.x = this.m_ExposureCorrectionValue;
				vector.y = this.m_ToneMapWhitePointScale;
				vector.w = vector.x / Mathf.Pow(2f, vector.y);
				vector.z = this.m_AdaptedWhitePointScale * vector.w;
				vector.z *= vector.z;
				if (this.m_BackupCorrectionKeyValue != vector)
				{
					this.m_CorrectMaterial.SetVector("_ToneCurve_Param", vector);
					this.m_BackupCorrectionKeyValue = vector;
				}
				if (this.m_BackupCorrectContrastLevel != this.m_CorrectContrastLevel)
				{
					this.m_CorrectMaterial.SetFloat("_CorrectContrastRate", this.m_CorrectContrastLevel);
					this.m_BackupCorrectContrastLevel = this.m_CorrectContrastLevel;
				}
				if (this.m_BackupCorrectBrightnessLevel != this.m_CorrectBrightnessLevel)
				{
					this.m_CorrectMaterial.SetFloat("_CorrectBrightnessRate", this.m_CorrectBrightnessLevel);
					this.m_BackupCorrectBrightnessLevel = this.m_CorrectBrightnessLevel;
				}
				if (this.m_BackupCorrectChromaLevel != this.m_CorrectChromaLevel)
				{
					this.m_CorrectMaterial.SetFloat("_CorrectChromaRate", this.m_CorrectChromaLevel);
					this.m_BackupCorrectChromaLevel = this.m_CorrectChromaLevel;
				}
				if (this.m_BackupCorrectBlendLevel != this.m_CorrectBlendLevel)
				{
					this.m_CorrectMaterial.SetFloat("_CorrectBlendRate", this.m_CorrectBlendLevel);
					this.m_BackupCorrectBlendLevel = this.m_CorrectBlendLevel;
				}
				if (this.m_BackupCorrectBlendColor != this.m_CorrectBlendColor)
				{
					this.m_CorrectMaterial.SetColor("_CorrectBlendColor", this.m_CorrectBlendColor);
					this.m_BackupCorrectBlendColor = this.m_CorrectBlendColor;
				}
				if (this.m_BackupBloomIntensity != this.m_BloomIntensity)
				{
					this.m_CorrectMaterial.SetFloat("_Bloomintensity", this.m_BloomIntensity);
					this.m_BackupBloomIntensity = this.m_BloomIntensity;
				}
				if (this.m_BackupBloomBrightThresholdOffset != this.m_BloomBrightThresholdOffset)
				{
					this.m_BrightPassFilterMaterial.SetFloat("_BloomKeyValue", this.m_BloomBrightThresholdOffset);
					this.m_BackupBloomBrightThresholdOffset = this.m_BloomBrightThresholdOffset;
				}
				if (this.m_BackupGaussFilterType != this.m_GaussFilterType)
				{
					MeigeShaderUtility.EnableKeyword(this.m_BloomMaterial, PostProcessRenderer.m_ShaderKeywordName_GaussFilterType, (this.m_GaussFilterType != eGaussFilterType.eGaussFilterType_x7) ? 0 : 1);
					this.m_BloomMaterial.SetFloat("_GaussFilterType", (float)this.m_GaussFilterType - 1f);
					this.m_BackupGaussFilterType = this.m_GaussFilterType;
				}
				if (this.m_enableBloom)
				{
					RenderTexture renderTexture = null;
					RenderTexture temporary = RenderTexture.GetTemporary(src.width / 2, src.height / 2, 0, src.format, RenderTextureReadWrite.Default, 1);
					if (temporary != null)
					{
						temporary.filterMode = FilterMode.Bilinear;
						eBloomReferenceType bloomReferenceType = this.m_BloomReferenceType;
						if (bloomReferenceType != eBloomReferenceType.eBloomReferenceType_ToneLuminanceValue)
						{
							if (bloomReferenceType == eBloomReferenceType.eBloomReferenceType_Fix)
							{
								Graphics.Blit(src, temporary, this.m_BrightPassFilterMaterial, 0);
							}
						}
						else
						{
							this.m_BrightPassFilterMaterial.SetTexture("_LuminunceTex", this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
							Graphics.Blit(src, temporary, this.m_BrightPassFilterMaterial, 1);
						}
						renderTexture = temporary;
						for (int i = 0; i < this.m_BlurIterationNum; i++)
						{
							if (this.m_BackupBloomOffset != this.m_BloomOffset)
							{
								this.m_BloomMaterial.SetFloat("_BloomOffset", this.m_BloomOffset + (float)i);
								this.m_BackupBloomOffset = this.m_BloomOffset;
							}
							RenderTexture temporary2 = RenderTexture.GetTemporary(src.width / 4, src.height / 4, 0, src.format, RenderTextureReadWrite.Default, 1);
							if (renderTexture != null)
							{
								renderTexture.filterMode = FilterMode.Bilinear;
								Graphics.Blit(renderTexture, temporary2, this.m_BloomMaterial, 0);
								RenderTexture.ReleaseTemporary(renderTexture);
							}
							renderTexture = RenderTexture.GetTemporary(src.width / 4, src.height / 4, 0, src.format, RenderTextureReadWrite.Default, 1);
							if (temporary2 != null)
							{
								temporary2.filterMode = FilterMode.Bilinear;
								Graphics.Blit(temporary2, renderTexture, this.m_BloomMaterial, 1);
								RenderTexture.ReleaseTemporary(temporary2);
							}
						}
					}
					this.m_CorrectMaterial.SetTexture("_BloomTex", renderTexture);
					eToneMapWhitePoint toneMapWhitePoint = this.m_ToneMapWhitePoint;
					if (toneMapWhitePoint != eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValue)
					{
						if (toneMapWhitePoint != eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValueMax)
						{
							if (toneMapWhitePoint == eToneMapWhitePoint.eToneMapWhitePoint_Fix)
							{
								Graphics.Blit(src, dst, this.m_CorrectMaterial, 0);
							}
						}
						else
						{
							this.m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
							Graphics.Blit(src, dst, this.m_CorrectMaterial, 2);
						}
					}
					else
					{
						this.m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
						Graphics.Blit(src, dst, this.m_CorrectMaterial, 1);
					}
					flag = true;
					if (renderTexture != null)
					{
						RenderTexture.ReleaseTemporary(renderTexture);
					}
				}
				else if (this.m_enableCorrectToneCurve || this.m_enableCorrectContrast || this.m_enableCorrectBrightness || this.m_enableCorrectChroma || this.m_enableCorrectColorBlend)
				{
					eToneMapWhitePoint toneMapWhitePoint2 = this.m_ToneMapWhitePoint;
					if (toneMapWhitePoint2 != eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValue)
					{
						if (toneMapWhitePoint2 != eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValueMax)
						{
							if (toneMapWhitePoint2 == eToneMapWhitePoint.eToneMapWhitePoint_Fix)
							{
								Graphics.Blit(src, dst, this.m_CorrectMaterial, 0);
							}
						}
						else
						{
							this.m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
							Graphics.Blit(src, dst, this.m_CorrectMaterial, 2);
						}
					}
					else
					{
						this.m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", this.m_AdaptRenderTex[this.m_CurAdaptIndex]);
						Graphics.Blit(src, dst, this.m_CorrectMaterial, 1);
					}
					flag = true;
				}
			}
			if (!flag)
			{
				Graphics.Blit(src, dst);
			}
		}

		// Token: 0x04005549 RID: 21833
		[SerializeField]
		private bool m_enableCorrectToneCurve;

		// Token: 0x0400554A RID: 21834
		[SerializeField]
		private bool m_enableCorrectContrast;

		// Token: 0x0400554B RID: 21835
		[SerializeField]
		private bool m_enableCorrectBrightness;

		// Token: 0x0400554C RID: 21836
		[SerializeField]
		private bool m_enableCorrectChroma;

		// Token: 0x0400554D RID: 21837
		[SerializeField]
		private bool m_enableCorrectColorBlend;

		// Token: 0x0400554E RID: 21838
		[SerializeField]
		private bool m_enableBloom;

		// Token: 0x0400554F RID: 21839
		[SerializeField]
		private eToneMapProc m_ToneMapProc = eToneMapProc.eToneMapProc_Fix;

		// Token: 0x04005550 RID: 21840
		[SerializeField]
		private eToneMapWhitePoint m_ToneMapWhitePoint = eToneMapWhitePoint.eToneMapWhitePoint_Fix;

		// Token: 0x04005551 RID: 21841
		[SerializeField]
		private float m_ToneMapWhitePointScale = 1f;

		// Token: 0x04005552 RID: 21842
		[SerializeField]
		private RangePareFloat m_ToneMapWhitePointRange = new RangePareFloat(0.2f, 5f);

		// Token: 0x04005553 RID: 21843
		[SerializeField]
		private float m_ExposureCorrectionValue = 1f;

		// Token: 0x04005554 RID: 21844
		[SerializeField]
		private float m_AdaptationRateSpeed = 0.2f;

		// Token: 0x04005555 RID: 21845
		[SerializeField]
		private eBloomReferenceType m_BloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;

		// Token: 0x04005556 RID: 21846
		[SerializeField]
		private float m_BloomBrightThresholdOffset = 1f;

		// Token: 0x04005557 RID: 21847
		[SerializeField]
		private float m_BloomOffset = 5f;

		// Token: 0x04005558 RID: 21848
		[SerializeField]
		private eGaussFilterType m_GaussFilterType = eGaussFilterType.eGaussFilterType_x5;

		// Token: 0x04005559 RID: 21849
		[SerializeField]
		[Range(1f, 4f)]
		private int m_BlurIterationNum = 2;

		// Token: 0x0400555A RID: 21850
		[SerializeField]
		private float m_BloomIntensity = 1f;

		// Token: 0x0400555B RID: 21851
		[SerializeField]
		private float m_CorrectContrastLevel;

		// Token: 0x0400555C RID: 21852
		[SerializeField]
		private float m_CorrectBrightnessLevel;

		// Token: 0x0400555D RID: 21853
		[SerializeField]
		private float m_CorrectChromaLevel;

		// Token: 0x0400555E RID: 21854
		[SerializeField]
		private float m_CorrectBlendLevel;

		// Token: 0x0400555F RID: 21855
		[SerializeField]
		private Color m_CorrectBlendColor = Color.white;

		// Token: 0x04005560 RID: 21856
		private bool m_BackupEnableCorrectToneCurve;

		// Token: 0x04005561 RID: 21857
		private bool m_BackupEnableCorrectContrast;

		// Token: 0x04005562 RID: 21858
		private bool m_BackupEnableCorrectBrightness;

		// Token: 0x04005563 RID: 21859
		private bool m_BackupEnableCorrectChroma;

		// Token: 0x04005564 RID: 21860
		private bool m_BackupEnableCorrectColorBlend;

		// Token: 0x04005565 RID: 21861
		private bool m_BackupEnableBloom;

		// Token: 0x04005566 RID: 21862
		private Vector4 m_BackupCorrectionKeyValue = Vector4.zero;

		// Token: 0x04005567 RID: 21863
		private float m_BackupCorrectContrastLevel = -9999f;

		// Token: 0x04005568 RID: 21864
		private float m_BackupCorrectBrightnessLevel = -9999f;

		// Token: 0x04005569 RID: 21865
		private float m_BackupCorrectChromaLevel = -9999f;

		// Token: 0x0400556A RID: 21866
		private float m_BackupCorrectBlendLevel = -9999f;

		// Token: 0x0400556B RID: 21867
		private Color m_BackupCorrectBlendColor = Color.black;

		// Token: 0x0400556C RID: 21868
		private float m_BackupBloomBrightThresholdOffset;

		// Token: 0x0400556D RID: 21869
		private float m_BackupBloomOffset;

		// Token: 0x0400556E RID: 21870
		private float m_BackupBloomIntensity;

		// Token: 0x0400556F RID: 21871
		private eGaussFilterType m_BackupGaussFilterType;

		// Token: 0x04005570 RID: 21872
		private Material m_CorrectMaterial;

		// Token: 0x04005571 RID: 21873
		private Material m_BrightPassFilterMaterial;

		// Token: 0x04005572 RID: 21874
		private Material m_BloomMaterial;

		// Token: 0x04005573 RID: 21875
		private Material m_LuminunceMaterial;

		// Token: 0x04005574 RID: 21876
		private float m_AdaptedWhitePointScale = 1f;

		// Token: 0x04005575 RID: 21877
		private RenderTexture[] m_AdaptRenderTex = new RenderTexture[2];

		// Token: 0x04005576 RID: 21878
		private int m_CurAdaptIndex;

		// Token: 0x04005577 RID: 21879
		private bool m_isAvailable;

		// Token: 0x04005578 RID: 21880
		private static string[] m_ShaderKeywordName_EnableToneCurve = new string[]
		{
			"_ENABLETONECURVE_DISABLE",
			"_ENABLETONECURVE_ENABLE"
		};

		// Token: 0x04005579 RID: 21881
		private static string[] m_ShaderKeywordName_EnableContrast = new string[]
		{
			"_ENABLECONTRAST_DISABLE",
			"_ENABLECONTRAST_ENABLE"
		};

		// Token: 0x0400557A RID: 21882
		private static string[] m_ShaderKeywordName_EnableBrightness = new string[]
		{
			"_ENABLEBRIGHTNESS_DISABLE",
			"_ENABLEBRIGHTNESS_ENABLE"
		};

		// Token: 0x0400557B RID: 21883
		private static string[] m_ShaderKeywordName_EnableChroma = new string[]
		{
			"_ENABLECHROMA_DISABLE",
			"_ENABLECHROMA_ENABLE"
		};

		// Token: 0x0400557C RID: 21884
		private static string[] m_ShaderKeywordName_EnableColorBlend = new string[]
		{
			"_ENABLECOLORBLEND_DISABLE",
			"_ENABLECOLORBLEND_ENABLE"
		};

		// Token: 0x0400557D RID: 21885
		private static string[] m_ShaderKeywordName_EnableBloom = new string[]
		{
			"_ENABLEBLOOM_DISABLE",
			"_ENABLEBLOOM_ENABLE"
		};

		// Token: 0x0400557E RID: 21886
		private static string[] m_ShaderKeywordName_GaussFilterType = new string[]
		{
			"_GAUSSFILTERTYPE_X5",
			"_GAUSSFILTERTYPE_X7"
		};
	}
}
