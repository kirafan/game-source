﻿using System;

namespace Meige
{
	// Token: 0x02000EA4 RID: 3748
	[Serializable]
	public class EffectRuleArrayParam<TYPE> : EffectRuleParamBase where TYPE : new()
	{
		// Token: 0x06004E56 RID: 20054 RVA: 0x0015C1FA File Offset: 0x0015A5FA
		public EffectRuleArrayParam()
		{
			this.m_OrType = typeof(TYPE);
			this.m_TypeCode = EffectRuleArrayParam<TYPE>.GetTypeCode(this.m_OrType);
		}

		// Token: 0x06004E57 RID: 20055 RVA: 0x0015C223 File Offset: 0x0015A623
		public eEffectAnimTypeCode GetTypeCode()
		{
			return this.m_TypeCode;
		}

		// Token: 0x06004E58 RID: 20056 RVA: 0x0015C22B File Offset: 0x0015A62B
		public Type GetOrType()
		{
			return this.m_OrType;
		}

		// Token: 0x06004E59 RID: 20057 RVA: 0x0015C233 File Offset: 0x0015A633
		public object GetValue(int tgtIdx)
		{
			return this.m_Value[0];
		}

		// Token: 0x06004E5A RID: 20058 RVA: 0x0015C246 File Offset: 0x0015A646
		public void SetValue(int tgtIdx, object value)
		{
			this.m_Value[0] = (TYPE)((object)value);
		}

		// Token: 0x06004E5B RID: 20059 RVA: 0x0015C25A File Offset: 0x0015A65A
		public object GetValue(int arrayIdx, int tgtIdx)
		{
			return this.m_Value[arrayIdx];
		}

		// Token: 0x06004E5C RID: 20060 RVA: 0x0015C26D File Offset: 0x0015A66D
		public void SetValue(int arrayIdx, int tgtIdx, object value)
		{
			this.m_Value[arrayIdx] = (TYPE)((object)value);
		}

		// Token: 0x06004E5D RID: 20061 RVA: 0x0015C281 File Offset: 0x0015A681
		public bool IsArray()
		{
			return false;
		}

		// Token: 0x06004E5E RID: 20062 RVA: 0x0015C284 File Offset: 0x0015A684
		public int GetArraySize()
		{
			return 1;
		}

		// Token: 0x06004E5F RID: 20063 RVA: 0x0015C287 File Offset: 0x0015A687
		public bool ChangeArraySize(int size)
		{
			return false;
		}

		// Token: 0x06004E60 RID: 20064 RVA: 0x0015C28A File Offset: 0x0015A68A
		private static eEffectAnimTypeCode GetTypeCode(Type type)
		{
			if (type.BaseType == typeof(Enum))
			{
				return eEffectAnimTypeCode.Enum;
			}
			return EffectHelper.GetTypeCodeFromType(type);
		}

		// Token: 0x04004E5E RID: 20062
		private eEffectAnimTypeCode m_TypeCode;

		// Token: 0x04004E5F RID: 20063
		private Type m_OrType;

		// Token: 0x04004E60 RID: 20064
		public TYPE[] m_Value;
	}
}
