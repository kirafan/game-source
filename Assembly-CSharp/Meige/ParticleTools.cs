﻿using System;

namespace Meige
{
	// Token: 0x02000EF7 RID: 3831
	public static class ParticleTools
	{
		// Token: 0x04004EA9 RID: 20137
		public const int PARTICLE_EMITTER_CHUNK_COLORCURVE_NUM = 4;

		// Token: 0x04004EAA RID: 20138
		public const int PARTICLE_EMITTER_CHUNK_COLORCURVE_POINT_NUM = 4;
	}
}
