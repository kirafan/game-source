﻿using System;

namespace Meige
{
	// Token: 0x02000E88 RID: 3720
	public class AssetBundleNameUtility
	{
		// Token: 0x06004CEF RID: 19695 RVA: 0x00156FEB File Offset: 0x001553EB
		public static string Encode(string str)
		{
			str = str.Replace(".muast", string.Empty);
			return AssetBundleNameUtility.AlphaEncode(str) + ".muast";
		}

		// Token: 0x06004CF0 RID: 19696 RVA: 0x0015700F File Offset: 0x0015540F
		public static string Decode(string str)
		{
			str = str.Replace(".muast", string.Empty);
			return AssetBundleNameUtility.AlphaDecode(str) + ".muast";
		}

		// Token: 0x06004CF1 RID: 19697 RVA: 0x00157033 File Offset: 0x00155433
		private static string AlphaEncode(string str)
		{
			return str;
		}

		// Token: 0x06004CF2 RID: 19698 RVA: 0x00157036 File Offset: 0x00155436
		private static string AlphaDecode(string str)
		{
			return str;
		}
	}
}
