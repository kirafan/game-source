﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EF8 RID: 3832
	[Serializable]
	public class RangeValue<Value> where Value : struct, IOperatorFast<Value>
	{
		// Token: 0x06004EEE RID: 20206 RVA: 0x0015D6A4 File Offset: 0x0015BAA4
		public RangeValue()
		{
			this.m_Value = new Value[2];
		}

		// Token: 0x06004EEF RID: 20207 RVA: 0x0015D6B8 File Offset: 0x0015BAB8
		public RangeValue(int num)
		{
			this.m_Value = new Value[num];
		}

		// Token: 0x06004EF0 RID: 20208 RVA: 0x0015D6CC File Offset: 0x0015BACC
		public RangeValue(RangeValue<Value> value)
		{
			this.m_Value = new Value[value.GetLength()];
			for (int i = 0; i < value.GetLength(); i++)
			{
				this.m_Value[i] = value[i];
			}
		}

		// Token: 0x1700053B RID: 1339
		public Value this[int idx]
		{
			get
			{
				return this.m_Value[idx];
			}
			set
			{
				this.m_Value[idx] = value;
			}
		}

		// Token: 0x06004EF3 RID: 20211 RVA: 0x0015D737 File Offset: 0x0015BB37
		public void Set(Value v, int idx)
		{
			this.m_Value[idx] = v;
		}

		// Token: 0x06004EF4 RID: 20212 RVA: 0x0015D746 File Offset: 0x0015BB46
		public void SetValueRow(object v, int idx)
		{
			this.m_Value[idx].SetRowValue(v);
		}

		// Token: 0x06004EF5 RID: 20213 RVA: 0x0015D762 File Offset: 0x0015BB62
		public Value Get(int idx)
		{
			return this.m_Value[idx];
		}

		// Token: 0x06004EF6 RID: 20214 RVA: 0x0015D770 File Offset: 0x0015BB70
		public int GetLength()
		{
			return this.m_Value.Length;
		}

		// Token: 0x06004EF7 RID: 20215 RVA: 0x0015D77C File Offset: 0x0015BB7C
		public static RangeValue<Value> Clone(RangeValue<Value> target)
		{
			RangeValue<Value> rangeValue = new RangeValue<Value>();
			for (int i = 0; i < target.GetLength(); i++)
			{
				rangeValue.m_Value[i] = target.m_Value[i];
			}
			return rangeValue;
		}

		// Token: 0x06004EF8 RID: 20216 RVA: 0x0015D7BF File Offset: 0x0015BBBF
		public virtual RangeValue<Value> Clone()
		{
			return RangeValue<Value>.Clone(this);
		}

		// Token: 0x06004EF9 RID: 20217 RVA: 0x0015D7C8 File Offset: 0x0015BBC8
		private void CreateSelectionPoint()
		{
			this.m_Point = new float[this.GetLength()];
			for (int i = 0; i < this.m_Point.Length; i++)
			{
				this.m_Point[i] = (float)i / (float)(this.m_Point.Length - 1);
			}
		}

		// Token: 0x06004EFA RID: 20218 RVA: 0x0015D815 File Offset: 0x0015BC15
		private void SetSectionPoint(int idx, float point)
		{
			if (this.m_Point != null)
			{
				this.m_Point[idx] = point;
			}
		}

		// Token: 0x06004EFB RID: 20219 RVA: 0x0015D82B File Offset: 0x0015BC2B
		private float GetSectionPoint(int idx)
		{
			if (this.m_Point != null && this.m_Point.Length > 0)
			{
				return this.m_Point[idx];
			}
			return (float)idx / (float)(this.m_Value.Length - 1);
		}

		// Token: 0x06004EFC RID: 20220 RVA: 0x0015D85D File Offset: 0x0015BC5D
		public Value Random()
		{
			return this.Lerp(UnityEngine.Random.Range(0f, 1f));
		}

		// Token: 0x06004EFD RID: 20221 RVA: 0x0015D874 File Offset: 0x0015BC74
		public Value Lerp(float point)
		{
			if (this.m_Value.Length == 2)
			{
				Value result = this.m_Value[1];
				result.Sub(this.m_Value[0]);
				result.Mul(point);
				result.Add(this.m_Value[0]);
				return result;
			}
			if (point <= 0f)
			{
				return this.m_Value[0];
			}
			if (point >= 1f)
			{
				return this.m_Value[this.m_Value.Length - 1];
			}
			int num = 0;
			if (this.m_Point != null && this.m_Point.Length > 0)
			{
				for (int i = 0; i < this.m_Value.Length; i++)
				{
					float sectionPoint = this.GetSectionPoint(i);
					if (point <= sectionPoint)
					{
						num = i;
						break;
					}
				}
			}
			else
			{
				float num2 = 1f / (float)(this.m_Value.Length - 1);
				num = (int)(point / num2);
			}
			float sectionPoint2 = this.GetSectionPoint(num);
			float sectionPoint3 = this.GetSectionPoint(num + 1);
			float num3 = sectionPoint3 - sectionPoint2;
			float num4 = (point - sectionPoint2) / num3;
			num4 = Mathf.Clamp(num4, 0f, 1f);
			Value a = default(Value);
			Value value = default(Value);
			a.Mul(this.m_Value[num], 1f - num4);
			value.Mul(this.m_Value[num + 1], num4);
			value.Add(a, value);
			return value;
		}

		// Token: 0x06004EFE RID: 20222 RVA: 0x0015DA21 File Offset: 0x0015BE21
		public Value ReverseLerp(float point)
		{
			return this.Lerp(1f - point);
		}

		// Token: 0x04004EAB RID: 20139
		[SerializeField]
		public Value[] m_Value;

		// Token: 0x04004EAC RID: 20140
		public float[] m_Point;
	}
}
