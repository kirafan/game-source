﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F0D RID: 3853
	[Serializable]
	public struct RGBA : IComparable, IComparable<RGBA>, IOperatorFast<RGBA>
	{
		// Token: 0x06005021 RID: 20513 RVA: 0x0015F956 File Offset: 0x0015DD56
		public RGBA(RGBA value)
		{
			this.m_Value = value.m_Value;
		}

		// Token: 0x06005022 RID: 20514 RVA: 0x0015F965 File Offset: 0x0015DD65
		public RGBA(Color value)
		{
			this.m_Value = value;
		}

		// Token: 0x06005023 RID: 20515 RVA: 0x0015F96E File Offset: 0x0015DD6E
		public RGBA(float r, float g, float b, float a)
		{
			this.m_Value.r = r;
			this.m_Value.g = g;
			this.m_Value.b = b;
			this.m_Value.a = a;
		}

		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06005025 RID: 20517 RVA: 0x0015F9AA File Offset: 0x0015DDAA
		// (set) Token: 0x06005024 RID: 20516 RVA: 0x0015F9A1 File Offset: 0x0015DDA1
		public Color value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x06005027 RID: 20519 RVA: 0x0015F9C0 File Offset: 0x0015DDC0
		// (set) Token: 0x06005026 RID: 20518 RVA: 0x0015F9B2 File Offset: 0x0015DDB2
		public float r
		{
			get
			{
				return this.m_Value.r;
			}
			set
			{
				this.m_Value.r = value;
			}
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06005029 RID: 20521 RVA: 0x0015F9DB File Offset: 0x0015DDDB
		// (set) Token: 0x06005028 RID: 20520 RVA: 0x0015F9CD File Offset: 0x0015DDCD
		public float g
		{
			get
			{
				return this.m_Value.g;
			}
			set
			{
				this.m_Value.g = value;
			}
		}

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x0600502B RID: 20523 RVA: 0x0015F9F6 File Offset: 0x0015DDF6
		// (set) Token: 0x0600502A RID: 20522 RVA: 0x0015F9E8 File Offset: 0x0015DDE8
		public float b
		{
			get
			{
				return this.m_Value.b;
			}
			set
			{
				this.m_Value.b = value;
			}
		}

		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x0600502D RID: 20525 RVA: 0x0015FA11 File Offset: 0x0015DE11
		// (set) Token: 0x0600502C RID: 20524 RVA: 0x0015FA03 File Offset: 0x0015DE03
		public float a
		{
			get
			{
				return this.m_Value.a;
			}
			set
			{
				this.m_Value.a = value;
			}
		}

		// Token: 0x17000558 RID: 1368
		public float this[int idx]
		{
			get
			{
				return this.m_Value[idx];
			}
			set
			{
				this.m_Value[idx] = value;
			}
		}

		// Token: 0x06005030 RID: 20528 RVA: 0x0015FA3B File Offset: 0x0015DE3B
		public static implicit operator RGBA(Color v)
		{
			return new RGBA(v);
		}

		// Token: 0x06005031 RID: 20529 RVA: 0x0015FA43 File Offset: 0x0015DE43
		public static implicit operator Color(RGBA v)
		{
			return v.value;
		}

		// Token: 0x06005032 RID: 20530 RVA: 0x0015FA4C File Offset: 0x0015DE4C
		public void Add(RGBA a, RGBA b)
		{
			this = a + b;
		}

		// Token: 0x06005033 RID: 20531 RVA: 0x0015FA5B File Offset: 0x0015DE5B
		public void Sub(RGBA a, RGBA b)
		{
			this = a - b;
		}

		// Token: 0x06005034 RID: 20532 RVA: 0x0015FA6A File Offset: 0x0015DE6A
		public void Mul(RGBA a, RGBA b)
		{
			this = a * b;
		}

		// Token: 0x06005035 RID: 20533 RVA: 0x0015FA79 File Offset: 0x0015DE79
		public void Mul(RGBA a, float b)
		{
			this = a * b;
		}

		// Token: 0x06005036 RID: 20534 RVA: 0x0015FA88 File Offset: 0x0015DE88
		public void Div(RGBA a, RGBA b)
		{
			this = a / b;
		}

		// Token: 0x06005037 RID: 20535 RVA: 0x0015FA97 File Offset: 0x0015DE97
		public void Add(RGBA b)
		{
			this += b;
		}

		// Token: 0x06005038 RID: 20536 RVA: 0x0015FAAB File Offset: 0x0015DEAB
		public void Sub(RGBA b)
		{
			this -= b;
		}

		// Token: 0x06005039 RID: 20537 RVA: 0x0015FABF File Offset: 0x0015DEBF
		public void Mul(RGBA b)
		{
			this *= b;
		}

		// Token: 0x0600503A RID: 20538 RVA: 0x0015FAD3 File Offset: 0x0015DED3
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x0600503B RID: 20539 RVA: 0x0015FAE7 File Offset: 0x0015DEE7
		public void Div(RGBA b)
		{
			this *= b;
		}

		// Token: 0x0600503C RID: 20540 RVA: 0x0015FAFB File Offset: 0x0015DEFB
		public void SetRowValue(object o)
		{
			this.m_Value = (Color)o;
		}

		// Token: 0x0600503D RID: 20541 RVA: 0x0015FB09 File Offset: 0x0015DF09
		public void SetRowValue(float o, int componentIdx)
		{
			this.m_Value[componentIdx] = o;
		}

		// Token: 0x0600503E RID: 20542 RVA: 0x0015FB18 File Offset: 0x0015DF18
		public int CompareTo(object obj)
		{
			RGBA? rgba = obj as RGBA?;
			if (rgba == null)
			{
				return 1;
			}
			return this.CompareTo(rgba);
		}

		// Token: 0x0600503F RID: 20543 RVA: 0x0015FB50 File Offset: 0x0015DF50
		public int CompareTo(RGBA target)
		{
			if (target.m_Value.r == this.r && target.m_Value.g == this.g && target.m_Value.b == this.b && target.m_Value.a == this.a)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06005040 RID: 20544 RVA: 0x0015FBBC File Offset: 0x0015DFBC
		public override bool Equals(object obj)
		{
			RGBA? rgba = obj as RGBA?;
			return rgba != null && this.Equals(rgba);
		}

		// Token: 0x06005041 RID: 20545 RVA: 0x0015FBF8 File Offset: 0x0015DFF8
		public bool Equals(RGBA obj)
		{
			return this == obj;
		}

		// Token: 0x06005042 RID: 20546 RVA: 0x0015FC06 File Offset: 0x0015E006
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06005043 RID: 20547 RVA: 0x0015FC19 File Offset: 0x0015E019
		public static RGBA operator +(RGBA v0)
		{
			return v0;
		}

		// Token: 0x06005044 RID: 20548 RVA: 0x0015FC1C File Offset: 0x0015E01C
		public static RGBA operator +(RGBA v0, RGBA v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r + v1.m_Value.r;
			result.m_Value.g = v0.m_Value.g + v1.m_Value.g;
			result.m_Value.b = v0.m_Value.b + v1.m_Value.b;
			result.m_Value.a = v0.m_Value.a + v1.m_Value.a;
			return result;
		}

		// Token: 0x06005045 RID: 20549 RVA: 0x0015FCC8 File Offset: 0x0015E0C8
		public static RGBA operator -(RGBA v0)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = -v0.m_Value.r;
			result.m_Value.g = -v0.m_Value.g;
			result.m_Value.b = -v0.m_Value.b;
			result.m_Value.a = -v0.m_Value.a;
			return result;
		}

		// Token: 0x06005046 RID: 20550 RVA: 0x0015FD44 File Offset: 0x0015E144
		public static RGBA operator -(RGBA v0, RGBA v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r - v1.m_Value.r;
			result.m_Value.g = v0.m_Value.g - v1.m_Value.g;
			result.m_Value.b = v0.m_Value.b - v1.m_Value.b;
			result.m_Value.a = v0.m_Value.a - v1.m_Value.a;
			return result;
		}

		// Token: 0x06005047 RID: 20551 RVA: 0x0015FDF0 File Offset: 0x0015E1F0
		public static RGBA operator *(RGBA v0, RGBA v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r * v1.m_Value.r;
			result.m_Value.g = v0.m_Value.g * v1.m_Value.g;
			result.m_Value.b = v0.m_Value.b * v1.m_Value.b;
			result.m_Value.a = v0.m_Value.a * v1.m_Value.a;
			return result;
		}

		// Token: 0x06005048 RID: 20552 RVA: 0x0015FE9C File Offset: 0x0015E29C
		public static RGBA operator *(RGBA v0, float v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r * v1;
			result.m_Value.g = v0.m_Value.g * v1;
			result.m_Value.b = v0.m_Value.b * v1;
			result.m_Value.a = v0.m_Value.a * v1;
			return result;
		}

		// Token: 0x06005049 RID: 20553 RVA: 0x0015FF1C File Offset: 0x0015E31C
		public static RGBA operator /(RGBA v0, RGBA v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r / v1.m_Value.r;
			result.m_Value.g = v0.m_Value.g / v1.m_Value.g;
			result.m_Value.b = v0.m_Value.b / v1.m_Value.b;
			result.m_Value.a = v0.m_Value.a / v1.m_Value.a;
			return result;
		}

		// Token: 0x0600504A RID: 20554 RVA: 0x0015FFC8 File Offset: 0x0015E3C8
		public static RGBA operator /(RGBA v0, float v1)
		{
			RGBA result = default(RGBA);
			result.m_Value.r = v0.m_Value.r / v1;
			result.m_Value.g = v0.m_Value.g / v1;
			result.m_Value.b = v0.m_Value.b / v1;
			result.m_Value.a = v0.m_Value.a / v1;
			return result;
		}

		// Token: 0x0600504B RID: 20555 RVA: 0x00160048 File Offset: 0x0015E448
		public static bool operator ==(RGBA v0, RGBA v1)
		{
			return v0.value == v1.value;
		}

		// Token: 0x0600504C RID: 20556 RVA: 0x0016005D File Offset: 0x0015E45D
		public static bool operator !=(RGBA v0, RGBA v1)
		{
			return v0.value != v1.value;
		}

		// Token: 0x04004EB6 RID: 20150
		[SerializeField]
		public Color m_Value;
	}
}
