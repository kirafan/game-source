﻿using System;

namespace Meige
{
	// Token: 0x02000EFA RID: 3834
	[Serializable]
	public class RangeValueInt : RangeValue<s32>
	{
		// Token: 0x06004F12 RID: 20242 RVA: 0x0015DC88 File Offset: 0x0015C088
		public RangeValueInt()
		{
		}

		// Token: 0x06004F13 RID: 20243 RVA: 0x0015DC90 File Offset: 0x0015C090
		public RangeValueInt(int num) : base(num)
		{
		}

		// Token: 0x06004F14 RID: 20244 RVA: 0x0015DC99 File Offset: 0x0015C099
		public RangeValueInt(RangeValueInt v) : base(v)
		{
		}

		// Token: 0x06004F15 RID: 20245 RVA: 0x0015DCA4 File Offset: 0x0015C0A4
		public new RangeValueInt Clone()
		{
			return new RangeValueInt(this);
		}
	}
}
