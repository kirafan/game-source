﻿using System;

namespace Meige
{
	// Token: 0x02000FD0 RID: 4048
	public enum eColorCorrectShaderType
	{
		// Token: 0x0400553B RID: 21819
		eColorCorrectShaderType_Invalid,
		// Token: 0x0400553C RID: 21820
		eColorCorrectShaderType_ToneMapParam,
		// Token: 0x0400553D RID: 21821
		eColorCorrectShaderType_Reduction,
		// Token: 0x0400553E RID: 21822
		eColorCorrectShaderType_Combiner
	}
}
