﻿using System;

namespace Meige
{
	// Token: 0x02000F94 RID: 3988
	public enum eOutlineType
	{
		// Token: 0x0400537F RID: 21375
		eOutlineType_Invalid = -1,
		// Token: 0x04005380 RID: 21376
		eOutlineType_Std,
		// Token: 0x04005381 RID: 21377
		eOutlineType_MulBaseColor,
		// Token: 0x04005382 RID: 21378
		eOutlineType_Max
	}
}
