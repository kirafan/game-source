﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EEC RID: 3820
	[RequireComponent(typeof(EffectTimeLineAnimation))]
	[DisallowMultipleComponent]
	public class EffectTimeLine : EffectComponentBase
	{
		// Token: 0x06004EBD RID: 20157 RVA: 0x0015CADE File Offset: 0x0015AEDE
		private void OnApplicationQuit()
		{
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06004EBE RID: 20158 RVA: 0x0015CAE0 File Offset: 0x0015AEE0
		public EffectComponentBase[] effectArray
		{
			get
			{
				return this.m_EffectArray;
			}
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06004EBF RID: 20159 RVA: 0x0015CAE8 File Offset: 0x0015AEE8
		public EffectTimeLineAnimation animation
		{
			get
			{
				return this.m_EffectTimeLineAnimation;
			}
		}

		// Token: 0x06004EC0 RID: 20160 RVA: 0x0015CAF0 File Offset: 0x0015AEF0
		private void Awake()
		{
			this.m_EffectTimeLineAnimation = base.gameObject.GetComponent<EffectTimeLineAnimation>();
			this.m_ThisTransform = base.transform;
			foreach (EffectComponentBase effectComponentBase in this.m_EffectArray)
			{
				string hierarchyPath = EffectHelper.GetHierarchyPath(this.m_ThisTransform, effectComponentBase.transform);
				this.m_EffectDictionary.Add(hierarchyPath, effectComponentBase);
			}
		}

		// Token: 0x06004EC1 RID: 20161 RVA: 0x0015CB58 File Offset: 0x0015AF58
		private void Start()
		{
		}

		// Token: 0x06004EC2 RID: 20162 RVA: 0x0015CB5C File Offset: 0x0015AF5C
		public override bool Shot()
		{
			if (this.isAlive)
			{
				return false;
			}
			this.m_EffectTimeLineAnimation.SetTimeNow(0f);
			this.m_EffectTimeLineAnimation.Play();
			bool flag = this.m_EffectTimeLineAnimation.IsPlaying();
			this.m_isAlive = flag;
			return flag;
		}

		// Token: 0x06004EC3 RID: 20163 RVA: 0x0015CBA8 File Offset: 0x0015AFA8
		public override bool Kill()
		{
			if (!this.isAlive)
			{
				return false;
			}
			for (int i = 0; i < this.m_EffectArray.Length; i++)
			{
				if (this.m_EffectArray[i] != null)
				{
					this.m_EffectArray[i].Kill();
				}
			}
			this.m_EffectTimeLineAnimation.Stop();
			this.m_isAlive = false;
			return true;
		}

		// Token: 0x06004EC4 RID: 20164 RVA: 0x0015CC10 File Offset: 0x0015B010
		private void Update()
		{
			if (this.m_EffectTimeLineAnimation == null)
			{
				return;
			}
			if (!this.isAlive && !this.isActive)
			{
				return;
			}
			if (this.isActive && !this.m_EffectTimeLineAnimation.IsPlaying())
			{
				this.Shot();
			}
			if (!this.m_EffectTimeLineAnimation.IsPlaying())
			{
				int num = 0;
				for (int i = 0; i < this.m_EffectArray.Length; i++)
				{
					if (this.m_EffectArray[i] != null)
					{
						num++;
						if (!this.m_EffectArray[i].isAlive && !this.m_EffectArray[i].isActive)
						{
							num--;
						}
					}
				}
				if (num == 0 && this.m_EffectTimeLineAnimation.IsEndFrame())
				{
					this.Kill();
				}
			}
		}

		// Token: 0x06004EC5 RID: 20165 RVA: 0x0015CCF0 File Offset: 0x0015B0F0
		public void EventActivate(string id)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			EffectComponentBase effectComponentBase;
			if (this.m_EffectDictionary.TryGetValue(id, out effectComponentBase))
			{
				effectComponentBase.Activate(true);
			}
		}

		// Token: 0x06004EC6 RID: 20166 RVA: 0x0015CD24 File Offset: 0x0015B124
		public void EventDeactivate(string id)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			EffectComponentBase effectComponentBase;
			if (this.m_EffectDictionary.TryGetValue(id, out effectComponentBase))
			{
				effectComponentBase.Activate(false);
			}
		}

		// Token: 0x06004EC7 RID: 20167 RVA: 0x0015CD58 File Offset: 0x0015B158
		public void EventShot(string id)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			EffectComponentBase effectComponentBase;
			if (this.m_EffectDictionary.TryGetValue(id, out effectComponentBase))
			{
				effectComponentBase.Shot();
			}
		}

		// Token: 0x06004EC8 RID: 20168 RVA: 0x0015CD8C File Offset: 0x0015B18C
		public void EventKill(string id)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			EffectComponentBase effectComponentBase;
			if (this.m_EffectDictionary.TryGetValue(id, out effectComponentBase))
			{
				effectComponentBase.Kill();
			}
		}

		// Token: 0x04004E7A RID: 20090
		public Transform m_ThisTransform;

		// Token: 0x04004E7B RID: 20091
		public EffectComponentBase[] m_EffectArray;

		// Token: 0x04004E7C RID: 20092
		private EffectTimeLineAnimation m_EffectTimeLineAnimation = new EffectTimeLineAnimation();

		// Token: 0x04004E7D RID: 20093
		private Dictionary<string, EffectComponentBase> m_EffectDictionary = new Dictionary<string, EffectComponentBase>();
	}
}
