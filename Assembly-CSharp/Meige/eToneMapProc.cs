﻿using System;

namespace Meige
{
	// Token: 0x02000FCD RID: 4045
	public enum eToneMapProc
	{
		// Token: 0x04005530 RID: 21808
		eToneMapProc_Comp,
		// Token: 0x04005531 RID: 21809
		eToneMapProc_Step,
		// Token: 0x04005532 RID: 21810
		eToneMapProc_Fix
	}
}
