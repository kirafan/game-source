﻿using System;

namespace Meige
{
	// Token: 0x02000F8F RID: 3983
	public enum eUsingTexture
	{
		// Token: 0x04005360 RID: 21344
		None,
		// Token: 0x04005361 RID: 21345
		Single,
		// Token: 0x04005362 RID: 21346
		Layer
	}
}
