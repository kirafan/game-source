﻿using System;

namespace Meige
{
	// Token: 0x02000F9B RID: 3995
	public struct MFFHeader
	{
		// Token: 0x040053FE RID: 21502
		public byte m_Magic0;

		// Token: 0x040053FF RID: 21503
		public byte m_Magic1;

		// Token: 0x04005400 RID: 21504
		public byte m_Magic2;

		// Token: 0x04005401 RID: 21505
		public byte m_Magic3;

		// Token: 0x04005402 RID: 21506
		public uint m_MFFVersion;

		// Token: 0x04005403 RID: 21507
		public uint m_Version;

		// Token: 0x04005404 RID: 21508
		public short m_Platform;

		// Token: 0x04005405 RID: 21509
		public ushort m_ChunkCnt;
	}
}
