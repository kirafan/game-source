﻿using System;

namespace Meige
{
	// Token: 0x02000EE7 RID: 3815
	[Serializable]
	public class EffectRuleBase
	{
		// Token: 0x06004EA4 RID: 20132 RVA: 0x0015C4C2 File Offset: 0x0015A8C2
		public virtual int GetPropertyNum()
		{
			return 0;
		}

		// Token: 0x06004EA5 RID: 20133 RVA: 0x0015C4C5 File Offset: 0x0015A8C5
		public virtual int GetArrayNum(int propertyIdx)
		{
			return 0;
		}

		// Token: 0x06004EA6 RID: 20134 RVA: 0x0015C4C8 File Offset: 0x0015A8C8
		public virtual object GetValue(int propertyIdx, int arrayIdx)
		{
			return null;
		}

		// Token: 0x06004EA7 RID: 20135 RVA: 0x0015C4CB File Offset: 0x0015A8CB
		public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
		{
		}

		// Token: 0x06004EA8 RID: 20136 RVA: 0x0015C4CD File Offset: 0x0015A8CD
		public virtual eEffectAnimTypeCode GetTypeCode(int propertyIdx)
		{
			return eEffectAnimTypeCode.Invalid;
		}

		// Token: 0x06004EA9 RID: 20137 RVA: 0x0015C4D0 File Offset: 0x0015A8D0
		public virtual string GetPropertyName(int propertyIdx)
		{
			return null;
		}

		// Token: 0x06004EAA RID: 20138 RVA: 0x0015C4D3 File Offset: 0x0015A8D3
		public virtual EffectRuleParamBase GetParamBase(int propertyIdx)
		{
			return null;
		}
	}
}
