﻿using System;

namespace Meige
{
	// Token: 0x02000F81 RID: 3969
	public enum eParticleLifeSpanType : byte
	{
		// Token: 0x040052F9 RID: 21241
		eParticleLifeSpanType_Time,
		// Token: 0x040052FA RID: 21242
		eParticleLifeSpanType_Distance,
		// Token: 0x040052FB RID: 21243
		eParticleLifeSpanType_HeightRange,
		// Token: 0x040052FC RID: 21244
		eParticleLifeSpanType_AnimFrame
	}
}
