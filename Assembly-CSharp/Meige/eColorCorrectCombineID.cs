﻿using System;

namespace Meige
{
	// Token: 0x02000FD1 RID: 4049
	public enum eColorCorrectCombineID
	{
		// Token: 0x04005540 RID: 21824
		eColorCorrectCombineID_ToneMap,
		// Token: 0x04005541 RID: 21825
		eColorCorrectCombineID_Contrast,
		// Token: 0x04005542 RID: 21826
		eColorCorrectCombineID_Brightness,
		// Token: 0x04005543 RID: 21827
		eColorCorrectCombineID_Chroma,
		// Token: 0x04005544 RID: 21828
		eColorCorrectCombineID_ColorBlend
	}
}
