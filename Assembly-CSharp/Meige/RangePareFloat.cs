﻿using System;

namespace Meige
{
	// Token: 0x02000F01 RID: 3841
	[Serializable]
	public class RangePareFloat : RangePare<f32>
	{
		// Token: 0x06004F2E RID: 20270 RVA: 0x0015DDD9 File Offset: 0x0015C1D9
		public RangePareFloat()
		{
		}

		// Token: 0x06004F2F RID: 20271 RVA: 0x0015DDE1 File Offset: 0x0015C1E1
		public RangePareFloat(f32 min, f32 max) : base(min, max)
		{
		}

		// Token: 0x06004F30 RID: 20272 RVA: 0x0015DDEB File Offset: 0x0015C1EB
		public RangePareFloat(RangePareFloat v) : base(v)
		{
		}

		// Token: 0x06004F31 RID: 20273 RVA: 0x0015DDF4 File Offset: 0x0015C1F4
		public new RangePareFloat Clone()
		{
			return new RangePareFloat(this);
		}
	}
}
