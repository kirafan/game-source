﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000E9F RID: 3743
	public class EffectModelPlayer : EffectComponentBase
	{
		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06004E1E RID: 19998 RVA: 0x0015B98C File Offset: 0x00159D8C
		// (set) Token: 0x06004E1D RID: 19997 RVA: 0x0015B930 File Offset: 0x00159D30
		public override bool isActive
		{
			get
			{
				return this.m_isActive;
			}
			set
			{
				this.m_isActive = value;
				if (this.m_isActive)
				{
					if (this.m_MeigeAnimCtrl)
					{
						this.m_MeigeAnimCtrl.m_WrapMode = WrapMode.Loop;
					}
				}
				else if (this.m_MeigeAnimCtrl)
				{
					this.m_MeigeAnimCtrl.m_WrapMode = WrapMode.ClampForever;
				}
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06004E20 RID: 20000 RVA: 0x0015B99D File Offset: 0x00159D9D
		// (set) Token: 0x06004E1F RID: 19999 RVA: 0x0015B994 File Offset: 0x00159D94
		public int clipIdx
		{
			get
			{
				return this.m_PlayAnimIdx;
			}
			set
			{
				this.m_PlayAnimIdx = value;
			}
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06004E21 RID: 20001 RVA: 0x0015B9A5 File Offset: 0x00159DA5
		public MsbHandler msbHandler
		{
			get
			{
				return this.m_MsbHandler;
			}
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06004E22 RID: 20002 RVA: 0x0015B9AD File Offset: 0x00159DAD
		public MeigeAnimClipHolder[] meigeAnimClipHolder
		{
			get
			{
				return this.m_Animes;
			}
		}

		// Token: 0x06004E23 RID: 20003 RVA: 0x0015B9B5 File Offset: 0x00159DB5
		public void SetMaterialColor(Color color)
		{
			this.m_Color = color;
		}

		// Token: 0x06004E24 RID: 20004 RVA: 0x0015B9C0 File Offset: 0x00159DC0
		public bool Setup(MsbHandler msb, MeigeAnimClipHolder[] animes)
		{
			if (this.isAlive)
			{
				return false;
			}
			if (msb == null)
			{
				return false;
			}
			if (animes == null)
			{
				return false;
			}
			if (animes.Length <= 0)
			{
				return false;
			}
			this.m_MsbHandler = msb;
			this.m_Animes = animes;
			if (this.m_MeigeAnimCtrl != null)
			{
				UnityEngine.Object.Destroy(this.m_MeigeAnimCtrl);
			}
			this.m_MeigeAnimCtrl = this.m_ThisGameObject.AddComponent<MeigeAnimCtrl>();
			this.m_MeigeAnimCtrl.transform.parent = base.transform;
			this.m_MeigeAnimCtrl.Open();
			for (int i = 0; i < animes.Length; i++)
			{
				this.m_MeigeAnimCtrl.AddClip(msb, animes[i]);
			}
			this.m_MeigeAnimCtrl.Close();
			for (int j = 0; j < this.m_MsbHandler.GetMsbObjectHandlerNum(); j++)
			{
				MsbObjectHandler msbObjectHandler = this.m_MsbHandler.GetMsbObjectHandler(j);
				MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
				if (work != null)
				{
				}
			}
			Renderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<Renderer>();
			if (componentsInChildren != null)
			{
				for (int k = 0; k < componentsInChildren.Length; k++)
				{
				}
			}
			this.m_MsbHandlerObject = this.m_MsbHandler.gameObject;
			this.m_MsbHandlerObject.SetActive(false);
			return true;
		}

		// Token: 0x06004E25 RID: 20005 RVA: 0x0015BB08 File Offset: 0x00159F08
		private void Start()
		{
			this.m_ThisGameObject = base.gameObject;
			MsbHandler msb = null;
			if (this.m_ModelDataPrefab != null)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_ModelDataPrefab);
				gameObject.transform.parent = base.transform;
				msb = gameObject.GetComponentInChildren<MsbHandler>();
			}
			MeigeAnimClipHolder[] array = null;
			if (this.m_AnimPrefab != null && this.m_AnimPrefab.Length > 0)
			{
				array = new MeigeAnimClipHolder[this.m_AnimPrefab.Length];
				for (int i = 0; i < array.Length; i++)
				{
					if (this.m_AnimPrefab[i] != null)
					{
						array[i] = UnityEngine.Object.Instantiate<MeigeAnimClipHolder>(this.m_AnimPrefab[i]);
						array[i].transform.parent = base.transform;
					}
				}
			}
			if (!this.Setup(msb, array))
			{
				Debug.LogWarning("EffectModelPlayer :: Setup Error In Start Function.");
			}
		}

		// Token: 0x06004E26 RID: 20006 RVA: 0x0015BBE4 File Offset: 0x00159FE4
		public override bool Kill()
		{
			if (!this.isAlive)
			{
				return false;
			}
			this.m_MeigeAnimCtrl.Pause();
			for (int i = 0; i < this.m_MsbHandler.GetParticleEmitterNum(); i++)
			{
				this.m_MsbHandler.GetParticleEmitter(i).isActive = false;
				this.m_MsbHandler.GetParticleEmitter(i).Kill();
			}
			for (int j = 0; j < this.m_MsbHandler.GetMsbObjectHandlerNum(); j++)
			{
				MsbObjectHandler msbObjectHandler = this.m_MsbHandler.GetMsbObjectHandler(j);
				MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
				if (work != null)
				{
				}
			}
			this.m_MsbHandlerObject.SetActive(false);
			this.isActive = false;
			this.m_isAlive = false;
			return true;
		}

		// Token: 0x06004E27 RID: 20007 RVA: 0x0015BC9C File Offset: 0x0015A09C
		public override bool Shot()
		{
			if (this.isAlive)
			{
				return false;
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, (!this.isActive) ? WrapMode.ClampForever : WrapMode.Loop);
			this.m_isAlive = true;
			this.m_MsbHandlerObject.SetActive(true);
			return true;
		}

		// Token: 0x06004E28 RID: 20008 RVA: 0x0015BCF4 File Offset: 0x0015A0F4
		private void Update()
		{
			if (this.isActive)
			{
				this.Shot();
			}
			if (!this.isAlive)
			{
				return;
			}
			for (int i = 0; i < this.m_MsbHandler.GetMsbMaterialHandlerNum(); i++)
			{
				this.m_MsbHandler.GetMsbMaterialHandler(i).GetWork().m_Diffuse = this.m_Color;
			}
			if (!this.isActive)
			{
				if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime >= 1f)
				{
					for (int j = 0; j < this.m_MsbHandler.GetParticleEmitterNum(); j++)
					{
						if (this.m_MsbHandler.GetParticleEmitter(j).isActive)
						{
							this.m_MsbHandler.GetParticleEmitter(j).isActive = false;
						}
					}
					for (int k = 0; k < this.m_MsbHandler.GetParticleEmitterNum(); k++)
					{
						if (this.m_MsbHandler.GetParticleEmitter(k).isAlive)
						{
							return;
						}
					}
					this.Kill();
				}
			}
		}

		// Token: 0x06004E29 RID: 20009 RVA: 0x0015BDFA File Offset: 0x0015A1FA
		public override int GetPropertyNum()
		{
			return 1;
		}

		// Token: 0x06004E2A RID: 20010 RVA: 0x0015BDFD File Offset: 0x0015A1FD
		public override int GetArrayNum(int propertyIdx)
		{
			return 1;
		}

		// Token: 0x06004E2B RID: 20011 RVA: 0x0015BE00 File Offset: 0x0015A200
		public override object GetValue(int propertyIdx, int arrayIdx)
		{
			return this.m_Color;
		}

		// Token: 0x06004E2C RID: 20012 RVA: 0x0015BE0D File Offset: 0x0015A20D
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
		{
			this.m_Color = (Color)value;
		}

		// Token: 0x06004E2D RID: 20013 RVA: 0x0015BE1C File Offset: 0x0015A21C
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value)
		{
			EffectHelper.Set(ref this.m_Color, tgtIdx, componentIdx, value);
		}

		// Token: 0x06004E2E RID: 20014 RVA: 0x0015BE2E File Offset: 0x0015A22E
		public override eEffectAnimTypeCode GetTypeCode(int propertyIdx)
		{
			return eEffectAnimTypeCode.Color;
		}

		// Token: 0x06004E2F RID: 20015 RVA: 0x0015BE31 File Offset: 0x0015A231
		public override Type GetPropertType(int propertyIdx)
		{
			return typeof(Color);
		}

		// Token: 0x06004E30 RID: 20016 RVA: 0x0015BE3D File Offset: 0x0015A23D
		public override string GetPropertyName(int propertyIdx)
		{
			return "Color";
		}

		// Token: 0x04004E48 RID: 20040
		[SerializeField]
		private GameObject m_ModelDataPrefab;

		// Token: 0x04004E49 RID: 20041
		[SerializeField]
		private MeigeAnimClipHolder[] m_AnimPrefab;

		// Token: 0x04004E4A RID: 20042
		[SerializeField]
		private int m_PlayAnimIdx;

		// Token: 0x04004E4B RID: 20043
		private GameObject m_MsbHandlerObject;

		// Token: 0x04004E4C RID: 20044
		private MsbHandler m_MsbHandler;

		// Token: 0x04004E4D RID: 20045
		private MeigeAnimClipHolder[] m_Animes;

		// Token: 0x04004E4E RID: 20046
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04004E4F RID: 20047
		private GameObject m_ThisGameObject;

		// Token: 0x04004E50 RID: 20048
		public Color m_Color = Color.white;

		// Token: 0x04004E51 RID: 20049
		private const string PLAY_KEY = "Take 001";
	}
}
