﻿using System;

namespace Meige
{
	// Token: 0x02000F6B RID: 3947
	public enum eFlagType
	{
		// Token: 0x04005219 RID: 21017
		eFlagType_None,
		// Token: 0x0400521A RID: 21018
		eFlagType_Area,
		// Token: 0x0400521B RID: 21019
		eFlagType_Chapter,
		// Token: 0x0400521C RID: 21020
		eFlagType_Permanent,
		// Token: 0x0400521D RID: 21021
		eFlagType_Max
	}
}
