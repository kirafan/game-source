﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FBF RID: 4031
	public class MsbHierarchyNode : MonoBehaviour
	{
		// Token: 0x06005409 RID: 21513 RVA: 0x0017866C File Offset: 0x00176A6C
		public Transform GetTransform()
		{
			return this.m_Transform;
		}

		// Token: 0x0600540A RID: 21514 RVA: 0x00178674 File Offset: 0x00176A74
		public void SetScaleOfDistance(float scaleOfDist)
		{
			this.m_ScaleOfDistance = scaleOfDist;
		}

		// Token: 0x0600540B RID: 21515 RVA: 0x0017867D File Offset: 0x00176A7D
		public float GetScaleOfDistance()
		{
			return this.m_ScaleOfDistance;
		}

		// Token: 0x0600540C RID: 21516 RVA: 0x00178688 File Offset: 0x00176A88
		public void Revert()
		{
			Vector3 distance = this.m_Distance;
			distance.x *= -1f;
			this.m_Transform.localPosition = distance;
			this.m_Transform.localRotation = Quaternion.identity;
			this.m_Transform.localScale = Vector3.one;
		}

		// Token: 0x0600540D RID: 21517 RVA: 0x001786DB File Offset: 0x00176ADB
		public void Init()
		{
			this.m_Transform = base.transform;
		}

		// Token: 0x0600540E RID: 21518 RVA: 0x001786EC File Offset: 0x00176AEC
		public static void EzMatrixCalcFunction(out Matrix4x4 outMtx, ref Vector3 position, ref Quaternion rot, ref Vector3 scale, float scaleOfDistance, Matrix4x4 parentMtx)
		{
			Vector3 pos = position * scaleOfDistance;
			Matrix4x4 rhs = Matrix4x4.TRS(pos, rot, scale);
			outMtx = parentMtx * rhs;
		}

		// Token: 0x040054E0 RID: 21728
		public Vector3 m_Distance;

		// Token: 0x040054E1 RID: 21729
		public int m_RefID;

		// Token: 0x040054E2 RID: 21730
		public MsbHierarchyNode.MsbHierarchyNodeAttr m_Src;

		// Token: 0x040054E3 RID: 21731
		private Transform m_Transform;

		// Token: 0x040054E4 RID: 21732
		private float m_ScaleOfDistance = 1f;

		// Token: 0x02000FC0 RID: 4032
		[Serializable]
		public class MsbHierarchyNodeAttr
		{
			// Token: 0x0600540F RID: 21519 RVA: 0x00178728 File Offset: 0x00176B28
			public MsbHierarchyNodeAttr()
			{
				this.m_bLinkConstraint = false;
			}

			// Token: 0x06005410 RID: 21520 RVA: 0x00178737 File Offset: 0x00176B37
			public MsbHierarchyNodeAttr(MsbHierarchyNode.MsbHierarchyNodeAttr src)
			{
				this.m_bSegmentScale = src.m_bSegmentScale;
				this.m_bSkeleton = src.m_bSkeleton;
				this.m_bJustTransAffectedByParent = src.m_bJustTransAffectedByParent;
				this.m_bLinkConstraint = src.m_bLinkConstraint;
			}

			// Token: 0x040054E5 RID: 21733
			public bool m_bSegmentScale;

			// Token: 0x040054E6 RID: 21734
			public bool m_bSkeleton;

			// Token: 0x040054E7 RID: 21735
			public bool m_bJustTransAffectedByParent;

			// Token: 0x040054E8 RID: 21736
			public bool m_bLinkConstraint;
		}
	}
}
