﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Meige
{
	// Token: 0x02000FEA RID: 4074
	public class CTrophyController : MonoBehaviour
	{
		// Token: 0x060054E9 RID: 21737 RVA: 0x0017CADC File Offset: 0x0017AEDC
		public static void Create()
		{
			CGlobalInstance.CreateInstance();
			CTrophyController.ms_Inst = CGlobalInstance.EntryClass<CTrophyController>();
		}

		// Token: 0x060054EA RID: 21738 RVA: 0x0017CAED File Offset: 0x0017AEED
		public static void EntryTrophyCustom(CallbackCustomTroph pcustomcallback)
		{
			CTrophyController.ms_Inst.m_CallbackCustom = pcustomcallback;
		}

		// Token: 0x060054EB RID: 21739 RVA: 0x0017CAFA File Offset: 0x0017AEFA
		public static void EntryCallbackEvent(CallbackCustomEvent peventcallback)
		{
		}

		// Token: 0x060054EC RID: 21740 RVA: 0x0017CAFC File Offset: 0x0017AEFC
		public static void Unlock(TrophyHandlerUnlock phandle, int ftrophyid)
		{
			phandle.m_isAvailable = false;
			phandle.m_processType = eTrophyProcessType.Unlock;
			phandle.m_trophyID = ftrophyid;
			CTrophyController.ms_Inst.m_List.Add(phandle);
		}

		// Token: 0x060054ED RID: 21741 RVA: 0x0017CB24 File Offset: 0x0017AF24
		private void Update()
		{
			switch (this.m_Step)
			{
			case CTrophyController.eStep.InitUp:
				IAcountController.Create();
				this.m_Step = CTrophyController.eStep.AcountCheck;
				break;
			case CTrophyController.eStep.AcountCheck:
				if (IAcountController.GetState() == IAcountController.eState.Online)
				{
					this.m_Step = CTrophyController.eStep.ListUp;
					this.m_ListUp = false;
					Social.LoadAchievementDescriptions(new Action<IAchievementDescription[]>(this.CallbackAchivementList));
				}
				break;
			case CTrophyController.eStep.ListUp:
				if (this.m_ListUp)
				{
					this.m_Step = CTrophyController.eStep.ListState;
					this.m_ListUp = false;
					Social.LoadAchievements(new Action<IAchievement[]>(this.CallbackAchivementState));
				}
				break;
			case CTrophyController.eStep.ListState:
				if (this.m_ListUp)
				{
					this.m_Step = CTrophyController.eStep.Update;
				}
				break;
			case CTrophyController.eStep.Update:
				if (this.m_List.Count != 0 && this.m_NowPlayHandle == null)
				{
					this.m_NowPlayHandle = this.m_List[0];
					this.m_List.RemoveAt(0);
					switch (this.m_NowPlayHandle.m_processType)
					{
					case eTrophyProcessType.Unlock:
					{
						TrophyHandlerUnlock trophyHandlerUnlock = (TrophyHandlerUnlock)this.m_NowPlayHandle;
						CTrophyController.CAchivementState cachivementState = this.m_NowAchivement = this.SearchAchivement(trophyHandlerUnlock.m_trophyID);
						if (cachivementState != null)
						{
							if (!cachivementState.m_Complete)
							{
								string achievementID = cachivementState.m_KeyName;
								if (this.m_CallbackCustom != null)
								{
									TTrophyCusotmInfo ttrophyCusotmInfo = new TTrophyCusotmInfo();
									ttrophyCusotmInfo.m_TrophyID = trophyHandlerUnlock.m_trophyID;
									this.m_CallbackCustom(ttrophyCusotmInfo);
									achievementID = ttrophyCusotmInfo.m_TrophyApiName;
								}
								Social.ReportProgress(achievementID, 100.0, new Action<bool>(this.CallbackUnlock));
							}
							else
							{
								this.m_NowPlayHandle.m_result = eTrophyResult.Unlocked;
								this.m_NowPlayHandle.m_isAvailable = true;
								this.m_NowPlayHandle = null;
								this.m_NowAchivement = null;
							}
						}
						else
						{
							this.m_NowPlayHandle.m_result = eTrophyResult.ErrorInvalidTrophyID;
							this.m_NowPlayHandle.m_isAvailable = true;
							this.m_NowPlayHandle = null;
							this.m_NowAchivement = null;
						}
						break;
					}
					}
				}
				break;
			}
		}

		// Token: 0x060054EE RID: 21742 RVA: 0x0017CD38 File Offset: 0x0017B138
		private static int ChangeNumberKey(string fkey)
		{
			int num = 0;
			foreach (char c in fkey)
			{
				if (c >= '0' && c <= '9')
				{
					num *= 10;
					num += (int)(c - '0');
				}
			}
			return num;
		}

		// Token: 0x060054EF RID: 21743 RVA: 0x0017CD88 File Offset: 0x0017B188
		private CTrophyController.CAchivementState SearchAchivement(int fkeyid)
		{
			CTrophyController.CAchivementState result = null;
			for (int i = 0; i < this.m_AchivementList.Count; i++)
			{
				if (this.m_AchivementList[i].m_AccessKey == fkeyid)
				{
					result = this.m_AchivementList[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x060054F0 RID: 21744 RVA: 0x0017CDE0 File Offset: 0x0017B1E0
		private void CallbackAchivementList(IAchievementDescription[] plist)
		{
			for (int i = 0; i < plist.Length; i++)
			{
				CTrophyController.CAchivementState cachivementState = new CTrophyController.CAchivementState();
				cachivementState.m_KeyName = plist[i].id;
				cachivementState.m_AccessKey = CTrophyController.ChangeNumberKey(plist[i].id);
				this.m_AchivementList.Add(cachivementState);
			}
			this.m_ListUp = true;
		}

		// Token: 0x060054F1 RID: 21745 RVA: 0x0017CE3C File Offset: 0x0017B23C
		private void CallbackAchivementState(IAchievement[] plist)
		{
			for (int i = 0; i < plist.Length; i++)
			{
				int count = this.m_AchivementList.Count;
				for (int j = 0; j < count; j++)
				{
					if (this.m_AchivementList[i].m_KeyName == plist[i].id)
					{
						this.m_AchivementList[i].m_Complete = plist[i].completed;
						break;
					}
				}
			}
		}

		// Token: 0x060054F2 RID: 21746 RVA: 0x0017CEBC File Offset: 0x0017B2BC
		private void CallbackUnlock(bool success)
		{
			if (success)
			{
				this.m_NowPlayHandle.m_result = eTrophyResult.Success;
				this.m_NowAchivement.m_Complete = true;
			}
			else
			{
				this.m_NowPlayHandle.m_result = eTrophyResult.CanNotAccess;
			}
			this.m_NowPlayHandle.m_isAvailable = true;
			this.m_NowPlayHandle = null;
			this.m_NowAchivement = null;
		}

		// Token: 0x040055D3 RID: 21971
		private static CTrophyController ms_Inst;

		// Token: 0x040055D4 RID: 21972
		private List<TrophyHandlerBase> m_List = new List<TrophyHandlerBase>();

		// Token: 0x040055D5 RID: 21973
		private TrophyHandlerBase m_NowPlayHandle;

		// Token: 0x040055D6 RID: 21974
		private CTrophyController.CAchivementState m_NowAchivement;

		// Token: 0x040055D7 RID: 21975
		private CallbackCustomTroph m_CallbackCustom;

		// Token: 0x040055D8 RID: 21976
		private CTrophyController.eStep m_Step;

		// Token: 0x040055D9 RID: 21977
		private bool m_ListUp;

		// Token: 0x040055DA RID: 21978
		private List<CTrophyController.CAchivementState> m_AchivementList = new List<CTrophyController.CAchivementState>();

		// Token: 0x02000FEB RID: 4075
		private enum eStep
		{
			// Token: 0x040055DC RID: 21980
			InitUp,
			// Token: 0x040055DD RID: 21981
			AcountCheck,
			// Token: 0x040055DE RID: 21982
			ListUp,
			// Token: 0x040055DF RID: 21983
			ListState,
			// Token: 0x040055E0 RID: 21984
			Update
		}

		// Token: 0x02000FEC RID: 4076
		public class CAchivementState
		{
			// Token: 0x040055E1 RID: 21985
			public string m_KeyName;

			// Token: 0x040055E2 RID: 21986
			public int m_AccessKey;

			// Token: 0x040055E3 RID: 21987
			public bool m_Complete;
		}
	}
}
