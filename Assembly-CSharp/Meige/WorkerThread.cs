﻿using System;
using System.Threading;

namespace Meige
{
	// Token: 0x02000FDD RID: 4061
	public class WorkerThread
	{
		// Token: 0x0600548B RID: 21643 RVA: 0x0017B083 File Offset: 0x00179483
		public WorkerThread()
		{
			this.Init();
		}

		// Token: 0x0600548C RID: 21644 RVA: 0x0017B094 File Offset: 0x00179494
		private void Init()
		{
			this.m_exit = false;
			this.m_event = new ManualResetEvent(false);
			this.m_thread = new Thread(new ThreadStart(this.Exec));
			this.m_thread.IsBackground = true;
			this.m_thread.Priority = ThreadPriority.AboveNormal;
			this.m_thread.Start();
		}

		// Token: 0x0600548D RID: 21645 RVA: 0x0017B0F0 File Offset: 0x001794F0
		public void Release()
		{
			this.m_exit = true;
			if (this.m_thread != null)
			{
				this.Signal();
				this.m_thread.Join();
				this.m_thread = null;
			}
			if (this.m_event != null)
			{
				this.m_event.Close();
				this.m_event = null;
			}
		}

		// Token: 0x0600548E RID: 21646 RVA: 0x0017B144 File Offset: 0x00179544
		public void Signal()
		{
			this.m_event.Set();
		}

		// Token: 0x0600548F RID: 21647 RVA: 0x0017B154 File Offset: 0x00179554
		private void Exec()
		{
			while (!this.m_exit)
			{
				try
				{
					this.m_event.WaitOne();
					this.m_event.Reset();
					Job job = JobQueue.Dequeue();
					if (job != null)
					{
						job.Invoke();
						JobQueue.Done(job);
						this.Signal();
					}
				}
				catch (ThreadAbortException)
				{
					this.m_exit = true;
					return;
				}
				catch (Exception)
				{
					this.m_exit = true;
					JobQueue.Release();
				}
			}
		}

		// Token: 0x040055A3 RID: 21923
		private Thread m_thread;

		// Token: 0x040055A4 RID: 21924
		private ManualResetEvent m_event;

		// Token: 0x040055A5 RID: 21925
		private bool m_exit;
	}
}
