﻿using System;

namespace Meige
{
	// Token: 0x02000F92 RID: 3986
	public enum eCullMode
	{
		// Token: 0x04005372 RID: 21362
		Off,
		// Token: 0x04005373 RID: 21363
		Front,
		// Token: 0x04005374 RID: 21364
		Back
	}
}
