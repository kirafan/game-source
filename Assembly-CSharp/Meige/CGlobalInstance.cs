﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000FE6 RID: 4070
	public class CGlobalInstance : MonoBehaviour
	{
		// Token: 0x060054DC RID: 21724 RVA: 0x0017CA1C File Offset: 0x0017AE1C
		public static void CreateInstance()
		{
			if (CGlobalInstance.ms_Instance == null)
			{
				GameObject gameObject = new GameObject("MeigeGlobal");
				UnityEngine.Object.DontDestroyOnLoad(gameObject);
				CGlobalInstance.ms_Instance = gameObject.AddComponent<CGlobalInstance>();
			}
		}

		// Token: 0x060054DD RID: 21725 RVA: 0x0017CA58 File Offset: 0x0017AE58
		public static T EntryClass<T>() where T : Component
		{
			T t = CGlobalInstance.ms_Instance.gameObject.GetComponent<T>();
			if (t == null)
			{
				t = CGlobalInstance.ms_Instance.gameObject.AddComponent<T>();
			}
			return t;
		}

		// Token: 0x060054DE RID: 21726 RVA: 0x0017CA98 File Offset: 0x0017AE98
		public static T GetClass<T>() where T : Component
		{
			return CGlobalInstance.ms_Instance.gameObject.GetComponent<T>();
		}

		// Token: 0x040055D0 RID: 21968
		private static CGlobalInstance ms_Instance;
	}
}
