﻿using System;

namespace Meige
{
	// Token: 0x02000FA2 RID: 4002
	public class CRC32
	{
		// Token: 0x060052A1 RID: 21153 RVA: 0x00171D58 File Offset: 0x00170158
		private static void BuildCRC32Table()
		{
			CRC32.crcTable = new uint[256];
			for (uint num = 0U; num < 256U; num += 1U)
			{
				uint num2 = num;
				for (int i = 0; i < 8; i++)
				{
					num2 = (uint)(((num2 & 1U) != 0U) ? (18446744073402876704UL ^ (ulong)(num2 >> 1)) : ((ulong)(num2 >> 1)));
				}
				CRC32.crcTable[(int)((UIntPtr)num)] = num2;
			}
		}

		// Token: 0x060052A2 RID: 21154 RVA: 0x00171DC8 File Offset: 0x001701C8
		public static uint Compute(byte[] bytes)
		{
			if (CRC32.crcTable == null)
			{
				CRC32.BuildCRC32Table();
			}
			uint num = uint.MaxValue;
			for (int i = 0; i < bytes.Length; i++)
			{
				num = (CRC32.crcTable[(int)((UIntPtr)((num ^ (uint)bytes[i]) & 255U))] ^ num >> 8);
			}
			return (uint)((ulong)num ^ ulong.MaxValue);
		}

		// Token: 0x04005414 RID: 21524
		private const int TABLE_LENGTH = 256;

		// Token: 0x04005415 RID: 21525
		private static uint[] crcTable;
	}
}
