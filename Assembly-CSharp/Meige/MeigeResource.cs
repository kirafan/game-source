﻿using System;
using System.Collections;
using Meige.AssetBundles;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Meige
{
	// Token: 0x02000F74 RID: 3956
	public class MeigeResource
	{
		// Token: 0x02000F75 RID: 3957
		public enum Option
		{
			// Token: 0x04005237 RID: 21047
			None,
			// Token: 0x04005238 RID: 21048
			Scene,
			// Token: 0x04005239 RID: 21049
			SceneAdditive,
			// Token: 0x0400523A RID: 21050
			SceneActivationFalse = 4,
			// Token: 0x0400523B RID: 21051
			DownloadOnly = 8,
			// Token: 0x0400523C RID: 21052
			ResourceKeep = 16,
			// Token: 0x0400523D RID: 21053
			ReferenceCounting = 32
		}

		// Token: 0x02000F76 RID: 3958
		public enum Error
		{
			// Token: 0x0400523F RID: 21055
			None,
			// Token: 0x04005240 RID: 21056
			Timeout,
			// Token: 0x04005241 RID: 21057
			Unknown
		}

		// Token: 0x02000F77 RID: 3959
		public class Handler : IEnumerator
		{
			// Token: 0x060051DA RID: 20954 RVA: 0x0016ED38 File Offset: 0x0016D138
			public Handler(string resourceName, string assetName = null, Action<MeigeResource.Handler> callback = null, params MeigeResource.Option[] options)
			{
				this.Clear();
				this.m_resourceName = resourceName;
				this.m_assetName = assetName;
				this.m_callback = callback;
				foreach (MeigeResource.Option option in options)
				{
					this.m_option |= (int)option;
				}
				this.m_refCount = 1;
			}

			// Token: 0x1700057B RID: 1403
			// (get) Token: 0x060051DB RID: 20955 RVA: 0x0016ED96 File Offset: 0x0016D196
			public string resourceName
			{
				get
				{
					return this.m_resourceName;
				}
			}

			// Token: 0x1700057C RID: 1404
			// (get) Token: 0x060051DC RID: 20956 RVA: 0x0016ED9E File Offset: 0x0016D19E
			public string assetName
			{
				get
				{
					return this.m_assetName;
				}
			}

			// Token: 0x1700057D RID: 1405
			// (get) Token: 0x060051DD RID: 20957 RVA: 0x0016EDA6 File Offset: 0x0016D1A6
			public string assetBundleName
			{
				get
				{
					return this.m_assetbundleName;
				}
			}

			// Token: 0x1700057E RID: 1406
			// (get) Token: 0x060051DE RID: 20958 RVA: 0x0016EDAE File Offset: 0x0016D1AE
			public int option
			{
				get
				{
					return this.m_option;
				}
			}

			// Token: 0x1700057F RID: 1407
			// (get) Token: 0x060051DF RID: 20959 RVA: 0x0016EDB6 File Offset: 0x0016D1B6
			// (set) Token: 0x060051E0 RID: 20960 RVA: 0x0016EDBE File Offset: 0x0016D1BE
			public Action<MeigeResource.Handler> callback
			{
				get
				{
					return this.m_callback;
				}
				set
				{
					this.m_callback = value;
					if (this.m_state == MeigeResource.Handler.State.Done && this.m_callback != null)
					{
						this.m_callback(this);
					}
				}
			}

			// Token: 0x17000580 RID: 1408
			// (get) Token: 0x060051E1 RID: 20961 RVA: 0x0016EDEA File Offset: 0x0016D1EA
			public bool IsDone
			{
				get
				{
					return this.m_state == MeigeResource.Handler.State.Done;
				}
			}

			// Token: 0x17000581 RID: 1409
			// (get) Token: 0x060051E2 RID: 20962 RVA: 0x0016EDF5 File Offset: 0x0016D1F5
			public bool IsUnloadDone
			{
				get
				{
					return this.m_state == MeigeResource.Handler.State.UnloadDone;
				}
			}

			// Token: 0x17000582 RID: 1410
			// (get) Token: 0x060051E3 RID: 20963 RVA: 0x0016EE00 File Offset: 0x0016D200
			public bool IsWait
			{
				get
				{
					return this.m_state == MeigeResource.Handler.State.Wait;
				}
			}

			// Token: 0x17000583 RID: 1411
			// (get) Token: 0x060051E4 RID: 20964 RVA: 0x0016EE0B File Offset: 0x0016D20B
			public bool IsLoading
			{
				get
				{
					return this.m_state == MeigeResource.Handler.State.Loading;
				}
			}

			// Token: 0x17000584 RID: 1412
			// (get) Token: 0x060051E5 RID: 20965 RVA: 0x0016EE16 File Offset: 0x0016D216
			public MeigeResource.Handler.State state
			{
				get
				{
					return this.m_state;
				}
			}

			// Token: 0x17000585 RID: 1413
			// (get) Token: 0x060051E6 RID: 20966 RVA: 0x0016EE1E File Offset: 0x0016D21E
			public MeigeResource.Error error
			{
				get
				{
					return this.m_error;
				}
			}

			// Token: 0x17000586 RID: 1414
			// (get) Token: 0x060051E7 RID: 20967 RVA: 0x0016EE26 File Offset: 0x0016D226
			public bool IsError
			{
				get
				{
					return this.m_error != MeigeResource.Error.None;
				}
			}

			// Token: 0x17000587 RID: 1415
			// (get) Token: 0x060051E8 RID: 20968 RVA: 0x0016EE34 File Offset: 0x0016D234
			public AsyncOperation asyncOperation
			{
				get
				{
					return this.m_asyncOperation;
				}
			}

			// Token: 0x17000588 RID: 1416
			// (get) Token: 0x060051E9 RID: 20969 RVA: 0x0016EE3C File Offset: 0x0016D23C
			public UnityEngine.Object asset
			{
				get
				{
					if (this.m_assets != null && this.m_assets.Length > 0)
					{
						return this.m_assets[0];
					}
					return null;
				}
			}

			// Token: 0x17000589 RID: 1417
			// (get) Token: 0x060051EA RID: 20970 RVA: 0x0016EE61 File Offset: 0x0016D261
			public UnityEngine.Object[] assets
			{
				get
				{
					return this.m_assets;
				}
			}

			// Token: 0x1700058A RID: 1418
			// (get) Token: 0x060051EB RID: 20971 RVA: 0x0016EE69 File Offset: 0x0016D269
			public bool IsReference
			{
				get
				{
					return this.m_refCount > 0;
				}
			}

			// Token: 0x1700058B RID: 1419
			// (get) Token: 0x060051EC RID: 20972 RVA: 0x0016EE74 File Offset: 0x0016D274
			public object Current
			{
				get
				{
					return null;
				}
			}

			// Token: 0x060051ED RID: 20973 RVA: 0x0016EE77 File Offset: 0x0016D277
			public void Reset()
			{
			}

			// Token: 0x060051EE RID: 20974 RVA: 0x0016EE79 File Offset: 0x0016D279
			public bool MoveNext()
			{
				return this.m_state != MeigeResource.Handler.State.Done;
			}

			// Token: 0x060051EF RID: 20975 RVA: 0x0016EE87 File Offset: 0x0016D287
			public void AddReferenceCount()
			{
				this.m_refCount++;
			}

			// Token: 0x060051F0 RID: 20976 RVA: 0x0016EE98 File Offset: 0x0016D298
			~Handler()
			{
				this.Unload(false);
				this.Clear();
			}

			// Token: 0x060051F1 RID: 20977 RVA: 0x0016EED0 File Offset: 0x0016D2D0
			public void Clear()
			{
				this.m_resourceName = string.Empty;
				this.m_option = 0;
				this.m_state = MeigeResource.Handler.State.Wait;
				this.m_error = MeigeResource.Error.None;
				this.m_assets = null;
				this.m_asyncOperation = null;
				this.m_isUnloadAssetbundle = false;
				this.m_refCount = 0;
			}

			// Token: 0x060051F2 RID: 20978 RVA: 0x0016EF0E File Offset: 0x0016D30E
			public T GetAsset<T>() where T : UnityEngine.Object
			{
				if (this.m_assets != null && this.m_assets.Length > 0)
				{
					return this.m_assets[0] as T;
				}
				return (T)((object)null);
			}

			// Token: 0x060051F3 RID: 20979 RVA: 0x0016EF44 File Offset: 0x0016D344
			public T[] GetAssets<T>() where T : UnityEngine.Object
			{
				if (this.m_assets == null)
				{
					return null;
				}
				T[] array = new T[this.m_assets.Length];
				for (int i = 0; i < this.m_assets.Length; i++)
				{
					array[i] = (this.m_assets[i] as T);
				}
				return array;
			}

			// Token: 0x060051F4 RID: 20980 RVA: 0x0016EF9F File Offset: 0x0016D39F
			public void Unload(bool isForce = false)
			{
				if (this.m_state == MeigeResource.Handler.State.UnloadDone)
				{
					return;
				}
				if (this.m_state == MeigeResource.Handler.State.Unloading)
				{
					return;
				}
				this.m_state = MeigeResource.Handler.State.Unloading;
				if (this._Unload(isForce))
				{
					this._UnloadScene();
				}
				this.m_state = MeigeResource.Handler.State.UnloadDone;
			}

			// Token: 0x060051F5 RID: 20981 RVA: 0x0016EFDC File Offset: 0x0016D3DC
			public IEnumerator UnloadAsync(bool isForce = false)
			{
				if (this.m_state == MeigeResource.Handler.State.UnloadDone)
				{
					yield break;
				}
				if (this.m_state == MeigeResource.Handler.State.Unloading)
				{
					yield break;
				}
				this.m_state = MeigeResource.Handler.State.Unloading;
				if (this._Unload(isForce))
				{
					yield return this._UnloadSceneAsync();
				}
				this.m_state = MeigeResource.Handler.State.UnloadDone;
				yield break;
			}

			// Token: 0x060051F6 RID: 20982 RVA: 0x0016F000 File Offset: 0x0016D400
			private bool _Unload(bool isForce = false)
			{
				this.m_refCount--;
				if (this.IsReferenceCounting() && !isForce && this.m_refCount > 0)
				{
					return false;
				}
				this.m_refCount = 0;
				if (this.m_assets != null)
				{
					for (int i = 0; i < this.m_assets.Length; i++)
					{
						if (this.m_assets[i] != null)
						{
							this.m_assets[i] = null;
						}
					}
					this.m_assets = null;
				}
				this.UnloadAssetbundle();
				this.m_error = MeigeResource.Error.None;
				return true;
			}

			// Token: 0x060051F7 RID: 20983 RVA: 0x0016F096 File Offset: 0x0016D496
			public bool IsUnloadAsync()
			{
				return this.IsScene() || this.IsSceneAdditive();
			}

			// Token: 0x060051F8 RID: 20984 RVA: 0x0016F0B3 File Offset: 0x0016D4B3
			private void UnloadAssetbundle()
			{
				if (this.IsDownload() && !this.m_isUnloadAssetbundle && !string.IsNullOrEmpty(this.m_assetbundleName))
				{
					AssetBundleManager.UnloadAssetBundle(this.m_assetbundleName);
					this.m_isUnloadAssetbundle = true;
				}
			}

			// Token: 0x060051F9 RID: 20985 RVA: 0x0016F0F0 File Offset: 0x0016D4F0
			private void _UnloadScene()
			{
				if (this.m_state != MeigeResource.Handler.State.Unloading)
				{
					return;
				}
				this.m_asyncOperation = null;
				if (!this.IsScene())
				{
					return;
				}
				if (!this.IsSceneAdditive())
				{
					return;
				}
				if (MeigeResourceManager.quit)
				{
					return;
				}
				string text = (!this.IsDownload()) ? this.m_resourceName : this.m_assetName;
				if (SceneManager.GetSceneByName(text).isLoaded)
				{
					this.m_asyncOperation = SceneManager.UnloadSceneAsync(text);
				}
			}

			// Token: 0x060051FA RID: 20986 RVA: 0x0016F170 File Offset: 0x0016D570
			private IEnumerator _UnloadSceneAsync()
			{
				this._UnloadScene();
				if (this.m_asyncOperation != null)
				{
					yield return this.m_asyncOperation;
				}
				yield break;
			}

			// Token: 0x060051FB RID: 20987 RVA: 0x0016F18C File Offset: 0x0016D58C
			public IEnumerator LoadAsync()
			{
				while (!MeigeResourceManager.isReadyAssetbundle || MeigeResourceManager.isError)
				{
					yield return null;
				}
				if (this.m_state != MeigeResource.Handler.State.Wait)
				{
					yield break;
				}
				this.m_state = MeigeResource.Handler.State.Loading;
				int retry = 0;
				while (MeigeResourceManager.retryCount > retry)
				{
					if (retry != 0)
					{
						this.UnloadAssetbundle();
					}
					retry++;
					if (this.IsScene())
					{
						bool isAdditive = this.IsOption(MeigeResource.Option.SceneAdditive);
						bool allowSceneActivation = !this.IsSceneActivationFalse();
						if (!this.IsDownload())
						{
							LoadSceneMode mode = LoadSceneMode.Single;
							if (isAdditive)
							{
								mode = LoadSceneMode.Additive;
							}
							this.m_asyncOperation = SceneManager.LoadSceneAsync(this.m_resourceName, mode);
							this.m_asyncOperation.allowSceneActivation = allowSceneActivation;
							yield return this.m_asyncOperation;
						}
						else
						{
							this.m_assetbundleName = this.m_resourceName.ToLower();
							if (this.IsDownloadOnly())
							{
								this.m_isUnloadAssetbundle = true;
								AssetBundleLoadOperation req = AssetBundleManager.DownloadOnlyAsync(this.m_assetbundleName, null);
								yield return req;
								if (req.GetError() == ErrorType.Timeout)
								{
									this.SetError(MeigeResource.Error.Timeout);
									continue;
								}
								if (req.GetError() == ErrorType.Unknown)
								{
									this.SetError(MeigeResource.Error.Unknown);
								}
								else
								{
									this.SetError(MeigeResource.Error.None);
								}
							}
							else
							{
								this.m_isUnloadAssetbundle = false;
								AssetBundleLoadOperation req2 = AssetBundleManager.LoadLevelAsync(this.m_assetbundleName, this.m_assetName, isAdditive, allowSceneActivation);
								yield return req2;
								this.m_asyncOperation = req2.asyncOperation;
								if (req2.GetError() == ErrorType.Timeout)
								{
									this.SetError(MeigeResource.Error.Timeout);
									continue;
								}
								if (req2.GetError() == ErrorType.Unknown)
								{
									this.SetError(MeigeResource.Error.Unknown);
								}
								else
								{
									this.SetError(MeigeResource.Error.None);
									bool IsDependence = AssetBundleManager.IsDependence(this.m_assetbundleName);
									if (!this.IsOption(MeigeResource.Option.ResourceKeep) && !IsDependence)
									{
										AssetBundleManager.UnloadAssetBundle(this.m_assetbundleName);
										this.m_isUnloadAssetbundle = true;
									}
								}
							}
						}
					}
					else if (!this.IsDownload())
					{
						this.m_assets = Resources.LoadAll<UnityEngine.Object>(this.m_resourceName);
						if (this.m_assets == null)
						{
							Debug.LogError("File does not exist : " + this.m_resourceName);
							this.m_error = MeigeResource.Error.Unknown;
						}
					}
					else
					{
						this.m_assetbundleName = this.m_resourceName.ToLower();
						if (this.IsDownloadOnly())
						{
							this.m_isUnloadAssetbundle = true;
							AssetBundleLoadOperation req3 = AssetBundleManager.DownloadOnlyAsync(this.m_assetbundleName, null);
							yield return req3;
							if (req3.GetError() == ErrorType.Timeout)
							{
								this.SetError(MeigeResource.Error.Timeout);
								continue;
							}
							if (req3.GetError() == ErrorType.Unknown)
							{
								this.SetError(MeigeResource.Error.Unknown);
							}
							else
							{
								this.SetError(MeigeResource.Error.None);
							}
						}
						else
						{
							this.m_isUnloadAssetbundle = false;
							AssetBundleLoadAssetOperation req4 = AssetBundleManager.LoadAssetAsync(this.m_assetbundleName, this.m_assetName, typeof(UnityEngine.Object), null);
							yield return req4;
							if (req4.GetError() == ErrorType.Timeout)
							{
								this.SetError(MeigeResource.Error.Timeout);
								continue;
							}
							if (req4.GetError() == ErrorType.Unknown)
							{
								this.SetError(MeigeResource.Error.Unknown);
							}
							else
							{
								this.m_assets = req4.GetAllAsset<UnityEngine.Object>();
								this.SetError(MeigeResource.Error.None);
								bool IsDependence2 = AssetBundleManager.IsDependence(this.m_assetbundleName);
								if (!this.IsOption(MeigeResource.Option.ResourceKeep) && !IsDependence2)
								{
									AssetBundleManager.UnloadAssetBundle(this.m_assetbundleName);
									this.m_isUnloadAssetbundle = true;
								}
							}
						}
					}
					break;
				}
				this.m_state = MeigeResource.Handler.State.Done;
				if (this.m_error != MeigeResource.Error.None)
				{
					MeigeResourceManager.SetError(this);
					yield break;
				}
				if (this.m_callback != null)
				{
					this.m_callback(this);
				}
				yield break;
			}

			// Token: 0x060051FC RID: 20988 RVA: 0x0016F1A8 File Offset: 0x0016D5A8
			public IEnumerator Retry()
			{
				yield return this.UnloadAsync(false);
				this.m_state = MeigeResource.Handler.State.Wait;
				yield return this.LoadAsync();
				yield break;
			}

			// Token: 0x060051FD RID: 20989 RVA: 0x0016F1C3 File Offset: 0x0016D5C3
			public string GetVersion()
			{
				if (string.IsNullOrEmpty(this.m_assetbundleName))
				{
					return string.Empty;
				}
				return AssetBundleManager.GetBundleVersion(this.m_assetbundleName);
			}

			// Token: 0x060051FE RID: 20990 RVA: 0x0016F1E6 File Offset: 0x0016D5E6
			private void SetError(MeigeResource.Error error)
			{
				this.m_error = error;
			}

			// Token: 0x060051FF RID: 20991 RVA: 0x0016F1EF File Offset: 0x0016D5EF
			private bool IsScene()
			{
				return (this.m_option & 3) != 0;
			}

			// Token: 0x06005200 RID: 20992 RVA: 0x0016F1FF File Offset: 0x0016D5FF
			private bool IsSceneAdditive()
			{
				return this.IsOption(MeigeResource.Option.SceneAdditive);
			}

			// Token: 0x06005201 RID: 20993 RVA: 0x0016F208 File Offset: 0x0016D608
			private bool IsSceneActivationFalse()
			{
				return this.IsOption(MeigeResource.Option.SceneActivationFalse);
			}

			// Token: 0x06005202 RID: 20994 RVA: 0x0016F211 File Offset: 0x0016D611
			private bool IsDownloadOnly()
			{
				return this.IsOption(MeigeResource.Option.DownloadOnly);
			}

			// Token: 0x06005203 RID: 20995 RVA: 0x0016F21A File Offset: 0x0016D61A
			private bool IsReferenceCounting()
			{
				return this.IsOption(MeigeResource.Option.ReferenceCounting);
			}

			// Token: 0x06005204 RID: 20996 RVA: 0x0016F224 File Offset: 0x0016D624
			private bool IsOption(MeigeResource.Option option)
			{
				return (this.m_option & (int)option) != 0;
			}

			// Token: 0x06005205 RID: 20997 RVA: 0x0016F234 File Offset: 0x0016D634
			private bool IsDownload()
			{
				return this.m_resourceName.Contains(".muast");
			}

			// Token: 0x04005242 RID: 21058
			protected string m_resourceName;

			// Token: 0x04005243 RID: 21059
			protected string m_assetName;

			// Token: 0x04005244 RID: 21060
			protected string m_assetbundleName;

			// Token: 0x04005245 RID: 21061
			protected int m_option;

			// Token: 0x04005246 RID: 21062
			protected Action<MeigeResource.Handler> m_callback;

			// Token: 0x04005247 RID: 21063
			protected MeigeResource.Handler.State m_state;

			// Token: 0x04005248 RID: 21064
			protected MeigeResource.Error m_error;

			// Token: 0x04005249 RID: 21065
			protected UnityEngine.Object[] m_assets;

			// Token: 0x0400524A RID: 21066
			protected AsyncOperation m_asyncOperation;

			// Token: 0x0400524B RID: 21067
			protected bool m_isUnloadAssetbundle;

			// Token: 0x0400524C RID: 21068
			protected int m_refCount;

			// Token: 0x02000F78 RID: 3960
			public enum State
			{
				// Token: 0x0400524E RID: 21070
				Wait,
				// Token: 0x0400524F RID: 21071
				Loading,
				// Token: 0x04005250 RID: 21072
				Done,
				// Token: 0x04005251 RID: 21073
				Unloading,
				// Token: 0x04005252 RID: 21074
				UnloadDone
			}
		}
	}
}
