﻿using System;
using System.Collections;
using System.Collections.Generic;
using Meige.AssetBundles;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F79 RID: 3961
	public class MeigeResourceManager : SingletonMonoBehaviour<MeigeResourceManager>
	{
		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06005207 RID: 20999 RVA: 0x0016FAF3 File Offset: 0x0016DEF3
		public static bool quit
		{
			get
			{
				return MeigeResourceManager.m_quit;
			}
		}

		// Token: 0x06005208 RID: 21000 RVA: 0x0016FAFA File Offset: 0x0016DEFA
		private void OnApplicationQuit()
		{
			MeigeResourceManager.m_quit = true;
		}

		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x0600520A RID: 21002 RVA: 0x0016FB0F File Offset: 0x0016DF0F
		// (set) Token: 0x06005209 RID: 21001 RVA: 0x0016FB02 File Offset: 0x0016DF02
		public static int retryCount
		{
			get
			{
				return SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_retryCount;
			}
			set
			{
				SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_retryCount = value;
			}
		}

		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x0600520C RID: 21004 RVA: 0x0016FB23 File Offset: 0x0016DF23
		// (set) Token: 0x0600520B RID: 21003 RVA: 0x0016FB1B File Offset: 0x0016DF1B
		public static float timeoutSec
		{
			get
			{
				return AssetBundleManager.timeoutSec;
			}
			set
			{
				AssetBundleManager.timeoutSec = value;
			}
		}

		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x0600520D RID: 21005 RVA: 0x0016FB2A File Offset: 0x0016DF2A
		public static bool isReadyAssetbundle
		{
			get
			{
				return AssetBundleManager.isReady;
			}
		}

		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x0600520E RID: 21006 RVA: 0x0016FB31 File Offset: 0x0016DF31
		public static bool isError
		{
			get
			{
				return SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.Count > 0;
			}
		}

		// Token: 0x0600520F RID: 21007 RVA: 0x0016FB45 File Offset: 0x0016DF45
		public static void SetAssetbundleServer(string url)
		{
			AssetBundleManager.Initialize(url);
		}

		// Token: 0x06005210 RID: 21008 RVA: 0x0016FB4D File Offset: 0x0016DF4D
		public static void AssetbundleCleanCache()
		{
			AssetbundleCache.CleanCache(string.Empty);
			AssetBundleManager.ClearFlag();
		}

		// Token: 0x06005211 RID: 21009 RVA: 0x0016FB60 File Offset: 0x0016DF60
		public static IEnumerator UpdateAssetbundleManifest()
		{
			yield return SingletonMonoBehaviour<AssetBundleManager>.Instance.UpdateManifest();
			yield break;
		}

		// Token: 0x06005212 RID: 21010 RVA: 0x0016FB74 File Offset: 0x0016DF74
		public static string GetVersion(string resName)
		{
			if (string.IsNullOrEmpty(resName))
			{
				return string.Empty;
			}
			if (!resName.Contains(".muast"))
			{
				resName += ".muast";
			}
			string assetBundleName = resName.ToLower();
			return AssetBundleManager.GetBundleVersion(assetBundleName);
		}

		// Token: 0x06005213 RID: 21011 RVA: 0x0016FBBC File Offset: 0x0016DFBC
		public static Dictionary<string, MeigeResource.Error> GetError()
		{
			return new Dictionary<string, MeigeResource.Error>(SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList);
		}

		// Token: 0x06005214 RID: 21012 RVA: 0x0016FBD0 File Offset: 0x0016DFD0
		public static void SetError(MeigeResource.Handler handler)
		{
			if (!SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.ContainsKey(handler.resourceName))
			{
				SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.Add(handler.resourceName, handler.error);
			}
			else
			{
				SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList[handler.resourceName] = handler.error;
			}
		}

		// Token: 0x06005215 RID: 21013 RVA: 0x0016FC32 File Offset: 0x0016E032
		public static void ClearError()
		{
			SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.Clear();
		}

		// Token: 0x06005216 RID: 21014 RVA: 0x0016FC43 File Offset: 0x0016E043
		public static void ClearError(string resourceName)
		{
			if (SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.ContainsKey(resourceName))
			{
				SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_errorList.Remove(resourceName);
			}
		}

		// Token: 0x06005217 RID: 21015 RVA: 0x0016FC6B File Offset: 0x0016E06B
		public static int GetLoadingCount()
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._GetLoadingCount();
		}

		// Token: 0x06005218 RID: 21016 RVA: 0x0016FC77 File Offset: 0x0016E077
		private int _GetLoadingCount()
		{
			return this._LoadingList().Count;
		}

		// Token: 0x06005219 RID: 21017 RVA: 0x0016FC84 File Offset: 0x0016E084
		public static List<string> GetLoadingList()
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._LoadingList();
		}

		// Token: 0x0600521A RID: 21018 RVA: 0x0016FC90 File Offset: 0x0016E090
		private List<string> _LoadingList()
		{
			List<string> list = new List<string>();
			foreach (string text in this.m_resourceHandlers.Keys)
			{
				MeigeResource.Handler handler = this.m_resourceHandlers[text];
				if (!handler.IsDone || handler.IsError)
				{
					list.Add(text);
				}
			}
			return list;
		}

		// Token: 0x0600521B RID: 21019 RVA: 0x0016FD1C File Offset: 0x0016E11C
		public static MeigeResource.Handler LoadHandler(string resourceName, params MeigeResource.Option[] options)
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._LoadHandler(resourceName, null, null, options);
		}

		// Token: 0x0600521C RID: 21020 RVA: 0x0016FD2C File Offset: 0x0016E12C
		public static MeigeResource.Handler LoadHandler(string resourceName, string assetName, params MeigeResource.Option[] options)
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._LoadHandler(resourceName, assetName, null, options);
		}

		// Token: 0x0600521D RID: 21021 RVA: 0x0016FD3C File Offset: 0x0016E13C
		public static MeigeResource.Handler LoadHandler(string resourceName, Action<MeigeResource.Handler> callback, params MeigeResource.Option[] options)
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._LoadHandler(resourceName, null, callback, options);
		}

		// Token: 0x0600521E RID: 21022 RVA: 0x0016FD4C File Offset: 0x0016E14C
		public static MeigeResource.Handler LoadHandler(string resourceName, string assetName, Action<MeigeResource.Handler> callback, params MeigeResource.Option[] options)
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._LoadHandler(resourceName, assetName, callback, options);
		}

		// Token: 0x0600521F RID: 21023 RVA: 0x0016FD5C File Offset: 0x0016E15C
		private MeigeResource.Handler _LoadHandler(string resourceName, string assetName = null, Action<MeigeResource.Handler> callback = null, params MeigeResource.Option[] options)
		{
			if (SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_resourceHandlers.ContainsKey(resourceName))
			{
				MeigeResource.Handler handler = SingletonMonoBehaviour<MeigeResourceManager>.Instance.m_resourceHandlers[resourceName];
				if (handler == null)
				{
					Debug.LogWarning("Handler does not exist : " + resourceName);
				}
				else if (handler.IsWait || handler.IsLoading || handler.IsError || handler.asset != null)
				{
					handler.AddReferenceCount();
					return handler;
				}
				this._UnloadHandler(resourceName, false);
			}
			MeigeResource.Handler handler2 = new MeigeResource.Handler(resourceName, assetName, callback, options);
			this.m_resourceHandlers.Add(resourceName, handler2);
			base.StartCoroutine(handler2.LoadAsync());
			return handler2;
		}

		// Token: 0x06005220 RID: 21024 RVA: 0x0016FE12 File Offset: 0x0016E212
		public static void RetryHandler(MeigeResource.Handler handler)
		{
			SingletonMonoBehaviour<MeigeResourceManager>.Instance._RetryHandler(handler);
		}

		// Token: 0x06005221 RID: 21025 RVA: 0x0016FE1F File Offset: 0x0016E21F
		public void _RetryHandler(MeigeResource.Handler handler)
		{
			MeigeResourceManager.ClearError(handler.resourceName);
			base.StartCoroutine(handler.Retry());
		}

		// Token: 0x06005222 RID: 21026 RVA: 0x0016FE39 File Offset: 0x0016E239
		public static void UnloadHandler(MeigeResource.Handler handler, bool isForce = false)
		{
			SingletonMonoBehaviour<MeigeResourceManager>.Instance._UnloadHandler(handler, isForce);
		}

		// Token: 0x06005223 RID: 21027 RVA: 0x0016FE47 File Offset: 0x0016E247
		private void _UnloadHandler(MeigeResource.Handler handler, bool isForce = false)
		{
			SingletonMonoBehaviour<MeigeResourceManager>.Instance._UnloadHandler(handler.resourceName, isForce);
		}

		// Token: 0x06005224 RID: 21028 RVA: 0x0016FE5C File Offset: 0x0016E25C
		private void _UnloadHandler(string resourceName, bool isForce = false)
		{
			if (this.m_resourceHandlers.ContainsKey(resourceName))
			{
				MeigeResource.Handler handler = this.m_resourceHandlers[resourceName];
				handler.Unload(isForce);
				if (!handler.IsReference)
				{
					this.m_resourceHandlers.Remove(resourceName);
				}
			}
		}

		// Token: 0x06005225 RID: 21029 RVA: 0x0016FEA6 File Offset: 0x0016E2A6
		public static Dictionary<string, MeigeResource.Handler> GetHandlerAll()
		{
			return SingletonMonoBehaviour<MeigeResourceManager>.Instance._GetHandlerAll();
		}

		// Token: 0x06005226 RID: 21030 RVA: 0x0016FEB2 File Offset: 0x0016E2B2
		private Dictionary<string, MeigeResource.Handler> _GetHandlerAll()
		{
			return this.m_resourceHandlers;
		}

		// Token: 0x06005227 RID: 21031 RVA: 0x0016FEBC File Offset: 0x0016E2BC
		public static IEnumerator UnloadHandlerAllAsync()
		{
			yield return SingletonMonoBehaviour<MeigeResourceManager>.Instance._UnloadHandlerAllAsync();
			yield break;
		}

		// Token: 0x06005228 RID: 21032 RVA: 0x0016FED0 File Offset: 0x0016E2D0
		public static void UnloadHandlerAll()
		{
			SingletonMonoBehaviour<MeigeResourceManager>.Instance._UnloadHandlerAll();
		}

		// Token: 0x06005229 RID: 21033 RVA: 0x0016FEDC File Offset: 0x0016E2DC
		private void _UnloadHandlerAll()
		{
			foreach (KeyValuePair<string, MeigeResource.Handler> keyValuePair in this.m_resourceHandlers)
			{
				base.StopCoroutine(keyValuePair.Value.LoadAsync());
				keyValuePair.Value.Unload(true);
			}
			this.m_resourceHandlers.Clear();
		}

		// Token: 0x0600522A RID: 21034 RVA: 0x0016FF5C File Offset: 0x0016E35C
		private IEnumerator _UnloadHandlerAllAsync()
		{
			foreach (KeyValuePair<string, MeigeResource.Handler> pair in this.m_resourceHandlers)
			{
				base.StopCoroutine(pair.Value.LoadAsync());
				if (pair.Value.IsUnloadAsync())
				{
					yield return pair.Value.UnloadAsync(true);
				}
				else
				{
					pair.Value.Unload(false);
				}
			}
			this.m_resourceHandlers.Clear();
			yield break;
		}

		// Token: 0x04005253 RID: 21075
		public const int DEFAULT_RETRY_COUNT = 5;

		// Token: 0x04005254 RID: 21076
		public const string AB_EXTENSION = ".muast";

		// Token: 0x04005255 RID: 21077
		private int m_retryCount = 5;

		// Token: 0x04005256 RID: 21078
		private Dictionary<string, MeigeResource.Handler> m_resourceHandlers = new Dictionary<string, MeigeResource.Handler>();

		// Token: 0x04005257 RID: 21079
		private Dictionary<string, MeigeResource.Error> m_errorList = new Dictionary<string, MeigeResource.Error>();

		// Token: 0x04005258 RID: 21080
		private static bool m_quit;
	}
}
