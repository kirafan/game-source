﻿using System;

namespace Meige
{
	// Token: 0x02000F91 RID: 3985
	public enum eCompareFunc
	{
		// Token: 0x04005368 RID: 21352
		Disabled,
		// Token: 0x04005369 RID: 21353
		Never,
		// Token: 0x0400536A RID: 21354
		Less,
		// Token: 0x0400536B RID: 21355
		Equal,
		// Token: 0x0400536C RID: 21356
		LessEqual,
		// Token: 0x0400536D RID: 21357
		Greater,
		// Token: 0x0400536E RID: 21358
		NotEqual,
		// Token: 0x0400536F RID: 21359
		GreaterEqual,
		// Token: 0x04005370 RID: 21360
		Always
	}
}
