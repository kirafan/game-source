﻿using System;

namespace Meige
{
	// Token: 0x02000F88 RID: 3976
	public enum eShadingType
	{
		// Token: 0x0400531A RID: 21274
		eShadingType_Invalid = -1,
		// Token: 0x0400531B RID: 21275
		eShadingType_Unlight,
		// Token: 0x0400531C RID: 21276
		eShadingType_BlinnPhong,
		// Token: 0x0400531D RID: 21277
		eShadingType_Toon,
		// Token: 0x0400531E RID: 21278
		eShadingType_BlinnToon,
		// Token: 0x0400531F RID: 21279
		eShadingType_Outline,
		// Token: 0x04005320 RID: 21280
		eShadingType_Regular_Max,
		// Token: 0x04005321 RID: 21281
		eShadingType_User_Start = 256,
		// Token: 0x04005322 RID: 21282
		eShadingType_User_Before = 255,
		// Token: 0x04005323 RID: 21283
		eShadingType_PostProcess_Start = 1024,
		// Token: 0x04005324 RID: 21284
		eShadingType_PostProcess_Before = 1023,
		// Token: 0x04005325 RID: 21285
		eShadingType_ColorCorrection,
		// Token: 0x04005326 RID: 21286
		eShadingType_BrightPassFilter,
		// Token: 0x04005327 RID: 21287
		eShadingType_Gauss,
		// Token: 0x04005328 RID: 21288
		eShadingType_Particle,
		// Token: 0x04005329 RID: 21289
		eShadingType_PostProcessUser_Start = 1280,
		// Token: 0x0400532A RID: 21290
		eShadingType_PostProcessUser_Before = 1279
	}
}
