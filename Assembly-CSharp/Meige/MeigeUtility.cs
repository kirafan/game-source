﻿using System;
using System.Xml;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F9E RID: 3998
	public static class MeigeUtility
	{
		// Token: 0x0600528B RID: 21131 RVA: 0x001718E4 File Offset: 0x0016FCE4
		public static int MakeEnumFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeEnumFromXmlNode ... XmlNode is not existed.");
				return -1;
			}
			string[] array = xmlNode.InnerXml.Split(":".ToCharArray());
			return int.Parse(array[0]);
		}

		// Token: 0x0600528C RID: 21132 RVA: 0x00171922 File Offset: 0x0016FD22
		public static bool MakeBoolFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeBoolFromXmlNode ... XmlNode is not existed.");
				return false;
			}
			return xmlNode.InnerXml.Equals("True");
		}

		// Token: 0x0600528D RID: 21133 RVA: 0x00171947 File Offset: 0x0016FD47
		public static int MakeIntFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeIntFromXmlNode ... XmlNode is not existed.");
				return 0;
			}
			return int.Parse(xmlNode.InnerXml);
		}

		// Token: 0x0600528E RID: 21134 RVA: 0x00171967 File Offset: 0x0016FD67
		public static float MakeFloatFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeFloatFromXmlNode ... XmlNode is not existed.");
				return 0f;
			}
			return float.Parse(xmlNode.InnerXml);
		}

		// Token: 0x0600528F RID: 21135 RVA: 0x0017198B File Offset: 0x0016FD8B
		public static string MakeStringFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeStringFromXmlNode ... XmlNode is not existed.");
				return string.Empty;
			}
			return xmlNode.InnerXml;
		}

		// Token: 0x06005290 RID: 21136 RVA: 0x001719AC File Offset: 0x0016FDAC
		public static Vector2 MakeFloat2FromXmlNode(XmlNode xmlNode)
		{
			string[] array = xmlNode.InnerXml.Split(":".ToCharArray());
			string[] array2 = array[1].Split(",".ToCharArray());
			Vector2 result;
			result.x = float.Parse(array2[0]);
			result.y = float.Parse(array2[1]);
			return result;
		}

		// Token: 0x06005291 RID: 21137 RVA: 0x00171A04 File Offset: 0x0016FE04
		public static Vector4 MakeVectorFromXmlNode(XmlNode xmlNode)
		{
			string[] array = xmlNode.InnerXml.Split(":".ToCharArray());
			string[] array2 = array[1].Split(",".ToCharArray());
			Vector4 result;
			result.x = float.Parse(array2[0]);
			result.y = float.Parse(array2[1]);
			result.z = float.Parse(array2[2]);
			result.w = float.Parse(array2[3]);
			return result;
		}

		// Token: 0x06005292 RID: 21138 RVA: 0x00171A78 File Offset: 0x0016FE78
		public static void MakeIntArrayFromXmlNode(out int[] iArray, XmlNode xmlNode)
		{
			string[] array = xmlNode.InnerXml.Split(",".ToCharArray());
			iArray = new int[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				iArray[i] = int.Parse(array[i]);
			}
		}

		// Token: 0x06005293 RID: 21139 RVA: 0x00171AC8 File Offset: 0x0016FEC8
		public static Color MakeColorFromXmlNode(XmlNode xmlNode)
		{
			if (xmlNode == null)
			{
				Helper.Break(false, "MakeColorFromXmlNode ... XmlNode is not existed.");
				return Color.black;
			}
			Vector4 v = MeigeUtility.MakeVectorFromXmlNode(xmlNode);
			return v;
		}

		// Token: 0x06005294 RID: 21140 RVA: 0x00171AFC File Offset: 0x0016FEFC
		public static Rect MakeRectFromXmlNode(XmlNode xmlNode)
		{
			Vector4 vector = MeigeUtility.MakeVectorFromXmlNode(xmlNode);
			Rect result = new Rect(vector.x, vector.y, vector.z, vector.w);
			return result;
		}

		// Token: 0x06005295 RID: 21141 RVA: 0x00171B34 File Offset: 0x0016FF34
		public static Bounds MakeAABBFromXmlNode(XmlNode xmlNode)
		{
			return new Bounds
			{
				min = MeigeUtility.MakeVectorFromXmlNode(xmlNode.SelectSingleNode("AABB.Min")),
				max = MeigeUtility.MakeVectorFromXmlNode(xmlNode.SelectSingleNode("AABB.Max"))
			};
		}

		// Token: 0x06005296 RID: 21142 RVA: 0x00171B84 File Offset: 0x0016FF84
		public static ProjDepend.eRenderQueue RenderStageToRenderQueue(eRenderStage renderStage, int renderOrder)
		{
			int num = renderOrder % 125;
			if (renderStage < eRenderStage.eRenderStage_Alpha)
			{
				return (ProjDepend.eRenderQueue)(renderStage * (eRenderStage)125 + num);
			}
			return (ProjDepend.eRenderQueue)(renderStage * (eRenderStage)125 + num + 1);
		}

		// Token: 0x06005297 RID: 21143 RVA: 0x00171BB0 File Offset: 0x0016FFB0
		public static bool FloatEqual(float f0, float f1, float threshold = 1E-06f)
		{
			float num = Mathf.Abs(f0 - f1);
			return num <= threshold;
		}

		// Token: 0x06005298 RID: 21144 RVA: 0x00171BCD File Offset: 0x0016FFCD
		public static Vector3 CM2M(Vector3 v)
		{
			return v / 100f;
		}

		// Token: 0x06005299 RID: 21145 RVA: 0x00171BDA File Offset: 0x0016FFDA
		public static float CM2M(float v)
		{
			return v / 100f;
		}
	}
}
