﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F0E RID: 3854
	public class MeigeParticleEmitter : EffectComponentBase
	{
		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x0600504F RID: 20559 RVA: 0x001600B0 File Offset: 0x0015E4B0
		// (set) Token: 0x0600504E RID: 20558 RVA: 0x001600A7 File Offset: 0x0015E4A7
		public override bool isAlive
		{
			get
			{
				return this.m_isAlive;
			}
			protected set
			{
				this.m_isAlive = value;
			}
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06005051 RID: 20561 RVA: 0x001600E8 File Offset: 0x0015E4E8
		// (set) Token: 0x06005050 RID: 20560 RVA: 0x001600B8 File Offset: 0x0015E4B8
		public Vector3 offsetRot
		{
			get
			{
				return this.m_offsetRot;
			}
			set
			{
				this.m_offsetRot = value;
				if (this.m_RenderObject != null)
				{
					this.m_RenderObject.transform.localEulerAngles = this.m_offsetRot;
				}
			}
		}

		// Token: 0x06005052 RID: 20562 RVA: 0x001600F0 File Offset: 0x0015E4F0
		public void AddHook_UpdateShot(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_Shot = (MeigeParticleEmitter.HookUpdate)Delegate.Combine(this.m_UpdateHook_Shot, hook);
		}

		// Token: 0x06005053 RID: 20563 RVA: 0x00160109 File Offset: 0x0015E509
		public void RemoveHook_UpdateShot(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_Shot = (MeigeParticleEmitter.HookUpdate)Delegate.Remove(this.m_UpdateHook_Shot, hook);
		}

		// Token: 0x06005054 RID: 20564 RVA: 0x00160122 File Offset: 0x0015E522
		public void AddHook_UpdateBefore(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_Before = (MeigeParticleEmitter.HookUpdate)Delegate.Combine(this.m_UpdateHook_Before, hook);
		}

		// Token: 0x06005055 RID: 20565 RVA: 0x0016013B File Offset: 0x0015E53B
		public void RemoveHook_UpdateBefore(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_Before = (MeigeParticleEmitter.HookUpdate)Delegate.Remove(this.m_UpdateHook_Before, hook);
		}

		// Token: 0x06005056 RID: 20566 RVA: 0x00160154 File Offset: 0x0015E554
		public void AddHook_UpdateAfter(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_After = (MeigeParticleEmitter.HookUpdate)Delegate.Combine(this.m_UpdateHook_After, hook);
		}

		// Token: 0x06005057 RID: 20567 RVA: 0x0016016D File Offset: 0x0015E56D
		public void RemoveHook_UpdateAfter(MeigeParticleEmitter.HookUpdate hook)
		{
			this.m_UpdateHook_After = (MeigeParticleEmitter.HookUpdate)Delegate.Remove(this.m_UpdateHook_After, hook);
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06005058 RID: 20568 RVA: 0x00160186 File Offset: 0x0015E586
		public float time
		{
			get
			{
				return this.m_Time;
			}
		}

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06005059 RID: 20569 RVA: 0x0016018E File Offset: 0x0015E58E
		public float framerate
		{
			get
			{
				return this.m_FrameRate;
			}
		}

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x0600505B RID: 20571 RVA: 0x0016019F File Offset: 0x0015E59F
		// (set) Token: 0x0600505A RID: 20570 RVA: 0x00160196 File Offset: 0x0015E596
		public ParticleRule rule
		{
			get
			{
				return this.m_Rule;
			}
			set
			{
				this.m_Rule = value;
			}
		}

		// Token: 0x0600505C RID: 20572 RVA: 0x001601A7 File Offset: 0x0015E5A7
		public static float GetRandomRadian()
		{
			return Helper.GetFRandom(-3.141592f, 3.141592f);
		}

		// Token: 0x0600505D RID: 20573 RVA: 0x001601B8 File Offset: 0x0015E5B8
		public static float GetRandomDegree()
		{
			return 57.29578f * MeigeParticleEmitter.GetRandomRadian();
		}

		// Token: 0x0600505E RID: 20574 RVA: 0x001601C5 File Offset: 0x0015E5C5
		public void SetLayer(int layer)
		{
			this.m_Layer = layer;
			if (this.m_RenderObject != null)
			{
				this.m_RenderObject.layer = layer;
			}
		}

		// Token: 0x0600505F RID: 20575 RVA: 0x001601EB File Offset: 0x0015E5EB
		private void FlushTime()
		{
			this.m_Time = Time.deltaTime * this.rule.m_frameSpeedScale;
			this.m_FrameRate = Time.deltaTime / Time.fixedDeltaTime * this.rule.m_frameSpeedScale;
		}

		// Token: 0x06005060 RID: 20576 RVA: 0x00160221 File Offset: 0x0015E621
		private void ResetTransform(Transform trans)
		{
			trans.localPosition = Vector3.zero;
			trans.localEulerAngles = Vector3.zero;
			trans.localScale = Vector3.one;
		}

		// Token: 0x06005061 RID: 20577 RVA: 0x00160244 File Offset: 0x0015E644
		private void ChangeParent()
		{
			if (this.m_PrimBillboardBuffer)
			{
				this.m_PrimBillboardBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimBillboardBuffer.transform);
			}
			if (this.m_PrimPointBuffer)
			{
				this.m_PrimPointBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimPointBuffer.transform);
			}
			if (this.m_PrimLineBuffer)
			{
				this.m_PrimLineBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimLineBuffer.transform);
			}
			if (this.m_PrimConfettiBuffer)
			{
				this.m_PrimConfettiBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimConfettiBuffer.transform);
			}
			if (this.m_PrimPolylineBuffer)
			{
				this.m_PrimPolylineBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimPolylineBuffer.transform);
			}
			if (this.m_PrimRibbonBuffer)
			{
				this.m_PrimRibbonBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_PrimRibbonBuffer.transform);
			}
			if (this.m_TailRenderObject)
			{
				this.m_TailRenderObject.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
				this.ResetTransform(this.m_TailRenderObject.transform);
			}
		}

		// Token: 0x06005062 RID: 20578 RVA: 0x001603F0 File Offset: 0x0015E7F0
		public void SetSortingOrder(int order)
		{
			this.m_SortingOrder = order;
			if (this.m_EffectRenderer != null)
			{
				this.m_EffectRenderer.SetSortingOrder(this.m_SortingOrder);
			}
			if (this.m_TailRenderer != null)
			{
				this.m_TailRenderer.SetSortingOrder(this.m_SortingOrder);
			}
		}

		// Token: 0x06005063 RID: 20579 RVA: 0x00160448 File Offset: 0x0015E848
		private void DestroyObject(bool isForce)
		{
			this.KillOnlyParticles();
			this.m_ActiveUnitArray.Clear();
			this.m_InActiveUnitArray.Clear();
			if (this.m_History.m_particleType != this.rule.m_particleType || isForce)
			{
				if (this.m_PrimBillboardBuffer)
				{
					this.m_PrimBillboardBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimBillboardBuffer);
					this.m_PrimBillboardBuffer.RemoveBuffer(this.m_BillboardBuffer);
					this.m_PrimBillboardBuffer = null;
					this.m_BillboardBuffer = null;
				}
				if (this.m_PrimPointBuffer)
				{
					this.m_PrimPointBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimPointBuffer);
					this.m_PrimPointBuffer.RemoveBuffer(this.m_PointBuffer);
					this.m_PrimPointBuffer = null;
					this.m_PointBuffer = null;
				}
				if (this.m_PrimLineBuffer)
				{
					this.m_PrimLineBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimLineBuffer);
					this.m_PrimLineBuffer.RemoveBuffer(this.m_LineBuffer);
					this.m_PrimLineBuffer = null;
					this.m_LineBuffer = null;
				}
				if (this.m_PrimPolylineBuffer)
				{
					this.m_PrimPolylineBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimPolylineBuffer);
					this.m_PrimPolylineBuffer.RemoveBuffer(this.m_PolylineBuffer);
					this.m_PrimPolylineBuffer = null;
					this.m_PolylineBuffer = null;
				}
				if (this.m_PrimConfettiBuffer)
				{
					this.m_PrimConfettiBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimConfettiBuffer);
					this.m_PrimConfettiBuffer.RemoveBuffer(this.m_ConfettiBuffer);
					this.m_PrimConfettiBuffer = null;
					this.m_ConfettiBuffer = null;
				}
				if (this.m_PrimRibbonBuffer)
				{
					this.m_PrimRibbonBuffer.transform.DetachChildren();
					UnityEngine.Object.Destroy(this.m_PrimRibbonBuffer);
					this.m_PrimRibbonBuffer.RemoveBuffer(this.m_RibbonBuffer);
					this.m_PrimRibbonBuffer = null;
					this.m_RibbonBuffer = null;
				}
			}
			if ((this.m_History.m_UsingTailFlg != this.rule.m_UsingTailFlg || this.m_History.m_particleNum != this.rule.m_particleNum || isForce) && this.m_TailRenderObject)
			{
				this.m_TailRenderObject.transform.DetachChildren();
				UnityEngine.Object.Destroy(this.m_TailRenderObject);
				this.m_PrimTailBuffer.RemoveBuffer(this.m_TailBuffer);
				this.m_PrimTailBuffer = null;
				this.m_TailBuffer = null;
			}
			if (this.m_History.m_particleNum != this.rule.m_particleNum || isForce)
			{
				if (this.m_BillboardBuffer != null)
				{
					this.m_PrimBillboardBuffer.RemoveBuffer(this.m_BillboardBuffer);
					this.m_BillboardBuffer = null;
				}
				if (this.m_PointBuffer != null)
				{
					this.m_PrimPointBuffer.RemoveBuffer(this.m_PointBuffer);
					this.m_PointBuffer = null;
				}
				if (this.m_LineBuffer != null)
				{
					this.m_PrimLineBuffer.RemoveBuffer(this.m_LineBuffer);
					this.m_LineBuffer = null;
				}
				if (this.m_PolylineBuffer != null)
				{
					this.m_PrimPolylineBuffer.RemoveBuffer(this.m_PolylineBuffer);
					this.m_PolylineBuffer = null;
				}
				if (this.m_ConfettiBuffer != null)
				{
					this.m_PrimConfettiBuffer.RemoveBuffer(this.m_ConfettiBuffer);
					this.m_ConfettiBuffer = null;
				}
				if (this.m_RibbonBuffer != null)
				{
					this.m_PrimRibbonBuffer.RemoveBuffer(this.m_RibbonBuffer);
					this.m_RibbonBuffer = null;
				}
				if (this.m_PrimTailBuffer != null)
				{
					this.m_PrimTailBuffer.RemoveBuffer(this.m_TailBuffer);
					this.m_TailBuffer = null;
				}
			}
		}

		// Token: 0x06005064 RID: 20580 RVA: 0x001607F4 File Offset: 0x0015EBF4
		private bool Setup()
		{
			if (this.rule == null)
			{
				return false;
			}
			if (this.rule.m_particleNum <= 0)
			{
				return false;
			}
			this.DestroyObject(false);
			switch (this.rule.m_particleType)
			{
			case eParticleType.eParticleType_Billboard:
				if (this.m_PrimBillboardBuffer == null)
				{
					this.m_PrimBillboardBuffer = this.m_RenderObject.AddComponent<PrimBillboardBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimBillboardBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimBillboard);
					}
				}
				if (this.m_BillboardBuffer == null)
				{
					this.m_BillboardBuffer = this.m_PrimBillboardBuffer.AddBuffer(this.rule.m_particleNum, true);
				}
				this.m_BillboardBuffer.rotateType = this.rule.m_particleTypeParam.m_billboard.m_RotateType;
				break;
			case eParticleType.eParticleType_Point:
				if (this.m_PrimPointBuffer == null)
				{
					this.m_PrimPointBuffer = this.m_RenderObject.AddComponent<PrimPointBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimPointBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimPoint);
					}
				}
				if (this.m_PointBuffer == null)
				{
					this.m_PointBuffer = this.m_PrimPointBuffer.AddBuffer(this.rule.m_particleNum);
				}
				break;
			case eParticleType.eParticleType_Line:
				if (this.m_PrimLineBuffer == null)
				{
					this.m_PrimLineBuffer = this.m_RenderObject.AddComponent<PrimLineBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimLineBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimLine);
					}
				}
				if (this.m_LineBuffer == null)
				{
					this.m_LineBuffer = this.m_PrimLineBuffer.AddBuffer(this.rule.m_particleNum, this.rule.m_particleTypeParam.m_line.m_jointNum + 2);
				}
				break;
			case eParticleType.eParticleType_PolyLine:
				if (this.m_PrimPolylineBuffer == null)
				{
					this.m_PrimPolylineBuffer = this.m_RenderObject.AddComponent<PrimPolyLineBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimPolylineBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimPolyline);
					}
				}
				if (this.m_PolylineBuffer == null)
				{
					this.m_PolylineBuffer = this.m_PrimPolylineBuffer.AddBuffer(this.rule.m_particleNum, this.rule.m_particleTypeParam.m_polyLine.m_jointNum + 2);
				}
				break;
			case eParticleType.eParticleType_Confetti:
				if (this.m_PrimConfettiBuffer == null)
				{
					this.m_PrimConfettiBuffer = this.m_RenderObject.AddComponent<PrimConfettiBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimConfettiBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimConfetti);
					}
				}
				if (this.m_ConfettiBuffer == null)
				{
					this.m_ConfettiBuffer = this.m_PrimConfettiBuffer.AddBuffer(this.rule.m_particleNum);
				}
				break;
			case eParticleType.eParticleType_Ribbon:
				if (this.m_PrimRibbonBuffer == null)
				{
					this.m_PrimRibbonBuffer = this.m_RenderObject.AddComponent<PrimPolyLineBuffer>();
					if (this.m_RenderSettingsFixed != null)
					{
						this.m_PrimRibbonBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimRibbon);
					}
				}
				if (this.m_RibbonBuffer == null)
				{
					this.m_RibbonBuffer = this.m_PrimRibbonBuffer.AddBuffer(this.rule.m_particleNum, this.rule.m_particleTypeParam.m_ribbon.m_jointNum + 2);
				}
				break;
			}
			if (this.rule.m_UsingTailFlg && this.m_TailRenderObject == null && this.m_TailRenderer == null)
			{
				this.m_TailRenderObject = new GameObject("m_TailObject");
				Transform transform = this.m_TailRenderObject.transform;
				transform.position = Vector3.zero;
				transform.eulerAngles = Vector3.zero;
				transform.localScale = Vector3.one;
				transform.parent = this.m_RenderObject.transform;
				this.m_TailRenderer = this.m_TailRenderObject.AddComponent<EffectRenderer>();
				if (this.m_TailRenderer != null)
				{
					this.m_TailRenderer.SetSortingOrder(this.m_SortingOrder);
				}
				this.m_PrimTailBuffer = this.m_TailRenderObject.AddComponent<PrimPolyLineBuffer>();
				if (this.m_RenderSettingsFixed != null)
				{
					this.m_PrimTailBuffer.SetExternalMeshBuffer(this.m_RenderSettingsFixed.m_MeshBufferPrimTail);
				}
				this.m_TailBuffer = this.m_PrimTailBuffer.AddBuffer(this.rule.m_particleNum, this.rule.m_TailComponent.m_tailJointNum + 2);
			}
			this.m_ActiveUnitArray.Clear();
			this.m_InActiveUnitArray.Clear();
			for (int i = 0; i < this.rule.m_particleNum; i++)
			{
				ParticleUnit particleUnit = new ParticleUnit();
				particleUnit.m_Index = i;
				particleUnit.m_activeFlg = false;
				switch (this.rule.m_particleType)
				{
				case eParticleType.eParticleType_Line:
					if (this.rule.m_particleTypeParam.m_line.m_HistoryPointNum == 0)
					{
						particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum = (short)(this.rule.m_particleTypeParam.m_line.m_jointNum + 2);
					}
					else
					{
						particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum = (short)this.rule.m_particleTypeParam.m_line.m_HistoryPointNum;
					}
					particleUnit.m_particleTypeParam.m_line.m_pPos = new Vector3[(int)particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum];
					particleUnit.m_particleTypeParam.m_line.m_jointNum = (short)this.rule.m_particleTypeParam.m_line.m_jointNum;
					break;
				case eParticleType.eParticleType_PolyLine:
					if (this.rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum == 0)
					{
						particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum = (short)(this.rule.m_particleTypeParam.m_polyLine.m_jointNum + 2);
					}
					else
					{
						particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum = (short)this.rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum;
					}
					particleUnit.m_particleTypeParam.m_polyLine.m_pPos = new Vector3[(int)particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum];
					particleUnit.m_particleTypeParam.m_polyLine.m_jointNum = (short)this.rule.m_particleTypeParam.m_polyLine.m_jointNum;
					break;
				case eParticleType.eParticleType_Ribbon:
					if (this.rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum == 0)
					{
						particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum = (short)(this.rule.m_particleTypeParam.m_ribbon.m_jointNum + 2);
					}
					else
					{
						particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum = (short)this.rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum;
					}
					particleUnit.m_particleTypeParam.m_ribbon.m_pPos = new Vector3[(int)particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum];
					particleUnit.m_particleTypeParam.m_ribbon.m_pVelocity = new Vector3[(int)particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum];
					particleUnit.m_particleTypeParam.m_ribbon.m_jointNum = (short)this.rule.m_particleTypeParam.m_ribbon.m_jointNum;
					break;
				}
				if (this.rule.m_UsingTailFlg)
				{
					particleUnit.m_TailComponent.m_pTailPos = new Vector3[this.rule.m_TailComponent.m_tailJointNum + 2];
				}
				if (this.rule.m_UsingPathMoveFlg)
				{
					particleUnit.m_PathMoveComponent.m_offsetPos = new Vector3[4];
				}
				this.m_InActiveUnitArray.Add(particleUnit);
			}
			this.m_AttachFuncList = null;
			if (this.rule.m_UsingAccelerationFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_Move_Acceleration));
			}
			if (this.rule.m_UsingRotationFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_Move_Rotation));
			}
			if (this.rule.m_UsingTailFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_Move_Tail));
			}
			if (this.rule.m_UsingPathMoveFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_Move_PathMove));
			}
			if (this.rule.m_UsingColorCurveFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_ColorAnim_ColorCurve));
			}
			if (this.rule.m_UsingBlinkFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_ColorAnim_Blink));
			}
			if (this.rule.m_UsingUVAnimationFlg)
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_UVAnim_UVAnimation));
			}
			else
			{
				this.m_AttachFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_AttachFuncList, new MeigeParticleEmitter.CalcFuncList(this.AttachFunc_UVAnim));
			}
			this.m_CalcFuncList = null;
			if (this.rule.m_UsingAccelerationFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_Move_Acceleration));
			}
			if (this.rule.m_UsingRotationFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_Move_Rotation));
			}
			if (this.rule.m_UsingPathMoveFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_Move_PathMove));
			}
			this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_Move));
			if (this.rule.m_UsingColorCurveFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_ColorAnim_ColorCurve));
			}
			else
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_ColorAnim_NotColorCurve));
			}
			if (this.rule.m_UsingBlinkFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_ColorAnim_Blink));
			}
			if (this.rule.m_UsingUVAnimationFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_UVAnim_UVAnimation));
			}
			this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFunc_LifeSpan));
			this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFuncMerge));
			if (this.rule.m_UsingRotationFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFuncMerge_Rotation));
			}
			else
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFuncMerge_NotRotation));
			}
			if (this.rule.m_UsingTailFlg)
			{
				this.m_CalcFuncList = (MeigeParticleEmitter.CalcFuncList)Delegate.Combine(this.m_CalcFuncList, new MeigeParticleEmitter.CalcFuncList(this.CalcFuncMerge_Tail));
			}
			this.m_History.m_particleType = this.rule.m_particleType;
			this.m_History.m_particleNum = this.rule.m_particleNum;
			this.m_History.m_UsingAccelerationFlg = this.rule.m_UsingAccelerationFlg;
			this.m_History.m_UsingRotationFlg = this.rule.m_UsingRotationFlg;
			this.m_History.m_UsingUVAnimationFlg = this.rule.m_UsingUVAnimationFlg;
			this.m_History.m_UsingColorCurveFlg = this.rule.m_UsingColorCurveFlg;
			this.m_History.m_UsingTailFlg = this.rule.m_UsingTailFlg;
			this.m_History.m_UsingBlinkFlg = this.rule.m_UsingBlinkFlg;
			this.m_History.m_UsingPathMoveFlg = this.rule.m_UsingPathMoveFlg;
			this.ChangeParent();
			this.UpdateDirtyMesh(false);
			this.m_isLocalTransHistory = this.rule.m_isLocalTrans;
			this.isAlive = this.isAlive;
			return true;
		}

		// Token: 0x06005065 RID: 20581 RVA: 0x00161474 File Offset: 0x0015F874
		protected override void Release()
		{
			this.DestroyObject(true);
			if (this.m_RenderObjectIsSelf)
			{
				if (this.m_TailRenderObject != null)
				{
					UnityEngine.Object.Destroy(this.m_TailRenderObject);
				}
				if (this.m_RenderObject != null)
				{
					UnityEngine.Object.Destroy(this.m_RenderObject);
				}
			}
			base.Release();
		}

		// Token: 0x06005066 RID: 20582 RVA: 0x001614D4 File Offset: 0x0015F8D4
		private void Start()
		{
			this.m_RenderSettingsFixed = this.m_RenderSettings;
			if (this.m_RenderSettingsFixed != null)
			{
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimBillboard != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimPoint != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimLine != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimPolyline != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimConfetti != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimRibbon != null)
				{
				}
				if (this.m_RenderSettingsFixed.m_MeshBufferPrimTail != null)
				{
				}
				this.m_EffectRenderer = this.m_RenderSettingsFixed.m_EffectRenderer;
			}
			if (this.m_EffectRenderer == null)
			{
				this.m_RenderObject = new GameObject("ParticleEmitterRenderObject");
				this.m_RenderObject.transform.localPosition = Vector3.zero;
				this.m_RenderObject.transform.localEulerAngles = this.m_offsetRot;
				this.m_RenderObject.transform.parent = base.transform;
				this.m_EffectRenderer = this.m_RenderObject.AddComponent<EffectRenderer>();
				this.m_RenderObjectIsSelf = true;
			}
			else
			{
				this.m_RenderObject = this.m_EffectRenderer.gameObject;
				this.m_RenderObjectIsSelf = false;
			}
			this.m_RenderObject.layer = this.m_Layer;
			if (this.m_EffectRenderer != null)
			{
				this.m_EffectRenderer.SetSortingOrder(this.m_SortingOrder);
			}
			this.m_thisTransform = base.transform;
			this.isAlive = false;
			this.m_isLocalTransHistory = this.rule.m_isLocalTrans;
			this.FlushTime();
		}

		// Token: 0x06005067 RID: 20583 RVA: 0x00161698 File Offset: 0x0015FA98
		public bool KillOnlyParticles()
		{
			if (!this.isAlive)
			{
				return false;
			}
			int count = this.m_ActiveUnitArray.Count;
			for (int i = 0; i < count; i++)
			{
				ParticleUnit particleUnit = this.m_ActiveUnitArray[0];
				particleUnit.m_activeFlg = false;
				this.ClearUnit(particleUnit);
				this.m_ActiveUnitArray.Remove(particleUnit);
				this.m_InActiveUnitArray.Add(particleUnit);
			}
			this.UpdateDirtyMesh(false);
			this.isAlive = false;
			return true;
		}

		// Token: 0x06005068 RID: 20584 RVA: 0x00161713 File Offset: 0x0015FB13
		public override bool Kill()
		{
			this.KillOnlyParticles();
			this.m_isActive = false;
			return true;
		}

		// Token: 0x06005069 RID: 20585 RVA: 0x00161724 File Offset: 0x0015FB24
		public override bool Shot()
		{
			this.m_isMatrixDirty = true;
			return this.SingleEmit();
		}

		// Token: 0x0600506A RID: 20586 RVA: 0x00161733 File Offset: 0x0015FB33
		private bool FirstSetting()
		{
			if (this.m_isInitialized)
			{
				return false;
			}
			this.m_isInitialized = true;
			this.Setup();
			return true;
		}

		// Token: 0x0600506B RID: 20587 RVA: 0x00161754 File Offset: 0x0015FB54
		private void Update()
		{
			if (!this.m_isInitialized && !this.FirstSetting())
			{
				return;
			}
			this.FlushTime();
			if (this.rule != null)
			{
				if (!this.m_History.IsEqual(this.rule))
				{
					this.Setup();
				}
				if (this.m_isLocalTransHistory != this.rule.m_isLocalTrans)
				{
					this.ChangeParent();
					this.m_isLocalTransHistory = this.rule.m_isLocalTrans;
				}
			}
			this.m_isMatrixDirty = true;
			if (this.m_isActive)
			{
				this.SingleEmit();
			}
			if (!this.isAlive)
			{
				return;
			}
			if (this.m_BillboardBuffer != null && this.rule != null && this.m_BillboardBuffer.rotateType != this.rule.m_particleTypeParam.m_billboard.m_RotateType)
			{
				this.m_BillboardBuffer.rotateType = this.rule.m_particleTypeParam.m_billboard.m_RotateType;
			}
			if (this.m_UpdateHook_Before != null)
			{
				this.m_UpdateHook_Before();
			}
			if (this.m_Material != null && this.m_EffectRenderer != null && this.m_RenderObjectIsSelf)
			{
				if (this.m_Material != this.m_EffectRenderer.material)
				{
					this.m_EffectRenderer.material = this.m_Material;
				}
				if (this.m_TailRenderer != null && this.m_Material != this.m_TailRenderer.material)
				{
					this.m_TailRenderer.material = this.m_Material;
				}
			}
			if (this.m_isMatrixDirty)
			{
				this.m_OldMatrix = this.m_Matrix;
				if (this.m_LocatorTransforms[0] != null)
				{
					this.m_Matrix = this.m_LocatorTransforms[0].localToWorldMatrix;
				}
				else
				{
					this.m_Matrix = this.m_thisTransform.localToWorldMatrix;
				}
				this.m_isMatrixDirty = false;
			}
			for (int i = this.m_ActiveUnitArray.Count - 1; i >= 0; i--)
			{
				ParticleUnit particleUnit = this.m_ActiveUnitArray[i];
				if (!this.UpdateUnit(particleUnit))
				{
					this.m_ActiveUnitArray.Remove(particleUnit);
					this.m_InActiveUnitArray.Add(particleUnit);
				}
			}
			if (this.m_UpdateHook_After != null)
			{
				this.m_UpdateHook_After();
			}
			if (this.m_ActiveUnitArray.Count == 0)
			{
				this.Kill();
				return;
			}
			this.UpdateDirtyMesh(true);
		}

		// Token: 0x0600506C RID: 20588 RVA: 0x001619E0 File Offset: 0x0015FDE0
		public bool SingleEmit()
		{
			if (!this.m_isInitialized)
			{
				return false;
			}
			int num = this.CalcEmitNumberBySingleEmit();
			if (num <= 0)
			{
				return false;
			}
			bool flag = false;
			if (this.m_UpdateHook_Shot != null && this.m_InActiveUnitArray.Count > 0)
			{
				this.m_UpdateHook_Shot();
			}
			int count = this.m_InActiveUnitArray.Count;
			for (int i = 0; i < count; i++)
			{
				ParticleUnit particleUnit = this.m_InActiveUnitArray[0];
				num--;
				if (num < 0)
				{
					break;
				}
				if (this.ActivateParticleUnit(particleUnit))
				{
					this.m_InActiveUnitArray.Remove(particleUnit);
					this.m_ActiveUnitArray.Add(particleUnit);
					flag = true;
				}
			}
			if (flag)
			{
				this.isAlive = true;
			}
			return flag;
		}

		// Token: 0x0600506D RID: 20589 RVA: 0x00161AA8 File Offset: 0x0015FEA8
		private int CalcEmitNumberBySingleEmit()
		{
			float num = (this.rule.m_incidentNumberPerSec + Helper.GetFRandom(0f, 1f) * this.rule.m_incidentRandomLevel) * this.time;
			if (num <= 0f)
			{
				return 0;
			}
			int num2 = (int)num;
			num -= (float)num2;
			float frandom = Helper.GetFRandom(0f, 1f);
			if (frandom < num)
			{
				num2++;
			}
			return num2;
		}

		// Token: 0x0600506E RID: 20590 RVA: 0x00161B18 File Offset: 0x0015FF18
		public static void CalcEmitionTransform(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			switch (type)
			{
			case eParticleEmitionType.eParticleEmitionType_Point:
				MeigeParticleEmitter.CalcEmitionTransform_Point(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_Box:
				MeigeParticleEmitter.CalcEmitionTransform_Box(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_PlaneQuad:
				MeigeParticleEmitter.CalcEmitionTransform_PlaneQuad(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_PlaneCircle:
				MeigeParticleEmitter.CalcEmitionTransform_PlaneCircle(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_Sphere:
				MeigeParticleEmitter.CalcEmitionTransform_Sphere(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_Torus:
				MeigeParticleEmitter.CalcEmitionTransform_Torus(type, param, ref pos, ref normal);
				break;
			case eParticleEmitionType.eParticleEmitionType_Cylinder:
				MeigeParticleEmitter.CalcEmitionTransform_Cylinder(type, param, ref pos, ref normal);
				break;
			}
		}

		// Token: 0x0600506F RID: 20591 RVA: 0x00161BB4 File Offset: 0x0015FFB4
		private static void CalcEmitionTransform_Point(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			Quaternion rotation = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), -90f, param.m_point.m_angleRange.Random().value);
			pos = Vector3.zero;
			normal = Vector3.right;
			normal = rotation * normal;
		}

		// Token: 0x06005070 RID: 20592 RVA: 0x00161C14 File Offset: 0x00160014
		private static void CalcEmitionTransform_Box(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			int num = Helper.GetIRandom() % 6;
			Vector3 a;
			a.x = param.m_box.m_widthRange.ValueMax.value * 0.5f;
			a.y = param.m_box.m_heightRange.ValueMax.value * 0.5f;
			a.z = param.m_box.m_depthRange.ValueMax.value * 0.5f;
			Vector3 vector = a * -1f;
			Vector3 vector2;
			vector2.x = Helper.GetFRandom(vector.x, a.x);
			vector2.y = Helper.GetFRandom(vector.y, a.y);
			vector2.z = Helper.GetFRandom(vector.z, a.z);
			switch (num)
			{
			default:
				pos.x = vector2.x;
				pos.y = vector2.y;
				pos.z = param.m_box.m_depthRange.Random().value * 0.5f;
				normal = Vector3.forward;
				break;
			case 1:
				pos.x = vector2.x;
				pos.y = vector2.y;
				pos.z = param.m_box.m_depthRange.Random().value * -0.5f;
				normal = Vector3.back;
				break;
			case 2:
				pos.x = vector2.x;
				pos.y = param.m_box.m_heightRange.Random().value * 0.5f;
				pos.z = vector2.z;
				normal = Vector3.up;
				break;
			case 3:
				pos.x = vector2.x;
				pos.y = param.m_box.m_heightRange.Random().value * -0.5f;
				pos.z = vector2.z;
				normal = Vector3.down;
				break;
			case 4:
				pos.x = param.m_box.m_widthRange.Random().value * 0.5f;
				pos.y = vector2.y;
				pos.z = vector2.z;
				normal = Vector3.right;
				break;
			case 5:
				pos.x = param.m_box.m_widthRange.Random().value * -0.5f;
				pos.y = vector2.y;
				pos.z = vector2.z;
				normal = Vector3.left;
				break;
			}
		}

		// Token: 0x06005071 RID: 20593 RVA: 0x00161EF4 File Offset: 0x001602F4
		private static void CalcEmitionTransform_PlaneQuad(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			Vector2 a;
			a.x = param.m_planeQuad.m_widthRange.ValueMax.value * 0.5f;
			a.y = param.m_planeQuad.m_heightRange.ValueMax.value * 0.5f;
			Vector2 vector = a * -1f;
			Vector2 vector2;
			vector2.x = Helper.GetFRandom(vector.x, a.x);
			vector2.y = Helper.GetFRandom(vector.y, a.y);
			if (Helper.GetIRandom() % 2 != 0)
			{
				pos.x = vector2.x;
				pos.y = param.m_planeQuad.m_heightRange.Random().value * 0.5f * (float)(Helper.GetIRandom() % 2 * 2 - 1);
				pos.z = 0f;
			}
			else
			{
				pos.x = param.m_planeQuad.m_widthRange.Random().value * 0.5f * (float)(Helper.GetIRandom() % 2 * 2 - 1);
				pos.y = vector2.y;
				pos.z = 0f;
			}
			normal = Vector3.forward;
		}

		// Token: 0x06005072 RID: 20594 RVA: 0x0016203C File Offset: 0x0016043C
		private static void CalcEmitionTransform_PlaneCircle(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			Quaternion rotation = Quaternion.Euler(0f, 0f, MeigeParticleEmitter.GetRandomDegree());
			pos.x = 0f;
			pos.y = param.m_planeCircle.m_radiusRange.Random().value;
			pos.z = 0f;
			pos = rotation * pos;
			normal = Vector3.forward;
		}

		// Token: 0x06005073 RID: 20595 RVA: 0x001620B0 File Offset: 0x001604B0
		private static void CalcEmitionTransform_Sphere(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			Quaternion rotation = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), -90f, param.m_sphere.m_angleRange.Random().value);
			float num = Helper.GetFRandom(0f, 1f);
			num = 1f - num * num;
			pos.x = param.m_sphere.m_radiusRange.Lerp(num).value;
			pos.y = 0f;
			pos.z = 0f;
			pos = rotation * pos;
			normal = rotation * Vector3.right;
		}

		// Token: 0x06005074 RID: 20596 RVA: 0x00162158 File Offset: 0x00160558
		private static void CalcEmitionTransform_Torus(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			float bigRadiusRange = param.m_torus.m_bigRadiusRange;
			float value = param.m_torus.m_smallRadiusRange.Random().value;
			float f = param.m_torus.m_angleRange.Random().value * 2f * 0.017453292f;
			float randomRadian = MeigeParticleEmitter.GetRandomRadian();
			float num = Mathf.Sin(f);
			float num2 = Mathf.Sin(randomRadian);
			float num3 = Mathf.Cos(f);
			float num4 = Mathf.Cos(randomRadian);
			pos.x = bigRadiusRange * num3 + value * num4 * num3;
			pos.y = bigRadiusRange * num + value * num4 * num;
			pos.z = value * num2;
			normal.x = bigRadiusRange * num3;
			normal.y = bigRadiusRange * num;
			normal.z = 0f;
			normal = pos - normal;
			if (Vector3.SqrMagnitude(normal) <= 0f)
			{
				normal = pos * -1f;
			}
			else
			{
				normal.Normalize();
			}
		}

		// Token: 0x06005075 RID: 20597 RVA: 0x00162274 File Offset: 0x00160674
		private static void CalcEmitionTransform_Cylinder(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal)
		{
			float num = Helper.GetFRandom(0f, 1f);
			num = 1f - num * num;
			normal = Quaternion.Euler(0f, MeigeParticleEmitter.GetRandomDegree(), 0f) * Vector3.forward;
			pos = normal * param.m_cylinder.m_RadiusRange.Lerp(num).value;
			pos.y = param.m_cylinder.m_HeightRange.Random().value;
		}

		// Token: 0x06005076 RID: 20598 RVA: 0x00162308 File Offset: 0x00160708
		private void MakePosAndNormal(ref Vector3 pos, ref Vector3 normal)
		{
			MeigeParticleEmitter.CalcEmitionTransform(this.rule.m_emitionType, this.rule.m_emitionParam, ref pos, ref normal);
			if (this.rule.m_isRandomEmitDir)
			{
				normal = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), MeigeParticleEmitter.GetRandomDegree(), 0f) * Vector3.forward;
			}
			if (this.m_isMatrixDirty)
			{
				this.m_OldMatrix = this.m_Matrix;
				if (this.m_LocatorTransforms[0] != null)
				{
					this.m_Matrix = this.m_LocatorTransforms[0].localToWorldMatrix;
				}
				else
				{
					this.m_Matrix = this.m_thisTransform.localToWorldMatrix;
				}
				this.m_isMatrixDirty = false;
			}
			if (!this.rule.m_isLocalTrans)
			{
				if (this.rule.m_particleType != eParticleType.eParticleType_Ribbon)
				{
					if (this.isAlive)
					{
						Vector3 vector = pos;
						vector = this.m_OldMatrix.MultiplyPoint(vector);
						pos = this.m_Matrix.MultiplyPoint(pos);
						pos = Vector3.Lerp(vector, pos, Helper.GetFRandom(0f, 1f));
					}
					else
					{
						pos = this.m_Matrix.MultiplyPoint(pos);
					}
				}
				else if (this.isAlive)
				{
					Vector3 vector2 = pos;
					vector2 = this.m_OldMatrix.MultiplyVector(vector2);
					pos = this.m_Matrix.MultiplyVector(pos);
					pos = Vector3.Lerp(vector2, pos, Helper.GetFRandom(0f, 1f));
				}
				else
				{
					pos = this.m_Matrix.MultiplyVector(pos);
				}
				normal = this.m_Matrix.MultiplyVector(normal);
			}
		}

		// Token: 0x06005077 RID: 20599 RVA: 0x001624E4 File Offset: 0x001608E4
		private bool ActivateParticleUnit(ParticleUnit unit)
		{
			Vector3 vector = Vector4.zero;
			Vector3 vector2 = Vector4.zero;
			this.MakePosAndNormal(ref vector, ref vector2);
			unit.m_particleType = this.rule.m_particleType;
			unit.m_birthPos = vector;
			unit.m_pos = vector;
			unit.m_offset = Vector4.zero;
			unit.m_velocity = vector2;
			unit.m_scale = 1f;
			unit.m_lifeSpanType = this.rule.m_lifeSpanType;
			unit.m_lifeSpanRate = 0f;
			switch (this.rule.m_lifeSpanType)
			{
			case eParticleLifeSpanType.eParticleLifeSpanType_Time:
				unit.m_lifeSpanParam[0] = this.rule.m_LifeSpanParam.m_Time.m_lifeSpanSecRange.Random().value;
				break;
			case eParticleLifeSpanType.eParticleLifeSpanType_Distance:
				unit.m_lifeSpanParam[0] = this.rule.m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange.Random().value;
				break;
			case eParticleLifeSpanType.eParticleLifeSpanType_HeightRange:
				unit.m_lifeSpanParam[0] = this.rule.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange.ValueMin.value;
				unit.m_lifeSpanParam[1] = this.rule.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange.ValueMax.value;
				break;
			}
			switch (this.rule.m_lifeSpanAlpha)
			{
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_None:
				unit.m_lifeSpanFadeAlphaRate = 1f;
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeInOut:
				unit.m_lifeSpanFadeAlphaRate = 0f;
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeIn:
				unit.m_lifeSpanFadeAlphaRate = 0f;
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeOut:
				unit.m_lifeSpanFadeAlphaRate = 1f;
				break;
			}
			this.AttachFunc(unit);
			unit.m_velocity = vector2 * this.rule.m_speedRange.Random().value;
			unit.m_isLocal = this.rule.m_isLocalTrans;
			unit.m_initializedFlg = false;
			unit.m_aliveCount = 0;
			unit.m_activeFlg = true;
			return true;
		}

		// Token: 0x06005078 RID: 20600 RVA: 0x00162710 File Offset: 0x00160B10
		private bool UpdateUnit(ParticleUnit unit)
		{
			this.m_CalcFuncList(unit);
			if (!unit.m_initializedFlg)
			{
				unit.m_initializedFlg = true;
			}
			unit.m_aliveCount++;
			return unit.m_activeFlg;
		}

		// Token: 0x06005079 RID: 20601 RVA: 0x00162744 File Offset: 0x00160B44
		private void AttachFunc(ParticleUnit unit)
		{
			unit.m_lifeScaleMax = this.rule.m_lifeScaleRange.Random().value;
			unit.m_gravityForce = this.rule.m_gravityDir * this.rule.m_gravityForceRange.Random().value * this.m_thisTransform.lossyScale.x;
			unit.m_gravityForce.x = -unit.m_gravityForce.x;
			switch (unit.m_particleType)
			{
			case eParticleType.eParticleType_Billboard:
			{
				float rate = UnityEngine.Random.Range(0f, 1f);
				unit.m_particleTypeParam.m_billboard.m_width = this.rule.m_particleTypeParam.m_billboard.m_widthRange.Lerp(rate).value * this.m_thisTransform.lossyScale.x;
				unit.m_particleTypeParam.m_billboard.m_height = this.rule.m_particleTypeParam.m_billboard.m_heightRange.Lerp(rate).value * this.m_thisTransform.lossyScale.x;
				break;
			}
			case eParticleType.eParticleType_Point:
				unit.m_particleTypeParam.m_point.m_size = this.rule.m_particleTypeParam.m_point.m_sizeRange.Random().value * this.m_thisTransform.lossyScale.x;
				break;
			case eParticleType.eParticleType_Line:
				unit.m_particleTypeParam.m_line.m_width = this.rule.m_particleTypeParam.m_line.m_width * this.m_thisTransform.lossyScale.x;
				for (int i = 0; i < this.rule.m_particleTypeParam.m_line.m_HistoryPointNum; i++)
				{
					unit.m_particleTypeParam.m_line.m_pPos[i] = unit.m_pos;
				}
				break;
			case eParticleType.eParticleType_PolyLine:
			{
				float rate2 = UnityEngine.Random.Range(0f, 1f);
				unit.m_particleTypeParam.m_polyLine.m_topWidth = this.rule.m_particleTypeParam.m_polyLine.m_topWidthRange.Lerp(rate2).value * this.m_thisTransform.lossyScale.x;
				unit.m_particleTypeParam.m_polyLine.m_endWidth = this.rule.m_particleTypeParam.m_polyLine.m_endWidthRange.Lerp(rate2).value * this.m_thisTransform.lossyScale.x;
				for (int j = 0; j < this.rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum; j++)
				{
					unit.m_particleTypeParam.m_polyLine.m_pPos[j] = unit.m_pos;
				}
				break;
			}
			case eParticleType.eParticleType_Confetti:
			{
				float rate3 = UnityEngine.Random.Range(0f, 1f);
				unit.m_particleTypeParam.m_confetti.m_width = this.rule.m_particleTypeParam.m_confetti.m_widthRange.Lerp(rate3).value * this.m_thisTransform.lossyScale.x;
				unit.m_particleTypeParam.m_confetti.m_height = this.rule.m_particleTypeParam.m_confetti.m_heightRange.Lerp(rate3).value * this.m_thisTransform.lossyScale.x;
				break;
			}
			case eParticleType.eParticleType_Ribbon:
			{
				float rate4 = UnityEngine.Random.Range(0f, 1f);
				unit.m_particleTypeParam.m_ribbon.m_topWidth = this.rule.m_particleTypeParam.m_ribbon.m_topWidthRange.Lerp(rate4).value * this.m_thisTransform.lossyScale.x;
				unit.m_particleTypeParam.m_ribbon.m_endWidth = this.rule.m_particleTypeParam.m_ribbon.m_endWidthRange.Lerp(rate4).value * this.m_thisTransform.lossyScale.x;
				for (int k = 0; k < this.rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum; k++)
				{
					unit.m_particleTypeParam.m_ribbon.m_pPos[k] = unit.m_pos;
					unit.m_particleTypeParam.m_ribbon.m_pVelocity[k] = Vector3.zero;
				}
				break;
			}
			}
			this.m_AttachFuncList(unit);
		}

		// Token: 0x0600507A RID: 20602 RVA: 0x00162C3C File Offset: 0x0016103C
		private void AttachFunc_Move_Acceleration(ParticleUnit unit)
		{
			unit.m_AccelerationComponent.m_acceleration = this.rule.m_AccelerationComponent.m_accelerationRange.Random().value * this.m_thisTransform.lossyScale.x;
			unit.m_AccelerationComponent.m_dragForce = this.rule.m_AccelerationComponent.m_dragForceRange.Random().value;
		}

		// Token: 0x0600507B RID: 20603 RVA: 0x00162CB0 File Offset: 0x001610B0
		private void AttachFunc_Move_Rotation(ParticleUnit unit)
		{
			unit.m_RotationComponent.m_rot.Set(this.rule.m_RotationComponent.m_rotRange.Random().value, this.rule.m_RotationComponent.m_rotRange.Random().value, this.rule.m_RotationComponent.m_rotRange.Random().value);
			unit.m_RotationComponent.m_rotAnchorOffset.Set(Helper.GetFRandom(0f, 1f) * this.rule.m_RotationComponent.m_rotAnchorOffsetRange.Random().value, Helper.GetFRandom(0f, 1f) * this.rule.m_RotationComponent.m_rotAnchorOffsetRange.Random().value, Helper.GetFRandom(0f, 1f) * this.rule.m_RotationComponent.m_rotAnchorOffsetRange.Random().value);
			unit.m_RotationComponent.m_rotAnchorOffset = unit.m_RotationComponent.m_rotAnchorOffset * this.m_thisTransform.lossyScale.x;
			unit.m_RotationComponent.m_rotVelocity.Set(this.rule.m_RotationComponent.m_rotSpeedRange.Random().value, this.rule.m_RotationComponent.m_rotSpeedRange.Random().value, this.rule.m_RotationComponent.m_rotSpeedRange.Random().value);
			unit.m_RotationComponent.m_rotAcceleration.Set(this.rule.m_RotationComponent.m_rotAccelerationRange.Random().value, this.rule.m_RotationComponent.m_rotAccelerationRange.Random().value, this.rule.m_RotationComponent.m_rotAccelerationRange.Random().value);
			unit.m_RotationComponent.m_rotDragForce = this.rule.m_RotationComponent.m_rotDragForceRange.Random().value;
		}

		// Token: 0x0600507C RID: 20604 RVA: 0x00162EEC File Offset: 0x001612EC
		private void AttachFunc_Move_Tail(ParticleUnit unit)
		{
			for (int i = 0; i < this.rule.m_TailComponent.m_tailJointNum + 2; i++)
			{
				unit.m_TailComponent.m_pTailPos[i] = unit.m_pos;
			}
			unit.m_TailComponent.m_tailUVRect = this.rule.m_TailComponent.m_tailUVRect;
		}

		// Token: 0x0600507D RID: 20605 RVA: 0x00162F54 File Offset: 0x00161354
		private void AttachFunc_Move_PathMove(ParticleUnit unit)
		{
			Vector4 v = Vector4.zero;
			Quaternion q = Quaternion.identity;
			Matrix4x4 identity = Matrix4x4.identity;
			v.Set(0f, 0f, this.rule.m_PathMoveComponent.m_FocusRadiusRange[0].Random().value, 1f);
			q = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), MeigeParticleEmitter.GetRandomDegree(), 0f);
			identity.SetTRS(Vector3.zero, q, Vector3.one);
			v = identity.MultiplyPoint(v);
			unit.m_PathMoveComponent.m_offsetPos[0] = v;
			v.x *= this.m_thisTransform.lossyScale.x;
			v.y *= this.m_thisTransform.lossyScale.y;
			v.z *= this.m_thisTransform.lossyScale.z;
			v.Set(0f, 0f, this.rule.m_PathMoveComponent.m_FocusRadiusRange[1].Random().value, 1f);
			q = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), MeigeParticleEmitter.GetRandomDegree(), 0f);
			identity.SetTRS(Vector3.zero, q, Vector3.one);
			v = identity.MultiplyPoint(v);
			v.x *= this.m_thisTransform.lossyScale.x;
			v.y *= this.m_thisTransform.lossyScale.y;
			v.z *= this.m_thisTransform.lossyScale.z;
			unit.m_PathMoveComponent.m_offsetPos[1] = v;
			v.Set(0f, 0f, this.rule.m_PathMoveComponent.m_FocusRadiusRange[2].Random().value, 1f);
			q = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), MeigeParticleEmitter.GetRandomDegree(), 0f);
			identity.SetTRS(Vector3.zero, q, Vector3.one);
			v = identity.MultiplyPoint(v);
			v.x *= this.m_thisTransform.lossyScale.x;
			v.y *= this.m_thisTransform.lossyScale.y;
			v.z *= this.m_thisTransform.lossyScale.z;
			unit.m_PathMoveComponent.m_offsetPos[2] = v;
			v.Set(0f, 0f, this.rule.m_PathMoveComponent.m_FocusRadiusRange[3].Random().value, 1f);
			q = Quaternion.Euler(MeigeParticleEmitter.GetRandomDegree(), MeigeParticleEmitter.GetRandomDegree(), 0f);
			identity.SetTRS(Vector3.zero, q, Vector3.one);
			v = identity.MultiplyPoint(v);
			v.x *= this.m_thisTransform.lossyScale.x;
			v.y *= this.m_thisTransform.lossyScale.y;
			v.z *= this.m_thisTransform.lossyScale.z;
			unit.m_PathMoveComponent.m_offsetPos[3] = v;
		}

		// Token: 0x0600507E RID: 20606 RVA: 0x00163348 File Offset: 0x00161748
		private void AttachFunc_ColorAnim_ColorCurve(ParticleUnit unit)
		{
			if (this.rule.m_ColorCurveComponent.m_pColorCurveArray == null || this.rule.m_ColorCurveComponent.m_pColorCurveArray.Length == 0)
			{
				unit.m_ColorCurveComponent.m_pColorCurve = null;
			}
			else
			{
				unit.m_ColorCurveComponent.m_pColorCurve = this.rule.m_ColorCurveComponent.m_pColorCurveArray[Helper.GetIRandom() % this.rule.m_ColorCurveComponent.m_pColorCurveArray.Length];
			}
		}

		// Token: 0x0600507F RID: 20607 RVA: 0x001633C8 File Offset: 0x001617C8
		private void AttachFunc_ColorAnim_Blink(ParticleUnit unit)
		{
			unit.m_BlinkComponent.m_blinkRate = Helper.GetFRandom(0f, 1f);
			unit.m_BlinkComponent.m_blinkSpeed = this.rule.m_BlinkComponent.m_blinkSpanSecRange.Random().value;
		}

		// Token: 0x06005080 RID: 20608 RVA: 0x00163418 File Offset: 0x00161818
		private void AttachFunc_UVAnim(ParticleUnit unit)
		{
			int num = (this.rule.m_uvBlockNum != 0) ? (Helper.GetIRandom() % this.rule.m_uvBlockNum) : 0;
			unit.m_uvRect = this.rule.m_uvRect_TopBlock;
			if (num >= 1)
			{
				int num2 = 0;
				for (;;)
				{
					if (unit.m_uvRect.x + unit.m_uvRect.width > 1f)
					{
						unit.m_uvRect.x = 0f;
						unit.m_uvRect.y = unit.m_uvRect.y - unit.m_uvRect.height;
						if (unit.m_uvRect.y - unit.m_uvRect.height < 0f)
						{
							unit.m_uvRect.y = this.rule.m_uvRect_TopBlock.y;
						}
					}
					if (num2++ >= num)
					{
						break;
					}
					unit.m_uvRect.x = unit.m_uvRect.x + unit.m_uvRect.width;
				}
			}
		}

		// Token: 0x06005081 RID: 20609 RVA: 0x00163530 File Offset: 0x00161930
		private void AttachFunc_UVAnim_UVAnimation(ParticleUnit unit)
		{
			unit.m_UVAnimationComponent.m_nowBlock = ((this.rule.m_uvBlockNum != 0) ? ((!this.rule.m_UVAnimationComponent.m_randomStartBlockFlg) ? 0 : (Helper.GetIRandom() % this.rule.m_uvBlockNum)) : 0);
			unit.m_UVAnimationComponent.m_switchSec = this.rule.m_UVAnimationComponent.m_switchBlockSecRange.Random().value;
			unit.m_UVAnimationComponent.m_switchSecWork = unit.m_UVAnimationComponent.m_switchSec;
			unit.m_uvRect = this.rule.m_uvRect_TopBlock;
			if (unit.m_UVAnimationComponent.m_nowBlock >= 1)
			{
				int num = 0;
				for (;;)
				{
					if (unit.m_uvRect.x + unit.m_uvRect.width > 1f)
					{
						unit.m_uvRect.x = 0f;
						unit.m_uvRect.y = unit.m_uvRect.y - unit.m_uvRect.height;
						if (unit.m_uvRect.y - unit.m_uvRect.height < 0f)
						{
							unit.m_uvRect.y = this.rule.m_uvRect_TopBlock.y;
						}
					}
					if (num++ >= unit.m_UVAnimationComponent.m_nowBlock)
					{
						break;
					}
					unit.m_uvRect.x = unit.m_uvRect.x + unit.m_uvRect.width;
				}
			}
		}

		// Token: 0x06005082 RID: 20610 RVA: 0x001636BC File Offset: 0x00161ABC
		private void CalcFunc_Move_Acceleration(ParticleUnit unit)
		{
			float framerate = this.framerate;
			float num = unit.m_AccelerationComponent.m_dragForce;
			Vector3 velocity = unit.m_velocity;
			if (velocity.sqrMagnitude > 0f)
			{
				velocity.Normalize();
				Vector3 b = velocity * unit.m_AccelerationComponent.m_acceleration * this.time;
				num = Mathf.Pow(num, framerate);
				unit.m_velocity += b;
				unit.m_velocity *= num;
			}
		}

		// Token: 0x06005083 RID: 20611 RVA: 0x00163744 File Offset: 0x00161B44
		private void CalcFunc_Move_Rotation(ParticleUnit unit)
		{
			float framerate = this.framerate;
			float num = Mathf.Min(1f, unit.m_RotationComponent.m_rotDragForce);
			Vector3 rotVelocity = unit.m_RotationComponent.m_rotVelocity;
			if (rotVelocity.sqrMagnitude > 0f)
			{
				rotVelocity.Normalize();
				Vector3 b = unit.m_RotationComponent.m_rotAcceleration * this.time;
				b.Scale(rotVelocity);
				num = Mathf.Pow(num, framerate);
				unit.m_RotationComponent.m_rot = unit.m_RotationComponent.m_rot + unit.m_RotationComponent.m_rotVelocity * this.time;
				unit.m_RotationComponent.m_rotVelocity = unit.m_RotationComponent.m_rotVelocity + b;
				unit.m_RotationComponent.m_rotVelocity = unit.m_RotationComponent.m_rotVelocity * num;
			}
		}

		// Token: 0x06005084 RID: 20612 RVA: 0x00163824 File Offset: 0x00161C24
		private void CalcFunc_Move_PathMove(ParticleUnit unit)
		{
			float value;
			if (unit.m_lifeSpanRate < 0.33f)
			{
				value = unit.m_lifeSpanRate / 0.33f;
				this.m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[0];
				this.m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[0];
				this.m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[1];
				this.m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[2];
			}
			else if (unit.m_lifeSpanRate < 0.66f)
			{
				value = (unit.m_lifeSpanRate - 0.33f) / 0.33f;
				this.m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[0];
				this.m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[1];
				this.m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[2];
				this.m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[3];
			}
			else
			{
				value = (unit.m_lifeSpanRate - 0.66f) / 0.33f;
				this.m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[1];
				this.m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[2];
				this.m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[3];
				this.m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[3];
			}
			MathFunc.CatmullRom(out unit.m_offset, this.m_WorkPaths, Mathf.Clamp01(value));
		}

		// Token: 0x06005085 RID: 20613 RVA: 0x00163A80 File Offset: 0x00161E80
		private void CalcFunc_Move(ParticleUnit unit)
		{
			float num;
			switch (this.rule.m_LifeScaleType)
			{
			default:
				num = unit.m_lifeSpanRate;
				break;
			case eParticleLifeScaleType.eParticleLifeScaleType_CurveAcceleration:
				num = unit.m_lifeSpanRate * unit.m_lifeSpanRate;
				break;
			case eParticleLifeScaleType.eParticleLifeScaleType_CurveSlowdonw:
				num = 1f - unit.m_lifeSpanRate;
				num = 1f - num * num;
				break;
			}
			unit.m_scale = Mathf.Lerp(1f, unit.m_lifeScaleMax, num);
			unit.m_velocity += unit.m_gravityForce * this.time;
		}

		// Token: 0x06005086 RID: 20614 RVA: 0x00163B24 File Offset: 0x00161F24
		private void CalcFunc_ColorAnim_NotColorCurve(ParticleUnit unit)
		{
			unit.m_color = Color.white;
			unit.m_color.a = unit.m_color.a * unit.m_lifeSpanFadeAlphaRate * this.rule.m_AlphaScale;
		}

		// Token: 0x06005087 RID: 20615 RVA: 0x00163B5C File Offset: 0x00161F5C
		private void CalcFunc_ColorAnim_ColorCurve(ParticleUnit unit)
		{
			if (unit.m_ColorCurveComponent.m_pColorCurve != null)
			{
				unit.m_color = unit.m_ColorCurveComponent.m_pColorCurve.Lerp(unit.m_lifeSpanRate).value;
			}
			else
			{
				unit.m_color = Color.white;
			}
			unit.m_color.a = unit.m_color.a * unit.m_lifeSpanFadeAlphaRate * this.rule.m_AlphaScale;
		}

		// Token: 0x06005088 RID: 20616 RVA: 0x00163BD8 File Offset: 0x00161FD8
		private void CalcFunc_ColorAnim_Blink(ParticleUnit unit)
		{
			unit.m_BlinkComponent.m_blinkRate = Mathf.Repeat(unit.m_BlinkComponent.m_blinkRate + this.time / unit.m_BlinkComponent.m_blinkSpeed, 2f);
			float num = (unit.m_BlinkComponent.m_blinkRate <= 1f) ? unit.m_BlinkComponent.m_blinkRate : (2f - unit.m_BlinkComponent.m_blinkRate);
			num *= num;
			num = 1f - num * num;
			unit.m_color.a = unit.m_color.a * num;
		}

		// Token: 0x06005089 RID: 20617 RVA: 0x00163C78 File Offset: 0x00162078
		private void CalcFunc_UVAnim_UVAnimation(ParticleUnit unit)
		{
			unit.m_UVAnimationComponent.m_switchSecWork = unit.m_UVAnimationComponent.m_switchSecWork - this.time;
			if (unit.m_UVAnimationComponent.m_switchSecWork <= 0f)
			{
				unit.m_UVAnimationComponent.m_switchSecWork = unit.m_UVAnimationComponent.m_switchSec;
				unit.m_UVAnimationComponent.m_nowBlock = ((this.rule.m_uvBlockNum != 0) ? ((unit.m_UVAnimationComponent.m_nowBlock + 1) % this.rule.m_uvBlockNum) : 0);
				unit.m_uvRect.x = unit.m_uvRect.x + unit.m_uvRect.width;
				if (unit.m_uvRect.x + unit.m_uvRect.width > 1f)
				{
					unit.m_uvRect.x = 0f;
					unit.m_uvRect.y = unit.m_uvRect.y - unit.m_uvRect.height;
					if (unit.m_uvRect.y - unit.m_uvRect.height < 0f)
					{
						unit.m_uvRect.y = this.rule.m_uvRect_TopBlock.y;
					}
				}
			}
		}

		// Token: 0x0600508A RID: 20618 RVA: 0x00163DB8 File Offset: 0x001621B8
		private void CalcFunc_LifeSpan(ParticleUnit unit)
		{
			if (!unit.m_initializedFlg)
			{
				return;
			}
			switch (this.rule.m_lifeSpanType)
			{
			case eParticleLifeSpanType.eParticleLifeSpanType_Time:
				unit.m_lifeSpanRate = Mathf.Clamp(unit.m_lifeSpanRate + this.time / unit.m_lifeSpanParam[0], 0f, 1f);
				break;
			case eParticleLifeSpanType.eParticleLifeSpanType_Distance:
				unit.m_lifeSpanRate = Mathf.Clamp((unit.m_pos - unit.m_birthPos).magnitude / unit.m_lifeSpanParam[0], 0f, 1f);
				break;
			case eParticleLifeSpanType.eParticleLifeSpanType_HeightRange:
				if (unit.m_pos.y >= unit.m_birthPos.y)
				{
					float num = unit.m_pos.y - unit.m_birthPos.y;
					float num2 = unit.m_lifeSpanParam[1] - unit.m_birthPos.y;
					if (num2 == 0f)
					{
						unit.m_lifeSpanRate = 1f;
					}
					else
					{
						unit.m_lifeSpanRate = Mathf.Clamp(num / num2, 0f, 1f);
					}
				}
				else
				{
					float num3 = unit.m_birthPos.y - unit.m_pos.y;
					float num4 = unit.m_birthPos.y - unit.m_lifeSpanParam[0];
					if (num4 == 0f)
					{
						unit.m_lifeSpanRate = 1f;
					}
					else
					{
						unit.m_lifeSpanRate = Mathf.Clamp(num3 / num4, 0f, 1f);
					}
				}
				break;
			case eParticleLifeSpanType.eParticleLifeSpanType_AnimFrame:
			{
				float framerate = this.framerate;
				unit.m_lifeSpanRate = Mathf.Clamp(unit.m_lifeSpanRate + unit.m_lifeSpanParam[0] * framerate, 0f, 1f);
				break;
			}
			}
			switch (this.rule.m_lifeSpanAlpha)
			{
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_None:
				unit.m_lifeSpanFadeAlphaRate = 1f;
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeInOut:
				if (unit.m_lifeSpanRate < 0.2f)
				{
					unit.m_lifeSpanFadeAlphaRate = unit.m_lifeSpanRate / 0.2f;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate * unit.m_lifeSpanFadeAlphaRate;
				}
				else if (unit.m_lifeSpanRate >= 0.8f)
				{
					unit.m_lifeSpanFadeAlphaRate = (unit.m_lifeSpanRate - 0.8f) / 0.2f;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate * unit.m_lifeSpanFadeAlphaRate;
				}
				else
				{
					unit.m_lifeSpanFadeAlphaRate = 1f;
				}
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeIn:
				if (unit.m_lifeSpanRate < 0.2f)
				{
					unit.m_lifeSpanFadeAlphaRate = unit.m_lifeSpanRate / 0.2f;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate * unit.m_lifeSpanFadeAlphaRate;
				}
				else
				{
					unit.m_lifeSpanFadeAlphaRate = 1f;
				}
				break;
			case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeOut:
				if (unit.m_lifeSpanRate >= 0.8f)
				{
					unit.m_lifeSpanFadeAlphaRate = (unit.m_lifeSpanRate - 0.8f) / 0.2f;
					unit.m_lifeSpanFadeAlphaRate = 1f - unit.m_lifeSpanFadeAlphaRate * unit.m_lifeSpanFadeAlphaRate;
				}
				break;
			}
			if (unit.m_lifeSpanRate >= 1f)
			{
				unit.m_activeFlg = false;
			}
		}

		// Token: 0x0600508B RID: 20619 RVA: 0x00164124 File Offset: 0x00162524
		private void CalcFuncMerge(ParticleUnit unit)
		{
			Vector3 pos = unit.m_pos;
			if (this.rule.m_particleType != eParticleType.eParticleType_Ribbon)
			{
				unit.m_pos += unit.m_velocity * this.time;
			}
			eParticleCollisionType collisionType = this.rule.m_collisionType;
			if (collisionType != eParticleCollisionType.eParticleCollisionType_Height)
			{
				if (collisionType != eParticleCollisionType.eParticleCollisionType_Collision)
				{
				}
			}
			else if (this.rule.m_collisionParam.m_height.m_isLocalHeight)
			{
				float height = this.rule.m_collisionParam.m_height.m_height;
				if (pos.y >= height && unit.m_pos.y < height)
				{
					unit.m_pos.y = height + Mathf.Max(height - unit.m_pos.y, 0.001f);
					unit.m_velocity.x = unit.m_velocity.x * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
					unit.m_velocity.y = -unit.m_velocity.y * this.rule.m_collisionParam.m_height.m_reflectionCoefficient;
					unit.m_velocity.z = unit.m_velocity.z * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
				}
				else if (pos.y <= height && unit.m_pos.y > height)
				{
					unit.m_pos.y = height + Mathf.Min(height - unit.m_pos.y, -0.001f);
					unit.m_velocity.x = unit.m_velocity.x * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
					unit.m_velocity.y = -unit.m_velocity.y * this.rule.m_collisionParam.m_height.m_reflectionCoefficient;
					unit.m_velocity.z = unit.m_velocity.z * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
				}
			}
			else if (pos.y >= this.rule.m_collisionParam.m_height.m_height && unit.m_pos.y < this.rule.m_collisionParam.m_height.m_height)
			{
				unit.m_pos.y = this.rule.m_collisionParam.m_height.m_height + Mathf.Max(this.rule.m_collisionParam.m_height.m_height - unit.m_pos.y, 0.001f);
				unit.m_velocity.x = unit.m_velocity.x * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
				unit.m_velocity.y = -unit.m_velocity.y * this.rule.m_collisionParam.m_height.m_reflectionCoefficient;
				unit.m_velocity.z = unit.m_velocity.z * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
			}
			else if (pos.y <= this.rule.m_collisionParam.m_height.m_height && unit.m_pos.y > this.rule.m_collisionParam.m_height.m_height)
			{
				unit.m_pos.y = this.rule.m_collisionParam.m_height.m_height + Mathf.Min(this.rule.m_collisionParam.m_height.m_height - unit.m_pos.y, -0.001f);
				unit.m_velocity.x = unit.m_velocity.x * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
				unit.m_velocity.y = -unit.m_velocity.y * this.rule.m_collisionParam.m_height.m_reflectionCoefficient;
				unit.m_velocity.z = unit.m_velocity.z * this.rule.m_collisionParam.m_height.m_frictionCoefficient;
			}
		}

		// Token: 0x0600508C RID: 20620 RVA: 0x00164590 File Offset: 0x00162990
		private void CalcFuncMerge_NotRotation(ParticleUnit unit)
		{
			int index = unit.m_Index;
			switch (unit.m_particleType)
			{
			case eParticleType.eParticleType_Billboard:
				if (!unit.m_activeFlg)
				{
					this.m_BillboardBuffer.positions[index] = Vector3.zero;
					this.m_BillboardBuffer.rots[index] = 0f;
					this.m_BillboardBuffer.sizes[index].Set(0f, 0f);
					this.m_BillboardBuffer.colors[index] = Color.clear;
					return;
				}
				this.m_BillboardBuffer.positions[index] = unit.m_pos + unit.m_offset;
				this.m_BillboardBuffer.sizes[index].Set(unit.m_particleTypeParam.m_billboard.m_width * unit.m_scale, unit.m_particleTypeParam.m_billboard.m_height * unit.m_scale);
				this.m_BillboardBuffer.rots[index] = 0f;
				this.m_BillboardBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
				this.m_BillboardBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_BillboardBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_BillboardBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_BillboardBuffer.positions[index];
					unit.m_TailComponent.m_width = this.m_BillboardBuffer.sizes[index].x;
				}
				break;
			case eParticleType.eParticleType_Point:
				if (!unit.m_activeFlg)
				{
					this.m_PointBuffer.positions[index] = Vector3.zero;
					this.m_PointBuffer.sizes[index] = 0f;
					this.m_PointBuffer.colors[index] = Color.clear;
					return;
				}
				this.m_PointBuffer.positions[index] = unit.m_pos + unit.m_offset;
				this.m_PointBuffer.sizes[index] = unit.m_particleTypeParam.m_point.m_size;
				this.m_PointBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_PointBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_PointBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_PointBuffer.positions[index];
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_point.m_size;
				}
				break;
			case eParticleType.eParticleType_Line:
			{
				PrimLineBuffer.LineBuffer.Line line = this.m_LineBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int i = 0; i < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); i++)
					{
						line.SetPosition(Vector3.zero, 0);
						line.SetWidth(0f, i);
						line.SetColor(Color.clear, i);
					}
					return;
				}
				if (!unit.m_initializedFlg)
				{
					Vector3 vector = unit.m_pos + unit.m_offset;
					for (int j = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1); j >= 1; j--)
					{
						unit.m_particleTypeParam.m_line.m_pPos[j] = vector;
					}
				}
				else
				{
					for (int k = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1); k >= 1; k--)
					{
						unit.m_particleTypeParam.m_line.m_pPos[k] = unit.m_particleTypeParam.m_line.m_pPos[k - 1];
					}
				}
				unit.m_particleTypeParam.m_line.m_pPos[0] = unit.m_pos + unit.m_offset;
				if (unit.m_particleTypeParam.m_line.m_HistoryPointNum == unit.m_particleTypeParam.m_line.m_jointNum + 2)
				{
					for (int l = 0; l < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); l++)
					{
						Vector3 vector2 = unit.m_particleTypeParam.m_line.m_pPos[l];
						if (unit.m_isLocal)
						{
							vector2 = this.m_Matrix.MultiplyPoint3x4(vector2);
						}
						line.SetPosition(vector2, l);
						Vector2 uv;
						uv.x = unit.m_uvRect.x;
						uv.y = unit.m_uvRect.y + unit.m_uvRect.height * (float)l / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						line.SetUV(uv, l);
						line.SetWidth(1f, l);
						if (unit.m_aliveCount > l + 2)
						{
							line.SetColor(unit.m_color, l);
						}
					}
				}
				else
				{
					float num = Mathf.Max((float)(unit.m_particleTypeParam.m_line.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_line.m_HistoryPointNum, 1f);
					for (int m = 0; m < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); m++)
					{
						float num2 = (float)m / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						float num3 = num2 * (float)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						int num4 = (int)num3;
						int num5 = num4 - 1;
						if (num5 < 0)
						{
							num5 = 0;
						}
						int num6 = num4 + 1;
						if (num6 >= (int)unit.m_particleTypeParam.m_line.m_HistoryPointNum)
						{
							num6 = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						}
						int num7 = num4 + 2;
						if (num7 >= (int)unit.m_particleTypeParam.m_line.m_HistoryPointNum)
						{
							num7 = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						}
						num3 -= (float)((int)num3);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_line.m_pPos[num5];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_line.m_pPos[num4];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_line.m_pPos[num6];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_line.m_pPos[num7];
						Vector3 vector2;
						MathFunc.CatmullRom(out vector2, this.m_WorkPaths, num3);
						if (unit.m_isLocal)
						{
							vector2 = this.m_Matrix.MultiplyPoint3x4(vector2);
						}
						line.SetPosition(vector2, m);
						Vector2 uv;
						uv.x = unit.m_uvRect.x;
						uv.y = unit.m_uvRect.y + unit.m_uvRect.height * (float)m / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						line.SetUV(uv, m);
						line.SetWidth(1f, m);
						if ((float)unit.m_aliveCount > (float)m / num + 2f)
						{
							line.SetColor(unit.m_color, m);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_line.m_width;
				}
				break;
			}
			case eParticleType.eParticleType_PolyLine:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line2 = this.m_PolylineBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int n = 0; n < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); n++)
					{
						line2.SetPosition(Vector3.zero, n);
						line2.SetWidth(0f, n);
						line2.SetColor(Color.clear, n);
					}
					return;
				}
				if (!unit.m_initializedFlg)
				{
					Vector3 vector3 = unit.m_pos + unit.m_offset;
					for (int num8 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1); num8 >= 1; num8--)
					{
						unit.m_particleTypeParam.m_polyLine.m_pPos[num8] = vector3;
					}
				}
				else
				{
					for (int num9 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1); num9 >= 1; num9--)
					{
						unit.m_particleTypeParam.m_polyLine.m_pPos[num9] = unit.m_particleTypeParam.m_polyLine.m_pPos[num9 - 1];
					}
				}
				unit.m_particleTypeParam.m_polyLine.m_pPos[0] = unit.m_pos + unit.m_offset;
				if (unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum == unit.m_particleTypeParam.m_polyLine.m_jointNum + 2)
				{
					for (int num10 = 0; num10 < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); num10++)
					{
						Vector3 vector4 = unit.m_particleTypeParam.m_polyLine.m_pPos[num10];
						if (unit.m_isLocal)
						{
							vector4 = this.m_Matrix.MultiplyPoint3x4(vector4);
						}
						line2.SetPosition(vector4, num10);
						Vector2 uv2;
						uv2.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num10 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						uv2.y = unit.m_uvRect.y;
						line2.SetUV(uv2, unit.m_uvRect.height, num10);
						line2.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_polyLine.m_topWidth, unit.m_particleTypeParam.m_polyLine.m_endWidth, (float)num10 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 1)) * unit.m_scale, num10);
						if (unit.m_aliveCount > num10 + 2)
						{
							line2.SetColor(unit.m_color, num10);
						}
					}
				}
				else
				{
					float num11 = Mathf.Max((float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum, 1f);
					for (int num12 = 0; num12 < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); num12++)
					{
						float num13 = (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						float num14 = num13 * (float)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						int num15 = (int)num14;
						int num16 = num15 - 1;
						if (num16 < 0)
						{
							num16 = 0;
						}
						int num17 = num15 + 1;
						if (num17 >= (int)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum)
						{
							num17 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						}
						int num18 = num15 + 2;
						if (num18 >= (int)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum)
						{
							num18 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						}
						num14 -= (float)((int)num14);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_polyLine.m_pPos[num16];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_polyLine.m_pPos[num15];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_polyLine.m_pPos[num17];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_polyLine.m_pPos[num18];
						Vector3 vector4;
						MathFunc.CatmullRom(out vector4, this.m_WorkPaths, num14);
						if (unit.m_isLocal)
						{
							vector4 = this.m_Matrix.MultiplyPoint3x4(vector4);
						}
						line2.SetPosition(vector4, num12);
						Vector2 uv2;
						uv2.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						uv2.y = unit.m_uvRect.y;
						line2.SetUV(uv2, unit.m_uvRect.height, num12);
						line2.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_polyLine.m_topWidth, unit.m_particleTypeParam.m_polyLine.m_endWidth, (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 1)) * unit.m_scale, num12);
						if ((float)unit.m_aliveCount > (float)num12 / num11 + 2f)
						{
							line2.SetColor(unit.m_color, num12);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line2.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_polyLine.m_topWidth * unit.m_scale;
				}
				break;
			}
			case eParticleType.eParticleType_Confetti:
				if (!unit.m_activeFlg)
				{
					this.m_ConfettiBuffer.positions[index] = Vector3.zero;
					this.m_ConfettiBuffer.sizes[index].Set(0f, 0f);
					this.m_ConfettiBuffer.colors[index] = Color.clear;
					return;
				}
				this.m_ConfettiBuffer.positions[index] = unit.m_pos + unit.m_offset;
				this.m_ConfettiBuffer.sizes[index].Set(unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale, unit.m_particleTypeParam.m_confetti.m_height * unit.m_scale);
				this.m_ConfettiBuffer.rots[index].Set(0f, 0f, 0f);
				this.m_ConfettiBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
				this.m_ConfettiBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_ConfettiBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_ConfettiBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_ConfettiBuffer.positions[index];
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale;
				}
				break;
			case eParticleType.eParticleType_Ribbon:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line3 = this.m_RibbonBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int num19 = 0; num19 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num19++)
					{
						line3.SetPosition(Vector3.zero, num19);
						line3.SetWidth(0f, num19);
						line3.SetColor(Color.clear, num19);
					}
					return;
				}
				if (!unit.m_initializedFlg)
				{
					Vector3 vector5 = new Vector3(this.m_Matrix.GetColumn(3).x, this.m_Matrix.GetColumn(3).y, this.m_Matrix.GetColumn(3).z);
					vector5 += unit.m_offset;
					for (int num20 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1); num20 >= 1; num20--)
					{
						unit.m_particleTypeParam.m_ribbon.m_pPos[num20] = vector5;
						unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = Vector3.zero;
					}
				}
				else
				{
					for (int num21 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1); num21 >= 1; num21--)
					{
						unit.m_particleTypeParam.m_ribbon.m_pPos[num21] = unit.m_particleTypeParam.m_ribbon.m_pPos[num21 - 1];
						unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21] = unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21 - 1];
						unit.m_particleTypeParam.m_ribbon.m_pPos[num21] = unit.m_particleTypeParam.m_ribbon.m_pPos[num21] + unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21] * this.time;
					}
				}
				if (this.m_isActive)
				{
					unit.m_particleTypeParam.m_ribbon.m_pPos[0] = new Vector3(this.m_Matrix.GetColumn(3).x, this.m_Matrix.GetColumn(3).y, this.m_Matrix.GetColumn(3).z);
					unit.m_particleTypeParam.m_ribbon.m_pPos[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[0] + unit.m_offset;
					unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = unit.m_velocity;
				}
				if (unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum == unit.m_particleTypeParam.m_ribbon.m_jointNum + 2)
				{
					for (int num22 = 0; num22 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num22++)
					{
						Vector3 vector6 = unit.m_particleTypeParam.m_ribbon.m_pPos[num22];
						if (unit.m_isLocal)
						{
							vector6 = this.m_Matrix.MultiplyPoint3x4(vector6);
						}
						line3.SetPosition(vector6, num22);
						Vector2 uv3;
						uv3.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num22 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						uv3.y = unit.m_uvRect.y;
						line3.SetUV(uv3, unit.m_uvRect.height, num22);
						line3.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_ribbon.m_topWidth, unit.m_particleTypeParam.m_ribbon.m_endWidth, (float)num22 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 1)) * unit.m_scale, num22);
						if (unit.m_aliveCount > num22 + 2)
						{
							line3.SetColor(unit.m_color, num22);
						}
					}
				}
				else
				{
					float num23 = Mathf.Max((float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum, 1f);
					for (int num24 = 0; num24 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num24++)
					{
						float num25 = (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						float num26 = num25 * (float)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						int num27 = (int)num26;
						int num28 = num27 - 1;
						if (num28 < 0)
						{
							num28 = 0;
						}
						int num29 = num27 + 1;
						if (num29 >= (int)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum)
						{
							num29 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						}
						int num30 = num27 + 2;
						if (num30 >= (int)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum)
						{
							num30 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						}
						num26 -= (float)((int)num26);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[num28];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_ribbon.m_pPos[num27];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_ribbon.m_pPos[num29];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_ribbon.m_pPos[num30];
						Vector3 vector6;
						MathFunc.CatmullRom(out vector6, this.m_WorkPaths, num26);
						if (unit.m_isLocal)
						{
							vector6 = this.m_Matrix.MultiplyPoint3x4(vector6);
						}
						line3.SetPosition(vector6, num24);
						Vector2 uv3;
						uv3.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						uv3.y = unit.m_uvRect.y;
						line3.SetUV(uv3, unit.m_uvRect.height, num24);
						line3.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_ribbon.m_topWidth, unit.m_particleTypeParam.m_ribbon.m_endWidth, (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 1)) * unit.m_scale, num24);
						if ((float)unit.m_aliveCount > (float)num24 / num23 + 2f)
						{
							line3.SetColor(unit.m_color, num24);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line3.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_ribbon.m_topWidth * unit.m_scale;
				}
				break;
			}
			}
		}

		// Token: 0x0600508D RID: 20621 RVA: 0x00165D50 File Offset: 0x00164150
		private void CalcFuncMerge_Rotation(ParticleUnit unit)
		{
			int index = unit.m_Index;
			switch (unit.m_particleType)
			{
			case eParticleType.eParticleType_Billboard:
			{
				if (!unit.m_activeFlg)
				{
					this.m_BillboardBuffer.positions[index] = Vector3.zero;
					this.m_BillboardBuffer.rots[index] = 0f;
					this.m_BillboardBuffer.sizes[index].Set(0f, 0f);
					this.m_BillboardBuffer.colors[index] = Color.clear;
					return;
				}
				Vector3 vector = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation = Quaternion.identity;
				rotation = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector = rotation * vector;
				this.m_BillboardBuffer.positions[index] = unit.m_pos + unit.m_offset + vector;
				this.m_BillboardBuffer.sizes[index].Set(unit.m_particleTypeParam.m_billboard.m_width * unit.m_scale, unit.m_particleTypeParam.m_billboard.m_height * unit.m_scale);
				this.m_BillboardBuffer.rots[index] = unit.m_RotationComponent.m_rot.z;
				this.m_BillboardBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
				this.m_BillboardBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_BillboardBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_BillboardBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_BillboardBuffer.positions[index];
					unit.m_TailComponent.m_width = this.m_BillboardBuffer.sizes[index].x;
				}
				break;
			}
			case eParticleType.eParticleType_Point:
			{
				if (!unit.m_activeFlg)
				{
					this.m_PointBuffer.positions[index] = Vector3.zero;
					this.m_PointBuffer.sizes[index] = 0f;
					this.m_PointBuffer.colors[index] = Color.clear;
					return;
				}
				Vector3 vector2 = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation2 = Quaternion.identity;
				rotation2 = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector2 = rotation2 * vector2;
				this.m_PointBuffer.positions[index] = unit.m_pos + unit.m_offset + vector2;
				this.m_PointBuffer.sizes[index] = unit.m_particleTypeParam.m_point.m_size;
				this.m_PointBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_PointBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_PointBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_PointBuffer.positions[index];
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_point.m_size;
				}
				break;
			}
			case eParticleType.eParticleType_Line:
			{
				PrimLineBuffer.LineBuffer.Line line = this.m_LineBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int i = 0; i < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); i++)
					{
						line.SetPosition(Vector3.zero, i);
						line.SetWidth(0f, i);
						line.SetColor(Color.clear, i);
					}
					return;
				}
				Vector3 vector3 = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation3 = Quaternion.identity;
				rotation3 = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector3 = rotation3 * vector3;
				if (!unit.m_initializedFlg)
				{
					Vector3 vector4 = unit.m_pos + unit.m_offset + vector3;
					for (int j = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1); j >= 1; j--)
					{
						unit.m_particleTypeParam.m_line.m_pPos[j] = vector4;
					}
				}
				else
				{
					for (int k = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1); k >= 1; k--)
					{
						unit.m_particleTypeParam.m_line.m_pPos[k] = unit.m_particleTypeParam.m_line.m_pPos[k - 1];
					}
				}
				unit.m_particleTypeParam.m_line.m_pPos[0] = unit.m_pos + unit.m_offset + vector3;
				if (unit.m_particleTypeParam.m_line.m_HistoryPointNum == unit.m_particleTypeParam.m_line.m_jointNum + 2)
				{
					for (int l = 0; l < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); l++)
					{
						Vector3 v = unit.m_particleTypeParam.m_line.m_pPos[l];
						if (unit.m_isLocal)
						{
							v = this.m_Matrix.MultiplyPoint3x4(v);
						}
						line.SetPosition(unit.m_particleTypeParam.m_line.m_pPos[l], l);
						Vector2 uv;
						uv.x = unit.m_uvRect.x;
						uv.y = unit.m_uvRect.y + unit.m_uvRect.height * (float)l / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						line.SetUV(uv, l);
						line.SetWidth(1f, l);
						if (unit.m_aliveCount > l + 2)
						{
							line.SetColor(unit.m_color, l);
						}
					}
				}
				else
				{
					float num = Mathf.Max((float)(unit.m_particleTypeParam.m_line.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_line.m_HistoryPointNum, 1f);
					for (int m = 0; m < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); m++)
					{
						float num2 = (float)m / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						float num3 = num2 * (float)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						int num4 = (int)num3;
						int num5 = num4 - 1;
						if (num5 < 0)
						{
							num5 = 0;
						}
						int num6 = num4 + 1;
						if (num6 >= (int)unit.m_particleTypeParam.m_line.m_HistoryPointNum)
						{
							num6 = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						}
						int num7 = num4 + 2;
						if (num7 >= (int)unit.m_particleTypeParam.m_line.m_HistoryPointNum)
						{
							num7 = (int)(unit.m_particleTypeParam.m_line.m_HistoryPointNum - 1);
						}
						num3 -= (float)((int)num3);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_line.m_pPos[num5];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_line.m_pPos[num4];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_line.m_pPos[num6];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_line.m_pPos[num7];
						Vector3 v;
						MathFunc.CatmullRom(out v, this.m_WorkPaths, num3);
						if (unit.m_isLocal)
						{
							v = this.m_Matrix.MultiplyPoint3x4(v);
						}
						line.SetPosition(unit.m_particleTypeParam.m_line.m_pPos[m], m);
						Vector2 uv;
						uv.x = unit.m_uvRect.x;
						uv.y = unit.m_uvRect.y + unit.m_uvRect.height * (float)m / (float)(unit.m_particleTypeParam.m_line.m_jointNum + 2 - 1);
						line.SetUV(uv, m);
						line.SetWidth(1f, m);
						if ((float)unit.m_aliveCount > (float)m / num + 2f)
						{
							line.SetColor(unit.m_color, m);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_line.m_width;
				}
				break;
			}
			case eParticleType.eParticleType_PolyLine:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line2 = this.m_PolylineBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int n = 0; n < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); n++)
					{
						line2.SetPosition(Vector3.zero, n);
						line2.SetWidth(0f, n);
						line2.SetColor(Color.clear, n);
					}
					return;
				}
				Vector3 vector5 = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation4 = Quaternion.identity;
				rotation4 = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector5 = rotation4 * vector5;
				if (!unit.m_initializedFlg)
				{
					Vector3 vector6 = unit.m_pos + unit.m_offset + vector5;
					for (int num8 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1); num8 >= 1; num8--)
					{
						unit.m_particleTypeParam.m_polyLine.m_pPos[num8] = vector6;
					}
				}
				else
				{
					for (int num9 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1); num9 >= 1; num9--)
					{
						unit.m_particleTypeParam.m_polyLine.m_pPos[num9] = unit.m_particleTypeParam.m_polyLine.m_pPos[num9 - 1];
					}
				}
				unit.m_particleTypeParam.m_polyLine.m_pPos[0] = unit.m_pos + unit.m_offset + vector5;
				if (unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum == unit.m_particleTypeParam.m_polyLine.m_jointNum + 2)
				{
					for (int num10 = 0; num10 < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); num10++)
					{
						Vector3 vector7 = unit.m_particleTypeParam.m_polyLine.m_pPos[num10];
						if (unit.m_isLocal)
						{
							vector7 = this.m_Matrix.MultiplyPoint3x4(vector7);
						}
						line2.SetPosition(vector7, num10);
						Vector2 uv2;
						uv2.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num10 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						uv2.y = unit.m_uvRect.y;
						line2.SetUV(uv2, unit.m_uvRect.height, num10);
						line2.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_polyLine.m_topWidth, unit.m_particleTypeParam.m_polyLine.m_endWidth, (float)num10 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 1)) * unit.m_scale, num10);
						if (unit.m_aliveCount > num10 + 2)
						{
							line2.SetColor(unit.m_color, num10);
						}
					}
				}
				else
				{
					float num11 = Mathf.Max((float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum, 1f);
					for (int num12 = 0; num12 < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); num12++)
					{
						float num13 = (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						float num14 = num13 * (float)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						int num15 = (int)num14;
						int num16 = num15 - 1;
						if (num16 < 0)
						{
							num16 = 0;
						}
						int num17 = num15 + 1;
						if (num17 >= (int)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum)
						{
							num17 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						}
						int num18 = num15 + 2;
						if (num18 >= (int)unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum)
						{
							num18 = (int)(unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum - 1);
						}
						num14 -= (float)((int)num14);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_polyLine.m_pPos[num16];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_polyLine.m_pPos[num15];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_polyLine.m_pPos[num17];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_polyLine.m_pPos[num18];
						Vector3 vector7;
						MathFunc.CatmullRom(out vector7, this.m_WorkPaths, num14);
						if (unit.m_isLocal)
						{
							vector7 = this.m_Matrix.MultiplyPoint3x4(vector7);
						}
						line2.SetPosition(vector7, num12);
						Vector2 uv2;
						uv2.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2 - 1);
						uv2.y = unit.m_uvRect.y;
						line2.SetUV(uv2, unit.m_uvRect.height, num12);
						line2.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_polyLine.m_topWidth, unit.m_particleTypeParam.m_polyLine.m_endWidth, (float)num12 / (float)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 1)) * unit.m_scale, num12);
						if ((float)unit.m_aliveCount > (float)num12 / num11 + 2f)
						{
							line2.SetColor(unit.m_color, num12);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line2.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_polyLine.m_topWidth * unit.m_scale;
				}
				break;
			}
			case eParticleType.eParticleType_Confetti:
			{
				if (!unit.m_activeFlg)
				{
					this.m_ConfettiBuffer.rots[index] = Vector3.zero;
					this.m_ConfettiBuffer.sizes[index].Set(0f, 0f);
					this.m_ConfettiBuffer.colors[index] = Color.clear;
					return;
				}
				Vector3 vector8 = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation5 = Quaternion.identity;
				rotation5 = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector8 = rotation5 * vector8;
				this.m_ConfettiBuffer.positions[index] = unit.m_pos + unit.m_offset + vector8;
				this.m_ConfettiBuffer.sizes[index].Set(unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale, unit.m_particleTypeParam.m_confetti.m_height * unit.m_scale);
				this.m_ConfettiBuffer.rots[index] = unit.m_RotationComponent.m_rot;
				this.m_ConfettiBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
				this.m_ConfettiBuffer.colors[index] = unit.m_color;
				if (unit.m_isLocal)
				{
					this.m_ConfettiBuffer.positions[index] = this.m_Matrix.MultiplyPoint3x4(this.m_ConfettiBuffer.positions[index]);
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = this.m_ConfettiBuffer.positions[index];
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale;
				}
				break;
			}
			case eParticleType.eParticleType_Ribbon:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line3 = this.m_RibbonBuffer.GetLine(index);
				if (!unit.m_activeFlg)
				{
					for (int num19 = 0; num19 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num19++)
					{
						line3.SetPosition(Vector3.zero, num19);
						line3.SetWidth(0f, num19);
						line3.SetColor(Color.clear, num19);
					}
					return;
				}
				Vector3 vector9 = unit.m_RotationComponent.m_rotAnchorOffset;
				Quaternion rotation6 = Quaternion.identity;
				rotation6 = Quaternion.Euler(unit.m_RotationComponent.m_rot);
				vector9 = rotation6 * vector9;
				if (!unit.m_initializedFlg)
				{
					Vector3 vector10 = new Vector3(this.m_Matrix.GetColumn(3).x, this.m_Matrix.GetColumn(3).y, this.m_Matrix.GetColumn(3).z);
					vector10 = vector10 + unit.m_offset + vector9;
					for (int num20 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1); num20 >= 1; num20--)
					{
						unit.m_particleTypeParam.m_ribbon.m_pPos[num20] = vector10;
						unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = Vector3.zero;
					}
				}
				else
				{
					for (int num21 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1); num21 >= 1; num21--)
					{
						unit.m_particleTypeParam.m_ribbon.m_pPos[num21] = unit.m_particleTypeParam.m_ribbon.m_pPos[num21 - 1];
						unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21] = unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21 - 1];
						unit.m_particleTypeParam.m_ribbon.m_pPos[num21] = unit.m_particleTypeParam.m_ribbon.m_pPos[num21] + unit.m_particleTypeParam.m_ribbon.m_pVelocity[num21] * this.time;
					}
				}
				if (this.m_isActive)
				{
					unit.m_particleTypeParam.m_ribbon.m_pPos[0] = new Vector3(this.m_Matrix.GetColumn(3).x, this.m_Matrix.GetColumn(3).y, this.m_Matrix.GetColumn(3).z);
					unit.m_particleTypeParam.m_ribbon.m_pPos[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[0] + unit.m_offset + vector9;
					unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = unit.m_velocity;
				}
				if (unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum == unit.m_particleTypeParam.m_ribbon.m_jointNum + 2)
				{
					for (int num22 = 0; num22 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num22++)
					{
						Vector3 vector11 = unit.m_particleTypeParam.m_ribbon.m_pPos[num22];
						if (unit.m_isLocal)
						{
							vector11 = this.m_Matrix.MultiplyPoint3x4(vector11);
						}
						line3.SetPosition(vector11, num22);
						Vector2 uv3;
						uv3.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num22 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						uv3.y = unit.m_uvRect.y;
						line3.SetUV(uv3, unit.m_uvRect.height, num22);
						line3.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_ribbon.m_topWidth, unit.m_particleTypeParam.m_ribbon.m_endWidth, (float)num22 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 1)) * unit.m_scale, num22);
						if (unit.m_aliveCount > num22 + 2)
						{
							line3.SetColor(unit.m_color, num22);
						}
					}
				}
				else
				{
					float num23 = Mathf.Max((float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2) / (float)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum, 1f);
					for (int num24 = 0; num24 < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); num24++)
					{
						float num25 = (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						float num26 = num25 * (float)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						int num27 = (int)num26;
						int num28 = num27 - 1;
						if (num28 < 0)
						{
							num28 = 0;
						}
						int num29 = num27 + 1;
						if (num29 >= (int)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum)
						{
							num29 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						}
						int num30 = num27 + 2;
						if (num30 >= (int)unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum)
						{
							num30 = (int)(unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum - 1);
						}
						num26 -= (float)((int)num26);
						this.m_WorkPaths[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[num28];
						this.m_WorkPaths[1] = unit.m_particleTypeParam.m_ribbon.m_pPos[num27];
						this.m_WorkPaths[2] = unit.m_particleTypeParam.m_ribbon.m_pPos[num29];
						this.m_WorkPaths[3] = unit.m_particleTypeParam.m_ribbon.m_pPos[num30];
						Vector3 vector11;
						MathFunc.CatmullRom(out vector11, this.m_WorkPaths, num26);
						if (unit.m_isLocal)
						{
							vector11 = this.m_Matrix.MultiplyPoint3x4(vector11);
						}
						line3.SetPosition(vector11, num24);
						Vector2 uv3;
						uv3.x = unit.m_uvRect.x + unit.m_uvRect.width * (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2 - 1);
						uv3.y = unit.m_uvRect.y;
						line3.SetUV(uv3, unit.m_uvRect.height, num24);
						line3.SetWidth(Mathf.Lerp(unit.m_particleTypeParam.m_ribbon.m_topWidth, unit.m_particleTypeParam.m_ribbon.m_endWidth, (float)num24 / (float)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 1)) * unit.m_scale, num24);
						if ((float)unit.m_aliveCount > (float)num24 / num23 + 2f)
						{
							line3.SetColor(unit.m_color, num24);
						}
					}
				}
				if (this.rule.m_UsingTailFlg)
				{
					unit.m_TailComponent.m_TopPos = line3.GetPosition(0);
					unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_ribbon.m_topWidth * unit.m_scale;
				}
				break;
			}
			}
		}

		// Token: 0x0600508E RID: 20622 RVA: 0x001676BC File Offset: 0x00165ABC
		private void CalcFuncMerge_Tail(ParticleUnit unit)
		{
			PrimPolyLineBuffer.PolyLineBuffer.Line line = this.m_TailBuffer.GetLine(unit.m_Index);
			if (!unit.m_activeFlg)
			{
				for (int i = 0; i < this.rule.m_TailComponent.m_tailJointNum + 2; i++)
				{
					line.SetWidth(0f, i);
					line.SetColor(Color.clear, i);
				}
				return;
			}
			if (!unit.m_initializedFlg)
			{
				for (int j = this.rule.m_TailComponent.m_tailJointNum + 2 - 1; j >= 1; j--)
				{
					unit.m_TailComponent.m_pTailPos[j] = unit.m_TailComponent.m_TopPos;
				}
			}
			else
			{
				for (int k = this.rule.m_TailComponent.m_tailJointNum + 2 - 1; k >= 1; k--)
				{
					unit.m_TailComponent.m_pTailPos[k] = unit.m_TailComponent.m_pTailPos[k - 1];
				}
			}
			unit.m_TailComponent.m_pTailPos[0] = unit.m_TailComponent.m_TopPos;
			for (int l = 0; l < this.rule.m_TailComponent.m_tailJointNum + 2; l++)
			{
				line.SetPosition(unit.m_TailComponent.m_pTailPos[l], l);
				Vector2 uv;
				uv.x = unit.m_TailComponent.m_tailUVRect.x;
				uv.y = unit.m_TailComponent.m_tailUVRect.y + unit.m_TailComponent.m_tailUVRect.height * (float)l / (float)(this.rule.m_TailComponent.m_tailJointNum + 2 - 1);
				line.SetUV(uv, unit.m_TailComponent.m_tailUVRect.width, l);
				line.SetWidth(unit.m_TailComponent.m_width * 0.75f, l);
				if (unit.m_aliveCount > l + 2)
				{
					line.SetColor(unit.m_color, l);
				}
			}
		}

		// Token: 0x0600508F RID: 20623 RVA: 0x001678DC File Offset: 0x00165CDC
		private void UpdateDirtyMesh(bool active)
		{
			if (this.m_BillboardBuffer != null)
			{
				this.m_BillboardBuffer.UpdatePositions();
				this.m_BillboardBuffer.UpdateSizes();
				this.m_BillboardBuffer.UpdateRots();
				this.m_BillboardBuffer.UpdateUVs();
				this.m_BillboardBuffer.UpdateColors();
				this.m_BillboardBuffer.enabled = active;
			}
			if (this.m_PointBuffer != null)
			{
				this.m_PointBuffer.UpdatePositions();
				this.m_PointBuffer.UpdateSizes();
				this.m_PointBuffer.UpdateUVs();
				this.m_PointBuffer.UpdateColors();
				this.m_PointBuffer.enabled = active;
			}
			if (this.m_LineBuffer != null)
			{
				this.m_LineBuffer.UpdatePositions();
				this.m_LineBuffer.UpdateWidths();
				this.m_LineBuffer.UpdateUVs();
				this.m_LineBuffer.UpdateColors();
				this.m_LineBuffer.enabled = active;
			}
			if (this.m_PolylineBuffer != null)
			{
				this.m_PolylineBuffer.UpdatePositions();
				this.m_PolylineBuffer.UpdateWidths();
				this.m_PolylineBuffer.UpdateUVs();
				this.m_PolylineBuffer.UpdateColors();
				this.m_PolylineBuffer.enabled = active;
			}
			if (this.m_ConfettiBuffer != null)
			{
				this.m_ConfettiBuffer.UpdatePositions();
				this.m_ConfettiBuffer.UpdateSizes();
				this.m_ConfettiBuffer.UpdateRots();
				this.m_ConfettiBuffer.UpdateUVs();
				this.m_ConfettiBuffer.UpdateColors();
				this.m_ConfettiBuffer.enabled = active;
			}
			if (this.m_RibbonBuffer != null)
			{
				this.m_RibbonBuffer.UpdatePositions();
				this.m_RibbonBuffer.UpdateWidths();
				this.m_RibbonBuffer.UpdateUVs();
				this.m_RibbonBuffer.UpdateColors();
				this.m_RibbonBuffer.enabled = active;
			}
			if (this.m_TailBuffer != null)
			{
				this.m_TailBuffer.UpdatePositions();
				this.m_TailBuffer.UpdateWidths();
				this.m_TailBuffer.UpdateUVs();
				this.m_TailBuffer.UpdateColors();
				this.m_TailBuffer.enabled = active;
			}
			if ((this.m_RenderSettingsFixed == null || (this.m_RenderSettingsFixed != null && this.m_RenderSettingsFixed.m_EffectRenderer == null)) && this.m_EffectRenderer != null)
			{
				this.m_EffectRenderer.enabled = active;
			}
			if (this.m_TailRenderer != null)
			{
				this.m_TailRenderer.enabled = active;
			}
		}

		// Token: 0x06005090 RID: 20624 RVA: 0x00167B3C File Offset: 0x00165F3C
		private void ClearUnit(ParticleUnit unit)
		{
			int index = unit.m_Index;
			switch (unit.m_particleType)
			{
			case eParticleType.eParticleType_Billboard:
				this.m_BillboardBuffer.positions[index] = Vector3.zero;
				this.m_BillboardBuffer.rots[index] = 0f;
				this.m_BillboardBuffer.sizes[index].Set(0f, 0f);
				this.m_BillboardBuffer.colors[index] = Color.clear;
				break;
			case eParticleType.eParticleType_Point:
				this.m_PointBuffer.positions[index] = Vector3.zero;
				this.m_PointBuffer.sizes[index] = 0f;
				this.m_PointBuffer.colors[index] = Color.clear;
				break;
			case eParticleType.eParticleType_Line:
			{
				PrimLineBuffer.LineBuffer.Line line = this.m_LineBuffer.GetLine(index);
				for (int i = 0; i < (int)(unit.m_particleTypeParam.m_line.m_jointNum + 2); i++)
				{
					line.SetPosition(Vector3.zero, i);
					line.SetWidth(0f, i);
					line.SetColor(Color.clear, i);
				}
				break;
			}
			case eParticleType.eParticleType_PolyLine:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line2 = this.m_PolylineBuffer.GetLine(index);
				for (int j = 0; j < (int)(unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); j++)
				{
					line2.SetPosition(Vector3.zero, j);
					line2.SetWidth(0f, j);
					line2.SetColor(Color.clear, j);
				}
				break;
			}
			case eParticleType.eParticleType_Confetti:
				this.m_ConfettiBuffer.rots[index] = Vector3.zero;
				this.m_ConfettiBuffer.sizes[index].Set(0f, 0f);
				this.m_ConfettiBuffer.colors[index] = Color.clear;
				break;
			case eParticleType.eParticleType_Ribbon:
			{
				PrimPolyLineBuffer.PolyLineBuffer.Line line3 = this.m_RibbonBuffer.GetLine(index);
				for (int k = 0; k < (int)(unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); k++)
				{
					line3.SetPosition(Vector3.zero, k);
					line3.SetWidth(0f, k);
					line3.SetColor(Color.clear, k);
				}
				break;
			}
			}
		}

		// Token: 0x06005091 RID: 20625 RVA: 0x00167DB0 File Offset: 0x001661B0
		public override int GetPropertyNum()
		{
			return this.m_Rule.GetPropertyNum();
		}

		// Token: 0x06005092 RID: 20626 RVA: 0x00167DBD File Offset: 0x001661BD
		public override int GetArrayNum(int propertyIdx)
		{
			return this.m_Rule.GetArrayNum(propertyIdx);
		}

		// Token: 0x06005093 RID: 20627 RVA: 0x00167DCB File Offset: 0x001661CB
		public override object GetValue(int propertyIdx, int arrayIdx)
		{
			return this.m_Rule.GetValue(propertyIdx, arrayIdx);
		}

		// Token: 0x06005094 RID: 20628 RVA: 0x00167DDA File Offset: 0x001661DA
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
		{
			this.m_Rule.SetValue(propertyIdx, arrayIdx, tgtIdx, value);
		}

		// Token: 0x06005095 RID: 20629 RVA: 0x00167DEC File Offset: 0x001661EC
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value)
		{
			this.m_Rule.SetValue(propertyIdx, arrayIdx, tgtIdx, componentIdx, value);
		}

		// Token: 0x06005096 RID: 20630 RVA: 0x00167E00 File Offset: 0x00166200
		public override eEffectAnimTypeCode GetTypeCode(int propertyIdx)
		{
			return this.m_Rule.GetTypeCode(propertyIdx);
		}

		// Token: 0x06005097 RID: 20631 RVA: 0x00167E0E File Offset: 0x0016620E
		public override Type GetPropertType(int propertyIdx)
		{
			return this.m_Rule.GetType(propertyIdx);
		}

		// Token: 0x06005098 RID: 20632 RVA: 0x00167E1C File Offset: 0x0016621C
		public override string GetPropertyName(int propertyIdx)
		{
			return this.m_Rule.GetPropertyName(propertyIdx);
		}

		// Token: 0x04004EB7 RID: 20151
		private Transform m_thisTransform;

		// Token: 0x04004EB8 RID: 20152
		private Matrix4x4 m_Matrix;

		// Token: 0x04004EB9 RID: 20153
		private Matrix4x4 m_OldMatrix;

		// Token: 0x04004EBA RID: 20154
		private bool m_isMatrixDirty;

		// Token: 0x04004EBB RID: 20155
		private Vector3[] m_WorkPaths = new Vector3[4];

		// Token: 0x04004EBC RID: 20156
		public MeigeParticleEmitter.RenderSettings m_RenderSettings;

		// Token: 0x04004EBD RID: 20157
		private MeigeParticleEmitter.RenderSettings m_RenderSettingsFixed;

		// Token: 0x04004EBE RID: 20158
		private GameObject m_RenderObject;

		// Token: 0x04004EBF RID: 20159
		private bool m_RenderObjectIsSelf;

		// Token: 0x04004EC0 RID: 20160
		private PrimBillboardBuffer m_PrimBillboardBuffer;

		// Token: 0x04004EC1 RID: 20161
		private PrimPointBuffer m_PrimPointBuffer;

		// Token: 0x04004EC2 RID: 20162
		private PrimLineBuffer m_PrimLineBuffer;

		// Token: 0x04004EC3 RID: 20163
		private PrimPolyLineBuffer m_PrimPolylineBuffer;

		// Token: 0x04004EC4 RID: 20164
		private PrimConfettiBuffer m_PrimConfettiBuffer;

		// Token: 0x04004EC5 RID: 20165
		private PrimPolyLineBuffer m_PrimRibbonBuffer;

		// Token: 0x04004EC6 RID: 20166
		private PrimBillboardBuffer.BillboardBuffer m_BillboardBuffer;

		// Token: 0x04004EC7 RID: 20167
		private PrimPointBuffer.PointBuffer m_PointBuffer;

		// Token: 0x04004EC8 RID: 20168
		private PrimLineBuffer.LineBuffer m_LineBuffer;

		// Token: 0x04004EC9 RID: 20169
		private PrimPolyLineBuffer.PolyLineBuffer m_PolylineBuffer;

		// Token: 0x04004ECA RID: 20170
		private PrimConfettiBuffer.ConfettiBuffer m_ConfettiBuffer;

		// Token: 0x04004ECB RID: 20171
		private PrimPolyLineBuffer.PolyLineBuffer m_RibbonBuffer;

		// Token: 0x04004ECC RID: 20172
		private EffectRenderer m_EffectRenderer;

		// Token: 0x04004ECD RID: 20173
		private GameObject m_TailRenderObject;

		// Token: 0x04004ECE RID: 20174
		private PrimPolyLineBuffer m_PrimTailBuffer;

		// Token: 0x04004ECF RID: 20175
		private PrimPolyLineBuffer.PolyLineBuffer m_TailBuffer;

		// Token: 0x04004ED0 RID: 20176
		private EffectRenderer m_TailRenderer;

		// Token: 0x04004ED1 RID: 20177
		private List<ParticleUnit> m_ActiveUnitArray = new List<ParticleUnit>();

		// Token: 0x04004ED2 RID: 20178
		private List<ParticleUnit> m_InActiveUnitArray = new List<ParticleUnit>();

		// Token: 0x04004ED3 RID: 20179
		[SerializeField]
		private ParticleRule m_Rule;

		// Token: 0x04004ED4 RID: 20180
		[SerializeField]
		public Material m_Material;

		// Token: 0x04004ED5 RID: 20181
		private float m_Time;

		// Token: 0x04004ED6 RID: 20182
		private float m_FrameRate;

		// Token: 0x04004ED7 RID: 20183
		private int m_SortingOrder;

		// Token: 0x04004ED8 RID: 20184
		private int m_Layer;

		// Token: 0x04004ED9 RID: 20185
		private bool m_isInitialized;

		// Token: 0x04004EDA RID: 20186
		public Vector3 m_offsetRot = Vector3.zero;

		// Token: 0x04004EDB RID: 20187
		private MeigeParticleEmitter.History m_History;

		// Token: 0x04004EDC RID: 20188
		private bool m_isLocalTransHistory;

		// Token: 0x04004EDD RID: 20189
		private MeigeParticleEmitter.CalcFuncList m_AttachFuncList;

		// Token: 0x04004EDE RID: 20190
		private MeigeParticleEmitter.CalcFuncList m_CalcFuncList;

		// Token: 0x04004EDF RID: 20191
		private MeigeParticleEmitter.HookUpdate m_UpdateHook_Shot;

		// Token: 0x04004EE0 RID: 20192
		private MeigeParticleEmitter.HookUpdate m_UpdateHook_Before;

		// Token: 0x04004EE1 RID: 20193
		private MeigeParticleEmitter.HookUpdate m_UpdateHook_After;

		// Token: 0x02000F0F RID: 3855
		[Serializable]
		public class RenderSettings
		{
			// Token: 0x04004EE2 RID: 20194
			public EffectMeshBuffer m_MeshBufferPrimBillboard;

			// Token: 0x04004EE3 RID: 20195
			public EffectMeshBuffer m_MeshBufferPrimPoint;

			// Token: 0x04004EE4 RID: 20196
			public EffectMeshBuffer m_MeshBufferPrimLine;

			// Token: 0x04004EE5 RID: 20197
			public EffectMeshBuffer m_MeshBufferPrimPolyline;

			// Token: 0x04004EE6 RID: 20198
			public EffectMeshBuffer m_MeshBufferPrimConfetti;

			// Token: 0x04004EE7 RID: 20199
			public EffectMeshBuffer m_MeshBufferPrimRibbon;

			// Token: 0x04004EE8 RID: 20200
			public EffectMeshBuffer m_MeshBufferPrimTail;

			// Token: 0x04004EE9 RID: 20201
			public EffectRenderer m_EffectRenderer;

			// Token: 0x04004EEA RID: 20202
			public EffectRenderer m_TailRenderer;
		}

		// Token: 0x02000F10 RID: 3856
		private struct History
		{
			// Token: 0x0600509A RID: 20634 RVA: 0x00167E34 File Offset: 0x00166234
			public bool IsEqual(ParticleRule ptclRule)
			{
				return ptclRule.m_particleType == this.m_particleType && ptclRule.m_particleNum == this.m_particleNum && ptclRule.m_UsingAccelerationFlg == this.m_UsingAccelerationFlg && ptclRule.m_UsingRotationFlg == this.m_UsingRotationFlg && ptclRule.m_UsingUVAnimationFlg == this.m_UsingUVAnimationFlg && ptclRule.m_UsingColorCurveFlg == this.m_UsingColorCurveFlg && ptclRule.m_UsingTailFlg == this.m_UsingTailFlg && ptclRule.m_UsingBlinkFlg == this.m_UsingBlinkFlg && ptclRule.m_UsingPathMoveFlg == this.m_UsingPathMoveFlg;
			}

			// Token: 0x04004EEB RID: 20203
			public eParticleType m_particleType;

			// Token: 0x04004EEC RID: 20204
			public int m_particleNum;

			// Token: 0x04004EED RID: 20205
			public bool m_UsingAccelerationFlg;

			// Token: 0x04004EEE RID: 20206
			public bool m_UsingRotationFlg;

			// Token: 0x04004EEF RID: 20207
			public bool m_UsingUVAnimationFlg;

			// Token: 0x04004EF0 RID: 20208
			public bool m_UsingColorCurveFlg;

			// Token: 0x04004EF1 RID: 20209
			public bool m_UsingTailFlg;

			// Token: 0x04004EF2 RID: 20210
			public bool m_UsingBlinkFlg;

			// Token: 0x04004EF3 RID: 20211
			public bool m_UsingPathMoveFlg;
		}

		// Token: 0x02000F11 RID: 3857
		// (Invoke) Token: 0x0600509C RID: 20636
		private delegate void CalcFuncList(ParticleUnit unit);

		// Token: 0x02000F12 RID: 3858
		// (Invoke) Token: 0x060050A0 RID: 20640
		public delegate void HookUpdate();
	}
}
