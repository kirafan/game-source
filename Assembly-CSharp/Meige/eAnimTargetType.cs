﻿using System;

namespace Meige
{
	// Token: 0x02000F95 RID: 3989
	public enum eAnimTargetType
	{
		// Token: 0x04005384 RID: 21380
		eAnimTarget_Invalid = -1,
		// Token: 0x04005385 RID: 21381
		eAnimTarget_JntTra,
		// Token: 0x04005386 RID: 21382
		eAnimTarget_JntRot,
		// Token: 0x04005387 RID: 21383
		eAnimTarget_JntScl,
		// Token: 0x04005388 RID: 21384
		eAnimTarget_MatColor,
		// Token: 0x04005389 RID: 21385
		eAnimTarget_TexCoverageUV,
		// Token: 0x0400538A RID: 21386
		eAnimTarget_TexTranslationUV,
		// Token: 0x0400538B RID: 21387
		eAnimTarget_TexRotateUV,
		// Token: 0x0400538C RID: 21388
		eAnimTarget_TexOffsetUV,
		// Token: 0x0400538D RID: 21389
		eAnimTarget_FocalLength,
		// Token: 0x0400538E RID: 21390
		eAnimTarget_MeshVisibility,
		// Token: 0x0400538F RID: 21391
		eAnimTarget_ADAnimBlend,
		// Token: 0x04005390 RID: 21392
		eAnimTarget_PEActive,
		// Token: 0x04005391 RID: 21393
		eAnimTarget_MeshColor,
		// Token: 0x04005392 RID: 21394
		eAnimTarget_SignalCtrl,
		// Token: 0x04005393 RID: 21395
		eAnimTarget_JntTra_X,
		// Token: 0x04005394 RID: 21396
		eAnimTarget_JntTra_Y,
		// Token: 0x04005395 RID: 21397
		eAnimTarget_JntTra_Z,
		// Token: 0x04005396 RID: 21398
		eAnimTarget_JntRot_X,
		// Token: 0x04005397 RID: 21399
		eAnimTarget_JntRot_Y,
		// Token: 0x04005398 RID: 21400
		eAnimTarget_JntRot_Z,
		// Token: 0x04005399 RID: 21401
		eAnimTarget_JntScl_X,
		// Token: 0x0400539A RID: 21402
		eAnimTarget_JntScl_Y,
		// Token: 0x0400539B RID: 21403
		eAnimTarget_JntScl_Z,
		// Token: 0x0400539C RID: 21404
		eAnimTarget_MatColor_R,
		// Token: 0x0400539D RID: 21405
		eAnimTarget_MatColor_G,
		// Token: 0x0400539E RID: 21406
		eAnimTarget_MatColor_B,
		// Token: 0x0400539F RID: 21407
		eAnimTarget_MatColor_A,
		// Token: 0x040053A0 RID: 21408
		eAnimTarget_TexCoverageUV_U,
		// Token: 0x040053A1 RID: 21409
		eAnimTarget_TexCoverageUV_V,
		// Token: 0x040053A2 RID: 21410
		eAnimTarget_TexTranslationUV_U,
		// Token: 0x040053A3 RID: 21411
		eAnimTarget_TexTranslationUV_V,
		// Token: 0x040053A4 RID: 21412
		eAnimTarget_TexOffsetUV_U,
		// Token: 0x040053A5 RID: 21413
		eAnimTarget_TexOffsetUV_V,
		// Token: 0x040053A6 RID: 21414
		eAnimTarget_MeshColor_R,
		// Token: 0x040053A7 RID: 21415
		eAnimTarget_MeshColor_G,
		// Token: 0x040053A8 RID: 21416
		eAnimTarget_MeshColor_B,
		// Token: 0x040053A9 RID: 21417
		eAnimTarget_MeshColor_A,
		// Token: 0x040053AA RID: 21418
		eAnimTarget_CamEnableCorrectToneMap,
		// Token: 0x040053AB RID: 21419
		eAnimTarget_CamExposureValue,
		// Token: 0x040053AC RID: 21420
		eAnimTarget_CamToneMapWhitePointType,
		// Token: 0x040053AD RID: 21421
		eAnimTarget_CamToneMapWhitePointRangeMin,
		// Token: 0x040053AE RID: 21422
		eAnimTarget_CamToneMapWhitePointRangeMax,
		// Token: 0x040053AF RID: 21423
		eAnimTarget_CamEnableBloom,
		// Token: 0x040053B0 RID: 21424
		eAnimTarget_CamBloomReferenceType,
		// Token: 0x040053B1 RID: 21425
		eAnimTarget_CamBloomReferenceThresholdOffset,
		// Token: 0x040053B2 RID: 21426
		eAnimTarget_CamBloomOffset,
		// Token: 0x040053B3 RID: 21427
		eAnimTarget_CamEnableCorrectContrast,
		// Token: 0x040053B4 RID: 21428
		eAnimTarget_CamContrastLevel,
		// Token: 0x040053B5 RID: 21429
		eAnimTarget_CamEnableCorrectBrightness,
		// Token: 0x040053B6 RID: 21430
		eAnimTarget_CamBrightnessLevel,
		// Token: 0x040053B7 RID: 21431
		eAnimTarget_CamEnableCorrectChroma,
		// Token: 0x040053B8 RID: 21432
		eAnimTarget_CamChromaLevel,
		// Token: 0x040053B9 RID: 21433
		eAnimTarget_CamEnableCorrectColorBlend,
		// Token: 0x040053BA RID: 21434
		eAnimTarget_CamColorBlendColor,
		// Token: 0x040053BB RID: 21435
		eAnimTarget_CamColorBlendColor_R,
		// Token: 0x040053BC RID: 21436
		eAnimTarget_CamColorBlendColor_G,
		// Token: 0x040053BD RID: 21437
		eAnimTarget_CamColorBlendColor_B,
		// Token: 0x040053BE RID: 21438
		eAnimTarget_CamColorBlendLevel,
		// Token: 0x040053BF RID: 21439
		eAnimTarget_CamAdaptationRateSpeed,
		// Token: 0x040053C0 RID: 21440
		eAnimTarget_CamToneMapWhitePointScale,
		// Token: 0x040053C1 RID: 21441
		eAnimTarget_MeshHDRFactor,
		// Token: 0x040053C2 RID: 21442
		eAnimTarget_CamOrthographicSize
	}
}
