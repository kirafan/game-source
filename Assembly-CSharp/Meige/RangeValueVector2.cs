﻿using System;

namespace Meige
{
	// Token: 0x02000EFC RID: 3836
	[Serializable]
	public class RangeValueVector2 : RangeValue<Float2>
	{
		// Token: 0x06004F1A RID: 20250 RVA: 0x0015DCE9 File Offset: 0x0015C0E9
		public RangeValueVector2()
		{
		}

		// Token: 0x06004F1B RID: 20251 RVA: 0x0015DCF1 File Offset: 0x0015C0F1
		public RangeValueVector2(int num) : base(num)
		{
		}

		// Token: 0x06004F1C RID: 20252 RVA: 0x0015DCFA File Offset: 0x0015C0FA
		public RangeValueVector2(RangeValueVector2 v) : base(v)
		{
		}

		// Token: 0x06004F1D RID: 20253 RVA: 0x0015DD04 File Offset: 0x0015C104
		public new RangeValueVector2 Clone()
		{
			return new RangeValueVector2(this);
		}
	}
}
