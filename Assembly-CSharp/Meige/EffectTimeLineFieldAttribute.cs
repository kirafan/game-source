﻿using System;

namespace Meige
{
	// Token: 0x02000EF3 RID: 3827
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class EffectTimeLineFieldAttribute : Attribute
	{
		// Token: 0x06004EDC RID: 20188 RVA: 0x0015D41D File Offset: 0x0015B81D
		public EffectTimeLineFieldAttribute(string displayName, TypeCode typeCode)
		{
			this.m_DisplayName = displayName;
			this.m_TypeCode = typeCode;
		}

		// Token: 0x04004E95 RID: 20117
		public string m_DisplayName;

		// Token: 0x04004E96 RID: 20118
		public TypeCode m_TypeCode;
	}
}
