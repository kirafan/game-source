﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F50 RID: 3920
	public class PrimPolyLineBuffer : MonoBehaviour
	{
		// Token: 0x06005166 RID: 20838 RVA: 0x0016C633 File Offset: 0x0016AA33
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x06005167 RID: 20839 RVA: 0x0016C65C File Offset: 0x0016AA5C
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer.RemoveHook_OnWillRender(new EffectMeshBuffer.HookUpdate(this.UpdateMesh));
				IEnumerator enumerator = this.m_PolyLineBufferList.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						PrimPolyLineBuffer.PolyLineBuffer polyLineBuffer = (PrimPolyLineBuffer.PolyLineBuffer)obj;
						this.m_EffectMeshBuffer.RemoveBuffer(polyLineBuffer.meshBuffer);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x06005168 RID: 20840 RVA: 0x0016C708 File Offset: 0x0016AB08
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x06005169 RID: 20841 RVA: 0x0016C710 File Offset: 0x0016AB10
		internal void AddActiveBufferList(PrimPolyLineBuffer.PolyLineBuffer meshBuffer)
		{
			this.m_activePolyLineBufferList.Add(meshBuffer);
		}

		// Token: 0x0600516A RID: 20842 RVA: 0x0016C71F File Offset: 0x0016AB1F
		internal void RemoveActiveBufferList(PrimPolyLineBuffer.PolyLineBuffer meshBuffer)
		{
			this.m_activePolyLineBufferList.Remove(meshBuffer);
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x0600516C RID: 20844 RVA: 0x0016C736 File Offset: 0x0016AB36
		// (set) Token: 0x0600516B RID: 20843 RVA: 0x0016C72D File Offset: 0x0016AB2D
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x0600516E RID: 20846 RVA: 0x0016C747 File Offset: 0x0016AB47
		// (set) Token: 0x0600516D RID: 20845 RVA: 0x0016C73E File Offset: 0x0016AB3E
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x06005170 RID: 20848 RVA: 0x0016C758 File Offset: 0x0016AB58
		// (set) Token: 0x0600516F RID: 20847 RVA: 0x0016C74F File Offset: 0x0016AB4F
		public bool bufferChangeFlg { get; set; }

		// Token: 0x06005171 RID: 20849 RVA: 0x0016C760 File Offset: 0x0016AB60
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x06005172 RID: 20850 RVA: 0x0016C768 File Offset: 0x0016AB68
		private void Setup()
		{
			if (this.m_SetupIsEnd)
			{
				return;
			}
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
				this.m_MeshBufferIsSelf = true;
			}
			if (this.m_EffectMeshBuffer != null)
			{
				this.m_EffectMeshBuffer.AddHook_OnWillRender(new EffectMeshBuffer.HookUpdate(this.UpdateMesh));
			}
			this.m_SetupIsEnd = true;
		}

		// Token: 0x06005173 RID: 20851 RVA: 0x0016C7F0 File Offset: 0x0016ABF0
		public virtual PrimPolyLineBuffer.PolyLineBuffer AddBuffer(int lineNum, int pointNum)
		{
			this.Setup();
			PrimPolyLineBuffer.PolyLineBuffer polyLineBuffer = new PrimPolyLineBuffer.PolyLineBuffer();
			polyLineBuffer.lines = new PrimPolyLineBuffer.PolyLineBuffer.Line[lineNum];
			polyLineBuffer.positions = new Vector3[lineNum * pointNum];
			polyLineBuffer.widths = new float[lineNum * pointNum];
			polyLineBuffer.UVs = new Vector2[lineNum * pointNum];
			polyLineBuffer.UWidths = new float[lineNum * pointNum];
			polyLineBuffer.colors = new Color[lineNum * pointNum];
			polyLineBuffer.lineNum = lineNum;
			polyLineBuffer.pointNum = pointNum;
			polyLineBuffer.parent = this;
			polyLineBuffer.enabled = false;
			this.m_PolyLineBufferList.Add(polyLineBuffer);
			this.bufferChangeFlg = true;
			polyLineBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(lineNum * pointNum * 2, lineNum * ((pointNum - 1) * 4));
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < lineNum; i++)
			{
				polyLineBuffer.lines[i] = new PrimPolyLineBuffer.PolyLineBuffer.Line();
				polyLineBuffer.lines[i].parent = polyLineBuffer;
				polyLineBuffer.lines[i].topPointIdx = num2;
				for (int j = 0; j < pointNum - 1; j++)
				{
					polyLineBuffer.meshBuffer.indices[num++] = num2 * 2;
					polyLineBuffer.meshBuffer.indices[num++] = num2 * 2 + 1;
					polyLineBuffer.meshBuffer.indices[num++] = num2 * 2 + 3;
					polyLineBuffer.meshBuffer.indices[num++] = num2 * 2 + 2;
					num2++;
				}
				num2++;
			}
			polyLineBuffer.UpdatePositions();
			polyLineBuffer.UpdateWidths();
			polyLineBuffer.UpdateUVs();
			polyLineBuffer.UpdateColors();
			polyLineBuffer.meshBuffer.UpdateIndices();
			return polyLineBuffer;
		}

		// Token: 0x06005174 RID: 20852 RVA: 0x0016C984 File Offset: 0x0016AD84
		public virtual void RemoveBuffer(PrimPolyLineBuffer.PolyLineBuffer lineBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			if (lineBuffer == null)
			{
				return;
			}
			lineBuffer.enabled = false;
			int num = this.m_PolyLineBufferList.IndexOf(lineBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(lineBuffer.meshBuffer);
				lineBuffer.meshBuffer = null;
				this.m_PolyLineBufferList.RemoveAt(num);
			}
		}

		// Token: 0x06005175 RID: 20853 RVA: 0x0016C9EC File Offset: 0x0016ADEC
		private void UpdateMesh()
		{
			if (this.m_PolyLineBufferList.Count == 0)
			{
				return;
			}
			Camera current = Camera.current;
			if (current == null)
			{
				return;
			}
			Vector4 row = current.worldToCameraMatrix.GetRow(2);
			Vector3 vector = new Vector3(row.x, row.y, row.z);
			for (int i = 0; i < this.m_PolyLineBufferList.Count; i++)
			{
				PrimPolyLineBuffer.PolyLineBuffer polyLineBuffer = this.m_PolyLineBufferList[i] as PrimPolyLineBuffer.PolyLineBuffer;
				if (polyLineBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = polyLineBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (polyLineBuffer.enabled)
						{
							for (int j = 0; j < polyLineBuffer.lineNum; j++)
							{
								PrimPolyLineBuffer.PolyLineBuffer.Line line = polyLineBuffer.GetLine(j);
								Vector3 rhs;
								switch (line.outerElement)
								{
								default:
									rhs = vector;
									break;
								case PrimPolyLineBuffer.eOuterElement.X:
									rhs = Vector3.right;
									break;
								case PrimPolyLineBuffer.eOuterElement.Y:
									rhs = Vector3.up;
									break;
								case PrimPolyLineBuffer.eOuterElement.Z:
									rhs = Vector3.forward;
									break;
								}
								if (line.isDirtyPositions)
								{
									for (int k = 0; k < polyLineBuffer.pointNum; k++)
									{
										Vector3 vector2;
										if (k < polyLineBuffer.pointNum - 1)
										{
											vector2 = line.GetPosition(k) - line.GetPosition(k + 1);
										}
										else
										{
											vector2 = line.GetPosition(k - 1) - line.GetPosition(k);
										}
										vector2 = Vector3.Cross(vector2, rhs);
										vector2 = Vector3.Normalize(vector2);
										Vector3 b = vector2 * line.GetWidth(k) / 2f;
										meshBuffer.vertices[(j * polyLineBuffer.pointNum + k) * 2] = line.GetPosition(k) + b;
										meshBuffer.vertices[(j * polyLineBuffer.pointNum + k) * 2 + 1] = line.GetPosition(k) - b;
									}
									line.isDirtyPositions = false;
									meshBuffer.UpdateVertices();
								}
								if (line.isDirtyUVs)
								{
									if (polyLineBuffer.m_UVReverse)
									{
										for (int l = 0; l < polyLineBuffer.pointNum; l++)
										{
											meshBuffer.UVs[(j * polyLineBuffer.pointNum + l) * 2] = line.GetUV(l);
											meshBuffer.UVs[(j * polyLineBuffer.pointNum + l) * 2 + 1] = line.GetUV(l) + Vector2.right * line.GetUWidth(l);
										}
									}
									else
									{
										for (int m = 0; m < polyLineBuffer.pointNum; m++)
										{
											meshBuffer.UVs[(j * polyLineBuffer.pointNum + m) * 2] = line.GetUV(m);
											meshBuffer.UVs[(j * polyLineBuffer.pointNum + m) * 2 + 1] = line.GetUV(m) + Vector2.up * line.GetUWidth(m);
										}
									}
									line.isDirtyUVs = false;
									meshBuffer.UpdateUVs();
								}
								if (line.isDirtyColors)
								{
									for (int n = 0; n < polyLineBuffer.pointNum; n++)
									{
										meshBuffer.colors[(j * polyLineBuffer.pointNum + n) * 2] = line.GetColor(n);
										meshBuffer.colors[(j * polyLineBuffer.pointNum + n) * 2 + 1] = line.GetColor(n);
									}
									line.isDirtyColors = false;
									meshBuffer.UpdateColors();
								}
							}
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
		}

		// Token: 0x06005176 RID: 20854 RVA: 0x0016CE14 File Offset: 0x0016B214
		private void UpdateAlways()
		{
			int num = 0;
			for (int i = 0; i < this.m_PolyLineBufferList.Count; i++)
			{
				PrimPolyLineBuffer.PolyLineBuffer polyLineBuffer = this.m_PolyLineBufferList[i] as PrimPolyLineBuffer.PolyLineBuffer;
				if (polyLineBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = polyLineBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (polyLineBuffer.enabled)
						{
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x06005177 RID: 20855 RVA: 0x0016CE9D File Offset: 0x0016B29D
		private void Update()
		{
			this.UpdateAlways();
		}

		// Token: 0x06005178 RID: 20856 RVA: 0x0016CEA5 File Offset: 0x0016B2A5
		private void OnWillRenderObject()
		{
		}

		// Token: 0x04005077 RID: 20599
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04005078 RID: 20600
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04005079 RID: 20601
		protected bool m_SetupIsEnd;

		// Token: 0x0400507A RID: 20602
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x0400507B RID: 20603
		protected ArrayList m_PolyLineBufferList = new ArrayList();

		// Token: 0x0400507C RID: 20604
		protected ArrayList m_activePolyLineBufferList = new ArrayList(32);

		// Token: 0x02000F51 RID: 3921
		public enum eOuterElement
		{
			// Token: 0x04005080 RID: 20608
			Eye,
			// Token: 0x04005081 RID: 20609
			X,
			// Token: 0x04005082 RID: 20610
			Y,
			// Token: 0x04005083 RID: 20611
			Z
		}

		// Token: 0x02000F52 RID: 3922
		public class PolyLineBuffer
		{
			// Token: 0x0600517A RID: 20858 RVA: 0x0016CEAF File Offset: 0x0016B2AF
			public PrimPolyLineBuffer.PolyLineBuffer.Line GetLine(int lineIdx)
			{
				return this.lines[lineIdx];
			}

			// Token: 0x0600517B RID: 20859 RVA: 0x0016CEB9 File Offset: 0x0016B2B9
			public void Remove()
			{
			}

			// Token: 0x0600517C RID: 20860 RVA: 0x0016CEBC File Offset: 0x0016B2BC
			public void UpdatePositions()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyPositions = true;
				}
			}

			// Token: 0x0600517D RID: 20861 RVA: 0x0016CEF0 File Offset: 0x0016B2F0
			public void UpdateWidths()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyWidths = true;
				}
			}

			// Token: 0x0600517E RID: 20862 RVA: 0x0016CF24 File Offset: 0x0016B324
			public void UpdateUVs()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyUVs = true;
				}
			}

			// Token: 0x0600517F RID: 20863 RVA: 0x0016CF58 File Offset: 0x0016B358
			public void UpdateColors()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyColors = true;
				}
			}

			// Token: 0x06005180 RID: 20864 RVA: 0x0016CF8A File Offset: 0x0016B38A
			public void Minimize()
			{
			}

			// Token: 0x17000576 RID: 1398
			// (get) Token: 0x06005181 RID: 20865 RVA: 0x0016CF8C File Offset: 0x0016B38C
			// (set) Token: 0x06005182 RID: 20866 RVA: 0x0016CF94 File Offset: 0x0016B394
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateWidths();
					this.UpdateColors();
				}
			}

			// Token: 0x04005084 RID: 20612
			internal PrimPolyLineBuffer parent;

			// Token: 0x04005085 RID: 20613
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04005086 RID: 20614
			public bool m_UVReverse;

			// Token: 0x04005087 RID: 20615
			internal PrimPolyLineBuffer.PolyLineBuffer.Line[] lines;

			// Token: 0x04005088 RID: 20616
			internal Vector3[] positions;

			// Token: 0x04005089 RID: 20617
			internal float[] widths;

			// Token: 0x0400508A RID: 20618
			internal Vector2[] UVs;

			// Token: 0x0400508B RID: 20619
			internal float[] UWidths;

			// Token: 0x0400508C RID: 20620
			internal Color[] colors;

			// Token: 0x0400508D RID: 20621
			internal int lineNum;

			// Token: 0x0400508E RID: 20622
			internal int pointNum;

			// Token: 0x0400508F RID: 20623
			private bool m_enabled;

			// Token: 0x02000F53 RID: 3923
			public class Line
			{
				// Token: 0x06005184 RID: 20868 RVA: 0x0016D00F File Offset: 0x0016B40F
				public void SetPosition(Vector3 pos, int pointIdx)
				{
					this.parent.positions[this.topPointIdx + pointIdx] = pos;
					this.isDirtyPositions = true;
				}

				// Token: 0x06005185 RID: 20869 RVA: 0x0016D036 File Offset: 0x0016B436
				public Vector3 GetPosition(int pointIdx)
				{
					return this.parent.positions[this.topPointIdx + pointIdx];
				}

				// Token: 0x06005186 RID: 20870 RVA: 0x0016D055 File Offset: 0x0016B455
				public void SetWidth(float width, int pointIdx)
				{
					this.parent.widths[this.topPointIdx + pointIdx] = width;
					this.isDirtyWidths = true;
				}

				// Token: 0x06005187 RID: 20871 RVA: 0x0016D073 File Offset: 0x0016B473
				public float GetWidth(int pointIdx)
				{
					return this.parent.widths[this.topPointIdx + pointIdx];
				}

				// Token: 0x06005188 RID: 20872 RVA: 0x0016D089 File Offset: 0x0016B489
				public void SetUV(Vector2 uv, float uWidth, int pointIdx)
				{
					this.parent.UVs[this.topPointIdx + pointIdx] = uv;
					this.parent.UWidths[this.topPointIdx + pointIdx] = uWidth;
					this.isDirtyUVs = true;
				}

				// Token: 0x06005189 RID: 20873 RVA: 0x0016D0C5 File Offset: 0x0016B4C5
				public Vector2 GetUV(int pointIdx)
				{
					return this.parent.UVs[this.topPointIdx + pointIdx];
				}

				// Token: 0x0600518A RID: 20874 RVA: 0x0016D0E4 File Offset: 0x0016B4E4
				public float GetUWidth(int pointIdx)
				{
					return this.parent.UWidths[this.topPointIdx + pointIdx];
				}

				// Token: 0x0600518B RID: 20875 RVA: 0x0016D0FA File Offset: 0x0016B4FA
				public void SetColor(Color color, int pointIdx)
				{
					this.parent.colors[this.topPointIdx + pointIdx] = color;
					this.isDirtyColors = true;
				}

				// Token: 0x0600518C RID: 20876 RVA: 0x0016D121 File Offset: 0x0016B521
				public Color GetColor(int pointIdx)
				{
					return this.parent.colors[this.topPointIdx + pointIdx];
				}

				// Token: 0x04005090 RID: 20624
				internal PrimPolyLineBuffer.PolyLineBuffer parent;

				// Token: 0x04005091 RID: 20625
				public int topPointIdx;

				// Token: 0x04005092 RID: 20626
				internal bool isDirtyPositions;

				// Token: 0x04005093 RID: 20627
				internal bool isDirtyWidths;

				// Token: 0x04005094 RID: 20628
				internal bool isDirtyUVs;

				// Token: 0x04005095 RID: 20629
				internal bool isDirtyColors;

				// Token: 0x04005096 RID: 20630
				public PrimPolyLineBuffer.eOuterElement outerElement;
			}
		}
	}
}
