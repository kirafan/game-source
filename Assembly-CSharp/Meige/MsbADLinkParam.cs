﻿using System;

namespace Meige
{
	// Token: 0x02000FB6 RID: 4022
	[Serializable]
	public class MsbADLinkParam
	{
		// Token: 0x060053B1 RID: 21425 RVA: 0x00176771 File Offset: 0x00174B71
		public MsbADLinkParam()
		{
		}

		// Token: 0x060053B2 RID: 21426 RVA: 0x0017677C File Offset: 0x00174B7C
		public MsbADLinkParam(MsbADLinkParam src)
		{
			this.m_SrcADJointIdx = src.m_SrcADJointIdx;
			this.m_DstADJointIdx = src.m_DstADJointIdx;
			this.m_Radius = src.m_Radius;
			this.m_Length = src.m_Length;
			this.m_LinkType = src.m_LinkType;
		}

		// Token: 0x04005483 RID: 21635
		public int m_SrcADJointIdx;

		// Token: 0x04005484 RID: 21636
		public int m_DstADJointIdx;

		// Token: 0x04005485 RID: 21637
		public float m_Radius;

		// Token: 0x04005486 RID: 21638
		public float m_Length;

		// Token: 0x04005487 RID: 21639
		public eADLinkType m_LinkType;
	}
}
