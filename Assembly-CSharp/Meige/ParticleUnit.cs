﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F34 RID: 3892
	[Serializable]
	public class ParticleUnit
	{
		// Token: 0x04004F97 RID: 20375
		public ParticleUnit.ParticleTypeParam m_particleTypeParam;

		// Token: 0x04004F98 RID: 20376
		public eParticleType m_particleType;

		// Token: 0x04004F99 RID: 20377
		public Vector3 m_birthPos;

		// Token: 0x04004F9A RID: 20378
		public Vector3 m_pos;

		// Token: 0x04004F9B RID: 20379
		public Vector3 m_offset;

		// Token: 0x04004F9C RID: 20380
		public float m_scale;

		// Token: 0x04004F9D RID: 20381
		public float m_lifeScaleMax;

		// Token: 0x04004F9E RID: 20382
		public Vector3 m_velocity;

		// Token: 0x04004F9F RID: 20383
		public Vector3 m_gravityForce;

		// Token: 0x04004FA0 RID: 20384
		public Rect m_uvRect;

		// Token: 0x04004FA1 RID: 20385
		public Color m_color;

		// Token: 0x04004FA2 RID: 20386
		public eParticleLifeSpanType m_lifeSpanType;

		// Token: 0x04004FA3 RID: 20387
		public float m_lifeSpanRate;

		// Token: 0x04004FA4 RID: 20388
		public float[] m_lifeSpanParam = new float[2];

		// Token: 0x04004FA5 RID: 20389
		public float m_lifeSpanFadeAlphaRate;

		// Token: 0x04004FA6 RID: 20390
		public int m_aliveCount;

		// Token: 0x04004FA7 RID: 20391
		public bool m_initializedFlg;

		// Token: 0x04004FA8 RID: 20392
		public bool m_activeFlg;

		// Token: 0x04004FA9 RID: 20393
		public bool m_isDirty;

		// Token: 0x04004FAA RID: 20394
		public bool m_isLocal;

		// Token: 0x04004FAB RID: 20395
		public int m_Index;

		// Token: 0x04004FAC RID: 20396
		public ParticleUnit.AccelerationComponent m_AccelerationComponent;

		// Token: 0x04004FAD RID: 20397
		public ParticleUnit.RotationComponent m_RotationComponent;

		// Token: 0x04004FAE RID: 20398
		public ParticleUnit.UVAnimationComponent m_UVAnimationComponent;

		// Token: 0x04004FAF RID: 20399
		public ParticleUnit.ColorCurveComponent m_ColorCurveComponent;

		// Token: 0x04004FB0 RID: 20400
		public ParticleUnit.TailComponent m_TailComponent;

		// Token: 0x04004FB1 RID: 20401
		public ParticleUnit.BlinkComponent m_BlinkComponent;

		// Token: 0x04004FB2 RID: 20402
		public ParticleUnit.PathMoveComponent m_PathMoveComponent;

		// Token: 0x02000F35 RID: 3893
		[Serializable]
		[StructLayout(LayoutKind.Explicit)]
		public struct ParticleTypeParam
		{
			// Token: 0x04004FB3 RID: 20403
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.Billboard m_billboard;

			// Token: 0x04004FB4 RID: 20404
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.Point m_point;

			// Token: 0x04004FB5 RID: 20405
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.Line m_line;

			// Token: 0x04004FB6 RID: 20406
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.PolyLine m_polyLine;

			// Token: 0x04004FB7 RID: 20407
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.Confetti m_confetti;

			// Token: 0x04004FB8 RID: 20408
			[FieldOffset(0)]
			public ParticleUnit.ParticleTypeParam.Ribbon m_ribbon;

			// Token: 0x02000F36 RID: 3894
			[Serializable]
			public struct Billboard
			{
				// Token: 0x04004FB9 RID: 20409
				public float m_width;

				// Token: 0x04004FBA RID: 20410
				public float m_height;
			}

			// Token: 0x02000F37 RID: 3895
			[Serializable]
			public struct Point
			{
				// Token: 0x04004FBB RID: 20411
				public float m_size;
			}

			// Token: 0x02000F38 RID: 3896
			[Serializable]
			public struct Line
			{
				// Token: 0x04004FBC RID: 20412
				public short m_jointNum;

				// Token: 0x04004FBD RID: 20413
				public short m_HistoryPointNum;

				// Token: 0x04004FBE RID: 20414
				public float m_width;

				// Token: 0x04004FBF RID: 20415
				public float m_width2;

				// Token: 0x04004FC0 RID: 20416
				public Vector3[] m_pPos;
			}

			// Token: 0x02000F39 RID: 3897
			[Serializable]
			public struct PolyLine
			{
				// Token: 0x04004FC1 RID: 20417
				public short m_jointNum;

				// Token: 0x04004FC2 RID: 20418
				public short m_HistoryPointNum;

				// Token: 0x04004FC3 RID: 20419
				public float m_topWidth;

				// Token: 0x04004FC4 RID: 20420
				public float m_endWidth;

				// Token: 0x04004FC5 RID: 20421
				public Vector3[] m_pPos;
			}

			// Token: 0x02000F3A RID: 3898
			[Serializable]
			public struct Confetti
			{
				// Token: 0x04004FC6 RID: 20422
				public float m_width;

				// Token: 0x04004FC7 RID: 20423
				public float m_height;
			}

			// Token: 0x02000F3B RID: 3899
			[Serializable]
			public struct Ribbon
			{
				// Token: 0x04004FC8 RID: 20424
				public short m_jointNum;

				// Token: 0x04004FC9 RID: 20425
				public short m_HistoryPointNum;

				// Token: 0x04004FCA RID: 20426
				public float m_topWidth;

				// Token: 0x04004FCB RID: 20427
				public float m_endWidth;

				// Token: 0x04004FCC RID: 20428
				public Vector3[] m_pPos;

				// Token: 0x04004FCD RID: 20429
				public Vector3[] m_pVelocity;
			}
		}

		// Token: 0x02000F3C RID: 3900
		[Serializable]
		public struct AccelerationComponent
		{
			// Token: 0x04004FCE RID: 20430
			public float m_acceleration;

			// Token: 0x04004FCF RID: 20431
			public float m_dragForce;
		}

		// Token: 0x02000F3D RID: 3901
		[Serializable]
		public struct RotationComponent
		{
			// Token: 0x04004FD0 RID: 20432
			public Vector3 m_rot;

			// Token: 0x04004FD1 RID: 20433
			public Vector3 m_rotAnchorOffset;

			// Token: 0x04004FD2 RID: 20434
			public Vector3 m_rotVelocity;

			// Token: 0x04004FD3 RID: 20435
			public Vector3 m_rotAcceleration;

			// Token: 0x04004FD4 RID: 20436
			public float m_rotDragForce;
		}

		// Token: 0x02000F3E RID: 3902
		[Serializable]
		public struct UVAnimationComponent
		{
			// Token: 0x04004FD5 RID: 20437
			public int m_nowBlock;

			// Token: 0x04004FD6 RID: 20438
			public float m_switchSec;

			// Token: 0x04004FD7 RID: 20439
			public float m_switchSecWork;
		}

		// Token: 0x02000F3F RID: 3903
		[Serializable]
		public struct ColorCurveComponent
		{
			// Token: 0x04004FD8 RID: 20440
			public RangeValueColor m_pColorCurve;
		}

		// Token: 0x02000F40 RID: 3904
		[Serializable]
		public struct TailComponent
		{
			// Token: 0x04004FD9 RID: 20441
			public Vector3[] m_pTailPos;

			// Token: 0x04004FDA RID: 20442
			public Vector3 m_TopPos;

			// Token: 0x04004FDB RID: 20443
			public Rect m_tailUVRect;

			// Token: 0x04004FDC RID: 20444
			public float m_width;
		}

		// Token: 0x02000F41 RID: 3905
		[Serializable]
		public struct BlinkComponent
		{
			// Token: 0x04004FDD RID: 20445
			public float m_blinkRate;

			// Token: 0x04004FDE RID: 20446
			public float m_blinkSpeed;
		}

		// Token: 0x02000F42 RID: 3906
		[Serializable]
		public struct PathMoveComponent
		{
			// Token: 0x04004FDF RID: 20447
			public Vector3[] m_offsetPos;
		}
	}
}
