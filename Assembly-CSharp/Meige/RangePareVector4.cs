﻿using System;

namespace Meige
{
	// Token: 0x02000F04 RID: 3844
	[Serializable]
	public class RangePareVector4 : RangePare<Float4>
	{
		// Token: 0x06004F3A RID: 20282 RVA: 0x0015DE69 File Offset: 0x0015C269
		public RangePareVector4()
		{
		}

		// Token: 0x06004F3B RID: 20283 RVA: 0x0015DE71 File Offset: 0x0015C271
		public RangePareVector4(Float4 min, Float4 max) : base(min, max)
		{
		}

		// Token: 0x06004F3C RID: 20284 RVA: 0x0015DE7B File Offset: 0x0015C27B
		public RangePareVector4(RangePareVector4 v) : base(v)
		{
		}

		// Token: 0x06004F3D RID: 20285 RVA: 0x0015DE84 File Offset: 0x0015C284
		public new RangePareVector4 Clone()
		{
			return new RangePareVector4(this);
		}
	}
}
