﻿using System;

namespace Meige
{
	// Token: 0x02000F8E RID: 3982
	public enum eTexAlphaType
	{
		// Token: 0x0400535B RID: 21339
		eTexAlpha_Invalid = -1,
		// Token: 0x0400535C RID: 21340
		eTexAlpha_Opaque,
		// Token: 0x0400535D RID: 21341
		eTexAlpha_PunchThrough,
		// Token: 0x0400535E RID: 21342
		eTexAlpha_Alpha
	}
}
