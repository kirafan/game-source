﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000E98 RID: 3736
	public class EffectHelper
	{
		// Token: 0x06004D5B RID: 19803 RVA: 0x00159036 File Offset: 0x00157436
		public static int GetComponentNum(eEffectAnimTypeCode typeCode)
		{
			return EffectHelper.m_EffectAnimTypeCodeComponentNum[(int)typeCode];
		}

		// Token: 0x06004D5C RID: 19804 RVA: 0x00159043 File Offset: 0x00157443
		public static Type GetTypeFromTypeCode(eEffectAnimTypeCode typeCode)
		{
			return EffectHelper.m_EffectAnimTypeCodeToType[(int)typeCode];
		}

		// Token: 0x06004D5D RID: 19805 RVA: 0x00159050 File Offset: 0x00157450
		public static eEffectAnimTypeCode GetTypeCodeFromType(Type type)
		{
			eEffectAnimTypeCode result;
			if (EffectHelper.m_EffectAnimTypeToTypeCode.TryGetValue(type, out result))
			{
				return result;
			}
			return eEffectAnimTypeCode.Invalid;
		}

		// Token: 0x06004D5E RID: 19806 RVA: 0x00159074 File Offset: 0x00157474
		public static int GetTargetNum(object target)
		{
			Type type = target.GetType();
			eEffectAnimTypeCode typeCodeFromType = EffectHelper.GetTypeCodeFromType(type);
			int result = 0;
			switch (typeCodeFromType)
			{
			case eEffectAnimTypeCode.RangeValueInt:
			{
				RangeValueInt rangeValueInt = (RangeValueInt)target;
				if (rangeValueInt.m_Value == null)
				{
				}
				result = rangeValueInt.m_Value.Length;
				break;
			}
			case eEffectAnimTypeCode.RangeValueFloat:
			{
				RangeValueFloat rangeValueFloat = (RangeValueFloat)target;
				if (rangeValueFloat.m_Value == null)
				{
				}
				result = rangeValueFloat.m_Value.Length;
				break;
			}
			case eEffectAnimTypeCode.RangeValueVector2:
			{
				RangeValueVector2 rangeValueVector = (RangeValueVector2)target;
				if (rangeValueVector.m_Value == null)
				{
				}
				result = rangeValueVector.m_Value.Length;
				break;
			}
			case eEffectAnimTypeCode.RangeValueVector3:
			{
				RangeValueVector3 rangeValueVector2 = (RangeValueVector3)target;
				if (rangeValueVector2.m_Value == null)
				{
				}
				result = rangeValueVector2.m_Value.Length;
				break;
			}
			case eEffectAnimTypeCode.RangeValueVector4:
			{
				RangeValueVector4 rangeValueVector3 = (RangeValueVector4)target;
				if (rangeValueVector3.m_Value == null)
				{
				}
				result = rangeValueVector3.m_Value.Length;
				break;
			}
			case eEffectAnimTypeCode.RangeValueColor:
			{
				RangeValueColor rangeValueColor = (RangeValueColor)target;
				if (rangeValueColor.m_Value == null)
				{
				}
				result = rangeValueColor.m_Value.Length;
				break;
			}
			}
			return result;
		}

		// Token: 0x06004D5F RID: 19807 RVA: 0x00159193 File Offset: 0x00157593
		public static T UnBox<T>(object value)
		{
			return (T)((object)value);
		}

		// Token: 0x06004D60 RID: 19808 RVA: 0x0015919B File Offset: 0x0015759B
		public static void Copy(ref bool target, int tgtIdx, object value)
		{
			target = (bool)value;
		}

		// Token: 0x06004D61 RID: 19809 RVA: 0x001591A5 File Offset: 0x001575A5
		public static void Copy(ref sbyte target, int tgtIdx, object value)
		{
			target = (sbyte)value;
		}

		// Token: 0x06004D62 RID: 19810 RVA: 0x001591AF File Offset: 0x001575AF
		public static void Copy(ref byte target, int tgtIdx, object value)
		{
			target = (byte)value;
		}

		// Token: 0x06004D63 RID: 19811 RVA: 0x001591B9 File Offset: 0x001575B9
		public static void Copy(ref short target, int tgtIdx, object value)
		{
			target = (short)value;
		}

		// Token: 0x06004D64 RID: 19812 RVA: 0x001591C3 File Offset: 0x001575C3
		public static void Copy(ref ushort target, int tgtIdx, object value)
		{
			target = (ushort)value;
		}

		// Token: 0x06004D65 RID: 19813 RVA: 0x001591CD File Offset: 0x001575CD
		public static void Copy(ref int target, int tgtIdx, object value)
		{
			target = (int)value;
		}

		// Token: 0x06004D66 RID: 19814 RVA: 0x001591D7 File Offset: 0x001575D7
		public static void Copy(ref uint target, int tgtIdx, object value)
		{
			target = (uint)value;
		}

		// Token: 0x06004D67 RID: 19815 RVA: 0x001591E1 File Offset: 0x001575E1
		public static void Copy(ref long target, int tgtIdx, object value)
		{
			target = (long)value;
		}

		// Token: 0x06004D68 RID: 19816 RVA: 0x001591EB File Offset: 0x001575EB
		public static void Copy(ref ulong target, int tgtIdx, object value)
		{
			target = (ulong)value;
		}

		// Token: 0x06004D69 RID: 19817 RVA: 0x001591F5 File Offset: 0x001575F5
		public static void Copy(ref float target, int tgtIdx, object value)
		{
			target = (float)value;
		}

		// Token: 0x06004D6A RID: 19818 RVA: 0x001591FF File Offset: 0x001575FF
		public static void Copy(ref double target, int tgtIdx, object value)
		{
			target = (double)value;
		}

		// Token: 0x06004D6B RID: 19819 RVA: 0x00159209 File Offset: 0x00157609
		public static void Copy(ref Vector2 target, int tgtIdx, object value)
		{
			target = (Vector2)value;
		}

		// Token: 0x06004D6C RID: 19820 RVA: 0x00159217 File Offset: 0x00157617
		public static void Copy(ref Vector3 target, int tgtIdx, object value)
		{
			target = (Vector3)value;
		}

		// Token: 0x06004D6D RID: 19821 RVA: 0x00159225 File Offset: 0x00157625
		public static void Copy(ref Vector4 target, int tgtIdx, object value)
		{
			target = (Vector4)value;
		}

		// Token: 0x06004D6E RID: 19822 RVA: 0x00159233 File Offset: 0x00157633
		public static void Copy(ref Color target, int tgtIdx, object value)
		{
			target = (Color)value;
		}

		// Token: 0x06004D6F RID: 19823 RVA: 0x00159241 File Offset: 0x00157641
		public static void Copy(ref Rect target, int tgtIdx, object value)
		{
			target = (Rect)value;
		}

		// Token: 0x06004D70 RID: 19824 RVA: 0x0015924F File Offset: 0x0015764F
		public static void Copy(ref s32 target, int tgtIdx, object value)
		{
			target = (s32)value;
		}

		// Token: 0x06004D71 RID: 19825 RVA: 0x0015925D File Offset: 0x0015765D
		public static void Copy(ref f32 target, int tgtIdx, object value)
		{
			target = (f32)value;
		}

		// Token: 0x06004D72 RID: 19826 RVA: 0x0015926B File Offset: 0x0015766B
		public static void Copy(ref Float2 target, int tgtIdx, object value)
		{
			target = (Float2)value;
		}

		// Token: 0x06004D73 RID: 19827 RVA: 0x00159279 File Offset: 0x00157679
		public static void Copy(ref Float3 target, int tgtIdx, object value)
		{
			target = (Float3)value;
		}

		// Token: 0x06004D74 RID: 19828 RVA: 0x00159287 File Offset: 0x00157687
		public static void Copy(ref Float4 target, int tgtIdx, object value)
		{
			target = (Float4)value;
		}

		// Token: 0x06004D75 RID: 19829 RVA: 0x00159295 File Offset: 0x00157695
		public static void Copy(ref RGBA target, int tgtIdx, object value)
		{
			target = (RGBA)value;
		}

		// Token: 0x06004D76 RID: 19830 RVA: 0x001592A3 File Offset: 0x001576A3
		public static void Copy(ref RangePareFloat target, int tgtIdx, object value)
		{
			target.Set((RangePareFloat)value);
		}

		// Token: 0x06004D77 RID: 19831 RVA: 0x001592B2 File Offset: 0x001576B2
		public static void Copy(ref RangePareInt target, int tgtIdx, object value)
		{
			target.Set((RangePareInt)value);
		}

		// Token: 0x06004D78 RID: 19832 RVA: 0x001592C1 File Offset: 0x001576C1
		public static void Copy(ref RangePareVector2 target, int tgtIdx, object value)
		{
			target.Set((RangePareVector2)value);
		}

		// Token: 0x06004D79 RID: 19833 RVA: 0x001592D0 File Offset: 0x001576D0
		public static void Copy(ref RangePareVector3 target, int tgtIdx, object value)
		{
			target.Set((RangePareVector3)value);
		}

		// Token: 0x06004D7A RID: 19834 RVA: 0x001592DF File Offset: 0x001576DF
		public static void Copy(ref RangePareVector4 target, int tgtIdx, object value)
		{
			target.Set((RangePareVector4)value);
		}

		// Token: 0x06004D7B RID: 19835 RVA: 0x001592EE File Offset: 0x001576EE
		public static void Copy(ref RangePareColor target, int tgtIdx, object value)
		{
			target.Set((RangePareColor)value);
		}

		// Token: 0x06004D7C RID: 19836 RVA: 0x00159300 File Offset: 0x00157700
		public static void Copy(ref RangeValueInt target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D7D RID: 19837 RVA: 0x00159320 File Offset: 0x00157720
		public static void Copy(ref RangeValueFloat target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D7E RID: 19838 RVA: 0x00159340 File Offset: 0x00157740
		public static void Copy(ref RangeValueVector2 target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D7F RID: 19839 RVA: 0x00159360 File Offset: 0x00157760
		public static void Copy(ref RangeValueVector3 target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D80 RID: 19840 RVA: 0x00159380 File Offset: 0x00157780
		public static void Copy(ref RangeValueVector4 target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D81 RID: 19841 RVA: 0x001593A0 File Offset: 0x001577A0
		public static void Copy(ref RangeValueColor target, int tgtIdx, object value)
		{
			target[tgtIdx].SetRowValue(value);
		}

		// Token: 0x06004D82 RID: 19842 RVA: 0x001593BE File Offset: 0x001577BE
		public static void Set(ref bool target, int tgtIdx, int componentIdx, float value)
		{
			target = (value != 0f);
		}

		// Token: 0x06004D83 RID: 19843 RVA: 0x001593D4 File Offset: 0x001577D4
		public static void Set(ref sbyte target, int tgtIdx, int componentIdx, float value)
		{
			target = (sbyte)value;
		}

		// Token: 0x06004D84 RID: 19844 RVA: 0x001593DA File Offset: 0x001577DA
		public static void Set(ref byte target, int tgtIdx, int componentIdx, float value)
		{
			target = (byte)value;
		}

		// Token: 0x06004D85 RID: 19845 RVA: 0x001593E0 File Offset: 0x001577E0
		public static void Set(ref short target, int tgtIdx, int componentIdx, float value)
		{
			target = (short)value;
		}

		// Token: 0x06004D86 RID: 19846 RVA: 0x001593E6 File Offset: 0x001577E6
		public static void Set(ref ushort target, int tgtIdx, int componentIdx, float value)
		{
			target = (ushort)value;
		}

		// Token: 0x06004D87 RID: 19847 RVA: 0x001593EC File Offset: 0x001577EC
		public static void Set(ref int target, int tgtIdx, int componentIdx, float value)
		{
			target = (int)value;
		}

		// Token: 0x06004D88 RID: 19848 RVA: 0x001593F2 File Offset: 0x001577F2
		public static void Set(ref uint target, int tgtIdx, int componentIdx, float value)
		{
			target = (uint)value;
		}

		// Token: 0x06004D89 RID: 19849 RVA: 0x001593F8 File Offset: 0x001577F8
		public static void Set(ref long target, int tgtIdx, int componentIdx, float value)
		{
			target = (long)value;
		}

		// Token: 0x06004D8A RID: 19850 RVA: 0x001593FE File Offset: 0x001577FE
		public static void Set(ref ulong target, int tgtIdx, int componentIdx, float value)
		{
			target = (ulong)value;
		}

		// Token: 0x06004D8B RID: 19851 RVA: 0x00159404 File Offset: 0x00157804
		public static void Set(ref float target, int tgtIdx, int componentIdx, float value)
		{
			target = value;
		}

		// Token: 0x06004D8C RID: 19852 RVA: 0x0015940A File Offset: 0x0015780A
		public static void Set(ref double target, int tgtIdx, int componentIdx, float value)
		{
			target = (double)value;
		}

		// Token: 0x06004D8D RID: 19853 RVA: 0x00159410 File Offset: 0x00157810
		public static void Set(ref Vector2 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D8E RID: 19854 RVA: 0x0015941A File Offset: 0x0015781A
		public static void Set(ref Vector3 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D8F RID: 19855 RVA: 0x00159424 File Offset: 0x00157824
		public static void Set(ref Vector4 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D90 RID: 19856 RVA: 0x0015942E File Offset: 0x0015782E
		public static void Set(ref Color target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D91 RID: 19857 RVA: 0x00159438 File Offset: 0x00157838
		public static void Set(ref Rect target, int tgtIdx, int componentIdx, float value)
		{
			if (componentIdx == 0)
			{
				target.x = value;
			}
			if (componentIdx == 1)
			{
				target.y = value;
			}
			if (componentIdx == 2)
			{
				target.width = value;
			}
			else
			{
				target.height = value;
			}
		}

		// Token: 0x06004D92 RID: 19858 RVA: 0x0015946F File Offset: 0x0015786F
		public static void Set(ref s32 target, int tgtIdx, int componentIdx, float value)
		{
			target = (int)value;
		}

		// Token: 0x06004D93 RID: 19859 RVA: 0x0015947E File Offset: 0x0015787E
		public static void Set(ref f32 target, int tgtIdx, int componentIdx, float value)
		{
			target = value;
		}

		// Token: 0x06004D94 RID: 19860 RVA: 0x0015948C File Offset: 0x0015788C
		public static void Set(ref Float2 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D95 RID: 19861 RVA: 0x00159496 File Offset: 0x00157896
		public static void Set(ref Float3 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D96 RID: 19862 RVA: 0x001594A0 File Offset: 0x001578A0
		public static void Set(ref Float4 target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D97 RID: 19863 RVA: 0x001594AA File Offset: 0x001578AA
		public static void Set(ref RGBA target, int tgtIdx, int componentIdx, float value)
		{
			target[componentIdx] = value;
		}

		// Token: 0x06004D98 RID: 19864 RVA: 0x001594B4 File Offset: 0x001578B4
		public static void Set(ref RangePareFloat target, int tgtIdx, int componentIdx, float value)
		{
			if (componentIdx == 0)
			{
				target.m_Min.m_Value = value;
			}
			else
			{
				target.m_Max.m_Value = value;
			}
		}

		// Token: 0x06004D99 RID: 19865 RVA: 0x001594DB File Offset: 0x001578DB
		public static void Set(ref RangePareInt target, int tgtIdx, int componentIdx, float value)
		{
			if (componentIdx == 0)
			{
				target.m_Min.m_Value = (int)value;
			}
			else
			{
				target.m_Max.m_Value = (int)value;
			}
		}

		// Token: 0x06004D9A RID: 19866 RVA: 0x00159504 File Offset: 0x00157904
		public static void Set(ref RangePareVector2 target, int tgtIdx, int componentIdx, float value)
		{
			switch (componentIdx)
			{
			case 0:
				target.m_Min.m_Value[0] = value;
				break;
			case 1:
				target.m_Min.m_Value[1] = value;
				break;
			case 2:
				target.m_Max.m_Value[0] = value;
				break;
			case 3:
				target.m_Max.m_Value[1] = value;
				break;
			}
		}

		// Token: 0x06004D9B RID: 19867 RVA: 0x0015958C File Offset: 0x0015798C
		public static void Set(ref RangePareVector3 target, int tgtIdx, int componentIdx, float value)
		{
			switch (componentIdx)
			{
			case 0:
				target.m_Min.m_Value[0] = value;
				break;
			case 1:
				target.m_Min.m_Value[1] = value;
				break;
			case 2:
				target.m_Min.m_Value[2] = value;
				break;
			case 3:
				target.m_Max.m_Value[0] = value;
				break;
			case 4:
				target.m_Max.m_Value[1] = value;
				break;
			case 5:
				target.m_Max.m_Value[2] = value;
				break;
			}
		}

		// Token: 0x06004D9C RID: 19868 RVA: 0x0015964C File Offset: 0x00157A4C
		public static void Set(ref RangePareVector4 target, int tgtIdx, int componentIdx, float value)
		{
			switch (componentIdx)
			{
			case 0:
				target.m_Min.m_Value[0] = value;
				break;
			case 1:
				target.m_Min.m_Value[1] = value;
				break;
			case 2:
				target.m_Min.m_Value[2] = value;
				break;
			case 3:
				target.m_Max.m_Value[3] = value;
				break;
			case 4:
				target.m_Max.m_Value[0] = value;
				break;
			case 5:
				target.m_Max.m_Value[1] = value;
				break;
			case 6:
				target.m_Max.m_Value[2] = value;
				break;
			case 7:
				target.m_Max.m_Value[3] = value;
				break;
			}
		}

		// Token: 0x06004D9D RID: 19869 RVA: 0x00159744 File Offset: 0x00157B44
		public static void Set(ref RangePareColor target, int tgtIdx, int componentIdx, float value)
		{
			switch (componentIdx)
			{
			case 0:
				target.m_Min.m_Value[0] = value;
				break;
			case 1:
				target.m_Min.m_Value[1] = value;
				break;
			case 2:
				target.m_Min.m_Value[2] = value;
				break;
			case 3:
				target.m_Max.m_Value[3] = value;
				break;
			case 4:
				target.m_Max.m_Value[0] = value;
				break;
			case 5:
				target.m_Max.m_Value[1] = value;
				break;
			case 6:
				target.m_Max.m_Value[2] = value;
				break;
			case 7:
				target.m_Max.m_Value[3] = value;
				break;
			}
		}

		// Token: 0x06004D9E RID: 19870 RVA: 0x0015983C File Offset: 0x00157C3C
		public static void Set(ref RangeValueInt target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value = (int)value;
		}

		// Token: 0x06004D9F RID: 19871 RVA: 0x00159852 File Offset: 0x00157C52
		public static void Set(ref RangeValueFloat target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value = value;
		}

		// Token: 0x06004DA0 RID: 19872 RVA: 0x00159867 File Offset: 0x00157C67
		public static void Set(ref RangeValueVector2 target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value[componentIdx] = value;
		}

		// Token: 0x06004DA1 RID: 19873 RVA: 0x00159882 File Offset: 0x00157C82
		public static void Set(ref RangeValueVector3 target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value[componentIdx] = value;
		}

		// Token: 0x06004DA2 RID: 19874 RVA: 0x0015989D File Offset: 0x00157C9D
		public static void Set(ref RangeValueVector4 target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value[componentIdx] = value;
		}

		// Token: 0x06004DA3 RID: 19875 RVA: 0x001598B8 File Offset: 0x00157CB8
		public static void Set(ref RangeValueColor target, int tgtIdx, int componentIdx, float value)
		{
			target.m_Value[tgtIdx].m_Value[componentIdx] = value;
		}

		// Token: 0x06004DA4 RID: 19876 RVA: 0x001598D3 File Offset: 0x00157CD3
		public static float Get(bool target, int tgtIdx, int componentIdx)
		{
			return (!target) ? 0f : 1f;
		}

		// Token: 0x06004DA5 RID: 19877 RVA: 0x001598EA File Offset: 0x00157CEA
		public static float Get(sbyte target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DA6 RID: 19878 RVA: 0x001598EE File Offset: 0x00157CEE
		public static float Get(byte target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DA7 RID: 19879 RVA: 0x001598F2 File Offset: 0x00157CF2
		public static float Get(short target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DA8 RID: 19880 RVA: 0x001598F6 File Offset: 0x00157CF6
		public static float Get(ushort target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DA9 RID: 19881 RVA: 0x001598FA File Offset: 0x00157CFA
		public static float Get(int target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DAA RID: 19882 RVA: 0x001598FE File Offset: 0x00157CFE
		public static float Get(uint target, int tgtIdx, int componentIdx)
		{
			return target;
		}

		// Token: 0x06004DAB RID: 19883 RVA: 0x00159903 File Offset: 0x00157D03
		public static float Get(long target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DAC RID: 19884 RVA: 0x00159907 File Offset: 0x00157D07
		public static float Get(ulong target, int tgtIdx, int componentIdx)
		{
			return target;
		}

		// Token: 0x06004DAD RID: 19885 RVA: 0x0015990C File Offset: 0x00157D0C
		public static float Get(float target, int tgtIdx, int componentIdx)
		{
			return target;
		}

		// Token: 0x06004DAE RID: 19886 RVA: 0x00159910 File Offset: 0x00157D10
		public static float Get(double target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DAF RID: 19887 RVA: 0x00159914 File Offset: 0x00157D14
		public static float Get(Vector2 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB0 RID: 19888 RVA: 0x0015991E File Offset: 0x00157D1E
		public static float Get(Vector3 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB1 RID: 19889 RVA: 0x00159928 File Offset: 0x00157D28
		public static float Get(Vector4 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB2 RID: 19890 RVA: 0x00159932 File Offset: 0x00157D32
		public static float Get(Color target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB3 RID: 19891 RVA: 0x0015993C File Offset: 0x00157D3C
		public static float Get(Rect target, int tgtIdx, int componentIdx)
		{
			if (componentIdx == 0)
			{
				return target.x;
			}
			if (componentIdx == 1)
			{
				return target.y;
			}
			if (componentIdx == 2)
			{
				return target.width;
			}
			return target.height;
		}

		// Token: 0x06004DB4 RID: 19892 RVA: 0x00159971 File Offset: 0x00157D71
		public static float Get(s32 target, int tgtIdx, int componentIdx)
		{
			return (float)target;
		}

		// Token: 0x06004DB5 RID: 19893 RVA: 0x0015997A File Offset: 0x00157D7A
		public static float Get(f32 target, int tgtIdx, int componentIdx)
		{
			return target;
		}

		// Token: 0x06004DB6 RID: 19894 RVA: 0x00159982 File Offset: 0x00157D82
		public static float Get(Float2 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB7 RID: 19895 RVA: 0x0015998C File Offset: 0x00157D8C
		public static float Get(Float3 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB8 RID: 19896 RVA: 0x00159996 File Offset: 0x00157D96
		public static float Get(Float4 target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DB9 RID: 19897 RVA: 0x001599A0 File Offset: 0x00157DA0
		public static float Get(RGBA target, int tgtIdx, int componentIdx)
		{
			return target[componentIdx];
		}

		// Token: 0x06004DBA RID: 19898 RVA: 0x001599AC File Offset: 0x00157DAC
		public static float Get(RangePareFloat target, int tgtIdx, int componentIdx)
		{
			if (componentIdx == 0)
			{
				return target[0].m_Value;
			}
			if (componentIdx != 1)
			{
				return 0f;
			}
			return target[1].m_Value;
		}

		// Token: 0x06004DBB RID: 19899 RVA: 0x001599F0 File Offset: 0x00157DF0
		public static float Get(RangePareInt target, int tgtIdx, int componentIdx)
		{
			if (componentIdx == 0)
			{
				return (float)target[0].m_Value;
			}
			if (componentIdx != 1)
			{
				return 0f;
			}
			return (float)target[1].m_Value;
		}

		// Token: 0x06004DBC RID: 19900 RVA: 0x00159A38 File Offset: 0x00157E38
		public static float Get(RangePareVector2 target, int tgtIdx, int componentIdx)
		{
			switch (componentIdx)
			{
			case 0:
				return target[0].m_Value[0];
			case 1:
				return target[0].m_Value[1];
			case 2:
				return target[1].m_Value[0];
			case 3:
				return target[1].m_Value[1];
			default:
				return 0f;
			}
		}

		// Token: 0x06004DBD RID: 19901 RVA: 0x00159AC4 File Offset: 0x00157EC4
		public static float Get(RangePareVector3 target, int tgtIdx, int componentIdx)
		{
			switch (componentIdx)
			{
			case 0:
				return target[0].m_Value[0];
			case 1:
				return target[0].m_Value[1];
			case 2:
				return target[0].m_Value[2];
			case 3:
				return target[1].m_Value[0];
			case 4:
				return target[1].m_Value[1];
			case 5:
				return target[1].m_Value[2];
			default:
				return 0f;
			}
		}

		// Token: 0x06004DBE RID: 19902 RVA: 0x00159B88 File Offset: 0x00157F88
		public static float Get(RangePareVector4 target, int tgtIdx, int componentIdx)
		{
			switch (componentIdx)
			{
			case 0:
				return target[0].m_Value[0];
			case 1:
				return target[0].m_Value[1];
			case 2:
				return target[0].m_Value[2];
			case 3:
				return target[0].m_Value[3];
			case 4:
				return target[1].m_Value[0];
			case 5:
				return target[1].m_Value[1];
			case 6:
				return target[1].m_Value[2];
			case 7:
				return target[1].m_Value[3];
			default:
				return 0f;
			}
		}

		// Token: 0x06004DBF RID: 19903 RVA: 0x00159C84 File Offset: 0x00158084
		public static float Get(RangePareColor target, int tgtIdx, int componentIdx)
		{
			switch (componentIdx)
			{
			case 0:
				return target[0].m_Value[0];
			case 1:
				return target[0].m_Value[1];
			case 2:
				return target[0].m_Value[2];
			case 3:
				return target[0].m_Value[3];
			case 4:
				return target[1].m_Value[0];
			case 5:
				return target[1].m_Value[1];
			case 6:
				return target[1].m_Value[2];
			case 7:
				return target[1].m_Value[3];
			default:
				return 0f;
			}
		}

		// Token: 0x06004DC0 RID: 19904 RVA: 0x00159D80 File Offset: 0x00158180
		public static float Get(RangeValueInt target, int tgtIdx, int componentIdx)
		{
			return (float)target[tgtIdx].m_Value;
		}

		// Token: 0x06004DC1 RID: 19905 RVA: 0x00159DA0 File Offset: 0x001581A0
		public static float Get(RangeValueFloat target, int tgtIdx, int componentIdx)
		{
			return target[tgtIdx].m_Value;
		}

		// Token: 0x06004DC2 RID: 19906 RVA: 0x00159DBC File Offset: 0x001581BC
		public static float Get(RangeValueVector2 target, int tgtIdx, int componentIdx)
		{
			return target.m_Value[tgtIdx].m_Value[componentIdx];
		}

		// Token: 0x06004DC3 RID: 19907 RVA: 0x00159DD5 File Offset: 0x001581D5
		public static float Get(RangeValueVector3 target, int tgtIdx, int componentIdx)
		{
			return target.m_Value[tgtIdx].m_Value[componentIdx];
		}

		// Token: 0x06004DC4 RID: 19908 RVA: 0x00159DEE File Offset: 0x001581EE
		public static float Get(RangeValueVector4 target, int tgtIdx, int componentIdx)
		{
			return target.m_Value[tgtIdx].m_Value[componentIdx];
		}

		// Token: 0x06004DC5 RID: 19909 RVA: 0x00159E07 File Offset: 0x00158207
		public static float Get(RangeValueColor target, int tgtIdx, int componentIdx)
		{
			return target.m_Value[tgtIdx].m_Value[componentIdx];
		}

		// Token: 0x06004DC6 RID: 19910 RVA: 0x00159E20 File Offset: 0x00158220
		public static float Get(object target, int tgtIdx, int componentIdx)
		{
			switch (EffectHelper.GetTypeCodeFromType(target.GetType()))
			{
			case eEffectAnimTypeCode.Bool:
				return EffectHelper.Get((bool)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Int:
				return EffectHelper.Get((int)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Long:
				return EffectHelper.Get((long)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Float:
				return EffectHelper.Get((float)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Vector2:
				return EffectHelper.Get((Vector2)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Vector3:
				return EffectHelper.Get((Vector3)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Vector4:
				return EffectHelper.Get((Vector4)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Color:
				return EffectHelper.Get((Color)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.S32:
				return EffectHelper.Get((s32)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.F32:
				return EffectHelper.Get((f32)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Float2:
				return EffectHelper.Get((Float2)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Float3:
				return EffectHelper.Get((Float3)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Float4:
				return EffectHelper.Get((Float4)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RGBA:
				return EffectHelper.Get((RGBA)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareFloat:
				return EffectHelper.Get((RangePareFloat)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareInt:
				return EffectHelper.Get((RangePareInt)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareVector2:
				return EffectHelper.Get((RangePareVector2)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareVector3:
				return EffectHelper.Get((RangePareVector3)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareVector4:
				return EffectHelper.Get((RangePareVector4)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangePareColor:
				return EffectHelper.Get((RangePareColor)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueInt:
				return EffectHelper.Get((RangeValueInt)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueFloat:
				return EffectHelper.Get((RangeValueFloat)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueVector2:
				return EffectHelper.Get((RangeValueVector2)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueVector3:
				return EffectHelper.Get((RangeValueVector3)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueVector4:
				return EffectHelper.Get((RangeValueVector4)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.RangeValueColor:
				return EffectHelper.Get((RangeValueColor)target, tgtIdx, componentIdx);
			case eEffectAnimTypeCode.Enum:
				return EffectHelper.Get((int)target, tgtIdx, componentIdx);
			}
			return 0f;
		}

		// Token: 0x06004DC7 RID: 19911 RVA: 0x0015A034 File Offset: 0x00158434
		public static string GetHierarchyPath(Transform top, Transform self)
		{
			string text = self.gameObject.name;
			Transform parent = self.parent;
			while (parent != null && parent != top)
			{
				text = parent.name + "/" + text;
				parent = parent.parent;
			}
			return text;
		}

		// Token: 0x06004DC8 RID: 19912 RVA: 0x0015A08B File Offset: 0x0015848B
		public static bool IsPrefab(GameObject obj)
		{
			return false;
		}

		// Token: 0x06004DC9 RID: 19913 RVA: 0x0015A08E File Offset: 0x0015848E
		public static bool IsPrefabInstance(GameObject obj)
		{
			return false;
		}

		// Token: 0x04004E01 RID: 19969
		private static ReadOnlyCollection<int> m_EffectAnimTypeCodeComponentNum = Array.AsReadOnly<int>(new int[]
		{
			1,
			1,
			1,
			1,
			2,
			3,
			4,
			4,
			4,
			1,
			1,
			2,
			3,
			4,
			4,
			2,
			2,
			4,
			6,
			8,
			8,
			1,
			1,
			2,
			3,
			4,
			4,
			1
		});

		// Token: 0x04004E02 RID: 19970
		private static ReadOnlyCollection<Type> m_EffectAnimTypeCodeToType = Array.AsReadOnly<Type>(new Type[]
		{
			typeof(bool),
			typeof(int),
			typeof(long),
			typeof(float),
			typeof(Vector2),
			typeof(Vector3),
			typeof(Vector4),
			typeof(Color),
			typeof(Rect),
			typeof(s32),
			typeof(f32),
			typeof(Float2),
			typeof(Float3),
			typeof(Float4),
			typeof(RGBA),
			typeof(RangePareFloat),
			typeof(RangePareInt),
			typeof(RangePareVector2),
			typeof(RangePareVector3),
			typeof(RangePareVector4),
			typeof(RangePareColor),
			typeof(RangeValueInt),
			typeof(RangeValueFloat),
			typeof(RangeValueVector2),
			typeof(RangeValueVector3),
			typeof(RangeValueVector4),
			typeof(RangeValueColor),
			typeof(Enum)
		});

		// Token: 0x04004E03 RID: 19971
		private static Dictionary<Type, eEffectAnimTypeCode> m_EffectAnimTypeToTypeCode = new Dictionary<Type, eEffectAnimTypeCode>
		{
			{
				typeof(bool),
				eEffectAnimTypeCode.Bool
			},
			{
				typeof(int),
				eEffectAnimTypeCode.Int
			},
			{
				typeof(long),
				eEffectAnimTypeCode.Long
			},
			{
				typeof(float),
				eEffectAnimTypeCode.Float
			},
			{
				typeof(Vector2),
				eEffectAnimTypeCode.Vector2
			},
			{
				typeof(Vector3),
				eEffectAnimTypeCode.Vector3
			},
			{
				typeof(Vector4),
				eEffectAnimTypeCode.Vector4
			},
			{
				typeof(Color),
				eEffectAnimTypeCode.Color
			},
			{
				typeof(Rect),
				eEffectAnimTypeCode.Rect
			},
			{
				typeof(s32),
				eEffectAnimTypeCode.S32
			},
			{
				typeof(f32),
				eEffectAnimTypeCode.F32
			},
			{
				typeof(Float2),
				eEffectAnimTypeCode.Float2
			},
			{
				typeof(Float3),
				eEffectAnimTypeCode.Float3
			},
			{
				typeof(Float4),
				eEffectAnimTypeCode.Float4
			},
			{
				typeof(RGBA),
				eEffectAnimTypeCode.RGBA
			},
			{
				typeof(RangePareFloat),
				eEffectAnimTypeCode.RangePareFloat
			},
			{
				typeof(RangePareInt),
				eEffectAnimTypeCode.RangePareInt
			},
			{
				typeof(RangePareVector2),
				eEffectAnimTypeCode.RangePareVector2
			},
			{
				typeof(RangePareVector3),
				eEffectAnimTypeCode.RangePareVector3
			},
			{
				typeof(RangePareVector4),
				eEffectAnimTypeCode.RangePareVector4
			},
			{
				typeof(RangePareColor),
				eEffectAnimTypeCode.RangePareColor
			},
			{
				typeof(RangeValueInt),
				eEffectAnimTypeCode.RangeValueInt
			},
			{
				typeof(RangeValueFloat),
				eEffectAnimTypeCode.RangeValueFloat
			},
			{
				typeof(RangeValueVector2),
				eEffectAnimTypeCode.RangeValueVector2
			},
			{
				typeof(RangeValueVector3),
				eEffectAnimTypeCode.RangeValueVector3
			},
			{
				typeof(RangeValueVector4),
				eEffectAnimTypeCode.RangeValueVector4
			},
			{
				typeof(RangeValueColor),
				eEffectAnimTypeCode.RangeValueColor
			},
			{
				typeof(Enum),
				eEffectAnimTypeCode.Enum
			}
		};
	}
}
