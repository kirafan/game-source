﻿using System;

namespace Meige
{
	// Token: 0x02000FB3 RID: 4019
	[Serializable]
	public class MsbADJointParam
	{
		// Token: 0x06005385 RID: 21381 RVA: 0x00175E72 File Offset: 0x00174272
		public MsbADJointParam()
		{
			this.m_bILKinkStretch = false;
		}

		// Token: 0x06005386 RID: 21382 RVA: 0x00175E84 File Offset: 0x00174284
		public MsbADJointParam(MsbADJointParam src)
		{
			this.m_HieName = src.m_HieName;
			this.m_ParentIdxOfADHierarchy = src.m_ParentIdxOfADHierarchy;
			this.m_NumOfIKGroup = src.m_NumOfIKGroup;
			this.m_bLock = src.m_bLock;
			this.m_bRoot = src.m_bRoot;
			this.m_bGravity = src.m_bGravity;
			this.m_bAirFrictiion = src.m_bAirFrictiion;
			this.m_bILKinkStretch = src.m_bILKinkStretch;
		}

		// Token: 0x04005465 RID: 21605
		public string m_HieName;

		// Token: 0x04005466 RID: 21606
		public int m_ParentIdxOfADHierarchy;

		// Token: 0x04005467 RID: 21607
		public int m_NumOfIKGroup;

		// Token: 0x04005468 RID: 21608
		public bool m_bLock;

		// Token: 0x04005469 RID: 21609
		public bool m_bRoot;

		// Token: 0x0400546A RID: 21610
		public bool m_bGravity;

		// Token: 0x0400546B RID: 21611
		public bool m_bAirFrictiion;

		// Token: 0x0400546C RID: 21612
		public bool m_bILKinkStretch;
	}
}
