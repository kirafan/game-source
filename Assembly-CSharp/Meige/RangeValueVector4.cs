﻿using System;

namespace Meige
{
	// Token: 0x02000EFE RID: 3838
	[Serializable]
	public class RangeValueVector4 : RangeValue<Float4>
	{
		// Token: 0x06004F22 RID: 20258 RVA: 0x0015DD49 File Offset: 0x0015C149
		public RangeValueVector4()
		{
		}

		// Token: 0x06004F23 RID: 20259 RVA: 0x0015DD51 File Offset: 0x0015C151
		public RangeValueVector4(int num) : base(num)
		{
		}

		// Token: 0x06004F24 RID: 20260 RVA: 0x0015DD5A File Offset: 0x0015C15A
		public RangeValueVector4(RangeValueVector4 v) : base(v)
		{
		}

		// Token: 0x06004F25 RID: 20261 RVA: 0x0015DD64 File Offset: 0x0015C164
		public new RangeValueVector4 Clone()
		{
			return new RangeValueVector4(this);
		}
	}
}
