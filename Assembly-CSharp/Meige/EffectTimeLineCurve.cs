﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EEF RID: 3823
	[Serializable]
	public class EffectTimeLineCurve
	{
		// Token: 0x06004ECB RID: 20171 RVA: 0x0015CDD0 File Offset: 0x0015B1D0
		private void UpdateProcessIndex(float keyframe)
		{
			EffectTimeLineKeyFrame[] keyFrameArray = this.m_SourceCurve.m_KeyFrameArray;
			float time = keyFrameArray[keyFrameArray.Length - 1].m_Time;
			float time2 = keyFrameArray[0].m_Time;
			keyframe = Mathf.Clamp(keyframe, time2, time);
			if (keyFrameArray[this.m_ProcessIdx].m_Time > keyframe)
			{
				int i;
				for (i = this.m_ProcessIdx - 1; i > 0; i--)
				{
					if (keyFrameArray[i].m_Time <= keyframe)
					{
						break;
					}
				}
				this.m_ProcessIdx = i;
			}
			else if (keyFrameArray[this.m_ProcessIdx + 1].m_Time < keyframe)
			{
				int j;
				for (j = this.m_ProcessIdx + 1; j < keyFrameArray.Length - 1; j++)
				{
					if (keyFrameArray[j].m_Time > keyframe)
					{
						break;
					}
				}
				this.m_ProcessIdx = j - 1;
			}
		}

		// Token: 0x06004ECC RID: 20172 RVA: 0x0015CEC0 File Offset: 0x0015B2C0
		public void CalcValue(float keyframe)
		{
			EffectTimeLineKeyFrame[] keyFrameArray = this.m_SourceCurve.m_KeyFrameArray;
			eEffectAnimTypeCode typeCode = this.m_SourceCurve.m_Target.GetTypeCode(this.m_PropertyID);
			if (keyFrameArray.Length <= 1)
			{
				for (int i = 0; i < EffectHelper.GetComponentNum(typeCode); i++)
				{
					this.m_SourceCurve.m_Target.SetValue(this.m_PropertyID, this.m_SourceCurve.m_ArrayIdx, this.m_SourceCurve.m_TargetIdx, i, keyFrameArray[0].m_ComponentValue[i]);
				}
				return;
			}
			this.UpdateProcessIndex(keyframe);
			EffectTimeLineKeyFrame effectTimeLineKeyFrame = keyFrameArray[this.m_ProcessIdx];
			EffectTimeLineKeyFrame effectTimeLineKeyFrame2 = keyFrameArray[this.m_ProcessIdx + 1];
			for (int j = 0; j < EffectHelper.GetComponentNum(typeCode); j++)
			{
				if (typeCode == eEffectAnimTypeCode.Bool || typeCode == eEffectAnimTypeCode.Enum)
				{
					this.m_SourceCurve.m_Target.SetValue(this.m_PropertyID, this.m_SourceCurve.m_ArrayIdx, this.m_SourceCurve.m_TargetIdx, j, effectTimeLineKeyFrame.m_ComponentValue[j]);
				}
				else
				{
					float t = (keyframe - effectTimeLineKeyFrame.m_Time) / (effectTimeLineKeyFrame2.m_Time - effectTimeLineKeyFrame.m_Time);
					float value = Mathf.Lerp(effectTimeLineKeyFrame.m_ComponentValue[j], effectTimeLineKeyFrame2.m_ComponentValue[j], t);
					value = Mathf.Lerp(effectTimeLineKeyFrame.m_ComponentValue[j], effectTimeLineKeyFrame2.m_ComponentValue[j], t);
					this.m_SourceCurve.m_Target.SetValue(this.m_PropertyID, this.m_SourceCurve.m_ArrayIdx, this.m_SourceCurve.m_TargetIdx, j, value);
				}
			}
		}

		// Token: 0x04004E86 RID: 20102
		public EffectTimeLineSourceCurve m_SourceCurve;

		// Token: 0x04004E87 RID: 20103
		public int m_PropertyID;

		// Token: 0x04004E88 RID: 20104
		private int m_ProcessIdx;
	}
}
