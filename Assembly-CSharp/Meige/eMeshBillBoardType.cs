﻿using System;

namespace Meige
{
	// Token: 0x02000F7F RID: 3967
	public enum eMeshBillBoardType
	{
		// Token: 0x040052EB RID: 21227
		eMeshBillBoardType_None,
		// Token: 0x040052EC RID: 21228
		eMeshBillBoardType_XYZ,
		// Token: 0x040052ED RID: 21229
		eMeshBillBoardType_X,
		// Token: 0x040052EE RID: 21230
		eMeshBillBoardType_Y,
		// Token: 0x040052EF RID: 21231
		eMeshBillBoardType_Z
	}
}
