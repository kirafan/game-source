﻿using System;

namespace Meige
{
	// Token: 0x02000EFB RID: 3835
	[Serializable]
	public class RangeValueFloat : RangeValue<f32>
	{
		// Token: 0x06004F16 RID: 20246 RVA: 0x0015DCB9 File Offset: 0x0015C0B9
		public RangeValueFloat()
		{
		}

		// Token: 0x06004F17 RID: 20247 RVA: 0x0015DCC1 File Offset: 0x0015C0C1
		public RangeValueFloat(int num) : base(num)
		{
		}

		// Token: 0x06004F18 RID: 20248 RVA: 0x0015DCCA File Offset: 0x0015C0CA
		public RangeValueFloat(RangeValueFloat v) : base(v)
		{
		}

		// Token: 0x06004F19 RID: 20249 RVA: 0x0015DCD4 File Offset: 0x0015C0D4
		public new RangeValueFloat Clone()
		{
			return new RangeValueFloat(this);
		}
	}
}
