﻿using System;

namespace Meige
{
	// Token: 0x02000EED RID: 3821
	[Serializable]
	public struct EffectTimeLineKeyFrame
	{
		// Token: 0x04004E7E RID: 20094
		public float m_Time;

		// Token: 0x04004E7F RID: 20095
		public float[] m_ComponentValue;
	}
}
