﻿using System;

namespace Meige
{
	// Token: 0x02000F71 RID: 3953
	public enum eActTransitSystemCond
	{
		// Token: 0x0400522C RID: 21036
		eActTransitSystemCond_Invalid = -1,
		// Token: 0x0400522D RID: 21037
		eActTransitSystemCond_I_StateChange,
		// Token: 0x0400522E RID: 21038
		eActTransitSystemCond_B_NextFromEnd,
		// Token: 0x0400522F RID: 21039
		eActTransitSystemCond_B_NextByFlag,
		// Token: 0x04005230 RID: 21040
		eActTransitSystemCond_Max = 32
	}
}
