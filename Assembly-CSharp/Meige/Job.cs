﻿using System;
using System.Threading;

namespace Meige
{
	// Token: 0x02000FDA RID: 4058
	public class Job
	{
		// Token: 0x06005472 RID: 21618 RVA: 0x0017AAEA File Offset: 0x00178EEA
		public Job()
		{
			this.Clear();
		}

		// Token: 0x06005473 RID: 21619 RVA: 0x0017AAF8 File Offset: 0x00178EF8
		public void Clear()
		{
			this.m_state = Job.JobState.Wait;
			this.m_isError = false;
			this.m_jobFlagMask = 0;
			this.m_func = null;
			this.m_arg = null;
		}

		// Token: 0x06005474 RID: 21620 RVA: 0x0017AB1D File Offset: 0x00178F1D
		public void SetFunction(WaitCallback func, object arg)
		{
			this.m_func = func;
			this.m_arg = arg;
		}

		// Token: 0x06005475 RID: 21621 RVA: 0x0017AB30 File Offset: 0x00178F30
		public void Invoke()
		{
			this.m_state = Job.JobState.Prosessing;
			try
			{
				if (this.m_func != null)
				{
					this.m_func(this.m_arg);
				}
			}
			catch (Exception)
			{
				this.m_isError = true;
			}
			this.m_state = Job.JobState.Done;
		}

		// Token: 0x06005476 RID: 21622 RVA: 0x0017AB8C File Offset: 0x00178F8C
		public bool IsWait()
		{
			return this.m_state == Job.JobState.Wait;
		}

		// Token: 0x06005477 RID: 21623 RVA: 0x0017AB97 File Offset: 0x00178F97
		public bool IsProsessing()
		{
			return this.m_state == Job.JobState.Prosessing;
		}

		// Token: 0x06005478 RID: 21624 RVA: 0x0017ABA2 File Offset: 0x00178FA2
		public bool IsDone()
		{
			return this.m_state == Job.JobState.Done;
		}

		// Token: 0x06005479 RID: 21625 RVA: 0x0017ABAD File Offset: 0x00178FAD
		public bool IsError()
		{
			return this.m_isError;
		}

		// Token: 0x0600547A RID: 21626 RVA: 0x0017ABB5 File Offset: 0x00178FB5
		public void SetJobFlagMask(int flagMask)
		{
			this.m_jobFlagMask = flagMask;
		}

		// Token: 0x0600547B RID: 21627 RVA: 0x0017ABBE File Offset: 0x00178FBE
		public int GetJobFlagMask()
		{
			return this.m_jobFlagMask;
		}

		// Token: 0x04005592 RID: 21906
		private Job.JobState m_state;

		// Token: 0x04005593 RID: 21907
		private bool m_isError;

		// Token: 0x04005594 RID: 21908
		private int m_jobFlagMask;

		// Token: 0x04005595 RID: 21909
		private WaitCallback m_func;

		// Token: 0x04005596 RID: 21910
		private object m_arg;

		// Token: 0x02000FDB RID: 4059
		public enum JobState
		{
			// Token: 0x04005598 RID: 21912
			Wait,
			// Token: 0x04005599 RID: 21913
			Prosessing,
			// Token: 0x0400559A RID: 21914
			Done
		}
	}
}
