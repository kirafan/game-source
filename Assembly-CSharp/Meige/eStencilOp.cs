﻿using System;

namespace Meige
{
	// Token: 0x02000F93 RID: 3987
	public enum eStencilOp
	{
		// Token: 0x04005376 RID: 21366
		Keep,
		// Token: 0x04005377 RID: 21367
		Zero,
		// Token: 0x04005378 RID: 21368
		Replace,
		// Token: 0x04005379 RID: 21369
		IncrementSaturate,
		// Token: 0x0400537A RID: 21370
		DecrementSaturate,
		// Token: 0x0400537B RID: 21371
		Invert,
		// Token: 0x0400537C RID: 21372
		IncrementWrap,
		// Token: 0x0400537D RID: 21373
		DecrementWrap
	}
}
