﻿using System;

namespace Meige
{
	// Token: 0x02000FCE RID: 4046
	public enum eToneMapWhitePoint
	{
		// Token: 0x04005534 RID: 21812
		eToneMapWhitePoint_ToneLuminanceValue,
		// Token: 0x04005535 RID: 21813
		eToneMapWhitePoint_ToneLuminanceValueMax,
		// Token: 0x04005536 RID: 21814
		eToneMapWhitePoint_Fix
	}
}
