﻿using System;

namespace Meige
{
	// Token: 0x02000F89 RID: 3977
	public enum eBlendMode
	{
		// Token: 0x0400532C RID: 21292
		eBlendMode_Invalid = -1,
		// Token: 0x0400532D RID: 21293
		eBlendMode_None,
		// Token: 0x0400532E RID: 21294
		eBlendMode_Std,
		// Token: 0x0400532F RID: 21295
		eBlendMode_Add,
		// Token: 0x04005330 RID: 21296
		eBlendMode_Sub,
		// Token: 0x04005331 RID: 21297
		eBlendMode_Negative,
		// Token: 0x04005332 RID: 21298
		eBlendMode_SrcOne,
		// Token: 0x04005333 RID: 21299
		eBlendMode_DstOne,
		// Token: 0x04005334 RID: 21300
		eBlendMode_Mul,
		// Token: 0x04005335 RID: 21301
		eBlendMode_Custom,
		// Token: 0x04005336 RID: 21302
		eBlendMode_Max
	}
}
