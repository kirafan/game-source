﻿using System;

namespace Meige
{
	// Token: 0x02000F8C RID: 3980
	public enum eBlendOp
	{
		// Token: 0x0400534D RID: 21325
		Add,
		// Token: 0x0400534E RID: 21326
		Sub,
		// Token: 0x0400534F RID: 21327
		RevSub,
		// Token: 0x04005350 RID: 21328
		Min,
		// Token: 0x04005351 RID: 21329
		Max
	}
}
