﻿using System;

namespace Meige
{
	// Token: 0x02000EF4 RID: 3828
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class EffectRuleFieldAttribute : Attribute
	{
		// Token: 0x06004EDD RID: 20189 RVA: 0x0015D433 File Offset: 0x0015B833
		public EffectRuleFieldAttribute(string path, string fieldName)
		{
			this.m_Path = path;
			this.m_FieldName = fieldName;
		}

		// Token: 0x06004EDE RID: 20190 RVA: 0x0015D449 File Offset: 0x0015B849
		public string GetPath()
		{
			return this.m_Path;
		}

		// Token: 0x06004EDF RID: 20191 RVA: 0x0015D451 File Offset: 0x0015B851
		public string GetFieldName()
		{
			return this.m_FieldName;
		}

		// Token: 0x04004E97 RID: 20119
		private string m_Path;

		// Token: 0x04004E98 RID: 20120
		private string m_FieldName;
	}
}
