﻿using System;

namespace Meige
{
	// Token: 0x02000FEE RID: 4078
	public enum eTrophyResult
	{
		// Token: 0x040055EA RID: 21994
		ErrorInvalidTrophyID = -1,
		// Token: 0x040055EB RID: 21995
		CanNotAccess,
		// Token: 0x040055EC RID: 21996
		Success,
		// Token: 0x040055ED RID: 21997
		Unlocked
	}
}
