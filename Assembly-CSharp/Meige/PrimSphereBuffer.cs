﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F54 RID: 3924
	public class PrimSphereBuffer : MonoBehaviour
	{
		// Token: 0x0600518E RID: 20878 RVA: 0x0016D160 File Offset: 0x0016B560
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x0600518F RID: 20879 RVA: 0x0016D188 File Offset: 0x0016B588
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				IEnumerator enumerator = this.m_SphereBufferList.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						PrimSphereBuffer.SphereBuffer sphereBuffer = (PrimSphereBuffer.SphereBuffer)obj;
						this.m_EffectMeshBuffer.RemoveBuffer(sphereBuffer.meshBuffer);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x06005190 RID: 20880 RVA: 0x0016D21C File Offset: 0x0016B61C
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x06005191 RID: 20881 RVA: 0x0016D224 File Offset: 0x0016B624
		internal void AddActiveBufferList(PrimSphereBuffer.SphereBuffer meshBuffer)
		{
			this.m_activeSphereBufferList.Add(meshBuffer);
		}

		// Token: 0x06005192 RID: 20882 RVA: 0x0016D233 File Offset: 0x0016B633
		internal void RemoveActiveBufferList(PrimSphereBuffer.SphereBuffer meshBuffer)
		{
			this.m_activeSphereBufferList.Remove(meshBuffer);
		}

		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x06005194 RID: 20884 RVA: 0x0016D24A File Offset: 0x0016B64A
		// (set) Token: 0x06005193 RID: 20883 RVA: 0x0016D241 File Offset: 0x0016B641
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x06005196 RID: 20886 RVA: 0x0016D25B File Offset: 0x0016B65B
		// (set) Token: 0x06005195 RID: 20885 RVA: 0x0016D252 File Offset: 0x0016B652
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x06005198 RID: 20888 RVA: 0x0016D26C File Offset: 0x0016B66C
		// (set) Token: 0x06005197 RID: 20887 RVA: 0x0016D263 File Offset: 0x0016B663
		public bool bufferChangeFlg { get; set; }

		// Token: 0x06005199 RID: 20889 RVA: 0x0016D274 File Offset: 0x0016B674
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x0600519A RID: 20890 RVA: 0x0016D27C File Offset: 0x0016B67C
		private void Setup()
		{
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
				this.m_MeshBufferIsSelf = true;
			}
		}

		// Token: 0x0600519B RID: 20891 RVA: 0x0016D2CC File Offset: 0x0016B6CC
		public virtual PrimSphereBuffer.SphereBuffer AddBuffer(int holyzonVtxNum, int verticalDivideNum)
		{
			this.Setup();
			PrimSphereBuffer.SphereBuffer sphereBuffer = new PrimSphereBuffer.SphereBuffer();
			sphereBuffer.holyzonVtxNum = holyzonVtxNum;
			sphereBuffer.verticalDivideNum = verticalDivideNum;
			sphereBuffer.parent = this;
			sphereBuffer.enabled = false;
			this.m_SphereBufferList.Add(sphereBuffer);
			sphereBuffer.UpdatePositions();
			sphereBuffer.UpdateUVs();
			sphereBuffer.UpdateColors();
			this.bufferChangeFlg = true;
			sphereBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(holyzonVtxNum * (verticalDivideNum + 2), (holyzonVtxNum - 1) * (verticalDivideNum + 1) * 4);
			int num = 0;
			for (int i = 0; i < verticalDivideNum + 1; i++)
			{
				int num2 = holyzonVtxNum * i;
				int num3 = holyzonVtxNum * (i + 1);
				for (int j = 0; j < holyzonVtxNum - 1; j++)
				{
					sphereBuffer.meshBuffer.indices[num++] = num2 + j;
					sphereBuffer.meshBuffer.indices[num++] = num3 + j;
					sphereBuffer.meshBuffer.indices[num++] = num3 + j + 1;
					sphereBuffer.meshBuffer.indices[num++] = num2 + j + 1;
				}
			}
			sphereBuffer.meshBuffer.UpdateIndices();
			return sphereBuffer;
		}

		// Token: 0x0600519C RID: 20892 RVA: 0x0016D3E8 File Offset: 0x0016B7E8
		public virtual void RemoveBuffer(PrimSphereBuffer.SphereBuffer sphereBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			sphereBuffer.enabled = false;
			int num = this.m_SphereBufferList.IndexOf(sphereBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(sphereBuffer.meshBuffer);
				sphereBuffer.meshBuffer = null;
				this.m_SphereBufferList.RemoveAt(num);
			}
		}

		// Token: 0x0600519D RID: 20893 RVA: 0x0016D448 File Offset: 0x0016B848
		private void UpdateMesh()
		{
			if (this.m_SphereBufferList.Count == 0)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < this.m_SphereBufferList.Count; i++)
			{
				PrimSphereBuffer.SphereBuffer sphereBuffer = this.m_SphereBufferList[i] as PrimSphereBuffer.SphereBuffer;
				if (sphereBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = sphereBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (sphereBuffer.enabled)
						{
							Quaternion rotation = Quaternion.Euler(sphereBuffer.m_Rotation);
							for (int j = 0; j < sphereBuffer.verticalDivideNum + 2; j++)
							{
								float num2 = (float)j / (float)(sphereBuffer.verticalDivideNum + 1);
								float angle = Mathf.Lerp(sphereBuffer.m_VerticalStartAngle, sphereBuffer.m_VerticalEndAngle, num2);
								int indexFromAngle = CircleVertexBase.GetIndexFromAngle(angle);
								for (int k = 0; k < sphereBuffer.holyzonVtxNum; k++)
								{
									float num3 = (float)k / (float)(sphereBuffer.holyzonVtxNum - 1);
									int num4 = j * sphereBuffer.holyzonVtxNum + k;
									if (sphereBuffer.isDirtyPositions)
									{
										float angle2 = Mathf.Lerp(sphereBuffer.m_HorizonStartAngle, sphereBuffer.m_HorizonEndAngle, num3);
										int indexFromAngle2 = CircleVertexBase.GetIndexFromAngle(angle2);
										meshBuffer.vertices[num4].x = CircleVertexBase.m_Vertices[indexFromAngle2].y * CircleVertexBase.m_Vertices[indexFromAngle].x * sphereBuffer.m_Scale.x;
										meshBuffer.vertices[num4].y = CircleVertexBase.m_Vertices[indexFromAngle].y * sphereBuffer.m_Scale.y;
										meshBuffer.vertices[num4].z = CircleVertexBase.m_Vertices[indexFromAngle2].x * CircleVertexBase.m_Vertices[indexFromAngle].x * sphereBuffer.m_Scale.z;
										meshBuffer.vertices[num4] = rotation * meshBuffer.vertices[num4] + sphereBuffer.m_Position;
									}
									if (sphereBuffer.isDirtyUVs)
									{
										float num5;
										float num6;
										if (sphereBuffer.m_UVReverse)
										{
											num5 = sphereBuffer.m_UVRect.width * num2;
											num6 = sphereBuffer.m_UVRect.height * num3;
										}
										else
										{
											num5 = sphereBuffer.m_UVRect.width * num3;
											num6 = sphereBuffer.m_UVRect.height * num2;
										}
										meshBuffer.UVs[num4].x = sphereBuffer.m_UVRect.x;
										meshBuffer.UVs[num4].y = sphereBuffer.m_UVRect.y;
										meshBuffer.UVs[num4].x = sphereBuffer.m_UVRect.x + num5;
										meshBuffer.UVs[num4].y = sphereBuffer.m_UVRect.y + num6;
									}
									if (sphereBuffer.isDirtyColors)
									{
										Color a = Color.Lerp(sphereBuffer.m_ColorStartVertical, sphereBuffer.m_ColorEndVertical, num2);
										Color b = Color.Lerp(sphereBuffer.m_ColorStartHorizon, sphereBuffer.m_ColorEndHorizon, num3);
										meshBuffer.colors[num4] = a * b;
									}
								}
							}
							if (sphereBuffer.isDirtyPositions)
							{
								meshBuffer.UpdateVertices();
								sphereBuffer.isDirtyPositions = false;
							}
							if (sphereBuffer.isDirtyUVs)
							{
								meshBuffer.UpdateUVs();
								sphereBuffer.isDirtyUVs = false;
							}
							if (sphereBuffer.isDirtyColors)
							{
								meshBuffer.UpdateColors();
								sphereBuffer.isDirtyColors = false;
							}
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x0600519E RID: 20894 RVA: 0x0016D7EE File Offset: 0x0016BBEE
		private void Update()
		{
			this.UpdateMesh();
		}

		// Token: 0x04005097 RID: 20631
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04005098 RID: 20632
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04005099 RID: 20633
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x0400509A RID: 20634
		protected ArrayList m_SphereBufferList = new ArrayList();

		// Token: 0x0400509B RID: 20635
		protected ArrayList m_activeSphereBufferList = new ArrayList(32);

		// Token: 0x02000F55 RID: 3925
		[Serializable]
		public class SphereBuffer
		{
			// Token: 0x060051A0 RID: 20896 RVA: 0x0016D881 File Offset: 0x0016BC81
			public void UpdatePositions()
			{
				this.isDirtyPositions = true;
			}

			// Token: 0x060051A1 RID: 20897 RVA: 0x0016D88A File Offset: 0x0016BC8A
			public void UpdateUVs()
			{
				this.isDirtyUVs = true;
			}

			// Token: 0x060051A2 RID: 20898 RVA: 0x0016D893 File Offset: 0x0016BC93
			public void UpdateColors()
			{
				this.isDirtyColors = true;
			}

			// Token: 0x1700057A RID: 1402
			// (get) Token: 0x060051A3 RID: 20899 RVA: 0x0016D89C File Offset: 0x0016BC9C
			// (set) Token: 0x060051A4 RID: 20900 RVA: 0x0016D8A4 File Offset: 0x0016BCA4
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateColors();
				}
			}

			// Token: 0x0400509E RID: 20638
			internal PrimSphereBuffer parent;

			// Token: 0x0400509F RID: 20639
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x040050A0 RID: 20640
			internal int holyzonVtxNum;

			// Token: 0x040050A1 RID: 20641
			internal int verticalDivideNum;

			// Token: 0x040050A2 RID: 20642
			internal bool isDirtyPositions;

			// Token: 0x040050A3 RID: 20643
			internal bool isDirtyUVs;

			// Token: 0x040050A4 RID: 20644
			internal bool isDirtyColors;

			// Token: 0x040050A5 RID: 20645
			public Vector3 m_Position;

			// Token: 0x040050A6 RID: 20646
			public Vector3 m_Rotation;

			// Token: 0x040050A7 RID: 20647
			public Vector3 m_Scale = Vector3.one * 100f;

			// Token: 0x040050A8 RID: 20648
			public float m_HorizonStartAngle;

			// Token: 0x040050A9 RID: 20649
			public float m_HorizonEndAngle = 360f;

			// Token: 0x040050AA RID: 20650
			public float m_VerticalStartAngle;

			// Token: 0x040050AB RID: 20651
			public float m_VerticalEndAngle = 180f;

			// Token: 0x040050AC RID: 20652
			public Rect m_UVRect = Rect.MinMaxRect(0f, 0f, 1f, 1f);

			// Token: 0x040050AD RID: 20653
			public bool m_UVReverse;

			// Token: 0x040050AE RID: 20654
			public Color m_ColorStartHorizon = Color.white;

			// Token: 0x040050AF RID: 20655
			public Color m_ColorEndHorizon = Color.white;

			// Token: 0x040050B0 RID: 20656
			public Color m_ColorStartVertical = Color.white;

			// Token: 0x040050B1 RID: 20657
			public Color m_ColorEndVertical = Color.white;

			// Token: 0x040050B2 RID: 20658
			private bool m_enabled;
		}
	}
}
