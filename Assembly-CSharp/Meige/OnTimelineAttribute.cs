﻿using System;

namespace Meige
{
	// Token: 0x02000EF1 RID: 3825
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true, Inherited = true)]
	public class OnTimelineAttribute : Attribute
	{
		// Token: 0x06004EDA RID: 20186 RVA: 0x0015D3F1 File Offset: 0x0015B7F1
		public OnTimelineAttribute(string name, string affiliation)
		{
			this.name = name;
			this.affiliation = affiliation;
		}

		// Token: 0x04004E8F RID: 20111
		public string name;

		// Token: 0x04004E90 RID: 20112
		public string affiliation;

		// Token: 0x04004E91 RID: 20113
		public int id;
	}
}
