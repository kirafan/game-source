﻿using System;

namespace Meige
{
	// Token: 0x02000EFD RID: 3837
	[Serializable]
	public class RangeValueVector3 : RangeValue<Float3>
	{
		// Token: 0x06004F1E RID: 20254 RVA: 0x0015DD19 File Offset: 0x0015C119
		public RangeValueVector3()
		{
		}

		// Token: 0x06004F1F RID: 20255 RVA: 0x0015DD21 File Offset: 0x0015C121
		public RangeValueVector3(int num) : base(num)
		{
		}

		// Token: 0x06004F20 RID: 20256 RVA: 0x0015DD2A File Offset: 0x0015C12A
		public RangeValueVector3(RangeValueVector3 v) : base(v)
		{
		}

		// Token: 0x06004F21 RID: 20257 RVA: 0x0015DD34 File Offset: 0x0015C134
		public new RangeValueVector3 Clone()
		{
			return new RangeValueVector3(this);
		}
	}
}
