﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000E9A RID: 3738
	public class EffectLocator : MonoBehaviour
	{
		// Token: 0x06004DCF RID: 19919 RVA: 0x0015A45C File Offset: 0x0015885C
		public void Reset()
		{
			if (this.m_ThisTransform == null)
			{
				return;
			}
			this.m_ThisTransform.localPosition = Vector3.zero;
			this.m_ThisTransform.localRotation = Quaternion.identity;
			this.m_ThisTransform.localScale = Vector3.one;
		}

		// Token: 0x06004DD0 RID: 19920 RVA: 0x0015A4AB File Offset: 0x001588AB
		public void SetOwner(GameObject go)
		{
			this.m_OwnerGO = go;
		}

		// Token: 0x06004DD1 RID: 19921 RVA: 0x0015A4B4 File Offset: 0x001588B4
		public GameObject GetOwner()
		{
			return this.m_OwnerGO;
		}

		// Token: 0x06004DD2 RID: 19922 RVA: 0x0015A4BC File Offset: 0x001588BC
		public Transform GetTransform()
		{
			return this.m_ThisTransform;
		}

		// Token: 0x06004DD3 RID: 19923 RVA: 0x0015A4C4 File Offset: 0x001588C4
		private void Awake()
		{
			this.m_ThisTransform = base.transform;
		}

		// Token: 0x06004DD4 RID: 19924 RVA: 0x0015A4D2 File Offset: 0x001588D2
		private void Update()
		{
		}

		// Token: 0x04004E04 RID: 19972
		private Transform m_ThisTransform;

		// Token: 0x04004E05 RID: 19973
		private GameObject m_OwnerGO;
	}
}
