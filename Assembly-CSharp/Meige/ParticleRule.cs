﻿using System;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F14 RID: 3860
	[Serializable]
	public class ParticleRule
	{
		// Token: 0x060050A7 RID: 20647 RVA: 0x00167F8E File Offset: 0x0016638E
		public int GetPropertyNum()
		{
			return 44;
		}

		// Token: 0x060050A8 RID: 20648 RVA: 0x00167F94 File Offset: 0x00166394
		public int GetArrayNum(int propertyIdx)
		{
			if (propertyIdx != 41)
			{
				if (propertyIdx != 43)
				{
					return 1;
				}
				if (this.m_PathMoveComponent != null && this.m_PathMoveComponent.m_FocusRadiusRange != null)
				{
					return this.m_PathMoveComponent.m_FocusRadiusRange.Length;
				}
			}
			else if (this.m_ColorCurveComponent != null && this.m_ColorCurveComponent.m_pColorCurveArray != null)
			{
				return this.m_ColorCurveComponent.m_pColorCurveArray.Length;
			}
			return 0;
		}

		// Token: 0x060050A9 RID: 20649 RVA: 0x00168018 File Offset: 0x00166418
		public object GetValue(int propertyIdx, int arrayIdx)
		{
			switch (propertyIdx)
			{
			case 0:
				return this.m_particleTypeParam.m_billboard.m_widthRange;
			case 1:
				return this.m_particleTypeParam.m_billboard.m_heightRange;
			case 2:
				return this.m_particleTypeParam.m_point.m_sizeRange;
			case 3:
				return this.m_particleTypeParam.m_line.m_width;
			case 4:
				return this.m_particleTypeParam.m_polyLine.m_topWidthRange;
			case 5:
				return this.m_particleTypeParam.m_polyLine.m_endWidthRange;
			case 6:
				return this.m_particleTypeParam.m_confetti.m_widthRange;
			case 7:
				return this.m_particleTypeParam.m_confetti.m_heightRange;
			case 8:
				return this.m_emitionParam.m_point.m_angleRange;
			case 9:
				return this.m_emitionParam.m_box.m_widthRange;
			case 10:
				return this.m_emitionParam.m_box.m_heightRange;
			case 11:
				return this.m_emitionParam.m_box.m_depthRange;
			case 12:
				return this.m_emitionParam.m_planeQuad.m_widthRange;
			case 13:
				return this.m_emitionParam.m_planeQuad.m_heightRange;
			case 14:
				return this.m_emitionParam.m_planeCircle.m_radiusRange;
			case 15:
				return this.m_emitionParam.m_sphere.m_angleRange;
			case 16:
				return this.m_emitionParam.m_sphere.m_radiusRange;
			case 17:
				return this.m_emitionParam.m_torus.m_angleRange;
			case 18:
				return this.m_emitionParam.m_torus.m_bigRadiusRange;
			case 19:
				return this.m_emitionParam.m_torus.m_smallRadiusRange;
			case 20:
				return this.m_emitionParam.m_cylinder.m_RadiusRange;
			case 21:
				return this.m_emitionParam.m_cylinder.m_HeightRange;
			case 22:
				return this.m_gravityDir;
			case 23:
				return this.m_gravityForceRange;
			case 24:
				return this.m_lifeSpanType;
			case 25:
				return this.m_lifeSpanAlpha;
			case 26:
				return this.m_LifeSpanParam.m_Time.m_lifeSpanSecRange;
			case 27:
				return this.m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange;
			case 28:
				return this.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange;
			case 29:
				return this.m_incidentNumberPerSec;
			case 30:
				return this.m_incidentRandomLevel;
			case 31:
				return this.m_HDR_Factor;
			case 32:
				return this.m_isLocalTrans;
			case 33:
				return this.m_isRandomEmitDir;
			case 34:
				return this.m_AccelerationComponent.m_accelerationRange;
			case 35:
				return this.m_AccelerationComponent.m_dragForceRange;
			case 36:
				return this.m_RotationComponent.m_rotSpeedRange;
			case 37:
				return this.m_RotationComponent.m_rotAccelerationRange;
			case 38:
				return this.m_RotationComponent.m_rotDragForceRange;
			case 39:
				return this.m_RotationComponent.m_rotAnchorOffsetRange;
			case 40:
				return this.m_RotationComponent.m_rotRange;
			case 41:
				return this.m_ColorCurveComponent.m_pColorCurveArray[arrayIdx];
			case 42:
				return this.m_BlinkComponent.m_blinkSpanSecRange;
			case 43:
				return this.m_PathMoveComponent.m_FocusRadiusRange[arrayIdx];
			default:
				return null;
			}
		}

		// Token: 0x060050AA RID: 20650 RVA: 0x00168378 File Offset: 0x00166778
		public void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
		{
			switch (propertyIdx)
			{
			case 0:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_billboard.m_widthRange, tgtIdx, value);
				break;
			case 1:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_billboard.m_heightRange, tgtIdx, value);
				break;
			case 2:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_point.m_sizeRange, tgtIdx, value);
				break;
			case 3:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_line.m_width, tgtIdx, value);
				break;
			case 4:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_polyLine.m_topWidthRange, tgtIdx, value);
				break;
			case 5:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_polyLine.m_endWidthRange, tgtIdx, value);
				break;
			case 6:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_confetti.m_widthRange, tgtIdx, value);
				break;
			case 7:
				EffectHelper.Copy(ref this.m_particleTypeParam.m_confetti.m_heightRange, tgtIdx, value);
				break;
			case 8:
				EffectHelper.Copy(ref this.m_emitionParam.m_point.m_angleRange, tgtIdx, value);
				break;
			case 9:
				EffectHelper.Copy(ref this.m_emitionParam.m_box.m_widthRange, tgtIdx, value);
				break;
			case 10:
				EffectHelper.Copy(ref this.m_emitionParam.m_box.m_heightRange, tgtIdx, value);
				break;
			case 11:
				EffectHelper.Copy(ref this.m_emitionParam.m_box.m_depthRange, tgtIdx, value);
				break;
			case 12:
				EffectHelper.Copy(ref this.m_emitionParam.m_planeQuad.m_widthRange, tgtIdx, value);
				break;
			case 13:
				EffectHelper.Copy(ref this.m_emitionParam.m_planeQuad.m_heightRange, tgtIdx, value);
				break;
			case 14:
				EffectHelper.Copy(ref this.m_emitionParam.m_planeCircle.m_radiusRange, tgtIdx, value);
				break;
			case 15:
				EffectHelper.Copy(ref this.m_emitionParam.m_sphere.m_angleRange, tgtIdx, value);
				break;
			case 16:
				EffectHelper.Copy(ref this.m_emitionParam.m_sphere.m_radiusRange, tgtIdx, value);
				break;
			case 17:
				EffectHelper.Copy(ref this.m_emitionParam.m_torus.m_angleRange, tgtIdx, value);
				break;
			case 18:
				EffectHelper.Copy(ref this.m_emitionParam.m_torus.m_bigRadiusRange, tgtIdx, value);
				break;
			case 19:
				EffectHelper.Copy(ref this.m_emitionParam.m_torus.m_smallRadiusRange, tgtIdx, value);
				break;
			case 20:
				EffectHelper.Copy(ref this.m_emitionParam.m_cylinder.m_RadiusRange, tgtIdx, value);
				break;
			case 21:
				EffectHelper.Copy(ref this.m_emitionParam.m_cylinder.m_HeightRange, tgtIdx, value);
				break;
			case 22:
				EffectHelper.Copy(ref this.m_gravityDir, tgtIdx, value);
				break;
			case 23:
				EffectHelper.Copy(ref this.m_gravityForceRange, tgtIdx, value);
				break;
			case 24:
			{
				int num = (int)this.m_lifeSpanType;
				EffectHelper.Copy(ref num, tgtIdx, value);
				this.m_lifeSpanType = (eParticleLifeSpanType)num;
				break;
			}
			case 25:
			{
				int num = (int)this.m_lifeSpanAlpha;
				EffectHelper.Copy(ref num, tgtIdx, value);
				this.m_lifeSpanAlpha = (eParticleLifeSpanAlpha)num;
				break;
			}
			case 26:
				EffectHelper.Copy(ref this.m_LifeSpanParam.m_Time.m_lifeSpanSecRange, tgtIdx, value);
				break;
			case 27:
				EffectHelper.Copy(ref this.m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange, tgtIdx, value);
				break;
			case 28:
				EffectHelper.Copy(ref this.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange, tgtIdx, value);
				break;
			case 29:
				EffectHelper.Copy(ref this.m_incidentNumberPerSec, tgtIdx, value);
				break;
			case 30:
				EffectHelper.Copy(ref this.m_incidentRandomLevel, tgtIdx, value);
				break;
			case 31:
				EffectHelper.Copy(ref this.m_HDR_Factor, tgtIdx, value);
				break;
			case 32:
				EffectHelper.Copy(ref this.m_isLocalTrans, tgtIdx, value);
				break;
			case 33:
				EffectHelper.Copy(ref this.m_isRandomEmitDir, tgtIdx, value);
				break;
			case 34:
				EffectHelper.Copy(ref this.m_AccelerationComponent.m_accelerationRange, tgtIdx, value);
				break;
			case 35:
				EffectHelper.Copy(ref this.m_AccelerationComponent.m_dragForceRange, tgtIdx, value);
				break;
			case 36:
				EffectHelper.Copy(ref this.m_RotationComponent.m_rotSpeedRange, tgtIdx, value);
				break;
			case 37:
				EffectHelper.Copy(ref this.m_RotationComponent.m_rotAccelerationRange, tgtIdx, value);
				break;
			case 38:
				EffectHelper.Copy(ref this.m_RotationComponent.m_rotDragForceRange, tgtIdx, value);
				break;
			case 39:
				EffectHelper.Copy(ref this.m_RotationComponent.m_rotAnchorOffsetRange, tgtIdx, value);
				break;
			case 40:
				EffectHelper.Copy(ref this.m_RotationComponent.m_rotRange, tgtIdx, value);
				break;
			case 41:
				EffectHelper.Copy(ref this.m_ColorCurveComponent.m_pColorCurveArray[arrayIdx], tgtIdx, value);
				break;
			case 42:
				EffectHelper.Copy(ref this.m_BlinkComponent.m_blinkSpanSecRange, tgtIdx, value);
				break;
			case 43:
				EffectHelper.Copy(ref this.m_PathMoveComponent.m_FocusRadiusRange[arrayIdx], tgtIdx, value);
				break;
			}
		}

		// Token: 0x060050AB RID: 20651 RVA: 0x001688D4 File Offset: 0x00166CD4
		public void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int compoentIdx, float value)
		{
			switch (propertyIdx)
			{
			case 0:
				EffectHelper.Set(ref this.m_particleTypeParam.m_billboard.m_widthRange, tgtIdx, compoentIdx, value);
				break;
			case 1:
				EffectHelper.Set(ref this.m_particleTypeParam.m_billboard.m_heightRange, tgtIdx, compoentIdx, value);
				break;
			case 2:
				EffectHelper.Set(ref this.m_particleTypeParam.m_point.m_sizeRange, tgtIdx, compoentIdx, value);
				break;
			case 3:
				EffectHelper.Set(ref this.m_particleTypeParam.m_line.m_width, tgtIdx, compoentIdx, value);
				break;
			case 4:
				EffectHelper.Set(ref this.m_particleTypeParam.m_polyLine.m_topWidthRange, tgtIdx, compoentIdx, value);
				break;
			case 5:
				EffectHelper.Set(ref this.m_particleTypeParam.m_polyLine.m_endWidthRange, tgtIdx, compoentIdx, value);
				break;
			case 6:
				EffectHelper.Set(ref this.m_particleTypeParam.m_confetti.m_widthRange, tgtIdx, compoentIdx, value);
				break;
			case 7:
				EffectHelper.Set(ref this.m_particleTypeParam.m_confetti.m_heightRange, tgtIdx, compoentIdx, value);
				break;
			case 8:
				EffectHelper.Set(ref this.m_emitionParam.m_point.m_angleRange, tgtIdx, compoentIdx, value);
				break;
			case 9:
				EffectHelper.Set(ref this.m_emitionParam.m_box.m_widthRange, tgtIdx, compoentIdx, value);
				break;
			case 10:
				EffectHelper.Set(ref this.m_emitionParam.m_box.m_heightRange, tgtIdx, compoentIdx, value);
				break;
			case 11:
				EffectHelper.Set(ref this.m_emitionParam.m_box.m_depthRange, tgtIdx, compoentIdx, value);
				break;
			case 12:
				EffectHelper.Set(ref this.m_emitionParam.m_planeQuad.m_widthRange, tgtIdx, compoentIdx, value);
				break;
			case 13:
				EffectHelper.Set(ref this.m_emitionParam.m_planeQuad.m_heightRange, tgtIdx, compoentIdx, value);
				break;
			case 14:
				EffectHelper.Set(ref this.m_emitionParam.m_planeCircle.m_radiusRange, tgtIdx, compoentIdx, value);
				break;
			case 15:
				EffectHelper.Set(ref this.m_emitionParam.m_sphere.m_angleRange, tgtIdx, compoentIdx, value);
				break;
			case 16:
				EffectHelper.Set(ref this.m_emitionParam.m_sphere.m_radiusRange, tgtIdx, compoentIdx, value);
				break;
			case 17:
				EffectHelper.Set(ref this.m_emitionParam.m_torus.m_angleRange, tgtIdx, compoentIdx, value);
				break;
			case 18:
				EffectHelper.Set(ref this.m_emitionParam.m_torus.m_bigRadiusRange, tgtIdx, compoentIdx, value);
				break;
			case 19:
				EffectHelper.Set(ref this.m_emitionParam.m_torus.m_smallRadiusRange, tgtIdx, compoentIdx, value);
				break;
			case 20:
				EffectHelper.Set(ref this.m_emitionParam.m_cylinder.m_RadiusRange, tgtIdx, compoentIdx, value);
				break;
			case 21:
				EffectHelper.Set(ref this.m_emitionParam.m_cylinder.m_HeightRange, tgtIdx, compoentIdx, value);
				break;
			case 22:
				EffectHelper.Set(ref this.m_gravityDir, tgtIdx, compoentIdx, value);
				break;
			case 23:
				EffectHelper.Set(ref this.m_gravityForceRange, tgtIdx, compoentIdx, value);
				break;
			case 24:
			{
				int num = (int)this.m_lifeSpanType;
				EffectHelper.Copy(ref num, tgtIdx, value);
				this.m_lifeSpanType = (eParticleLifeSpanType)num;
				break;
			}
			case 25:
			{
				int num = (int)this.m_lifeSpanAlpha;
				EffectHelper.Copy(ref num, tgtIdx, value);
				this.m_lifeSpanAlpha = (eParticleLifeSpanAlpha)num;
				break;
			}
			case 26:
				EffectHelper.Set(ref this.m_LifeSpanParam.m_Time.m_lifeSpanSecRange, tgtIdx, compoentIdx, value);
				break;
			case 27:
				EffectHelper.Set(ref this.m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange, tgtIdx, compoentIdx, value);
				break;
			case 28:
				EffectHelper.Set(ref this.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange, tgtIdx, compoentIdx, value);
				break;
			case 29:
				EffectHelper.Set(ref this.m_incidentNumberPerSec, tgtIdx, compoentIdx, value);
				break;
			case 30:
				EffectHelper.Set(ref this.m_incidentRandomLevel, tgtIdx, compoentIdx, value);
				break;
			case 31:
				EffectHelper.Set(ref this.m_HDR_Factor, tgtIdx, compoentIdx, value);
				break;
			case 32:
				EffectHelper.Set(ref this.m_isLocalTrans, tgtIdx, compoentIdx, value);
				break;
			case 33:
				EffectHelper.Set(ref this.m_isRandomEmitDir, tgtIdx, compoentIdx, value);
				break;
			case 34:
				EffectHelper.Set(ref this.m_AccelerationComponent.m_accelerationRange, tgtIdx, compoentIdx, value);
				break;
			case 35:
				EffectHelper.Set(ref this.m_AccelerationComponent.m_dragForceRange, tgtIdx, compoentIdx, value);
				break;
			case 36:
				EffectHelper.Set(ref this.m_RotationComponent.m_rotSpeedRange, tgtIdx, compoentIdx, value);
				break;
			case 37:
				EffectHelper.Set(ref this.m_RotationComponent.m_rotAccelerationRange, tgtIdx, compoentIdx, value);
				break;
			case 38:
				EffectHelper.Set(ref this.m_RotationComponent.m_rotDragForceRange, tgtIdx, compoentIdx, value);
				break;
			case 39:
				EffectHelper.Set(ref this.m_RotationComponent.m_rotAnchorOffsetRange, tgtIdx, compoentIdx, value);
				break;
			case 40:
				EffectHelper.Set(ref this.m_RotationComponent.m_rotRange, tgtIdx, compoentIdx, value);
				break;
			case 41:
				EffectHelper.Set(ref this.m_ColorCurveComponent.m_pColorCurveArray[arrayIdx], tgtIdx, compoentIdx, value);
				break;
			case 42:
				EffectHelper.Set(ref this.m_BlinkComponent.m_blinkSpanSecRange, tgtIdx, compoentIdx, value);
				break;
			case 43:
				EffectHelper.Set(ref this.m_PathMoveComponent.m_FocusRadiusRange[arrayIdx], tgtIdx, compoentIdx, value);
				break;
			}
		}

		// Token: 0x060050AC RID: 20652 RVA: 0x00168E8C File Offset: 0x0016728C
		public eEffectAnimTypeCode GetTypeCode(int propertyIdx)
		{
			return ParticleRule.m_Test[propertyIdx].m_TypeCode;
		}

		// Token: 0x060050AD RID: 20653 RVA: 0x00168EA0 File Offset: 0x001672A0
		public Type GetType(int propertyIdx)
		{
			eEffectAnimTypeCode typeCode = this.GetTypeCode(propertyIdx);
			if (typeCode == eEffectAnimTypeCode.Enum)
			{
				if (propertyIdx == 24)
				{
					return typeof(eParticleLifeSpanType);
				}
				if (propertyIdx == 25)
				{
					return typeof(eParticleLifeSpanAlpha);
				}
			}
			return EffectHelper.GetTypeFromTypeCode(typeCode);
		}

		// Token: 0x060050AE RID: 20654 RVA: 0x00168EEE File Offset: 0x001672EE
		public string GetPropertyName(int propertyIdx)
		{
			return ParticleRule.m_Test[propertyIdx].m_PropertyName;
		}

		// Token: 0x04004EF4 RID: 20212
		public int m_particleNum = 1;

		// Token: 0x04004EF5 RID: 20213
		public ParticleRule.ParticleTypeParam m_particleTypeParam = new ParticleRule.ParticleTypeParam();

		// Token: 0x04004EF6 RID: 20214
		public eParticleType m_particleType;

		// Token: 0x04004EF7 RID: 20215
		public ParticleRule.EmitionParam m_emitionParam = new ParticleRule.EmitionParam();

		// Token: 0x04004EF8 RID: 20216
		public eParticleEmitionType m_emitionType;

		// Token: 0x04004EF9 RID: 20217
		public Rect m_uvRect_TopBlock;

		// Token: 0x04004EFA RID: 20218
		public int m_uvBlockNum;

		// Token: 0x04004EFB RID: 20219
		public eParticleLifeScaleType m_LifeScaleType;

		// Token: 0x04004EFC RID: 20220
		public RangePareFloat m_lifeScaleRange = new RangePareFloat();

		// Token: 0x04004EFD RID: 20221
		public RangePareFloat m_speedRange = new RangePareFloat();

		// Token: 0x04004EFE RID: 20222
		public Vector3 m_gravityDir = new Vector3(0f, -1f, 0f);

		// Token: 0x04004EFF RID: 20223
		public RangePareFloat m_gravityForceRange = new RangePareFloat();

		// Token: 0x04004F00 RID: 20224
		public eParticleLifeSpanType m_lifeSpanType;

		// Token: 0x04004F01 RID: 20225
		public eParticleLifeSpanAlpha m_lifeSpanAlpha;

		// Token: 0x04004F02 RID: 20226
		public ParticleRule.LifeSpanParam m_LifeSpanParam = new ParticleRule.LifeSpanParam();

		// Token: 0x04004F03 RID: 20227
		public eParticleCollisionType m_collisionType;

		// Token: 0x04004F04 RID: 20228
		public ParticleRule.CollisionTypeParam m_collisionParam = new ParticleRule.CollisionTypeParam();

		// Token: 0x04004F05 RID: 20229
		public float m_incidentNumberPerSec;

		// Token: 0x04004F06 RID: 20230
		public float m_incidentRandomLevel;

		// Token: 0x04004F07 RID: 20231
		public float m_HDR_Factor = 1f;

		// Token: 0x04004F08 RID: 20232
		public float m_AlphaScale = 1f;

		// Token: 0x04004F09 RID: 20233
		public float m_frameSpeedScale = 1f;

		// Token: 0x04004F0A RID: 20234
		public bool m_usingLight;

		// Token: 0x04004F0B RID: 20235
		public bool m_isLocalTrans;

		// Token: 0x04004F0C RID: 20236
		public bool m_isRandomEmitDir;

		// Token: 0x04004F0D RID: 20237
		public bool m_UsingAccelerationFlg;

		// Token: 0x04004F0E RID: 20238
		public ParticleRule.AccelerationComponent m_AccelerationComponent;

		// Token: 0x04004F0F RID: 20239
		public bool m_UsingRotationFlg;

		// Token: 0x04004F10 RID: 20240
		public ParticleRule.RotationComponent m_RotationComponent;

		// Token: 0x04004F11 RID: 20241
		public bool m_UsingUVAnimationFlg;

		// Token: 0x04004F12 RID: 20242
		public ParticleRule.UVAnimationComponent m_UVAnimationComponent;

		// Token: 0x04004F13 RID: 20243
		public bool m_UsingColorCurveFlg;

		// Token: 0x04004F14 RID: 20244
		public ParticleRule.ColorCurveComponent m_ColorCurveComponent;

		// Token: 0x04004F15 RID: 20245
		public bool m_UsingTailFlg;

		// Token: 0x04004F16 RID: 20246
		public ParticleRule.TailComponent m_TailComponent;

		// Token: 0x04004F17 RID: 20247
		public bool m_UsingBlinkFlg;

		// Token: 0x04004F18 RID: 20248
		public ParticleRule.BlinkComponent m_BlinkComponent;

		// Token: 0x04004F19 RID: 20249
		public bool m_UsingPathMoveFlg;

		// Token: 0x04004F1A RID: 20250
		public ParticleRule.PathMoveComponent m_PathMoveComponent;

		// Token: 0x04004F1B RID: 20251
		private static ParticleRule.Test[] m_Test = new ParticleRule.Test[]
		{
			new ParticleRule.Test("TypeParamBillboardWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamBillboardHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamPointSizeRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamLineWidth", eEffectAnimTypeCode.Float),
			new ParticleRule.Test("TypeParamPolyLineTopWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamPolyLineEndWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamConfettiWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("TypeParamConfettiHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamPointAngleRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamBoxWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamBoxHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamBoxDepthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamPlaneQuadWidthRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamPlaneQuadHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamPlaneCircleRadiusRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamSphereAngleRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamSphereRadiusRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamTorusAngleRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamTorusBigRadius", eEffectAnimTypeCode.Float),
			new ParticleRule.Test("EmitParamTorusSmallRadiusRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamCylinderRadiusRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("EmitParamCylinderHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("GravityDir", eEffectAnimTypeCode.Vector3),
			new ParticleRule.Test("GravityForceRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("LifeSpanType", eEffectAnimTypeCode.Enum),
			new ParticleRule.Test("LifeSpanAlpha", eEffectAnimTypeCode.Enum),
			new ParticleRule.Test("LifeSpanTimeRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("LifeSpanDistanceMaxRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("LifeSpanHeightRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("IncidentNumberPerSec", eEffectAnimTypeCode.Float),
			new ParticleRule.Test("IncidentRandomLevel", eEffectAnimTypeCode.Float),
			new ParticleRule.Test("HDRFactor", eEffectAnimTypeCode.Float),
			new ParticleRule.Test("IsLocalTrans", eEffectAnimTypeCode.Bool),
			new ParticleRule.Test("IsRandomEmitDir", eEffectAnimTypeCode.Bool),
			new ParticleRule.Test("AccelerationAccelerationRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("AccelerationDragForceRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("RotationRotSpeedRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("RotationRotAccelerationRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("RotationRotDragForceRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("RotationRotAnchorOffsetRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("RotationRotRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("ColorCurveColorCurve", eEffectAnimTypeCode.RangeValueColor),
			new ParticleRule.Test("BlinkBlinkSpanRange", eEffectAnimTypeCode.RangePareFloat),
			new ParticleRule.Test("PathMoveFocusRadiusRange", eEffectAnimTypeCode.RangePareFloat)
		};

		// Token: 0x02000F15 RID: 3861
		[Serializable]
		public class ParticleTypeParam
		{
			// Token: 0x04004F1C RID: 20252
			public ParticleRule.ParticleTypeParam.Billboard m_billboard;

			// Token: 0x04004F1D RID: 20253
			public ParticleRule.ParticleTypeParam.Point m_point;

			// Token: 0x04004F1E RID: 20254
			public ParticleRule.ParticleTypeParam.Line m_line;

			// Token: 0x04004F1F RID: 20255
			public ParticleRule.ParticleTypeParam.PolyLine m_polyLine;

			// Token: 0x04004F20 RID: 20256
			public ParticleRule.ParticleTypeParam.Confetti m_confetti;

			// Token: 0x04004F21 RID: 20257
			public ParticleRule.ParticleTypeParam.Ribbon m_ribbon;

			// Token: 0x02000F16 RID: 3862
			[Serializable]
			public class Billboard
			{
				// Token: 0x04004F22 RID: 20258
				public RangePareFloat m_widthRange = new RangePareFloat();

				// Token: 0x04004F23 RID: 20259
				public RangePareFloat m_heightRange = new RangePareFloat();

				// Token: 0x04004F24 RID: 20260
				public PrimBillboardBuffer.eRotateType m_RotateType;
			}

			// Token: 0x02000F17 RID: 3863
			[Serializable]
			public class Point
			{
				// Token: 0x04004F25 RID: 20261
				public RangePareFloat m_sizeRange = new RangePareFloat();
			}

			// Token: 0x02000F18 RID: 3864
			[Serializable]
			public class Line
			{
				// Token: 0x04004F26 RID: 20262
				public float m_width = 1f;

				// Token: 0x04004F27 RID: 20263
				public int m_jointNum;

				// Token: 0x04004F28 RID: 20264
				public int m_HistoryPointNum;
			}

			// Token: 0x02000F19 RID: 3865
			[Serializable]
			public class PolyLine
			{
				// Token: 0x04004F29 RID: 20265
				public RangePareFloat m_topWidthRange = new RangePareFloat();

				// Token: 0x04004F2A RID: 20266
				public RangePareFloat m_endWidthRange = new RangePareFloat();

				// Token: 0x04004F2B RID: 20267
				public int m_jointNum;

				// Token: 0x04004F2C RID: 20268
				public int m_HistoryPointNum;
			}

			// Token: 0x02000F1A RID: 3866
			[Serializable]
			public class Confetti
			{
				// Token: 0x04004F2D RID: 20269
				public RangePareFloat m_widthRange = new RangePareFloat();

				// Token: 0x04004F2E RID: 20270
				public RangePareFloat m_heightRange = new RangePareFloat();
			}

			// Token: 0x02000F1B RID: 3867
			[Serializable]
			public class Ribbon
			{
				// Token: 0x04004F2F RID: 20271
				public RangePareFloat m_topWidthRange = new RangePareFloat();

				// Token: 0x04004F30 RID: 20272
				public RangePareFloat m_endWidthRange = new RangePareFloat();

				// Token: 0x04004F31 RID: 20273
				public int m_jointNum;

				// Token: 0x04004F32 RID: 20274
				public int m_HistoryPointNum;
			}
		}

		// Token: 0x02000F1C RID: 3868
		[Serializable]
		public class EmitionParam
		{
			// Token: 0x04004F33 RID: 20275
			public ParticleRule.EmitionParam.Point m_point;

			// Token: 0x04004F34 RID: 20276
			public ParticleRule.EmitionParam.Box m_box;

			// Token: 0x04004F35 RID: 20277
			public ParticleRule.EmitionParam.PlaneQuad m_planeQuad;

			// Token: 0x04004F36 RID: 20278
			public ParticleRule.EmitionParam.PlaneCircle m_planeCircle;

			// Token: 0x04004F37 RID: 20279
			public ParticleRule.EmitionParam.Sphere m_sphere;

			// Token: 0x04004F38 RID: 20280
			public ParticleRule.EmitionParam.Torus m_torus;

			// Token: 0x04004F39 RID: 20281
			public ParticleRule.EmitionParam.Cylinder m_cylinder;

			// Token: 0x02000F1D RID: 3869
			[Serializable]
			public class Point
			{
				// Token: 0x04004F3A RID: 20282
				public RangePareFloat m_angleRange = new RangePareFloat();
			}

			// Token: 0x02000F1E RID: 3870
			[Serializable]
			public class Box
			{
				// Token: 0x04004F3B RID: 20283
				public RangePareFloat m_widthRange = new RangePareFloat();

				// Token: 0x04004F3C RID: 20284
				public RangePareFloat m_heightRange = new RangePareFloat();

				// Token: 0x04004F3D RID: 20285
				public RangePareFloat m_depthRange = new RangePareFloat();
			}

			// Token: 0x02000F1F RID: 3871
			[Serializable]
			public class PlaneQuad
			{
				// Token: 0x04004F3E RID: 20286
				public RangePareFloat m_widthRange = new RangePareFloat();

				// Token: 0x04004F3F RID: 20287
				public RangePareFloat m_heightRange = new RangePareFloat();
			}

			// Token: 0x02000F20 RID: 3872
			[Serializable]
			public class PlaneCircle
			{
				// Token: 0x04004F40 RID: 20288
				public RangePareFloat m_radiusRange = new RangePareFloat();
			}

			// Token: 0x02000F21 RID: 3873
			[Serializable]
			public class Sphere
			{
				// Token: 0x04004F41 RID: 20289
				public RangePareFloat m_angleRange = new RangePareFloat();

				// Token: 0x04004F42 RID: 20290
				public RangePareFloat m_radiusRange = new RangePareFloat();
			}

			// Token: 0x02000F22 RID: 3874
			[Serializable]
			public class Torus
			{
				// Token: 0x04004F43 RID: 20291
				public RangePareFloat m_angleRange = new RangePareFloat();

				// Token: 0x04004F44 RID: 20292
				public float m_bigRadiusRange;

				// Token: 0x04004F45 RID: 20293
				public RangePareFloat m_smallRadiusRange = new RangePareFloat();
			}

			// Token: 0x02000F23 RID: 3875
			[Serializable]
			public class Cylinder
			{
				// Token: 0x04004F46 RID: 20294
				public RangePareFloat m_RadiusRange = new RangePareFloat();

				// Token: 0x04004F47 RID: 20295
				public RangePareFloat m_HeightRange = new RangePareFloat();
			}
		}

		// Token: 0x02000F24 RID: 3876
		[Serializable]
		public class LifeSpanParam
		{
			// Token: 0x04004F48 RID: 20296
			public ParticleRule.LifeSpanParam.Time m_Time;

			// Token: 0x04004F49 RID: 20297
			public ParticleRule.LifeSpanParam.Distance m_Distance;

			// Token: 0x04004F4A RID: 20298
			public ParticleRule.LifeSpanParam.Height m_Height;

			// Token: 0x02000F25 RID: 3877
			[Serializable]
			public class Time
			{
				// Token: 0x04004F4B RID: 20299
				public RangePareFloat m_lifeSpanSecRange = new RangePareFloat();
			}

			// Token: 0x02000F26 RID: 3878
			[Serializable]
			public class Distance
			{
				// Token: 0x04004F4C RID: 20300
				public RangePareFloat m_lifeSpanDistanceMaxRange = new RangePareFloat();
			}

			// Token: 0x02000F27 RID: 3879
			[Serializable]
			public class Height
			{
				// Token: 0x04004F4D RID: 20301
				public RangePareFloat m_lifeSpanHeightRange = new RangePareFloat();
			}
		}

		// Token: 0x02000F28 RID: 3880
		[Serializable]
		public class CollisionTypeParam
		{
			// Token: 0x04004F4E RID: 20302
			public ParticleRule.CollisionTypeParam.Height m_height;

			// Token: 0x04004F4F RID: 20303
			public ParticleRule.CollisionTypeParam.Collision m_collision;

			// Token: 0x02000F29 RID: 3881
			[Serializable]
			public struct Height
			{
				// Token: 0x04004F50 RID: 20304
				public float m_height;

				// Token: 0x04004F51 RID: 20305
				public float m_reflectionCoefficient;

				// Token: 0x04004F52 RID: 20306
				public float m_frictionCoefficient;

				// Token: 0x04004F53 RID: 20307
				public bool m_isLocalHeight;
			}

			// Token: 0x02000F2A RID: 3882
			[Serializable]
			public struct Collision
			{
				// Token: 0x04004F54 RID: 20308
				public float m_reflectionCoefficient;
			}
		}

		// Token: 0x02000F2B RID: 3883
		[Serializable]
		public class AccelerationComponent
		{
			// Token: 0x04004F55 RID: 20309
			public RangePareFloat m_accelerationRange = new RangePareFloat();

			// Token: 0x04004F56 RID: 20310
			public RangePareFloat m_dragForceRange = new RangePareFloat();
		}

		// Token: 0x02000F2C RID: 3884
		[Serializable]
		public class RotationComponent
		{
			// Token: 0x04004F57 RID: 20311
			public RangePareFloat m_rotSpeedRange = new RangePareFloat();

			// Token: 0x04004F58 RID: 20312
			public RangePareFloat m_rotAccelerationRange = new RangePareFloat();

			// Token: 0x04004F59 RID: 20313
			public RangePareFloat m_rotDragForceRange = new RangePareFloat();

			// Token: 0x04004F5A RID: 20314
			public RangePareFloat m_rotAnchorOffsetRange = new RangePareFloat();

			// Token: 0x04004F5B RID: 20315
			public RangePareFloat m_rotRange = new RangePareFloat();
		}

		// Token: 0x02000F2D RID: 3885
		[Serializable]
		public class UVAnimationComponent
		{
			// Token: 0x04004F5C RID: 20316
			public eParticleTextureAnim m_uvAnimeType;

			// Token: 0x04004F5D RID: 20317
			public RangePareFloat m_switchBlockSecRange = new RangePareFloat();

			// Token: 0x04004F5E RID: 20318
			public bool m_randomStartBlockFlg;
		}

		// Token: 0x02000F2E RID: 3886
		[Serializable]
		public class ColorCurveComponent
		{
			// Token: 0x04004F5F RID: 20319
			public RangeValueColor[] m_pColorCurveArray;

			// Token: 0x04004F60 RID: 20320
			public int m_colorCurveNum;
		}

		// Token: 0x02000F2F RID: 3887
		[Serializable]
		public struct TailComponent
		{
			// Token: 0x04004F61 RID: 20321
			public Rect m_tailUVRect;

			// Token: 0x04004F62 RID: 20322
			public int m_tailJointNum;
		}

		// Token: 0x02000F30 RID: 3888
		[Serializable]
		public class BlinkComponent
		{
			// Token: 0x04004F63 RID: 20323
			public RangePareFloat m_blinkSpanSecRange = new RangePareFloat();
		}

		// Token: 0x02000F31 RID: 3889
		[Serializable]
		public class PathMoveComponent
		{
			// Token: 0x04004F64 RID: 20324
			public RangePareFloat m_startFocusRadiusRange = new RangePareFloat();

			// Token: 0x04004F65 RID: 20325
			public RangePareFloat m_endFocusRadiusRange = new RangePareFloat();

			// Token: 0x04004F66 RID: 20326
			public RangePareFloat[] m_FocusRadiusRange = new RangePareFloat[4];
		}

		// Token: 0x02000F32 RID: 3890
		public enum ePropertyID
		{
			// Token: 0x04004F68 RID: 20328
			Particle_TypeParamBillboardWidthRange,
			// Token: 0x04004F69 RID: 20329
			Particle_TypeParamBillboardHeightRange,
			// Token: 0x04004F6A RID: 20330
			Particle_TypeParamPointSizeRange,
			// Token: 0x04004F6B RID: 20331
			Particle_TypeParamLineWidth,
			// Token: 0x04004F6C RID: 20332
			Particle_TypeParamPolyLineTopWidthRange,
			// Token: 0x04004F6D RID: 20333
			Particle_TypeParamPolyLineEndWidthRange,
			// Token: 0x04004F6E RID: 20334
			Particle_TypeParamConfettiWidthRange,
			// Token: 0x04004F6F RID: 20335
			Particle_TypeParamConfettiHeightRange,
			// Token: 0x04004F70 RID: 20336
			Particle_EmitParamPointAngleRange,
			// Token: 0x04004F71 RID: 20337
			Particle_EmitParamBoxWidthRange,
			// Token: 0x04004F72 RID: 20338
			Particle_EmitParamBoxHeightRange,
			// Token: 0x04004F73 RID: 20339
			Particle_EmitParamBoxDepthRange,
			// Token: 0x04004F74 RID: 20340
			Particle_EmitParamPlaneQuadWidthRange,
			// Token: 0x04004F75 RID: 20341
			Particle_EmitParamPlaneQuadHeightRange,
			// Token: 0x04004F76 RID: 20342
			Particle_EmitParamPlaneCircleRadiusRange,
			// Token: 0x04004F77 RID: 20343
			Particle_EmitParamSphereAngleRange,
			// Token: 0x04004F78 RID: 20344
			Particle_EmitParamSphereRadiusRange,
			// Token: 0x04004F79 RID: 20345
			Particle_EmitParamTorusAngleRange,
			// Token: 0x04004F7A RID: 20346
			Particle_EmitParamTorusBigRadius,
			// Token: 0x04004F7B RID: 20347
			Particle_EmitParamTorusSmallRadiusRange,
			// Token: 0x04004F7C RID: 20348
			Particle_EmitParamCylinderRadiusRange,
			// Token: 0x04004F7D RID: 20349
			Particle_EmitParamCylinderHeightRange,
			// Token: 0x04004F7E RID: 20350
			Particle_GravityDir,
			// Token: 0x04004F7F RID: 20351
			Particle_GravityForceRange,
			// Token: 0x04004F80 RID: 20352
			Particle_LifeSpanType,
			// Token: 0x04004F81 RID: 20353
			Particle_LifeSpanAlpha,
			// Token: 0x04004F82 RID: 20354
			Particle_LifeSpanTimeRange,
			// Token: 0x04004F83 RID: 20355
			Particle_LifeSpanDistanceMaxRange,
			// Token: 0x04004F84 RID: 20356
			Particle_LifeSpanHeightRange,
			// Token: 0x04004F85 RID: 20357
			Particle_IncidentNumberPerSec,
			// Token: 0x04004F86 RID: 20358
			Particle_IncidentRandomLevel,
			// Token: 0x04004F87 RID: 20359
			Particle_HDRFactor,
			// Token: 0x04004F88 RID: 20360
			Particle_IsLocalTrans,
			// Token: 0x04004F89 RID: 20361
			Particle_IsRandomEmitDir,
			// Token: 0x04004F8A RID: 20362
			Particle_AccelerationAccelerationRange,
			// Token: 0x04004F8B RID: 20363
			Particle_AccelerationDragForceRange,
			// Token: 0x04004F8C RID: 20364
			Particle_RotationRotSpeedRange,
			// Token: 0x04004F8D RID: 20365
			Particle_RotationRotAccelerationRange,
			// Token: 0x04004F8E RID: 20366
			Particle_RotationRotDragForceRange,
			// Token: 0x04004F8F RID: 20367
			Particle_RotationRotAnchorOffsetRange,
			// Token: 0x04004F90 RID: 20368
			Particle_RotationRotRange,
			// Token: 0x04004F91 RID: 20369
			Particle_ColorCurveColorCurve,
			// Token: 0x04004F92 RID: 20370
			Particle_BlinkBlinkSpanRange,
			// Token: 0x04004F93 RID: 20371
			Particle_PathMoveFocusRadiusRange,
			// Token: 0x04004F94 RID: 20372
			Max
		}

		// Token: 0x02000F33 RID: 3891
		public struct Test
		{
			// Token: 0x060050CA RID: 20682 RVA: 0x001695C7 File Offset: 0x001679C7
			public Test(string str, eEffectAnimTypeCode typeCode)
			{
				this.m_PropertyName = str;
				this.m_TypeCode = typeCode;
			}

			// Token: 0x04004F95 RID: 20373
			public string m_PropertyName;

			// Token: 0x04004F96 RID: 20374
			public eEffectAnimTypeCode m_TypeCode;
		}
	}
}
