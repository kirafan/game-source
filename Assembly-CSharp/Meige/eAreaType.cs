﻿using System;

namespace Meige
{
	// Token: 0x02000F68 RID: 3944
	public enum eAreaType
	{
		// Token: 0x04005211 RID: 21009
		eAreaType_Circle,
		// Token: 0x04005212 RID: 21010
		eAreaType_Square
	}
}
