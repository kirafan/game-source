﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F4B RID: 3915
	public class PrimLineBuffer : MonoBehaviour
	{
		// Token: 0x06005124 RID: 20772 RVA: 0x0016B9F1 File Offset: 0x00169DF1
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x06005125 RID: 20773 RVA: 0x0016BA0B File Offset: 0x00169E0B
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x06005126 RID: 20774 RVA: 0x0016BA30 File Offset: 0x00169E30
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x06005127 RID: 20775 RVA: 0x0016BA38 File Offset: 0x00169E38
		internal void AddActiveBufferList(PrimLineBuffer.LineBuffer meshBuffer)
		{
			this.m_activeLineBufferList.Add(meshBuffer);
		}

		// Token: 0x06005128 RID: 20776 RVA: 0x0016BA47 File Offset: 0x00169E47
		internal void RemoveActiveBufferList(PrimLineBuffer.LineBuffer meshBuffer)
		{
			this.m_activeLineBufferList.Remove(meshBuffer);
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x0600512A RID: 20778 RVA: 0x0016BA5E File Offset: 0x00169E5E
		// (set) Token: 0x06005129 RID: 20777 RVA: 0x0016BA55 File Offset: 0x00169E55
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x0600512C RID: 20780 RVA: 0x0016BA6F File Offset: 0x00169E6F
		// (set) Token: 0x0600512B RID: 20779 RVA: 0x0016BA66 File Offset: 0x00169E66
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x0600512E RID: 20782 RVA: 0x0016BA80 File Offset: 0x00169E80
		// (set) Token: 0x0600512D RID: 20781 RVA: 0x0016BA77 File Offset: 0x00169E77
		public bool bufferChangeFlg { get; set; }

		// Token: 0x0600512F RID: 20783 RVA: 0x0016BA88 File Offset: 0x00169E88
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x06005130 RID: 20784 RVA: 0x0016BA90 File Offset: 0x00169E90
		private void Setup()
		{
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Lines;
				this.m_MeshBufferIsSelf = true;
			}
		}

		// Token: 0x06005131 RID: 20785 RVA: 0x0016BAE0 File Offset: 0x00169EE0
		public virtual PrimLineBuffer.LineBuffer AddBuffer(int lineNum, int pointNum)
		{
			this.Setup();
			PrimLineBuffer.LineBuffer lineBuffer = new PrimLineBuffer.LineBuffer();
			lineBuffer.lines = new PrimLineBuffer.LineBuffer.Line[lineNum];
			lineBuffer.positions = new Vector3[lineNum * pointNum];
			lineBuffer.widths = new float[lineNum * pointNum];
			lineBuffer.UVs = new Vector2[lineNum * pointNum];
			lineBuffer.colors = new Color[lineNum * pointNum];
			lineBuffer.lineNum = lineNum;
			lineBuffer.pointNum = pointNum;
			lineBuffer.parent = this;
			lineBuffer.enabled = false;
			this.m_LineBufferList.Add(lineBuffer);
			this.bufferChangeFlg = true;
			lineBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(lineNum * pointNum, lineNum * ((pointNum - 1) * 2));
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < lineNum; i++)
			{
				lineBuffer.lines[i] = new PrimLineBuffer.LineBuffer.Line();
				lineBuffer.lines[i].parent = lineBuffer;
				lineBuffer.lines[i].topPointIdx = num2;
				lineBuffer.meshBuffer.indices[num++] = num2;
				num2++;
				for (int j = 0; j < pointNum - 2; j++)
				{
					lineBuffer.meshBuffer.indices[num++] = num2;
					lineBuffer.meshBuffer.indices[num++] = num2;
					num2++;
				}
				lineBuffer.meshBuffer.indices[num++] = num2;
				num2++;
			}
			lineBuffer.UpdatePositions();
			lineBuffer.UpdateWidths();
			lineBuffer.UpdateUVs();
			lineBuffer.UpdateColors();
			lineBuffer.meshBuffer.UpdateIndices();
			return lineBuffer;
		}

		// Token: 0x06005132 RID: 20786 RVA: 0x0016BC58 File Offset: 0x0016A058
		public virtual void RemoveBuffer(PrimLineBuffer.LineBuffer lineBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			lineBuffer.enabled = false;
			int num = this.m_LineBufferList.IndexOf(lineBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(lineBuffer.meshBuffer);
				lineBuffer.meshBuffer = null;
				this.m_LineBufferList.RemoveAt(num);
			}
		}

		// Token: 0x06005133 RID: 20787 RVA: 0x0016BCB8 File Offset: 0x0016A0B8
		private void UpdateMesh()
		{
			if (this.m_LineBufferList.Count == 0)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < this.m_LineBufferList.Count; i++)
			{
				PrimLineBuffer.LineBuffer lineBuffer = this.m_LineBufferList[i] as PrimLineBuffer.LineBuffer;
				if (lineBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = lineBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (lineBuffer.enabled)
						{
							for (int j = 0; j < lineBuffer.lineNum; j++)
							{
								PrimLineBuffer.LineBuffer.Line line = lineBuffer.GetLine(j);
								if (line.isDirtyPositions)
								{
									for (int k = 0; k < lineBuffer.pointNum; k++)
									{
										meshBuffer.vertices[j * lineBuffer.pointNum + k] = line.GetPosition(k);
									}
									line.isDirtyPositions = false;
									meshBuffer.UpdateVertices();
								}
								if (line.isDirtyUVs)
								{
									for (int l = 0; l < lineBuffer.pointNum; l++)
									{
										meshBuffer.UVs[j * lineBuffer.pointNum + l] = line.GetUV(l);
									}
									line.isDirtyUVs = false;
									meshBuffer.UpdateUVs();
								}
								if (line.isDirtyColors)
								{
									for (int m = 0; m < lineBuffer.pointNum; m++)
									{
										meshBuffer.colors[j * lineBuffer.pointNum + m] = line.GetColor(m);
									}
									line.isDirtyColors = false;
									meshBuffer.UpdateColors();
								}
							}
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x06005134 RID: 20788 RVA: 0x0016BE85 File Offset: 0x0016A285
		private void Update()
		{
			this.UpdateMesh();
		}

		// Token: 0x06005135 RID: 20789 RVA: 0x0016BE8D File Offset: 0x0016A28D
		private void OnWillRenderObject()
		{
		}

		// Token: 0x0400504C RID: 20556
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x0400504D RID: 20557
		protected bool m_MeshBufferIsSelf;

		// Token: 0x0400504E RID: 20558
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x0400504F RID: 20559
		protected ArrayList m_LineBufferList = new ArrayList();

		// Token: 0x04005050 RID: 20560
		protected ArrayList m_activeLineBufferList = new ArrayList(32);

		// Token: 0x02000F4C RID: 3916
		public class LineBuffer
		{
			// Token: 0x06005137 RID: 20791 RVA: 0x0016BE97 File Offset: 0x0016A297
			public PrimLineBuffer.LineBuffer.Line GetLine(int lineIdx)
			{
				return this.lines[lineIdx];
			}

			// Token: 0x06005138 RID: 20792 RVA: 0x0016BEA1 File Offset: 0x0016A2A1
			public void Remove()
			{
			}

			// Token: 0x06005139 RID: 20793 RVA: 0x0016BEA4 File Offset: 0x0016A2A4
			public void UpdatePositions()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyPositions = true;
				}
			}

			// Token: 0x0600513A RID: 20794 RVA: 0x0016BED8 File Offset: 0x0016A2D8
			public void UpdateWidths()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyWidths = true;
				}
			}

			// Token: 0x0600513B RID: 20795 RVA: 0x0016BF0C File Offset: 0x0016A30C
			public void UpdateUVs()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyUVs = true;
				}
			}

			// Token: 0x0600513C RID: 20796 RVA: 0x0016BF40 File Offset: 0x0016A340
			public void UpdateColors()
			{
				for (int i = 0; i < this.lineNum; i++)
				{
					this.lines[i].isDirtyColors = true;
				}
			}

			// Token: 0x0600513D RID: 20797 RVA: 0x0016BF72 File Offset: 0x0016A372
			public void Minimize()
			{
			}

			// Token: 0x1700056E RID: 1390
			// (get) Token: 0x0600513E RID: 20798 RVA: 0x0016BF74 File Offset: 0x0016A374
			// (set) Token: 0x0600513F RID: 20799 RVA: 0x0016BF7C File Offset: 0x0016A37C
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateWidths();
					this.UpdateColors();
				}
			}

			// Token: 0x04005053 RID: 20563
			internal PrimLineBuffer parent;

			// Token: 0x04005054 RID: 20564
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04005055 RID: 20565
			internal PrimLineBuffer.LineBuffer.Line[] lines;

			// Token: 0x04005056 RID: 20566
			public Vector3[] positions;

			// Token: 0x04005057 RID: 20567
			public float[] widths;

			// Token: 0x04005058 RID: 20568
			public float[] rots;

			// Token: 0x04005059 RID: 20569
			public Vector2[] UVs;

			// Token: 0x0400505A RID: 20570
			public Color[] colors;

			// Token: 0x0400505B RID: 20571
			public int lineNum;

			// Token: 0x0400505C RID: 20572
			public int pointNum;

			// Token: 0x0400505D RID: 20573
			private bool m_enabled;

			// Token: 0x02000F4D RID: 3917
			public class Line
			{
				// Token: 0x06005141 RID: 20801 RVA: 0x0016BFF7 File Offset: 0x0016A3F7
				public void SetPosition(Vector3 pos, int pointIdx)
				{
					this.parent.positions[this.topPointIdx + pointIdx] = pos;
					this.isDirtyPositions = true;
				}

				// Token: 0x06005142 RID: 20802 RVA: 0x0016C01E File Offset: 0x0016A41E
				public Vector3 GetPosition(int pointIdx)
				{
					return this.parent.positions[this.topPointIdx + pointIdx];
				}

				// Token: 0x06005143 RID: 20803 RVA: 0x0016C03D File Offset: 0x0016A43D
				public void SetWidth(float width, int pointIdx)
				{
					this.parent.widths[this.topPointIdx + pointIdx] = width;
					this.isDirtyWidths = true;
				}

				// Token: 0x06005144 RID: 20804 RVA: 0x0016C05B File Offset: 0x0016A45B
				public float GetWidth(int pointIdx)
				{
					return this.parent.widths[this.topPointIdx + pointIdx];
				}

				// Token: 0x06005145 RID: 20805 RVA: 0x0016C071 File Offset: 0x0016A471
				public void SetUV(Vector2 uv, int pointIdx)
				{
					this.parent.UVs[this.topPointIdx + pointIdx] = uv;
					this.isDirtyUVs = true;
				}

				// Token: 0x06005146 RID: 20806 RVA: 0x0016C098 File Offset: 0x0016A498
				public Vector2 GetUV(int pointIdx)
				{
					return this.parent.UVs[this.topPointIdx + pointIdx];
				}

				// Token: 0x06005147 RID: 20807 RVA: 0x0016C0B7 File Offset: 0x0016A4B7
				public void SetColor(Color color, int pointIdx)
				{
					this.parent.colors[this.topPointIdx + pointIdx] = color;
					this.isDirtyColors = true;
				}

				// Token: 0x06005148 RID: 20808 RVA: 0x0016C0DE File Offset: 0x0016A4DE
				public Color GetColor(int pointIdx)
				{
					return this.parent.colors[this.topPointIdx + pointIdx];
				}

				// Token: 0x0400505E RID: 20574
				internal PrimLineBuffer.LineBuffer parent;

				// Token: 0x0400505F RID: 20575
				public int topPointIdx;

				// Token: 0x04005060 RID: 20576
				internal bool isDirtyPositions;

				// Token: 0x04005061 RID: 20577
				internal bool isDirtyWidths;

				// Token: 0x04005062 RID: 20578
				internal bool isDirtyUVs;

				// Token: 0x04005063 RID: 20579
				internal bool isDirtyColors;
			}
		}
	}
}
