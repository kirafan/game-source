﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000EEB RID: 3819
	[RequireComponent(typeof(Animation))]
	[DisallowMultipleComponent]
	public class EffectSequencer : EffectComponentBase
	{
		// Token: 0x06004EB1 RID: 20145 RVA: 0x0015C5FB File Offset: 0x0015A9FB
		private void Awake()
		{
			this.m_Animation = base.gameObject.GetComponent<Animation>();
			this.m_Animation.Stop();
			this.m_Animation.playAutomatically = false;
			this.m_Animation.wrapMode = WrapMode.Once;
		}

		// Token: 0x06004EB2 RID: 20146 RVA: 0x0015C634 File Offset: 0x0015AA34
		private void Start()
		{
			Transform[] componentsInChildren = base.GetComponentsInChildren<Transform>();
			int num = 0;
			foreach (Transform transform in componentsInChildren)
			{
				if (transform.parent == base.gameObject.transform)
				{
					num++;
				}
			}
			this.m_EffectArray = new EffectComponentBase[num];
			num = 0;
			foreach (Transform transform2 in componentsInChildren)
			{
				EffectComponentBase component = transform2.GetComponent<EffectComponentBase>();
				if (component != null && component.transform.parent == base.gameObject.transform)
				{
					this.m_EffectArray[num++] = component;
				}
			}
			this.m_ClipName = this.m_Animation.clip.name;
		}

		// Token: 0x06004EB3 RID: 20147 RVA: 0x0015C714 File Offset: 0x0015AB14
		private void ComponentAction_Shot(int index)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			if (!this.isAlive)
			{
				return;
			}
			if (index < this.m_EffectArray.Length && this.m_EffectArray[index] != null)
			{
				this.m_EffectArray[index].Shot();
			}
		}

		// Token: 0x06004EB4 RID: 20148 RVA: 0x0015C768 File Offset: 0x0015AB68
		private void ComponentAction_Activate(int index)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			if (!this.isAlive)
			{
				return;
			}
			if (index < this.m_EffectArray.Length && this.m_EffectArray[index] != null)
			{
				this.m_EffectArray[index].Activate(true);
			}
		}

		// Token: 0x06004EB5 RID: 20149 RVA: 0x0015C7BC File Offset: 0x0015ABBC
		private void ComponentAction_Deactivate(int index)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			if (!this.isAlive)
			{
				return;
			}
			if (index < this.m_EffectArray.Length && this.m_EffectArray[index] != null)
			{
				this.m_EffectArray[index].Activate(false);
			}
		}

		// Token: 0x06004EB6 RID: 20150 RVA: 0x0015C810 File Offset: 0x0015AC10
		private void ComponentAction_Kill(int index)
		{
			if (this.m_EffectArray == null)
			{
				return;
			}
			if (!this.isAlive)
			{
				return;
			}
			if (index < this.m_EffectArray.Length && this.m_EffectArray[index] != null)
			{
				this.m_EffectArray[index].Kill();
			}
		}

		// Token: 0x06004EB7 RID: 20151 RVA: 0x0015C864 File Offset: 0x0015AC64
		private void Update()
		{
			if (this.m_Animation == null)
			{
				return;
			}
			if (!this.isAlive && !this.isActive)
			{
				return;
			}
			if (this.isActive && !this.m_Animation.isPlaying)
			{
				this.Shot();
			}
			if (!this.m_Animation.isPlaying)
			{
				int num = 0;
				for (int i = 0; i < this.m_EffectArray.Length; i++)
				{
					if (this.m_EffectArray[i] != null)
					{
						num++;
						if (!this.m_EffectArray[i].isAlive && !this.m_EffectArray[i].isActive)
						{
							num--;
						}
					}
				}
				if (num == 0)
				{
					this.Kill();
				}
			}
		}

		// Token: 0x06004EB8 RID: 20152 RVA: 0x0015C933 File Offset: 0x0015AD33
		public void SetClip(string name)
		{
			this.m_ClipName = name;
		}

		// Token: 0x06004EB9 RID: 20153 RVA: 0x0015C93C File Offset: 0x0015AD3C
		public override bool Shot()
		{
			if (this.isAlive)
			{
				return false;
			}
			IEnumerator enumerator = this.m_Animation.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					animationState.time = 0f;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			bool flag;
			if (this.m_ClipName == null)
			{
				flag = this.m_Animation.Play();
			}
			else
			{
				flag = this.m_Animation.Play(this.m_ClipName);
			}
			if (flag)
			{
				this.m_isAlive = true;
			}
			return flag;
		}

		// Token: 0x06004EBA RID: 20154 RVA: 0x0015C9F0 File Offset: 0x0015ADF0
		public override void Activate(bool flg)
		{
			this.isActive = flg;
		}

		// Token: 0x06004EBB RID: 20155 RVA: 0x0015C9FC File Offset: 0x0015ADFC
		public override bool Kill()
		{
			if (!this.isAlive)
			{
				return false;
			}
			for (int i = 0; i < this.m_EffectArray.Length; i++)
			{
				if (this.m_EffectArray[i] != null)
				{
					this.m_EffectArray[i].Kill();
				}
			}
			IEnumerator enumerator = this.m_Animation.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					animationState.time = 0f;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.m_Animation.Stop();
			this.m_isAlive = false;
			return true;
		}

		// Token: 0x04004E77 RID: 20087
		private EffectComponentBase[] m_EffectArray;

		// Token: 0x04004E78 RID: 20088
		private Animation m_Animation;

		// Token: 0x04004E79 RID: 20089
		private string m_ClipName;
	}
}
