﻿using System;

namespace Meige
{
	// Token: 0x02000F9A RID: 3994
	public enum eMFFChunkType
	{
		// Token: 0x040053EA RID: 21482
		eMFFChunk_Object,
		// Token: 0x040053EB RID: 21483
		eMFFChunk_Texture,
		// Token: 0x040053EC RID: 21484
		eMFFChunk_Animation,
		// Token: 0x040053ED RID: 21485
		eMFFChunk_Material,
		// Token: 0x040053EE RID: 21486
		eMFFChunk_Hierarchies,
		// Token: 0x040053EF RID: 21487
		eMFFChunk_ParticleEmitters,
		// Token: 0x040053F0 RID: 21488
		eMFFChunk_Collisions,
		// Token: 0x040053F1 RID: 21489
		eMFFChunk_SoundConfig,
		// Token: 0x040053F2 RID: 21490
		eMFFChunk_SoundBank,
		// Token: 0x040053F3 RID: 21491
		eMFFChunk_CatFileElement,
		// Token: 0x040053F4 RID: 21492
		eMFFChunk_ShaderKey,
		// Token: 0x040053F5 RID: 21493
		eMFFChunk_ShaderBinary,
		// Token: 0x040053F6 RID: 21494
		eMFFChunk_SpectrumCtrls,
		// Token: 0x040053F7 RID: 21495
		eMFFChunk_ADNode,
		// Token: 0x040053F8 RID: 21496
		eMFFChunk_Cameras,
		// Token: 0x040053F9 RID: 21497
		eMFFChunk_SignalCtrls,
		// Token: 0x040053FA RID: 21498
		eMFFChunk_FontParam,
		// Token: 0x040053FB RID: 21499
		eMFFChunk_RenderEnvironment,
		// Token: 0x040053FC RID: 21500
		eMFFChunk_LevelData,
		// Token: 0x040053FD RID: 21501
		eMFFChunk_ActData
	}
}
