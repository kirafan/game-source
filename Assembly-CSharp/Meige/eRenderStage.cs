﻿using System;

namespace Meige
{
	// Token: 0x02000F7E RID: 3966
	public enum eRenderStage
	{
		// Token: 0x040052BB RID: 21179
		eRenderStage_Start,
		// Token: 0x040052BC RID: 21180
		eRenderStage_Pass_Start = 0,
		// Token: 0x040052BD RID: 21181
		eRenderStage_BG_Higher = 0,
		// Token: 0x040052BE RID: 21182
		eRenderStage_BG_High,
		// Token: 0x040052BF RID: 21183
		eRenderStage_BG,
		// Token: 0x040052C0 RID: 21184
		eRenderStage_BG_Low,
		// Token: 0x040052C1 RID: 21185
		eRenderStage_BG_Lower,
		// Token: 0x040052C2 RID: 21186
		eRenderStage_Opaque_Higher,
		// Token: 0x040052C3 RID: 21187
		eRenderStage_Opaque_High,
		// Token: 0x040052C4 RID: 21188
		eRenderStage_Opaque,
		// Token: 0x040052C5 RID: 21189
		eRenderStage_Opaque_Low,
		// Token: 0x040052C6 RID: 21190
		eRenderStage_Opaque_Lower,
		// Token: 0x040052C7 RID: 21191
		eRenderStage_PunchThrough_Higher,
		// Token: 0x040052C8 RID: 21192
		eRenderStage_PunchThrough_High,
		// Token: 0x040052C9 RID: 21193
		eRenderStage_PunchThrough,
		// Token: 0x040052CA RID: 21194
		eRenderStage_PunchThrough_Low,
		// Token: 0x040052CB RID: 21195
		eRenderStage_PunchThrough_Lower,
		// Token: 0x040052CC RID: 21196
		eRenderStage_AfterPunchThrough_Higher,
		// Token: 0x040052CD RID: 21197
		eRenderStage_AfterPunchThrough_High,
		// Token: 0x040052CE RID: 21198
		eRenderStage_AfterPunchThrough,
		// Token: 0x040052CF RID: 21199
		eRenderStage_AfterPunchThrough_Low,
		// Token: 0x040052D0 RID: 21200
		eRenderStage_AfterPunchThrough_Lower,
		// Token: 0x040052D1 RID: 21201
		eRenderStage_Alpha_Higher,
		// Token: 0x040052D2 RID: 21202
		eRenderStage_Alpha_High,
		// Token: 0x040052D3 RID: 21203
		eRenderStage_Alpha,
		// Token: 0x040052D4 RID: 21204
		eRenderStage_Alpha_Low,
		// Token: 0x040052D5 RID: 21205
		eRenderStage_Alpha_Lower,
		// Token: 0x040052D6 RID: 21206
		eRenderStage_AfterAlpha_Higher,
		// Token: 0x040052D7 RID: 21207
		eRenderStage_AfterAlpha_High,
		// Token: 0x040052D8 RID: 21208
		eRenderStage_AfterAlpha,
		// Token: 0x040052D9 RID: 21209
		eRenderStage_AfterAlpha_Low,
		// Token: 0x040052DA RID: 21210
		eRenderStage_AfterAlpha_Lower,
		// Token: 0x040052DB RID: 21211
		eRenderStage_UI_Higher,
		// Token: 0x040052DC RID: 21212
		eRenderStage_UI_High,
		// Token: 0x040052DD RID: 21213
		eRenderStage_UI,
		// Token: 0x040052DE RID: 21214
		eRenderStage_UI_Low,
		// Token: 0x040052DF RID: 21215
		eRenderStage_UI_Lower,
		// Token: 0x040052E0 RID: 21216
		eRenderStage_Path_End = 34,
		// Token: 0x040052E1 RID: 21217
		eRenderStage_FinalPass_Start,
		// Token: 0x040052E2 RID: 21218
		eRenderStage_FinalPass_Higher = 35,
		// Token: 0x040052E3 RID: 21219
		eRenderStage_FinalPass_High,
		// Token: 0x040052E4 RID: 21220
		eRenderStage_FinalPass,
		// Token: 0x040052E5 RID: 21221
		eRenderStage_FinalPass_Low,
		// Token: 0x040052E6 RID: 21222
		eRenderStage_FinalPass_Lower,
		// Token: 0x040052E7 RID: 21223
		eRenderStage_FinalPath_End = 39,
		// Token: 0x040052E8 RID: 21224
		eRenderStage_SystemFont,
		// Token: 0x040052E9 RID: 21225
		eRenderStage_Cnt
	}
}
