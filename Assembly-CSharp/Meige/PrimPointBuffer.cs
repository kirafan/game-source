﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F4E RID: 3918
	public class PrimPointBuffer : MonoBehaviour
	{
		// Token: 0x0600514A RID: 20810 RVA: 0x0016C11D File Offset: 0x0016A51D
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x0600514B RID: 20811 RVA: 0x0016C144 File Offset: 0x0016A544
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				IEnumerator enumerator = this.m_PointBufferList.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						PrimPointBuffer.PointBuffer pointBuffer = (PrimPointBuffer.PointBuffer)obj;
						this.m_EffectMeshBuffer.RemoveBuffer(pointBuffer.meshBuffer);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x0600514C RID: 20812 RVA: 0x0016C1D8 File Offset: 0x0016A5D8
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x0600514D RID: 20813 RVA: 0x0016C1E0 File Offset: 0x0016A5E0
		internal void AddActiveBufferList(PrimPointBuffer.PointBuffer meshBuffer)
		{
			this.m_activePointBufferList.Add(meshBuffer);
		}

		// Token: 0x0600514E RID: 20814 RVA: 0x0016C1EF File Offset: 0x0016A5EF
		internal void RemoveActiveBufferList(PrimPointBuffer.PointBuffer meshBuffer)
		{
			this.m_activePointBufferList.Remove(meshBuffer);
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x06005150 RID: 20816 RVA: 0x0016C206 File Offset: 0x0016A606
		// (set) Token: 0x0600514F RID: 20815 RVA: 0x0016C1FD File Offset: 0x0016A5FD
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x06005152 RID: 20818 RVA: 0x0016C217 File Offset: 0x0016A617
		// (set) Token: 0x06005151 RID: 20817 RVA: 0x0016C20E File Offset: 0x0016A60E
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x06005154 RID: 20820 RVA: 0x0016C228 File Offset: 0x0016A628
		// (set) Token: 0x06005153 RID: 20819 RVA: 0x0016C21F File Offset: 0x0016A61F
		public bool bufferChangeFlg { get; set; }

		// Token: 0x06005155 RID: 20821 RVA: 0x0016C230 File Offset: 0x0016A630
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x06005156 RID: 20822 RVA: 0x0016C238 File Offset: 0x0016A638
		private void Setup()
		{
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Points;
				this.m_MeshBufferIsSelf = true;
			}
		}

		// Token: 0x06005157 RID: 20823 RVA: 0x0016C288 File Offset: 0x0016A688
		public virtual PrimPointBuffer.PointBuffer AddBuffer(int pointNum)
		{
			this.Setup();
			PrimPointBuffer.PointBuffer pointBuffer = new PrimPointBuffer.PointBuffer();
			pointBuffer.positions = new Vector3[pointNum];
			pointBuffer.sizes = new float[pointNum];
			pointBuffer.UVs = new Rect[pointNum];
			pointBuffer.colors = new Color[pointNum];
			pointBuffer.pointNum = pointNum;
			pointBuffer.parent = this;
			pointBuffer.enabled = false;
			this.m_PointBufferList.Add(pointBuffer);
			pointBuffer.UpdatePositions();
			pointBuffer.UpdateSizes();
			pointBuffer.UpdateUVs();
			pointBuffer.UpdateColors();
			this.bufferChangeFlg = true;
			pointBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(pointNum, pointNum);
			for (int i = 0; i < pointNum; i++)
			{
				pointBuffer.meshBuffer.indices[i] = i;
			}
			pointBuffer.meshBuffer.UpdateIndices();
			return pointBuffer;
		}

		// Token: 0x06005158 RID: 20824 RVA: 0x0016C354 File Offset: 0x0016A754
		public virtual void RemoveBuffer(PrimPointBuffer.PointBuffer billboardBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			billboardBuffer.enabled = false;
			int num = this.m_PointBufferList.IndexOf(billboardBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(billboardBuffer.meshBuffer);
				billboardBuffer.meshBuffer = null;
				this.m_PointBufferList.RemoveAt(num);
			}
		}

		// Token: 0x06005159 RID: 20825 RVA: 0x0016C3B4 File Offset: 0x0016A7B4
		private void UpdateMesh()
		{
			if (this.m_PointBufferList.Count == 0)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < this.m_PointBufferList.Count; i++)
			{
				PrimPointBuffer.PointBuffer pointBuffer = this.m_PointBufferList[i] as PrimPointBuffer.PointBuffer;
				if (pointBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = pointBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (pointBuffer.enabled)
						{
							for (int j = 0; j < pointBuffer.pointNum; j++)
							{
								if (pointBuffer.isDirtyPositions || pointBuffer.isDirtySizes)
								{
									meshBuffer.vertices[j] = pointBuffer.positions[j];
								}
								if (pointBuffer.isDirtyUVs)
								{
									meshBuffer.UVs[j] = pointBuffer.UVs[j].position;
								}
								if (pointBuffer.isDirtyColors)
								{
									meshBuffer.colors[j] = pointBuffer.colors[j];
								}
							}
							if (pointBuffer.isDirtyPositions || pointBuffer.isDirtySizes)
							{
								meshBuffer.UpdateVertices();
								pointBuffer.isDirtyPositions = false;
								pointBuffer.isDirtySizes = false;
							}
							if (pointBuffer.isDirtyUVs)
							{
								meshBuffer.UpdateUVs();
								pointBuffer.isDirtyUVs = false;
							}
							if (pointBuffer.isDirtyColors)
							{
								meshBuffer.UpdateColors();
								pointBuffer.isDirtyColors = false;
							}
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x0600515A RID: 20826 RVA: 0x0016C55B File Offset: 0x0016A95B
		private void Update()
		{
			this.UpdateMesh();
		}

		// Token: 0x0600515B RID: 20827 RVA: 0x0016C563 File Offset: 0x0016A963
		private void OnWillRenderObject()
		{
		}

		// Token: 0x04005064 RID: 20580
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04005065 RID: 20581
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04005066 RID: 20582
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x04005067 RID: 20583
		protected ArrayList m_PointBufferList = new ArrayList();

		// Token: 0x04005068 RID: 20584
		protected ArrayList m_activePointBufferList = new ArrayList(32);

		// Token: 0x02000F4F RID: 3919
		public class PointBuffer
		{
			// Token: 0x0600515D RID: 20829 RVA: 0x0016C56D File Offset: 0x0016A96D
			public void Remove()
			{
			}

			// Token: 0x0600515E RID: 20830 RVA: 0x0016C56F File Offset: 0x0016A96F
			public void UpdatePositions()
			{
				this.isDirtyPositions = true;
			}

			// Token: 0x0600515F RID: 20831 RVA: 0x0016C578 File Offset: 0x0016A978
			public void UpdateSizes()
			{
				this.isDirtySizes = true;
			}

			// Token: 0x06005160 RID: 20832 RVA: 0x0016C581 File Offset: 0x0016A981
			public void UpdateUVs()
			{
				this.isDirtyUVs = true;
			}

			// Token: 0x06005161 RID: 20833 RVA: 0x0016C58A File Offset: 0x0016A98A
			public void UpdateColors()
			{
				this.isDirtyColors = true;
			}

			// Token: 0x06005162 RID: 20834 RVA: 0x0016C593 File Offset: 0x0016A993
			public void Minimize()
			{
			}

			// Token: 0x17000572 RID: 1394
			// (get) Token: 0x06005163 RID: 20835 RVA: 0x0016C595 File Offset: 0x0016A995
			// (set) Token: 0x06005164 RID: 20836 RVA: 0x0016C5A0 File Offset: 0x0016A9A0
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateSizes();
					this.UpdateColors();
				}
			}

			// Token: 0x0400506B RID: 20587
			internal PrimPointBuffer parent;

			// Token: 0x0400506C RID: 20588
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x0400506D RID: 20589
			public Vector3[] positions;

			// Token: 0x0400506E RID: 20590
			public float[] sizes;

			// Token: 0x0400506F RID: 20591
			public Rect[] UVs;

			// Token: 0x04005070 RID: 20592
			public Color[] colors;

			// Token: 0x04005071 RID: 20593
			public int pointNum;

			// Token: 0x04005072 RID: 20594
			internal bool isDirtyPositions;

			// Token: 0x04005073 RID: 20595
			internal bool isDirtySizes;

			// Token: 0x04005074 RID: 20596
			internal bool isDirtyUVs;

			// Token: 0x04005075 RID: 20597
			internal bool isDirtyColors;

			// Token: 0x04005076 RID: 20598
			private bool m_enabled;
		}
	}
}
