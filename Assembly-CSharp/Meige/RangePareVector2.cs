﻿using System;

namespace Meige
{
	// Token: 0x02000F02 RID: 3842
	[Serializable]
	public class RangePareVector2 : RangePare<Float2>
	{
		// Token: 0x06004F32 RID: 20274 RVA: 0x0015DE09 File Offset: 0x0015C209
		public RangePareVector2()
		{
		}

		// Token: 0x06004F33 RID: 20275 RVA: 0x0015DE11 File Offset: 0x0015C211
		public RangePareVector2(Float2 min, Float2 max) : base(min, max)
		{
		}

		// Token: 0x06004F34 RID: 20276 RVA: 0x0015DE1B File Offset: 0x0015C21B
		public RangePareVector2(RangePareVector2 v) : base(v)
		{
		}

		// Token: 0x06004F35 RID: 20277 RVA: 0x0015DE24 File Offset: 0x0015C224
		public new RangePareVector2 Clone()
		{
			return new RangePareVector2(this);
		}
	}
}
