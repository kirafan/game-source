﻿using System;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F0B RID: 3851
	[Serializable]
	public struct Float3 : IComparable, IComparable<Float3>, IOperatorFast<Float3>
	{
		// Token: 0x06004FCB RID: 20427 RVA: 0x0015EC32 File Offset: 0x0015D032
		public Float3(Float3 value)
		{
			this.m_Value = value.m_Value;
		}

		// Token: 0x06004FCC RID: 20428 RVA: 0x0015EC41 File Offset: 0x0015D041
		public Float3(Vector3 value)
		{
			this.m_Value = value;
		}

		// Token: 0x06004FCD RID: 20429 RVA: 0x0015EC4A File Offset: 0x0015D04A
		public Float3(float x, float y, float z)
		{
			this.m_Value.x = x;
			this.m_Value.y = y;
			this.m_Value.z = z;
		}

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06004FCF RID: 20431 RVA: 0x0015EC79 File Offset: 0x0015D079
		// (set) Token: 0x06004FCE RID: 20430 RVA: 0x0015EC70 File Offset: 0x0015D070
		public Vector3 value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06004FD1 RID: 20433 RVA: 0x0015EC8F File Offset: 0x0015D08F
		// (set) Token: 0x06004FD0 RID: 20432 RVA: 0x0015EC81 File Offset: 0x0015D081
		public float x
		{
			get
			{
				return this.m_Value.x;
			}
			set
			{
				this.m_Value.x = value;
			}
		}

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06004FD3 RID: 20435 RVA: 0x0015ECAA File Offset: 0x0015D0AA
		// (set) Token: 0x06004FD2 RID: 20434 RVA: 0x0015EC9C File Offset: 0x0015D09C
		public float y
		{
			get
			{
				return this.m_Value.y;
			}
			set
			{
				this.m_Value.y = value;
			}
		}

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06004FD5 RID: 20437 RVA: 0x0015ECC5 File Offset: 0x0015D0C5
		// (set) Token: 0x06004FD4 RID: 20436 RVA: 0x0015ECB7 File Offset: 0x0015D0B7
		public float z
		{
			get
			{
				return this.m_Value.z;
			}
			set
			{
				this.m_Value.z = value;
			}
		}

		// Token: 0x1700054C RID: 1356
		public float this[int idx]
		{
			get
			{
				return this.m_Value[idx];
			}
			set
			{
				this.m_Value[idx] = value;
			}
		}

		// Token: 0x06004FD8 RID: 20440 RVA: 0x0015ECEF File Offset: 0x0015D0EF
		public static implicit operator Float3(Vector3 v)
		{
			return new Float3(v);
		}

		// Token: 0x06004FD9 RID: 20441 RVA: 0x0015ECF7 File Offset: 0x0015D0F7
		public static implicit operator Vector3(Float3 v)
		{
			return v.value;
		}

		// Token: 0x06004FDA RID: 20442 RVA: 0x0015ED00 File Offset: 0x0015D100
		public void Add(Float3 a, Float3 b)
		{
			this = a + b;
		}

		// Token: 0x06004FDB RID: 20443 RVA: 0x0015ED0F File Offset: 0x0015D10F
		public void Sub(Float3 a, Float3 b)
		{
			this = a - b;
		}

		// Token: 0x06004FDC RID: 20444 RVA: 0x0015ED1E File Offset: 0x0015D11E
		public void Mul(Float3 a, Float3 b)
		{
			this = a * b;
		}

		// Token: 0x06004FDD RID: 20445 RVA: 0x0015ED2D File Offset: 0x0015D12D
		public void Mul(Float3 a, float b)
		{
			this = a * b;
		}

		// Token: 0x06004FDE RID: 20446 RVA: 0x0015ED3C File Offset: 0x0015D13C
		public void Div(Float3 a, Float3 b)
		{
			this = a / b;
		}

		// Token: 0x06004FDF RID: 20447 RVA: 0x0015ED4B File Offset: 0x0015D14B
		public void Add(Float3 b)
		{
			this += b;
		}

		// Token: 0x06004FE0 RID: 20448 RVA: 0x0015ED5F File Offset: 0x0015D15F
		public void Sub(Float3 b)
		{
			this -= b;
		}

		// Token: 0x06004FE1 RID: 20449 RVA: 0x0015ED73 File Offset: 0x0015D173
		public void Mul(Float3 b)
		{
			this *= b;
		}

		// Token: 0x06004FE2 RID: 20450 RVA: 0x0015ED87 File Offset: 0x0015D187
		public void Mul(float b)
		{
			this *= b;
		}

		// Token: 0x06004FE3 RID: 20451 RVA: 0x0015ED9B File Offset: 0x0015D19B
		public void Div(Float3 b)
		{
			this *= b;
		}

		// Token: 0x06004FE4 RID: 20452 RVA: 0x0015EDAF File Offset: 0x0015D1AF
		public void SetRowValue(object o)
		{
			this.m_Value = (Vector3)o;
		}

		// Token: 0x06004FE5 RID: 20453 RVA: 0x0015EDBD File Offset: 0x0015D1BD
		public void SetRowValue(float o, int componentIdx)
		{
			this.m_Value[componentIdx] = o;
		}

		// Token: 0x06004FE6 RID: 20454 RVA: 0x0015EDCC File Offset: 0x0015D1CC
		public int CompareTo(object obj)
		{
			Float3? @float = obj as Float3?;
			if (@float == null)
			{
				return 1;
			}
			return this.CompareTo(@float);
		}

		// Token: 0x06004FE7 RID: 20455 RVA: 0x0015EE04 File Offset: 0x0015D204
		public int CompareTo(Float3 target)
		{
			if (target.m_Value.x == this.x && target.m_Value.y == this.y && target.m_Value.z == this.z)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06004FE8 RID: 20456 RVA: 0x0015EE5C File Offset: 0x0015D25C
		public override bool Equals(object obj)
		{
			Float3? @float = obj as Float3?;
			return @float != null && this.Equals(@float);
		}

		// Token: 0x06004FE9 RID: 20457 RVA: 0x0015EE98 File Offset: 0x0015D298
		public bool Equals(Float3 obj)
		{
			return this == obj;
		}

		// Token: 0x06004FEA RID: 20458 RVA: 0x0015EEA6 File Offset: 0x0015D2A6
		public override int GetHashCode()
		{
			return this.m_Value.GetHashCode();
		}

		// Token: 0x06004FEB RID: 20459 RVA: 0x0015EEB9 File Offset: 0x0015D2B9
		public static Float3 operator +(Float3 v0)
		{
			return v0;
		}

		// Token: 0x06004FEC RID: 20460 RVA: 0x0015EEBC File Offset: 0x0015D2BC
		public static Float3 operator +(Float3 v0, Float3 v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x + v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y + v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z + v1.m_Value.z;
			return result;
		}

		// Token: 0x06004FED RID: 20461 RVA: 0x0015EF44 File Offset: 0x0015D344
		public static Float3 operator -(Float3 v0)
		{
			Float3 result = default(Float3);
			result.m_Value.x = -v0.m_Value.x;
			result.m_Value.y = -v0.m_Value.y;
			result.m_Value.z = -v0.m_Value.z;
			return result;
		}

		// Token: 0x06004FEE RID: 20462 RVA: 0x0015EFA8 File Offset: 0x0015D3A8
		public static Float3 operator -(Float3 v0, Float3 v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x - v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y - v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z - v1.m_Value.z;
			return result;
		}

		// Token: 0x06004FEF RID: 20463 RVA: 0x0015F030 File Offset: 0x0015D430
		public static Float3 operator *(Float3 v0, Float3 v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x * v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y * v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z * v1.m_Value.z;
			return result;
		}

		// Token: 0x06004FF0 RID: 20464 RVA: 0x0015F0B8 File Offset: 0x0015D4B8
		public static Float3 operator *(Float3 v0, float v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x * v1;
			result.m_Value.y = v0.m_Value.y * v1;
			result.m_Value.z = v0.m_Value.z * v1;
			return result;
		}

		// Token: 0x06004FF1 RID: 20465 RVA: 0x0015F120 File Offset: 0x0015D520
		public static Float3 operator /(Float3 v0, Float3 v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x / v1.m_Value.x;
			result.m_Value.y = v0.m_Value.y / v1.m_Value.y;
			result.m_Value.z = v0.m_Value.z / v1.m_Value.z;
			return result;
		}

		// Token: 0x06004FF2 RID: 20466 RVA: 0x0015F1A8 File Offset: 0x0015D5A8
		public static Float3 operator /(Float3 v0, float v1)
		{
			Float3 result = default(Float3);
			result.m_Value.x = v0.m_Value.x / v1;
			result.m_Value.y = v0.m_Value.y / v1;
			result.m_Value.z = v0.m_Value.z / v1;
			return result;
		}

		// Token: 0x06004FF3 RID: 20467 RVA: 0x0015F20E File Offset: 0x0015D60E
		public static bool operator ==(Float3 v0, Float3 v1)
		{
			return v0.value == v1.value;
		}

		// Token: 0x06004FF4 RID: 20468 RVA: 0x0015F223 File Offset: 0x0015D623
		public static bool operator !=(Float3 v0, Float3 v1)
		{
			return v0.value != v1.value;
		}

		// Token: 0x04004EB4 RID: 20148
		[SerializeField]
		public Vector3 m_Value;
	}
}
