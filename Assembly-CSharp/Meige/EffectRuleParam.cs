﻿using System;

namespace Meige
{
	// Token: 0x02000EA3 RID: 3747
	[Serializable]
	public class EffectRuleParam<TYPE> : EffectRuleParamBase where TYPE : new()
	{
		// Token: 0x06004E4B RID: 20043 RVA: 0x0015C13C File Offset: 0x0015A53C
		public EffectRuleParam()
		{
			this.m_OrType = typeof(TYPE);
			this.m_TypeCode = EffectRuleParam<TYPE>.GetTypeCode(this.m_OrType);
			if (!this.m_OrType.IsValueType)
			{
				this.m_Value = Activator.CreateInstance<TYPE>();
			}
		}

		// Token: 0x06004E4C RID: 20044 RVA: 0x0015C18B File Offset: 0x0015A58B
		public eEffectAnimTypeCode GetTypeCode()
		{
			return this.m_TypeCode;
		}

		// Token: 0x06004E4D RID: 20045 RVA: 0x0015C193 File Offset: 0x0015A593
		public Type GetOrType()
		{
			return this.m_OrType;
		}

		// Token: 0x06004E4E RID: 20046 RVA: 0x0015C19B File Offset: 0x0015A59B
		public object GetValue(int tgtIdx)
		{
			return this.m_Value;
		}

		// Token: 0x06004E4F RID: 20047 RVA: 0x0015C1A8 File Offset: 0x0015A5A8
		public void SetValue(int tgtIdx, object value)
		{
			this.m_Value = (TYPE)((object)value);
		}

		// Token: 0x06004E50 RID: 20048 RVA: 0x0015C1B6 File Offset: 0x0015A5B6
		public object GetValue(int arrayIdx, int tgtIdx)
		{
			return this.m_Value;
		}

		// Token: 0x06004E51 RID: 20049 RVA: 0x0015C1C3 File Offset: 0x0015A5C3
		public void SetValue(int arrayIdx, int tgtIdx, object value)
		{
			this.m_Value = (TYPE)((object)value);
		}

		// Token: 0x06004E52 RID: 20050 RVA: 0x0015C1D1 File Offset: 0x0015A5D1
		public bool IsArray()
		{
			return false;
		}

		// Token: 0x06004E53 RID: 20051 RVA: 0x0015C1D4 File Offset: 0x0015A5D4
		public int GetArraySize()
		{
			return 1;
		}

		// Token: 0x06004E54 RID: 20052 RVA: 0x0015C1D7 File Offset: 0x0015A5D7
		public bool ChangeArraySize(int size)
		{
			return false;
		}

		// Token: 0x06004E55 RID: 20053 RVA: 0x0015C1DA File Offset: 0x0015A5DA
		private static eEffectAnimTypeCode GetTypeCode(Type type)
		{
			if (type.BaseType == typeof(Enum))
			{
				return eEffectAnimTypeCode.Enum;
			}
			return EffectHelper.GetTypeCodeFromType(type);
		}

		// Token: 0x04004E5B RID: 20059
		private eEffectAnimTypeCode m_TypeCode;

		// Token: 0x04004E5C RID: 20060
		private Type m_OrType;

		// Token: 0x04004E5D RID: 20061
		public TYPE m_Value;
	}
}
