﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000F47 RID: 3911
	public class PrimCircleBuffer : MonoBehaviour
	{
		// Token: 0x060050EF RID: 20719 RVA: 0x0016A5CC File Offset: 0x001689CC
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
			if (this.m_EffectMeshBuffer == null && meshBuffer != null)
			{
				this.m_EffectMeshBuffer = meshBuffer;
			}
		}

		// Token: 0x060050F0 RID: 20720 RVA: 0x0016A5F4 File Offset: 0x001689F4
		public void ClearExternalMeshBuffer()
		{
			if (this.m_EffectMeshBuffer != null && !this.m_MeshBufferIsSelf)
			{
				IEnumerator enumerator = this.m_CircleBufferList.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						PrimCircleBuffer.CircleBuffer circleBuffer = (PrimCircleBuffer.CircleBuffer)obj;
						this.m_EffectMeshBuffer.RemoveBuffer(circleBuffer.meshBuffer);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_EffectMeshBuffer = null;
			}
		}

		// Token: 0x060050F1 RID: 20721 RVA: 0x0016A688 File Offset: 0x00168A88
		private void OnDestroy()
		{
			this.ClearExternalMeshBuffer();
		}

		// Token: 0x060050F2 RID: 20722 RVA: 0x0016A690 File Offset: 0x00168A90
		internal void AddActiveBufferList(PrimCircleBuffer.CircleBuffer meshBuffer)
		{
			this.m_activeCircleBufferList.Add(meshBuffer);
		}

		// Token: 0x060050F3 RID: 20723 RVA: 0x0016A69F File Offset: 0x00168A9F
		internal void RemoveActiveBufferList(PrimCircleBuffer.CircleBuffer meshBuffer)
		{
			this.m_activeCircleBufferList.Remove(meshBuffer);
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x060050F5 RID: 20725 RVA: 0x0016A6B6 File Offset: 0x00168AB6
		// (set) Token: 0x060050F4 RID: 20724 RVA: 0x0016A6AD File Offset: 0x00168AAD
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x060050F7 RID: 20727 RVA: 0x0016A6C7 File Offset: 0x00168AC7
		// (set) Token: 0x060050F6 RID: 20726 RVA: 0x0016A6BE File Offset: 0x00168ABE
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x060050F9 RID: 20729 RVA: 0x0016A6D8 File Offset: 0x00168AD8
		// (set) Token: 0x060050F8 RID: 20728 RVA: 0x0016A6CF File Offset: 0x00168ACF
		public bool bufferChangeFlg { get; set; }

		// Token: 0x060050FA RID: 20730 RVA: 0x0016A6E0 File Offset: 0x00168AE0
		private void Start()
		{
			this.Setup();
		}

		// Token: 0x060050FB RID: 20731 RVA: 0x0016A6E8 File Offset: 0x00168AE8
		private void Setup()
		{
			if (this.m_EffectMeshBuffer == null && !this.m_MeshBufferIsSelf)
			{
				this.m_EffectMeshBuffer = base.gameObject.AddComponent<EffectMeshBuffer>();
				this.m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
				this.m_MeshBufferIsSelf = true;
			}
		}

		// Token: 0x060050FC RID: 20732 RVA: 0x0016A738 File Offset: 0x00168B38
		public virtual PrimCircleBuffer.CircleBuffer AddBuffer(int vtxNum)
		{
			this.Setup();
			PrimCircleBuffer.CircleBuffer circleBuffer = new PrimCircleBuffer.CircleBuffer();
			circleBuffer.vtxNum = vtxNum;
			circleBuffer.parent = this;
			circleBuffer.enabled = false;
			this.m_CircleBufferList.Add(circleBuffer);
			circleBuffer.UpdatePositions();
			circleBuffer.UpdateUVs();
			circleBuffer.UpdateColors();
			this.bufferChangeFlg = true;
			circleBuffer.meshBuffer = this.m_EffectMeshBuffer.AddBuffer(vtxNum * 2, (vtxNum - 1) * 4);
			for (int i = 0; i < vtxNum - 1; i++)
			{
				circleBuffer.meshBuffer.indices[i * 4] = i * 2;
				circleBuffer.meshBuffer.indices[i * 4 + 1] = i * 2 + 1;
				circleBuffer.meshBuffer.indices[i * 4 + 2] = i * 2 + 3;
				circleBuffer.meshBuffer.indices[i * 4 + 3] = i * 2 + 2;
			}
			circleBuffer.meshBuffer.UpdateIndices();
			return circleBuffer;
		}

		// Token: 0x060050FD RID: 20733 RVA: 0x0016A81C File Offset: 0x00168C1C
		public virtual void RemoveBuffer(PrimCircleBuffer.CircleBuffer circleBuffer)
		{
			if (this.m_EffectMeshBuffer == null)
			{
				return;
			}
			circleBuffer.enabled = false;
			int num = this.m_CircleBufferList.IndexOf(circleBuffer);
			if (num >= 0)
			{
				this.m_EffectMeshBuffer.RemoveBuffer(circleBuffer.meshBuffer);
				circleBuffer.meshBuffer = null;
				this.m_CircleBufferList.RemoveAt(num);
			}
		}

		// Token: 0x060050FE RID: 20734 RVA: 0x0016A87C File Offset: 0x00168C7C
		private void UpdateMesh()
		{
			if (this.m_CircleBufferList.Count == 0)
			{
				return;
			}
			int num = 0;
			for (int i = 0; i < this.m_CircleBufferList.Count; i++)
			{
				PrimCircleBuffer.CircleBuffer circleBuffer = this.m_CircleBufferList[i] as PrimCircleBuffer.CircleBuffer;
				if (circleBuffer != null)
				{
					EffectMeshBuffer.MeshBuffer meshBuffer = circleBuffer.meshBuffer;
					if (meshBuffer != null)
					{
						if (circleBuffer.enabled)
						{
							Quaternion rotation = Quaternion.Euler(circleBuffer.m_Rotation);
							for (int j = 0; j < circleBuffer.vtxNum; j++)
							{
								float num2 = (float)j / (float)(circleBuffer.vtxNum - 1);
								if (circleBuffer.isDirtyPositions)
								{
									float angle = Mathf.Lerp(circleBuffer.m_InStartAngle, circleBuffer.m_InEndAngle, num2);
									float angle2 = Mathf.Lerp(circleBuffer.m_OutStartAngle, circleBuffer.m_OutEndAngle, num2);
									float num3 = Mathf.Lerp(circleBuffer.m_OffsetStartInR, circleBuffer.m_OffsetEndInR, num2) + circleBuffer.m_InR;
									float num4 = Mathf.Lerp(circleBuffer.m_OffsetStartOutR, circleBuffer.m_OffsetEndOutR, num2) + circleBuffer.m_OutR;
									float num5 = Mathf.Lerp(circleBuffer.m_OffsetStartInH, circleBuffer.m_OffsetEndInH, num2) + circleBuffer.m_InH;
									float num6 = Mathf.Lerp(circleBuffer.m_OffsetStartOutH, circleBuffer.m_OffsetEndOutH, num2) + circleBuffer.m_OutH;
									float num7 = (float)(j % 2 * 2 - 1);
									num3 = Mathf.Lerp(circleBuffer.m_AddRandomOffsetStartInR, circleBuffer.m_AddRandomOffsetEndInR, num2) * num7 + num3;
									num4 = Mathf.Lerp(circleBuffer.m_AddRandomOffsetStartOutR, circleBuffer.m_AddRandomOffsetEndOutR, num2) * num7 + num4;
									num5 = Mathf.Lerp(circleBuffer.m_AddRandomOffsetStartInH, circleBuffer.m_AddRandomOffsetEndInH, num2) * num7 + num5;
									num6 = Mathf.Lerp(circleBuffer.m_AddRandomOffsetStartOutH, circleBuffer.m_AddRandomOffsetEndOutH, num2) * num7 + num6;
									float num8 = num3;
									float num9 = num4;
									float num10 = num5;
									float num11 = num6;
									int indexFromAngle = CircleVertexBase.GetIndexFromAngle(angle);
									int indexFromAngle2 = CircleVertexBase.GetIndexFromAngle(angle2);
									meshBuffer.vertices[j * 2].x = CircleVertexBase.m_Vertices[indexFromAngle].x * num8 * circleBuffer.m_Scale.x;
									meshBuffer.vertices[j * 2].y = CircleVertexBase.m_Vertices[indexFromAngle].y * num8 * circleBuffer.m_Scale.y;
									meshBuffer.vertices[j * 2].z = num10 * circleBuffer.m_Scale.z;
									meshBuffer.vertices[j * 2 + 1].x = CircleVertexBase.m_Vertices[indexFromAngle2].x * num9 * circleBuffer.m_Scale.x;
									meshBuffer.vertices[j * 2 + 1].y = CircleVertexBase.m_Vertices[indexFromAngle2].y * num9 * circleBuffer.m_Scale.y;
									meshBuffer.vertices[j * 2 + 1].z = num11 * circleBuffer.m_Scale.z;
									meshBuffer.vertices[j * 2] = rotation * meshBuffer.vertices[j * 2] + circleBuffer.m_Position;
									meshBuffer.vertices[j * 2 + 1] = rotation * meshBuffer.vertices[j * 2 + 1] + circleBuffer.m_Position;
								}
								if (circleBuffer.isDirtyUVs)
								{
									if (circleBuffer.m_UVReverse)
									{
										float num12 = circleBuffer.m_UVRect.height * num2;
										meshBuffer.UVs[j * 2].x = circleBuffer.m_UVRect.x;
										meshBuffer.UVs[j * 2].y = circleBuffer.m_UVRect.y + num12;
										meshBuffer.UVs[j * 2 + 1].x = circleBuffer.m_UVRect.x + circleBuffer.m_UVRect.width;
										meshBuffer.UVs[j * 2 + 1].y = circleBuffer.m_UVRect.y + num12;
									}
									else
									{
										float num13 = circleBuffer.m_UVRect.width * num2;
										meshBuffer.UVs[j * 2].x = circleBuffer.m_UVRect.x + num13;
										meshBuffer.UVs[j * 2].y = circleBuffer.m_UVRect.y;
										meshBuffer.UVs[j * 2 + 1].x = circleBuffer.m_UVRect.x + num13;
										meshBuffer.UVs[j * 2 + 1].y = circleBuffer.m_UVRect.y + circleBuffer.m_UVRect.height;
									}
								}
								if (circleBuffer.isDirtyColors)
								{
									meshBuffer.colors[j * 2] = Color.Lerp(circleBuffer.m_ColorStartInR, circleBuffer.m_ColorEndInR, num2);
									meshBuffer.colors[j * 2 + 1] = Color.Lerp(circleBuffer.m_ColorStartOutR, circleBuffer.m_ColorEndOutR, num2);
								}
							}
							if (circleBuffer.isDirtyPositions)
							{
								meshBuffer.UpdateVertices();
								circleBuffer.isDirtyPositions = false;
							}
							if (circleBuffer.isDirtyUVs)
							{
								meshBuffer.UpdateUVs();
								circleBuffer.isDirtyUVs = false;
							}
							if (circleBuffer.isDirtyColors)
							{
								meshBuffer.UpdateColors();
								circleBuffer.isDirtyColors = false;
							}
							num++;
							meshBuffer.enabled = true;
						}
						else
						{
							meshBuffer.enabled = false;
						}
					}
				}
			}
			if (num == 0)
			{
				base.enabled = false;
			}
		}

		// Token: 0x060050FF RID: 20735 RVA: 0x0016AE1F File Offset: 0x0016921F
		private void Update()
		{
			this.UpdateMesh();
		}

		// Token: 0x04005008 RID: 20488
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04005009 RID: 20489
		protected bool m_MeshBufferIsSelf;

		// Token: 0x0400500A RID: 20490
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x0400500B RID: 20491
		protected ArrayList m_CircleBufferList = new ArrayList();

		// Token: 0x0400500C RID: 20492
		protected ArrayList m_activeCircleBufferList = new ArrayList(32);

		// Token: 0x02000F48 RID: 3912
		[Serializable]
		public class CircleBuffer
		{
			// Token: 0x06005101 RID: 20737 RVA: 0x0016AEB2 File Offset: 0x001692B2
			public void UpdatePositions()
			{
				this.isDirtyPositions = true;
			}

			// Token: 0x06005102 RID: 20738 RVA: 0x0016AEBB File Offset: 0x001692BB
			public void UpdateUVs()
			{
				this.isDirtyUVs = true;
			}

			// Token: 0x06005103 RID: 20739 RVA: 0x0016AEC4 File Offset: 0x001692C4
			public void UpdateColors()
			{
				this.isDirtyColors = true;
			}

			// Token: 0x17000566 RID: 1382
			// (get) Token: 0x06005104 RID: 20740 RVA: 0x0016AECD File Offset: 0x001692CD
			// (set) Token: 0x06005105 RID: 20741 RVA: 0x0016AED8 File Offset: 0x001692D8
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.parent.RenderFlg = true;
						this.parent.AddActiveBufferList(this);
					}
					else
					{
						this.parent.RemoveActiveBufferList(this);
					}
					this.parent.m_maxTriangleBufferChanged = true;
					this.UpdatePositions();
					this.UpdateColors();
				}
			}

			// Token: 0x0400500F RID: 20495
			internal PrimCircleBuffer parent;

			// Token: 0x04005010 RID: 20496
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04005011 RID: 20497
			internal int vtxNum;

			// Token: 0x04005012 RID: 20498
			internal bool isDirtyPositions;

			// Token: 0x04005013 RID: 20499
			internal bool isDirtyUVs;

			// Token: 0x04005014 RID: 20500
			internal bool isDirtyColors;

			// Token: 0x04005015 RID: 20501
			public Vector3 m_Position;

			// Token: 0x04005016 RID: 20502
			public Vector3 m_Rotation;

			// Token: 0x04005017 RID: 20503
			public Vector3 m_Scale = Vector3.one;

			// Token: 0x04005018 RID: 20504
			public float m_InStartAngle;

			// Token: 0x04005019 RID: 20505
			public float m_InEndAngle = 360f;

			// Token: 0x0400501A RID: 20506
			public float m_OutStartAngle;

			// Token: 0x0400501B RID: 20507
			public float m_OutEndAngle = 360f;

			// Token: 0x0400501C RID: 20508
			public float m_InR;

			// Token: 0x0400501D RID: 20509
			public float m_OutR = 100f;

			// Token: 0x0400501E RID: 20510
			public float m_InH;

			// Token: 0x0400501F RID: 20511
			public float m_OutH;

			// Token: 0x04005020 RID: 20512
			public float m_OffsetStartInR;

			// Token: 0x04005021 RID: 20513
			public float m_OffsetEndInR;

			// Token: 0x04005022 RID: 20514
			public float m_OffsetStartOutR;

			// Token: 0x04005023 RID: 20515
			public float m_OffsetEndOutR;

			// Token: 0x04005024 RID: 20516
			public float m_OffsetStartInH;

			// Token: 0x04005025 RID: 20517
			public float m_OffsetEndInH;

			// Token: 0x04005026 RID: 20518
			public float m_OffsetStartOutH;

			// Token: 0x04005027 RID: 20519
			public float m_OffsetEndOutH;

			// Token: 0x04005028 RID: 20520
			public float m_AddRandomOffsetStartInR;

			// Token: 0x04005029 RID: 20521
			public float m_AddRandomOffsetEndInR;

			// Token: 0x0400502A RID: 20522
			public float m_AddRandomOffsetStartOutR;

			// Token: 0x0400502B RID: 20523
			public float m_AddRandomOffsetEndOutR;

			// Token: 0x0400502C RID: 20524
			public float m_AddRandomOffsetStartInH;

			// Token: 0x0400502D RID: 20525
			public float m_AddRandomOffsetEndInH;

			// Token: 0x0400502E RID: 20526
			public float m_AddRandomOffsetStartOutH;

			// Token: 0x0400502F RID: 20527
			public float m_AddRandomOffsetEndOutH;

			// Token: 0x04005030 RID: 20528
			public Rect m_UVRect = Rect.MinMaxRect(0f, 0f, 1f, 1f);

			// Token: 0x04005031 RID: 20529
			public bool m_UVReverse;

			// Token: 0x04005032 RID: 20530
			public Color m_ColorStartInR = Color.white;

			// Token: 0x04005033 RID: 20531
			public Color m_ColorEndInR = Color.white;

			// Token: 0x04005034 RID: 20532
			public Color m_ColorStartOutR = Color.white;

			// Token: 0x04005035 RID: 20533
			public Color m_ColorEndOutR = Color.white;

			// Token: 0x04005036 RID: 20534
			private bool m_enabled;
		}
	}
}
