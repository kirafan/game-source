﻿using System;

namespace Meige
{
	// Token: 0x02000F03 RID: 3843
	[Serializable]
	public class RangePareVector3 : RangePare<Float3>
	{
		// Token: 0x06004F36 RID: 20278 RVA: 0x0015DE39 File Offset: 0x0015C239
		public RangePareVector3()
		{
		}

		// Token: 0x06004F37 RID: 20279 RVA: 0x0015DE41 File Offset: 0x0015C241
		public RangePareVector3(Float3 min, Float3 max) : base(min, max)
		{
		}

		// Token: 0x06004F38 RID: 20280 RVA: 0x0015DE4B File Offset: 0x0015C24B
		public RangePareVector3(RangePareVector3 v) : base(v)
		{
		}

		// Token: 0x06004F39 RID: 20281 RVA: 0x0015DE54 File Offset: 0x0015C254
		public new RangePareVector3 Clone()
		{
			return new RangePareVector3(this);
		}
	}
}
