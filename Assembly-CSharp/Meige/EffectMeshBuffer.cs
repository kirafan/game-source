﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Meige
{
	// Token: 0x02000E9B RID: 3739
	public class EffectMeshBuffer : MonoBehaviour
	{
		// Token: 0x06004DD5 RID: 19925 RVA: 0x0015A4D4 File Offset: 0x001588D4
		public EffectMeshBuffer()
		{
			MeshTopology[] array = new MeshTopology[4];
			RuntimeHelpers.InitializeArray(array, fieldof(<PrivateImplementationDetails>.$field-B7463E121B746C9C4CAE4787FEB60ED0706BA039).FieldHandle);
			this.m_MeshTopologyTable = array;
			this.m_maxPrimitiveCount = 512;
			this.m_MeshBufferList = new List<EffectMeshBuffer.MeshBuffer>();
			this.m_ActiveMeshBufferList = new List<EffectMeshBuffer.MeshBuffer>(32);
			base..ctor();
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x06004DD7 RID: 19927 RVA: 0x0015A52A File Offset: 0x0015892A
		// (set) Token: 0x06004DD6 RID: 19926 RVA: 0x0015A521 File Offset: 0x00158921
		public bool isUsableSubUVsFix
		{
			get
			{
				return this.m_isUsableSubUVs;
			}
			protected set
			{
				this.m_isUsableSubUVs = value;
			}
		}

		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x06004DD9 RID: 19929 RVA: 0x0015A53B File Offset: 0x0015893B
		// (set) Token: 0x06004DD8 RID: 19928 RVA: 0x0015A532 File Offset: 0x00158932
		public bool isUsableNormalsFix
		{
			get
			{
				return this.m_isUsableNormals;
			}
			protected set
			{
				this.m_isUsableNormals = value;
			}
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x06004DDB RID: 19931 RVA: 0x0015A54C File Offset: 0x0015894C
		// (set) Token: 0x06004DDA RID: 19930 RVA: 0x0015A543 File Offset: 0x00158943
		public bool isUsableUVRegionRepeat
		{
			get
			{
				return this.m_isUsableUVRegionRepeat;
			}
			protected set
			{
				this.m_isUsableUVRegionRepeat = value;
			}
		}

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x06004DDD RID: 19933 RVA: 0x0015A55D File Offset: 0x0015895D
		// (set) Token: 0x06004DDC RID: 19932 RVA: 0x0015A554 File Offset: 0x00158954
		protected bool m_vertsChanged { get; set; }

		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x06004DDF RID: 19935 RVA: 0x0015A56E File Offset: 0x0015896E
		// (set) Token: 0x06004DDE RID: 19934 RVA: 0x0015A565 File Offset: 0x00158965
		protected bool m_normalsChanged { get; set; }

		// Token: 0x17000524 RID: 1316
		// (get) Token: 0x06004DE1 RID: 19937 RVA: 0x0015A57F File Offset: 0x0015897F
		// (set) Token: 0x06004DE0 RID: 19936 RVA: 0x0015A576 File Offset: 0x00158976
		protected bool m_uvsChanged { get; set; }

		// Token: 0x17000525 RID: 1317
		// (get) Token: 0x06004DE3 RID: 19939 RVA: 0x0015A590 File Offset: 0x00158990
		// (set) Token: 0x06004DE2 RID: 19938 RVA: 0x0015A587 File Offset: 0x00158987
		protected bool m_subuvsChanged { get; set; }

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x06004DE5 RID: 19941 RVA: 0x0015A5A1 File Offset: 0x001589A1
		// (set) Token: 0x06004DE4 RID: 19940 RVA: 0x0015A598 File Offset: 0x00158998
		protected bool m_colorsChanged { get; set; }

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x06004DE7 RID: 19943 RVA: 0x0015A5B2 File Offset: 0x001589B2
		// (set) Token: 0x06004DE6 RID: 19942 RVA: 0x0015A5A9 File Offset: 0x001589A9
		protected bool m_IndicesChanged { get; set; }

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x06004DE9 RID: 19945 RVA: 0x0015A5C3 File Offset: 0x001589C3
		// (set) Token: 0x06004DE8 RID: 19944 RVA: 0x0015A5BA File Offset: 0x001589BA
		protected bool m_RepeatUVRegionChanged { get; set; }

		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x06004DEA RID: 19946 RVA: 0x0015A5CB File Offset: 0x001589CB
		public int meshBufferCount
		{
			get
			{
				return this.m_MeshBufferList.Count;
			}
		}

		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x06004DEB RID: 19947 RVA: 0x0015A5D8 File Offset: 0x001589D8
		public int vertexBufferCount
		{
			get
			{
				if (this.m_vertices == null)
				{
					return 0;
				}
				return this.m_vertices.Length;
			}
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06004DEC RID: 19948 RVA: 0x0015A5EF File Offset: 0x001589EF
		public int indexBufferCount
		{
			get
			{
				if (this.m_Indices == null)
				{
					return 0;
				}
				return this.m_Indices.Length;
			}
		}

		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06004DED RID: 19949 RVA: 0x0015A606 File Offset: 0x00158A06
		public int primitiveCount
		{
			get
			{
				return this.m_primitiveCount;
			}
		}

		// Token: 0x06004DEE RID: 19950 RVA: 0x0015A60E File Offset: 0x00158A0E
		public void AddHook_Update(EffectMeshBuffer.HookUpdate hook)
		{
			this.m_Hook_Update = (EffectMeshBuffer.HookUpdate)Delegate.Combine(this.m_Hook_Update, hook);
		}

		// Token: 0x06004DEF RID: 19951 RVA: 0x0015A627 File Offset: 0x00158A27
		public void RemoveHook_Update(EffectMeshBuffer.HookUpdate hook)
		{
			this.m_Hook_Update = (EffectMeshBuffer.HookUpdate)Delegate.Remove(this.m_Hook_Update, hook);
		}

		// Token: 0x06004DF0 RID: 19952 RVA: 0x0015A640 File Offset: 0x00158A40
		public void AddHook_OnWillRender(EffectMeshBuffer.HookUpdate hook)
		{
			this.m_Hook_OnWillRender = (EffectMeshBuffer.HookUpdate)Delegate.Combine(this.m_Hook_OnWillRender, hook);
		}

		// Token: 0x06004DF1 RID: 19953 RVA: 0x0015A659 File Offset: 0x00158A59
		public void RemoveHook_OnWillRender(EffectMeshBuffer.HookUpdate hook)
		{
			this.m_Hook_OnWillRender = (EffectMeshBuffer.HookUpdate)Delegate.Remove(this.m_Hook_OnWillRender, hook);
		}

		// Token: 0x06004DF2 RID: 19954 RVA: 0x0015A674 File Offset: 0x00158A74
		private void Awake()
		{
			this.m_vertsChanged = false;
			this.m_normalsChanged = false;
			this.m_uvsChanged = false;
			this.m_subuvsChanged = false;
			this.m_colorsChanged = false;
			this.m_IndicesChanged = false;
			this.m_RepeatUVRegionChanged = false;
			this.m_vTopIndex = 0;
			this.m_iTopIndex = 0;
			this.m_primitiveCount = 0;
		}

		// Token: 0x06004DF3 RID: 19955 RVA: 0x0015A6C8 File Offset: 0x00158AC8
		private void Start()
		{
			EffectObjectManager.Instance.RegisterMeshBufferComponent(this);
			this.m_MeshFilter = base.gameObject.AddComponent<MeshFilter>();
			if (this.m_MeshFilter == null)
			{
				this.m_MeshFilter = base.gameObject.GetComponent<MeshFilter>();
			}
			this.m_Mesh = this.m_MeshFilter.mesh;
			this.m_Mesh.MarkDynamic();
			this.m_Mesh.SetIndices(null, MeshTopology.Triangles, 0);
			Bounds bounds = this.m_Mesh.bounds;
			bounds.size = Vector3.one * 10000f;
			this.m_Mesh.bounds = bounds;
			this.isUsableSubUVsFix = this.m_isUsableSubUVs;
			this.isUsableNormalsFix = this.m_isUsableNormals;
		}

		// Token: 0x06004DF4 RID: 19956 RVA: 0x0015A784 File Offset: 0x00158B84
		private void OnDestroy()
		{
			if (EffectObjectManager.Instance != null)
			{
				EffectObjectManager.Instance.RemoveMeshBufferComponent(this);
			}
		}

		// Token: 0x06004DF5 RID: 19957 RVA: 0x0015A7A4 File Offset: 0x00158BA4
		private void Update()
		{
			if (this.m_Indices != null)
			{
				this.m_primitiveCount = this.CalcPrimitiveNum(this.m_Indices.Length);
			}
			else
			{
				this.m_primitiveCount = 0;
			}
			if (this.m_Hook_Update != null)
			{
				this.m_Hook_Update();
			}
		}

		// Token: 0x06004DF6 RID: 19958 RVA: 0x0015A7F2 File Offset: 0x00158BF2
		private void OnWillRenderObject()
		{
			if (this.m_Hook_OnWillRender != null)
			{
				this.m_Hook_OnWillRender();
			}
			this.MeshUpdate();
		}

		// Token: 0x06004DF7 RID: 19959 RVA: 0x0015A810 File Offset: 0x00158C10
		protected virtual void MeshUpdate()
		{
			Vector3[] array = this.m_vertices;
			Vector3[] normals = this.m_normals;
			Vector2[] uv = this.m_UVs;
			Vector2[] uv2 = this.m_SubUVs;
			Color[] colors = this.m_colors;
			int[] indices = this.m_Indices;
			List<Vector4> repeatUVRegion = this.m_RepeatUVRegion;
			if (this.m_maxPrimitiveCountWork > 0 && (this.m_maxPrimitiveCountWork != this.m_maxPrimitiveCount || this.m_MeshTopology != this.m_MeshTopologyWork))
			{
				int num = this.CalcIndexMax(this.m_maxPrimitiveCountWork);
				this.m_verticesWork = new Vector3[num];
				this.m_UVsWork = new Vector2[num];
				this.m_colorsWork = new Color[num];
				this.m_IndicesWork = new int[num];
				if (this.isUsableSubUVsFix)
				{
					this.m_SubUVsWork = new Vector2[num];
				}
				if (this.isUsableNormalsFix)
				{
					this.m_normalsWork = new Vector3[num];
				}
				this.m_maxPrimitiveCountWork = this.m_maxPrimitiveCount;
				this.m_MeshTopologyWork = this.m_MeshTopology;
			}
			if (this.RenderFlg || this.bufferChangeFlg)
			{
				this.bufferChangeFlg = true;
				if (this.m_maxPrimitiveCountWork > 0 && this.m_primitiveCount > this.m_maxPrimitiveCountWork)
				{
					int num2 = 0;
					int num3 = 0;
					foreach (EffectMeshBuffer.MeshBuffer meshBuffer in this.m_ActiveMeshBufferList)
					{
						if (num2 + meshBuffer.vertices.Length >= this.m_verticesWork.Length || num3 + meshBuffer.indices.Length >= this.m_IndicesWork.Length)
						{
							break;
						}
						if (meshBuffer.enabled)
						{
							if (this.m_maxTriangleBufferChanged)
							{
								meshBuffer.isDirtyVerts = true;
								meshBuffer.isDirtyUVs = true;
								meshBuffer.isDirtyColors = true;
								meshBuffer.isDirtyIndices = true;
								if (this.isUsableSubUVsFix)
								{
									meshBuffer.isDirtySubUVs = true;
								}
								if (this.isUsableNormalsFix)
								{
									meshBuffer.isDirtyNorms = true;
								}
							}
							if (meshBuffer.isDirtyVerts)
							{
								meshBuffer.vertices.CopyTo(this.m_verticesWork, num2);
								this.m_vertsChanged = true;
							}
							if (meshBuffer.isDirtyNorms)
							{
								meshBuffer.normals.CopyTo(this.m_normalsWork, num2);
								this.m_normalsChanged = true;
							}
							if (meshBuffer.isDirtyUVs)
							{
								meshBuffer.UVs.CopyTo(this.m_UVsWork, num2);
								this.m_uvsChanged = true;
							}
							if (meshBuffer.isDirtyColors)
							{
								meshBuffer.colors.CopyTo(this.m_colorsWork, num2);
								this.m_colorsChanged = true;
							}
							if (meshBuffer.isDirtyIndices)
							{
								for (int i = 0; i < meshBuffer.indices.Length; i++)
								{
									this.m_IndicesWork[i + num3] = meshBuffer.indices[i] + num2;
								}
								this.m_IndicesChanged = true;
							}
							if (meshBuffer.isDirtySubUVs)
							{
								meshBuffer.SubUVs.CopyTo(this.m_SubUVsWork, num2);
								this.m_subuvsChanged = true;
							}
							if (meshBuffer.isDirtyRepeatUVRegion)
							{
								meshBuffer.repeatUVRegion.CopyTo(this.m_RepeatUVRegionWork.ToArray(), num2);
								this.m_RepeatUVRegionChanged = true;
							}
							num2 += meshBuffer.vertices.Length;
							num3 += meshBuffer.indices.Length;
							meshBuffer.isDirtyVerts = false;
							meshBuffer.isDirtyUVs = false;
							meshBuffer.isDirtyColors = false;
							meshBuffer.isDirtyIndices = false;
							if (this.isUsableSubUVsFix)
							{
								meshBuffer.isDirtySubUVs = false;
							}
							if (this.isUsableNormalsFix)
							{
								meshBuffer.isDirtyNorms = false;
							}
						}
					}
					if (this.m_maxTriangleBufferChanged)
					{
						for (int j = num3; j < this.m_IndicesWork.Length; j++)
						{
							this.m_IndicesWork[j] = num2;
						}
					}
					array = this.m_verticesWork;
					normals = this.m_normalsWork;
					uv = this.m_UVsWork;
					colors = this.m_colorsWork;
					indices = this.m_IndicesWork;
					if (this.isUsableSubUVsFix)
					{
						uv2 = this.m_SubUVsWork;
					}
					this.m_maxTriangleBufferChanged = false;
				}
				if (this.m_MeshBufferList != null)
				{
					int num4 = 0;
					foreach (EffectMeshBuffer.MeshBuffer meshBuffer2 in this.m_MeshBufferList)
					{
						if (meshBuffer2.enabled)
						{
							num4++;
						}
					}
					if (num4 == 0)
					{
						this.RenderFlg = false;
					}
				}
			}
			if (this.m_vertsChanged)
			{
				if (array == null)
				{
					return;
				}
				this.m_Mesh.vertices = array;
				if (this.m_Mesh.vertexCount != array.Length)
				{
					return;
				}
				this.m_vertsChanged = false;
				if (!this.bInit || this.m_Mesh.bounds.size.x < 100f)
				{
					Bounds bounds = this.m_Mesh.bounds;
					bounds.size = Vector3.one * 10000f;
					this.m_Mesh.bounds = bounds;
					this.bInit = true;
				}
			}
			bool flag = this.m_Mesh.vertexCount > 0;
			if (this.m_normalsChanged && flag)
			{
				this.m_Mesh.normals = normals;
				this.m_normalsChanged = false;
			}
			if (this.m_uvsChanged && flag)
			{
				this.m_Mesh.uv = uv;
				this.m_uvsChanged = false;
			}
			if (this.m_colorsChanged && flag)
			{
				this.m_Mesh.colors = colors;
				this.m_colorsChanged = false;
			}
			if (this.m_IndicesChanged && flag)
			{
				this.m_Mesh.SetIndices(indices, this.m_MeshTopologyTable[(int)this.m_MeshTopology], 0);
				this.m_IndicesChanged = false;
			}
			if (this.isUsableSubUVsFix && this.m_subuvsChanged && flag)
			{
				this.m_Mesh.uv2 = uv2;
				this.m_subuvsChanged = false;
			}
			if (this.isUsableUVRegionRepeat && this.m_RepeatUVRegionChanged && flag)
			{
				this.m_Mesh.SetUVs(3, repeatUVRegion);
				this.m_RepeatUVRegionChanged = false;
			}
		}

		// Token: 0x06004DF8 RID: 19960 RVA: 0x0015AE78 File Offset: 0x00159278
		internal void AddActiveBufferList(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			this.m_ActiveMeshBufferList.Add(meshBuffer);
		}

		// Token: 0x06004DF9 RID: 19961 RVA: 0x0015AE86 File Offset: 0x00159286
		internal void RemoveActiveBufferList(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			this.m_ActiveMeshBufferList.Remove(meshBuffer);
		}

		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x06004DFB RID: 19963 RVA: 0x0015AE9E File Offset: 0x0015929E
		// (set) Token: 0x06004DFA RID: 19962 RVA: 0x0015AE95 File Offset: 0x00159295
		public bool m_maxTriangleBufferChanged { get; set; }

		// Token: 0x06004DFC RID: 19964 RVA: 0x0015AEA8 File Offset: 0x001592A8
		public virtual int CalcPrimitiveNum(int IndicesNum)
		{
			switch (this.m_MeshTopology)
			{
			case EffectMeshBuffer.Topology.Triangles:
				return IndicesNum / 3;
			case EffectMeshBuffer.Topology.Quads:
				return IndicesNum / 4;
			case EffectMeshBuffer.Topology.Lines:
				return IndicesNum / 2;
			case EffectMeshBuffer.Topology.Points:
				return IndicesNum;
			default:
				return 0;
			}
		}

		// Token: 0x06004DFD RID: 19965 RVA: 0x0015AEE8 File Offset: 0x001592E8
		public virtual int CalcIndexMax(int primitiveNum)
		{
			switch (this.m_MeshTopology)
			{
			case EffectMeshBuffer.Topology.Triangles:
				return primitiveNum * 3;
			case EffectMeshBuffer.Topology.Quads:
				return primitiveNum * 4;
			case EffectMeshBuffer.Topology.Lines:
				return primitiveNum * 2;
			case EffectMeshBuffer.Topology.Points:
				return primitiveNum;
			default:
				return 0;
			}
		}

		// Token: 0x06004DFE RID: 19966 RVA: 0x0015AF28 File Offset: 0x00159328
		public virtual EffectMeshBuffer.MeshBuffer AddBuffer(int vertexCount, int indexCount)
		{
			EffectMeshBuffer.MeshBuffer meshBuffer = new EffectMeshBuffer.MeshBuffer();
			meshBuffer.vertices = new Vector3[vertexCount];
			meshBuffer.UVs = new Vector2[vertexCount];
			meshBuffer.colors = new Color[vertexCount];
			meshBuffer.indices = new int[indexCount];
			if (this.isUsableSubUVsFix)
			{
				meshBuffer.SubUVs = new Vector2[vertexCount];
			}
			if (this.isUsableNormalsFix)
			{
				meshBuffer.normals = new Vector3[vertexCount];
			}
			meshBuffer.vTopIndex = this.m_vTopIndex;
			meshBuffer.iTopIndex = this.m_iTopIndex;
			this.m_vTopIndex += vertexCount;
			this.m_iTopIndex += indexCount;
			int vTopIndex = this.m_vTopIndex;
			int iTopIndex = this.m_iTopIndex;
			Vector3[] array = new Vector3[vTopIndex];
			Vector2[] array2 = new Vector2[vTopIndex];
			Color[] array3 = new Color[vTopIndex];
			int[] array4 = new int[iTopIndex];
			Vector2[] array5 = null;
			if (this.isUsableSubUVsFix)
			{
				array5 = new Vector2[vTopIndex];
			}
			Vector3[] array6 = null;
			if (this.isUsableNormalsFix)
			{
				array6 = new Vector3[vTopIndex];
			}
			if (this.meshBufferCount != 0)
			{
				this.m_vertices.CopyTo(array, 0);
				this.m_UVs.CopyTo(array2, 0);
				this.m_colors.CopyTo(array3, 0);
				this.m_Indices.CopyTo(array4, 0);
				if (this.isUsableSubUVsFix)
				{
					this.m_SubUVs.CopyTo(array5, 0);
				}
				if (this.isUsableNormalsFix)
				{
					this.m_normals.CopyTo(array6, 0);
				}
			}
			this.m_vertices = array;
			this.m_UVs = array2;
			this.m_colors = array3;
			this.m_Indices = array4;
			if (this.isUsableSubUVsFix)
			{
				this.m_SubUVs = array5;
			}
			if (this.isUsableNormalsFix)
			{
				this.m_normals = array6;
			}
			meshBuffer.meshBuffer = this;
			meshBuffer.enabled = false;
			this.m_MeshBufferList.Add(meshBuffer);
			meshBuffer.UpdateVertices();
			meshBuffer.UpdateUVs();
			meshBuffer.UpdateColors();
			meshBuffer.UpdateIndices();
			if (this.isUsableSubUVsFix)
			{
				meshBuffer.UpdateSubUVs();
			}
			if (this.isUsableNormalsFix)
			{
				meshBuffer.UpdateNormals();
			}
			this.bufferChangeFlg = true;
			return meshBuffer;
		}

		// Token: 0x06004DFF RID: 19967 RVA: 0x0015B13C File Offset: 0x0015953C
		public virtual void RemoveBuffer(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			meshBuffer.enabled = false;
			int num = this.m_MeshBufferList.IndexOf(meshBuffer);
			if (num >= 0)
			{
				int num2 = meshBuffer.vertices.Length;
				int num3 = meshBuffer.indices.Length;
				int num4 = this.m_vertices.Length - num2;
				int num5 = this.m_Indices.Length - num3;
				this.m_vTopIndex -= num2;
				this.m_iTopIndex -= num3;
				if (num4 > 0 && num5 > 0)
				{
					Vector3[] vertices = new Vector3[num4];
					Vector2[] uvs = new Vector2[num4];
					Color[] colors = new Color[num4];
					int[] indices = new int[num5];
					if (this.isUsableSubUVsFix)
					{
						Vector2[] subUVs = new Vector2[num4];
						this.m_SubUVs = subUVs;
					}
					if (this.isUsableNormalsFix)
					{
						Vector3[] normals = new Vector3[num4];
						this.m_normals = normals;
					}
					this.m_vertices = vertices;
					this.m_UVs = uvs;
					this.m_colors = colors;
					this.m_Indices = indices;
				}
				else
				{
					this.m_vertices = null;
					this.m_UVs = null;
					this.m_colors = null;
					this.m_Indices = null;
					if (this.isUsableSubUVsFix)
					{
						this.m_SubUVs = null;
					}
					if (this.isUsableNormalsFix)
					{
						this.m_normals = null;
					}
				}
				this.m_MeshBufferList.RemoveAt(num);
				int num6 = 0;
				int num7 = 0;
				foreach (EffectMeshBuffer.MeshBuffer meshBuffer2 in this.m_MeshBufferList)
				{
					meshBuffer2.vTopIndex = num6;
					meshBuffer2.iTopIndex = num7;
					meshBuffer2.UpdateVertices();
					meshBuffer2.UpdateUVs();
					meshBuffer2.UpdateColors();
					meshBuffer2.UpdateIndices();
					if (this.isUsableSubUVsFix)
					{
						meshBuffer2.UpdateSubUVs();
					}
					if (this.isUsableNormalsFix)
					{
						meshBuffer2.UpdateNormals();
					}
					this.bufferChangeFlg = true;
					num6 += meshBuffer2.vertices.Length;
					num7 += meshBuffer2.indices.Length;
				}
			}
			if (this.m_Mesh.triangles == null)
			{
				return;
			}
			if (this.m_Mesh.triangles.Length <= 0)
			{
				return;
			}
			int[] indices2 = new int[3];
			this.m_Mesh.SetIndices(indices2, MeshTopology.Triangles, 0);
		}

		// Token: 0x06004E00 RID: 19968 RVA: 0x0015B390 File Offset: 0x00159790
		public virtual void RemoveBuffer(EffectMeshBuffer.MeshBuffer[] meshBuffer)
		{
			int num = this.m_vertices.Length;
			int num2 = this.m_Indices.Length;
			for (int i = 0; i < meshBuffer.Length; i++)
			{
				meshBuffer[i].enabled = false;
				int num3 = this.m_MeshBufferList.IndexOf(meshBuffer[i]);
				if (num3 >= 0)
				{
					int num4 = meshBuffer[i].vertices.Length;
					int num5 = meshBuffer[i].indices.Length;
					num -= num4;
					num2 -= num5;
					this.m_vTopIndex -= num4;
					this.m_iTopIndex -= num5;
					this.m_MeshBufferList.RemoveAt(num3);
				}
			}
			if (num > 0 && num2 > 0)
			{
				Vector3[] vertices = new Vector3[num];
				Vector2[] uvs = new Vector2[num];
				Color[] colors = new Color[num];
				int[] indices = new int[num2];
				if (this.isUsableSubUVsFix)
				{
					Vector2[] subUVs = new Vector2[num];
					this.m_SubUVs = subUVs;
				}
				if (this.isUsableSubUVsFix)
				{
					Vector3[] normals = new Vector3[num];
					this.m_normals = normals;
				}
				this.m_vertices = vertices;
				this.m_UVs = uvs;
				this.m_colors = colors;
				this.m_Indices = indices;
			}
			else
			{
				this.m_vertices = null;
				this.m_UVs = null;
				this.m_colors = null;
				this.m_Indices = null;
				if (this.isUsableSubUVsFix)
				{
					this.m_SubUVs = null;
				}
				if (this.isUsableNormalsFix)
				{
					this.m_normals = null;
				}
			}
			int num6 = 0;
			int num7 = 0;
			foreach (EffectMeshBuffer.MeshBuffer meshBuffer2 in this.m_MeshBufferList)
			{
				meshBuffer2.vTopIndex = num6;
				meshBuffer2.iTopIndex = num7;
				meshBuffer2.UpdateVertices();
				meshBuffer2.UpdateUVs();
				meshBuffer2.UpdateColors();
				meshBuffer2.UpdateIndices();
				if (this.isUsableSubUVsFix)
				{
					meshBuffer2.UpdateSubUVs();
				}
				if (this.isUsableNormalsFix)
				{
					meshBuffer2.UpdateNormals();
				}
				this.bufferChangeFlg = true;
				num6 += meshBuffer2.vertices.Length;
				num7 += meshBuffer2.indices.Length;
			}
			if (this.m_Mesh.triangles == null)
			{
				return;
			}
			if (this.m_Mesh.triangles.Length <= 0)
			{
				return;
			}
			int[] indices2 = new int[3];
			this.m_Mesh.SetIndices(indices2, MeshTopology.Triangles, 0);
		}

		// Token: 0x06004E01 RID: 19969 RVA: 0x0015B608 File Offset: 0x00159A08
		internal void SetVertices(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			if (meshBuffer.enabled)
			{
				meshBuffer.vertices.CopyTo(this.m_vertices, meshBuffer.vTopIndex);
				meshBuffer.isDirtyVerts = true;
			}
			else
			{
				for (int i = 0; i < meshBuffer.vertices.Length; i++)
				{
					this.m_vertices[i + meshBuffer.vTopIndex] = Vector3.zero;
				}
				meshBuffer.isDirtyVerts = true;
			}
			this.m_vertsChanged = true;
		}

		// Token: 0x06004E02 RID: 19970 RVA: 0x0015B687 File Offset: 0x00159A87
		internal void SetNormals(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			meshBuffer.normals.CopyTo(this.m_normals, meshBuffer.vTopIndex);
			meshBuffer.isDirtyNorms = true;
			this.m_normalsChanged = true;
		}

		// Token: 0x06004E03 RID: 19971 RVA: 0x0015B6AE File Offset: 0x00159AAE
		internal void SetUVs(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			meshBuffer.UVs.CopyTo(this.m_UVs, meshBuffer.vTopIndex);
			meshBuffer.isDirtyUVs = true;
			this.m_uvsChanged = true;
		}

		// Token: 0x06004E04 RID: 19972 RVA: 0x0015B6D5 File Offset: 0x00159AD5
		internal void SetColors(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			if (meshBuffer.enabled)
			{
				meshBuffer.colors.CopyTo(this.m_colors, meshBuffer.vTopIndex);
				meshBuffer.isDirtyColors = true;
			}
			this.m_colorsChanged = true;
		}

		// Token: 0x06004E05 RID: 19973 RVA: 0x0015B70C File Offset: 0x00159B0C
		internal void SetIndices(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			for (int i = 0; i < meshBuffer.indices.Length; i++)
			{
				this.m_Indices[i + meshBuffer.iTopIndex] = meshBuffer.indices[i] + meshBuffer.vTopIndex;
			}
			meshBuffer.isDirtyIndices = true;
			this.m_IndicesChanged = true;
		}

		// Token: 0x06004E06 RID: 19974 RVA: 0x0015B75E File Offset: 0x00159B5E
		internal void SetSubUVs(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			if (!this.isUsableSubUVsFix)
			{
				return;
			}
			meshBuffer.SubUVs.CopyTo(this.m_SubUVs, meshBuffer.vTopIndex);
			meshBuffer.isDirtySubUVs = true;
			this.m_subuvsChanged = true;
		}

		// Token: 0x06004E07 RID: 19975 RVA: 0x0015B791 File Offset: 0x00159B91
		internal void SetRepeatUVRegion(EffectMeshBuffer.MeshBuffer meshBuffer)
		{
			if (!this.isUsableUVRegionRepeat)
			{
				return;
			}
			meshBuffer.repeatUVRegion.CopyTo(this.m_RepeatUVRegion.ToArray(), meshBuffer.vTopIndex);
			meshBuffer.isDirtyRepeatUVRegion = true;
			this.m_RepeatUVRegionChanged = true;
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06004E09 RID: 19977 RVA: 0x0015B7D2 File Offset: 0x00159BD2
		// (set) Token: 0x06004E08 RID: 19976 RVA: 0x0015B7C9 File Offset: 0x00159BC9
		public bool RenderFlg
		{
			get
			{
				return base.enabled;
			}
			set
			{
				base.enabled = value;
			}
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06004E0B RID: 19979 RVA: 0x0015B7E3 File Offset: 0x00159BE3
		// (set) Token: 0x06004E0A RID: 19978 RVA: 0x0015B7DA File Offset: 0x00159BDA
		public bool bufferChangeFlg { get; set; }

		// Token: 0x04004E06 RID: 19974
		protected MeshTopology[] m_MeshTopologyTable;

		// Token: 0x04004E07 RID: 19975
		protected MeshFilter m_MeshFilter;

		// Token: 0x04004E08 RID: 19976
		protected Mesh m_Mesh;

		// Token: 0x04004E09 RID: 19977
		public EffectMeshBuffer.Topology m_MeshTopology;

		// Token: 0x04004E0A RID: 19978
		protected EffectMeshBuffer.Topology m_MeshTopologyWork;

		// Token: 0x04004E0B RID: 19979
		public int m_maxPrimitiveCount;

		// Token: 0x04004E0C RID: 19980
		protected int m_maxPrimitiveCountWork;

		// Token: 0x04004E0D RID: 19981
		protected int m_vTopIndex;

		// Token: 0x04004E0E RID: 19982
		protected int m_iTopIndex;

		// Token: 0x04004E0F RID: 19983
		public bool m_isUsableSubUVs;

		// Token: 0x04004E10 RID: 19984
		public bool m_isUsableNormals;

		// Token: 0x04004E11 RID: 19985
		public bool m_isUsableUVRegionRepeat;

		// Token: 0x04004E12 RID: 19986
		protected Vector3[] m_vertices;

		// Token: 0x04004E13 RID: 19987
		protected Vector3[] m_normals;

		// Token: 0x04004E14 RID: 19988
		protected Vector2[] m_UVs;

		// Token: 0x04004E15 RID: 19989
		protected Vector2[] m_SubUVs;

		// Token: 0x04004E16 RID: 19990
		protected Color[] m_colors;

		// Token: 0x04004E17 RID: 19991
		protected int[] m_Indices;

		// Token: 0x04004E18 RID: 19992
		protected List<Vector4> m_RepeatUVRegion;

		// Token: 0x04004E19 RID: 19993
		protected Vector3[] m_verticesWork;

		// Token: 0x04004E1A RID: 19994
		protected Vector3[] m_normalsWork;

		// Token: 0x04004E1B RID: 19995
		protected Vector2[] m_UVsWork;

		// Token: 0x04004E1C RID: 19996
		protected Vector2[] m_SubUVsWork;

		// Token: 0x04004E1D RID: 19997
		protected Color[] m_colorsWork;

		// Token: 0x04004E1E RID: 19998
		protected int[] m_IndicesWork;

		// Token: 0x04004E1F RID: 19999
		protected List<Vector4> m_RepeatUVRegionWork;

		// Token: 0x04004E27 RID: 20007
		[SerializeField]
		protected int m_primitiveCount;

		// Token: 0x04004E28 RID: 20008
		private EffectMeshBuffer.HookUpdate m_Hook_Update;

		// Token: 0x04004E29 RID: 20009
		private EffectMeshBuffer.HookUpdate m_Hook_OnWillRender;

		// Token: 0x04004E2A RID: 20010
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x04004E2B RID: 20011
		protected List<EffectMeshBuffer.MeshBuffer> m_MeshBufferList;

		// Token: 0x04004E2C RID: 20012
		protected List<EffectMeshBuffer.MeshBuffer> m_ActiveMeshBufferList;

		// Token: 0x04004E2F RID: 20015
		protected bool bInit;

		// Token: 0x02000E9C RID: 3740
		public enum Topology
		{
			// Token: 0x04004E31 RID: 20017
			Triangles,
			// Token: 0x04004E32 RID: 20018
			Quads,
			// Token: 0x04004E33 RID: 20019
			Lines,
			// Token: 0x04004E34 RID: 20020
			Points,
			// Token: 0x04004E35 RID: 20021
			Max
		}

		// Token: 0x02000E9D RID: 3741
		// (Invoke) Token: 0x06004E0D RID: 19981
		public delegate void HookUpdate();

		// Token: 0x02000E9E RID: 3742
		public class MeshBuffer
		{
			// Token: 0x06004E11 RID: 19985 RVA: 0x0015B7F3 File Offset: 0x00159BF3
			public void Remove()
			{
				this.meshBuffer.RemoveBuffer(this);
			}

			// Token: 0x06004E12 RID: 19986 RVA: 0x0015B801 File Offset: 0x00159C01
			public void UpdateVertices()
			{
				this.meshBuffer.SetVertices(this);
			}

			// Token: 0x06004E13 RID: 19987 RVA: 0x0015B80F File Offset: 0x00159C0F
			public void UpdateNormals()
			{
				this.meshBuffer.SetNormals(this);
			}

			// Token: 0x06004E14 RID: 19988 RVA: 0x0015B81D File Offset: 0x00159C1D
			public void UpdateUVs()
			{
				this.meshBuffer.SetUVs(this);
			}

			// Token: 0x06004E15 RID: 19989 RVA: 0x0015B82B File Offset: 0x00159C2B
			public void UpdateSubUVs()
			{
				this.meshBuffer.SetSubUVs(this);
			}

			// Token: 0x06004E16 RID: 19990 RVA: 0x0015B839 File Offset: 0x00159C39
			public void UpdateColors()
			{
				this.meshBuffer.SetColors(this);
			}

			// Token: 0x06004E17 RID: 19991 RVA: 0x0015B847 File Offset: 0x00159C47
			public void UpdateIndices()
			{
				this.meshBuffer.SetIndices(this);
			}

			// Token: 0x06004E18 RID: 19992 RVA: 0x0015B855 File Offset: 0x00159C55
			public void UpdateRepeatUVRegion()
			{
				this.meshBuffer.SetRepeatUVRegion(this);
			}

			// Token: 0x06004E19 RID: 19993 RVA: 0x0015B864 File Offset: 0x00159C64
			public void Minimize()
			{
				for (int i = 0; i < this.vertices.Length; i++)
				{
					this.vertices[i] = Vector3.zero;
				}
				this.UpdateVertices();
			}

			// Token: 0x17000530 RID: 1328
			// (get) Token: 0x06004E1A RID: 19994 RVA: 0x0015B8A6 File Offset: 0x00159CA6
			// (set) Token: 0x06004E1B RID: 19995 RVA: 0x0015B8B0 File Offset: 0x00159CB0
			public bool enabled
			{
				get
				{
					return this.m_enabled;
				}
				set
				{
					if (this.m_enabled == value)
					{
						return;
					}
					this.m_enabled = value;
					if (this.m_enabled)
					{
						this.meshBuffer.RenderFlg = true;
						this.meshBuffer.AddActiveBufferList(this);
					}
					else
					{
						this.meshBuffer.RemoveActiveBufferList(this);
					}
					this.meshBuffer.m_maxTriangleBufferChanged = true;
					this.UpdateVertices();
					this.UpdateColors();
				}
			}

			// Token: 0x04004E36 RID: 20022
			internal EffectMeshBuffer meshBuffer;

			// Token: 0x04004E37 RID: 20023
			internal int vTopIndex;

			// Token: 0x04004E38 RID: 20024
			internal int iTopIndex;

			// Token: 0x04004E39 RID: 20025
			public Vector3[] vertices;

			// Token: 0x04004E3A RID: 20026
			public Vector3[] normals;

			// Token: 0x04004E3B RID: 20027
			public Vector2[] UVs;

			// Token: 0x04004E3C RID: 20028
			public Vector2[] SubUVs;

			// Token: 0x04004E3D RID: 20029
			public Color[] colors;

			// Token: 0x04004E3E RID: 20030
			public int[] indices;

			// Token: 0x04004E3F RID: 20031
			public List<Vector4> repeatUVRegion;

			// Token: 0x04004E40 RID: 20032
			internal bool isDirtyVerts;

			// Token: 0x04004E41 RID: 20033
			internal bool isDirtyNorms;

			// Token: 0x04004E42 RID: 20034
			internal bool isDirtyUVs;

			// Token: 0x04004E43 RID: 20035
			internal bool isDirtySubUVs;

			// Token: 0x04004E44 RID: 20036
			internal bool isDirtyColors;

			// Token: 0x04004E45 RID: 20037
			internal bool isDirtyIndices;

			// Token: 0x04004E46 RID: 20038
			internal bool isDirtyRepeatUVRegion;

			// Token: 0x04004E47 RID: 20039
			private bool m_enabled;
		}
	}
}
