﻿using System;

namespace Meige
{
	// Token: 0x02000F99 RID: 3993
	public enum eADLinkType
	{
		// Token: 0x040053E6 RID: 21478
		eADLinkType_Standard,
		// Token: 0x040053E7 RID: 21479
		eADLinkType_Spring,
		// Token: 0x040053E8 RID: 21480
		eADLinkType_Max
	}
}
