﻿using System;

namespace Meige
{
	// Token: 0x02000F8A RID: 3978
	public enum eLayerBlendMode
	{
		// Token: 0x04005338 RID: 21304
		eLayerBlendMode_Invalid = -1,
		// Token: 0x04005339 RID: 21305
		eLayerBlendMode_Default,
		// Token: 0x0400533A RID: 21306
		eLayerBlendMode_Std,
		// Token: 0x0400533B RID: 21307
		eLayerBlendMode_Add,
		// Token: 0x0400533C RID: 21308
		eLayerBlendMode_Sub,
		// Token: 0x0400533D RID: 21309
		eLayerBlendMode_Mul,
		// Token: 0x0400533E RID: 21310
		eLayerBlendMode_SrcOne,
		// Token: 0x0400533F RID: 21311
		eLayerBlendMode_DstOne,
		// Token: 0x04005340 RID: 21312
		eLayerBlendMode_Max
	}
}
