﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Meige
{
	// Token: 0x02000FE5 RID: 4069
	public class NetworkQueueManager : SingletonMonoBehaviour<NetworkQueueManager>
	{
		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x060054C9 RID: 21705 RVA: 0x0017C65B File Offset: 0x0017AA5B
		public static bool IsLogin
		{
			get
			{
				return !string.IsNullOrEmpty(MeigeWWW.SessionId);
			}
		}

		// Token: 0x060054CA RID: 21706 RVA: 0x0017C66A File Offset: 0x0017AA6A
		public static void Request(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<NetworkQueueManager>.Instance.RequestParam(wwwParam);
		}

		// Token: 0x060054CB RID: 21707 RVA: 0x0017C677 File Offset: 0x0017AA77
		public static void Done(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<NetworkQueueManager>.Instance.DoneJob(wwwParam);
		}

		// Token: 0x060054CC RID: 21708 RVA: 0x0017C684 File Offset: 0x0017AA84
		public static void SetSessionId(string sessionId)
		{
			MeigeWWW.SessionId = sessionId;
		}

		// Token: 0x060054CD RID: 21709 RVA: 0x0017C68C File Offset: 0x0017AA8C
		public static string GetSessionId()
		{
			return MeigeWWW.SessionId;
		}

		// Token: 0x060054CE RID: 21710 RVA: 0x0017C693 File Offset: 0x0017AA93
		public static void SetAppVersion(string version)
		{
			MeigeWWW.AppVersion = version;
		}

		// Token: 0x060054CF RID: 21711 RVA: 0x0017C69B File Offset: 0x0017AA9B
		public static void SetTimeoutSec(int timeoutSec)
		{
			MeigeWWW.TimeoutSec = timeoutSec;
		}

		// Token: 0x060054D0 RID: 21712 RVA: 0x0017C6A3 File Offset: 0x0017AAA3
		public static void SetRelease(bool release)
		{
			MeigeWWW.IsRelease = release;
		}

		// Token: 0x060054D1 RID: 21713 RVA: 0x0017C6AB File Offset: 0x0017AAAB
		public static int TotalJobCounter()
		{
			return SingletonMonoBehaviour<NetworkQueueManager>.Instance.totalJobCount;
		}

		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x060054D2 RID: 21714 RVA: 0x0017C6B7 File Offset: 0x0017AAB7
		public int totalJobCount
		{
			get
			{
				return (from p in this.m_wwwParams
				where p.GetStatus() == MeigewwwParam.eStatus.Progress
				select p).Count<MeigewwwParam>();
			}
		}

		// Token: 0x060054D3 RID: 21715 RVA: 0x0017C6E6 File Offset: 0x0017AAE6
		protected new void Awake()
		{
			this.m_wwwParams = new List<MeigewwwParam>();
			this.m_barrierJobFlagMask = 0;
			this.m_doingJobCounter = new int[16];
			MeigeWWW.SetUp();
		}

		// Token: 0x060054D4 RID: 21716 RVA: 0x0017C70C File Offset: 0x0017AB0C
		public void ClearRequest()
		{
			this.m_wwwParams.Clear();
			this.m_barrierJobFlagMask = 0;
			for (int i = 0; i < 16; i++)
			{
				this.m_doingJobCounter[i] = 0;
			}
		}

		// Token: 0x060054D5 RID: 21717 RVA: 0x0017C748 File Offset: 0x0017AB48
		public void Update()
		{
			if (this.m_wwwParams == null)
			{
				return;
			}
			for (int i = this.m_wwwParams.Count - 1; i >= 0; i--)
			{
				if (this.m_wwwParams[i].IsDone())
				{
					this.m_wwwParams.RemoveAt(i);
				}
			}
			if (this.totalJobCount >= 10)
			{
				return;
			}
			MeigewwwParam meigewwwParam = null;
			int maskFlag;
			foreach (MeigewwwParam meigewwwParam2 in this.m_wwwParams)
			{
				if (meigewwwParam2.GetStatus() != MeigewwwParam.eStatus.Progress)
				{
					maskFlag = meigewwwParam2.GetMaskFlag();
					if (maskFlag == 0 || (this.m_barrierJobFlagMask & maskFlag) == 0)
					{
						meigewwwParam = meigewwwParam2;
						break;
					}
				}
			}
			if (meigewwwParam == null)
			{
				return;
			}
			maskFlag = meigewwwParam.GetMaskFlag();
			this.m_barrierJobFlagMask |= maskFlag;
			for (int j = 0; j < 16; j++)
			{
				if ((maskFlag & 1 << j) != 0)
				{
					this.m_doingJobCounter[j]++;
				}
			}
			meigewwwParam.SetStatus(MeigewwwParam.eStatus.Progress);
			MeigeWWW meigeWWW = new MeigeWWW(meigewwwParam);
			base.StartCoroutine(meigeWWW.Kick());
		}

		// Token: 0x060054D6 RID: 21718 RVA: 0x0017C8A4 File Offset: 0x0017ACA4
		private void DoneJob(MeigewwwParam wwwRequest)
		{
			wwwRequest.SetStatus(MeigewwwParam.eStatus.Done);
			int maskFlag = wwwRequest.GetMaskFlag();
			if (maskFlag != 0)
			{
				for (int i = 0; i < 16; i++)
				{
					if ((maskFlag & 1 << i) != 0)
					{
						this.m_doingJobCounter[i]--;
						if (this.m_doingJobCounter[i] <= 0 && (this.m_barrierJobFlagMask & 1 << i) != 0)
						{
							this.m_barrierJobFlagMask &= ~(1 << i);
						}
					}
				}
			}
			if (this.m_RecvSystemCallback != null)
			{
				this.m_RecvSystemCallback(wwwRequest);
			}
		}

		// Token: 0x060054D7 RID: 21719 RVA: 0x0017C941 File Offset: 0x0017AD41
		private void RequestParam(MeigewwwParam wwwParam)
		{
			wwwParam.SetStatus(MeigewwwParam.eStatus.Wait);
			this.m_wwwParams.Add(wwwParam);
		}

		// Token: 0x060054D8 RID: 21720 RVA: 0x0017C958 File Offset: 0x0017AD58
		public static string escapeSpecialChars(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return text;
			}
			return text.Replace("\\", "\\\\").Replace("\r\n", "\\n").Replace("\r", "\\n").Replace("\n", "\\n").Replace("\"", "\\\"").Replace("/", "\\/").Replace("\b", "\\b").Replace("\f", "\\f").Replace("\t", "\\t");
		}

		// Token: 0x060054D9 RID: 21721 RVA: 0x0017C9FC File Offset: 0x0017ADFC
		public static void SetSystemRecvCallback(Action<MeigewwwParam> callbacks)
		{
			SingletonMonoBehaviour<NetworkQueueManager>.Instance.m_RecvSystemCallback = callbacks;
		}

		// Token: 0x040055CB RID: 21963
		private List<MeigewwwParam> m_wwwParams;

		// Token: 0x040055CC RID: 21964
		private int m_barrierJobFlagMask;

		// Token: 0x040055CD RID: 21965
		private int[] m_doingJobCounter;

		// Token: 0x040055CE RID: 21966
		private Action<MeigewwwParam> m_RecvSystemCallback;
	}
}
