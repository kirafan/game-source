﻿using System;

namespace Meige
{
	// Token: 0x02000F98 RID: 3992
	public enum eCameraProjection
	{
		// Token: 0x040053E2 RID: 21474
		eCameraProjectionPerspective,
		// Token: 0x040053E3 RID: 21475
		eCameraProjectionOrthographic,
		// Token: 0x040053E4 RID: 21476
		eCameraProjectionMAX
	}
}
