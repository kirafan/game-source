﻿using System;

namespace Meige
{
	// Token: 0x02000F96 RID: 3990
	public enum eAnimControlType
	{
		// Token: 0x040053C4 RID: 21444
		eAnimControl_Invalid = -1,
		// Token: 0x040053C5 RID: 21445
		eAnimControl_Linear,
		// Token: 0x040053C6 RID: 21446
		eAnimControl_Bool,
		// Token: 0x040053C7 RID: 21447
		eAnimControl_Constant,
		// Token: 0x040053C8 RID: 21448
		eAnimControl_Cubic
	}
}
