﻿using System;

namespace Meige
{
	// Token: 0x02000F83 RID: 3971
	public enum eParticleEmitionType : byte
	{
		// Token: 0x04005302 RID: 21250
		eParticleEmitionType_Point,
		// Token: 0x04005303 RID: 21251
		eParticleEmitionType_Box,
		// Token: 0x04005304 RID: 21252
		eParticleEmitionType_PlaneQuad,
		// Token: 0x04005305 RID: 21253
		eParticleEmitionType_PlaneCircle,
		// Token: 0x04005306 RID: 21254
		eParticleEmitionType_Sphere,
		// Token: 0x04005307 RID: 21255
		eParticleEmitionType_Torus,
		// Token: 0x04005308 RID: 21256
		eParticleEmitionType_MeshVertex,
		// Token: 0x04005309 RID: 21257
		eParticleEmitionType_Cylinder
	}
}
