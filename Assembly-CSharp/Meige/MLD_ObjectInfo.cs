﻿using System;

namespace Meige
{
	// Token: 0x02000F9D RID: 3997
	public struct MLD_ObjectInfo
	{
		// Token: 0x0400540C RID: 21516
		public ushort m_ID;

		// Token: 0x0400540D RID: 21517
		public ushort m_Size;

		// Token: 0x0400540E RID: 21518
		public int m_DataOffs;
	}
}
