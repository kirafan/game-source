﻿using System;

namespace Meige
{
	// Token: 0x02000FD2 RID: 4050
	public enum eGaussFilterType
	{
		// Token: 0x04005546 RID: 21830
		eGaussFilterType_None,
		// Token: 0x04005547 RID: 21831
		eGaussFilterType_x5,
		// Token: 0x04005548 RID: 21832
		eGaussFilterType_x7
	}
}
