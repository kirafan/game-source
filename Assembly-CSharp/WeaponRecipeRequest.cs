﻿using System;
using Meige;
using WeaponRecipeRequestTypes;

// Token: 0x02000BF4 RID: 3060
public static class WeaponRecipeRequest
{
	// Token: 0x06003F4D RID: 16205 RVA: 0x0013C260 File Offset: 0x0013A660
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon_recipe/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003F4E RID: 16206 RVA: 0x0013C284 File Offset: 0x0013A684
	public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon_recipe/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003F4F RID: 16207 RVA: 0x0013C2A8 File Offset: 0x0013A6A8
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon_recipe/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("weaponId", param.weaponId);
		return meigewwwParam;
	}

	// Token: 0x06003F50 RID: 16208 RVA: 0x0013C2DC File Offset: 0x0013A6DC
	public static MeigewwwParam Get(string weaponId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon_recipe/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("weaponId", weaponId);
		return meigewwwParam;
	}
}
