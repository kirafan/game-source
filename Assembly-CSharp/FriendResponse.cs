﻿using System;
using System.Collections.Generic;
using FriendResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFA RID: 3066
public static class FriendResponse
{
	// Token: 0x06003F65 RID: 16229 RVA: 0x0013C538 File Offset: 0x0013A938
	public static Propose Propose(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Propose>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F66 RID: 16230 RVA: 0x0013C550 File Offset: 0x0013A950
	public static Accept Accept(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Accept>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F67 RID: 16231 RVA: 0x0013C568 File Offset: 0x0013A968
	public static Refuse Refuse(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Refuse>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F68 RID: 16232 RVA: 0x0013C580 File Offset: 0x0013A980
	public static Cancel Cancel(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Cancel>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F69 RID: 16233 RVA: 0x0013C598 File Offset: 0x0013A998
	public static Terminate Terminate(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Terminate>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F6A RID: 16234 RVA: 0x0013C5B0 File Offset: 0x0013A9B0
	public static Search Search(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Search>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F6B RID: 16235 RVA: 0x0013C5C8 File Offset: 0x0013A9C8
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}
}
