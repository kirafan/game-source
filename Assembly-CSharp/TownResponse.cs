﻿using System;
using System.Collections.Generic;
using Meige;
using TownResponseTypes;
using WWWTypes;

// Token: 0x02000C0F RID: 3087
public static class TownResponse
{
	// Token: 0x06003FD4 RID: 16340 RVA: 0x0013D288 File Offset: 0x0013B688
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD5 RID: 16341 RVA: 0x0013D2A0 File Offset: 0x0013B6A0
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD6 RID: 16342 RVA: 0x0013D2B8 File Offset: 0x0013B6B8
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Remove>(param, dialogType, acceptableResultCodes);
	}
}
