﻿using System;
using Meige;
using TUTORIALRequestTypes;

// Token: 0x02000B6F RID: 2927
public static class TUTORIALRequest
{
	// Token: 0x06003EA5 RID: 16037 RVA: 0x0013B368 File Offset: 0x00139768
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/step/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003EA6 RID: 16038 RVA: 0x0013B3A0 File Offset: 0x001397A0
	public static MeigewwwParam Add(int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/step/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003EA7 RID: 16039 RVA: 0x0013B3D4 File Offset: 0x001397D4
	public static MeigewwwParam SetParty(SetParty param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/party/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003EA8 RID: 16040 RVA: 0x0013B40C File Offset: 0x0013980C
	public static MeigewwwParam SetParty(int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/party/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}
}
