﻿using System;
using UnityEngine;

// Token: 0x02000E77 RID: 3703
public class ScriptDataMap : ScriptableObject
{
	// Token: 0x06004C8B RID: 19595 RVA: 0x0015536C File Offset: 0x0015376C
	public ScriptDataMap_Param GetParam(int ID)
	{
		foreach (ScriptDataMap_Param result in this.m_Params)
		{
			foreach (int num in result.IDs)
			{
				if (num == ID)
				{
					return result;
				}
			}
		}
		Debug.LogError("Script ID[" + ID + "] not found.");
		return default(ScriptDataMap_Param);
	}

	// Token: 0x06004C8C RID: 19596 RVA: 0x001553F8 File Offset: 0x001537F8
	public ScriptDataMap_Param GetParam(string part)
	{
		foreach (ScriptDataMap_Param result in this.m_Params)
		{
			if (result.part == part)
			{
				return result;
			}
		}
		Debug.LogError("Script Part[" + part + "] not found.");
		return default(ScriptDataMap_Param);
	}

	// Token: 0x04004D8B RID: 19851
	public ScriptDataMap_Param[] m_Params;
}
