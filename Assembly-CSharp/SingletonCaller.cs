﻿using System;
using UnityEngine;

// Token: 0x02000FB0 RID: 4016
public class SingletonCaller : MonoBehaviour
{
	// Token: 0x06005373 RID: 21363 RVA: 0x00175C24 File Offset: 0x00174024
	private void Awake()
	{
		if (SingletonCaller.m_Instance == null)
		{
			GameScreen.Create();
			InputTouch.Create();
			Frame.Create();
			SingletonCaller.m_Instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Helper.DestroyAll(base.gameObject);
		}
	}

	// Token: 0x06005374 RID: 21364 RVA: 0x00175C71 File Offset: 0x00174071
	private void Start()
	{
	}

	// Token: 0x06005375 RID: 21365 RVA: 0x00175C73 File Offset: 0x00174073
	private void Update()
	{
		Frame.Instance.Update();
		InputTouch.Instance.Update();
	}

	// Token: 0x06005376 RID: 21366 RVA: 0x00175C89 File Offset: 0x00174089
	private void LateUpdate()
	{
		Frame.Instance.LateUpdate();
	}

	// Token: 0x0400545F RID: 21599
	private static SingletonCaller m_Instance;
}
