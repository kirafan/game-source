﻿using System;

// Token: 0x0200006B RID: 107
public enum eADVShakeType
{
	// Token: 0x040001AA RID: 426
	RandomS,
	// Token: 0x040001AB RID: 427
	RandomM,
	// Token: 0x040001AC RID: 428
	RandomL,
	// Token: 0x040001AD RID: 429
	HorizontalS,
	// Token: 0x040001AE RID: 430
	HorizontalM,
	// Token: 0x040001AF RID: 431
	HorizontalL,
	// Token: 0x040001B0 RID: 432
	VerticalS,
	// Token: 0x040001B1 RID: 433
	VerticalM,
	// Token: 0x040001B2 RID: 434
	VerticalL
}
