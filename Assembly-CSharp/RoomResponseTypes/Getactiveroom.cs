﻿using System;
using CommonResponseTypes;

namespace RoomResponseTypes
{
	// Token: 0x02000C71 RID: 3185
	public class Getactiveroom : CommonResponse
	{
		// Token: 0x0600409B RID: 16539 RVA: 0x0013F594 File Offset: 0x0013D994
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.managedRoomId.ToString();
		}

		// Token: 0x04004673 RID: 18035
		public long managedRoomId;
	}
}
