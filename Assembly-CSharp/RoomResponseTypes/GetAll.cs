﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomResponseTypes
{
	// Token: 0x02000C6E RID: 3182
	public class GetAll : CommonResponse
	{
		// Token: 0x06004095 RID: 16533 RVA: 0x0013F510 File Offset: 0x0013D910
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedRooms == null) ? string.Empty : this.managedRooms.ToString());
		}

		// Token: 0x04004672 RID: 18034
		public PlayerRoom[] managedRooms;
	}
}
