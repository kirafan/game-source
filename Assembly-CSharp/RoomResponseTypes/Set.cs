﻿using System;
using CommonResponseTypes;

namespace RoomResponseTypes
{
	// Token: 0x02000C6D RID: 3181
	public class Set : CommonResponse
	{
		// Token: 0x06004093 RID: 16531 RVA: 0x0013F4D8 File Offset: 0x0013D8D8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.managedRoomId.ToString();
		}

		// Token: 0x04004671 RID: 18033
		public long managedRoomId;
	}
}
