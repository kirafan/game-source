﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000FC4 RID: 4036
[Serializable]
public class MsbObjectHandler
{
	// Token: 0x1700059C RID: 1436
	// (get) Token: 0x0600541E RID: 21534 RVA: 0x00178AF2 File Offset: 0x00176EF2
	public Transform transform
	{
		get
		{
			return this.m_Transfrom;
		}
	}

	// Token: 0x0600541F RID: 21535 RVA: 0x00178AFC File Offset: 0x00176EFC
	public void Init(MsbHandler owner)
	{
		this.m_Owner = owner;
		this.m_Work = new MsbObjectHandler.MsbObjectParam(this.m_Src);
		this.m_GO = null;
		this.m_Transfrom = null;
		foreach (Renderer renderer in this.m_Owner.GetComponentsInChildren<Renderer>(true))
		{
			string text = Helper.GetMeshNameByRenderer(renderer);
			if (text != null)
			{
				text = text.Replace(" Instance", string.Empty);
				if (text.Equals(this.m_Name))
				{
					this.m_GO = renderer.gameObject;
					this.m_Transfrom = this.m_GO.transform;
					this.m_Renderer = renderer;
					break;
				}
			}
		}
		if (this.m_Renderer != null && this.m_Renderer.materials != null)
		{
			this.m_MatArray = this.m_Renderer.materials;
			this.m_MatIndex = new int[this.m_Renderer.materials.Length];
			for (int j = 0; j < this.m_MatArray.Length; j++)
			{
				Material material = this.m_MatArray[j];
				string value = material.name.Replace(" (Instance)", string.Empty);
				this.m_MatIndex[j] = -1;
				for (int k = 0; k < this.m_Owner.GetMsbMaterialHandlerNum(); k++)
				{
					MsbMaterialHandler msbMaterialHandler = this.m_Owner.GetMsbMaterialHandler(k);
					if (msbMaterialHandler != null && msbMaterialHandler.m_Name.Equals(value))
					{
						this.m_MatIndex[j] = k;
						break;
					}
				}
				if (this.m_Src.m_eRenderStage >= eRenderStage.eRenderStage_PunchThrough_Higher && this.m_Src.m_eRenderStage <= eRenderStage.eRenderStage_PunchThrough_Lower)
				{
					MeigeShaderUtility.SetDepthTest(material, eCompareFunc.Less);
					MeigeShaderUtility.SetEnableDepthWrite(material, true);
					MeigeShaderUtility.SetEnableAlphaTest(material, true);
				}
				else if (this.m_Src.m_eRenderStage >= eRenderStage.eRenderStage_Alpha_Higher && this.m_Src.m_eRenderStage <= eRenderStage.eRenderStage_Alpha_Lower)
				{
					MeigeShaderUtility.SetDepthTest(material, eCompareFunc.Less);
					MeigeShaderUtility.SetEnableDepthWrite(material, false);
					MeigeShaderUtility.SetEnableAlphaTest(material, false);
				}
				else
				{
					MeigeShaderUtility.SetDepthTest(material, eCompareFunc.Less);
					MeigeShaderUtility.SetEnableDepthWrite(material, true);
					MeigeShaderUtility.SetEnableAlphaTest(material, false);
				}
				MeigeShaderUtility.SetOutlineType(material, this.m_Src.m_OutlineType);
				MeigeShaderUtility.SetOutlineColor(material, this.m_Src.m_OutlineColor);
				MeigeShaderUtility.SetOutlineWidth(material, this.m_Src.m_OutlineWidth);
				MeigeShaderUtility.SetOutlineDepthOffset(material, this.m_Src.m_OutlineDepthOffset * this.m_Transfrom.lossyScale.x);
			}
		}
		if (this.m_Work != null)
		{
			this.m_Work.m_History.Init();
		}
	}

	// Token: 0x06005420 RID: 21536 RVA: 0x00178DAC File Offset: 0x001771AC
	public void Destroy()
	{
		if (this.m_MatArray != null)
		{
			foreach (Material material in this.m_MatArray)
			{
				if (material && material.name.Contains("(Instance)"))
				{
					UnityEngine.Object.DestroyImmediate(material);
				}
			}
			this.m_MatArray = null;
		}
	}

	// Token: 0x06005421 RID: 21537 RVA: 0x00178E10 File Offset: 0x00177210
	public void UpdateParam()
	{
		if (this.m_Renderer == null)
		{
			return;
		}
		if (this.m_Renderer.enabled != this.m_Work.m_bVisibility)
		{
			this.m_Renderer.enabled = this.m_Work.m_bVisibility;
		}
		if (this.m_Work.m_bVisibility)
		{
			for (int i = 0; i < this.m_MatArray.Length; i++)
			{
				if (this.m_MatIndex[i] != -1)
				{
					Material material = this.m_MatArray[i];
					MsbMaterialHandler.MsbMaterialParam work = this.m_Owner.m_MsbMaterialHandlerArray[this.m_MatIndex[i]].GetWork();
					material.renderQueue = (int)MeigeUtility.RenderStageToRenderQueue(this.m_Work.m_eRenderStage, this.m_Work.m_RenderOrder);
					Color color = work.m_Diffuse * this.m_Work.m_MeshColor;
					if (this.m_Work.m_bHalfVertexColor)
					{
						color.r *= 2f;
						color.g *= 2f;
						color.b *= 2f;
					}
					if (this.m_Work.m_History.m_MeshColor != color)
					{
						MeigeShaderUtility.SetMeshColor(material, color);
						this.m_Work.m_History.m_MeshColor = color;
					}
					Matrix4x4 matrix = work.m_Texture[0].GetMatrix();
					MeigeShaderUtility.SetUVMatrix(material, matrix, eTextureType.eTextureType_Albedo, 0);
					if (work.m_Texture.Length >= 2)
					{
						Matrix4x4 matrix2 = work.m_Texture[1].GetMatrix();
						MeigeShaderUtility.SetUVMatrix(material, matrix2, eTextureType.eTextureType_Albedo, 1);
						if (this.m_Work.m_History.m_LayerBlendMode != work.m_Texture[1].m_LayerBlendMode || this.m_Work.m_History.m_LayerBlendModeAlpha != work.m_Texture[1].m_LayerBlendModeAlpha)
						{
							MeigeShaderUtility.SetLayerBlendMode(material, work.m_Texture[1].m_LayerBlendMode, work.m_Texture[1].m_LayerBlendModeAlpha, eTextureType.eTextureType_Albedo);
							this.m_Work.m_History.m_LayerBlendMode = work.m_Texture[1].m_LayerBlendMode;
							this.m_Work.m_History.m_LayerBlendModeAlpha = work.m_Texture[1].m_LayerBlendModeAlpha;
						}
					}
					if (this.m_Work.m_History.m_BlendMode != work.m_BlendMode)
					{
						MeigeShaderUtility.SetAlphaBlendMode(material, work.m_BlendMode);
						this.m_Work.m_History.m_BlendMode = work.m_BlendMode;
					}
					if (this.m_Work.m_History.m_HDRFactor != this.m_Work.m_HDRFactor)
					{
						MeigeShaderUtility.SetHDRFactor(material, this.m_Work.m_HDRFactor);
						this.m_Work.m_History.m_HDRFactor = this.m_Work.m_HDRFactor;
					}
					if (this.m_Work.m_History.m_bFogEnable != this.m_Work.m_bFogEnable)
					{
						MeigeShaderUtility.SetUsingFog(material, this.m_Work.m_bFogEnable);
						this.m_Work.m_History.m_bFogEnable = this.m_Work.m_bFogEnable;
					}
					if (this.m_Work.m_History.m_OutlineType != this.m_Work.m_OutlineType)
					{
						MeigeShaderUtility.SetOutlineType(material, this.m_Work.m_OutlineType);
						this.m_Work.m_History.m_OutlineType = this.m_Work.m_OutlineType;
					}
					if (this.m_Work.m_History.m_OutlineColor != this.m_Work.m_OutlineColor)
					{
						MeigeShaderUtility.SetOutlineColor(material, this.m_Work.m_OutlineColor);
						this.m_Work.m_History.m_OutlineColor = this.m_Work.m_OutlineColor;
					}
					if (this.m_Work.m_History.m_OutlineWidth != this.m_Work.m_OutlineWidth)
					{
						MeigeShaderUtility.SetOutlineWidth(material, this.m_Work.m_OutlineWidth);
						this.m_Work.m_History.m_OutlineWidth = this.m_Work.m_OutlineWidth;
					}
					float num = this.m_Work.m_OutlineDepthOffset * this.m_Transfrom.lossyScale.x;
					if (this.m_Work.m_History.m_OutlineDepthOffset != num)
					{
						MeigeShaderUtility.SetOutlineDepthOffset(material, num);
						this.m_Work.m_History.m_OutlineDepthOffset = num;
					}
					if (this.m_Work.m_History.m_AlphaTestRefValue != work.m_AlphaTestRefValue)
					{
						MeigeShaderUtility.SetAlphaTestRefValue(material, work.m_AlphaTestRefValue);
						this.m_Work.m_History.m_AlphaTestRefValue = work.m_AlphaTestRefValue;
					}
				}
			}
			if (this.m_Work.m_eBillboardType != eMeshBillBoardType.eMeshBillBoardType_None)
			{
				Camera main = Camera.main;
				if (main != null)
				{
					switch (this.m_Work.m_eBillboardType)
					{
					case eMeshBillBoardType.eMeshBillBoardType_XYZ:
						this.m_Transfrom.LookAt(main.transform);
						break;
					case eMeshBillBoardType.eMeshBillBoardType_X:
					{
						Vector3 position = main.transform.position;
						position.x = this.m_Transfrom.position.x;
						this.m_Transfrom.LookAt(position);
						break;
					}
					case eMeshBillBoardType.eMeshBillBoardType_Y:
					{
						Vector3 position2 = main.transform.position;
						position2.y = this.m_Transfrom.position.y;
						this.m_Transfrom.LookAt(position2);
						break;
					}
					case eMeshBillBoardType.eMeshBillBoardType_Z:
					{
						Vector3 position3 = main.transform.position;
						position3.z = this.m_Transfrom.position.z;
						this.m_Transfrom.LookAt(position3);
						break;
					}
					}
				}
			}
		}
	}

	// Token: 0x06005422 RID: 21538 RVA: 0x001793B7 File Offset: 0x001777B7
	public MsbObjectHandler.MsbObjectParam GetWork()
	{
		return this.m_Work;
	}

	// Token: 0x06005423 RID: 21539 RVA: 0x001793BF File Offset: 0x001777BF
	public Renderer GetRenderer()
	{
		return this.m_Renderer;
	}

	// Token: 0x040054FC RID: 21756
	public string m_Name;

	// Token: 0x040054FD RID: 21757
	public int m_RefID;

	// Token: 0x040054FE RID: 21758
	public int m_HieIndex;

	// Token: 0x040054FF RID: 21759
	public int m_BoundingReferHieIndex;

	// Token: 0x04005500 RID: 21760
	public MsbObjectHandler.MsbObjectParam m_Src;

	// Token: 0x04005501 RID: 21761
	private MsbObjectHandler.MsbObjectParam m_Work;

	// Token: 0x04005502 RID: 21762
	private MsbHandler m_Owner;

	// Token: 0x04005503 RID: 21763
	private GameObject m_GO;

	// Token: 0x04005504 RID: 21764
	private Transform m_Transfrom;

	// Token: 0x04005505 RID: 21765
	private Renderer m_Renderer;

	// Token: 0x04005506 RID: 21766
	private Material[] m_MatArray;

	// Token: 0x04005507 RID: 21767
	private int[] m_MatIndex;

	// Token: 0x02000FC5 RID: 4037
	[Serializable]
	public class MsbObjectParam
	{
		// Token: 0x06005424 RID: 21540 RVA: 0x001793C7 File Offset: 0x001777C7
		public MsbObjectParam()
		{
		}

		// Token: 0x06005425 RID: 21541 RVA: 0x001793FC File Offset: 0x001777FC
		public MsbObjectParam(MsbObjectHandler.MsbObjectParam src)
		{
			this.m_MeshColor = src.m_MeshColor;
			this.m_AABB = src.m_AABB;
			this.m_eRenderStage = src.m_eRenderStage;
			this.m_RenderOrder = src.m_RenderOrder;
			this.m_eBillboardType = src.m_eBillboardType;
			this.m_bVisibility = src.m_bVisibility;
			this.m_HDRFactor = src.m_HDRFactor;
			this.m_bHalfVertexColor = src.m_bHalfVertexColor;
			this.m_bFogEnable = src.m_bFogEnable;
			this.m_OutlineWidth = src.m_OutlineWidth;
			this.m_OutlineDepthOffset = src.m_OutlineDepthOffset;
			this.m_OutlineColor = src.m_OutlineColor;
			this.m_OutlineType = src.m_OutlineType;
			this.m_History.m_MeshColor = Color.black;
			this.m_History.m_HDRFactor = 0f;
			this.m_History.m_OutlineWidth = -1f;
			this.m_History.m_OutlineDepthOffset = -1f;
			this.m_History.m_OutlineColor = Color.white;
			this.m_History.m_OutlineType = eOutlineType.eOutlineType_Invalid;
			this.m_History.m_bFogEnable = !src.m_bFogEnable;
			this.m_History.m_LayerBlendMode = eLayerBlendMode.eLayerBlendMode_Invalid;
			this.m_History.m_LayerBlendModeAlpha = eLayerBlendMode.eLayerBlendMode_Invalid;
			this.m_History.m_BlendMode = eBlendMode.eBlendMode_Invalid;
			this.m_History.m_AlphaTestRefValue = -1f;
		}

		// Token: 0x04005508 RID: 21768
		public Bounds m_AABB;

		// Token: 0x04005509 RID: 21769
		public Color m_MeshColor;

		// Token: 0x0400550A RID: 21770
		public float m_HDRFactor = 1f;

		// Token: 0x0400550B RID: 21771
		public eRenderStage m_eRenderStage;

		// Token: 0x0400550C RID: 21772
		public int m_RenderOrder;

		// Token: 0x0400550D RID: 21773
		public eMeshBillBoardType m_eBillboardType;

		// Token: 0x0400550E RID: 21774
		public bool m_bVisibility;

		// Token: 0x0400550F RID: 21775
		public bool m_bHalfVertexColor;

		// Token: 0x04005510 RID: 21776
		public bool m_bFogEnable;

		// Token: 0x04005511 RID: 21777
		public float m_OutlineWidth = 0.2f;

		// Token: 0x04005512 RID: 21778
		public float m_OutlineDepthOffset = 0.01f;

		// Token: 0x04005513 RID: 21779
		public Color m_OutlineColor = Color.black;

		// Token: 0x04005514 RID: 21780
		public eOutlineType m_OutlineType;

		// Token: 0x04005515 RID: 21781
		public MsbObjectHandler.MsbObjectParam.History m_History;

		// Token: 0x02000FC6 RID: 4038
		public struct History
		{
			// Token: 0x06005426 RID: 21542 RVA: 0x0017957C File Offset: 0x0017797C
			public void Init()
			{
				this.m_MeshColor = Color.white * -1f;
				this.m_HDRFactor = 1f;
				this.m_OutlineWidth = -1f;
				this.m_OutlineDepthOffset = -1f;
				this.m_OutlineColor = Color.white * -1f;
				this.m_OutlineType = eOutlineType.eOutlineType_Invalid;
				this.m_bFogEnable = false;
				this.m_LayerBlendMode = eLayerBlendMode.eLayerBlendMode_Invalid;
				this.m_LayerBlendModeAlpha = eLayerBlendMode.eLayerBlendMode_Invalid;
				this.m_BlendMode = eBlendMode.eBlendMode_Invalid;
				this.m_AlphaTestRefValue = 0f;
			}

			// Token: 0x04005516 RID: 21782
			public Color m_MeshColor;

			// Token: 0x04005517 RID: 21783
			public float m_HDRFactor;

			// Token: 0x04005518 RID: 21784
			public float m_OutlineWidth;

			// Token: 0x04005519 RID: 21785
			public float m_OutlineDepthOffset;

			// Token: 0x0400551A RID: 21786
			public Color m_OutlineColor;

			// Token: 0x0400551B RID: 21787
			public eOutlineType m_OutlineType;

			// Token: 0x0400551C RID: 21788
			public bool m_bFogEnable;

			// Token: 0x0400551D RID: 21789
			public eLayerBlendMode m_LayerBlendMode;

			// Token: 0x0400551E RID: 21790
			public eLayerBlendMode m_LayerBlendModeAlpha;

			// Token: 0x0400551F RID: 21791
			public eBlendMode m_BlendMode;

			// Token: 0x04005520 RID: 21792
			public float m_AlphaTestRefValue;
		}
	}
}
