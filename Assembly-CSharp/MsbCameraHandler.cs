﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000FBB RID: 4027
[Serializable]
public class MsbCameraHandler
{
	// Token: 0x060053E3 RID: 21475 RVA: 0x0017772C File Offset: 0x00175B2C
	private void AttachPostProcessRenderer(Camera cam)
	{
		if (this.m_Src == null)
		{
			return;
		}
		bool flag = this.m_Src.m_bEnableCorrectToneMap || this.m_Src.m_bEnableCorrectContrast || this.m_Src.m_bEnableCorrectBrightness || this.m_Src.m_bEnableCorrectChroma || this.m_Src.m_bEnableCorrectColorBlend || this.m_Src.m_bEnableBloom;
		if (!flag)
		{
			return;
		}
		if (this.m_PostProcessAttachedCamera != cam)
		{
			this.m_PostProcessAttachedCamera = cam;
			if (this.m_PostProcessRenderer != null)
			{
				this.m_PostProcessRenderer.enabled = false;
			}
			if (this.m_PostProcessAttachedCamera == null)
			{
				return;
			}
			this.m_PostProcessRenderer = this.m_PostProcessAttachedCamera.gameObject.GetComponent<PostProcessRenderer>();
			if (this.m_PostProcessRenderer == null)
			{
				this.m_PostProcessRenderer = this.m_PostProcessAttachedCamera.gameObject.AddComponent<PostProcessRenderer>();
			}
			this.m_PostProcessAttachedCamera.hdr = flag;
		}
	}

	// Token: 0x060053E4 RID: 21476 RVA: 0x0017783C File Offset: 0x00175C3C
	public void UpdatePostProcessParameter(bool enabled)
	{
		if (this.m_PostProcessRenderer != null && this.m_Camera != null)
		{
			this.m_PostProcessRenderer.enableCorrectToneCurve = this.m_Work.m_bEnableCorrectToneMap;
			this.m_PostProcessRenderer.enableCorrectContrast = this.m_Work.m_bEnableCorrectContrast;
			this.m_PostProcessRenderer.enableCorrectBrightness = this.m_Work.m_bEnableCorrectBrightness;
			this.m_PostProcessRenderer.enableCorrectChroma = this.m_Work.m_bEnableCorrectChroma;
			this.m_PostProcessRenderer.enableCorrectColorBlend = this.m_Work.m_bEnableCorrectColorBlend;
			this.m_PostProcessRenderer.enableBloom = this.m_Work.m_bEnableBloom;
			this.m_PostProcessRenderer.toneMapWhitePoint = eToneMapWhitePoint.eToneMapWhitePoint_Fix;
			this.m_PostProcessRenderer.toneMapWhitePointScale = this.m_Work.m_ToneMapWhitePointScale;
			this.m_PostProcessRenderer.toneMapWhitePointRange.SetValue(this.m_Work.m_ToneMapWhitePointRangeMin, this.m_Work.m_ToneMapWhitePointRangeMax);
			this.m_PostProcessRenderer.exposureCorrectionValue = this.m_Work.m_ExposureValue;
			this.m_PostProcessRenderer.adaptationRateSpeed = this.m_Work.m_AdaptationRateSpeed;
			this.m_PostProcessRenderer.bloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;
			this.m_PostProcessRenderer.bloomBrightThresholdOffset = this.m_Work.m_BloomReferenceThresholdOffset;
			this.m_PostProcessRenderer.bloomOffset = this.m_Work.m_BloomOffset * (float)this.m_Camera.pixelWidth / 960f;
			this.m_PostProcessRenderer.correctContrastLevel = this.m_Work.m_ContrastLevel;
			this.m_PostProcessRenderer.correctBrightnessLevel = this.m_Work.m_BrightnessLevel;
			this.m_PostProcessRenderer.correctChromaLevel = this.m_Work.m_ChromaLevel;
			this.m_PostProcessRenderer.correctBlendLevel = this.m_Work.m_ColorBlendLevel;
			this.m_PostProcessRenderer.correctBlendColor = this.m_Work.m_ColorBlendColor;
			bool flag = this.m_Work.m_bEnableCorrectToneMap || this.m_Work.m_bEnableCorrectContrast || this.m_Work.m_bEnableCorrectBrightness || this.m_Work.m_bEnableCorrectChroma || this.m_Work.m_bEnableCorrectColorBlend || this.m_Work.m_bEnableBloom;
			this.m_PostProcessRenderer.enabled = (flag && enabled && this.m_PostProcessEnabled);
		}
	}

	// Token: 0x060053E5 RID: 21477 RVA: 0x00177AA8 File Offset: 0x00175EA8
	public void SetCamera(Camera cam)
	{
		this.m_Camera = cam;
		if (this.m_Camera != null)
		{
			this.m_CameraTransform = this.m_Camera.transform;
		}
		else
		{
			this.m_CameraTransform = null;
		}
		this.AttachPostProcessRenderer(this.m_Camera);
	}

	// Token: 0x060053E6 RID: 21478 RVA: 0x00177AF6 File Offset: 0x00175EF6
	public MsbCameraHandler.MsbCameraParam GetWork()
	{
		return this.m_Work;
	}

	// Token: 0x060053E7 RID: 21479 RVA: 0x00177B00 File Offset: 0x00175F00
	public void Init(MsbHandler owner)
	{
		this.m_Owner = owner;
		this.m_Camera = null;
		this.m_HierarchyGO = null;
		this.m_Work = null;
		if (this.m_HierarchyName == null)
		{
			return;
		}
		foreach (Transform transform in this.m_Owner.GetComponentsInChildren<Transform>())
		{
			if (transform.gameObject.name.Equals(this.m_HierarchyName))
			{
				this.m_HierarchyGO = transform.gameObject;
				break;
			}
		}
		this.m_Work = new MsbCameraHandler.MsbCameraParam(this.m_Src);
	}

	// Token: 0x060053E8 RID: 21480 RVA: 0x00177B98 File Offset: 0x00175F98
	public void UpdateParam()
	{
		if (this.m_HierarchyGO == null)
		{
			return;
		}
		if (this.m_Camera == null)
		{
			return;
		}
		this.UpdatePostProcessParameter(true);
		this.m_Camera.orthographic = (this.m_ProjectionType == eCameraProjection.eCameraProjectionOrthographic);
		this.m_CameraTransform.position = this.m_HierarchyGO.transform.position;
		if (this.m_Camera.orthographic)
		{
			Vector3 euler;
			euler.x = 0f;
			euler.y = 180f;
			euler.z = 0f;
			this.m_CameraTransform.rotation = Quaternion.Euler(euler);
			this.m_Camera.orthographicSize = this.m_Work.m_OrthographicsSize / 354f;
		}
		else
		{
			Quaternion rhs = Quaternion.AngleAxis(-90f, Vector3.up);
			this.m_CameraTransform.rotation = this.m_HierarchyGO.transform.rotation * rhs;
			this.m_Camera.fieldOfView = this.CalcHorizontalFov(this.m_Work.m_FocalLength, this.m_Work.m_ApartureWidth);
		}
	}

	// Token: 0x060053E9 RID: 21481 RVA: 0x00177CBD File Offset: 0x001760BD
	private float CalcVerticalFov(float focalLength, float apartureHeight)
	{
		return 180f * Mathf.Atan(apartureHeight * 0.5f / focalLength) / 3.1415927f;
	}

	// Token: 0x060053EA RID: 21482 RVA: 0x00177CD9 File Offset: 0x001760D9
	private float CalcHorizontalFov(float focalLength, float apartureWidth)
	{
		return 180f * Mathf.Atan(apartureWidth * 0.5f / (focalLength * 0.81f)) / 3.1415927f;
	}

	// Token: 0x040054AA RID: 21674
	public string m_HierarchyName;

	// Token: 0x040054AB RID: 21675
	public eCameraProjection m_ProjectionType;

	// Token: 0x040054AC RID: 21676
	public MsbCameraHandler.MsbCameraParam m_Src;

	// Token: 0x040054AD RID: 21677
	private MsbCameraHandler.MsbCameraParam m_Work;

	// Token: 0x040054AE RID: 21678
	private MsbHandler m_Owner;

	// Token: 0x040054AF RID: 21679
	private GameObject m_HierarchyGO;

	// Token: 0x040054B0 RID: 21680
	private Camera m_Camera;

	// Token: 0x040054B1 RID: 21681
	private Transform m_CameraTransform;

	// Token: 0x040054B2 RID: 21682
	private PostProcessRenderer m_PostProcessRenderer;

	// Token: 0x040054B3 RID: 21683
	private Camera m_PostProcessAttachedCamera;

	// Token: 0x040054B4 RID: 21684
	[SerializeField]
	private bool m_PostProcessEnabled = true;

	// Token: 0x040054B5 RID: 21685
	private const float m_BloomOffsetBaseSize = 960f;

	// Token: 0x02000FBC RID: 4028
	[Serializable]
	public class MsbCameraParam
	{
		// Token: 0x060053EB RID: 21483 RVA: 0x00177CFC File Offset: 0x001760FC
		public MsbCameraParam()
		{
			this.m_FocalLength = 0f;
			this.m_ApartureWidth = 0f;
			this.m_ApartureHeight = 0f;
			this.m_Znear = 0f;
			this.m_Zfar = 0f;
			this.m_ExposureValue = 1f;
			this.m_ToneMapWhitePointScale = 1f;
			this.m_ToneMapWhitePointRangeMin = 0.2f;
			this.m_ToneMapWhitePointRangeMax = 5f;
			this.m_BloomReferenceThresholdOffset = 1f;
			this.m_BloomOffset = 5f;
			this.m_ContrastLevel = 0f;
			this.m_BrightnessLevel = 0f;
			this.m_ChromaLevel = 0f;
			this.m_ColorBlendLevel = 0f;
			this.m_ColorBlendColor = Color.white;
			this.m_AdaptationRateSpeed = 0.2f;
			this.m_bEnableCorrectToneMap = false;
			this.m_bEnableBloom = false;
			this.m_bEnableCorrectContrast = false;
			this.m_bEnableCorrectBrightness = false;
			this.m_bEnableCorrectChroma = false;
			this.m_bEnableCorrectColorBlend = false;
			this.m_ToneMapWhitePointType = eToneMapWhitePoint.eToneMapWhitePoint_Fix;
			this.m_BloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;
		}

		// Token: 0x060053EC RID: 21484 RVA: 0x00177E04 File Offset: 0x00176204
		public MsbCameraParam(MsbCameraHandler.MsbCameraParam src)
		{
			this.m_FocalLength = src.m_FocalLength;
			this.m_ApartureWidth = src.m_ApartureWidth;
			this.m_ApartureHeight = src.m_ApartureHeight;
			this.m_Znear = src.m_Znear;
			this.m_Zfar = src.m_Zfar;
			this.m_OrthographicsSize = src.m_OrthographicsSize;
			this.m_ExposureValue = src.m_ExposureValue;
			this.m_ToneMapWhitePointScale = src.m_ToneMapWhitePointScale;
			this.m_ToneMapWhitePointRangeMin = src.m_ToneMapWhitePointRangeMin;
			this.m_ToneMapWhitePointRangeMax = src.m_ToneMapWhitePointRangeMax;
			this.m_BloomReferenceThresholdOffset = src.m_BloomReferenceThresholdOffset;
			this.m_BloomOffset = src.m_BloomOffset;
			this.m_ContrastLevel = src.m_ContrastLevel;
			this.m_BrightnessLevel = src.m_BrightnessLevel;
			this.m_ChromaLevel = src.m_ChromaLevel;
			this.m_ColorBlendLevel = src.m_ColorBlendLevel;
			this.m_ColorBlendColor = src.m_ColorBlendColor;
			this.m_AdaptationRateSpeed = src.m_AdaptationRateSpeed;
			this.m_bEnableCorrectToneMap = src.m_bEnableCorrectToneMap;
			this.m_bEnableBloom = src.m_bEnableBloom;
			this.m_bEnableCorrectContrast = src.m_bEnableCorrectContrast;
			this.m_bEnableCorrectBrightness = src.m_bEnableCorrectBrightness;
			this.m_bEnableCorrectChroma = src.m_bEnableCorrectChroma;
			this.m_bEnableCorrectColorBlend = src.m_bEnableCorrectColorBlend;
			this.m_ToneMapWhitePointType = src.m_ToneMapWhitePointType;
			this.m_BloomReferenceType = src.m_BloomReferenceType;
		}

		// Token: 0x040054B6 RID: 21686
		public float m_FocalLength;

		// Token: 0x040054B7 RID: 21687
		public float m_ApartureWidth;

		// Token: 0x040054B8 RID: 21688
		public float m_ApartureHeight;

		// Token: 0x040054B9 RID: 21689
		public float m_Znear;

		// Token: 0x040054BA RID: 21690
		public float m_Zfar;

		// Token: 0x040054BB RID: 21691
		public float m_OrthographicsSize;

		// Token: 0x040054BC RID: 21692
		public float m_ExposureValue;

		// Token: 0x040054BD RID: 21693
		public float m_ToneMapWhitePointScale;

		// Token: 0x040054BE RID: 21694
		public float m_ToneMapWhitePointRangeMin;

		// Token: 0x040054BF RID: 21695
		public float m_ToneMapWhitePointRangeMax;

		// Token: 0x040054C0 RID: 21696
		public float m_BloomReferenceThresholdOffset;

		// Token: 0x040054C1 RID: 21697
		public float m_BloomOffset;

		// Token: 0x040054C2 RID: 21698
		public float m_ContrastLevel;

		// Token: 0x040054C3 RID: 21699
		public float m_BrightnessLevel;

		// Token: 0x040054C4 RID: 21700
		public float m_ChromaLevel;

		// Token: 0x040054C5 RID: 21701
		public float m_ColorBlendLevel;

		// Token: 0x040054C6 RID: 21702
		public Color m_ColorBlendColor;

		// Token: 0x040054C7 RID: 21703
		public float m_AdaptationRateSpeed;

		// Token: 0x040054C8 RID: 21704
		public bool m_bEnableCorrectToneMap;

		// Token: 0x040054C9 RID: 21705
		public bool m_bEnableBloom;

		// Token: 0x040054CA RID: 21706
		public bool m_bEnableCorrectContrast;

		// Token: 0x040054CB RID: 21707
		public bool m_bEnableCorrectBrightness;

		// Token: 0x040054CC RID: 21708
		public bool m_bEnableCorrectChroma;

		// Token: 0x040054CD RID: 21709
		public bool m_bEnableCorrectColorBlend;

		// Token: 0x040054CE RID: 21710
		public eToneMapWhitePoint m_ToneMapWhitePointType;

		// Token: 0x040054CF RID: 21711
		public eBloomReferenceType m_BloomReferenceType;
	}
}
