﻿using System;

// Token: 0x02000E95 RID: 3733
internal sealed class FieldAccessor<TTarget, TField> : IFieldAccessor
{
	// Token: 0x06004D54 RID: 19796 RVA: 0x00158FCE File Offset: 0x001573CE
	public FieldAccessor(Func<TTarget, TField> getter, Action<TTarget, TField> setter)
	{
		this.getter = getter;
		this.setter = setter;
	}

	// Token: 0x06004D55 RID: 19797 RVA: 0x00158FE4 File Offset: 0x001573E4
	public object GetValue(object target)
	{
		return this.getter((TTarget)((object)target));
	}

	// Token: 0x06004D56 RID: 19798 RVA: 0x00158FFC File Offset: 0x001573FC
	public void SetValue(object target, object value)
	{
		this.setter((TTarget)((object)target), (TField)((object)value));
	}

	// Token: 0x04004DFF RID: 19967
	private readonly Func<TTarget, TField> getter;

	// Token: 0x04004E00 RID: 19968
	private readonly Action<TTarget, TField> setter;
}
