﻿using System;
using System.Collections.Generic;

// Token: 0x020002F9 RID: 761
public static class Dictionary_Ext
{
	// Token: 0x06000EDB RID: 3803 RVA: 0x0004FDCC File Offset: 0x0004E1CC
	public static void AddOrReplace<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key, TValue value)
	{
		if (self.ContainsKey(key))
		{
			self[key] = value;
		}
		else
		{
			self.Add(key, value);
		}
	}
}
