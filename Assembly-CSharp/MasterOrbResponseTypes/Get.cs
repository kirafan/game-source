﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace MasterOrbResponseTypes
{
	// Token: 0x02000C37 RID: 3127
	public class Get : CommonResponse
	{
		// Token: 0x06004027 RID: 16423 RVA: 0x0013DEC0 File Offset: 0x0013C2C0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.masterOrbs == null) ? string.Empty : this.masterOrbs.ToString());
		}

		// Token: 0x040045FF RID: 17919
		public MasterOrb[] masterOrbs;
	}
}
