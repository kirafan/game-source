﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace MasterOrbResponseTypes
{
	// Token: 0x02000C36 RID: 3126
	public class Getall : CommonResponse
	{
		// Token: 0x06004025 RID: 16421 RVA: 0x0013DE7C File Offset: 0x0013C27C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.masterOrbs == null) ? string.Empty : this.masterOrbs.ToString());
		}

		// Token: 0x040045FE RID: 17918
		public MasterOrb[] masterOrbs;
	}
}
