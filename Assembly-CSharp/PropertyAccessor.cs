﻿using System;

// Token: 0x02000E92 RID: 3730
internal sealed class PropertyAccessor<TTarget, TProperty> : IPropertyAccessor
{
	// Token: 0x06004D4C RID: 19788 RVA: 0x00158EAC File Offset: 0x001572AC
	public PropertyAccessor(Func<TTarget, TProperty> getter, Action<TTarget, TProperty> setter)
	{
		this.getter = getter;
		this.setter = setter;
	}

	// Token: 0x06004D4D RID: 19789 RVA: 0x00158EC2 File Offset: 0x001572C2
	public object GetValue(object target)
	{
		return this.getter((TTarget)((object)target));
	}

	// Token: 0x06004D4E RID: 19790 RVA: 0x00158EDA File Offset: 0x001572DA
	public void SetValue(object target, object value)
	{
		this.setter((TTarget)((object)target), (TProperty)((object)value));
	}

	// Token: 0x06004D4F RID: 19791 RVA: 0x00158EF3 File Offset: 0x001572F3
	public TProperty GetValue(TTarget target)
	{
		return this.getter(target);
	}

	// Token: 0x06004D50 RID: 19792 RVA: 0x00158F01 File Offset: 0x00157301
	public void SetValue(TTarget target, TProperty value)
	{
		this.setter(target, value);
	}

	// Token: 0x04004DFD RID: 19965
	private readonly Func<TTarget, TProperty> getter;

	// Token: 0x04004DFE RID: 19966
	private readonly Action<TTarget, TProperty> setter;
}
