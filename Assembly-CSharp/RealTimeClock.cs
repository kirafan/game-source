﻿using System;
using System.Globalization;
using UnityEngine;

// Token: 0x02000FAF RID: 4015
public sealed class RealTimeClock : MonoBehaviour
{
	// Token: 0x06005368 RID: 21352 RVA: 0x00175B25 File Offset: 0x00173F25
	private void Awake()
	{
		RealTimeClock.UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0);
		this.SetupDate();
		Helper.RandomInit();
	}

	// Token: 0x06005369 RID: 21353 RVA: 0x00175B47 File Offset: 0x00173F47
	private void Update()
	{
		this.SetupDate();
	}

	// Token: 0x0600536A RID: 21354 RVA: 0x00175B4F File Offset: 0x00173F4F
	private void SetupDate()
	{
		this.m_DateTime = DateTime.Now;
		this.m_UTC = this.m_DateTime.ToUniversalTime();
		this.m_UTC_FromEpoch = this.m_UTC - RealTimeClock.UNIX_EPOCH;
	}

	// Token: 0x0600536B RID: 21355 RVA: 0x00175B83 File Offset: 0x00173F83
	public int GetYear()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetYear(this.m_DateTime);
	}

	// Token: 0x0600536C RID: 21356 RVA: 0x00175B9A File Offset: 0x00173F9A
	public int GetMonth()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetMonth(this.m_DateTime);
	}

	// Token: 0x0600536D RID: 21357 RVA: 0x00175BB1 File Offset: 0x00173FB1
	public int GetDay()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetDayOfMonth(this.m_DateTime);
	}

	// Token: 0x0600536E RID: 21358 RVA: 0x00175BC8 File Offset: 0x00173FC8
	public int GetHour()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetHour(this.m_DateTime);
	}

	// Token: 0x0600536F RID: 21359 RVA: 0x00175BDF File Offset: 0x00173FDF
	public int GetMinuite()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetMinute(this.m_DateTime);
	}

	// Token: 0x06005370 RID: 21360 RVA: 0x00175BF6 File Offset: 0x00173FF6
	public int GetSecond()
	{
		return DateTimeFormatInfo.CurrentInfo.Calendar.GetSecond(this.m_DateTime);
	}

	// Token: 0x06005371 RID: 21361 RVA: 0x00175C0D File Offset: 0x0017400D
	public long GetUTCSecondFromEpoch()
	{
		return (long)this.m_UTC_FromEpoch.TotalSeconds;
	}

	// Token: 0x0400545B RID: 21595
	private static DateTime UNIX_EPOCH;

	// Token: 0x0400545C RID: 21596
	private DateTime m_DateTime;

	// Token: 0x0400545D RID: 21597
	private DateTime m_UTC;

	// Token: 0x0400545E RID: 21598
	private TimeSpan m_UTC_FromEpoch;
}
