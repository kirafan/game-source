﻿using System;
using Meige;
using SupportcharacterRequestTypes;
using WWWTypes;

// Token: 0x02000B6D RID: 2925
public static class SupportcharacterRequest
{
	// Token: 0x06003E9D RID: 16029 RVA: 0x0013B1D0 File Offset: 0x001395D0
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("supportCharacters", param.supportCharacters);
		return meigewwwParam;
	}

	// Token: 0x06003E9E RID: 16030 RVA: 0x0013B204 File Offset: 0x00139604
	public static MeigewwwParam Set(PlayerSupport[] supportCharacters, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("supportCharacters", supportCharacters);
		return meigewwwParam;
	}

	// Token: 0x06003E9F RID: 16031 RVA: 0x0013B234 File Offset: 0x00139634
	public static MeigewwwParam Setname(Setname param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/set_name", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedSupportId", param.managedSupportId);
		meigewwwParam.Add("name", param.name);
		return meigewwwParam;
	}

	// Token: 0x06003EA0 RID: 16032 RVA: 0x0013B27C File Offset: 0x0013967C
	public static MeigewwwParam Setname(long managedSupportId, string name, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/set_name", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedSupportId", managedSupportId);
		meigewwwParam.Add("name", name);
		return meigewwwParam;
	}

	// Token: 0x06003EA1 RID: 16033 RVA: 0x0013B2BC File Offset: 0x001396BC
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003EA2 RID: 16034 RVA: 0x0013B2E0 File Offset: 0x001396E0
	public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/support/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}
}
