﻿using System;
using Meige;
using PlayerRequestTypes;
using UnityEngine;

// Token: 0x0200065B RID: 1627
public class PushNotificationService : SingletonMonoBehaviour<PushNotificationService>
{
	// Token: 0x17000241 RID: 577
	// (get) Token: 0x060020EA RID: 8426 RVA: 0x000AF87B File Offset: 0x000ADC7B
	public bool isRegist
	{
		get
		{
			return this.m_isRegist;
		}
	}

	// Token: 0x060020EB RID: 8427 RVA: 0x000AF884 File Offset: 0x000ADC84
	public void RegisterDevice()
	{
		if (this.m_isRegist)
		{
			return;
		}
		if (string.IsNullOrEmpty("624793215742"))
		{
			Debug.Log("sender id is null");
			return;
		}
		GCM.Register(delegate(string regId)
		{
			if (string.IsNullOrEmpty(regId))
			{
				Debug.Log(string.Format("Failed to get the registration id", new object[0]));
				return;
			}
			Debug.Log(string.Format("Your registration Id is = {0}", regId));
			this.PushRegister(regId);
		}, new string[]
		{
			"624793215742"
		});
	}

	// Token: 0x060020EC RID: 8428 RVA: 0x000AF8D8 File Offset: 0x000ADCD8
	private void PushRegister(string token)
	{
		MeigewwwParam wwwParam = PlayerRequest.Setpushtoken(new Setpushtoken
		{
			pushToken = token
		}, delegate(MeigewwwParam responseWWW)
		{
			this.m_isRegist = true;
		});
		NetworkQueueManager.Request(wwwParam);
	}

	// Token: 0x04002723 RID: 10019
	private const string GoogleConsoleProjectId = "624793215742";

	// Token: 0x04002724 RID: 10020
	private bool m_isRegist;
}
