﻿using System;
using System.Collections.Generic;
using Meige;
using WeaponRecipeResponseTypes;
using WWWTypes;

// Token: 0x02000C93 RID: 3219
public static class WeaponRecipeResponse
{
	// Token: 0x060040DE RID: 16606 RVA: 0x0013FF08 File Offset: 0x0013E308
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x060040DF RID: 16607 RVA: 0x0013FF20 File Offset: 0x0013E320
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}
}
