﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000CDF RID: 3295
public class ProjDepend
{
	// Token: 0x0600415B RID: 16731 RVA: 0x0014112E File Offset: 0x0013F52E
	public static int GetMaskFromLayer(ProjDepend.eLayer id)
	{
		return 1 << (int)id;
	}

	// Token: 0x170003EA RID: 1002
	// (get) Token: 0x0600415C RID: 16732 RVA: 0x00141136 File Offset: 0x0013F536
	public static GameScreen.ASPECT_ADJUST_TYPE aspectAdjustType
	{
		get
		{
			return GameScreen.ASPECT_ADJUST_TYPE.CINEMA_SCOPE;
		}
	}

	// Token: 0x170003EB RID: 1003
	// (get) Token: 0x0600415D RID: 16733 RVA: 0x00141139 File Offset: 0x0013F539
	public static Vector2 uiSpaceSize
	{
		get
		{
			return ProjDepend.m_uiSpaceSize;
		}
	}

	// Token: 0x170003EC RID: 1004
	// (get) Token: 0x0600415E RID: 16734 RVA: 0x00141140 File Offset: 0x0013F540
	public static byte[] CACK
	{
		get
		{
			return PDBUtility.SerializeBytes(ProjDepend.m_ABKBinary);
		}
	}

	// Token: 0x170003ED RID: 1005
	// (get) Token: 0x0600415F RID: 16735 RVA: 0x0014114C File Offset: 0x0013F54C
	public static byte[] CACIV
	{
		get
		{
			return PDBUtility.SerializeBytes(ProjDepend.m_ACIVBinary);
		}
	}

	// Token: 0x170003EE RID: 1006
	// (get) Token: 0x06004160 RID: 16736 RVA: 0x00141158 File Offset: 0x0013F558
	public static string WHERHK
	{
		get
		{
			return PDBUtility.Serialize(ProjDepend.m_RHKBinary);
		}
	}

	// Token: 0x170003EF RID: 1007
	// (get) Token: 0x06004161 RID: 16737 RVA: 0x00141164 File Offset: 0x0013F564
	public static string WHSIK
	{
		get
		{
			return PDBUtility.Serialize(ProjDepend.m_SIKBinary);
		}
	}

	// Token: 0x170003F0 RID: 1008
	// (get) Token: 0x06004162 RID: 16738 RVA: 0x00141170 File Offset: 0x0013F570
	public static string WHRPK
	{
		get
		{
			return PDBUtility.Serialize(ProjDepend.m_RKBinary);
		}
	}

	// Token: 0x06004163 RID: 16739 RVA: 0x0014117C File Offset: 0x0013F57C
	public static void SetRHKBinary(byte[] binary)
	{
		ProjDepend.m_RHKBinary = binary;
	}

	// Token: 0x06004164 RID: 16740 RVA: 0x00141184 File Offset: 0x0013F584
	public static void SetSIKBinary(byte[] binary)
	{
		ProjDepend.m_SIKBinary = binary;
	}

	// Token: 0x06004165 RID: 16741 RVA: 0x0014118C File Offset: 0x0013F58C
	public static void SetRKBinary(byte[] binary)
	{
		ProjDepend.m_RKBinary = binary;
	}

	// Token: 0x06004166 RID: 16742 RVA: 0x00141194 File Offset: 0x0013F594
	public static void SetABKBinary(byte[] binary)
	{
		ProjDepend.m_ABKBinary = binary;
	}

	// Token: 0x06004167 RID: 16743 RVA: 0x0014119C File Offset: 0x0013F59C
	public static void SetACIVBinary(byte[] binary)
	{
		ProjDepend.m_ACIVBinary = binary;
	}

	// Token: 0x0400499C RID: 18844
	private const GameScreen.ASPECT_ADJUST_TYPE m_aspectAdjustType = GameScreen.ASPECT_ADJUST_TYPE.CINEMA_SCOPE;

	// Token: 0x0400499D RID: 18845
	private static readonly Vector2 m_uiSpaceSize = new Vector2(320f, 480f);

	// Token: 0x0400499E RID: 18846
	public static int ASSETBUNDLE_MAX_LOADING_COUNT = 200;

	// Token: 0x0400499F RID: 18847
	public static int ASSETBUNDLE_MAX_DOWNLOADING_COUNT = 5;

	// Token: 0x040049A0 RID: 18848
	public static readonly int CRYPT_AES_CBC_LENGTH = 128;

	// Token: 0x040049A1 RID: 18849
	public static readonly bool RH_CRC_IS_HEXADECIMAL = true;

	// Token: 0x040049A2 RID: 18850
	private static byte[] m_RHKBinary = null;

	// Token: 0x040049A3 RID: 18851
	private static byte[] m_SIKBinary = null;

	// Token: 0x040049A4 RID: 18852
	private static byte[] m_RKBinary = null;

	// Token: 0x040049A5 RID: 18853
	private static byte[] m_ABKBinary = null;

	// Token: 0x040049A6 RID: 18854
	private static byte[] m_ACIVBinary = null;

	// Token: 0x040049A7 RID: 18855
	public static string SERVER_APPLYING_URL = string.Empty;

	// Token: 0x040049A8 RID: 18856
	public static string REQUESTHASH_SECRET_APPLYING = "2d1fd943fb14bb20dbceb05cff23157d4b2097929f197a4833fed9a36e5b0cbb";

	// Token: 0x040049A9 RID: 18857
	public static readonly string SERVER_URL = "https://krr-prd.star-api.com/api/";

	// Token: 0x040049AA RID: 18858
	public static readonly string REQUESTHASH_SECRET = "85af4a94ce7a280f69844743212a8b867206ab28946e1e30e6c1a10196609a11";

	// Token: 0x02000CE0 RID: 3296
	public enum eRenderQueue
	{
		// Token: 0x040049AC RID: 18860
		eRenderQueue_BG_Higher,
		// Token: 0x040049AD RID: 18861
		eRenderQueue_BG_High = 125,
		// Token: 0x040049AE RID: 18862
		eRenderQueue_BG = 250,
		// Token: 0x040049AF RID: 18863
		eRenderQueue_BG_Low = 375,
		// Token: 0x040049B0 RID: 18864
		eRenderQueue_BG_Lower = 500,
		// Token: 0x040049B1 RID: 18865
		eRenderQueue_Opaque_Higher = 625,
		// Token: 0x040049B2 RID: 18866
		eRenderQueue_Opaque_High = 750,
		// Token: 0x040049B3 RID: 18867
		eRenderQueue_Opaque = 875,
		// Token: 0x040049B4 RID: 18868
		eRenderQueue_Opaque_Low = 1000,
		// Token: 0x040049B5 RID: 18869
		eRenderQueue_Opaque_Lower = 1125,
		// Token: 0x040049B6 RID: 18870
		eRenderQueue_PunchThrough_Higher = 1250,
		// Token: 0x040049B7 RID: 18871
		eRenderQueue_PunchThrough_High = 1375,
		// Token: 0x040049B8 RID: 18872
		eRenderQueue_PunchThrough = 1500,
		// Token: 0x040049B9 RID: 18873
		eRenderQueue_PunchThrough_Low = 1625,
		// Token: 0x040049BA RID: 18874
		eRenderQueue_PunchThrough_Lower = 1750,
		// Token: 0x040049BB RID: 18875
		eRenderQueue_AfterPunchThrough_Higher = 1875,
		// Token: 0x040049BC RID: 18876
		eRenderQueue_AfterPunchThrough_High = 2000,
		// Token: 0x040049BD RID: 18877
		eRenderQueue_AfterPunchThrough = 2125,
		// Token: 0x040049BE RID: 18878
		eRenderQueue_AfterPunchThrough_Low = 2250,
		// Token: 0x040049BF RID: 18879
		eRenderQueue_AfterPunchThrough_Lower = 2375,
		// Token: 0x040049C0 RID: 18880
		eRenderQueue_Alpha_Higher = 2501,
		// Token: 0x040049C1 RID: 18881
		eRenderQueue_Alpha_High = 2626,
		// Token: 0x040049C2 RID: 18882
		eRenderQueue_Alpha = 2751,
		// Token: 0x040049C3 RID: 18883
		eRenderQueue_Alpha_Low = 2876,
		// Token: 0x040049C4 RID: 18884
		eRenderQueue_Alpha_Lower = 3001,
		// Token: 0x040049C5 RID: 18885
		eRenderQueue_AfterAlpha_Higher = 3126,
		// Token: 0x040049C6 RID: 18886
		eRenderQueue_AfterAlpha_High = 3251,
		// Token: 0x040049C7 RID: 18887
		eRenderQueue_AfterAlpha = 3376,
		// Token: 0x040049C8 RID: 18888
		eRenderQueue_AfterAlpha_Low = 3501,
		// Token: 0x040049C9 RID: 18889
		eRenderQueue_AfterAlpha_Lower = 3626,
		// Token: 0x040049CA RID: 18890
		eRenderQueue_UI_Higher = 3751,
		// Token: 0x040049CB RID: 18891
		eRenderQueue_UI_High = 3876,
		// Token: 0x040049CC RID: 18892
		eRenderQueue_UI = 4001,
		// Token: 0x040049CD RID: 18893
		eRenderQueue_UI_Low = 4126,
		// Token: 0x040049CE RID: 18894
		eRenderQueue_UI_Lower = 4251,
		// Token: 0x040049CF RID: 18895
		eRenderQueue_FinalPass_Higher = 4376,
		// Token: 0x040049D0 RID: 18896
		eRenderQueue_FinalPass_High = 4501,
		// Token: 0x040049D1 RID: 18897
		eRenderQueue_FinalPass = 4626,
		// Token: 0x040049D2 RID: 18898
		eRenderQueue_FinalPass_Low = 4751,
		// Token: 0x040049D3 RID: 18899
		eRenderQueue_FinalPass_Lower = 4876,
		// Token: 0x040049D4 RID: 18900
		eRenderQueue_SystemFont = 5001
	}

	// Token: 0x02000CE1 RID: 3297
	public enum eLayer
	{
		// Token: 0x040049D6 RID: 18902
		eBuiltinLayerStart,
		// Token: 0x040049D7 RID: 18903
		eLayer_Default = 0,
		// Token: 0x040049D8 RID: 18904
		eLayer_TransparentFX,
		// Token: 0x040049D9 RID: 18905
		eLayer_IgnoreRaycast,
		// Token: 0x040049DA RID: 18906
		eLayer_Blank0,
		// Token: 0x040049DB RID: 18907
		eLayer_Water,
		// Token: 0x040049DC RID: 18908
		eLayer_Blank1,
		// Token: 0x040049DD RID: 18909
		eLayer_Blank2,
		// Token: 0x040049DE RID: 18910
		eLayer_Blank3,
		// Token: 0x040049DF RID: 18911
		eBuiltinLayer_End = 7,
		// Token: 0x040049E0 RID: 18912
		eUserLayerStart,
		// Token: 0x040049E1 RID: 18913
		eLayer_Graphic2D = 8,
		// Token: 0x040049E2 RID: 18914
		eLayer_Player,
		// Token: 0x040049E3 RID: 18915
		eLayer_Enemy,
		// Token: 0x040049E4 RID: 18916
		eLayer_Sample
	}
}
