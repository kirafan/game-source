﻿using System;
using UnityEngine;

// Token: 0x02000CDB RID: 3291
public class GameScreen
{
	// Token: 0x0600413F RID: 16703 RVA: 0x00140B10 File Offset: 0x0013EF10
	public static void Create()
	{
		if (GameScreen.m_Instance == null)
		{
			GameScreen.m_Instance = new GameScreen();
		}
		GameScreen.m_Instance.Init();
	}

	// Token: 0x170003DC RID: 988
	// (get) Token: 0x06004140 RID: 16704 RVA: 0x00140B30 File Offset: 0x0013EF30
	public static GameScreen Instance
	{
		get
		{
			if (GameScreen.m_Instance == null)
			{
				Debug.Break();
			}
			return GameScreen.m_Instance;
		}
	}

	// Token: 0x170003DD RID: 989
	// (get) Token: 0x06004141 RID: 16705 RVA: 0x00140B46 File Offset: 0x0013EF46
	public GameScreen.ScreenSpace screenSpace
	{
		get
		{
			return this.m_screenSpace;
		}
	}

	// Token: 0x170003DE RID: 990
	// (get) Token: 0x06004142 RID: 16706 RVA: 0x00140B4E File Offset: 0x0013EF4E
	public GameScreen.UISpace uiSpace
	{
		get
		{
			return this.m_uiSpace;
		}
	}

	// Token: 0x170003DF RID: 991
	// (get) Token: 0x06004143 RID: 16707 RVA: 0x00140B56 File Offset: 0x0013EF56
	public GameScreen.ASPECT_ADJUST_TYPE aspectAdjustType
	{
		get
		{
			return this.m_aspectAdjustType;
		}
	}

	// Token: 0x06004144 RID: 16708 RVA: 0x00140B60 File Offset: 0x0013EF60
	private void InitScreen()
	{
		Vector2 zero = Vector2.zero;
		Vector2 zero2 = Vector2.zero;
		Vector2 zero3 = Vector2.zero;
		Vector2 zero4 = Vector2.zero;
		Vector2 zero5 = Vector2.zero;
		Vector2 uiSpaceSize = ProjDepend.uiSpaceSize;
		GameScreen.ASPECT_ADJUST_TYPE aspectAdjustType = ProjDepend.aspectAdjustType;
		switch (Screen.orientation)
		{
		case ScreenOrientation.Portrait:
			if (Screen.width >= Screen.height)
			{
				zero.x = (float)Screen.height;
				zero.y = (float)Screen.width;
			}
			else
			{
				zero.x = (float)Screen.width;
				zero.y = (float)Screen.height;
			}
			goto IL_135;
		case ScreenOrientation.LandscapeLeft:
			if (Screen.width >= Screen.height)
			{
				zero.x = (float)Screen.width;
				zero.y = (float)Screen.height;
			}
			else
			{
				zero.x = (float)Screen.height;
				zero.y = (float)Screen.width;
			}
			goto IL_135;
		case ScreenOrientation.AutoRotation:
			zero.x = (float)Screen.height;
			zero.y = (float)Screen.width;
			goto IL_135;
		}
		zero.x = (float)Screen.width;
		zero.y = (float)Screen.height;
		IL_135:
		float num = uiSpaceSize.x / uiSpaceSize.y;
		zero4.x = zero.x / uiSpaceSize.x;
		zero4.y = zero.y / uiSpaceSize.y;
		float num2 = zero.y;
		float num3 = zero.y * num;
		if (zero.x < num3)
		{
			num3 = zero.x;
			num2 = zero.x / num;
		}
		if (aspectAdjustType != GameScreen.ASPECT_ADJUST_TYPE.STRETCH)
		{
			if (aspectAdjustType == GameScreen.ASPECT_ADJUST_TYPE.CINEMA_SCOPE)
			{
				zero3.x = num3;
				zero3.y = num2;
				zero2.x = (zero.x - num3) / 2f;
				zero2.y = (zero.y - num2) / 2f;
			}
		}
		else
		{
			zero3.x = zero.x;
			zero3.y = zero.y;
			zero2.x = 0f;
			zero2.y = 0f;
		}
		zero5.x = zero3.x / uiSpaceSize.x;
		zero5.y = zero3.y / uiSpaceSize.y;
		this.m_aspectAdjustType = aspectAdjustType;
		this.m_screenSpace = new GameScreen.ScreenSpace(zero, zero2, zero3);
		this.m_uiSpace = new GameScreen.UISpace(uiSpaceSize, zero4, zero5);
		this.m_screenSpace.uiSpace = this.m_uiSpace;
		this.m_uiSpace.screenSpace = this.m_screenSpace;
	}

	// Token: 0x06004145 RID: 16709 RVA: 0x00140E25 File Offset: 0x0013F225
	public void Init()
	{
		this.InitScreen();
	}

	// Token: 0x06004146 RID: 16710 RVA: 0x00140E30 File Offset: 0x0013F230
	public Vector2 ScreenToUI(Vector2 pos)
	{
		Vector2 result = new Vector2((pos.x - this.screenSpace.fixedAspectPos.x) / this.screenSpace.fixedAspectSize.x, (pos.y - this.screenSpace.fixedAspectPos.y) / this.screenSpace.fixedAspectSize.y);
		result = new Vector2(this.uiSpace.size.x * result.x, this.uiSpace.size.y * (1f - result.y));
		return result;
	}

	// Token: 0x06004147 RID: 16711 RVA: 0x00140EEC File Offset: 0x0013F2EC
	public float GetGUITextX(int x)
	{
		return (float)x / this.uiSpace.size.x;
	}

	// Token: 0x06004148 RID: 16712 RVA: 0x00140F10 File Offset: 0x0013F310
	public float GetGUITextY(int y)
	{
		return (this.uiSpace.size.y - (float)y) / this.uiSpace.size.y;
	}

	// Token: 0x06004149 RID: 16713 RVA: 0x00140F49 File Offset: 0x0013F349
	public Vector2 GetGUIText(int x, int y)
	{
		return new Vector2(this.GetGUITextX(x), this.GetGUITextY(y));
	}

	// Token: 0x0600414A RID: 16714 RVA: 0x00140F60 File Offset: 0x0013F360
	public Vector3 GetGUITextScale()
	{
		return new Vector3(320f / this.screenSpace.fixedAspectSize.x * this.uiSpace.fixedAspectPerUI.y, 320f / this.screenSpace.fixedAspectSize.y * this.uiSpace.fixedAspectPerUI.y, 1f);
	}

	// Token: 0x0600414B RID: 16715 RVA: 0x00140FD4 File Offset: 0x0013F3D4
	public Vector2 CalcScreenPos(Vector2 xy)
	{
		return new Vector2(xy.x * this.uiSpace.screenPerUI.x, xy.y * this.uiSpace.screenPerUI.y);
	}

	// Token: 0x0600414C RID: 16716 RVA: 0x0014101C File Offset: 0x0013F41C
	public Vector2 CalcRenderSize(Vector2 wh)
	{
		return new Vector2(wh.x * this.uiSpace.screenPerUI.x, wh.y * this.uiSpace.screenPerUI.y);
	}

	// Token: 0x04004989 RID: 18825
	private static GameScreen m_Instance;

	// Token: 0x0400498A RID: 18826
	protected GameScreen.ScreenSpace m_screenSpace;

	// Token: 0x0400498B RID: 18827
	protected GameScreen.UISpace m_uiSpace;

	// Token: 0x0400498C RID: 18828
	protected GameScreen.ASPECT_ADJUST_TYPE m_aspectAdjustType;

	// Token: 0x0400498D RID: 18829
	private const float GUITEXT_SCALE_BASE = 320f;

	// Token: 0x02000CDC RID: 3292
	public class ScreenSpace
	{
		// Token: 0x0600414E RID: 16718 RVA: 0x00141066 File Offset: 0x0013F466
		public ScreenSpace(Vector2 size, Vector3 fixedAspectPos, Vector3 fixedAspectSize)
		{
			this.m_size = size;
			this.m_aspect = size.x / size.y;
			this.m_fixedAspectPos = fixedAspectPos;
			this.m_fixedAspectSize = fixedAspectSize;
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x0600414F RID: 16719 RVA: 0x001410A2 File Offset: 0x0013F4A2
		public Vector2 size
		{
			get
			{
				return this.m_size;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06004150 RID: 16720 RVA: 0x001410AA File Offset: 0x0013F4AA
		public float aspect
		{
			get
			{
				return this.m_aspect;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x06004151 RID: 16721 RVA: 0x001410B2 File Offset: 0x0013F4B2
		public Vector2 fixedAspectPos
		{
			get
			{
				return this.m_fixedAspectPos;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x06004152 RID: 16722 RVA: 0x001410BA File Offset: 0x0013F4BA
		public Vector2 fixedAspectSize
		{
			get
			{
				return this.m_fixedAspectSize;
			}
		}

		// Token: 0x170003E4 RID: 996
		// (set) Token: 0x06004153 RID: 16723 RVA: 0x001410C2 File Offset: 0x0013F4C2
		public GameScreen.UISpace uiSpace
		{
			set
			{
				this.m_uiSpace = value;
			}
		}

		// Token: 0x0400498E RID: 18830
		protected Vector2 m_size;

		// Token: 0x0400498F RID: 18831
		protected float m_aspect;

		// Token: 0x04004990 RID: 18832
		protected Vector2 m_fixedAspectPos;

		// Token: 0x04004991 RID: 18833
		protected Vector2 m_fixedAspectSize;

		// Token: 0x04004992 RID: 18834
		protected GameScreen.UISpace m_uiSpace;
	}

	// Token: 0x02000CDD RID: 3293
	public class UISpace
	{
		// Token: 0x06004154 RID: 16724 RVA: 0x001410CB File Offset: 0x0013F4CB
		public UISpace(Vector2 size, Vector2 screenPerUI, Vector2 fixedAspectPerUI)
		{
			this.m_size = size;
			this.m_aspect = size.x / size.y;
			this.m_ScreenPerUI = screenPerUI;
			this.m_fixedAspectPerUI = fixedAspectPerUI;
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x06004155 RID: 16725 RVA: 0x001410FD File Offset: 0x0013F4FD
		public Vector2 size
		{
			get
			{
				return this.m_size;
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06004156 RID: 16726 RVA: 0x00141105 File Offset: 0x0013F505
		public float aspect
		{
			get
			{
				return this.m_aspect;
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06004157 RID: 16727 RVA: 0x0014110D File Offset: 0x0013F50D
		public Vector2 screenPerUI
		{
			get
			{
				return this.m_ScreenPerUI;
			}
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06004158 RID: 16728 RVA: 0x00141115 File Offset: 0x0013F515
		public Vector2 fixedAspectPerUI
		{
			get
			{
				return this.m_fixedAspectPerUI;
			}
		}

		// Token: 0x170003E9 RID: 1001
		// (set) Token: 0x06004159 RID: 16729 RVA: 0x0014111D File Offset: 0x0013F51D
		public GameScreen.ScreenSpace screenSpace
		{
			set
			{
				this.m_screenSpace = value;
			}
		}

		// Token: 0x04004993 RID: 18835
		protected Vector2 m_size;

		// Token: 0x04004994 RID: 18836
		protected float m_aspect;

		// Token: 0x04004995 RID: 18837
		protected Vector2 m_ScreenPerUI;

		// Token: 0x04004996 RID: 18838
		protected Vector2 m_fixedAspectPerUI;

		// Token: 0x04004997 RID: 18839
		protected GameScreen.ScreenSpace m_screenSpace;
	}

	// Token: 0x02000CDE RID: 3294
	public enum ASPECT_ADJUST_TYPE
	{
		// Token: 0x04004999 RID: 18841
		STRETCH,
		// Token: 0x0400499A RID: 18842
		CINEMA_SCOPE,
		// Token: 0x0400499B RID: 18843
		DEFAULT = 1
	}
}
