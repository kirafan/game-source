﻿using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace PreviewLabs
{
	// Token: 0x02000FAE RID: 4014
	public static class PlayerPrefs
	{
		// Token: 0x0600534A RID: 21322 RVA: 0x001750B4 File Offset: 0x001734B4
		static PlayerPrefs()
		{
			StreamReader streamReader = null;
			if (File.Exists(PlayerPrefs.secureFileName))
			{
				streamReader = new StreamReader(PlayerPrefs.secureFileName);
				PlayerPrefs.wasEncrypted = true;
				PlayerPrefs.serializedInput = PlayerPrefs.Decrypt(streamReader.ReadToEnd());
			}
			else if (File.Exists(PlayerPrefs.fileName))
			{
				streamReader = new StreamReader(PlayerPrefs.fileName);
				PlayerPrefs.serializedInput = streamReader.ReadToEnd();
			}
			if (!string.IsNullOrEmpty(PlayerPrefs.serializedInput))
			{
				if (PlayerPrefs.serializedInput.Length > 0 && PlayerPrefs.serializedInput[PlayerPrefs.serializedInput.Length - 1] == '\n')
				{
					PlayerPrefs.serializedInput = PlayerPrefs.serializedInput.Substring(0, PlayerPrefs.serializedInput.Length - 1);
					if (PlayerPrefs.serializedInput.Length > 0 && PlayerPrefs.serializedInput[PlayerPrefs.serializedInput.Length - 1] == '\r')
					{
						PlayerPrefs.serializedInput = PlayerPrefs.serializedInput.Substring(0, PlayerPrefs.serializedInput.Length - 1);
					}
				}
				PlayerPrefs.Deserialize();
			}
			if (streamReader != null)
			{
				streamReader.Close();
			}
		}

		// Token: 0x0600534B RID: 21323 RVA: 0x00175267 File Offset: 0x00173667
		public static bool HasKey(string key)
		{
			return PlayerPrefs.playerPrefsHashtable.ContainsKey(key);
		}

		// Token: 0x0600534C RID: 21324 RVA: 0x00175274 File Offset: 0x00173674
		public static void SetString(string key, string value)
		{
			if (!PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				PlayerPrefs.playerPrefsHashtable.Add(key, value);
			}
			else
			{
				PlayerPrefs.playerPrefsHashtable[key] = value;
			}
			PlayerPrefs.hashTableChanged = true;
		}

		// Token: 0x0600534D RID: 21325 RVA: 0x001752A9 File Offset: 0x001736A9
		public static void SetInt(string key, int value)
		{
			if (!PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				PlayerPrefs.playerPrefsHashtable.Add(key, value);
			}
			else
			{
				PlayerPrefs.playerPrefsHashtable[key] = value;
			}
			PlayerPrefs.hashTableChanged = true;
		}

		// Token: 0x0600534E RID: 21326 RVA: 0x001752E8 File Offset: 0x001736E8
		public static void SetFloat(string key, float value)
		{
			if (!PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				PlayerPrefs.playerPrefsHashtable.Add(key, value);
			}
			else
			{
				PlayerPrefs.playerPrefsHashtable[key] = value;
			}
			PlayerPrefs.hashTableChanged = true;
		}

		// Token: 0x0600534F RID: 21327 RVA: 0x00175327 File Offset: 0x00173727
		public static void SetBool(string key, bool value)
		{
			if (!PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				PlayerPrefs.playerPrefsHashtable.Add(key, value);
			}
			else
			{
				PlayerPrefs.playerPrefsHashtable[key] = value;
			}
			PlayerPrefs.hashTableChanged = true;
		}

		// Token: 0x06005350 RID: 21328 RVA: 0x00175366 File Offset: 0x00173766
		public static void SetLong(string key, long value)
		{
			if (!PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				PlayerPrefs.playerPrefsHashtable.Add(key, value);
			}
			else
			{
				PlayerPrefs.playerPrefsHashtable[key] = value;
			}
			PlayerPrefs.hashTableChanged = true;
		}

		// Token: 0x06005351 RID: 21329 RVA: 0x001753A5 File Offset: 0x001737A5
		public static string GetString(string key)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return PlayerPrefs.playerPrefsHashtable[key].ToString();
			}
			return null;
		}

		// Token: 0x06005352 RID: 21330 RVA: 0x001753C9 File Offset: 0x001737C9
		public static string GetString(string key, string defaultValue)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return PlayerPrefs.playerPrefsHashtable[key].ToString();
			}
			PlayerPrefs.playerPrefsHashtable.Add(key, defaultValue);
			PlayerPrefs.hashTableChanged = true;
			return defaultValue;
		}

		// Token: 0x06005353 RID: 21331 RVA: 0x001753FF File Offset: 0x001737FF
		public static int GetInt(string key)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (int)PlayerPrefs.playerPrefsHashtable[key];
			}
			return 0;
		}

		// Token: 0x06005354 RID: 21332 RVA: 0x00175423 File Offset: 0x00173823
		public static int GetInt(string key, int defaultValue)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (int)PlayerPrefs.playerPrefsHashtable[key];
			}
			PlayerPrefs.playerPrefsHashtable.Add(key, defaultValue);
			PlayerPrefs.hashTableChanged = true;
			return defaultValue;
		}

		// Token: 0x06005355 RID: 21333 RVA: 0x0017545E File Offset: 0x0017385E
		public static long GetLong(string key)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (long)PlayerPrefs.playerPrefsHashtable[key];
			}
			return 0L;
		}

		// Token: 0x06005356 RID: 21334 RVA: 0x00175483 File Offset: 0x00173883
		public static long GetLong(string key, long defaultValue)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (long)PlayerPrefs.playerPrefsHashtable[key];
			}
			PlayerPrefs.playerPrefsHashtable.Add(key, defaultValue);
			PlayerPrefs.hashTableChanged = true;
			return defaultValue;
		}

		// Token: 0x06005357 RID: 21335 RVA: 0x001754BE File Offset: 0x001738BE
		public static float GetFloat(string key)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (float)PlayerPrefs.playerPrefsHashtable[key];
			}
			return 0f;
		}

		// Token: 0x06005358 RID: 21336 RVA: 0x001754E6 File Offset: 0x001738E6
		public static float GetFloat(string key, float defaultValue)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (float)PlayerPrefs.playerPrefsHashtable[key];
			}
			PlayerPrefs.playerPrefsHashtable.Add(key, defaultValue);
			PlayerPrefs.hashTableChanged = true;
			return defaultValue;
		}

		// Token: 0x06005359 RID: 21337 RVA: 0x00175521 File Offset: 0x00173921
		public static bool GetBool(string key)
		{
			return PlayerPrefs.playerPrefsHashtable.ContainsKey(key) && (bool)PlayerPrefs.playerPrefsHashtable[key];
		}

		// Token: 0x0600535A RID: 21338 RVA: 0x00175545 File Offset: 0x00173945
		public static bool GetBool(string key, bool defaultValue)
		{
			if (PlayerPrefs.playerPrefsHashtable.ContainsKey(key))
			{
				return (bool)PlayerPrefs.playerPrefsHashtable[key];
			}
			PlayerPrefs.playerPrefsHashtable.Add(key, defaultValue);
			PlayerPrefs.hashTableChanged = true;
			return defaultValue;
		}

		// Token: 0x0600535B RID: 21339 RVA: 0x00175580 File Offset: 0x00173980
		public static void DeleteKey(string key)
		{
			PlayerPrefs.playerPrefsHashtable.Remove(key);
		}

		// Token: 0x0600535C RID: 21340 RVA: 0x0017558D File Offset: 0x0017398D
		public static void DeleteAll()
		{
			PlayerPrefs.playerPrefsHashtable.Clear();
		}

		// Token: 0x0600535D RID: 21341 RVA: 0x00175599 File Offset: 0x00173999
		public static bool WasReadPlayerPrefsFileEncrypted()
		{
			return PlayerPrefs.wasEncrypted;
		}

		// Token: 0x0600535E RID: 21342 RVA: 0x001755A0 File Offset: 0x001739A0
		public static void EnableEncryption(bool enabled)
		{
			PlayerPrefs.securityModeEnabled = enabled;
		}

		// Token: 0x0600535F RID: 21343 RVA: 0x001755A8 File Offset: 0x001739A8
		public static void Flush()
		{
			if (PlayerPrefs.hashTableChanged)
			{
				PlayerPrefs.Serialize();
				string value = (!PlayerPrefs.securityModeEnabled) ? PlayerPrefs.serializedOutput : PlayerPrefs.Encrypt(PlayerPrefs.serializedOutput);
				StreamWriter streamWriter = File.CreateText((!PlayerPrefs.securityModeEnabled) ? PlayerPrefs.fileName : PlayerPrefs.secureFileName);
				File.Delete((!PlayerPrefs.securityModeEnabled) ? PlayerPrefs.secureFileName : PlayerPrefs.fileName);
				if (streamWriter == null)
				{
					Debug.LogWarning("PlayerPrefs::Flush() opening file for writing failed: " + PlayerPrefs.fileName);
					return;
				}
				streamWriter.Write(value);
				streamWriter.Close();
				PlayerPrefs.serializedOutput = string.Empty;
			}
		}

		// Token: 0x06005360 RID: 21344 RVA: 0x00175654 File Offset: 0x00173A54
		private static void Serialize()
		{
			IDictionaryEnumerator enumerator = PlayerPrefs.playerPrefsHashtable.GetEnumerator();
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = true;
			while (enumerator.MoveNext())
			{
				if (!flag)
				{
					stringBuilder.Append(" ");
					stringBuilder.Append(";");
					stringBuilder.Append(" ");
				}
				stringBuilder.Append(PlayerPrefs.EscapeNonSeperators(enumerator.Key.ToString(), PlayerPrefs.seperators));
				stringBuilder.Append(" ");
				stringBuilder.Append(":");
				stringBuilder.Append(" ");
				stringBuilder.Append(PlayerPrefs.EscapeNonSeperators(enumerator.Value.ToString(), PlayerPrefs.seperators));
				stringBuilder.Append(" ");
				stringBuilder.Append(":");
				stringBuilder.Append(" ");
				stringBuilder.Append(enumerator.Value.GetType());
				flag = false;
			}
			PlayerPrefs.serializedOutput = stringBuilder.ToString();
		}

		// Token: 0x06005361 RID: 21345 RVA: 0x00175750 File Offset: 0x00173B50
		private static void Deserialize()
		{
			string[] array = PlayerPrefs.serializedInput.Split(new string[]
			{
				" ; "
			}, StringSplitOptions.RemoveEmptyEntries);
			foreach (string text in array)
			{
				string[] array3 = text.Split(new string[]
				{
					" : "
				}, StringSplitOptions.None);
				PlayerPrefs.playerPrefsHashtable.Add(PlayerPrefs.DeEscapeNonSeperators(array3[0], PlayerPrefs.seperators), PlayerPrefs.GetTypeValue(array3[2], PlayerPrefs.DeEscapeNonSeperators(array3[1], PlayerPrefs.seperators)));
				if (array3.Length > 3)
				{
					Debug.LogWarning("PlayerPrefs::Deserialize() parameterContent has " + array3.Length + " elements");
				}
			}
		}

		// Token: 0x06005362 RID: 21346 RVA: 0x00175800 File Offset: 0x00173C00
		public static string EscapeNonSeperators(string inputToEscape, string[] seperators)
		{
			inputToEscape = inputToEscape.Replace("\\", "\\\\");
			for (int i = 0; i < seperators.Length; i++)
			{
				inputToEscape = inputToEscape.Replace(seperators[i], "\\" + seperators[i]);
			}
			return inputToEscape;
		}

		// Token: 0x06005363 RID: 21347 RVA: 0x0017584C File Offset: 0x00173C4C
		public static string DeEscapeNonSeperators(string inputToDeEscape, string[] seperators)
		{
			for (int i = 0; i < seperators.Length; i++)
			{
				inputToDeEscape = inputToDeEscape.Replace("\\" + seperators[i], seperators[i]);
			}
			inputToDeEscape = inputToDeEscape.Replace("\\\\", "\\");
			return inputToDeEscape;
		}

		// Token: 0x06005364 RID: 21348 RVA: 0x00175898 File Offset: 0x00173C98
		private static string Encrypt(string originalString)
		{
			if (string.IsNullOrEmpty(originalString))
			{
				return string.Empty;
			}
			string result = string.Empty;
			using (DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider())
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateEncryptor(PlayerPrefs.bytes, PlayerPrefs.bytes), CryptoStreamMode.Write))
					{
						using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
						{
							streamWriter.Write(originalString);
							streamWriter.Flush();
							cryptoStream.FlushFinalBlock();
							streamWriter.Flush();
							result = Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06005365 RID: 21349 RVA: 0x00175998 File Offset: 0x00173D98
		private static string Decrypt(string cryptedString)
		{
			if (string.IsNullOrEmpty(cryptedString))
			{
				return string.Empty;
			}
			string result = string.Empty;
			using (DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider())
			{
				using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString)))
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateDecryptor(PlayerPrefs.bytes, PlayerPrefs.bytes), CryptoStreamMode.Read))
					{
						using (StreamReader streamReader = new StreamReader(cryptoStream))
						{
							result = streamReader.ReadToEnd();
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06005366 RID: 21350 RVA: 0x00175A78 File Offset: 0x00173E78
		private static object GetTypeValue(string typeName, string value)
		{
			if (typeName == "System.String")
			{
				return value.ToString();
			}
			if (typeName == "System.Int32")
			{
				return Convert.ToInt32(value);
			}
			if (typeName == "System.Boolean")
			{
				return Convert.ToBoolean(value);
			}
			if (typeName == "System.Single")
			{
				return Convert.ToSingle(value);
			}
			if (typeName == "System.Int64")
			{
				return Convert.ToInt64(value);
			}
			Debug.LogError("Unsupported type: " + typeName);
			return null;
		}

		// Token: 0x0400544F RID: 21583
		private static readonly Hashtable playerPrefsHashtable = new Hashtable();

		// Token: 0x04005450 RID: 21584
		private static bool hashTableChanged = false;

		// Token: 0x04005451 RID: 21585
		private static string serializedOutput = string.Empty;

		// Token: 0x04005452 RID: 21586
		private static string serializedInput = string.Empty;

		// Token: 0x04005453 RID: 21587
		private const string PARAMETERS_SEPERATOR = ";";

		// Token: 0x04005454 RID: 21588
		private const string KEY_VALUE_SEPERATOR = ":";

		// Token: 0x04005455 RID: 21589
		private static string[] seperators = new string[]
		{
			";",
			":"
		};

		// Token: 0x04005456 RID: 21590
		private static readonly string fileName = Application.persistentDataPath + "/PlayerPrefs.txt";

		// Token: 0x04005457 RID: 21591
		private static readonly string secureFileName = Application.persistentDataPath + "/AdvancedPlayerPrefs.txt";

		// Token: 0x04005458 RID: 21592
		private static byte[] bytes = Encoding.ASCII.GetBytes("iw3q" + SystemInfo.deviceUniqueIdentifier.Substring(0, 4));

		// Token: 0x04005459 RID: 21593
		private static bool wasEncrypted = false;

		// Token: 0x0400545A RID: 21594
		private static bool securityModeEnabled = false;
	}
}
