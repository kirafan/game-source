﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020002F8 RID: 760
public static class GameObject_Ext
{
	// Token: 0x06000ED9 RID: 3801 RVA: 0x0004FD30 File Offset: 0x0004E130
	public static void RemoveComponent<T>(this GameObject self) where T : Component
	{
		UnityEngine.Object.Destroy(self.GetComponent<T>());
	}

	// Token: 0x06000EDA RID: 3802 RVA: 0x0004FD44 File Offset: 0x0004E144
	public static void SetLayer(this GameObject gameObject, int layerNo, bool needSetChildrens = true)
	{
		if (gameObject == null)
		{
			return;
		}
		gameObject.layer = layerNo;
		if (!needSetChildrens)
		{
			return;
		}
		IEnumerator enumerator = gameObject.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				Transform transform = (Transform)obj;
				transform.gameObject.SetLayer(layerNo, needSetChildrens);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
