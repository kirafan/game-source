﻿using System;
using System.Collections.Generic;
using FieldPartyMemberResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BF9 RID: 3065
public static class FieldPartyMemberResponse
{
	// Token: 0x06003F5A RID: 16218 RVA: 0x0013C430 File Offset: 0x0013A830
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F5B RID: 16219 RVA: 0x0013C448 File Offset: 0x0013A848
	public static AddAll AddAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<AddAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F5C RID: 16220 RVA: 0x0013C460 File Offset: 0x0013A860
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F5D RID: 16221 RVA: 0x0013C478 File Offset: 0x0013A878
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F5E RID: 16222 RVA: 0x0013C490 File Offset: 0x0013A890
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F5F RID: 16223 RVA: 0x0013C4A8 File Offset: 0x0013A8A8
	public static SetAll SetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<SetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F60 RID: 16224 RVA: 0x0013C4C0 File Offset: 0x0013A8C0
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Remove>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F61 RID: 16225 RVA: 0x0013C4D8 File Offset: 0x0013A8D8
	public static RemoveAll RemoveAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<RemoveAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F62 RID: 16226 RVA: 0x0013C4F0 File Offset: 0x0013A8F0
	public static Changeschedule Changeschedule(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Changeschedule>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F63 RID: 16227 RVA: 0x0013C508 File Offset: 0x0013A908
	public static ChangescheduleAll ChangescheduleAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<ChangescheduleAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F64 RID: 16228 RVA: 0x0013C520 File Offset: 0x0013A920
	public static Itemup Itemup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Itemup>(param, dialogType, acceptableResultCodes);
	}
}
