﻿using System;

// Token: 0x02000071 RID: 113
public enum eADVSidePosition
{
	// Token: 0x040001DB RID: 475
	LeftSide,
	// Token: 0x040001DC RID: 476
	RightSide
}
