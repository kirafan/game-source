﻿using System;
using WWWTypes;

namespace CommonResponseTypes
{
	// Token: 0x02000C15 RID: 3093
	public class CommonResponse
	{
		// Token: 0x06003FE2 RID: 16354 RVA: 0x0013D2D7 File Offset: 0x0013B6D7
		public ResultCode GetResult()
		{
			return (ResultCode)this.resultCode;
		}

		// Token: 0x06003FE3 RID: 16355 RVA: 0x0013D2E0 File Offset: 0x0013B6E0
		public virtual string ToMessage()
		{
			string str = string.Empty;
			str += this.resultCode.ToString();
			str += this.serverTime.ToString();
			str += this.serverVersion.ToString();
			return str + ((this.message == null) ? string.Empty : this.message.ToString());
		}

		// Token: 0x040045D2 RID: 17874
		public int resultCode;

		// Token: 0x040045D3 RID: 17875
		public DateTime serverTime;

		// Token: 0x040045D4 RID: 17876
		public int serverVersion;

		// Token: 0x040045D5 RID: 17877
		public string message;
	}
}
