﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace FriendResponseTypes
{
	// Token: 0x02000C27 RID: 3111
	public class GetAll : CommonResponse
	{
		// Token: 0x06004007 RID: 16391 RVA: 0x0013D824 File Offset: 0x0013BC24
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.friends == null) ? string.Empty : this.friends.ToString());
			return str + ((this.guests == null) ? string.Empty : this.guests.ToString());
		}

		// Token: 0x040045DF RID: 17887
		public FriendList[] friends;

		// Token: 0x040045E0 RID: 17888
		public FriendList[] guests;
	}
}
