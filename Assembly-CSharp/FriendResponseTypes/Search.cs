﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace FriendResponseTypes
{
	// Token: 0x02000C26 RID: 3110
	public class Search : CommonResponse
	{
		// Token: 0x06004005 RID: 16389 RVA: 0x0013D7E0 File Offset: 0x0013BBE0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.friend == null) ? string.Empty : this.friend.ToString());
		}

		// Token: 0x040045DE RID: 17886
		public FriendList friend;
	}
}
