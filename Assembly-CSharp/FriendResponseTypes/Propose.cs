﻿using System;
using CommonResponseTypes;

namespace FriendResponseTypes
{
	// Token: 0x02000C21 RID: 3105
	public class Propose : CommonResponse
	{
		// Token: 0x06003FFB RID: 16379 RVA: 0x0013D728 File Offset: 0x0013BB28
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.managedFriendId.ToString();
		}

		// Token: 0x040045DD RID: 17885
		public long managedFriendId;
	}
}
