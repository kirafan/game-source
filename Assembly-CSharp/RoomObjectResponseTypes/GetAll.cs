﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C68 RID: 3176
	public class GetAll : CommonResponse
	{
		// Token: 0x06004089 RID: 16521 RVA: 0x0013F380 File Offset: 0x0013D780
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedRoomObjects == null) ? string.Empty : this.managedRoomObjects.ToString());
		}

		// Token: 0x0400466C RID: 18028
		public PlayerRoomObject[] managedRoomObjects;
	}
}
