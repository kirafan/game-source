﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C6B RID: 3179
	public class GetList : CommonResponse
	{
		// Token: 0x0600408F RID: 16527 RVA: 0x0013F450 File Offset: 0x0013D850
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.roomObjects == null) ? string.Empty : this.roomObjects.ToString());
		}

		// Token: 0x0400466F RID: 18031
		public RoomObject[] roomObjects;
	}
}
