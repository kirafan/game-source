﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C67 RID: 3175
	public class Buy : CommonResponse
	{
		// Token: 0x06004087 RID: 16519 RVA: 0x0013F2EC File Offset: 0x0013D6EC
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.managedRoomObjectIds == null) ? string.Empty : this.managedRoomObjectIds.ToString());
			str += ((this.managedRoomObjects == null) ? string.Empty : this.managedRoomObjects.ToString());
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004669 RID: 18025
		public string managedRoomObjectIds;

		// Token: 0x0400466A RID: 18026
		public PlayerRoomObject[] managedRoomObjects;

		// Token: 0x0400466B RID: 18027
		public Player player;
	}
}
