﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C6C RID: 3180
	public class Limitadd : CommonResponse
	{
		// Token: 0x06004091 RID: 16529 RVA: 0x0013F494 File Offset: 0x0013D894
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004670 RID: 18032
		public Player player;
	}
}
