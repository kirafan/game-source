﻿using System;
using CommonResponseTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C66 RID: 3174
	public class Add : CommonResponse
	{
		// Token: 0x06004085 RID: 16517 RVA: 0x0013F2A8 File Offset: 0x0013D6A8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedRoomObjectIds == null) ? string.Empty : this.managedRoomObjectIds.ToString());
		}

		// Token: 0x04004668 RID: 18024
		public string managedRoomObjectIds;
	}
}
