﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes
{
	// Token: 0x02000C6A RID: 3178
	public class Sale : CommonResponse
	{
		// Token: 0x0600408D RID: 16525 RVA: 0x0013F3E4 File Offset: 0x0013D7E4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.managedRoomObjects == null) ? string.Empty : this.managedRoomObjects.ToString());
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x0400466D RID: 18029
		public PlayerRoomObject[] managedRoomObjects;

		// Token: 0x0400466E RID: 18030
		public Player player;
	}
}
