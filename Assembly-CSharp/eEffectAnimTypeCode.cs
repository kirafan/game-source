﻿using System;

// Token: 0x02000E90 RID: 3728
public enum eEffectAnimTypeCode
{
	// Token: 0x04004DDF RID: 19935
	Invalid = -1,
	// Token: 0x04004DE0 RID: 19936
	Bool,
	// Token: 0x04004DE1 RID: 19937
	Int,
	// Token: 0x04004DE2 RID: 19938
	Long,
	// Token: 0x04004DE3 RID: 19939
	Float,
	// Token: 0x04004DE4 RID: 19940
	Vector2,
	// Token: 0x04004DE5 RID: 19941
	Vector3,
	// Token: 0x04004DE6 RID: 19942
	Vector4,
	// Token: 0x04004DE7 RID: 19943
	Color,
	// Token: 0x04004DE8 RID: 19944
	Rect,
	// Token: 0x04004DE9 RID: 19945
	S32,
	// Token: 0x04004DEA RID: 19946
	F32,
	// Token: 0x04004DEB RID: 19947
	Float2,
	// Token: 0x04004DEC RID: 19948
	Float3,
	// Token: 0x04004DED RID: 19949
	Float4,
	// Token: 0x04004DEE RID: 19950
	RGBA,
	// Token: 0x04004DEF RID: 19951
	RangePareFloat,
	// Token: 0x04004DF0 RID: 19952
	RangePareInt,
	// Token: 0x04004DF1 RID: 19953
	RangePareVector2,
	// Token: 0x04004DF2 RID: 19954
	RangePareVector3,
	// Token: 0x04004DF3 RID: 19955
	RangePareVector4,
	// Token: 0x04004DF4 RID: 19956
	RangePareColor,
	// Token: 0x04004DF5 RID: 19957
	RangeValueInt,
	// Token: 0x04004DF6 RID: 19958
	RangeValueFloat,
	// Token: 0x04004DF7 RID: 19959
	RangeValueVector2,
	// Token: 0x04004DF8 RID: 19960
	RangeValueVector3,
	// Token: 0x04004DF9 RID: 19961
	RangeValueVector4,
	// Token: 0x04004DFA RID: 19962
	RangeValueColor,
	// Token: 0x04004DFB RID: 19963
	Enum,
	// Token: 0x04004DFC RID: 19964
	Max
}
