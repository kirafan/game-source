﻿using System;
using System.Collections.Generic;
using Meige;
using QuestResponseTypes;
using WWWTypes;

// Token: 0x02000C05 RID: 3077
public static class QuestResponse
{
	// Token: 0x06003FA9 RID: 16297 RVA: 0x0013CB98 File Offset: 0x0013AF98
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}
}
