﻿using System;
using UnityEngine;

// Token: 0x02000FC7 RID: 4039
[Serializable]
public class MsbWorkInspector : MonoBehaviour
{
	// Token: 0x06005428 RID: 21544 RVA: 0x0017960A File Offset: 0x00177A0A
	private void Awake()
	{
		this.m_ObjWorkInfoArray = null;
		this.m_MatWorkInfoArray = null;
		this.m_CamWorkInfoArray = null;
	}

	// Token: 0x06005429 RID: 21545 RVA: 0x00179624 File Offset: 0x00177A24
	public void InitObjectWorkInfoArray(int num)
	{
		this.m_ObjWorkInfoArray = new MsbWorkInspector.MsbObjectWorkInfo[num];
		for (int i = 0; i < num; i++)
		{
			this.m_ObjWorkInfoArray[i] = new MsbWorkInspector.MsbObjectWorkInfo();
			this.m_ObjWorkInfoArray[i].m_SrcHndl = null;
			this.m_ObjWorkInfoArray[i].m_Work = null;
		}
	}

	// Token: 0x0600542A RID: 21546 RVA: 0x00179678 File Offset: 0x00177A78
	public void InitMaterialWorkArray(int num)
	{
		this.m_MatWorkInfoArray = new MsbWorkInspector.MsbMaterialWorkInfo[num];
		for (int i = 0; i < num; i++)
		{
			this.m_MatWorkInfoArray[i] = new MsbWorkInspector.MsbMaterialWorkInfo();
			this.m_MatWorkInfoArray[i].m_SrcHndl = null;
			this.m_MatWorkInfoArray[i].m_Work = null;
		}
	}

	// Token: 0x0600542B RID: 21547 RVA: 0x001796CC File Offset: 0x00177ACC
	public void InitCameraWorkArray(int num)
	{
		this.m_CamWorkInfoArray = new MsbWorkInspector.MsbCameraWorkInfo[num];
		for (int i = 0; i < num; i++)
		{
			this.m_CamWorkInfoArray[i] = new MsbWorkInspector.MsbCameraWorkInfo();
			this.m_CamWorkInfoArray[i].m_SrcHndl = null;
			this.m_CamWorkInfoArray[i].m_Work = null;
		}
	}

	// Token: 0x04005521 RID: 21793
	public MsbWorkInspector.MsbObjectWorkInfo[] m_ObjWorkInfoArray;

	// Token: 0x04005522 RID: 21794
	public MsbWorkInspector.MsbMaterialWorkInfo[] m_MatWorkInfoArray;

	// Token: 0x04005523 RID: 21795
	public MsbWorkInspector.MsbCameraWorkInfo[] m_CamWorkInfoArray;

	// Token: 0x02000FC8 RID: 4040
	[Serializable]
	public class MsbObjectWorkInfo
	{
		// Token: 0x04005524 RID: 21796
		public string m_Name;

		// Token: 0x04005525 RID: 21797
		public MsbObjectHandler m_SrcHndl;

		// Token: 0x04005526 RID: 21798
		public MsbObjectHandler.MsbObjectParam m_Work;
	}

	// Token: 0x02000FC9 RID: 4041
	[Serializable]
	public class MsbMaterialWorkInfo
	{
		// Token: 0x04005527 RID: 21799
		public string m_Name;

		// Token: 0x04005528 RID: 21800
		public MsbMaterialHandler m_SrcHndl;

		// Token: 0x04005529 RID: 21801
		public MsbMaterialHandler.MsbMaterialParam m_Work;
	}

	// Token: 0x02000FCA RID: 4042
	[Serializable]
	public class MsbCameraWorkInfo
	{
		// Token: 0x0400552A RID: 21802
		public string m_Name;

		// Token: 0x0400552B RID: 21803
		public MsbCameraHandler m_SrcHndl;

		// Token: 0x0400552C RID: 21804
		public MsbCameraHandler.MsbCameraParam m_Work;
	}
}
