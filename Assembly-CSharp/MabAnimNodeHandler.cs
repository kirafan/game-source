﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000F57 RID: 3927
[Serializable]
public class MabAnimNodeHandler
{
	// Token: 0x060051AA RID: 20906 RVA: 0x0016D9CD File Offset: 0x0016BDCD
	public void MakeTargetInfoForHierarchy(eAnimTargetType type, string name, int refID, int hieIndex)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = hieIndex;
	}

	// Token: 0x060051AB RID: 20907 RVA: 0x0016DA0B File Offset: 0x0016BE0B
	public void MakeTargetInfoForMesh(eAnimTargetType type, string name, int refID, int msbObjIndex)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = msbObjIndex;
	}

	// Token: 0x060051AC RID: 20908 RVA: 0x0016DA49 File Offset: 0x0016BE49
	public void MakeTargetInfoForMaterial(eAnimTargetType type, string name, int refID, int msbMatIndex)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = msbMatIndex;
	}

	// Token: 0x060051AD RID: 20909 RVA: 0x0016DA88 File Offset: 0x0016BE88
	public void MakeTargetInfoForTexture(eAnimTargetType type, string name, int refID, int msbMatIndex, eTextureType texType, int layerIdx)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = msbMatIndex;
		this.m_Target.m_Param1 = (int)texType;
		this.m_Target.m_Param2 = layerIdx;
	}

	// Token: 0x060051AE RID: 20910 RVA: 0x0016DAEB File Offset: 0x0016BEEB
	public void MakeTargetInfoForCamera(eAnimTargetType type, string name, int refID, int msbCamIndex)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = msbCamIndex;
	}

	// Token: 0x060051AF RID: 20911 RVA: 0x0016DB29 File Offset: 0x0016BF29
	public void MakeTargetInfoForParticle(eAnimTargetType type, string name, int refID, int peIndex)
	{
		this.m_Target = new MabAnimNodeHandler.AnimTargetInfo();
		this.m_Target.m_TargetType = type;
		this.m_Target.m_TargetName = name;
		this.m_Target.m_RefID = refID;
		this.m_Target.m_Param0 = peIndex;
	}

	// Token: 0x060051B0 RID: 20912 RVA: 0x0016DB68 File Offset: 0x0016BF68
	public void Setup(ref MeigeAnimCtrl.TargetInstance tgtCache, MsbHandler msbHndl)
	{
		tgtCache.Init();
		eAnimTargetType targetType = this.m_Target.m_TargetType;
		switch (targetType)
		{
		case eAnimTargetType.eAnimTarget_MatColor_R:
		case eAnimTargetType.eAnimTarget_MatColor_G:
		case eAnimTargetType.eAnimTarget_MatColor_B:
		case eAnimTargetType.eAnimTarget_MatColor_A:
			goto IL_B9;
		case eAnimTargetType.eAnimTarget_TexCoverageUV_U:
		case eAnimTargetType.eAnimTarget_TexCoverageUV_V:
		case eAnimTargetType.eAnimTarget_TexTranslationUV_U:
		case eAnimTargetType.eAnimTarget_TexTranslationUV_V:
		case eAnimTargetType.eAnimTarget_TexOffsetUV_U:
		case eAnimTargetType.eAnimTarget_TexOffsetUV_V:
			goto IL_E2;
		case eAnimTargetType.eAnimTarget_MeshColor_R:
		case eAnimTargetType.eAnimTarget_MeshColor_G:
		case eAnimTargetType.eAnimTarget_MeshColor_B:
		case eAnimTargetType.eAnimTarget_MeshColor_A:
			break;
		default:
			switch (targetType)
			{
			case eAnimTargetType.eAnimTarget_MatColor:
				goto IL_B9;
			case eAnimTargetType.eAnimTarget_TexCoverageUV:
			case eAnimTargetType.eAnimTarget_TexTranslationUV:
			case eAnimTargetType.eAnimTarget_TexRotateUV:
			case eAnimTargetType.eAnimTarget_TexOffsetUV:
				goto IL_E2;
			case eAnimTargetType.eAnimTarget_FocalLength:
				break;
			case eAnimTargetType.eAnimTarget_MeshVisibility:
			case eAnimTargetType.eAnimTarget_MeshColor:
				goto IL_90;
			default:
				if (targetType != eAnimTargetType.eAnimTarget_CamOrthographicSize)
				{
					return;
				}
				break;
			case eAnimTargetType.eAnimTarget_PEActive:
				if (msbHndl.GetParticleEmitterNum() > this.m_Target.m_Param0)
				{
					tgtCache.m_MeigeParticleEmitterWork = msbHndl.GetParticleEmitter(this.m_Target.m_Param0);
				}
				return;
			}
			if (msbHndl.GetMsbCameraHandlerNum() > 0)
			{
				MsbCameraHandler msbCameraHandler = msbHndl.GetMsbCameraHandler(0);
				if (msbCameraHandler != null)
				{
					tgtCache.m_MsbCameraWork = msbCameraHandler.GetWork();
				}
			}
			return;
		}
		IL_90:
		MsbObjectHandler msbObjectHandlerByName = msbHndl.GetMsbObjectHandlerByName(this.m_Target.m_TargetName);
		if (msbObjectHandlerByName != null)
		{
			tgtCache.m_MsbObjWork = msbObjectHandlerByName.GetWork();
		}
		return;
		IL_B9:
		MsbMaterialHandler msbMaterialHandlerByName = msbHndl.GetMsbMaterialHandlerByName(this.m_Target.m_TargetName);
		if (msbMaterialHandlerByName != null)
		{
			tgtCache.m_MsbMatWork = msbMaterialHandlerByName.GetWork();
		}
		return;
		IL_E2:
		MsbMaterialHandler msbMaterialHandlerByName2 = msbHndl.GetMsbMaterialHandlerByName(this.m_Target.m_TargetName);
		if (msbMaterialHandlerByName2 != null)
		{
			int num = msbMaterialHandlerByName2.m_Src.SearchTexIndex((eTextureType)this.m_Target.m_Param1, this.m_Target.m_Param2);
			if (num != -1)
			{
				tgtCache.m_MsbTextureWork = msbMaterialHandlerByName2.GetWork().m_Texture[num];
			}
		}
	}

	// Token: 0x060051B1 RID: 20913 RVA: 0x0016DD18 File Offset: 0x0016C118
	public void Animate(ref MeigeAnimCtrl.TargetInstance tgtCache, float frame)
	{
		int num = 0;
		for (int i = 0; i < this.m_Curves.Length; i++)
		{
			for (int j = 0; j < this.m_Curves[i].m_ComponentCurves.Length; j++)
			{
				MabAnimNodeHandler.MabCurve mabCurve = this.m_Curves[i].m_ComponentCurves[j];
				float num2 = mabCurve.CalcValue(frame);
				eAnimTargetType targetType = this.m_Target.m_TargetType;
				switch (targetType)
				{
				case eAnimTargetType.eAnimTarget_MatColor_R:
					if (tgtCache.m_MsbMatWork != null)
					{
						tgtCache.m_MsbMatWork.m_Diffuse.r = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MatColor_G:
					if (tgtCache.m_MsbMatWork != null)
					{
						tgtCache.m_MsbMatWork.m_Diffuse.g = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MatColor_B:
					if (tgtCache.m_MsbMatWork != null)
					{
						tgtCache.m_MsbMatWork.m_Diffuse.b = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MatColor_A:
					if (tgtCache.m_MsbMatWork != null)
					{
						tgtCache.m_MsbMatWork.m_Diffuse.a = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexCoverageUV_U:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_CoverageUV.x = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexCoverageUV_V:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_CoverageUV.y = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexTranslationUV_U:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_TranslationUV.x = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexTranslationUV_V:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_TranslationUV.y = -num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexOffsetUV_U:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_OffsetUV.x = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_TexOffsetUV_V:
					if (tgtCache.m_MsbTextureWork != null)
					{
						tgtCache.m_MsbTextureWork.m_OffsetUV.y = -num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MeshColor_R:
					if (tgtCache.m_MsbObjWork != null)
					{
						tgtCache.m_MsbObjWork.m_MeshColor.r = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MeshColor_G:
					if (tgtCache.m_MsbObjWork != null)
					{
						tgtCache.m_MsbObjWork.m_MeshColor.g = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MeshColor_B:
					if (tgtCache.m_MsbObjWork != null)
					{
						tgtCache.m_MsbObjWork.m_MeshColor.b = num2;
					}
					break;
				case eAnimTargetType.eAnimTarget_MeshColor_A:
					if (tgtCache.m_MsbObjWork != null)
					{
						tgtCache.m_MsbObjWork.m_MeshColor.a = num2;
					}
					break;
				default:
					switch (targetType)
					{
					case eAnimTargetType.eAnimTarget_MatColor:
						if (tgtCache.m_MsbMatWork != null)
						{
							tgtCache.m_MsbMatWork.m_Diffuse[num] = num2;
						}
						break;
					case eAnimTargetType.eAnimTarget_TexCoverageUV:
						if (tgtCache.m_MsbTextureWork != null)
						{
							tgtCache.m_MsbTextureWork.m_CoverageUV[num] = num2;
						}
						break;
					case eAnimTargetType.eAnimTarget_TexTranslationUV:
						if (tgtCache.m_MsbTextureWork != null)
						{
							tgtCache.m_MsbTextureWork.m_TranslationUV[num] = ((num != 1) ? num2 : (-num2));
						}
						break;
					case eAnimTargetType.eAnimTarget_TexRotateUV:
						if (tgtCache.m_MsbTextureWork != null)
						{
							tgtCache.m_MsbTextureWork.m_RotateUV = num2;
						}
						break;
					case eAnimTargetType.eAnimTarget_TexOffsetUV:
						if (tgtCache.m_MsbTextureWork != null)
						{
							tgtCache.m_MsbTextureWork.m_OffsetUV[num] = ((num != 1) ? num2 : (-num2));
						}
						break;
					case eAnimTargetType.eAnimTarget_FocalLength:
						if (tgtCache.m_MsbCameraWork != null)
						{
							tgtCache.m_MsbCameraWork.m_FocalLength = num2;
						}
						break;
					case eAnimTargetType.eAnimTarget_MeshVisibility:
						if (tgtCache.m_MsbObjWork != null)
						{
							tgtCache.m_MsbObjWork.m_bVisibility = ((int)num2 == 1);
						}
						break;
					default:
						if (targetType == eAnimTargetType.eAnimTarget_CamOrthographicSize)
						{
							if (tgtCache.m_MsbCameraWork != null)
							{
								tgtCache.m_MsbCameraWork.m_OrthographicsSize = num2;
							}
						}
						break;
					case eAnimTargetType.eAnimTarget_PEActive:
						if (tgtCache.m_MeigeParticleEmitterWork != null)
						{
							tgtCache.m_MeigeParticleEmitterWork.isActive = ((int)num2 == 1);
						}
						break;
					case eAnimTargetType.eAnimTarget_MeshColor:
						if (tgtCache.m_MsbObjWork != null)
						{
							tgtCache.m_MsbObjWork.m_MeshColor[num] = num2;
						}
						break;
					}
					break;
				}
				num++;
			}
		}
	}

	// Token: 0x040050B6 RID: 20662
	public MabAnimNodeHandler.AnimTargetInfo m_Target;

	// Token: 0x040050B7 RID: 20663
	public MabAnimNodeHandler.AnimNodeCurve[] m_Curves;

	// Token: 0x02000F58 RID: 3928
	[Serializable]
	public class MabKeyData
	{
		// Token: 0x040050B8 RID: 20664
		public eAnimControlType m_CtrlType;

		// Token: 0x040050B9 RID: 20665
		public float m_Frame;

		// Token: 0x040050BA RID: 20666
		public float m_Value;

		// Token: 0x040050BB RID: 20667
		public float m_LeftDerivative;

		// Token: 0x040050BC RID: 20668
		public float m_RightDerivative;
	}

	// Token: 0x02000F59 RID: 3929
	[Serializable]
	public class MabCurve
	{
		// Token: 0x060051B4 RID: 20916 RVA: 0x0016E168 File Offset: 0x0016C568
		private void UpdateProcessIndex(float keyframe)
		{
			float frame = this.m_KeyDatas[this.m_KeyDatas.Length - 1].m_Frame;
			while (keyframe < 0f)
			{
				keyframe += frame;
			}
			while (keyframe > frame)
			{
				keyframe -= frame;
			}
			if (this.m_KeyDatas[this.m_ProcessIdx].m_Frame > keyframe)
			{
				int i;
				for (i = this.m_ProcessIdx - 1; i > 0; i--)
				{
					if (this.m_KeyDatas[i].m_Frame <= keyframe)
					{
						break;
					}
				}
				this.m_ProcessIdx = i;
			}
			else if (this.m_KeyDatas[this.m_ProcessIdx + 1].m_Frame < keyframe)
			{
				int j;
				for (j = this.m_ProcessIdx + 1; j < this.m_KeyDatas.Length - 1; j++)
				{
					if (this.m_KeyDatas[j].m_Frame > keyframe)
					{
						break;
					}
				}
				this.m_ProcessIdx = j - 1;
			}
		}

		// Token: 0x060051B5 RID: 20917 RVA: 0x0016E264 File Offset: 0x0016C664
		public float CalcValue(float keyframe)
		{
			if (this.m_KeyDatas.Length <= 1)
			{
				return this.m_KeyDatas[0].m_Value;
			}
			this.UpdateProcessIndex(keyframe);
			MabAnimNodeHandler.MabKeyData mabKeyData = this.m_KeyDatas[this.m_ProcessIdx];
			MabAnimNodeHandler.MabKeyData mabKeyData2 = this.m_KeyDatas[this.m_ProcessIdx + 1];
			if (mabKeyData.m_CtrlType == eAnimControlType.eAnimControl_Bool || mabKeyData.m_CtrlType == eAnimControlType.eAnimControl_Constant)
			{
				return mabKeyData.m_Value;
			}
			float t = (keyframe - mabKeyData.m_Frame) / (mabKeyData2.m_Frame - mabKeyData.m_Frame);
			float result;
			if (mabKeyData.m_CtrlType == eAnimControlType.eAnimControl_Linear)
			{
				result = Mathf.Lerp(mabKeyData.m_Value, mabKeyData2.m_Value, t);
			}
			else
			{
				result = MathFunc.Hermite_CalcValue(mabKeyData.m_Value, mabKeyData.m_RightDerivative, mabKeyData2.m_Value, mabKeyData2.m_LeftDerivative, t);
			}
			return result;
		}

		// Token: 0x040050BD RID: 20669
		public MabAnimNodeHandler.MabKeyData[] m_KeyDatas;

		// Token: 0x040050BE RID: 20670
		protected int m_ProcessIdx;
	}

	// Token: 0x02000F5A RID: 3930
	[Serializable]
	public class AnimNodeCurve
	{
		// Token: 0x040050BF RID: 20671
		public MabAnimNodeHandler.MabCurve[] m_ComponentCurves;
	}

	// Token: 0x02000F5B RID: 3931
	[Serializable]
	public class AnimTargetInfo
	{
		// Token: 0x040050C0 RID: 20672
		public eAnimTargetType m_TargetType;

		// Token: 0x040050C1 RID: 20673
		public string m_TargetName;

		// Token: 0x040050C2 RID: 20674
		public int m_RefID;

		// Token: 0x040050C3 RID: 20675
		public int m_Param0;

		// Token: 0x040050C4 RID: 20676
		public int m_Param1;

		// Token: 0x040050C5 RID: 20677
		public int m_Param2;
	}
}
