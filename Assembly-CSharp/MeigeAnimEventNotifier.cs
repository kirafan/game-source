﻿using System;
using UnityEngine;

// Token: 0x02000F64 RID: 3940
public class MeigeAnimEventNotifier : MonoBehaviour
{
	// Token: 0x060051CE RID: 20942 RVA: 0x0016EC54 File Offset: 0x0016D054
	public void Notify_MeigeAnimEvent(int ev_id)
	{
		if (this.m_NotifyFuncList == null)
		{
			return;
		}
		if (this.m_MabHandler == null)
		{
			return;
		}
		if (this.m_MabHandler.m_AnimEvArray == null)
		{
			return;
		}
		if (this.m_MabHandler.m_AnimEvArray.Length <= ev_id)
		{
			return;
		}
		this.m_NotifyFuncList(this.m_MabHandler.m_AnimEvArray[ev_id]);
	}

	// Token: 0x060051CF RID: 20943 RVA: 0x0016ECB6 File Offset: 0x0016D0B6
	public void UpdateAnimClip(MabHandler mabHndl)
	{
		this.m_MabHandler = mabHndl;
	}

	// Token: 0x060051D0 RID: 20944 RVA: 0x0016ECBF File Offset: 0x0016D0BF
	public void ClearNotify()
	{
		this.m_NotifyFuncList = null;
	}

	// Token: 0x060051D1 RID: 20945 RVA: 0x0016ECC8 File Offset: 0x0016D0C8
	public void AddNotify(MeigeAnimEventNotifier.OnEvent func)
	{
		this.m_NotifyFuncList = (MeigeAnimEventNotifier.OnEvent)Delegate.Combine(this.m_NotifyFuncList, func);
	}

	// Token: 0x060051D2 RID: 20946 RVA: 0x0016ECE1 File Offset: 0x0016D0E1
	public void RemoveNotify(MeigeAnimEventNotifier.OnEvent func)
	{
		this.m_NotifyFuncList = (MeigeAnimEventNotifier.OnEvent)Delegate.Remove(this.m_NotifyFuncList, func);
	}

	// Token: 0x040050EC RID: 20716
	private MeigeAnimEventNotifier.OnEvent m_NotifyFuncList;

	// Token: 0x040050ED RID: 20717
	private MabHandler m_MabHandler;

	// Token: 0x040050EE RID: 20718
	public const string NOTIFY_FUNCNAME = "Notify_MeigeAnimEvent";

	// Token: 0x02000F65 RID: 3941
	// (Invoke) Token: 0x060051D4 RID: 20948
	public delegate void OnEvent(MabHandler.MabAnimEvent ev);
}
