﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000E8F RID: 3727
public class EffectComponentBase : MonoBehaviour
{
	// Token: 0x1700051D RID: 1309
	// (get) Token: 0x06004D2D RID: 19757 RVA: 0x00158C2D File Offset: 0x0015702D
	// (set) Token: 0x06004D2C RID: 19756 RVA: 0x00158C24 File Offset: 0x00157024
	public virtual bool isAlive
	{
		get
		{
			return this.m_isAlive;
		}
		protected set
		{
			this.m_isAlive = value;
		}
	}

	// Token: 0x1700051E RID: 1310
	// (get) Token: 0x06004D2F RID: 19759 RVA: 0x00158C3E File Offset: 0x0015703E
	// (set) Token: 0x06004D2E RID: 19758 RVA: 0x00158C35 File Offset: 0x00157035
	public virtual bool isActive
	{
		get
		{
			return this.m_isActive;
		}
		set
		{
			this.m_isActive = value;
		}
	}

	// Token: 0x06004D30 RID: 19760 RVA: 0x00158C46 File Offset: 0x00157046
	private void OnApplicationQuit()
	{
	}

	// Token: 0x06004D31 RID: 19761 RVA: 0x00158C48 File Offset: 0x00157048
	public virtual int GetPropertyNum()
	{
		return 0;
	}

	// Token: 0x06004D32 RID: 19762 RVA: 0x00158C4B File Offset: 0x0015704B
	public virtual int GetArrayNum(int propertyIdx)
	{
		return 0;
	}

	// Token: 0x06004D33 RID: 19763 RVA: 0x00158C4E File Offset: 0x0015704E
	public virtual object GetValue(int propertyIdx, int arrayIdx)
	{
		return null;
	}

	// Token: 0x06004D34 RID: 19764 RVA: 0x00158C51 File Offset: 0x00157051
	public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
	{
	}

	// Token: 0x06004D35 RID: 19765 RVA: 0x00158C53 File Offset: 0x00157053
	public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value)
	{
	}

	// Token: 0x06004D36 RID: 19766 RVA: 0x00158C55 File Offset: 0x00157055
	public virtual eEffectAnimTypeCode GetTypeCode(int propertyIdx)
	{
		return eEffectAnimTypeCode.Invalid;
	}

	// Token: 0x06004D37 RID: 19767 RVA: 0x00158C58 File Offset: 0x00157058
	public virtual Type GetPropertType(int propertyIdx)
	{
		return typeof(object);
	}

	// Token: 0x06004D38 RID: 19768 RVA: 0x00158C64 File Offset: 0x00157064
	public virtual string GetPropertyName(int propertyIdx)
	{
		return null;
	}

	// Token: 0x06004D39 RID: 19769 RVA: 0x00158C67 File Offset: 0x00157067
	public virtual EffectRuleBase GetEffectRule()
	{
		return null;
	}

	// Token: 0x06004D3A RID: 19770 RVA: 0x00158C6A File Offset: 0x0015706A
	public EffectComponentBase Clone()
	{
		return (EffectComponentBase)base.MemberwiseClone();
	}

	// Token: 0x06004D3B RID: 19771 RVA: 0x00158C77 File Offset: 0x00157077
	public virtual int GetLocatorNum()
	{
		return 1;
	}

	// Token: 0x06004D3C RID: 19772 RVA: 0x00158C7C File Offset: 0x0015707C
	public void PrepareLocator()
	{
		if (this.m_Locators == null)
		{
			this.m_Locators = new EffectLocator[this.GetLocatorNum()];
		}
		else
		{
			int num = this.m_Locators.Length;
			if (num < this.GetLocatorNum())
			{
				EffectLocator[] array = new EffectLocator[this.GetLocatorNum()];
				for (int i = 0; i < num; i++)
				{
					array[i] = this.m_Locators[i];
				}
				this.m_Locators = array;
			}
		}
	}

	// Token: 0x06004D3D RID: 19773 RVA: 0x00158CEF File Offset: 0x001570EF
	public void SetLocator(int idx, EffectLocator locator)
	{
		this.m_Locators[idx] = locator;
	}

	// Token: 0x06004D3E RID: 19774 RVA: 0x00158CFA File Offset: 0x001570FA
	public EffectLocator GetLocator(int idx)
	{
		return this.m_Locators[idx];
	}

	// Token: 0x06004D3F RID: 19775 RVA: 0x00158D04 File Offset: 0x00157104
	public void SetLocatorsTransform(int idx, Transform trans)
	{
		this.m_LocatorTransforms[idx] = trans;
	}

	// Token: 0x06004D40 RID: 19776 RVA: 0x00158D0F File Offset: 0x0015710F
	public Transform GetLocatorsTransform(int idx)
	{
		return this.m_LocatorTransforms[idx];
	}

	// Token: 0x06004D41 RID: 19777 RVA: 0x00158D1C File Offset: 0x0015711C
	protected void InitLocator()
	{
		this.PrepareLocator();
		this.m_LocatorTransforms = new Transform[this.GetLocatorNum()];
		if (this.GetLocatorNum() > 1)
		{
			for (int i = 0; i < this.GetLocatorNum(); i++)
			{
				if (this.m_Locators[i] == null)
				{
					GameObject gameObject = new GameObject("LocatorObject[" + i.ToString() + "]");
					this.m_Locators[i] = gameObject.AddComponent<EffectLocator>();
					this.m_Locators[i].Reset();
					this.m_Locators[i].SetOwner(gameObject);
					this.m_LocatorTransforms[i] = this.m_Locators[i].transform;
					this.m_LocatorTransforms[i].parent = base.transform;
				}
				else
				{
					this.m_LocatorTransforms[i] = this.m_Locators[i].transform;
				}
			}
		}
		else
		{
			this.m_LocatorTransforms[0] = base.transform;
		}
	}

	// Token: 0x06004D42 RID: 19778 RVA: 0x00158E18 File Offset: 0x00157218
	protected void ReleaseLocator()
	{
		for (int i = 0; i < this.GetLocatorNum(); i++)
		{
			if (this.m_Locators[i] != null)
			{
				GameObject owner = this.m_Locators[i].GetOwner();
				if (owner != null)
				{
					this.m_Locators[i].SetOwner(null);
					UnityEngine.Object.Destroy(owner);
				}
			}
		}
	}

	// Token: 0x06004D43 RID: 19779 RVA: 0x00158E7D File Offset: 0x0015727D
	protected virtual void Init()
	{
		this.InitLocator();
	}

	// Token: 0x06004D44 RID: 19780 RVA: 0x00158E85 File Offset: 0x00157285
	protected virtual void Release()
	{
		this.ReleaseLocator();
	}

	// Token: 0x06004D45 RID: 19781 RVA: 0x00158E8D File Offset: 0x0015728D
	public virtual bool Shot()
	{
		return false;
	}

	// Token: 0x06004D46 RID: 19782 RVA: 0x00158E90 File Offset: 0x00157290
	public virtual void Activate(bool flg)
	{
		this.isActive = flg;
	}

	// Token: 0x06004D47 RID: 19783 RVA: 0x00158E99 File Offset: 0x00157299
	public virtual bool Kill()
	{
		return false;
	}

	// Token: 0x06004D48 RID: 19784 RVA: 0x00158E9C File Offset: 0x0015729C
	private void Awake()
	{
		this.Init();
	}

	// Token: 0x06004D49 RID: 19785 RVA: 0x00158EA4 File Offset: 0x001572A4
	private void OnDestroy()
	{
		this.Release();
	}

	// Token: 0x04004DD9 RID: 19929
	protected bool m_isAlive;

	// Token: 0x04004DDA RID: 19930
	[SerializeField]
	protected bool m_isActive;

	// Token: 0x04004DDB RID: 19931
	protected const int DEFAULT_LOCATOR_NUM = 1;

	// Token: 0x04004DDC RID: 19932
	public EffectLocator[] m_Locators;

	// Token: 0x04004DDD RID: 19933
	protected Transform[] m_LocatorTransforms;
}
