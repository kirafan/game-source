﻿using System;

// Token: 0x02000E94 RID: 3732
public interface IFieldAccessor
{
	// Token: 0x06004D52 RID: 19794
	object GetValue(object target);

	// Token: 0x06004D53 RID: 19795
	void SetValue(object target, object value);
}
