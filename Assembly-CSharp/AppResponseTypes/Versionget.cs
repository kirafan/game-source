﻿using System;
using CommonResponseTypes;

namespace AppResponseTypes
{
	// Token: 0x02000C12 RID: 3090
	public class Versionget : CommonResponse
	{
		// Token: 0x06003FDC RID: 16348 RVA: 0x0013D3C4 File Offset: 0x0013B7C4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.abVer.ToString();
			str += ((this.applyUrl == null) ? string.Empty : this.applyUrl.ToString());
			return str + ((this.applys3Url == null) ? string.Empty : this.applys3Url.ToString());
		}

		// Token: 0x040045CD RID: 17869
		public int abVer;

		// Token: 0x040045CE RID: 17870
		public string applyUrl;

		// Token: 0x040045CF RID: 17871
		public string applys3Url;
	}
}
