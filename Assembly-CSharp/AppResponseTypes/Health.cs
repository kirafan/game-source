﻿using System;
using CommonResponseTypes;

namespace AppResponseTypes
{
	// Token: 0x02000C11 RID: 3089
	public class Health : CommonResponse
	{
		// Token: 0x06003FDA RID: 16346 RVA: 0x0013D38C File Offset: 0x0013B78C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.dbTime.ToString();
		}

		// Token: 0x040045CC RID: 17868
		public long dbTime;
	}
}
