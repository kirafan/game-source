﻿using System;
using FriendRequestTypes;
using Meige;

// Token: 0x02000B5E RID: 2910
public static class FriendRequest
{
	// Token: 0x06003DEF RID: 15855 RVA: 0x00138924 File Offset: 0x00136D24
	public static MeigewwwParam Propose(Propose param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/propose", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("targetPlayerId", param.targetPlayerId);
		return meigewwwParam;
	}

	// Token: 0x06003DF0 RID: 15856 RVA: 0x0013895C File Offset: 0x00136D5C
	public static MeigewwwParam Propose(long targetPlayerId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/propose", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("targetPlayerId", targetPlayerId);
		return meigewwwParam;
	}

	// Token: 0x06003DF1 RID: 15857 RVA: 0x00138990 File Offset: 0x00136D90
	public static MeigewwwParam Accept(Accept param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/accept", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", param.managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF2 RID: 15858 RVA: 0x001389C8 File Offset: 0x00136DC8
	public static MeigewwwParam Accept(long managedFriendId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/accept", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF3 RID: 15859 RVA: 0x001389FC File Offset: 0x00136DFC
	public static MeigewwwParam Refuse(Refuse param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/refuse", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", param.managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF4 RID: 15860 RVA: 0x00138A34 File Offset: 0x00136E34
	public static MeigewwwParam Refuse(long managedFriendId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/refuse", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF5 RID: 15861 RVA: 0x00138A68 File Offset: 0x00136E68
	public static MeigewwwParam Cancel(Cancel param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/cancel", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", param.managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF6 RID: 15862 RVA: 0x00138AA0 File Offset: 0x00136EA0
	public static MeigewwwParam Cancel(long managedFriendId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/cancel", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF7 RID: 15863 RVA: 0x00138AD4 File Offset: 0x00136ED4
	public static MeigewwwParam Terminate(Terminate param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/terminate", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", param.managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF8 RID: 15864 RVA: 0x00138B0C File Offset: 0x00136F0C
	public static MeigewwwParam Terminate(long managedFriendId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/terminate", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedFriendId", managedFriendId);
		return meigewwwParam;
	}

	// Token: 0x06003DF9 RID: 15865 RVA: 0x00138B40 File Offset: 0x00136F40
	public static MeigewwwParam Search(Search param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/search", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("myCode", param.myCode);
		return meigewwwParam;
	}

	// Token: 0x06003DFA RID: 15866 RVA: 0x00138B74 File Offset: 0x00136F74
	public static MeigewwwParam Search(string myCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/search", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("myCode", myCode);
		return meigewwwParam;
	}

	// Token: 0x06003DFB RID: 15867 RVA: 0x00138BA4 File Offset: 0x00136FA4
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", param.type);
		return meigewwwParam;
	}

	// Token: 0x06003DFC RID: 15868 RVA: 0x00138BDC File Offset: 0x00136FDC
	public static MeigewwwParam GetAll(int type, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/friend/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", type);
		return meigewwwParam;
	}
}
