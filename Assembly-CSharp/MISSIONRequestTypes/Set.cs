﻿using System;
using WWWTypes;

namespace MISSIONRequestTypes
{
	// Token: 0x02000B94 RID: 2964
	public class Set
	{
		// Token: 0x04004530 RID: 17712
		public PlayerMissionLog[] missionLogs;
	}
}
