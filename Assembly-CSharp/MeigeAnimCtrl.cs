﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Meige;
using UnityEngine;

// Token: 0x02000F5F RID: 3935
public class MeigeAnimCtrl : MonoBehaviour
{
	// Token: 0x060051BC RID: 20924 RVA: 0x0016E386 File Offset: 0x0016C786
	public void Init()
	{
		this.m_AnimationPlayTime = 0f;
		this.m_PlayState = MeigeAnimCtrl.ePlayState.ePlayState_Invalid;
		this.m_TargetInstanceCache = null;
		this.m_CurAnim = null;
		this.m_ReferenceAnimHandlerDic = null;
	}

	// Token: 0x060051BD RID: 20925 RVA: 0x0016E3B0 File Offset: 0x0016C7B0
	public MeigeAnimCtrl.ReferenceAnimHandler GetReferenceAnimHandler(string clipName)
	{
		MeigeAnimCtrl.ReferenceAnimHandler result = null;
		if (this.m_ReferenceAnimHandlerDic.TryGetValue(clipName, out result))
		{
			return result;
		}
		return null;
	}

	// Token: 0x060051BE RID: 20926 RVA: 0x0016E3D5 File Offset: 0x0016C7D5
	public void EnableManualUpdate(bool b)
	{
		this.m_bManualUpdate = b;
	}

	// Token: 0x060051BF RID: 20927 RVA: 0x0016E3DE File Offset: 0x0016C7DE
	public void SetAnimationPlaySpeed(float spd)
	{
		this.m_AnimationPlaySpeed = spd;
	}

	// Token: 0x060051C0 RID: 20928 RVA: 0x0016E3E7 File Offset: 0x0016C7E7
	public MeigeAnimCtrl.ePlayState GetPlayState()
	{
		if (this.m_bManualUpdate)
		{
			return MeigeAnimCtrl.ePlayState.ePlaySteta_Manual;
		}
		return this.m_PlayState;
	}

	// Token: 0x060051C1 RID: 20929 RVA: 0x0016E3FC File Offset: 0x0016C7FC
	public void Open()
	{
		this.m_ReferenceAnimHandlerList = new List<MeigeAnimCtrl.ReferenceAnimHandler>();
		this.m_ReferenceAnimHandlerDic = new Dictionary<string, MeigeAnimCtrl.ReferenceAnimHandler>();
		this.m_TargetInstanceCache = null;
		this.m_CurAnim = null;
		this.m_PlayState = MeigeAnimCtrl.ePlayState.ePlayState_Invalid;
	}

	// Token: 0x060051C2 RID: 20930 RVA: 0x0016E42C File Offset: 0x0016C82C
	public bool AddClip(MsbHandler targetMsb, MeigeAnimClipHolder clipHolder)
	{
		if (targetMsb == null)
		{
			return false;
		}
		if (clipHolder == null)
		{
			return false;
		}
		string[] array;
		if (string.IsNullOrEmpty(clipHolder.m_MeigeAnimClip.m_Name))
		{
			array = clipHolder.m_UnityAnimClip.name.Split("@".ToCharArray());
		}
		else
		{
			array = clipHolder.m_MeigeAnimClip.m_Name.Split("@".ToCharArray());
		}
		string text = (array == null || array.Length <= 1) ? string.Empty : array[1];
		MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler;
		if (!this.m_ReferenceAnimHandlerDic.TryGetValue(text, out referenceAnimHandler))
		{
			referenceAnimHandler = new MeigeAnimCtrl.ReferenceAnimHandler();
			referenceAnimHandler.m_ClipName = text;
			referenceAnimHandler.m_AnimationTime = 0f;
			this.m_ReferenceAnimHandlerList.Add(referenceAnimHandler);
			this.m_ReferenceAnimHandlerDic.Add(text, referenceAnimHandler);
			referenceAnimHandler.m_ClipTargetHandlerList = new List<MeigeAnimCtrl.ClipTargetHandler>();
		}
		MeigeAnimCtrl.ClipTargetHandler clipTargetHandler = new MeigeAnimCtrl.ClipTargetHandler();
		clipTargetHandler.m_Target = targetMsb;
		clipTargetHandler.m_Clip = clipHolder;
		if (clipHolder.m_MeigeAnimClip != null)
		{
			referenceAnimHandler.m_AnimationTime = Mathf.Max(referenceAnimHandler.m_AnimationTime, clipHolder.m_MeigeAnimClip.m_AnimTimeBySec);
		}
		if (clipHolder.m_UnityAnimClip != null)
		{
			referenceAnimHandler.m_AnimationTime = Mathf.Max(referenceAnimHandler.m_AnimationTime, clipHolder.m_UnityAnimClip.length);
			Animation animationTarget = targetMsb.GetAnimationTarget();
			if (animationTarget != null)
			{
				animationTarget.AddClip(clipHolder.m_UnityAnimClip, clipHolder.m_UnityAnimClip.name);
			}
		}
		referenceAnimHandler.m_ClipTargetHandlerList.Add(clipTargetHandler);
		return true;
	}

	// Token: 0x060051C3 RID: 20931 RVA: 0x0016E5B4 File Offset: 0x0016C9B4
	public void Close()
	{
		int num = 0;
		for (int i = 0; i < this.m_ReferenceAnimHandlerList.Count; i++)
		{
			num = Mathf.Max(num, this.m_ReferenceAnimHandlerList[i].CalcNumOfMabAnimNode());
		}
		this.m_TargetInstanceCache = new MeigeAnimCtrl.TargetInstance[num];
	}

	// Token: 0x060051C4 RID: 20932 RVA: 0x0016E604 File Offset: 0x0016CA04
	public void Play(string clipName, float blendSec, WrapMode wrap)
	{
		if (wrap == WrapMode.Default)
		{
			wrap = WrapMode.Loop;
		}
		MeigeAnimCtrl.ReferenceAnimHandler curAnim = this.m_ReferenceAnimHandlerDic[clipName];
		this.m_CurAnim = curAnim;
		int num = 0;
		for (int i = 0; i < this.m_CurAnim.m_ClipTargetHandlerList.Count; i++)
		{
			MeigeAnimCtrl.ClipTargetHandler clipTargetHandler = this.m_CurAnim.m_ClipTargetHandlerList[i];
			Animation animationTarget = clipTargetHandler.m_Target.GetAnimationTarget();
			if (clipTargetHandler.m_Clip.m_UnityAnimClip)
			{
				Helper.InterpolateAnimation(animationTarget, clipTargetHandler.m_Clip.m_UnityAnimClip.name, blendSec, wrap);
			}
			else
			{
				animationTarget.Stop();
			}
			MeigeAnimEventNotifier animEventNotifier = clipTargetHandler.m_Target.GetAnimEventNotifier();
			if (animEventNotifier != null)
			{
				animEventNotifier.UpdateAnimClip(clipTargetHandler.m_Clip.m_MeigeAnimClip);
			}
			for (int j = 0; j < clipTargetHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length; j++)
			{
				MabAnimNodeHandler mabAnimNodeHandler = clipTargetHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray[j];
				mabAnimNodeHandler.Setup(ref this.m_TargetInstanceCache[num], clipTargetHandler.m_Target);
				num++;
			}
		}
		this.m_AnimationPlayTime = 0f;
		this.m_NormalizedPlayTime = -0.1f;
		this.m_OldNormalizedPlayTime = -0.1f;
		this.m_WrapMode = wrap;
		this.m_PlayState = MeigeAnimCtrl.ePlayState.ePlayState_Play;
	}

	// Token: 0x060051C5 RID: 20933 RVA: 0x0016E75F File Offset: 0x0016CB5F
	public void Pause()
	{
		if (this.m_CurAnim == null)
		{
			return;
		}
		this.m_PlayState = MeigeAnimCtrl.ePlayState.ePlayState_Pause;
	}

	// Token: 0x060051C6 RID: 20934 RVA: 0x0016E774 File Offset: 0x0016CB74
	public void UpdateAnimation(float sec)
	{
		int num = 0;
		this.m_OldNormalizedPlayTime = this.m_NormalizedPlayTime;
		this.m_NormalizedPlayTime = sec / this.m_CurAnim.m_AnimationTime;
		switch (this.m_WrapMode)
		{
		case WrapMode.Once:
			this.m_NormalizedPlayTime = ((this.m_NormalizedPlayTime <= 1f) ? this.m_NormalizedPlayTime : 0f);
			break;
		case WrapMode.Loop:
			this.m_NormalizedPlayTime = Mathf.Repeat(this.m_NormalizedPlayTime, 1f);
			break;
		case WrapMode.PingPong:
			this.m_NormalizedPlayTime = Mathf.PingPong(this.m_NormalizedPlayTime, 1f);
			break;
		case WrapMode.ClampForever:
			this.m_NormalizedPlayTime = Mathf.Min(1f, this.m_NormalizedPlayTime);
			break;
		}
		if (this.m_OldNormalizedPlayTime > this.m_NormalizedPlayTime)
		{
			this.m_OldNormalizedPlayTime = -0.1f;
		}
		float num2 = this.m_CurAnim.m_AnimationTime * this.m_NormalizedPlayTime;
		for (int i = 0; i < this.m_CurAnim.m_ClipTargetHandlerList.Count; i++)
		{
			MeigeAnimCtrl.ClipTargetHandler clipTargetHandler = this.m_CurAnim.m_ClipTargetHandlerList[i];
			float num3 = -1f;
			AnimationClip unityAnimClip = clipTargetHandler.m_Clip.m_UnityAnimClip;
			if (unityAnimClip != null)
			{
				Animation animationTarget = clipTargetHandler.m_Target.GetAnimationTarget();
				AnimationState animationState = animationTarget[unityAnimClip.name];
				animationState.time = Mathf.Clamp(num2, 0f, animationState.length);
				animationState.speed = 0f;
			}
			MabHandler meigeAnimClip = clipTargetHandler.m_Clip.m_MeigeAnimClip;
			if (meigeAnimClip != null)
			{
				if (num3 < 0f)
				{
					num3 = Mathf.Clamp(num2 / meigeAnimClip.m_AnimTimeBySec, 0f, 1f);
				}
				float frame = num3 * (float)(meigeAnimClip.m_NumOfKeyframe - 1);
				for (int j = 0; j < clipTargetHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length; j++)
				{
					MabAnimNodeHandler mabAnimNodeHandler = meigeAnimClip.m_AnimNodeHandlerArray[j];
					mabAnimNodeHandler.Animate(ref this.m_TargetInstanceCache[num], frame);
					num++;
				}
			}
		}
	}

	// Token: 0x060051C7 RID: 20935 RVA: 0x0016E9B4 File Offset: 0x0016CDB4
	private void Update()
	{
		if (this.m_CurAnim == null)
		{
			return;
		}
		if (this.m_TargetInstanceCache == null)
		{
			return;
		}
		if (this.m_bManualUpdate || this.m_PlayState == MeigeAnimCtrl.ePlayState.ePlayState_Pause)
		{
			for (int i = 0; i < this.m_CurAnim.m_ClipTargetHandlerList.Count; i++)
			{
				MeigeAnimCtrl.ClipTargetHandler clipTargetHandler = this.m_CurAnim.m_ClipTargetHandlerList[i];
				if (clipTargetHandler.m_Target.m_AnimationTarget[clipTargetHandler.m_Clip.m_UnityAnimClip.name] != null)
				{
					clipTargetHandler.m_Target.m_AnimationTarget[clipTargetHandler.m_Clip.m_UnityAnimClip.name].speed = 0f;
				}
			}
			return;
		}
		if (this.m_PlayState == MeigeAnimCtrl.ePlayState.ePlayState_Play)
		{
			this.UpdateAnimation(this.m_AnimationPlayTime);
			if (this.m_bFixedDeltaTime)
			{
				this.m_AnimationPlayTime += Time.fixedDeltaTime + 1E-06f;
			}
			else
			{
				this.m_AnimationPlayTime += Time.deltaTime * this.m_AnimationPlaySpeed + 1E-06f;
			}
		}
	}

	// Token: 0x060051C8 RID: 20936 RVA: 0x0016EAD8 File Offset: 0x0016CED8
	private void LateUpdate()
	{
		if (this.m_CurAnim == null)
		{
			return;
		}
		if (this.m_TargetInstanceCache == null)
		{
			return;
		}
		if (!this.m_bManualUpdate && this.m_PlayState != MeigeAnimCtrl.ePlayState.ePlayState_Play)
		{
			return;
		}
		for (int i = 0; i < this.m_CurAnim.m_ClipTargetHandlerList.Count; i++)
		{
			MeigeAnimCtrl.ClipTargetHandler clipTargetHandler = this.m_CurAnim.m_ClipTargetHandlerList[i];
			MeigeAnimEventNotifier animEventNotifier = clipTargetHandler.m_Target.GetAnimEventNotifier();
			if (!(animEventNotifier == null))
			{
				MabHandler meigeAnimClip = clipTargetHandler.m_Clip.m_MeigeAnimClip;
				if (meigeAnimClip != null && meigeAnimClip.m_AnimEvArray != null)
				{
					for (int j = 0; j < meigeAnimClip.m_AnimEvArray.Length; j++)
					{
						MabHandler.MabAnimEvent mabAnimEvent = meigeAnimClip.m_AnimEvArray[j];
						float num = mabAnimEvent.m_Frame / (float)(meigeAnimClip.m_NumOfKeyframe - 1);
						if (this.m_OldNormalizedPlayTime < num && this.m_NormalizedPlayTime >= num)
						{
							animEventNotifier.Notify_MeigeAnimEvent(j);
						}
					}
				}
			}
		}
	}

	// Token: 0x040050D0 RID: 20688
	public bool m_bManualUpdate;

	// Token: 0x040050D1 RID: 20689
	public bool m_bFixedDeltaTime;

	// Token: 0x040050D2 RID: 20690
	public float m_AnimationPlaySpeed = 1f;

	// Token: 0x040050D3 RID: 20691
	public float m_AnimationPlayTime;

	// Token: 0x040050D4 RID: 20692
	public float m_NormalizedPlayTime;

	// Token: 0x040050D5 RID: 20693
	public WrapMode m_WrapMode = WrapMode.Loop;

	// Token: 0x040050D6 RID: 20694
	public List<MeigeAnimCtrl.ReferenceAnimHandler> m_ReferenceAnimHandlerList;

	// Token: 0x040050D7 RID: 20695
	private float m_OldNormalizedPlayTime;

	// Token: 0x040050D8 RID: 20696
	private MeigeAnimCtrl.ePlayState m_PlayState = MeigeAnimCtrl.ePlayState.ePlayState_Invalid;

	// Token: 0x040050D9 RID: 20697
	private Dictionary<string, MeigeAnimCtrl.ReferenceAnimHandler> m_ReferenceAnimHandlerDic;

	// Token: 0x040050DA RID: 20698
	private MeigeAnimCtrl.TargetInstance[] m_TargetInstanceCache;

	// Token: 0x040050DB RID: 20699
	private MeigeAnimCtrl.ReferenceAnimHandler m_CurAnim;

	// Token: 0x02000F60 RID: 3936
	[Serializable]
	public class ClipTargetHandler
	{
		// Token: 0x040050DC RID: 20700
		public MeigeAnimClipHolder m_Clip;

		// Token: 0x040050DD RID: 20701
		public MsbHandler m_Target;
	}

	// Token: 0x02000F61 RID: 3937
	[Serializable]
	public class ReferenceAnimHandler
	{
		// Token: 0x060051CB RID: 20939 RVA: 0x0016EBF4 File Offset: 0x0016CFF4
		public int CalcNumOfMabAnimNode()
		{
			int num = 0;
			for (int i = 0; i < this.m_ClipTargetHandlerList.Count; i++)
			{
				num += this.m_ClipTargetHandlerList[i].m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length;
			}
			return num;
		}

		// Token: 0x040050DE RID: 20702
		public string m_ClipName;

		// Token: 0x040050DF RID: 20703
		public float m_AnimationTime;

		// Token: 0x040050E0 RID: 20704
		public List<MeigeAnimCtrl.ClipTargetHandler> m_ClipTargetHandlerList;
	}

	// Token: 0x02000F62 RID: 3938
	[StructLayout(LayoutKind.Explicit)]
	public struct TargetInstance
	{
		// Token: 0x060051CC RID: 20940 RVA: 0x0016EC40 File Offset: 0x0016D040
		public void Init()
		{
			this.m_Reference = null;
		}

		// Token: 0x040050E1 RID: 20705
		[FieldOffset(0)]
		public UnityEngine.Object m_Reference;

		// Token: 0x040050E2 RID: 20706
		[FieldOffset(0)]
		public MsbObjectHandler.MsbObjectParam m_MsbObjWork;

		// Token: 0x040050E3 RID: 20707
		[FieldOffset(0)]
		public MsbMaterialHandler.MsbMaterialParam m_MsbMatWork;

		// Token: 0x040050E4 RID: 20708
		[FieldOffset(0)]
		public MsbMaterialHandler.MsbTextureParam m_MsbTextureWork;

		// Token: 0x040050E5 RID: 20709
		[FieldOffset(0)]
		public MeigeParticleEmitter m_MeigeParticleEmitterWork;

		// Token: 0x040050E6 RID: 20710
		[FieldOffset(0)]
		public MsbCameraHandler.MsbCameraParam m_MsbCameraWork;
	}

	// Token: 0x02000F63 RID: 3939
	public enum ePlayState
	{
		// Token: 0x040050E8 RID: 20712
		ePlayState_Invalid = -1,
		// Token: 0x040050E9 RID: 20713
		ePlayState_Play,
		// Token: 0x040050EA RID: 20714
		ePlayState_Pause,
		// Token: 0x040050EB RID: 20715
		ePlaySteta_Manual
	}
}
