﻿using System;
using UnityEngine;

// Token: 0x02000659 RID: 1625
public class GCM
{
	// Token: 0x060020D9 RID: 8409 RVA: 0x000AF374 File Offset: 0x000AD774
	public static void Register(Action<string> OnRegisterCallback, params string[] senderId)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.AWSUnityGCMWrapper"))
		{
			string text = string.Join(",", senderId);
			string text2 = androidJavaClass.CallStatic<string>("register", new object[]
			{
				text
			});
			Debug.Log("regId = " + text2);
			if (OnRegisterCallback != null)
			{
				OnRegisterCallback(text2);
			}
		}
	}

	// Token: 0x060020DA RID: 8410 RVA: 0x000AF3F0 File Offset: 0x000AD7F0
	public static void Unregister()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.AWSUnityGCMWrapper"))
			{
				androidJavaClass.CallStatic("unregister", new object[0]);
			}
		}
	}

	// Token: 0x0400271E RID: 10014
	private const string CLASS_NAME = "com.amazonaws.unity.AWSUnityGCMWrapper";
}
