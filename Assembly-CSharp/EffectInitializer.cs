﻿using System;
using UnityEngine;

// Token: 0x02000E99 RID: 3737
[CreateAssetMenu(fileName = "EffectInitializeScript", menuName = "Effect/Create EffectInitializeScript", order = 1000)]
public class EffectInitializer : ScriptableObject
{
	// Token: 0x06004DCC RID: 19916 RVA: 0x0015A450 File Offset: 0x00158850
	private void Awake()
	{
	}

	// Token: 0x06004DCD RID: 19917 RVA: 0x0015A452 File Offset: 0x00158852
	private void OnEnable()
	{
	}
}
