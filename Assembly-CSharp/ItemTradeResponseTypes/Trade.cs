﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace ItemTradeResponseTypes
{
	// Token: 0x02000C31 RID: 3121
	public class Trade : CommonResponse
	{
		// Token: 0x0600401B RID: 16411 RVA: 0x0013DC7C File Offset: 0x0013C07C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			str += ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
			str += this.totalTradeCount.ToString();
			return str + this.monthlyTradeCount.ToString();
		}

		// Token: 0x040045F4 RID: 17908
		public Player player;

		// Token: 0x040045F5 RID: 17909
		public PlayerItemSummary[] itemSummary;

		// Token: 0x040045F6 RID: 17910
		public PlayerWeapon[] managedWeapons;

		// Token: 0x040045F7 RID: 17911
		public PlayerCharacter[] managedCharacters;

		// Token: 0x040045F8 RID: 17912
		public PlayerNamedType[] managedNamedTypes;

		// Token: 0x040045F9 RID: 17913
		public int totalTradeCount;

		// Token: 0x040045FA RID: 17914
		public int monthlyTradeCount;
	}
}
