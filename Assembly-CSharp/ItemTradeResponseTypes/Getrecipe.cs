﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace ItemTradeResponseTypes
{
	// Token: 0x02000C30 RID: 3120
	public class Getrecipe : CommonResponse
	{
		// Token: 0x06004019 RID: 16409 RVA: 0x0013DC38 File Offset: 0x0013C038
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.itemTradeRecipes == null) ? string.Empty : this.itemTradeRecipes.ToString());
		}

		// Token: 0x040045F3 RID: 17907
		public ItemTradeRecipe[] itemTradeRecipes;
	}
}
