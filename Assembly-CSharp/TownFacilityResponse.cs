﻿using System;
using System.Collections.Generic;
using Meige;
using TownFacilityResponseTypes;
using WWWTypes;

// Token: 0x02000C0E RID: 3086
public static class TownFacilityResponse
{
	// Token: 0x06003FC6 RID: 16326 RVA: 0x0013D138 File Offset: 0x0013B538
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC7 RID: 16327 RVA: 0x0013D150 File Offset: 0x0013B550
	public static Buyall Buyall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Buyall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC8 RID: 16328 RVA: 0x0013D168 File Offset: 0x0013B568
	public static Buildpointset Buildpointset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Buildpointset>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC9 RID: 16329 RVA: 0x0013D180 File Offset: 0x0013B580
	public static Buildpointsetall Buildpointsetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Buildpointsetall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCA RID: 16330 RVA: 0x0013D198 File Offset: 0x0013B598
	public static Buildpointremove Buildpointremove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Buildpointremove>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCB RID: 16331 RVA: 0x0013D1B0 File Offset: 0x0013B5B0
	public static Preparelevelup Preparelevelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Preparelevelup>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCC RID: 16332 RVA: 0x0013D1C8 File Offset: 0x0013B5C8
	public static Levelup Levelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Levelup>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCD RID: 16333 RVA: 0x0013D1E0 File Offset: 0x0013B5E0
	public static Gemlevelup Gemlevelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Gemlevelup>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCE RID: 16334 RVA: 0x0013D1F8 File Offset: 0x0013B5F8
	public static Itemup Itemup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Itemup>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FCF RID: 16335 RVA: 0x0013D210 File Offset: 0x0013B610
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD0 RID: 16336 RVA: 0x0013D228 File Offset: 0x0013B628
	public static SetState SetState(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<SetState>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD1 RID: 16337 RVA: 0x0013D240 File Offset: 0x0013B640
	public static Sale Sale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Sale>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD2 RID: 16338 RVA: 0x0013D258 File Offset: 0x0013B658
	public static Limitadd Limitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Limitadd>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FD3 RID: 16339 RVA: 0x0013D270 File Offset: 0x0013B670
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Remove>(param, dialogType, acceptableResultCodes);
	}
}
