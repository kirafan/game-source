﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TUTORIALResponseTypes
{
	// Token: 0x02000C7D RID: 3197
	public class SetParty : CommonResponse
	{
		// Token: 0x060040B3 RID: 16563 RVA: 0x0013F958 File Offset: 0x0013DD58
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.managedBattleParties == null) ? string.Empty : this.managedBattleParties.ToString());
			return str + ((this.managedFieldPartyMembers == null) ? string.Empty : this.managedFieldPartyMembers.ToString());
		}

		// Token: 0x04004685 RID: 18053
		public PlayerBattleParty[] managedBattleParties;

		// Token: 0x04004686 RID: 18054
		public PlayerFieldPartyMember[] managedFieldPartyMembers;
	}
}
