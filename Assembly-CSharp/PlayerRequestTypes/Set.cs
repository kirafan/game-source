﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BA3 RID: 2979
	public class Set
	{
		// Token: 0x04004547 RID: 17735
		public string name;

		// Token: 0x04004548 RID: 17736
		public string comment;

		// Token: 0x04004549 RID: 17737
		public string linkId;
	}
}
