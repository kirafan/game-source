﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BB0 RID: 2992
	public class Characterlimitbreak
	{
		// Token: 0x04004558 RID: 17752
		public long managedCharacterId;

		// Token: 0x04004559 RID: 17753
		public int recipeId;

		// Token: 0x0400455A RID: 17754
		public int itemIndex;
	}
}
