﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000B9A RID: 2970
	public class Login
	{
		// Token: 0x04004537 RID: 17719
		public string uuid;

		// Token: 0x04004538 RID: 17720
		public string accessToken;

		// Token: 0x04004539 RID: 17721
		public int platform;

		// Token: 0x0400453A RID: 17722
		public string appVersion;
	}
}
