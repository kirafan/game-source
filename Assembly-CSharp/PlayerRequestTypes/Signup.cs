﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000B99 RID: 2969
	public class Signup
	{
		// Token: 0x04004533 RID: 17715
		public string uuid;

		// Token: 0x04004534 RID: 17716
		public int platform;

		// Token: 0x04004535 RID: 17717
		public string name;

		// Token: 0x04004536 RID: 17718
		public int stepCode;
	}
}
