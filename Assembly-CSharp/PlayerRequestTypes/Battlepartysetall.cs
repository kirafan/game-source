﻿using System;
using WWWTypes;

namespace PlayerRequestTypes
{
	// Token: 0x02000BB4 RID: 2996
	public class Battlepartysetall
	{
		// Token: 0x04004562 RID: 17762
		public PlayerBattlePartyMember[] battlePartyMembers;
	}
}
