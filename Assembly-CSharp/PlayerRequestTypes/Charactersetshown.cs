﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BAF RID: 2991
	public class Charactersetshown
	{
		// Token: 0x04004556 RID: 17750
		public long managedCharacterId;

		// Token: 0x04004557 RID: 17751
		public int shown;
	}
}
