﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BB9 RID: 3001
	public class Questlogadd
	{
		// Token: 0x04004564 RID: 17764
		public int type;

		// Token: 0x04004565 RID: 17765
		public int questId;

		// Token: 0x04004566 RID: 17766
		public long managedBattlePartyId;

		// Token: 0x04004567 RID: 17767
		public long supportCharacterId;

		// Token: 0x04004568 RID: 17768
		public string questData;
	}
}
