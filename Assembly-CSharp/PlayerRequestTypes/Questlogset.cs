﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BBB RID: 3003
	public class Questlogset
	{
		// Token: 0x0400456B RID: 17771
		public long orderReceiveId;

		// Token: 0x0400456C RID: 17772
		public int state;

		// Token: 0x0400456D RID: 17773
		public int clearRank;

		// Token: 0x0400456E RID: 17774
		public string skillExps;

		// Token: 0x0400456F RID: 17775
		public string dropItems;

		// Token: 0x04004570 RID: 17776
		public string killedEnemies;

		// Token: 0x04004571 RID: 17777
		public string weaponSkillExps;

		// Token: 0x04004572 RID: 17778
		public int friendUseNum;

		// Token: 0x04004573 RID: 17779
		public int masterSkillUseNum;

		// Token: 0x04004574 RID: 17780
		public int uniqueSkillUseNum;

		// Token: 0x04004575 RID: 17781
		public int stepCode;
	}
}
