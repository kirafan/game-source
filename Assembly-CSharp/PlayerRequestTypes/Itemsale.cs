﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BA8 RID: 2984
	public class Itemsale
	{
		// Token: 0x0400454E RID: 17742
		public string itemId;

		// Token: 0x0400454F RID: 17743
		public string amount;
	}
}
