﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BB3 RID: 2995
	public class Battlepartysetname
	{
		// Token: 0x04004560 RID: 17760
		public long managedBattlePartyId;

		// Token: 0x04004561 RID: 17761
		public string name;
	}
}
