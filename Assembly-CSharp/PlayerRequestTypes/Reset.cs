﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000B9F RID: 2975
	public class Reset
	{
		// Token: 0x04004544 RID: 17732
		public string uuid;

		// Token: 0x04004545 RID: 17733
		public string accessToken;
	}
}
