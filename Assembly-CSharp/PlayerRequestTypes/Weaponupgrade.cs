﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BAB RID: 2987
	public class Weaponupgrade
	{
		// Token: 0x04004551 RID: 17745
		public long managedWeaponId;

		// Token: 0x04004552 RID: 17746
		public string itemId;

		// Token: 0x04004553 RID: 17747
		public string amount;
	}
}
