﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000B9D RID: 2973
	public class Moveset
	{
		// Token: 0x0400453D RID: 17725
		public string moveCode;

		// Token: 0x0400453E RID: 17726
		public string movePassword;

		// Token: 0x0400453F RID: 17727
		public string uuid;

		// Token: 0x04004540 RID: 17728
		public int platform;
	}
}
