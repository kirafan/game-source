﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000BB1 RID: 2993
	public class Characterupgrade
	{
		// Token: 0x0400455B RID: 17755
		public long managedCharacterId;

		// Token: 0x0400455C RID: 17756
		public string itemId;

		// Token: 0x0400455D RID: 17757
		public string amount;
	}
}
