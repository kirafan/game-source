﻿using System;

namespace PlayerRequestTypes
{
	// Token: 0x02000B9E RID: 2974
	public class Linkmoveset
	{
		// Token: 0x04004541 RID: 17729
		public string linkId;

		// Token: 0x04004542 RID: 17730
		public string uuid;

		// Token: 0x04004543 RID: 17731
		public int platform;
	}
}
