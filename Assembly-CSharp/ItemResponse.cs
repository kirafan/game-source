﻿using System;
using System.Collections.Generic;
using ItemResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFE RID: 3070
public static class ItemResponse
{
	// Token: 0x06003F72 RID: 16242 RVA: 0x0013C670 File Offset: 0x0013AA70
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F73 RID: 16243 RVA: 0x0013C688 File Offset: 0x0013AA88
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}
}
