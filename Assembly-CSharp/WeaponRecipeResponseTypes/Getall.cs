﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace WeaponRecipeResponseTypes
{
	// Token: 0x02000C8F RID: 3215
	public class Getall : CommonResponse
	{
		// Token: 0x060040D7 RID: 16599 RVA: 0x0013FE00 File Offset: 0x0013E200
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.weaponRecipes == null) ? string.Empty : this.weaponRecipes.ToString());
		}

		// Token: 0x04004696 RID: 18070
		public WeaponRecipe[] weaponRecipes;
	}
}
