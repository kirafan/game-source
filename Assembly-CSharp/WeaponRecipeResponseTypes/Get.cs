﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace WeaponRecipeResponseTypes
{
	// Token: 0x02000C90 RID: 3216
	public class Get : CommonResponse
	{
		// Token: 0x060040D9 RID: 16601 RVA: 0x0013FE44 File Offset: 0x0013E244
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.weaponRecipes == null) ? string.Empty : this.weaponRecipes.ToString());
		}

		// Token: 0x04004697 RID: 18071
		public WeaponRecipe[] weaponRecipes;
	}
}
