﻿using System;
using System.Collections.Generic;
using ADVResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BF6 RID: 3062
public static class ADVResponse
{
	// Token: 0x06003F55 RID: 16213 RVA: 0x0013C3B8 File Offset: 0x0013A7B8
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}
}
