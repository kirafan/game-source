﻿using System;
using System.Collections.Generic;
using Meige;
using WeaponResponseTypes;
using WWWTypes;

// Token: 0x02000C94 RID: 3220
public static class WeaponResponse
{
	// Token: 0x060040E0 RID: 16608 RVA: 0x0013FF38 File Offset: 0x0013E338
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x060040E1 RID: 16609 RVA: 0x0013FF50 File Offset: 0x0013E350
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}
}
