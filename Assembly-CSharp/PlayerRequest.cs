﻿using System;
using Meige;
using PlayerRequestTypes;
using WWWTypes;

// Token: 0x02000B67 RID: 2919
public static class PlayerRequest
{
	// Token: 0x06003E1D RID: 15901 RVA: 0x00139294 File Offset: 0x00137694
	public static MeigewwwParam Signup(Signup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/signup", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", param.uuid);
		meigewwwParam.Add("platform", param.platform);
		meigewwwParam.Add("name", param.name);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E1E RID: 15902 RVA: 0x00139304 File Offset: 0x00137704
	public static MeigewwwParam Signup(string uuid, int platform, string name, int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/signup", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", uuid);
		meigewwwParam.Add("platform", platform);
		meigewwwParam.Add("name", name);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E1F RID: 15903 RVA: 0x00139360 File Offset: 0x00137760
	public static MeigewwwParam Login(Login param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/login", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", param.uuid);
		meigewwwParam.Add("accessToken", param.accessToken);
		meigewwwParam.Add("platform", param.platform);
		meigewwwParam.Add("appVersion", param.appVersion);
		return meigewwwParam;
	}

	// Token: 0x06003E20 RID: 15904 RVA: 0x001393CC File Offset: 0x001377CC
	public static MeigewwwParam Login(string uuid, string accessToken, int platform, string appVersion, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/login", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", uuid);
		meigewwwParam.Add("accessToken", accessToken);
		meigewwwParam.Add("platform", platform);
		meigewwwParam.Add("appVersion", appVersion);
		return meigewwwParam;
	}

	// Token: 0x06003E21 RID: 15905 RVA: 0x00139424 File Offset: 0x00137824
	public static MeigewwwParam Setpushtoken(Setpushtoken param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/push_token/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("pushToken", param.pushToken);
		return meigewwwParam;
	}

	// Token: 0x06003E22 RID: 15906 RVA: 0x00139458 File Offset: 0x00137858
	public static MeigewwwParam Setpushtoken(string pushToken, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/push_token/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("pushToken", pushToken);
		return meigewwwParam;
	}

	// Token: 0x06003E23 RID: 15907 RVA: 0x00139488 File Offset: 0x00137888
	public static MeigewwwParam Moveget(Moveget param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/get", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("password", param.password);
		return meigewwwParam;
	}

	// Token: 0x06003E24 RID: 15908 RVA: 0x001394BC File Offset: 0x001378BC
	public static MeigewwwParam Moveget(string password, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/get", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("password", password);
		return meigewwwParam;
	}

	// Token: 0x06003E25 RID: 15909 RVA: 0x001394EC File Offset: 0x001378EC
	public static MeigewwwParam Moveset(Moveset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("moveCode", param.moveCode);
		meigewwwParam.Add("movePassword", param.movePassword);
		meigewwwParam.Add("uuid", param.uuid);
		meigewwwParam.Add("platform", param.platform);
		return meigewwwParam;
	}

	// Token: 0x06003E26 RID: 15910 RVA: 0x00139558 File Offset: 0x00137958
	public static MeigewwwParam Moveset(string moveCode, string movePassword, string uuid, int platform, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("moveCode", moveCode);
		meigewwwParam.Add("movePassword", movePassword);
		meigewwwParam.Add("uuid", uuid);
		meigewwwParam.Add("platform", platform);
		return meigewwwParam;
	}

	// Token: 0x06003E27 RID: 15911 RVA: 0x001395B0 File Offset: 0x001379B0
	public static MeigewwwParam Linkmoveset(Linkmoveset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/link/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("linkId", param.linkId);
		meigewwwParam.Add("uuid", param.uuid);
		meigewwwParam.Add("platform", param.platform);
		return meigewwwParam;
	}

	// Token: 0x06003E28 RID: 15912 RVA: 0x0013960C File Offset: 0x00137A0C
	public static MeigewwwParam Linkmoveset(string linkId, string uuid, int platform, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/move/link/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("linkId", linkId);
		meigewwwParam.Add("uuid", uuid);
		meigewwwParam.Add("platform", platform);
		return meigewwwParam;
	}

	// Token: 0x06003E29 RID: 15913 RVA: 0x00139658 File Offset: 0x00137A58
	public static MeigewwwParam Reset(Reset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/reset", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", param.uuid);
		meigewwwParam.Add("accessToken", param.accessToken);
		return meigewwwParam;
	}

	// Token: 0x06003E2A RID: 15914 RVA: 0x0013969C File Offset: 0x00137A9C
	public static MeigewwwParam Reset(string uuid, string accessToken, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/reset", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("uuid", uuid);
		meigewwwParam.Add("accessToken", accessToken);
		return meigewwwParam;
	}

	// Token: 0x06003E2B RID: 15915 RVA: 0x001396D8 File Offset: 0x00137AD8
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E2C RID: 15916 RVA: 0x001396FC File Offset: 0x00137AFC
	public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E2D RID: 15917 RVA: 0x00139720 File Offset: 0x00137B20
	public static MeigewwwParam Getbadgecount(Getbadgecount param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/badge/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E2E RID: 15918 RVA: 0x00139744 File Offset: 0x00137B44
	public static MeigewwwParam Getbadgecount(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/badge/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E2F RID: 15919 RVA: 0x00139768 File Offset: 0x00137B68
	public static MeigewwwParam Resumeget(Resumeget param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/resume/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", param.playerId);
		return meigewwwParam;
	}

	// Token: 0x06003E30 RID: 15920 RVA: 0x001397A0 File Offset: 0x00137BA0
	public static MeigewwwParam Resumeget(long playerId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/resume/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", playerId);
		return meigewwwParam;
	}

	// Token: 0x06003E31 RID: 15921 RVA: 0x001397D4 File Offset: 0x00137BD4
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("name", param.name);
		meigewwwParam.Add("comment", param.comment);
		meigewwwParam.Add("linkId", param.linkId);
		return meigewwwParam;
	}

	// Token: 0x06003E32 RID: 15922 RVA: 0x00139828 File Offset: 0x00137C28
	public static MeigewwwParam Set(string name, string comment, string linkId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("name", name);
		meigewwwParam.Add("comment", comment);
		meigewwwParam.Add("linkId", linkId);
		return meigewwwParam;
	}

	// Token: 0x06003E33 RID: 15923 RVA: 0x00139870 File Offset: 0x00137C70
	public static MeigewwwParam Setpushnotification(Setpushnotification param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/push_notification/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("flag1", param.flag1);
		meigewwwParam.Add("flag2", param.flag2);
		meigewwwParam.Add("flag3", param.flag3);
		return meigewwwParam;
	}

	// Token: 0x06003E34 RID: 15924 RVA: 0x001398D4 File Offset: 0x00137CD4
	public static MeigewwwParam Setpushnotification(int flag1, int flag2, int flag3, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/push_notification/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("flag1", flag1);
		meigewwwParam.Add("flag2", flag2);
		meigewwwParam.Add("flag3", flag3);
		return meigewwwParam;
	}

	// Token: 0x06003E35 RID: 15925 RVA: 0x00139928 File Offset: 0x00137D28
	public static MeigewwwParam Setage(Setage param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/age/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("age", param.age);
		return meigewwwParam;
	}

	// Token: 0x06003E36 RID: 15926 RVA: 0x00139960 File Offset: 0x00137D60
	public static MeigewwwParam Setage(int age, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/age/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("age", age);
		return meigewwwParam;
	}

	// Token: 0x06003E37 RID: 15927 RVA: 0x00139994 File Offset: 0x00137D94
	public static MeigewwwParam Loginbonus(Loginbonus param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/login_bonus/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E38 RID: 15928 RVA: 0x001399B8 File Offset: 0x00137DB8
	public static MeigewwwParam Loginbonus(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/login_bonus/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E39 RID: 15929 RVA: 0x001399DC File Offset: 0x00137DDC
	public static MeigewwwParam Itemgetsummary(Itemgetsummary param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/get_summary", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E3A RID: 15930 RVA: 0x00139A00 File Offset: 0x00137E00
	public static MeigewwwParam Itemgetsummary(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/get_summary", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E3B RID: 15931 RVA: 0x00139A24 File Offset: 0x00137E24
	public static MeigewwwParam Itemsale(Itemsale param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("itemId", param.itemId);
		meigewwwParam.Add("amount", param.amount);
		return meigewwwParam;
	}

	// Token: 0x06003E3C RID: 15932 RVA: 0x00139A68 File Offset: 0x00137E68
	public static MeigewwwParam Itemsale(string itemId, string amount, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("itemId", itemId);
		meigewwwParam.Add("amount", amount);
		return meigewwwParam;
	}

	// Token: 0x06003E3D RID: 15933 RVA: 0x00139AA4 File Offset: 0x00137EA4
	public static MeigewwwParam Weapongetall(Weapongetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E3E RID: 15934 RVA: 0x00139AC8 File Offset: 0x00137EC8
	public static MeigewwwParam Weapongetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E3F RID: 15935 RVA: 0x00139AEC File Offset: 0x00137EEC
	public static MeigewwwParam Weaponmake(Weaponmake param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/make", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("recipeId", param.recipeId);
		return meigewwwParam;
	}

	// Token: 0x06003E40 RID: 15936 RVA: 0x00139B24 File Offset: 0x00137F24
	public static MeigewwwParam Weaponmake(int recipeId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/make", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("recipeId", recipeId);
		return meigewwwParam;
	}

	// Token: 0x06003E41 RID: 15937 RVA: 0x00139B58 File Offset: 0x00137F58
	public static MeigewwwParam Weaponupgrade(Weaponupgrade param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedWeaponId", param.managedWeaponId);
		meigewwwParam.Add("itemId", param.itemId);
		meigewwwParam.Add("amount", param.amount);
		return meigewwwParam;
	}

	// Token: 0x06003E42 RID: 15938 RVA: 0x00139BB4 File Offset: 0x00137FB4
	public static MeigewwwParam Weaponupgrade(long managedWeaponId, string itemId, string amount, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedWeaponId", managedWeaponId);
		meigewwwParam.Add("itemId", itemId);
		meigewwwParam.Add("amount", amount);
		return meigewwwParam;
	}

	// Token: 0x06003E43 RID: 15939 RVA: 0x00139C00 File Offset: 0x00138000
	public static MeigewwwParam Weaponsale(Weaponsale param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedWeaponId", param.managedWeaponId);
		return meigewwwParam;
	}

	// Token: 0x06003E44 RID: 15940 RVA: 0x00139C34 File Offset: 0x00138034
	public static MeigewwwParam Weaponsale(string managedWeaponId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedWeaponId", managedWeaponId);
		return meigewwwParam;
	}

	// Token: 0x06003E45 RID: 15941 RVA: 0x00139C64 File Offset: 0x00138064
	public static MeigewwwParam Weaponlimitadd(Weaponlimitadd param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", param.num);
		return meigewwwParam;
	}

	// Token: 0x06003E46 RID: 15942 RVA: 0x00139C9C File Offset: 0x0013809C
	public static MeigewwwParam Weaponlimitadd(int num, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/weapon/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", num);
		return meigewwwParam;
	}

	// Token: 0x06003E47 RID: 15943 RVA: 0x00139CD0 File Offset: 0x001380D0
	public static MeigewwwParam Charactergetall(Charactergetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E48 RID: 15944 RVA: 0x00139CF4 File Offset: 0x001380F4
	public static MeigewwwParam Charactergetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E49 RID: 15945 RVA: 0x00139D18 File Offset: 0x00138118
	public static MeigewwwParam Charactersetshown(Charactersetshown param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", param.managedCharacterId);
		meigewwwParam.Add("shown", param.shown);
		return meigewwwParam;
	}

	// Token: 0x06003E4A RID: 15946 RVA: 0x00139D68 File Offset: 0x00138168
	public static MeigewwwParam Charactersetshown(long managedCharacterId, int shown, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", managedCharacterId);
		meigewwwParam.Add("shown", shown);
		return meigewwwParam;
	}

	// Token: 0x06003E4B RID: 15947 RVA: 0x00139DAC File Offset: 0x001381AC
	public static MeigewwwParam Characterlimitbreak(Characterlimitbreak param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/limitBreak", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", param.managedCharacterId);
		meigewwwParam.Add("recipeId", param.recipeId);
		meigewwwParam.Add("itemIndex", param.itemIndex);
		return meigewwwParam;
	}

	// Token: 0x06003E4C RID: 15948 RVA: 0x00139E10 File Offset: 0x00138210
	public static MeigewwwParam Characterlimitbreak(long managedCharacterId, int recipeId, int itemIndex, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/limitBreak", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", managedCharacterId);
		meigewwwParam.Add("recipeId", recipeId);
		meigewwwParam.Add("itemIndex", itemIndex);
		return meigewwwParam;
	}

	// Token: 0x06003E4D RID: 15949 RVA: 0x00139E64 File Offset: 0x00138264
	public static MeigewwwParam Characterupgrade(Characterupgrade param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", param.managedCharacterId);
		meigewwwParam.Add("itemId", param.itemId);
		meigewwwParam.Add("amount", param.amount);
		return meigewwwParam;
	}

	// Token: 0x06003E4E RID: 15950 RVA: 0x00139EC0 File Offset: 0x001382C0
	public static MeigewwwParam Characterupgrade(long managedCharacterId, string itemId, string amount, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", managedCharacterId);
		meigewwwParam.Add("itemId", itemId);
		meigewwwParam.Add("amount", amount);
		return meigewwwParam;
	}

	// Token: 0x06003E4F RID: 15951 RVA: 0x00139F0C File Offset: 0x0013830C
	public static MeigewwwParam Characterevolution(Characterevolution param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/evolution", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", param.managedCharacterId);
		meigewwwParam.Add("recipeId", param.recipeId);
		return meigewwwParam;
	}

	// Token: 0x06003E50 RID: 15952 RVA: 0x00139F5C File Offset: 0x0013835C
	public static MeigewwwParam Characterevolution(long managedCharacterId, int recipeId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/character/evolution", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedCharacterId", managedCharacterId);
		meigewwwParam.Add("recipeId", recipeId);
		return meigewwwParam;
	}

	// Token: 0x06003E51 RID: 15953 RVA: 0x00139FA0 File Offset: 0x001383A0
	public static MeigewwwParam Battlepartysetname(Battlepartysetname param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/set_name", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedBattlePartyId", param.managedBattlePartyId);
		meigewwwParam.Add("name", param.name);
		return meigewwwParam;
	}

	// Token: 0x06003E52 RID: 15954 RVA: 0x00139FE8 File Offset: 0x001383E8
	public static MeigewwwParam Battlepartysetname(long managedBattlePartyId, string name, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/set_name", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedBattlePartyId", managedBattlePartyId);
		meigewwwParam.Add("name", name);
		return meigewwwParam;
	}

	// Token: 0x06003E53 RID: 15955 RVA: 0x0013A028 File Offset: 0x00138428
	public static MeigewwwParam Battlepartysetall(Battlepartysetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("battlePartyMembers", param.battlePartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003E54 RID: 15956 RVA: 0x0013A05C File Offset: 0x0013845C
	public static MeigewwwParam Battlepartysetall(PlayerBattlePartyMember[] battlePartyMembers, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("battlePartyMembers", battlePartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003E55 RID: 15957 RVA: 0x0013A08C File Offset: 0x0013848C
	public static MeigewwwParam Battlepartygetall(Battlepartygetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E56 RID: 15958 RVA: 0x0013A0B0 File Offset: 0x001384B0
	public static MeigewwwParam Battlepartygetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/battle_party/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E57 RID: 15959 RVA: 0x0013A0D4 File Offset: 0x001384D4
	public static MeigewwwParam Masterorbgetall(Masterorbgetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E58 RID: 15960 RVA: 0x0013A0F8 File Offset: 0x001384F8
	public static MeigewwwParam Masterorbgetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E59 RID: 15961 RVA: 0x0013A11C File Offset: 0x0013851C
	public static MeigewwwParam Masterorbset(Masterorbset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/master_orb/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedMasterOrbId", param.managedMasterOrbId);
		return meigewwwParam;
	}

	// Token: 0x06003E5A RID: 15962 RVA: 0x0013A154 File Offset: 0x00138554
	public static MeigewwwParam Masterorbset(long managedMasterOrbId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/master_orb/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedMasterOrbId", managedMasterOrbId);
		return meigewwwParam;
	}

	// Token: 0x06003E5B RID: 15963 RVA: 0x0013A188 File Offset: 0x00138588
	public static MeigewwwParam Questgetall(Questgetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E5C RID: 15964 RVA: 0x0013A1AC File Offset: 0x001385AC
	public static MeigewwwParam Questgetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E5D RID: 15965 RVA: 0x0013A1D0 File Offset: 0x001385D0
	public static MeigewwwParam Questlogadd(Questlogadd param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("type", param.type);
		meigewwwParam.Add("questId", param.questId);
		meigewwwParam.Add("managedBattlePartyId", param.managedBattlePartyId);
		meigewwwParam.Add("supportCharacterId", param.supportCharacterId);
		meigewwwParam.Add("questData", param.questData);
		return meigewwwParam;
	}

	// Token: 0x06003E5E RID: 15966 RVA: 0x0013A25C File Offset: 0x0013865C
	public static MeigewwwParam Questlogadd(int type, int questId, long managedBattlePartyId, long supportCharacterId, string questData, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("type", type);
		meigewwwParam.Add("questId", questId);
		meigewwwParam.Add("managedBattlePartyId", managedBattlePartyId);
		meigewwwParam.Add("supportCharacterId", supportCharacterId);
		meigewwwParam.Add("questData", questData);
		return meigewwwParam;
	}

	// Token: 0x06003E5F RID: 15967 RVA: 0x0013A2D0 File Offset: 0x001386D0
	public static MeigewwwParam Questlogretry(Questlogretry param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/retry", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", param.orderReceiveId);
		meigewwwParam.Add("questData", param.questData);
		return meigewwwParam;
	}

	// Token: 0x06003E60 RID: 15968 RVA: 0x0013A318 File Offset: 0x00138718
	public static MeigewwwParam Questlogretry(long orderReceiveId, string questData, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/retry", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", orderReceiveId);
		meigewwwParam.Add("questData", questData);
		return meigewwwParam;
	}

	// Token: 0x06003E61 RID: 15969 RVA: 0x0013A358 File Offset: 0x00138758
	public static MeigewwwParam Questlogset(Questlogset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", param.orderReceiveId);
		meigewwwParam.Add("state", param.state);
		meigewwwParam.Add("clearRank", param.clearRank);
		meigewwwParam.Add("skillExps", param.skillExps);
		meigewwwParam.Add("dropItems", param.dropItems);
		meigewwwParam.Add("killedEnemies", param.killedEnemies);
		meigewwwParam.Add("weaponSkillExps", param.weaponSkillExps);
		meigewwwParam.Add("friendUseNum", param.friendUseNum);
		meigewwwParam.Add("masterSkillUseNum", param.masterSkillUseNum);
		meigewwwParam.Add("uniqueSkillUseNum", param.uniqueSkillUseNum);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E62 RID: 15970 RVA: 0x0013A458 File Offset: 0x00138858
	public static MeigewwwParam Questlogset(long orderReceiveId, int state, int clearRank, string skillExps, string dropItems, string killedEnemies, string weaponSkillExps, int friendUseNum, int masterSkillUseNum, int uniqueSkillUseNum, int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", orderReceiveId);
		meigewwwParam.Add("state", state);
		meigewwwParam.Add("clearRank", clearRank);
		meigewwwParam.Add("skillExps", skillExps);
		meigewwwParam.Add("dropItems", dropItems);
		meigewwwParam.Add("killedEnemies", killedEnemies);
		meigewwwParam.Add("weaponSkillExps", weaponSkillExps);
		meigewwwParam.Add("friendUseNum", friendUseNum);
		meigewwwParam.Add("masterSkillUseNum", masterSkillUseNum);
		meigewwwParam.Add("uniqueSkillUseNum", uniqueSkillUseNum);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E63 RID: 15971 RVA: 0x0013A528 File Offset: 0x00138928
	public static MeigewwwParam Questloggetall(Questloggetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("state", param.state);
		return meigewwwParam;
	}

	// Token: 0x06003E64 RID: 15972 RVA: 0x0013A560 File Offset: 0x00138960
	public static MeigewwwParam Questloggetall(int state, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("state", state);
		return meigewwwParam;
	}

	// Token: 0x06003E65 RID: 15973 RVA: 0x0013A594 File Offset: 0x00138994
	public static MeigewwwParam Questlogsave(Questlogsave param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/save", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", param.orderReceiveId);
		meigewwwParam.Add("questData", param.questData);
		return meigewwwParam;
	}

	// Token: 0x06003E66 RID: 15974 RVA: 0x0013A5DC File Offset: 0x001389DC
	public static MeigewwwParam Questlogsave(long orderReceiveId, string questData, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/save", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("orderReceiveId", orderReceiveId);
		meigewwwParam.Add("questData", questData);
		return meigewwwParam;
	}

	// Token: 0x06003E67 RID: 15975 RVA: 0x0013A61C File Offset: 0x00138A1C
	public static MeigewwwParam Questlogload(Questlogload param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/load", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E68 RID: 15976 RVA: 0x0013A640 File Offset: 0x00138A40
	public static MeigewwwParam Questlogload(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/load", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E69 RID: 15977 RVA: 0x0013A664 File Offset: 0x00138A64
	public static MeigewwwParam Questlogreset(Questlogreset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/reset", MeigewwwParam.eRequestMethod.Post, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6A RID: 15978 RVA: 0x0013A688 File Offset: 0x00138A88
	public static MeigewwwParam Questlogreset(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest_log/reset", MeigewwwParam.eRequestMethod.Post, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6B RID: 15979 RVA: 0x0013A6AC File Offset: 0x00138AAC
	public static MeigewwwParam Presentgetall(Presentgetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6C RID: 15980 RVA: 0x0013A6D0 File Offset: 0x00138AD0
	public static MeigewwwParam Presentgetall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6D RID: 15981 RVA: 0x0013A6F4 File Offset: 0x00138AF4
	public static MeigewwwParam Presentreceived(Presentreceived param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get_received", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6E RID: 15982 RVA: 0x0013A718 File Offset: 0x00138B18
	public static MeigewwwParam Presentreceived(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get_received", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E6F RID: 15983 RVA: 0x0013A73C File Offset: 0x00138B3C
	public static MeigewwwParam Presentget(Presentget param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("managedPresentId", param.managedPresentId);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E70 RID: 15984 RVA: 0x0013A784 File Offset: 0x00138B84
	public static MeigewwwParam Presentget(string managedPresentId, int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/present/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("managedPresentId", managedPresentId);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003E71 RID: 15985 RVA: 0x0013A7C4 File Offset: 0x00138BC4
	public static MeigewwwParam Staminaadd(Staminaadd param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/stamina/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("type", param.type);
		meigewwwParam.Add("num", param.num);
		meigewwwParam.Add("itemId", param.itemId);
		return meigewwwParam;
	}

	// Token: 0x06003E72 RID: 15986 RVA: 0x0013A828 File Offset: 0x00138C28
	public static MeigewwwParam Staminaadd(int type, int num, int itemId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/stamina/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("type", type);
		meigewwwParam.Add("num", num);
		meigewwwParam.Add("itemId", itemId);
		return meigewwwParam;
	}
}
