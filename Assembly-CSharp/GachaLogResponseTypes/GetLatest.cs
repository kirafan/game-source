﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace GachaLogResponseTypes
{
	// Token: 0x02000C28 RID: 3112
	public class GetLatest : CommonResponse
	{
		// Token: 0x06004009 RID: 16393 RVA: 0x0013D890 File Offset: 0x0013BC90
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.gachaResults == null) ? string.Empty : this.gachaResults.ToString());
		}

		// Token: 0x040045E1 RID: 17889
		public GachaResult[] gachaResults;
	}
}
