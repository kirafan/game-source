﻿using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

// Token: 0x02000FA5 RID: 4005
public static class Helper
{
	// Token: 0x060052BC RID: 21180 RVA: 0x00172168 File Offset: 0x00170568
	public static void ShowUsingMemory()
	{
	}

	// Token: 0x060052BD RID: 21181 RVA: 0x0017216A File Offset: 0x0017056A
	public static void Break(bool b, string msg)
	{
	}

	// Token: 0x060052BE RID: 21182 RVA: 0x0017216C File Offset: 0x0017056C
	public static void Log(string msg)
	{
	}

	// Token: 0x060052BF RID: 21183 RVA: 0x00172170 File Offset: 0x00170570
	public static float CalculateLinearTangent(AnimationCurve curve, int index, int toIndex)
	{
		return (float)(((double)curve[index].value - (double)curve[toIndex].value) / ((double)curve[index].time - (double)curve[toIndex].time));
	}

	// Token: 0x060052C0 RID: 21184 RVA: 0x001721C1 File Offset: 0x001705C1
	public static void RandomInit()
	{
		MathFunc.InitRandom();
	}

	// Token: 0x060052C1 RID: 21185 RVA: 0x001721C8 File Offset: 0x001705C8
	public static float GetFRandom(float min, float max)
	{
		return UnityEngine.Random.Range(min, max);
	}

	// Token: 0x060052C2 RID: 21186 RVA: 0x001721D1 File Offset: 0x001705D1
	public static float GetFRandom()
	{
		return UnityEngine.Random.value;
	}

	// Token: 0x060052C3 RID: 21187 RVA: 0x001721D8 File Offset: 0x001705D8
	public static int GetIRandom(int min, int max)
	{
		if (min > max)
		{
			int num = min;
			min = max;
			max = num;
		}
		return Helper.GetIRandom() % (max - min + 1) + min;
	}

	// Token: 0x060052C4 RID: 21188 RVA: 0x00172201 File Offset: 0x00170601
	public static int GetIRandom()
	{
		return (int)(UnityEngine.Random.value * 8388607f);
	}

	// Token: 0x060052C5 RID: 21189 RVA: 0x0017220F File Offset: 0x0017060F
	public static float GetDegreeRandom()
	{
		return Helper.GetFRandom(-180f, 180f);
	}

	// Token: 0x060052C6 RID: 21190 RVA: 0x00172220 File Offset: 0x00170620
	public static AnimationState InterpolateAnimationByFrame(Animation anim, string nextAnimName, float blendFrame, WrapMode wrap)
	{
		return Helper.InterpolateAnimation(anim, nextAnimName, Frame.Instance.FrameToSecond(blendFrame), wrap);
	}

	// Token: 0x060052C7 RID: 21191 RVA: 0x00172238 File Offset: 0x00170638
	public static AnimationState InterpolateAnimation(Animation anim, string nextAnimName, float blendSec, WrapMode wrap)
	{
		AnimationState animationState = anim[nextAnimName];
		Helper.Break(animationState != null, "[InterpolateAnimation]not found " + nextAnimName);
		if (animationState == null)
		{
			return null;
		}
		animationState.wrapMode = ((wrap != WrapMode.Once) ? wrap : WrapMode.ClampForever);
		anim.CrossFade(nextAnimName, blendSec, PlayMode.StopSameLayer);
		animationState.time = 0f;
		return animationState;
	}

	// Token: 0x060052C8 RID: 21192 RVA: 0x0017229C File Offset: 0x0017069C
	public static bool IsAnimationPlaying(AnimationState animState)
	{
		return !(animState == null) && (animState.wrapMode == WrapMode.Loop || (animState.enabled && animState.time < animState.length));
	}

	// Token: 0x060052C9 RID: 21193 RVA: 0x001722EC File Offset: 0x001706EC
	public static void PauseAnimation(Animation anim, bool bPause)
	{
		float speed = (!bPause) ? 1f : 0f;
		IEnumerator enumerator = anim.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				AnimationState animationState = (AnimationState)obj;
				animationState.speed = speed;
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	// Token: 0x060052CA RID: 21194 RVA: 0x00172364 File Offset: 0x00170764
	public static DifferentialAnimHandler GetShapeAndDifferentialAnimation(out Animation shapeAnim, out Animation differentialAnim, GameObject go)
	{
		shapeAnim = null;
		differentialAnim = null;
		DifferentialAnimHandler componentInChildren = go.GetComponentInChildren<DifferentialAnimHandler>();
		if (componentInChildren != null)
		{
			differentialAnim = componentInChildren.GetAnimation();
		}
		foreach (Animation animation in go.GetComponentsInChildren<Animation>())
		{
			if (!animation.Equals(differentialAnim))
			{
				shapeAnim = animation;
				break;
			}
		}
		return componentInChildren;
	}

	// Token: 0x060052CB RID: 21195 RVA: 0x001723CC File Offset: 0x001707CC
	public static string GetMeshNameByRenderer(Renderer rd)
	{
		MeshFilter component = rd.gameObject.GetComponent<MeshFilter>();
		if (component != null)
		{
			if (component.sharedMesh == null)
			{
				return null;
			}
			return component.sharedMesh.name;
		}
		else
		{
			SkinnedMeshRenderer skinnedMeshRenderer = rd as SkinnedMeshRenderer;
			if (!(skinnedMeshRenderer != null))
			{
				return null;
			}
			if (skinnedMeshRenderer.sharedMesh == null)
			{
				return null;
			}
			return skinnedMeshRenderer.sharedMesh.name;
		}
	}

	// Token: 0x060052CC RID: 21196 RVA: 0x00172443 File Offset: 0x00170843
	public static Material SearchMaterial(Renderer[] rdArray, string matName)
	{
		return Helper.SearchMaterial(rdArray, null, matName);
	}

	// Token: 0x060052CD RID: 21197 RVA: 0x00172450 File Offset: 0x00170850
	public static Material SearchMaterial(Renderer[] rdArray, string goName, string matName)
	{
		foreach (Renderer renderer in rdArray)
		{
			if (goName == null || renderer.gameObject.name.Equals(goName))
			{
				foreach (Material material in renderer.materials)
				{
					if (matName == material.name)
					{
						return material;
					}
					string text = new string(material.name.ToCharArray());
					if (text.Contains(" (Instance)"))
					{
						text = text.Replace(" (Instance)", string.Empty);
					}
					if (text == matName)
					{
						return material;
					}
				}
			}
		}
		return null;
	}

	// Token: 0x060052CE RID: 21198 RVA: 0x00172518 File Offset: 0x00170918
	public static void SetRenderOrder(GameObject go, int renderOrder)
	{
		Helper.SetRenderOrder(go.GetComponentsInChildren<Renderer>(), renderOrder);
	}

	// Token: 0x060052CF RID: 21199 RVA: 0x00172528 File Offset: 0x00170928
	public static void SetRenderOrder(Renderer[] mrArray, int renderOrder)
	{
		foreach (Renderer renderer in mrArray)
		{
			foreach (Material mat in renderer.materials)
			{
				Helper.SetRenderOrder(mat, renderOrder);
			}
		}
	}

	// Token: 0x060052D0 RID: 21200 RVA: 0x00172580 File Offset: 0x00170980
	public static void SetRenderOrder(Renderer mr, int renderOrder)
	{
		foreach (Material mat in mr.materials)
		{
			Helper.SetRenderOrder(mat, renderOrder);
		}
	}

	// Token: 0x060052D1 RID: 21201 RVA: 0x001725B3 File Offset: 0x001709B3
	public static void SetRenderOrder(Material mat, int renderOrder)
	{
		mat.renderQueue = renderOrder;
	}

	// Token: 0x060052D2 RID: 21202 RVA: 0x001725BC File Offset: 0x001709BC
	public static void SetRenderColor(Renderer[] mrArray, Color color)
	{
		foreach (Renderer renderer in mrArray)
		{
			foreach (Material material in renderer.materials)
			{
				material.SetColor("_Color", color);
			}
		}
	}

	// Token: 0x060052D3 RID: 21203 RVA: 0x00172618 File Offset: 0x00170A18
	public static void SetRenderColor(Renderer mr, Color color)
	{
		foreach (Material mat in mr.materials)
		{
			Helper.SetMaterialColor(mat, color);
		}
	}

	// Token: 0x060052D4 RID: 21204 RVA: 0x0017264B File Offset: 0x00170A4B
	public static void SetMaterialColor(Material mat, Color color)
	{
		mat.SetColor("_Color", color);
	}

	// Token: 0x060052D5 RID: 21205 RVA: 0x0017265C File Offset: 0x00170A5C
	public static void SetTextureOffset(Renderer[] mrArray, Vector2 offs)
	{
		foreach (Renderer renderer in mrArray)
		{
			foreach (Material material in renderer.materials)
			{
				material.SetTextureOffset("_MainTex", offs);
			}
		}
	}

	// Token: 0x060052D6 RID: 21206 RVA: 0x001726B8 File Offset: 0x00170AB8
	public static void EnableRender(Renderer[] mrArray, bool bEnable)
	{
		if (mrArray == null)
		{
			return;
		}
		foreach (Renderer renderer in mrArray)
		{
			if (renderer != null)
			{
				renderer.enabled = bEnable;
			}
		}
	}

	// Token: 0x060052D7 RID: 21207 RVA: 0x001726F9 File Offset: 0x00170AF9
	public static void EnableRender(Renderer mr, bool bEnable)
	{
		if (mr != null)
		{
			mr.enabled = bEnable;
		}
	}

	// Token: 0x060052D8 RID: 21208 RVA: 0x0017270E File Offset: 0x00170B0E
	public static bool CalcProbability(float freq)
	{
		return freq > 0f && Helper.GetFRandom() <= freq;
	}

	// Token: 0x060052D9 RID: 21209 RVA: 0x00172730 File Offset: 0x00170B30
	public static T CopyComponent<T>(T original, GameObject destination) where T : Component
	{
		Type type = original.GetType();
		Component component = destination.AddComponent(type);
		FieldInfo[] fields = type.GetFields();
		foreach (FieldInfo fieldInfo in fields)
		{
			fieldInfo.SetValue(component, fieldInfo.GetValue(original));
		}
		return component as T;
	}

	// Token: 0x060052DA RID: 21210 RVA: 0x0017279A File Offset: 0x00170B9A
	public static void DestroyAll(GameObject go)
	{
		Helper.DestroyAll(go, 1f);
	}

	// Token: 0x060052DB RID: 21211 RVA: 0x001727A7 File Offset: 0x00170BA7
	public static void DestroyAll(GameObject go, float frame)
	{
		Helper.DestroyAll(go, frame, true);
	}

	// Token: 0x060052DC RID: 21212 RVA: 0x001727B4 File Offset: 0x00170BB4
	public static void DestroyAll(GameObject go, float frame, bool unloadAssets)
	{
		if (go == null)
		{
			return;
		}
		Transform[] componentsInChildren = go.GetComponentsInChildren<Transform>();
		foreach (Transform transform in componentsInChildren)
		{
			GameObject gameObject = transform.gameObject;
			if (gameObject.GetComponent<Rigidbody>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<Rigidbody>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<Camera>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<Camera>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<Light>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<Light>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<Animation>() != null)
			{
				IEnumerator enumerator = gameObject.GetComponent<Animation>().GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						AnimationState animationState = (AnimationState)obj;
						gameObject.GetComponent<Animation>().RemoveClip(animationState.clip);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				UnityEngine.Object.Destroy(gameObject.GetComponent<Animation>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<ConstantForce>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<ConstantForce>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<Renderer>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<Renderer>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<AudioSource>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<AudioSource>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<GUIText>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<GUIText>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<NetworkView>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<NetworkView>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<GUITexture>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<GUITexture>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<Collider>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<Collider>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<HingeJoint>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<HingeJoint>(), Frame.Instance.FrameToSecond(frame));
			}
			if (gameObject.GetComponent<ParticleSystem>() != null)
			{
				UnityEngine.Object.Destroy(gameObject.GetComponent<ParticleSystem>(), Frame.Instance.FrameToSecond(frame));
			}
			UnityEngine.Object.Destroy(gameObject, Frame.Instance.FrameToSecond(frame));
		}
		UnityEngine.Object.Destroy(go, Frame.Instance.FrameToSecond(frame));
		if (unloadAssets)
		{
			Resources.UnloadUnusedAssets();
		}
	}

	// Token: 0x060052DD RID: 21213 RVA: 0x00172AA8 File Offset: 0x00170EA8
	public static string CheckAvailableString(string str)
	{
		if (str != null)
		{
			return (str.Length > 0) ? str : null;
		}
		return null;
	}

	// Token: 0x060052DE RID: 21214 RVA: 0x00172AC8 File Offset: 0x00170EC8
	public static bool CheckFieldTouch(out RaycastHit hitInfo, Vector3 position, int layerMask)
	{
		Vector3 origin = default(Vector3);
		origin = position;
		origin.y = 100f;
		Vector3 direction = Vector3.up * -1f;
		float maxDistance = 10000f;
		return Physics.Raycast(origin, direction, out hitInfo, maxDistance, layerMask);
	}

	// Token: 0x060052DF RID: 21215 RVA: 0x00172B0C File Offset: 0x00170F0C
	public static bool CheckFieldTouch(out RaycastHit[] hitInfoArray, Vector3 position, int layerMask)
	{
		Vector3 origin = default(Vector3);
		origin = position;
		origin.y = 100f;
		Vector3 direction = Vector3.up * -1f;
		float maxDistance = 10000f;
		hitInfoArray = Physics.RaycastAll(origin, direction, maxDistance, layerMask);
		return hitInfoArray.Length > 0;
	}

	// Token: 0x060052E0 RID: 21216 RVA: 0x00172B58 File Offset: 0x00170F58
	public static bool CollisionDetectionByScreenRayScattering(Camera cam, Vector3 cam_pos, Vector2 scrPosS, Vector2 scrPosD, float fPrecisionOfAngle, int layerMask)
	{
		Ray rayS = cam.ScreenPointToRay(scrPosS);
		Ray rayD = cam.ScreenPointToRay(scrPosD);
		return Helper.CollisionDetectionByScreenRayScattering(cam_pos, rayS, rayD, fPrecisionOfAngle, layerMask);
	}

	// Token: 0x060052E1 RID: 21217 RVA: 0x00172B8C File Offset: 0x00170F8C
	public static bool CollisionDetectionByScreenRayScattering(Vector3 origin, Ray rayS, Ray rayD, float fPrecisionOfAngle, int layerMask)
	{
		float num = Vector3.Angle(rayD.direction, rayS.direction);
		if (Physics.Raycast(origin, rayS.direction, float.PositiveInfinity, layerMask))
		{
			return true;
		}
		if (Physics.Raycast(origin, rayD.direction, float.PositiveInfinity, layerMask))
		{
			return true;
		}
		Vector3 vector = default(Vector3);
		for (float num2 = fPrecisionOfAngle; num2 < num; num2 += fPrecisionOfAngle)
		{
			if (Physics.Raycast(origin, Vector3.Lerp(rayS.direction, rayD.direction, num2 / num).normalized, float.PositiveInfinity, layerMask))
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060052E2 RID: 21218 RVA: 0x00172C30 File Offset: 0x00171030
	public static void CalculateWorldPosOfScreen(out Vector3 vCenter, out float width, out float height, Camera cam, Transform camTrfm, float zOffs)
	{
		Vector3 zero = Vector3.zero;
		zero.y = (float)Screen.height;
		Vector3 direction = cam.ScreenPointToRay(zero).direction;
		Vector3 vector = -camTrfm.right;
		Vector3 up = camTrfm.up;
		Vector3 forward = camTrfm.forward;
		Vector3 position = camTrfm.position;
		float num = MathFunc.Dot(ref direction, ref forward);
		float num2 = MathFunc.Dot(ref direction, ref vector);
		float num3 = MathFunc.Dot(ref direction, ref up);
		width = num2 * zOffs / num * 2f;
		height = num3 * zOffs / num * 2f;
		width *= GameScreen.Instance.screenSpace.fixedAspectSize.x / GameScreen.Instance.screenSpace.size.x;
		height *= GameScreen.Instance.screenSpace.fixedAspectSize.y / GameScreen.Instance.screenSpace.size.y;
		vCenter = position + forward * zOffs;
	}

	// Token: 0x060052E3 RID: 21219 RVA: 0x00172D4F File Offset: 0x0017114F
	public static bool IsNonRetina()
	{
		return false;
	}

	// Token: 0x060052E4 RID: 21220 RVA: 0x00172D54 File Offset: 0x00171154
	public static Color ParseColorCode(string code)
	{
		if (!string.IsNullOrEmpty(code) && code[0] == '#')
		{
			string text = code.Replace("#", string.Empty);
			if (text.Length <= 6)
			{
				text += "FF";
			}
			int value = Convert.ToInt32(text, 16);
			byte[] bytes = BitConverter.GetBytes(value);
			Color result = new Color((float)bytes[3] / 255f, (float)bytes[2] / 255f, (float)bytes[1] / 255f, (float)bytes[0] / 255f);
			return result;
		}
		return Color.white;
	}

	// Token: 0x060052E5 RID: 21221 RVA: 0x00172DEC File Offset: 0x001711EC
	public static string GetUniqueIDForVendor()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getContentResolver", new object[0]);
		AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("android.provider.Settings$Secure");
		return androidJavaClass2.CallStatic<string>("getString", new object[]
		{
			androidJavaObject,
			"android_id"
		});
	}

	// Token: 0x02000FA6 RID: 4006
	public enum eTangentMode
	{
		// Token: 0x04005423 RID: 21539
		eTangentMode_Editable,
		// Token: 0x04005424 RID: 21540
		eTangentMode_Smooth,
		// Token: 0x04005425 RID: 21541
		eTangentMode_Linear,
		// Token: 0x04005426 RID: 21542
		eTangentMode_Stepped
	}
}
