﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000FA7 RID: 4007
public class InitApplication : MonoBehaviour
{
	// Token: 0x060052E7 RID: 21223 RVA: 0x00172E56 File Offset: 0x00171256
	private void Awake()
	{
	}

	// Token: 0x060052E8 RID: 21224 RVA: 0x00172E58 File Offset: 0x00171258
	private void FixedUpdate()
	{
		this.m_waitSecWork += Time.deltaTime;
		if (this.m_waitSecWork >= 1f)
		{
			SceneManager.LoadScene(1);
		}
	}

	// Token: 0x04005427 RID: 21543
	private const float m_waitSec = 1f;

	// Token: 0x04005428 RID: 21544
	protected float m_waitSecWork;
}
