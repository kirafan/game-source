﻿using System;
using GachaRequestTypes;
using Meige;

// Token: 0x02000B60 RID: 2912
public static class GachaRequest
{
	// Token: 0x06003DFF RID: 15871 RVA: 0x00138C58 File Offset: 0x00137058
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("gachaIds", param.gachaIds);
		return meigewwwParam;
	}

	// Token: 0x06003E00 RID: 15872 RVA: 0x00138C8C File Offset: 0x0013708C
	public static MeigewwwParam GetAll(string gachaIds, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("gachaIds", gachaIds);
		return meigewwwParam;
	}

	// Token: 0x06003E01 RID: 15873 RVA: 0x00138CBC File Offset: 0x001370BC
	public static MeigewwwParam Draw(Draw param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/draw", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("gachaId", param.gachaId);
		meigewwwParam.Add("drawType", param.drawType);
		meigewwwParam.Add("stepCode", param.stepCode);
		meigewwwParam.Add("reDraw", param.reDraw);
		return meigewwwParam;
	}

	// Token: 0x06003E02 RID: 15874 RVA: 0x00138D38 File Offset: 0x00137138
	public static MeigewwwParam Draw(int gachaId, int drawType, int stepCode, bool reDraw, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/draw", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("gachaId", gachaId);
		meigewwwParam.Add("drawType", drawType);
		meigewwwParam.Add("stepCode", stepCode);
		meigewwwParam.Add("reDraw", reDraw);
		return meigewwwParam;
	}

	// Token: 0x06003E03 RID: 15875 RVA: 0x00138DA0 File Offset: 0x001371A0
	public static MeigewwwParam Stepdraw(Stepdraw param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/step/draw", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("gachaId", param.gachaId);
		return meigewwwParam;
	}

	// Token: 0x06003E04 RID: 15876 RVA: 0x00138DD8 File Offset: 0x001371D8
	public static MeigewwwParam Stepdraw(int gachaId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/step/draw", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("gachaId", gachaId);
		return meigewwwParam;
	}

	// Token: 0x06003E05 RID: 15877 RVA: 0x00138E0C File Offset: 0x0013720C
	public static MeigewwwParam GetBox(GetBox param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/box/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("gachaId", param.gachaId);
		return meigewwwParam;
	}

	// Token: 0x06003E06 RID: 15878 RVA: 0x00138E44 File Offset: 0x00137244
	public static MeigewwwParam GetBox(int gachaId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha/box/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("gachaId", gachaId);
		return meigewwwParam;
	}
}
