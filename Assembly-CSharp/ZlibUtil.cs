﻿using System;
using System.IO;
using zlib;

// Token: 0x02000E72 RID: 3698
public class ZlibUtil
{
	// Token: 0x06004C86 RID: 19590 RVA: 0x001550C0 File Offset: 0x001534C0
	public static byte[] compress(byte[] inbuf, int level = 6)
	{
		byte[] result = null;
		ZStream zstream = new ZStream();
		MemoryStream memoryStream = new MemoryStream();
		try
		{
			byte[] array = new byte[2048];
			if (zstream.deflateInit(level, ZlibUtil.bits) != 0)
			{
				return null;
			}
			zstream.next_in = inbuf;
			zstream.avail_in = inbuf.Length;
			zstream.next_in_index = 0;
			zstream.next_out = array;
			zstream.avail_out = array.Length;
			zstream.next_out_index = 0;
			for (;;)
			{
				int num = zstream.deflate(4);
				if (num == 1)
				{
					break;
				}
				if (num != 0)
				{
					goto Block_6;
				}
				if (zstream.avail_out == 0)
				{
					memoryStream.Write(array, 0, array.Length);
					zstream.next_out = array;
					zstream.avail_out = array.Length;
					zstream.next_out_index = 0;
				}
			}
			if (array.Length - zstream.avail_out != 0)
			{
				memoryStream.Write(array, 0, array.Length - zstream.avail_out);
			}
			if (zstream.deflateEnd() == 0)
			{
				result = memoryStream.ToArray();
			}
			return result;
			Block_6:
			return null;
		}
		catch (Exception ex)
		{
			Console.Write("ZlibUtil.compress error : " + ex.ToString());
		}
		finally
		{
			zstream.free();
			memoryStream.Close();
		}
		return result;
	}

	// Token: 0x06004C87 RID: 19591 RVA: 0x00155208 File Offset: 0x00153608
	public static byte[] uncompress(byte[] inbuf)
	{
		byte[] result = null;
		ZStream zstream = new ZStream();
		MemoryStream memoryStream = new MemoryStream();
		try
		{
			byte[] array = new byte[2048];
			if (zstream.inflateInit(ZlibUtil.bits) != 0)
			{
				return null;
			}
			zstream.next_in = inbuf;
			zstream.avail_in = inbuf.Length;
			zstream.next_in_index = 0;
			zstream.next_out = array;
			zstream.avail_out = array.Length;
			zstream.next_out_index = 0;
			for (;;)
			{
				int num = zstream.inflate(0);
				if (num == 1)
				{
					break;
				}
				if (num != 0)
				{
					goto Block_6;
				}
				if (zstream.avail_out == 0)
				{
					memoryStream.Write(array, 0, array.Length);
					zstream.next_out = array;
					zstream.avail_out = array.Length;
					zstream.next_out_index = 0;
				}
			}
			if (array.Length - zstream.avail_out != 0)
			{
				memoryStream.Write(array, 0, array.Length - zstream.avail_out);
			}
			if (zstream.inflateEnd() == 0)
			{
				result = memoryStream.ToArray();
			}
			return result;
			Block_6:
			return null;
		}
		catch (Exception ex)
		{
			Console.Write("ZlibUtil.uncompress error : " + ex.ToString());
		}
		finally
		{
			memoryStream.Close();
			zstream.free();
		}
		return result;
	}

	// Token: 0x04004D75 RID: 19829
	public const int bufferSize = 2048;

	// Token: 0x04004D76 RID: 19830
	public static int bits = 15;
}
