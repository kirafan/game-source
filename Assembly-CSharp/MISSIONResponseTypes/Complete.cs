﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace MISSIONResponseTypes
{
	// Token: 0x02000C34 RID: 3124
	public class Complete : CommonResponse
	{
		// Token: 0x06004021 RID: 16417 RVA: 0x0013DDF0 File Offset: 0x0013C1F0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.presents == null) ? string.Empty : this.presents.ToString());
		}

		// Token: 0x040045FC RID: 17916
		public Player player;

		// Token: 0x040045FD RID: 17917
		public PlayerPresent[] presents;
	}
}
