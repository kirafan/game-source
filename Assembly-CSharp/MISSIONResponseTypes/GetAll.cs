﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace MISSIONResponseTypes
{
	// Token: 0x02000C32 RID: 3122
	public class GetAll : CommonResponse
	{
		// Token: 0x0600401D RID: 16413 RVA: 0x0013DD8C File Offset: 0x0013C18C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.missionLogs == null) ? string.Empty : this.missionLogs.ToString());
		}

		// Token: 0x040045FB RID: 17915
		public PlayerMissionLog[] missionLogs;
	}
}
