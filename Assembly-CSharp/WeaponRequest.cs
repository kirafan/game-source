﻿using System;
using Meige;
using WeaponRequestTypes;

// Token: 0x02000BF5 RID: 3061
public static class WeaponRequest
{
	// Token: 0x06003F51 RID: 16209 RVA: 0x0013C30C File Offset: 0x0013A70C
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003F52 RID: 16210 RVA: 0x0013C330 File Offset: 0x0013A730
	public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003F53 RID: 16211 RVA: 0x0013C354 File Offset: 0x0013A754
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("weaponId", param.weaponId);
		return meigewwwParam;
	}

	// Token: 0x06003F54 RID: 16212 RVA: 0x0013C388 File Offset: 0x0013A788
	public static MeigewwwParam Get(string weaponId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("weapon/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("weaponId", weaponId);
		return meigewwwParam;
	}
}
