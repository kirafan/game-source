﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

// Token: 0x02000FA3 RID: 4003
public class CSVReader
{
	// Token: 0x060052A4 RID: 21156 RVA: 0x00171E19 File Offset: 0x00170219
	public CSVReader(string path)
	{
		this.parse(path);
	}

	// Token: 0x060052A5 RID: 21157 RVA: 0x00171E28 File Offset: 0x00170228
	public void parse(string path)
	{
		this.m_path = path;
		Debug.Log(path + "\n");
		FileInfo fileInfo = new FileInfo(path);
		if (fileInfo == null || !fileInfo.Exists)
		{
			Debug.Log("[" + this.m_path + "] is not found");
			return;
		}
		StreamReader streamReader = new StreamReader(this.m_path);
		if (streamReader == null)
		{
			Debug.Log("[" + this.m_path + "] is not readable");
			return;
		}
		this.m_strings = new ArrayList();
		string text;
		while ((text = streamReader.ReadLine()) != null)
		{
			string[] value = text.Split(new char[]
			{
				","[0]
			});
			this.m_strings.Add(value);
		}
		streamReader.Close();
	}

	// Token: 0x060052A6 RID: 21158 RVA: 0x00171EFC File Offset: 0x001702FC
	public string get(int row, int col)
	{
		if (this.getRowSize() <= row)
		{
			return string.Empty;
		}
		string[] array = (string[])this.m_strings[row];
		if (array.Length <= col)
		{
			return string.Empty;
		}
		return array[col];
	}

	// Token: 0x060052A7 RID: 21159 RVA: 0x00171F3F File Offset: 0x0017033F
	public int getRowSize()
	{
		return this.m_strings.Count;
	}

	// Token: 0x060052A8 RID: 21160 RVA: 0x00171F4C File Offset: 0x0017034C
	public int getColumnSize(int row)
	{
		if (this.getRowSize() <= row)
		{
			return 0;
		}
		string[] array = (string[])this.m_strings[row];
		return array.Length;
	}

	// Token: 0x060052A9 RID: 21161 RVA: 0x00171F7C File Offset: 0x0017037C
	public int getInt(int row, int col)
	{
		string s = this.get(row, col);
		int result = 0;
		int.TryParse(s, out result);
		return result;
	}

	// Token: 0x060052AA RID: 21162 RVA: 0x00171FA0 File Offset: 0x001703A0
	public float getFloat(int row, int col)
	{
		string s = this.get(row, col);
		float result = 0f;
		float.TryParse(s, out result);
		return result;
	}

	// Token: 0x060052AB RID: 21163 RVA: 0x00171FC8 File Offset: 0x001703C8
	public double getDouble(int row, int col)
	{
		string s = this.get(row, col);
		double result = 0.0;
		double.TryParse(s, out result);
		return result;
	}

	// Token: 0x04005416 RID: 21526
	private string m_path;

	// Token: 0x04005417 RID: 21527
	private ArrayList m_strings;
}
