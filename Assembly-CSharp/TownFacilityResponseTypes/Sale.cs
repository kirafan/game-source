﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C89 RID: 3209
	public class Sale : CommonResponse
	{
		// Token: 0x060040CB RID: 16587 RVA: 0x0013FC94 File Offset: 0x0013E094
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.managedTownFacilities == null) ? string.Empty : this.managedTownFacilities.ToString());
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004691 RID: 18065
		public PlayerTownFacility[] managedTownFacilities;

		// Token: 0x04004692 RID: 18066
		public Player player;
	}
}
