﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C8A RID: 3210
	public class Limitadd : CommonResponse
	{
		// Token: 0x060040CD RID: 16589 RVA: 0x0013FD00 File Offset: 0x0013E100
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004693 RID: 18067
		public Player player;
	}
}
