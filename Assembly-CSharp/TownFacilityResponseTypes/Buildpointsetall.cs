﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C81 RID: 3201
	public class Buildpointsetall : CommonResponse
	{
		// Token: 0x060040BB RID: 16571 RVA: 0x0013FAB8 File Offset: 0x0013DEB8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x0400468B RID: 18059
		public Player player;
	}
}
