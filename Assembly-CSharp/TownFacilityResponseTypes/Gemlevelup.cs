﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C85 RID: 3205
	public class Gemlevelup : CommonResponse
	{
		// Token: 0x060040C3 RID: 16579 RVA: 0x0013FB80 File Offset: 0x0013DF80
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x0400468D RID: 18061
		public Player player;
	}
}
