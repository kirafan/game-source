﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C83 RID: 3203
	public class Preparelevelup : CommonResponse
	{
		// Token: 0x060040BF RID: 16575 RVA: 0x0013FB1C File Offset: 0x0013DF1C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x0400468C RID: 18060
		public Player player;
	}
}
