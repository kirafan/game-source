﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C7F RID: 3199
	public class Buyall : CommonResponse
	{
		// Token: 0x060040B7 RID: 16567 RVA: 0x0013FA08 File Offset: 0x0013DE08
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.managedTownFacilityIds == null) ? string.Empty : this.managedTownFacilityIds.ToString());
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004688 RID: 18056
		public string managedTownFacilityIds;

		// Token: 0x04004689 RID: 18057
		public Player player;
	}
}
