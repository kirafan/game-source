﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C86 RID: 3206
	public class Itemup : CommonResponse
	{
		// Token: 0x060040C5 RID: 16581 RVA: 0x0013FBC4 File Offset: 0x0013DFC4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
		}

		// Token: 0x0400468E RID: 18062
		public Player player;

		// Token: 0x0400468F RID: 18063
		public PlayerItemSummary[] itemSummary;
	}
}
