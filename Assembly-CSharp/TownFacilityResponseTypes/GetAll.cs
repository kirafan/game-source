﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C87 RID: 3207
	public class GetAll : CommonResponse
	{
		// Token: 0x060040C7 RID: 16583 RVA: 0x0013FC30 File Offset: 0x0013E030
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedTownFacilities == null) ? string.Empty : this.managedTownFacilities.ToString());
		}

		// Token: 0x04004690 RID: 18064
		public PlayerTownFacility[] managedTownFacilities;
	}
}
