﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C80 RID: 3200
	public class Buildpointset : CommonResponse
	{
		// Token: 0x060040B9 RID: 16569 RVA: 0x0013FA74 File Offset: 0x0013DE74
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x0400468A RID: 18058
		public Player player;
	}
}
