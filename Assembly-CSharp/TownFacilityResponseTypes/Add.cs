﻿using System;
using CommonResponseTypes;

namespace TownFacilityResponseTypes
{
	// Token: 0x02000C7E RID: 3198
	public class Add : CommonResponse
	{
		// Token: 0x060040B5 RID: 16565 RVA: 0x0013F9C4 File Offset: 0x0013DDC4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedTownFacilityIds == null) ? string.Empty : this.managedTownFacilityIds.ToString());
		}

		// Token: 0x04004687 RID: 18055
		public string managedTownFacilityIds;
	}
}
