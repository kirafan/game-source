﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000F7A RID: 3962
public class MeigeShaderUtility
{
	// Token: 0x0600522D RID: 21037 RVA: 0x0017024B File Offset: 0x0016E64B
	public static int GetPropertyID(string propertyName)
	{
		return Shader.PropertyToID(propertyName);
	}

	// Token: 0x0600522E RID: 21038 RVA: 0x00170254 File Offset: 0x0016E654
	public static void MakeBlendingParam(Material material, eBlendMode blendMode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha)
	{
		if (blendMode == eBlendMode.eBlendMode_Invalid)
		{
			Helper.Break(false, "MakeBlendingParam .... BlendMode is Invalid.");
			src = eBlendFactor.SrcColor;
			dst = eBlendFactor.Zero;
			opColor = eBlendOp.Add;
			opAlpha = eBlendOp.Add;
			return;
		}
		src = MeigeShaderUtility.m_blendComponent[(int)blendMode].m_BlendFactorSrc;
		dst = MeigeShaderUtility.m_blendComponent[(int)blendMode].m_BlendFactorDst;
		opColor = MeigeShaderUtility.m_blendComponent[(int)blendMode].m_BlendOp;
		opAlpha = eBlendOp.Add;
	}

	// Token: 0x0600522F RID: 21039 RVA: 0x001702C0 File Offset: 0x0016E6C0
	public static void GetBlendingParam(Material material, out eBlendMode mode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha)
	{
		src = (eBlendFactor)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[2]);
		dst = (eBlendFactor)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[3]);
		opColor = (eBlendOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[4]);
		opAlpha = (eBlendOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[5]);
		mode = (eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[1]);
	}

	// Token: 0x06005230 RID: 21040 RVA: 0x00170320 File Offset: 0x0016E720
	public static eBlendMode ConvertLayerBlend(eLayerBlendMode mode)
	{
		eBlendMode[] array = new eBlendMode[]
		{
			eBlendMode.eBlendMode_Std,
			eBlendMode.eBlendMode_Std,
			eBlendMode.eBlendMode_Add,
			eBlendMode.eBlendMode_Sub,
			eBlendMode.eBlendMode_Mul,
			eBlendMode.eBlendMode_SrcOne,
			eBlendMode.eBlendMode_DstOne
		};
		return array[(int)mode];
	}

	// Token: 0x06005231 RID: 21041 RVA: 0x00170344 File Offset: 0x0016E744
	public static eLayerBlendMode ConvertAlphaBlend(eBlendMode mode)
	{
		eLayerBlendMode[] array = new eLayerBlendMode[]
		{
			eLayerBlendMode.eLayerBlendMode_Std,
			eLayerBlendMode.eLayerBlendMode_Std,
			eLayerBlendMode.eLayerBlendMode_Add,
			eLayerBlendMode.eLayerBlendMode_Sub,
			eLayerBlendMode.eLayerBlendMode_Std,
			eLayerBlendMode.eLayerBlendMode_SrcOne,
			eLayerBlendMode.eLayerBlendMode_DstOne,
			eLayerBlendMode.eLayerBlendMode_Mul,
			eLayerBlendMode.eLayerBlendMode_Std
		};
		return array[(int)mode];
	}

	// Token: 0x06005232 RID: 21042 RVA: 0x00170368 File Offset: 0x0016E768
	public static void SaveBlendingParam(Material material, eBlendMode mode)
	{
		if (mode == eBlendMode.eBlendMode_Custom)
		{
			return;
		}
		eBlendFactor src;
		eBlendFactor dst;
		eBlendOp opColor;
		eBlendOp opAlpha;
		MeigeShaderUtility.MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendMode, (float)mode);
		MeigeShaderUtility.SaveBlendingParam(material, src, dst, opColor, opAlpha);
	}

	// Token: 0x06005233 RID: 21043 RVA: 0x001703A4 File Offset: 0x0016E7A4
	public static void SaveBlendingParam(Material material, eBlendMode mode, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha)
	{
		if (mode != eBlendMode.eBlendMode_Custom)
		{
			MeigeShaderUtility.MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
		}
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendMode, (float)mode);
		MeigeShaderUtility.SaveBlendingParam(material, src, dst, opColor, opAlpha);
	}

	// Token: 0x06005234 RID: 21044 RVA: 0x001703D6 File Offset: 0x0016E7D6
	public static void SaveBlendingParam(Material material, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendSrc, (float)src);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendDst, (float)dst);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendOpColor, (float)opColor);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendOpAlpha, (float)opAlpha);
	}

	// Token: 0x06005235 RID: 21045 RVA: 0x00170414 File Offset: 0x0016E814
	public static void SaveOutlineBlendingParam(Material material, eBlendMode mode)
	{
		if (mode == eBlendMode.eBlendMode_Custom)
		{
			return;
		}
		eBlendFactor src;
		eBlendFactor dst;
		eBlendOp opColor;
		eBlendOp opAlpha;
		MeigeShaderUtility.MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendMode, (float)mode);
		MeigeShaderUtility.SaveOutlineBlendingParam(material, src, dst, opColor, opAlpha);
	}

	// Token: 0x06005236 RID: 21046 RVA: 0x00170451 File Offset: 0x0016E851
	public static void SaveOutlineBlendingParam(Material material, eBlendMode mode, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha)
	{
		if (mode != eBlendMode.eBlendMode_Custom)
		{
			MeigeShaderUtility.MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
		}
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendMode, (float)mode);
		MeigeShaderUtility.SaveOutlineBlendingParam(material, src, dst, opColor, opAlpha);
	}

	// Token: 0x06005237 RID: 21047 RVA: 0x00170484 File Offset: 0x0016E884
	public static void SaveOutlineBlendingParam(Material material, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendSrc, (float)src);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendDst, (float)dst);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendOpColor, (float)opColor);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineBlendOpAlpha, (float)opAlpha);
	}

	// Token: 0x06005238 RID: 21048 RVA: 0x001704C3 File Offset: 0x0016E8C3
	public static string GetShaderPropertyName(MeigeShaderUtility.eShaderPropertyID propertyID)
	{
		return MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID];
	}

	// Token: 0x06005239 RID: 21049 RVA: 0x001704CC File Offset: 0x0016E8CC
	public static void SaveShaderProperty(Material material, MeigeShaderUtility.eShaderPropertyID propertyID, object value)
	{
		if (value == null)
		{
			return;
		}
		Type type = value.GetType();
		if (type == typeof(float))
		{
			float value2 = (float)value;
			material.SetFloat(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], value2);
		}
		else if (type == typeof(Texture) || type == typeof(Texture2D) || type == typeof(Texture3D) || type == typeof(RenderTexture))
		{
			Texture texture = (Texture)value;
			material.SetTexture(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], texture);
		}
		else if (type == typeof(Matrix4x4))
		{
			Matrix4x4 matrix = (Matrix4x4)value;
			material.SetMatrix(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], matrix);
		}
		else if (type == typeof(Vector4))
		{
			Vector4 vector = (Vector4)value;
			material.SetVector(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], vector);
		}
		else if (type == typeof(Color))
		{
			Color color = (Color)value;
			material.SetColor(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], color);
		}
		else
		{
			int value3 = (int)value;
			material.SetInt(MeigeShaderUtility.m_ShaderPropertyName[(int)propertyID], value3);
		}
	}

	// Token: 0x0600523A RID: 21050 RVA: 0x00170604 File Offset: 0x0016EA04
	public static void EnableKeyword(Material material, string[] keywords, int value)
	{
		for (int i = 0; i < keywords.Length; i++)
		{
			if (i == value)
			{
				material.EnableKeyword(keywords[i]);
			}
			else
			{
				material.DisableKeyword(keywords[i]);
			}
		}
	}

	// Token: 0x0600523B RID: 21051 RVA: 0x00170643 File Offset: 0x0016EA43
	public static void SetShadingType(Material material, eShadingType value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_ShaderType, (int)value);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.ShadingType, (float)value);
	}

	// Token: 0x0600523C RID: 21052 RVA: 0x0017065F File Offset: 0x0016EA5F
	public static void SetAlphaBlendMode(Material material, eBlendMode value)
	{
		MeigeShaderUtility.SaveBlendingParam(material, value);
	}

	// Token: 0x0600523D RID: 21053 RVA: 0x00170668 File Offset: 0x0016EA68
	public static void SetAlphaBlendSrcFactor(Material material, eBlendFactor value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendSrc, (float)value);
	}

	// Token: 0x0600523E RID: 21054 RVA: 0x00170678 File Offset: 0x0016EA78
	public static void SetAlphaBlendDstFactor(Material material, eBlendFactor value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendDst, (float)value);
	}

	// Token: 0x0600523F RID: 21055 RVA: 0x00170688 File Offset: 0x0016EA88
	public static void SetAlphaBlendOpColor(Material material, eBlendOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendOpColor, (float)value);
	}

	// Token: 0x06005240 RID: 21056 RVA: 0x00170698 File Offset: 0x0016EA98
	public static void SetAlphaBlendOpAlpha(Material material, eBlendOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.BlendOpAlpha, (float)value);
	}

	// Token: 0x06005241 RID: 21057 RVA: 0x001706A8 File Offset: 0x0016EAA8
	public static void SetDepthTest(Material material, eCompareFunc value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.DepthTest, (float)value);
	}

	// Token: 0x06005242 RID: 21058 RVA: 0x001706B8 File Offset: 0x0016EAB8
	public static void SetEnableDepthWrite(Material material, bool value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.DepthWrite, (!value) ? 0f : 1f);
	}

	// Token: 0x06005243 RID: 21059 RVA: 0x001706DB File Offset: 0x0016EADB
	public static void SetDepthOffsetFactor(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.DepthOffsetFactor, value);
	}

	// Token: 0x06005244 RID: 21060 RVA: 0x001706EB File Offset: 0x0016EAEB
	public static void SetDepthOffsetUnits(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.DepthOffsetUnits, value);
	}

	// Token: 0x06005245 RID: 21061 RVA: 0x001706FC File Offset: 0x0016EAFC
	public static void SetEnableAlphaTest(Material material, bool value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_AlphaTest, (!value) ? 0 : 1);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_AlphaTest, (!value) ? 0f : 1f);
	}

	// Token: 0x06005246 RID: 21062 RVA: 0x00170738 File Offset: 0x0016EB38
	public static void SetCullMode(Material material, eCullMode value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.CullMode, (float)value);
	}

	// Token: 0x06005247 RID: 21063 RVA: 0x00170749 File Offset: 0x0016EB49
	public static void SetLightingType(Material material, int value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_Lighting, value);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Lighting_Type, (float)value);
	}

	// Token: 0x06005248 RID: 21064 RVA: 0x00170766 File Offset: 0x0016EB66
	public static void SetUsingFog(Material material, bool value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_Fog, (!value) ? 0 : 1);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_Fog, (!value) ? 0f : 1f);
	}

	// Token: 0x06005249 RID: 21065 RVA: 0x001707A2 File Offset: 0x0016EBA2
	public static void SetMeshColor(Material material, Color value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MeshColor, value);
	}

	// Token: 0x0600524A RID: 21066 RVA: 0x001707B2 File Offset: 0x0016EBB2
	public static void SetHDRFactor(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.HDRFactor, value);
	}

	// Token: 0x0600524B RID: 21067 RVA: 0x001707C3 File Offset: 0x0016EBC3
	public static void SetAlphaTestRefValue(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.AlphaTest_RefValue, value);
	}

	// Token: 0x0600524C RID: 21068 RVA: 0x001707D4 File Offset: 0x0016EBD4
	public static void SetHalfAccuracyColor(Material material, bool value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_HalfAccuracyColor, (!value) ? 0 : 1);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UsingHalfAccuracyColor, (!value) ? 0f : 1f);
	}

	// Token: 0x0600524D RID: 21069 RVA: 0x00170810 File Offset: 0x0016EC10
	public static void SetUsingTexture(Material material, eUsingTexture value, eTextureType textureID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_UsingTexture_Albedo, (int)value);
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_AlbedoTexture, (float)value);
			break;
		case eTextureType.eTextureType_Normal:
			MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_UsingTexture_Normal, (int)value);
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_NormalTexture, (float)value);
			break;
		case eTextureType.eTextureType_Specular:
			MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_UsingTexture_Specular, (int)value);
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_SpecularTexture, (float)value);
			break;
		case eTextureType.eTextureType_Additional_Start:
			MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_UsingTexture_Additional0, (int)value);
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Using_Additional0Texture, (float)value);
			break;
		}
	}

	// Token: 0x0600524E RID: 21070 RVA: 0x001708B8 File Offset: 0x0016ECB8
	public static void SetTexture(Material material, Texture value, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_AlbedoLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_Albedo, value);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_NormalLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_Normal, value);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_SpecularLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_Specular, value);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_Additional0Layer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.Texture_Additional0, value);
			}
			break;
		}
	}

	// Token: 0x0600524F RID: 21071 RVA: 0x001709AC File Offset: 0x0016EDAC
	public static void SetMaterialColor(Material material, Color value, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_AlbedoLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_Albedo, value);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_NormalLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_Normal, value);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_SpecularLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_Specular, value);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_Additional0Layer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.MatColor_Additional0, value);
			}
			break;
		}
	}

	// Token: 0x06005250 RID: 21072 RVA: 0x00170AC8 File Offset: 0x0016EEC8
	public static void SetUVMatrix(Material material, Matrix4x4 value, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_AlbedoLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_Albedo, value);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_NormalLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_Normal, value);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_SpecularLayer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_Specular, value);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID != 0)
			{
				if (layerID == 1)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_Additional0Layer, value);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.UVMatrix_Additional0, value);
			}
			break;
		}
	}

	// Token: 0x06005251 RID: 21073 RVA: 0x00170BE4 File Offset: 0x0016EFE4
	public static void SetLayerBlendMode(Material material, eLayerBlendMode colValue, eLayerBlendMode alpValue, eTextureType textureID)
	{
		eBlendMode eBlendMode = MeigeShaderUtility.ConvertLayerBlend(colValue);
		eBlendMode eBlendMode2 = MeigeShaderUtility.ConvertLayerBlend(alpValue);
		if (textureID != eTextureType.eTextureType_Albedo)
		{
			if (textureID != eTextureType.eTextureType_Normal)
			{
				if (textureID == eTextureType.eTextureType_Specular)
				{
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_SpecularColor, (float)eBlendMode);
					MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_SpecularAlpha, (float)eBlendMode2);
				}
			}
			else
			{
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_NormalColor, (float)eBlendMode);
				MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_NormalAlpha, (float)eBlendMode2);
			}
		}
		else
		{
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_AlbedoColor, (float)eBlendMode);
			MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.LayerBlendMode_AlbedoAlpha, (float)eBlendMode2);
		}
	}

	// Token: 0x06005252 RID: 21074 RVA: 0x00170C81 File Offset: 0x0016F081
	public static void SetOutlineType(Material material, eOutlineType value)
	{
		MeigeShaderUtility.EnableKeyword(material, MeigeShaderUtility.m_ShaderKeywordName_OutlineType, (int)value);
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineType, (float)value);
	}

	// Token: 0x06005253 RID: 21075 RVA: 0x00170C9E File Offset: 0x0016F09E
	public static void SetOutlineColor(Material material, Color value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineColor, value);
	}

	// Token: 0x06005254 RID: 21076 RVA: 0x00170CAE File Offset: 0x0016F0AE
	public static void SetOutlineWidth(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineWidth, value);
	}

	// Token: 0x06005255 RID: 21077 RVA: 0x00170CBF File Offset: 0x0016F0BF
	public static void SetOutlineDepthOffset(Material material, float value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineDepthOffset, value);
	}

	// Token: 0x06005256 RID: 21078 RVA: 0x00170CD0 File Offset: 0x0016F0D0
	public static void SetOutlineAlphaBlendMode(Material material, eBlendMode value)
	{
		MeigeShaderUtility.SaveOutlineBlendingParam(material, value);
	}

	// Token: 0x06005257 RID: 21079 RVA: 0x00170CDC File Offset: 0x0016F0DC
	public static eLayerBlendMode SetLayerBlendMode(Material material, eLayerBlendMode colValue, eBlendMode alpValue, eTextureType textureID)
	{
		eLayerBlendMode result = eLayerBlendMode.eLayerBlendMode_Std;
		if (textureID != eTextureType.eTextureType_Albedo)
		{
			if (textureID != eTextureType.eTextureType_Normal)
			{
				if (textureID == eTextureType.eTextureType_Specular)
				{
					result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[46]));
					result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[47]));
				}
			}
			else
			{
				result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[44]));
				result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[45]));
			}
		}
		else
		{
			result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[42]));
			result = MeigeShaderUtility.ConvertAlphaBlend((eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[43]));
		}
		return result;
	}

	// Token: 0x06005258 RID: 21080 RVA: 0x00170D92 File Offset: 0x0016F192
	public static void SetStencilCompare(Material material, eCompareFunc value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilCompare, (float)value);
	}

	// Token: 0x06005259 RID: 21081 RVA: 0x00170DA3 File Offset: 0x0016F1A3
	public static void SetStencilID(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilID, (float)value);
	}

	// Token: 0x0600525A RID: 21082 RVA: 0x00170DB4 File Offset: 0x0016F1B4
	public static void SetStencilOp(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilOp, (float)value);
	}

	// Token: 0x0600525B RID: 21083 RVA: 0x00170DC5 File Offset: 0x0016F1C5
	public static void SetStencilOpFail(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilOpFail, (float)value);
	}

	// Token: 0x0600525C RID: 21084 RVA: 0x00170DD6 File Offset: 0x0016F1D6
	public static void SetStencilOpZFail(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilOpZFail, (float)value);
	}

	// Token: 0x0600525D RID: 21085 RVA: 0x00170DE7 File Offset: 0x0016F1E7
	public static void SetStencilWriteMask(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.StencilWriteMask, (float)value);
	}

	// Token: 0x0600525E RID: 21086 RVA: 0x00170DF8 File Offset: 0x0016F1F8
	public static void SetStencilReadMask(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.SteancilReadMask, (float)value);
	}

	// Token: 0x0600525F RID: 21087 RVA: 0x00170E09 File Offset: 0x0016F209
	public static void SetOutlineStencilCompare(Material material, eCompareFunc value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilCompare, (float)value);
	}

	// Token: 0x06005260 RID: 21088 RVA: 0x00170E1A File Offset: 0x0016F21A
	public static void SetOutlineStencilID(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilID, (float)value);
	}

	// Token: 0x06005261 RID: 21089 RVA: 0x00170E2B File Offset: 0x0016F22B
	public static void SetOutlineStencilOp(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilOp, (float)value);
	}

	// Token: 0x06005262 RID: 21090 RVA: 0x00170E3C File Offset: 0x0016F23C
	public static void SetOutlineStencilOpFail(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilOpFail, (float)value);
	}

	// Token: 0x06005263 RID: 21091 RVA: 0x00170E4D File Offset: 0x0016F24D
	public static void SetOutlineStencilOpZFail(Material material, eStencilOp value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilOpZFail, (float)value);
	}

	// Token: 0x06005264 RID: 21092 RVA: 0x00170E5E File Offset: 0x0016F25E
	public static void SetOutlineStencilWriteMask(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.OutlineStencilWriteMask, (float)value);
	}

	// Token: 0x06005265 RID: 21093 RVA: 0x00170E6F File Offset: 0x0016F26F
	public static void SetOutlineStencilReadMask(Material material, byte value)
	{
		MeigeShaderUtility.SaveShaderProperty(material, MeigeShaderUtility.eShaderPropertyID.SteancilReadMask, (float)value);
	}

	// Token: 0x06005266 RID: 21094 RVA: 0x00170E80 File Offset: 0x0016F280
	public static eBlendMode GetAlphaBlendMode(Material material)
	{
		return (eBlendMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[1]);
	}

	// Token: 0x06005267 RID: 21095 RVA: 0x00170E90 File Offset: 0x0016F290
	public static eBlendFactor GetAlphaBlendSrcFactor(Material material)
	{
		return (eBlendFactor)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[2]);
	}

	// Token: 0x06005268 RID: 21096 RVA: 0x00170EA0 File Offset: 0x0016F2A0
	public static eBlendFactor GetAlphaBlendDstFactor(Material material)
	{
		return (eBlendFactor)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[3]);
	}

	// Token: 0x06005269 RID: 21097 RVA: 0x00170EB0 File Offset: 0x0016F2B0
	public static eBlendOp GetAlphaBlendOpColor(Material material)
	{
		return (eBlendOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[4]);
	}

	// Token: 0x0600526A RID: 21098 RVA: 0x00170EC0 File Offset: 0x0016F2C0
	public static eBlendOp GetAlphaBlendOpAlpha(Material material)
	{
		return (eBlendOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[5]);
	}

	// Token: 0x0600526B RID: 21099 RVA: 0x00170ED0 File Offset: 0x0016F2D0
	public static eCompareFunc GetDepthTest(Material material)
	{
		return (eCompareFunc)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[6]);
	}

	// Token: 0x0600526C RID: 21100 RVA: 0x00170EE0 File Offset: 0x0016F2E0
	public static bool GetEnableDepthWrite(Material material)
	{
		return (double)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[7]) > 0.0;
	}

	// Token: 0x0600526D RID: 21101 RVA: 0x00170EFB File Offset: 0x0016F2FB
	public static float GetDepthOffsetFactor(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[8]);
	}

	// Token: 0x0600526E RID: 21102 RVA: 0x00170F0A File Offset: 0x0016F30A
	public static float GetDepthOffsetUnits(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[9]);
	}

	// Token: 0x0600526F RID: 21103 RVA: 0x00170F1A File Offset: 0x0016F31A
	public static bool GetEnableAlphaTest(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[10]) > 0f;
	}

	// Token: 0x06005270 RID: 21104 RVA: 0x00170F31 File Offset: 0x0016F331
	public static eCullMode GetCullMode(Material material)
	{
		return (eCullMode)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[11]);
	}

	// Token: 0x06005271 RID: 21105 RVA: 0x00170F42 File Offset: 0x0016F342
	public static int GetLightingType(Material material)
	{
		return (int)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[12]);
	}

	// Token: 0x06005272 RID: 21106 RVA: 0x00170F53 File Offset: 0x0016F353
	public static bool GetUsingFog(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[13]) > 0f;
	}

	// Token: 0x06005273 RID: 21107 RVA: 0x00170F6A File Offset: 0x0016F36A
	public static Color GetMeshColor(Material material)
	{
		return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[14]);
	}

	// Token: 0x06005274 RID: 21108 RVA: 0x00170F7A File Offset: 0x0016F37A
	public static float GetHDRFactor(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[15]);
	}

	// Token: 0x06005275 RID: 21109 RVA: 0x00170F8A File Offset: 0x0016F38A
	public static float GetAlphaTestRefValue(Material material)
	{
		return material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[16]);
	}

	// Token: 0x06005276 RID: 21110 RVA: 0x00170F9A File Offset: 0x0016F39A
	public static bool GetHalfAccuracyColor(Material material)
	{
		return (double)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[17]) > 0.0;
	}

	// Token: 0x06005277 RID: 21111 RVA: 0x00170FB8 File Offset: 0x0016F3B8
	public static eUsingTexture GetUsingTexture(Material material, int textureID)
	{
		switch (textureID)
		{
		case 0:
			return (eUsingTexture)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[48]);
		case 1:
			return (eUsingTexture)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[49]);
		case 2:
			return (eUsingTexture)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[50]);
		case 3:
			return (eUsingTexture)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[51]);
		default:
			return eUsingTexture.None;
		}
	}

	// Token: 0x06005278 RID: 21112 RVA: 0x00171024 File Offset: 0x0016F424
	public static Texture GetTexture(Material material, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID == 0)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[18]);
			}
			if (layerID == 1)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[19]);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID == 0)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[20]);
			}
			if (layerID == 1)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[21]);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID == 0)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[22]);
			}
			if (layerID == 1)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[23]);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID == 0)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[24]);
			}
			if (layerID == 1)
			{
				return material.GetTexture(MeigeShaderUtility.m_ShaderPropertyName[25]);
			}
			break;
		}
		return null;
	}

	// Token: 0x06005279 RID: 21113 RVA: 0x00171124 File Offset: 0x0016F524
	public static Color GetMaterialColor(Material material, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID == 0)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[26]);
			}
			if (layerID == 1)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[27]);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID == 0)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[28]);
			}
			if (layerID == 1)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[29]);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID == 0)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[30]);
			}
			if (layerID == 1)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[31]);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID == 0)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[32]);
			}
			if (layerID == 1)
			{
				return material.GetColor(MeigeShaderUtility.m_ShaderPropertyName[33]);
			}
			break;
		}
		return Color.white;
	}

	// Token: 0x0600527A RID: 21114 RVA: 0x00171228 File Offset: 0x0016F628
	public static Matrix4x4 GetUVMatrix(Material material, Matrix4x4 value, eTextureType textureID, int layerID)
	{
		switch (textureID)
		{
		case eTextureType.eTextureType_Albedo:
			if (layerID == 0)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[34]);
			}
			if (layerID == 1)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[35]);
			}
			break;
		case eTextureType.eTextureType_Normal:
			if (layerID == 0)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[36]);
			}
			if (layerID == 1)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[37]);
			}
			break;
		case eTextureType.eTextureType_Specular:
			if (layerID == 0)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[38]);
			}
			if (layerID == 1)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[39]);
			}
			break;
		case eTextureType.eTextureType_Additional_Start:
			if (layerID == 0)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[40]);
			}
			if (layerID == 1)
			{
				return material.GetMatrix(MeigeShaderUtility.m_ShaderPropertyName[41]);
			}
			break;
		}
		return Matrix4x4.identity;
	}

	// Token: 0x0600527B RID: 21115 RVA: 0x00171329 File Offset: 0x0016F729
	public static eCompareFunc GetStencilCompare(Material material, eCompareFunc value)
	{
		return (eCompareFunc)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[61]);
	}

	// Token: 0x0600527C RID: 21116 RVA: 0x0017133A File Offset: 0x0016F73A
	public static byte GetStencilID(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[62]);
	}

	// Token: 0x0600527D RID: 21117 RVA: 0x0017134B File Offset: 0x0016F74B
	public static eStencilOp GetStencilOp(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[63]);
	}

	// Token: 0x0600527E RID: 21118 RVA: 0x0017135C File Offset: 0x0016F75C
	public static eStencilOp GetStencilOpFail(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[64]);
	}

	// Token: 0x0600527F RID: 21119 RVA: 0x0017136D File Offset: 0x0016F76D
	public static eStencilOp GetStencilOpZFail(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[65]);
	}

	// Token: 0x06005280 RID: 21120 RVA: 0x0017137E File Offset: 0x0016F77E
	public static byte GetStencilWriteMask(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[66]);
	}

	// Token: 0x06005281 RID: 21121 RVA: 0x0017138F File Offset: 0x0016F78F
	public static byte GetStencilReadMask(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[67]);
	}

	// Token: 0x06005282 RID: 21122 RVA: 0x001713A0 File Offset: 0x0016F7A0
	public static eCompareFunc GetOutlineStencilCompare(Material material, eCompareFunc value)
	{
		return (eCompareFunc)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[68]);
	}

	// Token: 0x06005283 RID: 21123 RVA: 0x001713B1 File Offset: 0x0016F7B1
	public static byte GetOutlineStencilID(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[69]);
	}

	// Token: 0x06005284 RID: 21124 RVA: 0x001713C2 File Offset: 0x0016F7C2
	public static eStencilOp GetOutlineStencilOp(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[70]);
	}

	// Token: 0x06005285 RID: 21125 RVA: 0x001713D3 File Offset: 0x0016F7D3
	public static eStencilOp GetOutlineStencilOpFail(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[71]);
	}

	// Token: 0x06005286 RID: 21126 RVA: 0x001713E4 File Offset: 0x0016F7E4
	public static eStencilOp GetOutlineStencilOpZFail(Material material, eStencilOp value)
	{
		return (eStencilOp)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[72]);
	}

	// Token: 0x06005287 RID: 21127 RVA: 0x001713F5 File Offset: 0x0016F7F5
	public static byte GetOutlineStencilWriteMask(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[73]);
	}

	// Token: 0x06005288 RID: 21128 RVA: 0x00171406 File Offset: 0x0016F806
	public static byte GetOutlineStencilReadMask(Material material, byte value)
	{
		return (byte)material.GetFloat(MeigeShaderUtility.m_ShaderPropertyName[67]);
	}

	// Token: 0x04005259 RID: 21081
	private static string[] m_ShaderPropertyName = new string[]
	{
		"_ShadingType",
		"_BlendMode",
		"_BlendSrc",
		"_BlendDst",
		"_BlendOpColor",
		"_BlendOpAlpha",
		"_DepthTest",
		"_DepthWrite",
		"_DepthOffsetFactor",
		"_DepthOffsetUnits",
		"_Using_AlphaTest",
		"_CullMode",
		"_Lighting_Type",
		"_Using_Fog",
		"_MeshColor",
		"_HDRFactor",
		"_AlphaTestRefValue",
		"_Using_HalfAccuracy_Color",
		"_Texture_Albedo",
		"_Texture_AlbedoLayer",
		"_Texture_Normal",
		"_Texture_NormalLayer",
		"_Texture_Specular",
		"_Texture_SpecularLayer",
		"_Texture_Additional0",
		"_Texture_Additional0Layer",
		"_MatColor_Albedo",
		"_MatColor_AlbedoLayer",
		"_MatColor_Normal",
		"_MatColor_NormalLayer",
		"_MatColor_Specular",
		"_MatColor_SpecularLayer",
		"_MatColor_Additional0",
		"_MatColor_Additional0Layer",
		"_UVMatrix_Albedo",
		"_UVMatrix_AlbedoLayer",
		"_UVMatrix_Normal",
		"_UVMatrix_NormalLayer",
		"_UVMatrix_Specular",
		"_UVMatrix_SpecularLayer",
		"_UVMatrix_Additional0",
		"_UVMatrix_Additional0Layer",
		"_LayerColorBlendMode_Albedo",
		"_LayerAlphaBlendMode_Albedo",
		"_LayerColorBlendMode_Normal",
		"_LayerAlphaBlendMode_Normal",
		"_LayerColorBlendMode_Specular",
		"_LayerAlphaBlendMode_Specular",
		"_Using_Albedo_Texture",
		"_Using_Normal_Texture",
		"_Using_Specular_Texture",
		"_Using_Additional0_Texture",
		"_OutlineType",
		"_OutlineColor",
		"_OutlineWidth",
		"_OutlineOffset",
		"_OutlineBlendMode",
		"_OutlineBlendSrc",
		"_OutlineBlendDst",
		"_OutlineBlendOpColor",
		"_OutlineBlendOpAlpha",
		"_StencilComp",
		"_Stencil",
		"_StencilOp",
		"_StencilOpFail",
		"_StencilOpZFail",
		"_StencilWriteMask",
		"_StencilReadMask",
		"_OutlineStencilComp",
		"_OutlineStencil",
		"_OutlineStencilOp",
		"_OutlineStencilOpFail",
		"_OutlineStencilOpZFail",
		"_OutlineStencilWriteMask",
		"_OutlineStencilReadMask"
	};

	// Token: 0x0400525A RID: 21082
	private static string[] m_ShaderKeywordName_ShaderType = new string[]
	{
		"_SHADINGTYPE_UNLIGHT",
		"_SHADINGTYPE_BLINNPHONG",
		"_SHADINGTYPE_TOON"
	};

	// Token: 0x0400525B RID: 21083
	private static string[] m_ShaderKeywordName_AlphaTest = new string[]
	{
		"_USING_ALPHATEST_DISABLE",
		"_USING_ALPHATEST_ENABLE"
	};

	// Token: 0x0400525C RID: 21084
	private static string[] m_ShaderKeywordName_Lighting = new string[]
	{
		"_LIGHTING_TYPE_VERTEX",
		"_LIGHTING_TYPE_PIXEL"
	};

	// Token: 0x0400525D RID: 21085
	private static string[] m_ShaderKeywordName_Fog = new string[]
	{
		"_USING_FOG_DISABLE",
		"_USING_FOG_ENABLE"
	};

	// Token: 0x0400525E RID: 21086
	private static string[] m_ShaderKeywordName_UsingTexture_Albedo = new string[]
	{
		"_USING_ALBEDO_TEXTURE_NONE",
		"_USING_ALBEDO_TEXTURE_SINGLE",
		"_USING_ALBEDO_TEXTURE_LAYER"
	};

	// Token: 0x0400525F RID: 21087
	private static string[] m_ShaderKeywordName_UsingTexture_Normal = new string[]
	{
		"_USING_NORMAL_TEXTURE_NONE",
		"_USING_NORMAL_TEXTURE_SINGLE",
		"_USING_NORMAL_TEXTURE_LAYER"
	};

	// Token: 0x04005260 RID: 21088
	private static string[] m_ShaderKeywordName_UsingTexture_Specular = new string[]
	{
		"_USING_SPECULAR_TEXTURE_NONE",
		"_USING_SPECULAR_TEXTURE_SINGLE",
		"_USING_SPECULAR_TEXTURE_LAYER"
	};

	// Token: 0x04005261 RID: 21089
	private static string[] m_ShaderKeywordName_UsingTexture_Additional0 = new string[]
	{
		"_USING_ADDITIONAL0_TEXTURE_NONE",
		"_USING_ADDITIONAL0_TEXTURE_SINGLE",
		"_USING_ADDITIONAL0_TEXTURE_LAYER"
	};

	// Token: 0x04005262 RID: 21090
	private static string[] m_ShaderKeywordName_HalfAccuracyColor = new string[]
	{
		"_USING_HALFACCURACY_COLOR_DISABLE",
		"_USING_HALFACCURACY_COLOR_ENABLE",
		"_USING_ADDITIONAL0_TEXTURE_LAYER"
	};

	// Token: 0x04005263 RID: 21091
	private static string[] m_ShaderKeywordName_OutlineType = new string[]
	{
		"_OTLINETYPE_STD",
		"_OTLINETYPE_MULBASECOLOR"
	};

	// Token: 0x04005264 RID: 21092
	private static MeigeShaderUtility.BlendComponent[] m_blendComponent = new MeigeShaderUtility.BlendComponent[]
	{
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.One, eBlendFactor.Zero),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.OneMinusSrcAlpha),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.One),
		new MeigeShaderUtility.BlendComponent(eBlendOp.RevSub, eBlendFactor.SrcAlpha, eBlendFactor.One),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.OneMinusDstColor, eBlendFactor.OneMinusSrcAlpha),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.One, eBlendFactor.Zero),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.Zero, eBlendFactor.One),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.DstColor, eBlendFactor.Zero),
		new MeigeShaderUtility.BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.OneMinusSrcAlpha)
	};

	// Token: 0x02000F7B RID: 3963
	public enum eShaderPropertyID
	{
		// Token: 0x04005266 RID: 21094
		ShadingType,
		// Token: 0x04005267 RID: 21095
		BlendMode,
		// Token: 0x04005268 RID: 21096
		BlendSrc,
		// Token: 0x04005269 RID: 21097
		BlendDst,
		// Token: 0x0400526A RID: 21098
		BlendOpColor,
		// Token: 0x0400526B RID: 21099
		BlendOpAlpha,
		// Token: 0x0400526C RID: 21100
		DepthTest,
		// Token: 0x0400526D RID: 21101
		DepthWrite,
		// Token: 0x0400526E RID: 21102
		DepthOffsetFactor,
		// Token: 0x0400526F RID: 21103
		DepthOffsetUnits,
		// Token: 0x04005270 RID: 21104
		Using_AlphaTest,
		// Token: 0x04005271 RID: 21105
		CullMode,
		// Token: 0x04005272 RID: 21106
		Lighting_Type,
		// Token: 0x04005273 RID: 21107
		Using_Fog,
		// Token: 0x04005274 RID: 21108
		MeshColor,
		// Token: 0x04005275 RID: 21109
		HDRFactor,
		// Token: 0x04005276 RID: 21110
		AlphaTest_RefValue,
		// Token: 0x04005277 RID: 21111
		UsingHalfAccuracyColor,
		// Token: 0x04005278 RID: 21112
		Texture_Albedo,
		// Token: 0x04005279 RID: 21113
		Texture_AlbedoLayer,
		// Token: 0x0400527A RID: 21114
		Texture_Normal,
		// Token: 0x0400527B RID: 21115
		Texture_NormalLayer,
		// Token: 0x0400527C RID: 21116
		Texture_Specular,
		// Token: 0x0400527D RID: 21117
		Texture_SpecularLayer,
		// Token: 0x0400527E RID: 21118
		Texture_Additional0,
		// Token: 0x0400527F RID: 21119
		Texture_Additional0Layer,
		// Token: 0x04005280 RID: 21120
		MatColor_Albedo,
		// Token: 0x04005281 RID: 21121
		MatColor_AlbedoLayer,
		// Token: 0x04005282 RID: 21122
		MatColor_Normal,
		// Token: 0x04005283 RID: 21123
		MatColor_NormalLayer,
		// Token: 0x04005284 RID: 21124
		MatColor_Specular,
		// Token: 0x04005285 RID: 21125
		MatColor_SpecularLayer,
		// Token: 0x04005286 RID: 21126
		MatColor_Additional0,
		// Token: 0x04005287 RID: 21127
		MatColor_Additional0Layer,
		// Token: 0x04005288 RID: 21128
		UVMatrix_Albedo,
		// Token: 0x04005289 RID: 21129
		UVMatrix_AlbedoLayer,
		// Token: 0x0400528A RID: 21130
		UVMatrix_Normal,
		// Token: 0x0400528B RID: 21131
		UVMatrix_NormalLayer,
		// Token: 0x0400528C RID: 21132
		UVMatrix_Specular,
		// Token: 0x0400528D RID: 21133
		UVMatrix_SpecularLayer,
		// Token: 0x0400528E RID: 21134
		UVMatrix_Additional0,
		// Token: 0x0400528F RID: 21135
		UVMatrix_Additional0Layer,
		// Token: 0x04005290 RID: 21136
		LayerBlendMode_AlbedoColor,
		// Token: 0x04005291 RID: 21137
		LayerBlendMode_AlbedoAlpha,
		// Token: 0x04005292 RID: 21138
		LayerBlendMode_NormalColor,
		// Token: 0x04005293 RID: 21139
		LayerBlendMode_NormalAlpha,
		// Token: 0x04005294 RID: 21140
		LayerBlendMode_SpecularColor,
		// Token: 0x04005295 RID: 21141
		LayerBlendMode_SpecularAlpha,
		// Token: 0x04005296 RID: 21142
		Using_AlbedoTexture,
		// Token: 0x04005297 RID: 21143
		Using_NormalTexture,
		// Token: 0x04005298 RID: 21144
		Using_SpecularTexture,
		// Token: 0x04005299 RID: 21145
		Using_Additional0Texture,
		// Token: 0x0400529A RID: 21146
		OutlineType,
		// Token: 0x0400529B RID: 21147
		OutlineColor,
		// Token: 0x0400529C RID: 21148
		OutlineWidth,
		// Token: 0x0400529D RID: 21149
		OutlineDepthOffset,
		// Token: 0x0400529E RID: 21150
		OutlineBlendMode,
		// Token: 0x0400529F RID: 21151
		OutlineBlendSrc,
		// Token: 0x040052A0 RID: 21152
		OutlineBlendDst,
		// Token: 0x040052A1 RID: 21153
		OutlineBlendOpColor,
		// Token: 0x040052A2 RID: 21154
		OutlineBlendOpAlpha,
		// Token: 0x040052A3 RID: 21155
		StencilCompare,
		// Token: 0x040052A4 RID: 21156
		StencilID,
		// Token: 0x040052A5 RID: 21157
		StencilOp,
		// Token: 0x040052A6 RID: 21158
		StencilOpFail,
		// Token: 0x040052A7 RID: 21159
		StencilOpZFail,
		// Token: 0x040052A8 RID: 21160
		StencilWriteMask,
		// Token: 0x040052A9 RID: 21161
		SteancilReadMask,
		// Token: 0x040052AA RID: 21162
		OutlineStencilCompare,
		// Token: 0x040052AB RID: 21163
		OutlineStencilID,
		// Token: 0x040052AC RID: 21164
		OutlineStencilOp,
		// Token: 0x040052AD RID: 21165
		OutlineStencilOpFail,
		// Token: 0x040052AE RID: 21166
		OutlineStencilOpZFail,
		// Token: 0x040052AF RID: 21167
		OutlineStencilWriteMask,
		// Token: 0x040052B0 RID: 21168
		OutlineSteancilReadMask
	}

	// Token: 0x02000F7C RID: 3964
	private struct BlendComponent
	{
		// Token: 0x0600528A RID: 21130 RVA: 0x001718CC File Offset: 0x0016FCCC
		public BlendComponent(eBlendOp op, eBlendFactor src, eBlendFactor dst)
		{
			this.m_BlendOp = op;
			this.m_BlendFactorSrc = src;
			this.m_BlendFactorDst = dst;
		}

		// Token: 0x040052B1 RID: 21169
		public eBlendOp m_BlendOp;

		// Token: 0x040052B2 RID: 21170
		public eBlendFactor m_BlendFactorSrc;

		// Token: 0x040052B3 RID: 21171
		public eBlendFactor m_BlendFactorDst;
	}
}
