﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace QuestChapterResponseTypes
{
	// Token: 0x02000C63 RID: 3171
	public class Get : CommonResponse
	{
		// Token: 0x0600407F RID: 16511 RVA: 0x0013F1B4 File Offset: 0x0013D5B4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.chapters == null) ? string.Empty : this.chapters.ToString());
		}

		// Token: 0x04004664 RID: 18020
		public QuestChapter[] chapters;
	}
}
