﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace QuestChapterResponseTypes
{
	// Token: 0x02000C64 RID: 3172
	public class GetAll : CommonResponse
	{
		// Token: 0x06004081 RID: 16513 RVA: 0x0013F1F8 File Offset: 0x0013D5F8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.chapters == null) ? string.Empty : this.chapters.ToString());
		}

		// Token: 0x04004665 RID: 18021
		public QuestChapterArray[] chapters;
	}
}
