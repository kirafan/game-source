﻿using System;
using UnityEngine;

// Token: 0x020002F7 RID: 759
public static class Component_Ext
{
	// Token: 0x06000ED8 RID: 3800 RVA: 0x0004FD1E File Offset: 0x0004E11E
	public static void RemoveComponent<T>(this Component self) where T : Component
	{
		UnityEngine.Object.Destroy(self.GetComponent<T>());
	}
}
