﻿using System;
using Meige;
using STORERequestTypes;

// Token: 0x02000B6C RID: 2924
public static class STORERequest
{
	// Token: 0x06003E91 RID: 16017 RVA: 0x0013AF38 File Offset: 0x00139338
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", param.platform);
		meigewwwParam.Add("env", param.env);
		return meigewwwParam;
	}

	// Token: 0x06003E92 RID: 16018 RVA: 0x0013AF88 File Offset: 0x00139388
	public static MeigewwwParam GetAll(int platform, int env, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", platform);
		meigewwwParam.Add("env", env);
		return meigewwwParam;
	}

	// Token: 0x06003E93 RID: 16019 RVA: 0x0013AFCC File Offset: 0x001393CC
	public static MeigewwwParam PurchaseAppStore(PurchaseAppStore param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/app_store/purchase", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("receipt", param.receipt);
		return meigewwwParam;
	}

	// Token: 0x06003E94 RID: 16020 RVA: 0x0013B000 File Offset: 0x00139400
	public static MeigewwwParam PurchaseAppStore(string receipt, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/app_store/purchase", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("receipt", receipt);
		return meigewwwParam;
	}

	// Token: 0x06003E95 RID: 16021 RVA: 0x0013B030 File Offset: 0x00139430
	public static MeigewwwParam PurchaseGooglePlay(PurchaseGooglePlay param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/google_play/purchase", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("receipt", param.receipt);
		meigewwwParam.Add("signature", param.signature);
		return meigewwwParam;
	}

	// Token: 0x06003E96 RID: 16022 RVA: 0x0013B074 File Offset: 0x00139474
	public static MeigewwwParam PurchaseGooglePlay(string receipt, string signature, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/google_play/purchase", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("receipt", receipt);
		meigewwwParam.Add("signature", signature);
		return meigewwwParam;
	}

	// Token: 0x06003E97 RID: 16023 RVA: 0x0013B0B0 File Offset: 0x001394B0
	public static MeigewwwParam GetGooglePayload(GetGooglePayload param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/google_play/payload", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("id", param.id);
		return meigewwwParam;
	}

	// Token: 0x06003E98 RID: 16024 RVA: 0x0013B0E8 File Offset: 0x001394E8
	public static MeigewwwParam GetGooglePayload(int id, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/google_play/payload", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("id", id);
		return meigewwwParam;
	}

	// Token: 0x06003E99 RID: 16025 RVA: 0x0013B11C File Offset: 0x0013951C
	public static MeigewwwParam GetApplePayload(GetApplePayload param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/app_store/payload", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("id", param.id);
		return meigewwwParam;
	}

	// Token: 0x06003E9A RID: 16026 RVA: 0x0013B154 File Offset: 0x00139554
	public static MeigewwwParam GetApplePayload(int id, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/app_store/payload", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("id", id);
		return meigewwwParam;
	}

	// Token: 0x06003E9B RID: 16027 RVA: 0x0013B188 File Offset: 0x00139588
	public static MeigewwwParam GetPurchaseLog(GetPurchaseLog param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/purchase/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E9C RID: 16028 RVA: 0x0013B1AC File Offset: 0x001395AC
	public static MeigewwwParam GetPurchaseLog(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/store/purchase/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}
}
