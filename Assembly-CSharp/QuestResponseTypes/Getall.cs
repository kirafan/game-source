﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace QuestResponseTypes
{
	// Token: 0x02000C65 RID: 3173
	public class Getall : CommonResponse
	{
		// Token: 0x06004083 RID: 16515 RVA: 0x0013F23C File Offset: 0x0013D63C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.quests == null) ? string.Empty : this.quests.ToString());
			return str + ((this.eventQuests == null) ? string.Empty : this.eventQuests.ToString());
		}

		// Token: 0x04004666 RID: 18022
		public Quest[] quests;

		// Token: 0x04004667 RID: 18023
		public EventQuest[] eventQuests;
	}
}
