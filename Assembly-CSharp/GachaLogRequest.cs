﻿using System;
using GachaLogRequestTypes;
using Meige;

// Token: 0x02000B5F RID: 2911
public static class GachaLogRequest
{
	// Token: 0x06003DFD RID: 15869 RVA: 0x00138C10 File Offset: 0x00137010
	public static MeigewwwParam GetLatest(GetLatest param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha_log/get_latest", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003DFE RID: 15870 RVA: 0x00138C34 File Offset: 0x00137034
	public static MeigewwwParam GetLatest(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/gacha_log/get_latest", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}
}
