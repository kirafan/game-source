﻿using System;

// Token: 0x02000E74 RID: 3700
[Serializable]
public struct ScriptData_Param
{
	// Token: 0x04004D85 RID: 19845
	public int ScriptID;

	// Token: 0x04004D86 RID: 19846
	public string ScriptName;

	// Token: 0x04004D87 RID: 19847
	public ScriptData_FuncParam[] FuncParam;
}
