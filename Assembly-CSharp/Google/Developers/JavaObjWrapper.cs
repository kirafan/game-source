﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Google.Developers
{
	// Token: 0x02000D3C RID: 3388
	public class JavaObjWrapper
	{
		// Token: 0x06004408 RID: 17416 RVA: 0x0014556B File Offset: 0x0014396B
		protected JavaObjWrapper()
		{
		}

		// Token: 0x06004409 RID: 17417 RVA: 0x0014557E File Offset: 0x0014397E
		public JavaObjWrapper(string clazzName)
		{
			this.raw = AndroidJNI.AllocObject(AndroidJNI.FindClass(clazzName));
		}

		// Token: 0x0600440A RID: 17418 RVA: 0x001455A2 File Offset: 0x001439A2
		public JavaObjWrapper(IntPtr rawObject)
		{
			this.raw = rawObject;
		}

		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x0600440B RID: 17419 RVA: 0x001455BC File Offset: 0x001439BC
		public IntPtr RawObject
		{
			get
			{
				return this.raw;
			}
		}

		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x0600440C RID: 17420 RVA: 0x001455C4 File Offset: 0x001439C4
		public virtual IntPtr RawClass
		{
			get
			{
				if (this.cachedRawClass == IntPtr.Zero && this.raw != IntPtr.Zero)
				{
					this.cachedRawClass = AndroidJNI.GetObjectClass(this.raw);
				}
				return this.cachedRawClass;
			}
		}

		// Token: 0x0600440D RID: 17421 RVA: 0x00145614 File Offset: 0x00143A14
		public void CreateInstance(string clazzName, params object[] args)
		{
			if (this.raw != IntPtr.Zero)
			{
				throw new Exception("Java object already set");
			}
			IntPtr constructorID = AndroidJNIHelper.GetConstructorID(this.RawClass, args);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			this.raw = AndroidJNI.NewObject(this.RawClass, constructorID, args2);
		}

		// Token: 0x0600440E RID: 17422 RVA: 0x00145668 File Offset: 0x00143A68
		protected static jvalue[] ConstructArgArray(object[] theArgs)
		{
			object[] array = new object[theArgs.Length];
			for (int i = 0; i < theArgs.Length; i++)
			{
				if (theArgs[i] is JavaObjWrapper)
				{
					array[i] = ((JavaObjWrapper)theArgs[i]).raw;
				}
				else
				{
					array[i] = theArgs[i];
				}
			}
			jvalue[] array2 = AndroidJNIHelper.CreateJNIArgArray(array);
			for (int j = 0; j < theArgs.Length; j++)
			{
				if (theArgs[j] is JavaObjWrapper)
				{
					array2[j].l = ((JavaObjWrapper)theArgs[j]).raw;
				}
				else if (theArgs[j] is JavaInterfaceProxy)
				{
					IntPtr l = AndroidJNIHelper.CreateJavaProxy((AndroidJavaProxy)theArgs[j]);
					array2[j].l = l;
				}
			}
			if (array2.Length == 1)
			{
				for (int k = 0; k < array2.Length; k++)
				{
					Debug.Log(string.Concat(new object[]
					{
						"---- [",
						k,
						"] -- ",
						array2[k].l
					}));
				}
			}
			return array2;
		}

		// Token: 0x0600440F RID: 17423 RVA: 0x00145790 File Offset: 0x00143B90
		public static T StaticInvokeObjectCall<T>(string type, string name, string sig, params object[] args)
		{
			IntPtr clazz = AndroidJNI.FindClass(type);
			IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(clazz, name, sig);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			IntPtr intPtr = AndroidJNI.CallStaticObjectMethod(clazz, staticMethodID, args2);
			ConstructorInfo constructor = typeof(T).GetConstructor(new Type[]
			{
				intPtr.GetType()
			});
			if (constructor != null)
			{
				return (T)((object)constructor.Invoke(new object[]
				{
					intPtr
				}));
			}
			if (typeof(T).IsArray)
			{
				return AndroidJNIHelper.ConvertFromJNIArray<T>(intPtr);
			}
			Debug.Log("Trying cast....");
			Type typeFromHandle = typeof(T);
			return (T)((object)Marshal.PtrToStructure(intPtr, typeFromHandle));
		}

		// Token: 0x06004410 RID: 17424 RVA: 0x00145848 File Offset: 0x00143C48
		public static void StaticInvokeCallVoid(string type, string name, string sig, params object[] args)
		{
			IntPtr clazz = AndroidJNI.FindClass(type);
			IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(clazz, name, sig);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			AndroidJNI.CallStaticVoidMethod(clazz, staticMethodID, args2);
		}

		// Token: 0x06004411 RID: 17425 RVA: 0x00145874 File Offset: 0x00143C74
		public static T GetStaticObjectField<T>(string clsName, string name, string sig)
		{
			IntPtr clazz = AndroidJNI.FindClass(clsName);
			IntPtr staticFieldID = AndroidJNI.GetStaticFieldID(clazz, name, sig);
			IntPtr staticObjectField = AndroidJNI.GetStaticObjectField(clazz, staticFieldID);
			ConstructorInfo constructor = typeof(T).GetConstructor(new Type[]
			{
				staticObjectField.GetType()
			});
			if (constructor != null)
			{
				return (T)((object)constructor.Invoke(new object[]
				{
					staticObjectField
				}));
			}
			Type typeFromHandle = typeof(T);
			return (T)((object)Marshal.PtrToStructure(staticObjectField, typeFromHandle));
		}

		// Token: 0x06004412 RID: 17426 RVA: 0x001458FC File Offset: 0x00143CFC
		public static int GetStaticIntField(string clsName, string name)
		{
			IntPtr clazz = AndroidJNI.FindClass(clsName);
			IntPtr staticFieldID = AndroidJNI.GetStaticFieldID(clazz, name, "I");
			return AndroidJNI.GetStaticIntField(clazz, staticFieldID);
		}

		// Token: 0x06004413 RID: 17427 RVA: 0x00145924 File Offset: 0x00143D24
		public static string GetStaticStringField(string clsName, string name)
		{
			IntPtr clazz = AndroidJNI.FindClass(clsName);
			IntPtr staticFieldID = AndroidJNI.GetStaticFieldID(clazz, name, "Ljava/lang/String;");
			return AndroidJNI.GetStaticStringField(clazz, staticFieldID);
		}

		// Token: 0x06004414 RID: 17428 RVA: 0x0014594C File Offset: 0x00143D4C
		public static float GetStaticFloatField(string clsName, string name)
		{
			IntPtr clazz = AndroidJNI.FindClass(clsName);
			IntPtr staticFieldID = AndroidJNI.GetStaticFieldID(clazz, name, "F");
			return AndroidJNI.GetStaticFloatField(clazz, staticFieldID);
		}

		// Token: 0x06004415 RID: 17429 RVA: 0x00145974 File Offset: 0x00143D74
		public void InvokeCallVoid(string name, string sig, params object[] args)
		{
			IntPtr methodID = AndroidJNI.GetMethodID(this.RawClass, name, sig);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			AndroidJNI.CallVoidMethod(this.raw, methodID, args2);
		}

		// Token: 0x06004416 RID: 17430 RVA: 0x001459A4 File Offset: 0x00143DA4
		public T InvokeCall<T>(string name, string sig, params object[] args)
		{
			Type typeFromHandle = typeof(T);
			IntPtr methodID = AndroidJNI.GetMethodID(this.RawClass, name, sig);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			if (methodID == IntPtr.Zero)
			{
				Debug.LogError("Cannot get method for " + name);
				throw new Exception("Cannot get method for " + name);
			}
			if (typeFromHandle == typeof(bool))
			{
				return (T)((object)AndroidJNI.CallBooleanMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(string))
			{
				return (T)((object)AndroidJNI.CallStringMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(int))
			{
				return (T)((object)AndroidJNI.CallIntMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(float))
			{
				return (T)((object)AndroidJNI.CallFloatMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(double))
			{
				return (T)((object)AndroidJNI.CallDoubleMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(byte))
			{
				return (T)((object)AndroidJNI.CallByteMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(char))
			{
				return (T)((object)AndroidJNI.CallCharMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(long))
			{
				return (T)((object)AndroidJNI.CallLongMethod(this.raw, methodID, args2));
			}
			if (typeFromHandle == typeof(short))
			{
				return (T)((object)AndroidJNI.CallShortMethod(this.raw, methodID, args2));
			}
			return this.InvokeObjectCall<T>(name, sig, args);
		}

		// Token: 0x06004417 RID: 17431 RVA: 0x00145B70 File Offset: 0x00143F70
		public static T StaticInvokeCall<T>(string type, string name, string sig, params object[] args)
		{
			Type typeFromHandle = typeof(T);
			IntPtr clazz = AndroidJNI.FindClass(type);
			IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(clazz, name, sig);
			jvalue[] args2 = JavaObjWrapper.ConstructArgArray(args);
			if (typeFromHandle == typeof(bool))
			{
				return (T)((object)AndroidJNI.CallStaticBooleanMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(string))
			{
				return (T)((object)AndroidJNI.CallStaticStringMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(int))
			{
				return (T)((object)AndroidJNI.CallStaticIntMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(float))
			{
				return (T)((object)AndroidJNI.CallStaticFloatMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(double))
			{
				return (T)((object)AndroidJNI.CallStaticDoubleMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(byte))
			{
				return (T)((object)AndroidJNI.CallStaticByteMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(char))
			{
				return (T)((object)AndroidJNI.CallStaticCharMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(long))
			{
				return (T)((object)AndroidJNI.CallStaticLongMethod(clazz, staticMethodID, args2));
			}
			if (typeFromHandle == typeof(short))
			{
				return (T)((object)AndroidJNI.CallStaticShortMethod(clazz, staticMethodID, args2));
			}
			return JavaObjWrapper.StaticInvokeObjectCall<T>(type, name, sig, args);
		}

		// Token: 0x06004418 RID: 17432 RVA: 0x00145CE0 File Offset: 0x001440E0
		public T InvokeObjectCall<T>(string name, string sig, params object[] theArgs)
		{
			IntPtr methodID = AndroidJNI.GetMethodID(this.RawClass, name, sig);
			jvalue[] args = JavaObjWrapper.ConstructArgArray(theArgs);
			IntPtr intPtr = AndroidJNI.CallObjectMethod(this.raw, methodID, args);
			if (intPtr.Equals(IntPtr.Zero))
			{
				return default(T);
			}
			ConstructorInfo constructor = typeof(T).GetConstructor(new Type[]
			{
				intPtr.GetType()
			});
			if (constructor != null)
			{
				return (T)((object)constructor.Invoke(new object[]
				{
					intPtr
				}));
			}
			Type typeFromHandle = typeof(T);
			return (T)((object)Marshal.PtrToStructure(intPtr, typeFromHandle));
		}

		// Token: 0x04004B79 RID: 19321
		private IntPtr raw;

		// Token: 0x04004B7A RID: 19322
		private IntPtr cachedRawClass = IntPtr.Zero;
	}
}
