﻿using System;
using UnityEngine;

namespace Google.Developers
{
	// Token: 0x02000D3B RID: 3387
	public abstract class JavaInterfaceProxy : AndroidJavaProxy
	{
		// Token: 0x06004407 RID: 17415 RVA: 0x00144E18 File Offset: 0x00143218
		public JavaInterfaceProxy(string interfaceName) : base(interfaceName)
		{
		}
	}
}
