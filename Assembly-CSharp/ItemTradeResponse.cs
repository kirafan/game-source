﻿using System;
using System.Collections.Generic;
using ItemTradeResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFF RID: 3071
public static class ItemTradeResponse
{
	// Token: 0x06003F74 RID: 16244 RVA: 0x0013C6A0 File Offset: 0x0013AAA0
	public static Getrecipe Getrecipe(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getrecipe>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F75 RID: 16245 RVA: 0x0013C6B8 File Offset: 0x0013AAB8
	public static Trade Trade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Trade>(param, dialogType, acceptableResultCodes);
	}
}
