﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes
{
	// Token: 0x02000C18 RID: 3096
	public class Get : CommonResponse
	{
		// Token: 0x06003FE9 RID: 16361 RVA: 0x0013D54C File Offset: 0x0013B94C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.fieldPartyMember == null) ? string.Empty : this.fieldPartyMember.ToString());
		}

		// Token: 0x040045D8 RID: 17880
		public PlayerFieldPartyMember fieldPartyMember;
	}
}
