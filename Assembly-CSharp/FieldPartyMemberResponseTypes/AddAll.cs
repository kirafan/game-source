﻿using System;
using CommonResponseTypes;

namespace FieldPartyMemberResponseTypes
{
	// Token: 0x02000C17 RID: 3095
	public class AddAll : CommonResponse
	{
		// Token: 0x06003FE7 RID: 16359 RVA: 0x0013D508 File Offset: 0x0013B908
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedPartyMemberIds == null) ? string.Empty : this.managedPartyMemberIds.ToString());
		}

		// Token: 0x040045D7 RID: 17879
		public long[] managedPartyMemberIds;
	}
}
