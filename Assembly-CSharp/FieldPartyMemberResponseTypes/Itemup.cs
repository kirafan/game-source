﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes
{
	// Token: 0x02000C20 RID: 3104
	public class Itemup : CommonResponse
	{
		// Token: 0x06003FF9 RID: 16377 RVA: 0x0013D694 File Offset: 0x0013BA94
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			return str + ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
		}

		// Token: 0x040045DA RID: 17882
		public Player player;

		// Token: 0x040045DB RID: 17883
		public PlayerItemSummary[] itemSummary;

		// Token: 0x040045DC RID: 17884
		public PlayerNamedType[] managedNamedTypes;
	}
}
