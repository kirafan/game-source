﻿using System;
using CommonResponseTypes;

namespace FieldPartyMemberResponseTypes
{
	// Token: 0x02000C16 RID: 3094
	public class Add : CommonResponse
	{
		// Token: 0x06003FE5 RID: 16357 RVA: 0x0013D4D0 File Offset: 0x0013B8D0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.managedPartyMemberId.ToString();
		}

		// Token: 0x040045D6 RID: 17878
		public long managedPartyMemberId;
	}
}
