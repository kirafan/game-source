﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes
{
	// Token: 0x02000C19 RID: 3097
	public class GetAll : CommonResponse
	{
		// Token: 0x06003FEB RID: 16363 RVA: 0x0013D590 File Offset: 0x0013B990
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.fieldPartyMembers == null) ? string.Empty : this.fieldPartyMembers.ToString());
		}

		// Token: 0x040045D9 RID: 17881
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
