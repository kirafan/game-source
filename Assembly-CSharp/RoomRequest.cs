﻿using System;
using Meige;
using RoomRequestTypes;
using WWWTypes;

// Token: 0x02000B6B RID: 2923
public static class RoomRequest
{
	// Token: 0x06003E87 RID: 16007 RVA: 0x0013AC88 File Offset: 0x00139088
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", param.managedRoomId);
		meigewwwParam.Add("floorId", param.floorId);
		meigewwwParam.Add("groupId", param.groupId);
		meigewwwParam.Add("arrangeData", param.arrangeData);
		return meigewwwParam;
	}

	// Token: 0x06003E88 RID: 16008 RVA: 0x0013ACFC File Offset: 0x001390FC
	public static MeigewwwParam Set(long managedRoomId, int floorId, int groupId, PlayerRoomArrangement[] arrangeData, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", managedRoomId);
		meigewwwParam.Add("floorId", floorId);
		meigewwwParam.Add("groupId", groupId);
		meigewwwParam.Add("arrangeData", arrangeData);
		return meigewwwParam;
	}

	// Token: 0x06003E89 RID: 16009 RVA: 0x0013AD60 File Offset: 0x00139160
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", param.playerId);
		meigewwwParam.Add("floorId", param.floorId);
		meigewwwParam.Add("groupId", param.groupId);
		return meigewwwParam;
	}

	// Token: 0x06003E8A RID: 16010 RVA: 0x0013ADC4 File Offset: 0x001391C4
	public static MeigewwwParam GetAll(long playerId, int floorId, int groupId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", playerId);
		meigewwwParam.Add("floorId", floorId);
		meigewwwParam.Add("groupId", groupId);
		return meigewwwParam;
	}

	// Token: 0x06003E8B RID: 16011 RVA: 0x0013AE18 File Offset: 0x00139218
	public static MeigewwwParam Remove(Remove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", param.managedRoomId);
		return meigewwwParam;
	}

	// Token: 0x06003E8C RID: 16012 RVA: 0x0013AE50 File Offset: 0x00139250
	public static MeigewwwParam Remove(long managedRoomId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", managedRoomId);
		return meigewwwParam;
	}

	// Token: 0x06003E8D RID: 16013 RVA: 0x0013AE84 File Offset: 0x00139284
	public static MeigewwwParam Setactiveroom(Setactiveroom param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/active/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", param.managedRoomId);
		return meigewwwParam;
	}

	// Token: 0x06003E8E RID: 16014 RVA: 0x0013AEBC File Offset: 0x001392BC
	public static MeigewwwParam Setactiveroom(long managedRoomId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/active/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomId", managedRoomId);
		return meigewwwParam;
	}

	// Token: 0x06003E8F RID: 16015 RVA: 0x0013AEF0 File Offset: 0x001392F0
	public static MeigewwwParam Getactiveroom(Getactiveroom param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/active/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E90 RID: 16016 RVA: 0x0013AF14 File Offset: 0x00139314
	public static MeigewwwParam Getactiveroom(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room/active/get", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}
}
