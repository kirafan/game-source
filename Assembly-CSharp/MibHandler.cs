﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000F9F RID: 3999
public class MibHandler : MonoBehaviour
{
	// Token: 0x0400540F RID: 21519
	public MibHandler.MibInfo[] m_MibInfoArray;

	// Token: 0x02000FA0 RID: 4000
	[Serializable]
	public class MibInfo
	{
		// Token: 0x04005410 RID: 21520
		public string m_Name;

		// Token: 0x04005411 RID: 21521
		public ePixelFormat m_PixelFormat;

		// Token: 0x04005412 RID: 21522
		public bool m_bRepeatU;

		// Token: 0x04005413 RID: 21523
		public bool m_bRepeatV;
	}
}
