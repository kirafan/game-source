﻿using System;
using Meige;
using MISSIONRequestTypes;
using WWWTypes;

// Token: 0x02000B64 RID: 2916
public static class MISSIONRequest
{
	// Token: 0x06003E11 RID: 15889 RVA: 0x00139088 File Offset: 0x00137488
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E12 RID: 15890 RVA: 0x001390AC File Offset: 0x001374AC
	public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E13 RID: 15891 RVA: 0x001390D0 File Offset: 0x001374D0
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("missionLogs", param.missionLogs);
		return meigewwwParam;
	}

	// Token: 0x06003E14 RID: 15892 RVA: 0x00139104 File Offset: 0x00137504
	public static MeigewwwParam Set(PlayerMissionLog[] missionLogs, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("missionLogs", missionLogs);
		return meigewwwParam;
	}

	// Token: 0x06003E15 RID: 15893 RVA: 0x00139134 File Offset: 0x00137534
	public static MeigewwwParam Complete(Complete param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/complete", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedMissionId", param.managedMissionId);
		return meigewwwParam;
	}

	// Token: 0x06003E16 RID: 15894 RVA: 0x0013916C File Offset: 0x0013756C
	public static MeigewwwParam Complete(long managedMissionId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/complete", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedMissionId", managedMissionId);
		return meigewwwParam;
	}

	// Token: 0x06003E17 RID: 15895 RVA: 0x001391A0 File Offset: 0x001375A0
	public static MeigewwwParam Refresh(Refresh param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/refresh", MeigewwwParam.eRequestMethod.Post, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E18 RID: 15896 RVA: 0x001391C4 File Offset: 0x001375C4
	public static MeigewwwParam Refresh(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/mission/refresh", MeigewwwParam.eRequestMethod.Post, callback);
		return meigewwwParam;
	}
}
