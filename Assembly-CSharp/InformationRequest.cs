﻿using System;
using InformationRequestTypes;
using Meige;

// Token: 0x02000B61 RID: 2913
public static class InformationRequest
{
	// Token: 0x06003E07 RID: 15879 RVA: 0x00138E78 File Offset: 0x00137278
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("information/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", param.platform);
		return meigewwwParam;
	}

	// Token: 0x06003E08 RID: 15880 RVA: 0x00138EB0 File Offset: 0x001372B0
	public static MeigewwwParam Getall(int platform, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("information/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", platform);
		return meigewwwParam;
	}
}
