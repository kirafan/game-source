﻿using System;

namespace AppRequestTypes
{
	// Token: 0x02000B74 RID: 2932
	public class Versionget
	{
		// Token: 0x04004506 RID: 17670
		public int platform;

		// Token: 0x04004507 RID: 17671
		public string version;
	}
}
