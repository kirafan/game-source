﻿using System;

namespace RoomRequestTypes
{
	// Token: 0x02000BCF RID: 3023
	public class GetAll
	{
		// Token: 0x0400458C RID: 17804
		public long playerId;

		// Token: 0x0400458D RID: 17805
		public int floorId;

		// Token: 0x0400458E RID: 17806
		public int groupId;
	}
}
