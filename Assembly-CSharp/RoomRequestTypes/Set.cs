﻿using System;
using WWWTypes;

namespace RoomRequestTypes
{
	// Token: 0x02000BCE RID: 3022
	public class Set
	{
		// Token: 0x04004588 RID: 17800
		public long managedRoomId;

		// Token: 0x04004589 RID: 17801
		public int floorId;

		// Token: 0x0400458A RID: 17802
		public int groupId;

		// Token: 0x0400458B RID: 17803
		public PlayerRoomArrangement[] arrangeData;
	}
}
