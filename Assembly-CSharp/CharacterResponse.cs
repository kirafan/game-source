﻿using System;
using System.Collections.Generic;
using CharacterResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BF8 RID: 3064
public static class CharacterResponse
{
	// Token: 0x06003F58 RID: 16216 RVA: 0x0013C400 File Offset: 0x0013A800
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F59 RID: 16217 RVA: 0x0013C418 File Offset: 0x0013A818
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}
}
