﻿using System;
using UnityEngine;

// Token: 0x02000E75 RID: 3701
public class ScriptData : ScriptableObject
{
	// Token: 0x04004D88 RID: 19848
	public ScriptData_Param[] m_Params;
}
