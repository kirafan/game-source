﻿using System;
using FieldPartyMemberRequestTypes;
using Meige;
using WWWTypes;

// Token: 0x02000B5D RID: 2909
public static class FieldPartyMemberRequest
{
	// Token: 0x06003DD9 RID: 15833 RVA: 0x00138354 File Offset: 0x00136754
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMember", param.fieldPartyMember);
		return meigewwwParam;
	}

	// Token: 0x06003DDA RID: 15834 RVA: 0x00138388 File Offset: 0x00136788
	public static MeigewwwParam Add(PlayerFieldPartyMember fieldPartyMember, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMember", fieldPartyMember);
		return meigewwwParam;
	}

	// Token: 0x06003DDB RID: 15835 RVA: 0x001383B8 File Offset: 0x001367B8
	public static MeigewwwParam AddAll(AddAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/add_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMembers", param.fieldPartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DDC RID: 15836 RVA: 0x001383EC File Offset: 0x001367EC
	public static MeigewwwParam AddAll(PlayerFieldPartyMember[] fieldPartyMembers, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/add_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMembers", fieldPartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DDD RID: 15837 RVA: 0x0013841C File Offset: 0x0013681C
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("managedPartyMemberId", param.managedPartyMemberId);
		return meigewwwParam;
	}

	// Token: 0x06003DDE RID: 15838 RVA: 0x00138454 File Offset: 0x00136854
	public static MeigewwwParam Get(long managedPartyMemberId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("managedPartyMemberId", managedPartyMemberId);
		return meigewwwParam;
	}

	// Token: 0x06003DDF RID: 15839 RVA: 0x00138488 File Offset: 0x00136888
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", param.playerId);
		return meigewwwParam;
	}

	// Token: 0x06003DE0 RID: 15840 RVA: 0x001384C0 File Offset: 0x001368C0
	public static MeigewwwParam GetAll(long playerId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", playerId);
		return meigewwwParam;
	}

	// Token: 0x06003DE1 RID: 15841 RVA: 0x001384F4 File Offset: 0x001368F4
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMember", param.fieldPartyMember);
		return meigewwwParam;
	}

	// Token: 0x06003DE2 RID: 15842 RVA: 0x00138528 File Offset: 0x00136928
	public static MeigewwwParam Set(PlayerFieldPartyMember fieldPartyMember, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMember", fieldPartyMember);
		return meigewwwParam;
	}

	// Token: 0x06003DE3 RID: 15843 RVA: 0x00138558 File Offset: 0x00136958
	public static MeigewwwParam SetAll(SetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMembers", param.fieldPartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DE4 RID: 15844 RVA: 0x0013858C File Offset: 0x0013698C
	public static MeigewwwParam SetAll(PlayerFieldPartyMember[] fieldPartyMembers, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("fieldPartyMembers", fieldPartyMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DE5 RID: 15845 RVA: 0x001385BC File Offset: 0x001369BC
	public static MeigewwwParam Remove(Remove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", param.managedPartyMemberId);
		return meigewwwParam;
	}

	// Token: 0x06003DE6 RID: 15846 RVA: 0x001385F4 File Offset: 0x001369F4
	public static MeigewwwParam Remove(long managedPartyMemberId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", managedPartyMemberId);
		return meigewwwParam;
	}

	// Token: 0x06003DE7 RID: 15847 RVA: 0x00138628 File Offset: 0x00136A28
	public static MeigewwwParam RemoveAll(RemoveAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/remove_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberIds", param.managedPartyMemberIds);
		return meigewwwParam;
	}

	// Token: 0x06003DE8 RID: 15848 RVA: 0x0013865C File Offset: 0x00136A5C
	public static MeigewwwParam RemoveAll(long[] managedPartyMemberIds, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/remove_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberIds", managedPartyMemberIds);
		return meigewwwParam;
	}

	// Token: 0x06003DE9 RID: 15849 RVA: 0x0013868C File Offset: 0x00136A8C
	public static MeigewwwParam Changeschedule(Changeschedule param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/change_schedule", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", param.managedPartyMemberId);
		meigewwwParam.Add("scheduleId", param.scheduleId);
		meigewwwParam.Add("scheduleTag", param.scheduleTag);
		meigewwwParam.Add("managedFacilityId", param.managedFacilityId);
		meigewwwParam.Add("touchItemResultNo", param.touchItemResultNo);
		meigewwwParam.Add("flag", param.flag);
		meigewwwParam.Add("scheduleTable", param.scheduleTable);
		return meigewwwParam;
	}

	// Token: 0x06003DEA RID: 15850 RVA: 0x00138744 File Offset: 0x00136B44
	public static MeigewwwParam Changeschedule(long managedPartyMemberId, int scheduleId, int scheduleTag, long managedFacilityId, int touchItemResultNo, int flag, string scheduleTable, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/change_schedule", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", managedPartyMemberId);
		meigewwwParam.Add("scheduleId", scheduleId);
		meigewwwParam.Add("scheduleTag", scheduleTag);
		meigewwwParam.Add("managedFacilityId", managedFacilityId);
		meigewwwParam.Add("touchItemResultNo", touchItemResultNo);
		meigewwwParam.Add("flag", flag);
		meigewwwParam.Add("scheduleTable", scheduleTable);
		return meigewwwParam;
	}

	// Token: 0x06003DEB RID: 15851 RVA: 0x001387DC File Offset: 0x00136BDC
	public static MeigewwwParam ChangescheduleAll(ChangescheduleAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/change_schedule_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("changeScheduleMembers", param.changeScheduleMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DEC RID: 15852 RVA: 0x00138810 File Offset: 0x00136C10
	public static MeigewwwParam ChangescheduleAll(ChangeScheduleMember[] changeScheduleMembers, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/change_schedule_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("changeScheduleMembers", changeScheduleMembers);
		return meigewwwParam;
	}

	// Token: 0x06003DED RID: 15853 RVA: 0x00138840 File Offset: 0x00136C40
	public static MeigewwwParam Itemup(Itemup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/item_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", param.managedPartyMemberId);
		meigewwwParam.Add("touchItemResultNo", param.touchItemResultNo);
		meigewwwParam.Add("amount", param.amount);
		meigewwwParam.Add("flag", param.flag);
		return meigewwwParam;
	}

	// Token: 0x06003DEE RID: 15854 RVA: 0x001388BC File Offset: 0x00136CBC
	public static MeigewwwParam Itemup(long managedPartyMemberId, int touchItemResultNo, int amount, int flag, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/field_party/member/item_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedPartyMemberId", managedPartyMemberId);
		meigewwwParam.Add("touchItemResultNo", touchItemResultNo);
		meigewwwParam.Add("amount", amount);
		meigewwwParam.Add("flag", flag);
		return meigewwwParam;
	}
}
