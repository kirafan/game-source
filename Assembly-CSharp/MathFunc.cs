﻿using System;
using UnityEngine;

// Token: 0x02000FAD RID: 4013
public static class MathFunc
{
	// Token: 0x0600530D RID: 21261 RVA: 0x00173CD8 File Offset: 0x001720D8
	public static void InitRandom()
	{
		MathFunc.rand = new System.Random(Environment.TickCount);
	}

	// Token: 0x0600530E RID: 21262 RVA: 0x00173CE9 File Offset: 0x001720E9
	public static float RandomValue()
	{
		return UnityEngine.Random.value;
	}

	// Token: 0x0600530F RID: 21263 RVA: 0x00173CF0 File Offset: 0x001720F0
	public static int RandomRange(int min, int max)
	{
		return UnityEngine.Random.Range(min, max + 1);
	}

	// Token: 0x06005310 RID: 21264 RVA: 0x00173CFB File Offset: 0x001720FB
	public static float RandomRange(float min, float max)
	{
		return UnityEngine.Random.Range(min, max);
	}

	// Token: 0x06005311 RID: 21265 RVA: 0x00173D04 File Offset: 0x00172104
	public static float RandomValueToOtherThread()
	{
		return (float)MathFunc.rand.NextDouble();
	}

	// Token: 0x06005312 RID: 21266 RVA: 0x00173D20 File Offset: 0x00172120
	public static int RandomRangeToOtherThread(int min, int max)
	{
		int num = MathFunc.rand.Next();
		return num % (max - min + 1) + min;
	}

	// Token: 0x06005313 RID: 21267 RVA: 0x00173D44 File Offset: 0x00172144
	public static Vector2 Normalize(ref Vector2 v)
	{
		float num = MathFunc.Magnitude(ref v);
		if (num == 0f)
		{
			return Vector2.zero;
		}
		v /= num;
		return v;
	}

	// Token: 0x06005314 RID: 21268 RVA: 0x00173D84 File Offset: 0x00172184
	public static Vector3 Normalize(ref Vector3 v)
	{
		float num = MathFunc.Magnitude(ref v);
		if (num == 0f)
		{
			return Vector3.zero;
		}
		v /= num;
		return v;
	}

	// Token: 0x06005315 RID: 21269 RVA: 0x00173DC1 File Offset: 0x001721C1
	public static float Magnitude(ref Vector2 v)
	{
		return Mathf.Sqrt(MathFunc.SqMagnitude(ref v));
	}

	// Token: 0x06005316 RID: 21270 RVA: 0x00173DCE File Offset: 0x001721CE
	public static float Magnitude(ref Vector3 v)
	{
		return Mathf.Sqrt(MathFunc.SqMagnitude(ref v));
	}

	// Token: 0x06005317 RID: 21271 RVA: 0x00173DDB File Offset: 0x001721DB
	public static float SqMagnitude(ref Vector2 v)
	{
		return MathFunc.Dot(ref v, ref v);
	}

	// Token: 0x06005318 RID: 21272 RVA: 0x00173DE4 File Offset: 0x001721E4
	public static float SqMagnitude(ref Vector3 v)
	{
		return MathFunc.Dot(ref v, ref v);
	}

	// Token: 0x06005319 RID: 21273 RVA: 0x00173DF0 File Offset: 0x001721F0
	public static float Distance(ref Vector2 v0, ref Vector2 v1)
	{
		Vector2 vector = v0 - v1;
		return MathFunc.Magnitude(ref vector);
	}

	// Token: 0x0600531A RID: 21274 RVA: 0x00173E18 File Offset: 0x00172218
	public static float Distance(ref Vector3 v0, ref Vector3 v1)
	{
		Vector3 vector = v0 - v1;
		return MathFunc.Magnitude(ref vector);
	}

	// Token: 0x0600531B RID: 21275 RVA: 0x00173E40 File Offset: 0x00172240
	public static float SqDistance(ref Vector2 v0, ref Vector2 v1)
	{
		Vector2 vector = v0 - v1;
		return MathFunc.Dot(ref vector, ref vector);
	}

	// Token: 0x0600531C RID: 21276 RVA: 0x00173E68 File Offset: 0x00172268
	public static float SqDistance(ref Vector3 v0, ref Vector3 v1)
	{
		Vector3 vector = v0 - v1;
		return MathFunc.Dot(ref vector, ref vector);
	}

	// Token: 0x0600531D RID: 21277 RVA: 0x00173E90 File Offset: 0x00172290
	public static float Dot(ref Vector2 v0, ref Vector2 v1)
	{
		return v0.x * v1.x + v0.y * v1.y;
	}

	// Token: 0x0600531E RID: 21278 RVA: 0x00173EAD File Offset: 0x001722AD
	public static float Dot(ref Vector3 v0, ref Vector3 v1)
	{
		return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
	}

	// Token: 0x0600531F RID: 21279 RVA: 0x00173ED8 File Offset: 0x001722D8
	public static Vector2 Lerp(ref Vector2 src, ref Vector2 dst, float s)
	{
		Vector2 a = dst - src;
		a *= s;
		return a + src;
	}

	// Token: 0x06005320 RID: 21280 RVA: 0x00173F10 File Offset: 0x00172310
	public static Vector3 Lerp(ref Vector3 src, ref Vector3 dst, float s)
	{
		Vector3 a = dst - src;
		a *= s;
		return a + src;
	}

	// Token: 0x06005321 RID: 21281 RVA: 0x00173F48 File Offset: 0x00172348
	public static float PingPong(float t, float len)
	{
		float num = t / len;
		float num2 = num - (float)((int)num);
		if ((int)num % 2 == 0)
		{
			return Mathf.Lerp(0f, len, num2);
		}
		return Mathf.Lerp(0f, len, 1f - num2);
	}

	// Token: 0x06005322 RID: 21282 RVA: 0x00173F88 File Offset: 0x00172388
	public static float Angle(ref Vector2 dir0, ref Vector2 dir1)
	{
		float num = MathFunc.Dot(ref dir0, ref dir1);
		float num2 = num / (MathFunc.Magnitude(ref dir0) * MathFunc.Magnitude(ref dir1));
		num2 = Mathf.Max(num2, -1f);
		num2 = Mathf.Min(num2, 1f);
		float num3 = Mathf.Acos(num2);
		return (!float.IsNaN(num3)) ? num3 : 0f;
	}

	// Token: 0x06005323 RID: 21283 RVA: 0x00173FE2 File Offset: 0x001723E2
	public static float AngleDeg(ref Vector2 dir0, ref Vector2 dir1)
	{
		return MathFunc.Angle(ref dir0, ref dir1) * 57.29578f;
	}

	// Token: 0x06005324 RID: 21284 RVA: 0x00173FF4 File Offset: 0x001723F4
	public static float Angle(ref Vector3 dir0, ref Vector3 dir1)
	{
		float num = MathFunc.Dot(ref dir0, ref dir1);
		float num2 = num / (MathFunc.Magnitude(ref dir0) * MathFunc.Magnitude(ref dir1));
		num2 = Mathf.Max(num2, -1f);
		num2 = Mathf.Min(num2, 1f);
		float num3 = Mathf.Acos(num2);
		return (!float.IsNaN(num3)) ? num3 : 0f;
	}

	// Token: 0x06005325 RID: 21285 RVA: 0x0017404E File Offset: 0x0017244E
	public static float AngleDeg(ref Vector3 dir0, ref Vector3 dir1)
	{
		return MathFunc.Angle(ref dir0, ref dir1) * 57.29578f;
	}

	// Token: 0x06005326 RID: 21286 RVA: 0x00174060 File Offset: 0x00172460
	public static float AngleFast(ref Vector2 nDir0, ref Vector2 nDir1)
	{
		float num = MathFunc.Dot(ref nDir0, ref nDir1);
		num = Mathf.Max(num, -1f);
		num = Mathf.Min(num, 1f);
		float num2 = Mathf.Acos(num);
		return (!float.IsNaN(num2)) ? num2 : 0f;
	}

	// Token: 0x06005327 RID: 21287 RVA: 0x001740AA File Offset: 0x001724AA
	public static float AngleDegFast(ref Vector2 nDir0, ref Vector2 nDir1)
	{
		return MathFunc.AngleFast(ref nDir0, ref nDir1) * 57.29578f;
	}

	// Token: 0x06005328 RID: 21288 RVA: 0x001740BC File Offset: 0x001724BC
	public static float AngleFast(ref Vector3 nDir0, ref Vector3 nDir1)
	{
		float num = MathFunc.Dot(ref nDir0, ref nDir1);
		num = Mathf.Max(num, -1f);
		num = Mathf.Min(num, 1f);
		float num2 = Mathf.Acos(num);
		return (!float.IsNaN(num2)) ? num2 : 0f;
	}

	// Token: 0x06005329 RID: 21289 RVA: 0x00174107 File Offset: 0x00172507
	public static float AngleDegFast(ref Vector3 nDir0, ref Vector3 nDir1)
	{
		return MathFunc.AngleFast(ref nDir0, ref nDir1) * 57.29578f;
	}

	// Token: 0x0600532A RID: 21290 RVA: 0x00174116 File Offset: 0x00172516
	public static float AngleY(ref Vector3 dir)
	{
		return Mathf.Atan2(dir.x, dir.z);
	}

	// Token: 0x0600532B RID: 21291 RVA: 0x00174129 File Offset: 0x00172529
	public static float AngleDegY(ref Vector3 dir)
	{
		return Mathf.Atan2(dir.x, dir.z) * 57.29578f;
	}

	// Token: 0x0600532C RID: 21292 RVA: 0x00174144 File Offset: 0x00172544
	public static float DegToRad(float deg)
	{
		while (deg > 180f)
		{
			deg -= 360f;
		}
		while (deg < -180f)
		{
			deg += 360f;
		}
		return deg / 180f * 3.1415927f;
	}

	// Token: 0x0600532D RID: 21293 RVA: 0x00174194 File Offset: 0x00172594
	public static void CalcDigits(int cnt, out int digitCnt, ref int[] digits)
	{
		int i = 0;
		while (cnt > 0)
		{
			digits[i] = cnt % 10;
			cnt /= 10;
			i++;
		}
		digitCnt = i;
		while (i < digits.Length)
		{
			digits[i] = 0;
			i++;
		}
	}

	// Token: 0x0600532E RID: 21294 RVA: 0x001741DC File Offset: 0x001725DC
	public static void Swap(ref float f1, ref float f2)
	{
		float num = f1;
		f1 = f2;
		f2 = num;
	}

	// Token: 0x0600532F RID: 21295 RVA: 0x001741F4 File Offset: 0x001725F4
	public static void Swap(ref int n1, ref int n2)
	{
		int num = n1;
		n1 = n2;
		n2 = num;
	}

	// Token: 0x06005330 RID: 21296 RVA: 0x0017420B File Offset: 0x0017260B
	public static void CatmullRom(out float Result, float[] Source, float Rate)
	{
		MathFunc.CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
	}

	// Token: 0x06005331 RID: 21297 RVA: 0x00174220 File Offset: 0x00172620
	public static void CatmullRom(out float Result, float Source0, float Source1, float Source2, float Source3, float Rate)
	{
		float num = Rate * Rate;
		float num2 = num * Rate;
		float num3 = -num2 + 2f * num - Rate;
		float num4 = 3f * num2 - 5f * num + 2f;
		float num5 = -3f * num2 + 4f * num + Rate;
		float num6 = num2 - num;
		Result = (num3 * Source0 + num4 * Source1 + num5 * Source2 + num6 * Source3) * 0.5f;
	}

	// Token: 0x06005332 RID: 21298 RVA: 0x0017428F File Offset: 0x0017268F
	public static void CatmullRom(out Vector2 Result, Vector2[] Source, float Rate)
	{
		MathFunc.CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
	}

	// Token: 0x06005333 RID: 21299 RVA: 0x001742C8 File Offset: 0x001726C8
	public static void CatmullRom(out Vector2 Result, Vector2 Source0, Vector2 Source1, Vector2 Source2, Vector2 Source3, float Rate)
	{
		float num = Rate * Rate;
		float num2 = num * Rate;
		float num3 = -num2 + 2f * num - Rate;
		float num4 = 3f * num2 - 5f * num + 2f;
		float num5 = -3f * num2 + 4f * num + Rate;
		float num6 = num2 - num;
		Result.x = (num3 * Source0.x + num4 * Source1.x + num5 * Source2.x + num6 * Source3.x) * 0.5f;
		Result.y = (num3 * Source0.y + num4 * Source1.y + num5 * Source2.y + num6 * Source3.y) * 0.5f;
	}

	// Token: 0x06005334 RID: 21300 RVA: 0x00174388 File Offset: 0x00172788
	public static void CatmullRom(out Vector3 Result, Vector3[] Source, float Rate)
	{
		MathFunc.CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
	}

	// Token: 0x06005335 RID: 21301 RVA: 0x001743C4 File Offset: 0x001727C4
	public static void CatmullRom(out Vector3 Result, Vector3 Source0, Vector3 Source1, Vector3 Source2, Vector3 Source3, float Rate)
	{
		float num = Rate * Rate;
		float num2 = num * Rate;
		float num3 = -num2 + 2f * num - Rate;
		float num4 = 3f * num2 - 5f * num + 2f;
		float num5 = -3f * num2 + 4f * num + Rate;
		float num6 = num2 - num;
		Result.x = (num3 * Source0.x + num4 * Source1.x + num5 * Source2.x + num6 * Source3.x) * 0.5f;
		Result.y = (num3 * Source0.y + num4 * Source1.y + num5 * Source2.y + num6 * Source3.y) * 0.5f;
		Result.z = (num3 * Source0.z + num4 * Source1.z + num5 * Source2.z + num6 * Source3.z) * 0.5f;
	}

	// Token: 0x06005336 RID: 21302 RVA: 0x001744BA File Offset: 0x001728BA
	public static void CatmullRom(out Vector4 Result, Vector4[] Source, float Rate)
	{
		MathFunc.CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
	}

	// Token: 0x06005337 RID: 21303 RVA: 0x001744F4 File Offset: 0x001728F4
	public static void CatmullRom(out Vector4 Result, Vector4 Source0, Vector4 Source1, Vector4 Source2, Vector4 Source3, float Rate)
	{
		float num = Rate * Rate;
		float num2 = num * Rate;
		float num3 = -num2 + 2f * num - Rate;
		float num4 = 3f * num2 - 5f * num + 2f;
		float num5 = -3f * num2 + 4f * num + Rate;
		float num6 = num2 - num;
		Result.x = (num3 * Source0.x + num4 * Source1.x + num5 * Source2.x + num6 * Source3.x) * 0.5f;
		Result.y = (num3 * Source0.y + num4 * Source1.y + num5 * Source2.y + num6 * Source3.y) * 0.5f;
		Result.z = (num3 * Source0.z + num4 * Source1.z + num5 * Source2.z + num6 * Source3.z) * 0.5f;
		Result.w = (num3 * Source0.w + num4 * Source1.w + num5 * Source2.w + num6 * Source3.w) * 0.5f;
	}

	// Token: 0x06005338 RID: 21304 RVA: 0x00174620 File Offset: 0x00172A20
	public static Vector2 Bezier(Vector2 st, Vector2 ed, Vector2 pt, float t)
	{
		Vector2 result;
		result.x = st.x * (1f - t) * (1f - t) + 2f * pt.x * t * (1f - t) + ed.x * t * t;
		result.y = st.y * (1f - t) * (1f - t) + 2f * pt.y * t * (1f - t) + ed.y * t * t;
		return result;
	}

	// Token: 0x06005339 RID: 21305 RVA: 0x001746B4 File Offset: 0x00172AB4
	public static Vector3 Bezier(Vector3 st, Vector3 ed, Vector3 pt, float t)
	{
		Vector3 result;
		result.x = st.x * (1f - t) * (1f - t) + 2f * pt.x * t * (1f - t) + ed.x * t * t;
		result.y = st.y * (1f - t) * (1f - t) + 2f * pt.y * t * (1f - t) + ed.y * t * t;
		result.z = st.z * (1f - t) * (1f - t) + 2f * pt.z * t * (1f - t) + ed.z * t * t;
		return result;
	}

	// Token: 0x0600533A RID: 21306 RVA: 0x00174788 File Offset: 0x00172B88
	public static Vector2 BezierTangent(Vector2 st, Vector2 ed, Vector2 pt, float t)
	{
		Vector2 result;
		result.x = 2f * st.x * (1f - t) + pt.x * (1f - 2f * t) + 2f * ed.x * t;
		result.y = 2f * st.y * (1f - t) + pt.y * (1f - 2f * t) + 2f * ed.y * t;
		return result;
	}

	// Token: 0x0600533B RID: 21307 RVA: 0x0017481C File Offset: 0x00172C1C
	public static Vector2 Bezier(Vector2 st, Vector2 ed, Vector2 pt, Vector2 pt2, float t)
	{
		Vector2 result;
		result.x = st.x * (1f - t) * (1f - t) * (1f - t) + 3f * pt.x * t * (1f - t) * (1f - t) + 3f * pt2.x * t * t * (1f - t) + ed.x * t * t * t;
		result.y = st.y * (1f - t) * (1f - t) * (1f - t) + 3f * pt.y * t * (1f - t) * (1f - t) + 3f * pt2.y * t * t * (1f - t) + ed.y * t * t * t;
		return result;
	}

	// Token: 0x0600533C RID: 21308 RVA: 0x00174920 File Offset: 0x00172D20
	public static Vector3 Bezier(Vector3 st, Vector3 ed, Vector3 pt, Vector3 pt2, float t)
	{
		Vector3 result;
		result.x = st.x * (1f - t) * (1f - t) * (1f - t) + 3f * pt.x * t * (1f - t) * (1f - t) + 3f * pt2.x * t * t * (1f - t) + ed.x * t * t * t;
		result.y = st.y * (1f - t) * (1f - t) * (1f - t) + 3f * pt.y * t * (1f - t) * (1f - t) + 3f * pt2.y * t * t * (1f - t) + ed.y * t * t * t;
		result.z = st.z * (1f - t) * (1f - t) * (1f - t) + 3f * pt.z * t * (1f - t) * (1f - t) + 3f * pt2.z * t * t * (1f - t) + ed.z * t * t * t;
		return result;
	}

	// Token: 0x0600533D RID: 21309 RVA: 0x00174A9C File Offset: 0x00172E9C
	public static float Hermite_CalcValue(float p0, float d0, float p1, float d1, float t)
	{
		float num = t * t;
		float num2 = num * t;
		float num3 = 2f * num2 - 3f * num + 1f;
		float num4 = -2f * num2 + 3f * num;
		float num5 = num2 - 2f * num + t;
		float num6 = num2 - num;
		return num3 * p0 + num4 * p1 + num5 * d0 + num6 * d1;
	}

	// Token: 0x0600533E RID: 21310 RVA: 0x00174B04 File Offset: 0x00172F04
	public static Vector2 Hermite_CalcValue(Vector2 p0, Vector2 d0, Vector2 p1, Vector2 d1, float t)
	{
		float num = t * t;
		float num2 = num * t;
		float d2 = 2f * num2 - 3f * num + 1f;
		float d3 = -2f * num2 + 3f * num;
		float d4 = num2 - 2f * num + t;
		float d5 = num2 - num;
		Vector2 a = p0 * d2;
		Vector2 b = p1 * d3;
		Vector2 b2 = d0 * d4;
		Vector2 b3 = d1 * d5;
		return a + b + b2 + b3;
	}

	// Token: 0x0600533F RID: 21311 RVA: 0x00174B98 File Offset: 0x00172F98
	public static Vector3 Hermite_CalcValue(Vector3 p0, Vector3 d0, Vector3 p1, Vector3 d1, float t)
	{
		float num = t * t;
		float num2 = num * t;
		float d2 = 2f * num2 - 3f * num + 1f;
		float d3 = -2f * num2 + 3f * num;
		float d4 = num2 - 2f * num + t;
		float d5 = num2 - num;
		Vector3 a = p0 * d2;
		Vector3 b = p1 * d3;
		Vector3 b2 = d0 * d4;
		Vector3 b3 = d1 * d5;
		return a + b + b2 + b3;
	}

	// Token: 0x06005340 RID: 21312 RVA: 0x00174C2C File Offset: 0x0017302C
	public static Vector4 Hermite_CalcValue_V4(Vector4 p0, Vector4 d0, Vector4 p1, Vector4 d1, float t)
	{
		float num = t * t;
		float num2 = num * t;
		float num3 = 2f * num2 - 3f * num + 1f;
		float num4 = -2f * num2 + 3f * num;
		float num5 = num2 - 2f * num + t;
		float num6 = num2 - num;
		float x = num3 * p0.x + num4 * p1.x + num5 * d0.x + num6 * d1.x;
		float y = num3 * p0.y + num4 * p1.y + num5 * d0.y + num6 * d1.y;
		float z = num3 * p0.z + num4 * p1.z + num5 * d0.z + num6 * d1.z;
		float w = num3 * p0.w + num4 * p1.w + num5 * d0.w + num6 * d1.w;
		Vector4 result = new Vector4(x, y, z, w);
		return result;
	}

	// Token: 0x06005341 RID: 21313 RVA: 0x00174D3A File Offset: 0x0017313A
	public static Quaternion QuaternionFromMatrix(Matrix4x4 mat)
	{
		return Quaternion.LookRotation(mat.GetColumn(2), mat.GetColumn(1));
	}

	// Token: 0x06005342 RID: 21314 RVA: 0x00174D5C File Offset: 0x0017315C
	public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint)
	{
		intersection = Vector3.zero;
		float num = Vector3.Dot(planePoint - linePoint, planeNormal);
		float num2 = Vector3.Dot(lineVec, planeNormal);
		if (num2 != 0f)
		{
			float d = num / num2;
			Vector3 b = Vector3.Normalize(lineVec) * d;
			intersection = linePoint + b;
			return true;
		}
		return false;
	}

	// Token: 0x06005343 RID: 21315 RVA: 0x00174DB8 File Offset: 0x001731B8
	public static bool IsLineInRectangle(Vector3 linePoint1, Vector3 linePoint2, Vector3 rectA, Vector3 rectB, Vector3 rectC, Vector3 rectD)
	{
		bool flag = false;
		bool flag2 = MathFunc.IsPointInRectangle(linePoint1, rectA, rectC, rectB, rectD);
		if (!flag2)
		{
			flag = MathFunc.IsPointInRectangle(linePoint2, rectA, rectC, rectB, rectD);
		}
		if (!flag2 && !flag)
		{
			bool flag3 = MathFunc.AreLineSegmentsCrossing(linePoint1, linePoint2, rectA, rectB);
			bool flag4 = MathFunc.AreLineSegmentsCrossing(linePoint1, linePoint2, rectB, rectC);
			bool flag5 = MathFunc.AreLineSegmentsCrossing(linePoint1, linePoint2, rectC, rectD);
			bool flag6 = MathFunc.AreLineSegmentsCrossing(linePoint1, linePoint2, rectD, rectA);
			return flag3 || flag4 || flag5 || flag6;
		}
		return true;
	}

	// Token: 0x06005344 RID: 21316 RVA: 0x00174E44 File Offset: 0x00173244
	public static Vector3 AddVectorLength(Vector3 vector, float size)
	{
		float num = Vector3.Magnitude(vector);
		float num2 = num + size;
		float d = num2 / num;
		return vector * d;
	}

	// Token: 0x06005345 RID: 21317 RVA: 0x00174E68 File Offset: 0x00173268
	public static bool IsPointInRectangle(Vector3 point, Vector3 rectA, Vector3 rectC, Vector3 rectB, Vector3 rectD)
	{
		Vector3 vector = rectC - rectA;
		float size = -(vector.magnitude / 2f);
		vector = MathFunc.AddVectorLength(vector, size);
		Vector3 linePoint = rectA + vector;
		Vector3 vector2 = rectB - rectA;
		float num = vector2.magnitude / 2f;
		Vector3 vector3 = rectD - rectA;
		float num2 = vector3.magnitude / 2f;
		Vector3 a = MathFunc.ProjectPointOnLine(linePoint, vector2.normalized, point);
		float magnitude = (a - point).magnitude;
		a = MathFunc.ProjectPointOnLine(linePoint, vector3.normalized, point);
		float magnitude2 = (a - point).magnitude;
		return magnitude2 <= num && magnitude <= num2;
	}

	// Token: 0x06005346 RID: 21318 RVA: 0x00174F24 File Offset: 0x00173324
	public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
	{
		Vector3 lhs = point - linePoint;
		float d = Vector3.Dot(lhs, lineVec);
		return linePoint + lineVec * d;
	}

	// Token: 0x06005347 RID: 21319 RVA: 0x00174F50 File Offset: 0x00173350
	public static bool AreLineSegmentsCrossing(Vector3 pointA1, Vector3 pointA2, Vector3 pointB1, Vector3 pointB2)
	{
		Vector3 vector = pointA2 - pointA1;
		Vector3 vector2 = pointB2 - pointB1;
		Vector3 point;
		Vector3 point2;
		bool flag = MathFunc.ClosestPointsOnTwoLines(out point, out point2, pointA1, vector.normalized, pointB1, vector2.normalized);
		if (flag)
		{
			int num = MathFunc.PointOnWhichSideOfLineSegment(pointA1, pointA2, point);
			int num2 = MathFunc.PointOnWhichSideOfLineSegment(pointB1, pointB2, point2);
			return num == 0 && num2 == 0;
		}
		return false;
	}

	// Token: 0x06005348 RID: 21320 RVA: 0x00174FB4 File Offset: 0x001733B4
	public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
	{
		closestPointLine1 = Vector3.zero;
		closestPointLine2 = Vector3.zero;
		float num = Vector3.Dot(lineVec1, lineVec1);
		float num2 = Vector3.Dot(lineVec1, lineVec2);
		float num3 = Vector3.Dot(lineVec2, lineVec2);
		float num4 = num * num3 - num2 * num2;
		if (num4 != 0f)
		{
			Vector3 rhs = linePoint1 - linePoint2;
			float num5 = Vector3.Dot(lineVec1, rhs);
			float num6 = Vector3.Dot(lineVec2, rhs);
			float d = (num2 * num6 - num5 * num3) / num4;
			float d2 = (num * num6 - num5 * num2) / num4;
			closestPointLine1 = linePoint1 + lineVec1 * d;
			closestPointLine2 = linePoint2 + lineVec2 * d2;
			return true;
		}
		return false;
	}

	// Token: 0x06005349 RID: 21321 RVA: 0x0017506C File Offset: 0x0017346C
	public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
	{
		Vector3 rhs = linePoint2 - linePoint1;
		Vector3 lhs = point - linePoint1;
		float num = Vector3.Dot(lhs, rhs);
		if (num <= 0f)
		{
			return 1;
		}
		if (lhs.magnitude <= rhs.magnitude)
		{
			return 0;
		}
		return 2;
	}

	// Token: 0x0400544E RID: 21582
	public static System.Random rand;
}
