﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020004DC RID: 1244
public class AppsFlyersHandler : MonoBehaviour
{
	// Token: 0x060018B0 RID: 6320 RVA: 0x00080CD8 File Offset: 0x0007F0D8
	private void Start()
	{
		if (AppsFlyersHandler.Instance == null)
		{
			AppsFlyer.init(this.m_AndroidDevKey);
			AppsFlyer.setAppID("com.aniplex.kirarafantasia");
			AppsFlyer.setIsDebug(this.m_isDebug);
			AppsFlyer.getConversionData();
			AppsFlyer.trackAppLaunch();
			AppsFlyersHandler.Instance = this;
		}
		else
		{
			Helper.DestroyAll(base.gameObject);
		}
	}

	// Token: 0x060018B1 RID: 6321 RVA: 0x00080D35 File Offset: 0x0007F135
	public void TrackEvent(string eventName, string eventValue)
	{
		AppsFlyer.trackEvent(eventName, eventValue);
	}

	// Token: 0x060018B2 RID: 6322 RVA: 0x00080D3E File Offset: 0x0007F13E
	public void TrackRichEvent(string eventName, Dictionary<string, string> eventValue)
	{
		AppsFlyer.trackRichEvent(eventName, eventValue);
	}

	// Token: 0x060018B3 RID: 6323 RVA: 0x00080D48 File Offset: 0x0007F148
	public void TrackEvent_Purchase(long revenueValue, string contentType, int contentID = 0, string currency = "JPY")
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		if (revenueValue > 0L)
		{
			dictionary.Add("af_revenue", revenueValue.ToString());
		}
		if (contentType != null)
		{
			dictionary.Add("af_content_type", contentType);
		}
		if (contentID >= 0)
		{
			dictionary.Add("af_content_id", contentID.ToString());
		}
		if (currency != null)
		{
			dictionary.Add("af_currency", currency);
		}
		this.TrackRichEvent("af_purchase", dictionary);
	}

	// Token: 0x060018B4 RID: 6324 RVA: 0x00080DCC File Offset: 0x0007F1CC
	public void TrackEvent_TutorialClear()
	{
		this.TrackRichEvent("af_tutorial_completion", new Dictionary<string, string>
		{
			{
				"af_success",
				"Tutorial"
			}
		});
	}

	// Token: 0x060018B5 RID: 6325 RVA: 0x00080DFB File Offset: 0x0007F1FB
	public void didReceiveConversionData(string conversionData)
	{
		if (conversionData.Contains("Non"))
		{
		}
	}

	// Token: 0x060018B6 RID: 6326 RVA: 0x00080E12 File Offset: 0x0007F212
	public void didReceiveConversionDataWithError(string error)
	{
	}

	// Token: 0x060018B7 RID: 6327 RVA: 0x00080E14 File Offset: 0x0007F214
	public void onAppOpenAttribution(string validateResult)
	{
	}

	// Token: 0x060018B8 RID: 6328 RVA: 0x00080E16 File Offset: 0x0007F216
	public void onAppOpenAttributionFailure(string error)
	{
	}

	// Token: 0x04001F58 RID: 8024
	public static AppsFlyersHandler Instance;

	// Token: 0x04001F59 RID: 8025
	public bool m_isDebug = true;

	// Token: 0x04001F5A RID: 8026
	public string m_AndroidDevKey;

	// Token: 0x04001F5B RID: 8027
	public string m_iOSDevKey;

	// Token: 0x04001F5C RID: 8028
	public string m_iOSAppID;
}
