﻿using System;
using UnityEngine;

// Token: 0x02000FAC RID: 4012
public class KeyboardInput : SingletonMonoBehaviour<KeyboardInput>
{
	// Token: 0x06005306 RID: 21254 RVA: 0x00173BD1 File Offset: 0x00171FD1
	public void Open(string placeHolder)
	{
		this.m_active = true;
		this.m_text = string.Empty;
		this.m_keyboard = TouchScreenKeyboard.Open(this.m_text, TouchScreenKeyboardType.Default, true, false, false, false, placeHolder);
	}

	// Token: 0x06005307 RID: 21255 RVA: 0x00173BFC File Offset: 0x00171FFC
	public void OpenMultiline(string placeHolder)
	{
		this.m_active = true;
		this.m_text = string.Empty;
		this.m_keyboard = TouchScreenKeyboard.Open(this.m_text, TouchScreenKeyboardType.Default, true, true, false, false, placeHolder);
	}

	// Token: 0x06005308 RID: 21256 RVA: 0x00173C27 File Offset: 0x00172027
	public void OpenPassword(string placeHolder)
	{
		this.m_active = true;
		this.m_text = string.Empty;
		this.m_keyboard = TouchScreenKeyboard.Open(this.m_text, TouchScreenKeyboardType.Default, true, false, true, false, placeHolder);
	}

	// Token: 0x06005309 RID: 21257 RVA: 0x00173C52 File Offset: 0x00172052
	public void Open(bool autocorrection, bool multiline, bool secure, bool alert, string placeHolder)
	{
		this.m_active = true;
		this.m_text = string.Empty;
		this.m_keyboard = TouchScreenKeyboard.Open(this.m_text, TouchScreenKeyboardType.Default, autocorrection, multiline, secure, alert, placeHolder);
		Debug.Log("TouchScreenKeyboard.Open");
	}

	// Token: 0x0600530A RID: 21258 RVA: 0x00173C89 File Offset: 0x00172089
	public void Update()
	{
		if (this.m_keyboard != null && this.m_keyboard.done)
		{
			this.m_text = this.m_keyboard.text;
			this.m_keyboard = null;
			this.m_active = false;
		}
	}

	// Token: 0x17000599 RID: 1433
	// (get) Token: 0x0600530B RID: 21259 RVA: 0x00173CC5 File Offset: 0x001720C5
	public bool isDone
	{
		get
		{
			return !this.m_active;
		}
	}

	// Token: 0x1700059A RID: 1434
	// (get) Token: 0x0600530C RID: 21260 RVA: 0x00173CD0 File Offset: 0x001720D0
	public string text
	{
		get
		{
			return this.m_text;
		}
	}

	// Token: 0x0400544B RID: 21579
	private TouchScreenKeyboard m_keyboard;

	// Token: 0x0400544C RID: 21580
	private string m_text = string.Empty;

	// Token: 0x0400544D RID: 21581
	private bool m_active;
}
