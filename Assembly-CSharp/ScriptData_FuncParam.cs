﻿using System;

// Token: 0x02000E73 RID: 3699
[Serializable]
public struct ScriptData_FuncParam
{
	// Token: 0x04004D77 RID: 19831
	public string funcName;

	// Token: 0x04004D78 RID: 19832
	public int argNum;

	// Token: 0x04004D79 RID: 19833
	public int argType1;

	// Token: 0x04004D7A RID: 19834
	public string m_value1;

	// Token: 0x04004D7B RID: 19835
	public int argType2;

	// Token: 0x04004D7C RID: 19836
	public string m_value2;

	// Token: 0x04004D7D RID: 19837
	public int argType3;

	// Token: 0x04004D7E RID: 19838
	public string m_value3;

	// Token: 0x04004D7F RID: 19839
	public int argType4;

	// Token: 0x04004D80 RID: 19840
	public string m_value4;

	// Token: 0x04004D81 RID: 19841
	public int argType5;

	// Token: 0x04004D82 RID: 19842
	public string m_value5;

	// Token: 0x04004D83 RID: 19843
	public int argType6;

	// Token: 0x04004D84 RID: 19844
	public string m_value6;
}
