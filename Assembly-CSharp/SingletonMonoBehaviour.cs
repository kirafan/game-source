﻿using System;
using UnityEngine;

// Token: 0x02000FB1 RID: 4017
public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
{
	// Token: 0x1700059B RID: 1435
	// (get) Token: 0x06005379 RID: 21369 RVA: 0x000AF450 File Offset: 0x000AD850
	public static T Instance
	{
		get
		{
			if (SingletonMonoBehaviour<T>.instance == null)
			{
				SingletonMonoBehaviour<T>.instance = (T)((object)UnityEngine.Object.FindObjectOfType(typeof(T)));
				if (SingletonMonoBehaviour<T>.instance == null)
				{
					Debug.LogWarning(typeof(T) + " is nothing");
				}
			}
			return SingletonMonoBehaviour<T>.instance;
		}
	}

	// Token: 0x0600537A RID: 21370 RVA: 0x000AF4BE File Offset: 0x000AD8BE
	protected void Awake()
	{
		this.CheckInstance();
	}

	// Token: 0x0600537B RID: 21371 RVA: 0x000AF4C8 File Offset: 0x000AD8C8
	protected bool CheckInstance()
	{
		if (SingletonMonoBehaviour<T>.instance == null)
		{
			SingletonMonoBehaviour<T>.instance = (T)((object)this);
			return true;
		}
		if (SingletonMonoBehaviour<T>.Instance == this)
		{
			return true;
		}
		UnityEngine.Object.Destroy(this);
		return false;
	}

	// Token: 0x04005460 RID: 21600
	protected static T instance;
}
