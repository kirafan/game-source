﻿using System;
using UnityEngine;

// Token: 0x02000FD4 RID: 4052
public class SceneViewer : MonoBehaviour
{
	// Token: 0x06005469 RID: 21609 RVA: 0x0017A71A File Offset: 0x00178B1A
	private void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
	{
	}

	// Token: 0x0600546A RID: 21610 RVA: 0x0017A71C File Offset: 0x00178B1C
	private void Start()
	{
		this.m_ScenePrefabArray = new SceneViewer.ScenePrefab[10];
		this.m_SceneInstanceArray = null;
		this.m_AnimCtrl = base.gameObject.AddComponent<MeigeAnimCtrl>();
		this.m_Phase = SceneViewer.ePhase.ePhase_Main;
	}

	// Token: 0x0600546B RID: 21611 RVA: 0x0017A74C File Offset: 0x00178B4C
	private void Update()
	{
		if (this.m_bInstantiate)
		{
			if (this.m_SceneInstanceArray != null)
			{
				foreach (SceneViewer.SceneInstance sceneInstance in this.m_SceneInstanceArray)
				{
					if (sceneInstance != null)
					{
						Helper.DestroyAll(sceneInstance.m_AnimClipGO);
						Helper.DestroyAll(sceneInstance.m_TargetGO);
					}
				}
				this.m_SceneInstanceArray = null;
			}
			this.m_AnimCtrl.Init();
			this.m_ClipName = null;
			this.m_Phase = SceneViewer.ePhase.ePhase_Instantiate;
			this.m_bInstantiate = false;
		}
		switch (this.m_Phase)
		{
		case SceneViewer.ePhase.ePhase_Instantiate:
		{
			this.m_SceneInstanceArray = new SceneViewer.SceneInstance[this.m_ScenePrefabArray.Length];
			int num = 0;
			foreach (SceneViewer.ScenePrefab scenePrefab in this.m_ScenePrefabArray)
			{
				if (!(scenePrefab.m_TargetPrefab == null))
				{
					this.m_SceneInstanceArray[num] = new SceneViewer.SceneInstance();
					GameObject gameObject = (GameObject)UnityEngine.Object.Instantiate(scenePrefab.m_TargetPrefab);
					gameObject.GetComponentInChildren<MeigeAnimEventNotifier>().AddNotify(new MeigeAnimEventNotifier.OnEvent(this.OnMeigeAnimEvent));
					this.m_SceneInstanceArray[num].m_TargetGO = gameObject;
					this.m_SceneInstanceArray[num].m_MsbHndl = gameObject.GetComponentInChildren<MsbHandler>();
					if (scenePrefab.m_bCameraAnim && this.m_SceneInstanceArray[num].m_MsbHndl != null)
					{
						this.m_SceneInstanceArray[num].m_MsbHndl.AttachCamera(0, Camera.main);
					}
					if (scenePrefab.m_AnimClipPrefab != null)
					{
						GameObject gameObject2 = (GameObject)UnityEngine.Object.Instantiate(scenePrefab.m_AnimClipPrefab);
						this.m_SceneInstanceArray[num].m_AnimClipGO = gameObject2;
						this.m_SceneInstanceArray[num].m_ClipHolder = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
						if (this.m_ClipName == null && this.m_SceneInstanceArray[num].m_ClipHolder != null)
						{
							string[] array = scenePrefab.m_AnimClipPrefab.name.Split("@".ToCharArray());
							if (array.Length < 2)
							{
								return;
							}
							this.m_ClipName = array[1];
						}
					}
					num++;
				}
			}
			this.m_Phase = SceneViewer.ePhase.ePhase_Setup;
			break;
		}
		case SceneViewer.ePhase.ePhase_Setup:
		{
			bool flag = false;
			this.m_AnimCtrl.Open();
			foreach (SceneViewer.SceneInstance sceneInstance2 in this.m_SceneInstanceArray)
			{
				if (sceneInstance2 != null)
				{
					if (!(sceneInstance2.m_ClipHolder == null))
					{
						if (sceneInstance2.m_MsbHndl != null && sceneInstance2.m_ClipHolder != null)
						{
							this.m_AnimCtrl.AddClip(sceneInstance2.m_MsbHndl, sceneInstance2.m_ClipHolder);
						}
						flag = true;
					}
				}
			}
			this.m_AnimCtrl.Close();
			if (flag)
			{
				this.m_Phase = SceneViewer.ePhase.ePhase_Play;
			}
			else
			{
				this.m_Phase = SceneViewer.ePhase.ePhase_Main;
			}
			break;
		}
		case SceneViewer.ePhase.ePhase_Play:
			this.m_AnimCtrl.Play(this.m_ClipName, 0f, WrapMode.Loop);
			this.m_Phase = SceneViewer.ePhase.ePhase_Main;
			break;
		}
	}

	// Token: 0x0400557F RID: 21887
	public bool m_bInstantiate;

	// Token: 0x04005580 RID: 21888
	public SceneViewer.ScenePrefab[] m_ScenePrefabArray;

	// Token: 0x04005581 RID: 21889
	private SceneViewer.SceneInstance[] m_SceneInstanceArray;

	// Token: 0x04005582 RID: 21890
	private string m_ClipName;

	// Token: 0x04005583 RID: 21891
	private SceneViewer.ePhase m_Phase;

	// Token: 0x04005584 RID: 21892
	private MeigeAnimCtrl m_AnimCtrl;

	// Token: 0x02000FD5 RID: 4053
	private enum ePhase
	{
		// Token: 0x04005586 RID: 21894
		ePhase_None,
		// Token: 0x04005587 RID: 21895
		ePhase_Instantiate,
		// Token: 0x04005588 RID: 21896
		ePhase_Setup,
		// Token: 0x04005589 RID: 21897
		ePhase_Play,
		// Token: 0x0400558A RID: 21898
		ePhase_Main
	}

	// Token: 0x02000FD6 RID: 4054
	[Serializable]
	public class ScenePrefab
	{
		// Token: 0x0600546C RID: 21612 RVA: 0x0017AA89 File Offset: 0x00178E89
		public ScenePrefab()
		{
			this.m_TargetPrefab = null;
			this.m_AnimClipPrefab = null;
			this.m_bCameraAnim = false;
		}

		// Token: 0x0400558B RID: 21899
		public UnityEngine.Object m_TargetPrefab;

		// Token: 0x0400558C RID: 21900
		public UnityEngine.Object m_AnimClipPrefab;

		// Token: 0x0400558D RID: 21901
		public bool m_bCameraAnim;
	}

	// Token: 0x02000FD7 RID: 4055
	public class SceneInstance
	{
		// Token: 0x0600546D RID: 21613 RVA: 0x0017AAA6 File Offset: 0x00178EA6
		public SceneInstance()
		{
			this.m_TargetGO = null;
			this.m_MsbHndl = null;
			this.m_ClipHolder = null;
			this.m_AnimClipGO = null;
		}

		// Token: 0x0400558E RID: 21902
		public GameObject m_TargetGO;

		// Token: 0x0400558F RID: 21903
		public MsbHandler m_MsbHndl;

		// Token: 0x04005590 RID: 21904
		public MeigeAnimClipHolder m_ClipHolder;

		// Token: 0x04005591 RID: 21905
		public GameObject m_AnimClipGO;
	}
}
