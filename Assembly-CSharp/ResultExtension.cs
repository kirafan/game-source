﻿using System;
using WWWTypes;

// Token: 0x02000C97 RID: 3223
public static class ResultExtension
{
	// Token: 0x060040E3 RID: 16611 RVA: 0x0013FF7F File Offset: 0x0013E37F
	public static string ToMessageString(this ResultCode e)
	{
		return ResultExtension.getString(e).Replace("<br>", "\n");
	}

	// Token: 0x060040E4 RID: 16612 RVA: 0x0013FF96 File Offset: 0x0013E396
	public static string ToDebugString(this ResultCode e)
	{
		return string.Empty;
	}

	// Token: 0x060040E5 RID: 16613 RVA: 0x0013FFA0 File Offset: 0x0013E3A0
	private static string getString(ResultCode e)
	{
		switch (e)
		{
		case ResultCode.WEAPON_UPGRADE_LIMIT:
			return "ぶきの強化上限です。";
		case ResultCode.CHARACTER_UPGRADE_LIMIT:
			return "キャラクターの強化上限です。";
		case ResultCode.FACILITY_EXT_LIMIT:
			return "タウン施設の所持枠の拡張上限に達しています。";
		case ResultCode.SUPPORT_CHAR_LIMIT:
			return "サポートキャラクター数の上限に達しています。";
		case ResultCode.SUPPORT_LIMIT:
			return "サポート編成数の上限に達しています。";
		case ResultCode.ROOM_OBJECT_EXT_LIMIT:
			return "ルームオブジェクトの所持枠の拡張上限に達しています。";
		case ResultCode.WEAPON_EXT_LIMIT:
			return "ぶきの所持枠の拡張上限に達しています。";
		default:
			switch (e)
			{
			case ResultCode.PLAYER_ALREADY_EXISTS:
				return "通信エラーが発生しました。<br>[エラーコード：201]";
			case ResultCode.PLAYER_NOT_FOUND:
				return "該当するユーザーが見つかりませんでした。<br>[エラーコード：202]";
			case ResultCode.PLAYER_SESSION_EXPIRED:
				return "セッションの有効期限が切れています。<br>再度ログインを行なってください。";
			case ResultCode.PLAYER_SUSPENDED:
				return "休止中のユーザーです。";
			case ResultCode.PLAYER_STOPPED:
				return "停止中のユーザーです。";
			case ResultCode.PLAYER_DELETED:
				return "削除済のユーザーです。";
			default:
				switch (e)
				{
				case ResultCode.CURL_CONNECTION_TIMEOUT:
					return "cURL 通信エラー";
				case ResultCode.CURL_CONNETION_ERROR:
					return "cURL タイムアウト";
				case ResultCode.APP_STORE_ERROR:
					return "課金エラー (App Store)";
				case ResultCode.GOOGLE_PLAY_ERROR:
					return "課金エラー (Google Play)";
				case ResultCode.APP_STORE_INVALID_RECEIPT:
					return "レシートの検証に失敗しました。";
				case ResultCode.GOOGLE_PLAY_INVALID_STATUS:
					return "購入の検証に失敗しました。";
				case ResultCode.STORE_ITEM_NOT_FOUND:
					return "商品が存在しません。";
				case ResultCode.STORE_ALREADY_PURCHASED:
					return "すでに購入済みの商品です。";
				case ResultCode.PURCHASE_GEM_OVERFLOW:
					return "星彩石の購入上限です。";
				default:
					switch (e)
					{
					case ResultCode.GOLD_IS_SHORT:
						return "コインが不足しています。";
					case ResultCode.ITEM_IS_SHORT:
						return "所持アイテムが不足しています。";
					case ResultCode.UNLIMITED_GEM_IS_SHORT:
						return "有償の星彩石が不足しています。";
					case ResultCode.LIMITED_GEM_IS_SHORT:
						return "星彩石が不足しています。";
					case ResultCode.GEM_IS_SHORT:
						return "星彩石が不足しています。";
					case ResultCode.KIRARA_IS_SHORT:
						return "きららポイントが不足しています。";
					default:
						switch (e + 1)
						{
						case ResultCode.SUCCESS:
							return "通信エラーが発生しました。<br>通信状況をご確認ください。";
						case ResultCode.OUTDATED:
							return "成功";
						case ResultCode.APPLYING:
							return "最新のバージョンがあります。<br>アプリを更新してください。";
						case ResultCode.ACCESS_LIMITATION:
							return "申請中アプリバージョン";
						case ResultCode.TERMS_UPDATED:
							return "お使いの通信環境ではご利用頂けません。<br>[エラーコード：3]";
						case (ResultCode)5:
							return "利用規約が更新されています。<br>再度確認してください。";
						default:
							switch (e)
							{
							case ResultCode.QUEST_OUT_OF_PERIOD:
								return "クエスト開催期間の範囲外です。";
							case ResultCode.GACHA_OUT_OF_PERIOD:
								return "召喚期間の範囲外です。";
							case ResultCode.TRADE_OUT_OF_PERIOD:
								return "アイテム交換期間の範囲外です。";
							case ResultCode.BUY_OUT_OF_PERIOD:
								return "購入期間の範囲外です。";
							case ResultCode.QUEST_RETRY_LIMIT:
								return "リトライ制限のあるクエストです。";
							case ResultCode.QUEST_ORDER_LIMIT:
								return "1日の上限に達しています。";
							case ResultCode.BARGAIN_OUT_OF_PERIOD:
								return "バーゲン期間外です。";
							default:
								switch (e)
								{
								case ResultCode.INVALID_REQUEST_HASH:
									return "通信エラーが発生しました。<br>[エラーコード：101]";
								case ResultCode.INVALID_PARAMETERS:
									return "通信エラーが発生しました。<br>[エラーコード：102]";
								case ResultCode.INSUFFICIENT_PARAMETERS:
									return "通信エラーが発生しました。<br>[エラーコード：103]";
								case ResultCode.INVALID_JSON_SCHEMA:
									return "通信エラーが発生しました。<br>[エラーコード：104]";
								case ResultCode.DB_ERROR:
									return "通信エラーが発生しました。<br>[エラーコード：105]";
								case ResultCode.NG_WORD:
									return "このテキストは使用できません。";
								default:
									switch (e)
									{
									case ResultCode.PROMOTION_CODE_NOT_FOUND:
										return "コードが存在しません。";
									case ResultCode.PROMOTION_CODE_ALREADY_USED:
										return "コードは使用済みです。";
									case ResultCode.PROMOTION_CODE_OF_SAME_CAMPAIGN:
										return "同じキャンペーンのコードは<br>これ以上使用できません。";
									case ResultCode.PROMOTION_CAMPAIGN_NOT_OPEN:
										return "キャンペーン期間外です。";
									case ResultCode.PROMOTION_CAMPAIGN_ALREADY_OPEN:
										return "テスト用コードです。";
									default:
										if (e == ResultCode.NOT_YET_FRIEND)
										{
											return "フレンドではありません。";
										}
										if (e == ResultCode.ALREADY_FRIEND)
										{
											return "すでにフレンドです。";
										}
										if (e == ResultCode.UNAVAILABLE)
										{
											return "この機能は現在使用できません。";
										}
										if (e == ResultCode.OUTDATED_AB_VERSION)
										{
											return "データ更新が必要です。";
										}
										if (e == ResultCode.ALREADY_COMPLETE_MISSION)
										{
											return "すでに完了しています。";
										}
										if (e != ResultCode.UNKNOWN_ERROR)
										{
											string str = "通信エラーが発生しました。<br>";
											int num = (int)e;
											return str + num.ToString();
										}
										return "通信エラーが発生しました。<br>[エラーコード：999]";
									}
									break;
								}
								break;
							}
							break;
						case (ResultCode)11:
							return "現在メンテナンス中です。<br>メンテナンス完了までお待ちください。";
						}
						break;
					case ResultCode.STAMINA_IS_SHORT:
						return "スタミナが不足しています。";
					case ResultCode.COST_IS_SHORT:
						return "コストが不足しています。";
					}
					break;
				}
				break;
			case ResultCode.INVALID_MOVE_PARAMETERS:
				return "引き継ぎIDまたはパスワードが間違っています。";
			case ResultCode.INVALID_LINK_PARAMETERS:
				return "ご指定のアカウントで連携したユーザーが見つかりません。";
			case ResultCode.UUID_NOT_FOUND:
				return "引き継ぎが行われました。";
			}
			break;
		case ResultCode.WEAPON_LIMIT:
			return "ぶきの所持上限に達しています。";
		case ResultCode.FACILITY_LIMIT:
			return "タウン施設の所持上限に達しています。";
		case ResultCode.ROOM_OBJECT_LIMIT:
			return "ルームオブジェクトの所持上限に達しています。";
		case ResultCode.CHARACTER_LIMIT:
			return "キャラクターの所持上限に達しています。";
		case ResultCode.ITEM_LIMIT:
			return "アイテムの所持上限に達しています。";
		case ResultCode.TOWN_LIMIT:
			return "タウンの所持上限に達しています。";
		case ResultCode.ROOM_LIMIT:
			return "ルームの所持上限に達しています。";
		case ResultCode.LIMIT_BREAK_LIMIT:
			return "限界突破の上限に達しています。";
		case ResultCode.GACHA_DRAW_LIMIT:
			return "1日の上限に達しています。";
		case ResultCode.FRIEND_LIMIT:
			return "フレンド数の上限です。";
		case ResultCode.FAVORITE_ITEM:
			return "お気に入り登録されているアイテムです。";
		case ResultCode.FAVORITE_WEAPON:
			return "お気に入り登録されているぶきです。";
		case ResultCode.FAVORITE_CHARACTER:
			return "お気に入り登録されているキャラクターです。";
		case ResultCode.GACHA_STEP_DRAW_LIMIT:
			return "召喚の上限に達しています。";
		case ResultCode.ALREADY_OWN_CHARACTER:
			return "すでに所持しているキャラクターです。";
		case ResultCode.ALREADY_LOAD_WEAPON:
			return "すでに装備しているぶきです。";
		case ResultCode.ALREADY_OWN_ORB:
			return "すでに所持しているオーブです。";
		}
	}
}
