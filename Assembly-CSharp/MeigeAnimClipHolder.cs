﻿using System;
using UnityEngine;

// Token: 0x02000F5E RID: 3934
public class MeigeAnimClipHolder : MonoBehaviour
{
	// Token: 0x040050CE RID: 20686
	public AnimationClip m_UnityAnimClip;

	// Token: 0x040050CF RID: 20687
	public MabHandler m_MeigeAnimClip;
}
