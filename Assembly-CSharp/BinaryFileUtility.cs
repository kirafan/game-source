﻿using System;
using System.IO;
using System.Runtime.InteropServices;

// Token: 0x02000FA1 RID: 4001
public static class BinaryFileUtility
{
	// Token: 0x0600529C RID: 21148 RVA: 0x00171BF4 File Offset: 0x0016FFF4
	public static void Write<TStruct>(BinaryWriter writer, TStruct s) where TStruct : struct
	{
		int num = Marshal.SizeOf(typeof(TStruct));
		byte[] array = new byte[num];
		IntPtr intPtr = IntPtr.Zero;
		try
		{
			intPtr = Marshal.AllocHGlobal(num);
			Marshal.StructureToPtr(s, intPtr, false);
			Marshal.Copy(intPtr, array, 0, num);
		}
		finally
		{
			if (intPtr != IntPtr.Zero)
			{
				Marshal.FreeHGlobal(intPtr);
			}
		}
		writer.Write(array);
	}

	// Token: 0x0600529D RID: 21149 RVA: 0x00171C70 File Offset: 0x00170070
	public static TStruct Read<TStruct>(BinaryReader reader) where TStruct : struct
	{
		int num = Marshal.SizeOf(typeof(TStruct));
		IntPtr intPtr = IntPtr.Zero;
		TStruct result;
		try
		{
			intPtr = Marshal.AllocHGlobal(num);
			Marshal.Copy(reader.ReadBytes(num), 0, intPtr, num);
			result = (TStruct)((object)Marshal.PtrToStructure(intPtr, typeof(TStruct)));
		}
		finally
		{
			if (intPtr != IntPtr.Zero)
			{
				Marshal.FreeHGlobal(intPtr);
			}
		}
		return result;
	}

	// Token: 0x0600529E RID: 21150 RVA: 0x00171CEC File Offset: 0x001700EC
	public static int SizeOf<TStruct>() where TStruct : struct
	{
		return Marshal.SizeOf(typeof(TStruct));
	}

	// Token: 0x0600529F RID: 21151 RVA: 0x00171D00 File Offset: 0x00170100
	public static void WriteString(BinaryWriter writer, string str)
	{
		char[] array = str.ToCharArray();
		byte[] array2 = new byte[str.Length + 1];
		for (int i = 0; i < str.Length; i++)
		{
			array2[i] = (byte)array[i];
		}
		array2[str.Length] = 0;
		writer.Write(array2);
	}
}
