﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace ItemResponseTypes
{
	// Token: 0x02000C2F RID: 3119
	public class Get : CommonResponse
	{
		// Token: 0x06004017 RID: 16407 RVA: 0x0013DBF4 File Offset: 0x0013BFF4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.items == null) ? string.Empty : this.items.ToString());
		}

		// Token: 0x040045F2 RID: 17906
		public Item[] items;
	}
}
