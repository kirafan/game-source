﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace ItemResponseTypes
{
	// Token: 0x02000C2E RID: 3118
	public class Getall : CommonResponse
	{
		// Token: 0x06004015 RID: 16405 RVA: 0x0013DBB0 File Offset: 0x0013BFB0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.items == null) ? string.Empty : this.items.ToString());
		}

		// Token: 0x040045F1 RID: 17905
		public Item[] items;
	}
}
