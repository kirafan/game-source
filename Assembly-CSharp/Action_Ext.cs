﻿using System;

// Token: 0x020002F6 RID: 758
public static class Action_Ext
{
	// Token: 0x06000ED4 RID: 3796 RVA: 0x0004FCE0 File Offset: 0x0004E0E0
	public static void Call(this Action action)
	{
		if (action != null)
		{
			action();
		}
	}

	// Token: 0x06000ED5 RID: 3797 RVA: 0x0004FCEE File Offset: 0x0004E0EE
	public static void Call<T>(this Action<T> action, T arg)
	{
		if (action != null)
		{
			action(arg);
		}
	}

	// Token: 0x06000ED6 RID: 3798 RVA: 0x0004FCFD File Offset: 0x0004E0FD
	public static void Call<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2)
	{
		if (action != null)
		{
			action(arg1, arg2);
		}
	}

	// Token: 0x06000ED7 RID: 3799 RVA: 0x0004FD0D File Offset: 0x0004E10D
	public static void Call<T1, T2, T3>(this Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
	{
		if (action != null)
		{
			action(arg1, arg2, arg3);
		}
	}
}
