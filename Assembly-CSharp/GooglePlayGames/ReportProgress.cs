﻿using System;

namespace GooglePlayGames
{
	// Token: 0x02000D2B RID: 3371
	// (Invoke) Token: 0x0600432A RID: 17194
	internal delegate void ReportProgress(string id, double progress, Action<bool> callback);
}
