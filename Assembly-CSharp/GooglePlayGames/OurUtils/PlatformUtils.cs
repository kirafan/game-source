﻿using System;
using UnityEngine;

namespace GooglePlayGames.OurUtils
{
	// Token: 0x02000D34 RID: 3380
	public static class PlatformUtils
	{
		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x060043D2 RID: 17362 RVA: 0x001446B4 File Offset: 0x00142AB4
		public static bool Supported
		{
			get
			{
				AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
				AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getPackageManager", new object[0]);
				AndroidJavaObject androidJavaObject2 = null;
				try
				{
					androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getLaunchIntentForPackage", new object[]
					{
						"com.google.android.play.games"
					});
				}
				catch (Exception)
				{
					return false;
				}
				return androidJavaObject2 != null;
			}
		}
	}
}
