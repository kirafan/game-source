﻿using System;

namespace GooglePlayGames.OurUtils
{
	// Token: 0x02000D33 RID: 3379
	public static class Misc
	{
		// Token: 0x060043CE RID: 17358 RVA: 0x001445BC File Offset: 0x001429BC
		public static bool BuffersAreIdentical(byte[] a, byte[] b)
		{
			if (a == b)
			{
				return true;
			}
			if (a == null || b == null)
			{
				return false;
			}
			if (a.Length != b.Length)
			{
				return false;
			}
			for (int i = 0; i < a.Length; i++)
			{
				if (a[i] != b[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060043CF RID: 17359 RVA: 0x00144610 File Offset: 0x00142A10
		public static byte[] GetSubsetBytes(byte[] array, int offset, int length)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (offset < 0 || offset >= array.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (length < 0 || array.Length - offset < length)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			if (offset == 0 && length == array.Length)
			{
				return array;
			}
			byte[] array2 = new byte[length];
			Array.Copy(array, offset, array2, 0, length);
			return array2;
		}

		// Token: 0x060043D0 RID: 17360 RVA: 0x00144689 File Offset: 0x00142A89
		public static T CheckNotNull<T>(T value)
		{
			if (value == null)
			{
				throw new ArgumentNullException();
			}
			return value;
		}

		// Token: 0x060043D1 RID: 17361 RVA: 0x0014469D File Offset: 0x00142A9D
		public static T CheckNotNull<T>(T value, string paramName)
		{
			if (value == null)
			{
				throw new ArgumentNullException(paramName);
			}
			return value;
		}
	}
}
