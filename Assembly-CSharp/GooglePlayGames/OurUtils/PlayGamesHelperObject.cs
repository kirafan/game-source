﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GooglePlayGames.OurUtils
{
	// Token: 0x02000D35 RID: 3381
	public class PlayGamesHelperObject : MonoBehaviour
	{
		// Token: 0x060043D4 RID: 17364 RVA: 0x00144744 File Offset: 0x00142B44
		public static void CreateObject()
		{
			if (PlayGamesHelperObject.instance != null)
			{
				return;
			}
			if (Application.isPlaying)
			{
				GameObject gameObject = new GameObject("PlayGames_QueueRunner");
				UnityEngine.Object.DontDestroyOnLoad(gameObject);
				PlayGamesHelperObject.instance = gameObject.AddComponent<PlayGamesHelperObject>();
			}
			else
			{
				PlayGamesHelperObject.instance = new PlayGamesHelperObject();
				PlayGamesHelperObject.sIsDummy = true;
			}
		}

		// Token: 0x060043D5 RID: 17365 RVA: 0x0014479D File Offset: 0x00142B9D
		public void Awake()
		{
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x060043D6 RID: 17366 RVA: 0x001447AA File Offset: 0x00142BAA
		public void OnDisable()
		{
			if (PlayGamesHelperObject.instance == this)
			{
				PlayGamesHelperObject.instance = null;
			}
		}

		// Token: 0x060043D7 RID: 17367 RVA: 0x001447C4 File Offset: 0x00142BC4
		public static void RunCoroutine(IEnumerator action)
		{
			if (PlayGamesHelperObject.instance != null)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					PlayGamesHelperObject.instance.StartCoroutine(action);
				});
			}
		}

		// Token: 0x060043D8 RID: 17368 RVA: 0x00144800 File Offset: 0x00142C00
		public static void RunOnGameThread(Action action)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			if (PlayGamesHelperObject.sIsDummy)
			{
				return;
			}
			object obj = PlayGamesHelperObject.sQueue;
			lock (obj)
			{
				PlayGamesHelperObject.sQueue.Add(action);
				PlayGamesHelperObject.sQueueEmpty = false;
			}
		}

		// Token: 0x060043D9 RID: 17369 RVA: 0x00144864 File Offset: 0x00142C64
		public void Update()
		{
			if (PlayGamesHelperObject.sIsDummy)
			{
				return;
			}
			this.localQueue.Clear();
			object obj = PlayGamesHelperObject.sQueue;
			lock (obj)
			{
				if (PlayGamesHelperObject.sQueueEmpty)
				{
					return;
				}
				this.localQueue.AddRange(PlayGamesHelperObject.sQueue);
				PlayGamesHelperObject.sQueue.Clear();
				PlayGamesHelperObject.sQueueEmpty = true;
			}
			for (int i = 0; i < this.localQueue.Count; i++)
			{
				this.localQueue[i]();
			}
		}

		// Token: 0x060043DA RID: 17370 RVA: 0x00144910 File Offset: 0x00142D10
		public void OnApplicationFocus(bool focused)
		{
			foreach (Action<bool> action in PlayGamesHelperObject.sFocusCallbackList)
			{
				try
				{
					action(focused);
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnApplicationFocus:" + ex.Message + "\n" + ex.StackTrace);
				}
			}
		}

		// Token: 0x060043DB RID: 17371 RVA: 0x001449A4 File Offset: 0x00142DA4
		public void OnApplicationPause(bool paused)
		{
			foreach (Action<bool> action in PlayGamesHelperObject.sPauseCallbackList)
			{
				try
				{
					action(paused);
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnApplicationPause:" + ex.Message + "\n" + ex.StackTrace);
				}
			}
		}

		// Token: 0x060043DC RID: 17372 RVA: 0x00144A38 File Offset: 0x00142E38
		public static void AddFocusCallback(Action<bool> callback)
		{
			if (!PlayGamesHelperObject.sFocusCallbackList.Contains(callback))
			{
				PlayGamesHelperObject.sFocusCallbackList.Add(callback);
			}
		}

		// Token: 0x060043DD RID: 17373 RVA: 0x00144A55 File Offset: 0x00142E55
		public static bool RemoveFocusCallback(Action<bool> callback)
		{
			return PlayGamesHelperObject.sFocusCallbackList.Remove(callback);
		}

		// Token: 0x060043DE RID: 17374 RVA: 0x00144A62 File Offset: 0x00142E62
		public static void AddPauseCallback(Action<bool> callback)
		{
			if (!PlayGamesHelperObject.sPauseCallbackList.Contains(callback))
			{
				PlayGamesHelperObject.sPauseCallbackList.Add(callback);
			}
		}

		// Token: 0x060043DF RID: 17375 RVA: 0x00144A7F File Offset: 0x00142E7F
		public static bool RemovePauseCallback(Action<bool> callback)
		{
			return PlayGamesHelperObject.sPauseCallbackList.Remove(callback);
		}

		// Token: 0x04004B59 RID: 19289
		private static PlayGamesHelperObject instance = null;

		// Token: 0x04004B5A RID: 19290
		private static bool sIsDummy = false;

		// Token: 0x04004B5B RID: 19291
		private static List<Action> sQueue = new List<Action>();

		// Token: 0x04004B5C RID: 19292
		private List<Action> localQueue = new List<Action>();

		// Token: 0x04004B5D RID: 19293
		private static volatile bool sQueueEmpty = true;

		// Token: 0x04004B5E RID: 19294
		private static List<Action<bool>> sPauseCallbackList = new List<Action<bool>>();

		// Token: 0x04004B5F RID: 19295
		private static List<Action<bool>> sFocusCallbackList = new List<Action<bool>>();
	}
}
