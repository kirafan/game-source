﻿using System;
using UnityEngine;

namespace GooglePlayGames.OurUtils
{
	// Token: 0x02000D32 RID: 3378
	public class Logger
	{
		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x060043C4 RID: 17348 RVA: 0x00144419 File Offset: 0x00142819
		// (set) Token: 0x060043C5 RID: 17349 RVA: 0x00144420 File Offset: 0x00142820
		public static bool DebugLogEnabled
		{
			get
			{
				return Logger.debugLogEnabled;
			}
			set
			{
				Logger.debugLogEnabled = value;
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x060043C6 RID: 17350 RVA: 0x00144428 File Offset: 0x00142828
		// (set) Token: 0x060043C7 RID: 17351 RVA: 0x0014442F File Offset: 0x0014282F
		public static bool WarningLogEnabled
		{
			get
			{
				return Logger.warningLogEnabled;
			}
			set
			{
				Logger.warningLogEnabled = value;
			}
		}

		// Token: 0x060043C8 RID: 17352 RVA: 0x00144438 File Offset: 0x00142838
		public static void d(string msg)
		{
			if (Logger.debugLogEnabled)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					Debug.Log(Logger.ToLogMessage(string.Empty, "DEBUG", msg));
				});
			}
		}

		// Token: 0x060043C9 RID: 17353 RVA: 0x00144470 File Offset: 0x00142870
		public static void w(string msg)
		{
			if (Logger.warningLogEnabled)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					Debug.LogWarning(Logger.ToLogMessage("!!!", "WARNING", msg));
				});
			}
		}

		// Token: 0x060043CA RID: 17354 RVA: 0x001444A8 File Offset: 0x001428A8
		public static void e(string msg)
		{
			if (Logger.warningLogEnabled)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					Debug.LogWarning(Logger.ToLogMessage("***", "ERROR", msg));
				});
			}
		}

		// Token: 0x060043CB RID: 17355 RVA: 0x001444DD File Offset: 0x001428DD
		public static string describe(byte[] b)
		{
			return (b != null) ? ("byte[" + b.Length + "]") : "(null)";
		}

		// Token: 0x060043CC RID: 17356 RVA: 0x00144508 File Offset: 0x00142908
		private static string ToLogMessage(string prefix, string logType, string msg)
		{
			return string.Format("{0} [Play Games Plugin DLL] {1} {2}: {3}", new object[]
			{
				prefix,
				DateTime.Now.ToString("MM/dd/yy H:mm:ss zzz"),
				logType,
				msg
			});
		}

		// Token: 0x04004B57 RID: 19287
		private static bool debugLogEnabled;

		// Token: 0x04004B58 RID: 19288
		private static bool warningLogEnabled = true;
	}
}
