﻿using System;

namespace GooglePlayGames
{
	// Token: 0x02000D2A RID: 3370
	public static class GameInfo
	{
		// Token: 0x06004324 RID: 17188 RVA: 0x00142997 File Offset: 0x00140D97
		public static bool ApplicationIdInitialized()
		{
			return !string.IsNullOrEmpty("906022166608") && !"906022166608".Equals(GameInfo.ToEscapedToken("APP_ID"));
		}

		// Token: 0x06004325 RID: 17189 RVA: 0x001429C2 File Offset: 0x00140DC2
		public static bool IosClientIdInitialized()
		{
			return !string.IsNullOrEmpty(string.Empty) && !string.Empty.Equals(GameInfo.ToEscapedToken("IOS_CLIENTID"));
		}

		// Token: 0x06004326 RID: 17190 RVA: 0x001429ED File Offset: 0x00140DED
		public static bool WebClientIdInitialized()
		{
			return !string.IsNullOrEmpty(string.Empty) && !string.Empty.Equals(GameInfo.ToEscapedToken("WEB_CLIENTID"));
		}

		// Token: 0x06004327 RID: 17191 RVA: 0x00142A18 File Offset: 0x00140E18
		public static bool NearbyConnectionsInitialized()
		{
			return !string.IsNullOrEmpty(string.Empty) && !string.Empty.Equals(GameInfo.ToEscapedToken("NEARBY_SERVICE_ID"));
		}

		// Token: 0x06004328 RID: 17192 RVA: 0x00142A43 File Offset: 0x00140E43
		private static string ToEscapedToken(string token)
		{
			return string.Format("__{0}__", token);
		}

		// Token: 0x04004B1F RID: 19231
		private const string UnescapedApplicationId = "APP_ID";

		// Token: 0x04004B20 RID: 19232
		private const string UnescapedIosClientId = "IOS_CLIENTID";

		// Token: 0x04004B21 RID: 19233
		private const string UnescapedWebClientId = "WEB_CLIENTID";

		// Token: 0x04004B22 RID: 19234
		private const string UnescapedNearbyServiceId = "NEARBY_SERVICE_ID";

		// Token: 0x04004B23 RID: 19235
		public const string ApplicationId = "906022166608";

		// Token: 0x04004B24 RID: 19236
		public const string IosClientId = "";

		// Token: 0x04004B25 RID: 19237
		public const string WebClientId = "";

		// Token: 0x04004B26 RID: 19238
		public const string NearbyConnectionServiceId = "";
	}
}
