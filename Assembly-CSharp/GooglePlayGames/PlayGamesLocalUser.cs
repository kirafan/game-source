﻿using System;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D2E RID: 3374
	public class PlayGamesLocalUser : PlayGamesUserProfile, ILocalUser, IUserProfile
	{
		// Token: 0x06004359 RID: 17241 RVA: 0x00142F8E File Offset: 0x0014138E
		internal PlayGamesLocalUser(PlayGamesPlatform plaf) : base("localUser", string.Empty, string.Empty)
		{
			this.mPlatform = plaf;
			this.emailAddress = null;
			this.mStats = null;
		}

		// Token: 0x0600435A RID: 17242 RVA: 0x00142FBA File Offset: 0x001413BA
		public void Authenticate(Action<bool> callback)
		{
			this.mPlatform.Authenticate(callback);
		}

		// Token: 0x0600435B RID: 17243 RVA: 0x00142FC8 File Offset: 0x001413C8
		public void Authenticate(Action<bool, string> callback)
		{
			this.mPlatform.Authenticate(callback);
		}

		// Token: 0x0600435C RID: 17244 RVA: 0x00142FD6 File Offset: 0x001413D6
		public void Authenticate(Action<bool> callback, bool silent)
		{
			this.mPlatform.Authenticate(callback, silent);
		}

		// Token: 0x0600435D RID: 17245 RVA: 0x00142FE5 File Offset: 0x001413E5
		public void Authenticate(Action<bool, string> callback, bool silent)
		{
			this.mPlatform.Authenticate(callback, silent);
		}

		// Token: 0x0600435E RID: 17246 RVA: 0x00142FF4 File Offset: 0x001413F4
		public void LoadFriends(Action<bool> callback)
		{
			this.mPlatform.LoadFriends(this, callback);
		}

		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x0600435F RID: 17247 RVA: 0x00143003 File Offset: 0x00141403
		public IUserProfile[] friends
		{
			get
			{
				return this.mPlatform.GetFriends();
			}
		}

		// Token: 0x06004360 RID: 17248 RVA: 0x00143010 File Offset: 0x00141410
		public string GetIdToken()
		{
			return this.mPlatform.GetIdToken();
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x06004361 RID: 17249 RVA: 0x0014301D File Offset: 0x0014141D
		public bool authenticated
		{
			get
			{
				return this.mPlatform.IsAuthenticated();
			}
		}

		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x06004362 RID: 17250 RVA: 0x0014302A File Offset: 0x0014142A
		public bool underage
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x06004363 RID: 17251 RVA: 0x00143030 File Offset: 0x00141430
		public new string userName
		{
			get
			{
				string text = string.Empty;
				if (this.authenticated)
				{
					text = this.mPlatform.GetUserDisplayName();
					if (!base.userName.Equals(text))
					{
						base.ResetIdentity(text, this.mPlatform.GetUserId(), this.mPlatform.GetUserImageUrl());
					}
				}
				return text;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x06004364 RID: 17252 RVA: 0x0014308C File Offset: 0x0014148C
		public new string id
		{
			get
			{
				string text = string.Empty;
				if (this.authenticated)
				{
					text = this.mPlatform.GetUserId();
					if (!base.id.Equals(text))
					{
						base.ResetIdentity(this.mPlatform.GetUserDisplayName(), text, this.mPlatform.GetUserImageUrl());
					}
				}
				return text;
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x06004365 RID: 17253 RVA: 0x001430E5 File Offset: 0x001414E5
		public new bool isFriend
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x06004366 RID: 17254 RVA: 0x001430E8 File Offset: 0x001414E8
		public new UserState state
		{
			get
			{
				return UserState.Online;
			}
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06004367 RID: 17255 RVA: 0x001430EC File Offset: 0x001414EC
		public new string AvatarURL
		{
			get
			{
				string text = string.Empty;
				if (this.authenticated)
				{
					text = this.mPlatform.GetUserImageUrl();
					if (!base.id.Equals(text))
					{
						base.ResetIdentity(this.mPlatform.GetUserDisplayName(), this.mPlatform.GetUserId(), text);
					}
				}
				return text;
			}
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x06004368 RID: 17256 RVA: 0x00143148 File Offset: 0x00141548
		public string Email
		{
			get
			{
				if (this.authenticated && string.IsNullOrEmpty(this.emailAddress))
				{
					this.emailAddress = this.mPlatform.GetUserEmail();
					this.emailAddress = (this.emailAddress ?? string.Empty);
				}
				return (!this.authenticated) ? string.Empty : this.emailAddress;
			}
		}

		// Token: 0x06004369 RID: 17257 RVA: 0x001431B4 File Offset: 0x001415B4
		public void GetStats(Action<CommonStatusCodes, PlayerStats> callback)
		{
			if (this.mStats == null || !this.mStats.Valid)
			{
				this.mPlatform.GetPlayerStats(delegate(CommonStatusCodes rc, PlayerStats stats)
				{
					this.mStats = stats;
					callback(rc, stats);
				});
			}
			else
			{
				callback(CommonStatusCodes.Success, this.mStats);
			}
		}

		// Token: 0x04004B41 RID: 19265
		internal PlayGamesPlatform mPlatform;

		// Token: 0x04004B42 RID: 19266
		private string emailAddress;

		// Token: 0x04004B43 RID: 19267
		private PlayerStats mStats;
	}
}
