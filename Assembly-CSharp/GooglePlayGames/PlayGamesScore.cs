﻿using System;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D30 RID: 3376
	public class PlayGamesScore : IScore
	{
		// Token: 0x060043AC RID: 17324 RVA: 0x0014432C File Offset: 0x0014272C
		internal PlayGamesScore(DateTime date, string leaderboardId, ulong rank, string playerId, ulong value, string metadata)
		{
			this.mDate = date;
			this.mLbId = this.leaderboardID;
			this.mRank = rank;
			this.mPlayerId = playerId;
			this.mValue = (long)value;
			this.mMetadata = metadata;
		}

		// Token: 0x060043AD RID: 17325 RVA: 0x0014439C File Offset: 0x0014279C
		public void ReportScore(Action<bool> callback)
		{
			PlayGamesPlatform.Instance.ReportScore(this.mValue, this.mLbId, this.mMetadata, callback);
		}

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x060043AE RID: 17326 RVA: 0x001443BB File Offset: 0x001427BB
		// (set) Token: 0x060043AF RID: 17327 RVA: 0x001443C3 File Offset: 0x001427C3
		public string leaderboardID
		{
			get
			{
				return this.mLbId;
			}
			set
			{
				this.mLbId = value;
			}
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x060043B0 RID: 17328 RVA: 0x001443CC File Offset: 0x001427CC
		// (set) Token: 0x060043B1 RID: 17329 RVA: 0x001443D4 File Offset: 0x001427D4
		public long value
		{
			get
			{
				return this.mValue;
			}
			set
			{
				this.mValue = value;
			}
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x060043B2 RID: 17330 RVA: 0x001443DD File Offset: 0x001427DD
		public DateTime date
		{
			get
			{
				return this.mDate;
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x060043B3 RID: 17331 RVA: 0x001443E5 File Offset: 0x001427E5
		public string formattedValue
		{
			get
			{
				return this.mValue.ToString();
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x060043B4 RID: 17332 RVA: 0x001443F8 File Offset: 0x001427F8
		public string userID
		{
			get
			{
				return this.mPlayerId;
			}
		}

		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x060043B5 RID: 17333 RVA: 0x00144400 File Offset: 0x00142800
		public int rank
		{
			get
			{
				return (int)this.mRank;
			}
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x060043B6 RID: 17334 RVA: 0x00144409 File Offset: 0x00142809
		public string metaData
		{
			get
			{
				return this.mMetadata;
			}
		}

		// Token: 0x04004B4C RID: 19276
		private string mLbId;

		// Token: 0x04004B4D RID: 19277
		private long mValue;

		// Token: 0x04004B4E RID: 19278
		private ulong mRank;

		// Token: 0x04004B4F RID: 19279
		private string mPlayerId = string.Empty;

		// Token: 0x04004B50 RID: 19280
		private string mMetadata = string.Empty;

		// Token: 0x04004B51 RID: 19281
		private DateTime mDate = new DateTime(1970, 1, 1, 0, 0, 0);
	}
}
