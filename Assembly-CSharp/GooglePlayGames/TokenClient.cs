﻿using System;

namespace GooglePlayGames
{
	// Token: 0x02000E6F RID: 3695
	internal interface TokenClient
	{
		// Token: 0x06004C6C RID: 19564
		string GetEmail();

		// Token: 0x06004C6D RID: 19565
		string GetAuthCode();

		// Token: 0x06004C6E RID: 19566
		string GetIdToken();

		// Token: 0x06004C6F RID: 19567
		void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback);

		// Token: 0x06004C70 RID: 19568
		void Signout();

		// Token: 0x06004C71 RID: 19569
		void SetRequestAuthCode(bool flag, bool forceRefresh);

		// Token: 0x06004C72 RID: 19570
		void SetRequestEmail(bool flag);

		// Token: 0x06004C73 RID: 19571
		void SetRequestIdToken(bool flag);

		// Token: 0x06004C74 RID: 19572
		void SetWebClientId(string webClientId);

		// Token: 0x06004C75 RID: 19573
		void SetAccountName(string accountName);

		// Token: 0x06004C76 RID: 19574
		void AddOauthScopes(string[] scopes);

		// Token: 0x06004C77 RID: 19575
		void SetHidePopups(bool flag);

		// Token: 0x06004C78 RID: 19576
		bool NeedsToRun();

		// Token: 0x06004C79 RID: 19577
		void FetchTokens(Action<int> callback);
	}
}
