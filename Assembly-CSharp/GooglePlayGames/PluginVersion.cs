﻿using System;

namespace GooglePlayGames
{
	// Token: 0x02000E70 RID: 3696
	public class PluginVersion
	{
		// Token: 0x04004D67 RID: 19815
		public const string VersionKeyCPP = "00911";

		// Token: 0x04004D68 RID: 19816
		public const string VersionKeyU5 = "00915";

		// Token: 0x04004D69 RID: 19817
		public const string VersionKey27Patch = "00927a";

		// Token: 0x04004D6A RID: 19818
		public const string VersionKeyJarResolver = "00928";

		// Token: 0x04004D6B RID: 19819
		public const string VersionKeyNativeCRM = "00930";

		// Token: 0x04004D6C RID: 19820
		public const string VersionKeyJNIStats = "00934";

		// Token: 0x04004D6D RID: 19821
		public const string VersionKeyJarResolverDLL = "00935";

		// Token: 0x04004D6E RID: 19822
		public const int VersionInt = 2369;

		// Token: 0x04004D6F RID: 19823
		public const string VersionString = "0.9.41";

		// Token: 0x04004D70 RID: 19824
		public const string VersionKey = "00941";

		// Token: 0x04004D71 RID: 19825
		public const int MinGmsCoreVersionCode = 10200000;

		// Token: 0x04004D72 RID: 19826
		public const string PlayServicesVersionConstraint = "10+";
	}
}
