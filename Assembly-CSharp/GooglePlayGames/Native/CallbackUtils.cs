﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000D4D RID: 3405
	internal static class CallbackUtils
	{
		// Token: 0x060044B9 RID: 17593 RVA: 0x00146B54 File Offset: 0x00144F54
		internal static Action<T> ToOnGameThread<T>(Action<T> toConvert)
		{
			if (toConvert == null)
			{
				return delegate(T A_0)
				{
				};
			}
			return delegate(T val)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toConvert(val);
				});
			};
		}

		// Token: 0x060044BA RID: 17594 RVA: 0x00146B94 File Offset: 0x00144F94
		internal static Action<T1, T2> ToOnGameThread<T1, T2>(Action<T1, T2> toConvert)
		{
			if (toConvert == null)
			{
				return delegate(T1 A_0, T2 A_1)
				{
				};
			}
			return delegate(T1 val1, T2 val2)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toConvert(val1, val2);
				});
			};
		}

		// Token: 0x060044BB RID: 17595 RVA: 0x00146BD4 File Offset: 0x00144FD4
		internal static Action<T1, T2, T3> ToOnGameThread<T1, T2, T3>(Action<T1, T2, T3> toConvert)
		{
			if (toConvert == null)
			{
				return delegate(T1 A_0, T2 A_1, T3 A_2)
				{
				};
			}
			return delegate(T1 val1, T2 val2, T3 val3)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toConvert(val1, val2, val3);
				});
			};
		}
	}
}
