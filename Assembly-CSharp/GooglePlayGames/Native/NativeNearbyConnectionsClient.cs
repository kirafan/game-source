﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF5 RID: 3573
	internal class NativeNearbyConnectionsClient : INearbyConnectionClient
	{
		// Token: 0x06004830 RID: 18480 RVA: 0x001490B1 File Offset: 0x001474B1
		internal NativeNearbyConnectionsClient(NearbyConnectionsManager manager)
		{
			this.mManager = Misc.CheckNotNull<NearbyConnectionsManager>(manager);
		}

		// Token: 0x06004831 RID: 18481 RVA: 0x001490C5 File Offset: 0x001474C5
		public int MaxUnreliableMessagePayloadLength()
		{
			return 1168;
		}

		// Token: 0x06004832 RID: 18482 RVA: 0x001490CC File Offset: 0x001474CC
		public int MaxReliableMessagePayloadLength()
		{
			return 4096;
		}

		// Token: 0x06004833 RID: 18483 RVA: 0x001490D3 File Offset: 0x001474D3
		public void SendReliable(List<string> recipientEndpointIds, byte[] payload)
		{
			this.InternalSend(recipientEndpointIds, payload, true);
		}

		// Token: 0x06004834 RID: 18484 RVA: 0x001490DE File Offset: 0x001474DE
		public void SendUnreliable(List<string> recipientEndpointIds, byte[] payload)
		{
			this.InternalSend(recipientEndpointIds, payload, false);
		}

		// Token: 0x06004835 RID: 18485 RVA: 0x001490EC File Offset: 0x001474EC
		private void InternalSend(List<string> recipientEndpointIds, byte[] payload, bool isReliable)
		{
			if (recipientEndpointIds == null)
			{
				throw new ArgumentNullException("recipientEndpointIds");
			}
			if (payload == null)
			{
				throw new ArgumentNullException("payload");
			}
			if (recipientEndpointIds.Contains(null))
			{
				throw new InvalidOperationException("Cannot send a message to a null recipient");
			}
			if (recipientEndpointIds.Count == 0)
			{
				Logger.w("Attempted to send a reliable message with no recipients");
				return;
			}
			if (isReliable)
			{
				if (payload.Length > this.MaxReliableMessagePayloadLength())
				{
					throw new InvalidOperationException("cannot send more than " + this.MaxReliableMessagePayloadLength() + " bytes");
				}
			}
			else if (payload.Length > this.MaxUnreliableMessagePayloadLength())
			{
				throw new InvalidOperationException("cannot send more than " + this.MaxUnreliableMessagePayloadLength() + " bytes");
			}
			foreach (string remoteEndpointId in recipientEndpointIds)
			{
				if (isReliable)
				{
					this.mManager.SendReliable(remoteEndpointId, payload);
				}
				else
				{
					this.mManager.SendUnreliable(remoteEndpointId, payload);
				}
			}
		}

		// Token: 0x06004836 RID: 18486 RVA: 0x00149218 File Offset: 0x00147618
		public void StartAdvertising(string name, List<string> appIdentifiers, TimeSpan? advertisingDuration, Action<AdvertisingResult> resultCallback, Action<ConnectionRequest> requestCallback)
		{
			Misc.CheckNotNull<List<string>>(appIdentifiers, "appIdentifiers");
			Misc.CheckNotNull<Action<AdvertisingResult>>(resultCallback, "resultCallback");
			Misc.CheckNotNull<Action<ConnectionRequest>>(requestCallback, "connectionRequestCallback");
			if (advertisingDuration != null && advertisingDuration.Value.Ticks < 0L)
			{
				throw new InvalidOperationException("advertisingDuration must be positive");
			}
			resultCallback = Callbacks.AsOnGameThreadCallback<AdvertisingResult>(resultCallback);
			requestCallback = Callbacks.AsOnGameThreadCallback<ConnectionRequest>(requestCallback);
			NearbyConnectionsManager nearbyConnectionsManager = this.mManager;
			if (NativeNearbyConnectionsClient.<>f__mg$cache0 == null)
			{
				NativeNearbyConnectionsClient.<>f__mg$cache0 = new Func<string, NativeAppIdentifier>(NativeAppIdentifier.FromString);
			}
			nearbyConnectionsManager.StartAdvertising(name, appIdentifiers.Select(NativeNearbyConnectionsClient.<>f__mg$cache0).ToList<NativeAppIdentifier>(), NativeNearbyConnectionsClient.ToTimeoutMillis(advertisingDuration), delegate(long localClientId, NativeStartAdvertisingResult result)
			{
				resultCallback(result.AsResult());
			}, delegate(long localClientId, NativeConnectionRequest request)
			{
				requestCallback(request.AsRequest());
			});
		}

		// Token: 0x06004837 RID: 18487 RVA: 0x0014930A File Offset: 0x0014770A
		private static long ToTimeoutMillis(TimeSpan? span)
		{
			return (span == null) ? 0L : PInvokeUtilities.ToMilliseconds(span.Value);
		}

		// Token: 0x06004838 RID: 18488 RVA: 0x0014932B File Offset: 0x0014772B
		public void StopAdvertising()
		{
			this.mManager.StopAdvertising();
		}

		// Token: 0x06004839 RID: 18489 RVA: 0x00149338 File Offset: 0x00147738
		public void SendConnectionRequest(string name, string remoteEndpointId, byte[] payload, Action<ConnectionResponse> responseCallback, IMessageListener listener)
		{
			Misc.CheckNotNull<string>(remoteEndpointId, "remoteEndpointId");
			Misc.CheckNotNull<byte[]>(payload, "payload");
			Misc.CheckNotNull<Action<ConnectionResponse>>(responseCallback, "responseCallback");
			Misc.CheckNotNull<IMessageListener>(listener, "listener");
			responseCallback = Callbacks.AsOnGameThreadCallback<ConnectionResponse>(responseCallback);
			using (NativeMessageListenerHelper nativeMessageListenerHelper = NativeNearbyConnectionsClient.ToMessageListener(listener))
			{
				this.mManager.SendConnectionRequest(name, remoteEndpointId, payload, delegate(long localClientId, NativeConnectionResponse response)
				{
					responseCallback(response.AsResponse(localClientId));
				}, nativeMessageListenerHelper);
			}
		}

		// Token: 0x0600483A RID: 18490 RVA: 0x001493E0 File Offset: 0x001477E0
		private static NativeMessageListenerHelper ToMessageListener(IMessageListener listener)
		{
			listener = new NativeNearbyConnectionsClient.OnGameThreadMessageListener(listener);
			NativeMessageListenerHelper nativeMessageListenerHelper = new NativeMessageListenerHelper();
			nativeMessageListenerHelper.SetOnMessageReceivedCallback(delegate(long localClientId, string endpointId, byte[] data, bool isReliable)
			{
				listener.OnMessageReceived(endpointId, data, isReliable);
			});
			nativeMessageListenerHelper.SetOnDisconnectedCallback(delegate(long localClientId, string endpointId)
			{
				listener.OnRemoteEndpointDisconnected(endpointId);
			});
			return nativeMessageListenerHelper;
		}

		// Token: 0x0600483B RID: 18491 RVA: 0x00149438 File Offset: 0x00147838
		public void AcceptConnectionRequest(string remoteEndpointId, byte[] payload, IMessageListener listener)
		{
			Misc.CheckNotNull<string>(remoteEndpointId, "remoteEndpointId");
			Misc.CheckNotNull<byte[]>(payload, "payload");
			Misc.CheckNotNull<IMessageListener>(listener, "listener");
			Logger.d("Calling AcceptConncectionRequest");
			this.mManager.AcceptConnectionRequest(remoteEndpointId, payload, NativeNearbyConnectionsClient.ToMessageListener(listener));
			Logger.d("Called!");
		}

		// Token: 0x0600483C RID: 18492 RVA: 0x00149490 File Offset: 0x00147890
		public void StartDiscovery(string serviceId, TimeSpan? advertisingTimeout, IDiscoveryListener listener)
		{
			Misc.CheckNotNull<string>(serviceId, "serviceId");
			Misc.CheckNotNull<IDiscoveryListener>(listener, "listener");
			using (NativeEndpointDiscoveryListenerHelper nativeEndpointDiscoveryListenerHelper = NativeNearbyConnectionsClient.ToDiscoveryListener(listener))
			{
				this.mManager.StartDiscovery(serviceId, NativeNearbyConnectionsClient.ToTimeoutMillis(advertisingTimeout), nativeEndpointDiscoveryListenerHelper);
			}
		}

		// Token: 0x0600483D RID: 18493 RVA: 0x001494F4 File Offset: 0x001478F4
		private static NativeEndpointDiscoveryListenerHelper ToDiscoveryListener(IDiscoveryListener listener)
		{
			listener = new NativeNearbyConnectionsClient.OnGameThreadDiscoveryListener(listener);
			NativeEndpointDiscoveryListenerHelper nativeEndpointDiscoveryListenerHelper = new NativeEndpointDiscoveryListenerHelper();
			nativeEndpointDiscoveryListenerHelper.SetOnEndpointFound(delegate(long localClientId, NativeEndpointDetails endpoint)
			{
				listener.OnEndpointFound(endpoint.ToDetails());
			});
			nativeEndpointDiscoveryListenerHelper.SetOnEndpointLostCallback(delegate(long localClientId, string lostEndpointId)
			{
				listener.OnEndpointLost(lostEndpointId);
			});
			return nativeEndpointDiscoveryListenerHelper;
		}

		// Token: 0x0600483E RID: 18494 RVA: 0x0014954A File Offset: 0x0014794A
		public void StopDiscovery(string serviceId)
		{
			Misc.CheckNotNull<string>(serviceId, "serviceId");
			this.mManager.StopDiscovery(serviceId);
		}

		// Token: 0x0600483F RID: 18495 RVA: 0x00149564 File Offset: 0x00147964
		public void RejectConnectionRequest(string requestingEndpointId)
		{
			Misc.CheckNotNull<string>(requestingEndpointId, "requestingEndpointId");
			this.mManager.RejectConnectionRequest(requestingEndpointId);
		}

		// Token: 0x06004840 RID: 18496 RVA: 0x0014957E File Offset: 0x0014797E
		public void DisconnectFromEndpoint(string remoteEndpointId)
		{
			this.mManager.DisconnectFromEndpoint(remoteEndpointId);
		}

		// Token: 0x06004841 RID: 18497 RVA: 0x0014958C File Offset: 0x0014798C
		public void StopAllConnections()
		{
			this.mManager.StopAllConnections();
		}

		// Token: 0x06004842 RID: 18498 RVA: 0x00149599 File Offset: 0x00147999
		public string LocalEndpointId()
		{
			return this.mManager.LocalEndpointId();
		}

		// Token: 0x06004843 RID: 18499 RVA: 0x001495A6 File Offset: 0x001479A6
		public string LocalDeviceId()
		{
			return this.mManager.LocalDeviceId();
		}

		// Token: 0x06004844 RID: 18500 RVA: 0x001495B3 File Offset: 0x001479B3
		public string GetAppBundleId()
		{
			return this.mManager.AppBundleId;
		}

		// Token: 0x06004845 RID: 18501 RVA: 0x001495C0 File Offset: 0x001479C0
		public string GetServiceId()
		{
			return NearbyConnectionsManager.ServiceId;
		}

		// Token: 0x04004C9C RID: 19612
		private readonly NearbyConnectionsManager mManager;

		// Token: 0x04004C9D RID: 19613
		[CompilerGenerated]
		private static Func<string, NativeAppIdentifier> <>f__mg$cache0;

		// Token: 0x02000DF6 RID: 3574
		protected class OnGameThreadMessageListener : IMessageListener
		{
			// Token: 0x06004846 RID: 18502 RVA: 0x001495C7 File Offset: 0x001479C7
			public OnGameThreadMessageListener(IMessageListener listener)
			{
				this.mListener = Misc.CheckNotNull<IMessageListener>(listener);
			}

			// Token: 0x06004847 RID: 18503 RVA: 0x001495DC File Offset: 0x001479DC
			public void OnMessageReceived(string remoteEndpointId, byte[] data, bool isReliableMessage)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnMessageReceived(remoteEndpointId, data, isReliableMessage);
				});
			}

			// Token: 0x06004848 RID: 18504 RVA: 0x0014961C File Offset: 0x00147A1C
			public void OnRemoteEndpointDisconnected(string remoteEndpointId)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnRemoteEndpointDisconnected(remoteEndpointId);
				});
			}

			// Token: 0x04004C9E RID: 19614
			private readonly IMessageListener mListener;
		}

		// Token: 0x02000DF7 RID: 3575
		protected class OnGameThreadDiscoveryListener : IDiscoveryListener
		{
			// Token: 0x06004849 RID: 18505 RVA: 0x0014969A File Offset: 0x00147A9A
			public OnGameThreadDiscoveryListener(IDiscoveryListener listener)
			{
				this.mListener = Misc.CheckNotNull<IDiscoveryListener>(listener);
			}

			// Token: 0x0600484A RID: 18506 RVA: 0x001496B0 File Offset: 0x00147AB0
			public void OnEndpointFound(EndpointDetails discoveredEndpoint)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnEndpointFound(discoveredEndpoint);
				});
			}

			// Token: 0x0600484B RID: 18507 RVA: 0x001496E4 File Offset: 0x00147AE4
			public void OnEndpointLost(string lostEndpointId)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnEndpointLost(lostEndpointId);
				});
			}

			// Token: 0x04004C9F RID: 19615
			private readonly IDiscoveryListener mListener;
		}
	}
}
