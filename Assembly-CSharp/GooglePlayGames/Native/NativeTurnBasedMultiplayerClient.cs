﻿using System;
using System.Collections;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000E09 RID: 3593
	public class NativeTurnBasedMultiplayerClient : ITurnBasedMultiplayerClient
	{
		// Token: 0x060048FE RID: 18686 RVA: 0x0014CEFF File Offset: 0x0014B2FF
		internal NativeTurnBasedMultiplayerClient(NativeClient nativeClient, TurnBasedManager manager)
		{
			this.mTurnBasedManager = manager;
			this.mNativeClient = nativeClient;
		}

		// Token: 0x060048FF RID: 18687 RVA: 0x0014CF15 File Offset: 0x0014B315
		public void CreateQuickMatch(uint minOpponents, uint maxOpponents, uint variant, Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			this.CreateQuickMatch(minOpponents, maxOpponents, variant, 0UL, callback);
		}

		// Token: 0x06004900 RID: 18688 RVA: 0x0014CF24 File Offset: 0x0014B324
		public void CreateQuickMatch(uint minOpponents, uint maxOpponents, uint variant, ulong exclusiveBitmask, Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(callback);
			using (GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder turnBasedMatchConfigBuilder = GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder.Create())
			{
				turnBasedMatchConfigBuilder.SetVariant(variant).SetMinimumAutomatchingPlayers(minOpponents).SetMaximumAutomatchingPlayers(maxOpponents).SetExclusiveBitMask(exclusiveBitmask);
				using (GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig turnBasedMatchConfig = turnBasedMatchConfigBuilder.Build())
				{
					this.mTurnBasedManager.CreateMatch(turnBasedMatchConfig, this.BridgeMatchToUserCallback(delegate(UIStatus status, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match)
					{
						callback(status == UIStatus.Valid, match);
					}));
				}
			}
		}

		// Token: 0x06004901 RID: 18689 RVA: 0x0014CFD8 File Offset: 0x0014B3D8
		public void CreateWithInvitationScreen(uint minOpponents, uint maxOpponents, uint variant, Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			this.CreateWithInvitationScreen(minOpponents, maxOpponents, variant, delegate(UIStatus status, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match)
			{
				callback(status == UIStatus.Valid, match);
			});
		}

		// Token: 0x06004902 RID: 18690 RVA: 0x0014D008 File Offset: 0x0014B408
		public void CreateWithInvitationScreen(uint minOpponents, uint maxOpponents, uint variant, Action<UIStatus, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<UIStatus, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(callback);
			this.mTurnBasedManager.ShowPlayerSelectUI(minOpponents, maxOpponents, true, delegate(PlayerSelectUIResponse result)
			{
				if (result.Status() != CommonErrorStatus.UIStatus.VALID)
				{
					callback((UIStatus)result.Status(), null);
					return;
				}
				using (GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder turnBasedMatchConfigBuilder = GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder.Create())
				{
					turnBasedMatchConfigBuilder.PopulateFromUIResponse(result).SetVariant(variant);
					using (GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig turnBasedMatchConfig = turnBasedMatchConfigBuilder.Build())
					{
						this.mTurnBasedManager.CreateMatch(turnBasedMatchConfig, this.BridgeMatchToUserCallback(callback));
					}
				}
			});
		}

		// Token: 0x06004903 RID: 18691 RVA: 0x0014D05C File Offset: 0x0014B45C
		public void GetAllInvitations(Action<Invitation[]> callback)
		{
			this.mTurnBasedManager.GetAllTurnbasedMatches(delegate(TurnBasedManager.TurnBasedMatchesResponse allMatches)
			{
				Invitation[] array = new Invitation[allMatches.InvitationCount()];
				int num = 0;
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerInvitation multiplayerInvitation in allMatches.Invitations())
				{
					array[num++] = multiplayerInvitation.AsInvitation();
				}
				callback(array);
			});
		}

		// Token: 0x06004904 RID: 18692 RVA: 0x0014D090 File Offset: 0x0014B490
		public void GetAllMatches(Action<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch[]> callback)
		{
			this.mTurnBasedManager.GetAllTurnbasedMatches(delegate(TurnBasedManager.TurnBasedMatchesResponse allMatches)
			{
				int num = allMatches.MyTurnMatchesCount() + allMatches.TheirTurnMatchesCount() + allMatches.CompletedMatchesCount();
				GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch[] array = new GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch[num];
				int num2 = 0;
				foreach (NativeTurnBasedMatch nativeTurnBasedMatch in allMatches.MyTurnMatches())
				{
					array[num2++] = nativeTurnBasedMatch.AsTurnBasedMatch(this.mNativeClient.GetUserId());
				}
				foreach (NativeTurnBasedMatch nativeTurnBasedMatch2 in allMatches.TheirTurnMatches())
				{
					array[num2++] = nativeTurnBasedMatch2.AsTurnBasedMatch(this.mNativeClient.GetUserId());
				}
				foreach (NativeTurnBasedMatch nativeTurnBasedMatch3 in allMatches.CompletedMatches())
				{
					array[num2++] = nativeTurnBasedMatch3.AsTurnBasedMatch(this.mNativeClient.GetUserId());
				}
				callback(array);
			});
		}

		// Token: 0x06004905 RID: 18693 RVA: 0x0014D0C8 File Offset: 0x0014B4C8
		private Action<TurnBasedManager.TurnBasedMatchResponse> BridgeMatchToUserCallback(Action<UIStatus, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> userCallback)
		{
			return delegate(TurnBasedManager.TurnBasedMatchResponse callbackResult)
			{
				using (NativeTurnBasedMatch nativeTurnBasedMatch = callbackResult.Match())
				{
					if (nativeTurnBasedMatch == null)
					{
						UIStatus arg = UIStatus.InternalError;
						CommonErrorStatus.MultiplayerStatus multiplayerStatus = callbackResult.ResponseStatus();
						switch (multiplayerStatus + 5)
						{
						case (CommonErrorStatus.MultiplayerStatus)0:
							arg = UIStatus.Timeout;
							break;
						case CommonErrorStatus.MultiplayerStatus.VALID:
							arg = UIStatus.VersionUpdateRequired;
							break;
						case CommonErrorStatus.MultiplayerStatus.VALID_BUT_STALE:
							arg = UIStatus.NotAuthorized;
							break;
						case (CommonErrorStatus.MultiplayerStatus)3:
							arg = UIStatus.InternalError;
							break;
						case (CommonErrorStatus.MultiplayerStatus)6:
							arg = UIStatus.Valid;
							break;
						case (CommonErrorStatus.MultiplayerStatus)7:
							arg = UIStatus.Valid;
							break;
						}
						userCallback(arg, null);
					}
					else
					{
						GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch turnBasedMatch = nativeTurnBasedMatch.AsTurnBasedMatch(this.mNativeClient.GetUserId());
						Logger.d("Passing converted match to user callback:" + turnBasedMatch);
						userCallback(UIStatus.Valid, turnBasedMatch);
					}
				}
			};
		}

		// Token: 0x06004906 RID: 18694 RVA: 0x0014D0F8 File Offset: 0x0014B4F8
		public void AcceptFromInbox(Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(callback);
			this.mTurnBasedManager.ShowInboxUI(delegate(TurnBasedManager.MatchInboxUIResponse callbackResult)
			{
				using (NativeTurnBasedMatch nativeTurnBasedMatch = callbackResult.Match())
				{
					if (nativeTurnBasedMatch == null)
					{
						callback(false, null);
					}
					else
					{
						GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch turnBasedMatch = nativeTurnBasedMatch.AsTurnBasedMatch(this.mNativeClient.GetUserId());
						Logger.d("Passing converted match to user callback:" + turnBasedMatch);
						callback(true, turnBasedMatch);
					}
				}
			});
		}

		// Token: 0x06004907 RID: 18695 RVA: 0x0014D144 File Offset: 0x0014B544
		public void AcceptInvitation(string invitationId, Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(callback);
			this.FindInvitationWithId(invitationId, delegate(GooglePlayGames.Native.PInvoke.MultiplayerInvitation invitation)
			{
				if (invitation == null)
				{
					Logger.e("Could not find invitation with id " + invitationId);
					callback(false, null);
					return;
				}
				this.mTurnBasedManager.AcceptInvitation(invitation, this.BridgeMatchToUserCallback(delegate(UIStatus status, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match)
				{
					callback(status == UIStatus.Valid, match);
				}));
			});
		}

		// Token: 0x06004908 RID: 18696 RVA: 0x0014D198 File Offset: 0x0014B598
		private void FindInvitationWithId(string invitationId, Action<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> callback)
		{
			this.mTurnBasedManager.GetAllTurnbasedMatches(delegate(TurnBasedManager.TurnBasedMatchesResponse allMatches)
			{
				if (allMatches.Status() <= (CommonErrorStatus.MultiplayerStatus)0)
				{
					callback(null);
					return;
				}
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerInvitation multiplayerInvitation in allMatches.Invitations())
				{
					using (multiplayerInvitation)
					{
						if (multiplayerInvitation.Id().Equals(invitationId))
						{
							callback(multiplayerInvitation);
							return;
						}
					}
				}
				callback(null);
			});
		}

		// Token: 0x06004909 RID: 18697 RVA: 0x0014D1D0 File Offset: 0x0014B5D0
		public void RegisterMatchDelegate(MatchDelegate del)
		{
			if (del == null)
			{
				this.mMatchDelegate = null;
			}
			else
			{
				this.mMatchDelegate = Callbacks.AsOnGameThreadCallback<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch, bool>(delegate(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, bool autoLaunch)
				{
					del(match, autoLaunch);
				});
			}
		}

		// Token: 0x0600490A RID: 18698 RVA: 0x0014D21C File Offset: 0x0014B61C
		internal void HandleMatchEvent(Types.MultiplayerEvent eventType, string matchId, NativeTurnBasedMatch match)
		{
			Action<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch, bool> currentDelegate = this.mMatchDelegate;
			if (currentDelegate == null)
			{
				return;
			}
			if (eventType == Types.MultiplayerEvent.REMOVED)
			{
				Logger.d("Ignoring REMOVE event for match " + matchId);
				return;
			}
			bool shouldAutolaunch = eventType == Types.MultiplayerEvent.UPDATED_FROM_APP_LAUNCH;
			match.ReferToMe();
			Callbacks.AsCoroutine(this.WaitForLogin(delegate
			{
				currentDelegate(match.AsTurnBasedMatch(this.mNativeClient.GetUserId()), shouldAutolaunch);
				match.ForgetMe();
			}));
		}

		// Token: 0x0600490B RID: 18699 RVA: 0x0014D29C File Offset: 0x0014B69C
		private IEnumerator WaitForLogin(Action method)
		{
			if (string.IsNullOrEmpty(this.mNativeClient.GetUserId()))
			{
				yield return null;
			}
			method();
			yield break;
		}

		// Token: 0x0600490C RID: 18700 RVA: 0x0014D2C0 File Offset: 0x0014B6C0
		public void TakeTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, byte[] data, string pendingParticipantId, Action<bool> callback)
		{
			Logger.describe(data);
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatchWithParticipant(match, pendingParticipantId, callback, delegate(GooglePlayGames.Native.PInvoke.MultiplayerParticipant pendingParticipant, NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.TakeTurn(foundMatch, data, pendingParticipant, delegate(TurnBasedManager.TurnBasedMatchResponse result)
				{
					if (result.RequestSucceeded())
					{
						callback(true);
					}
					else
					{
						Logger.d("Taking turn failed: " + result.ResponseStatus());
						callback(false);
					}
				});
			});
		}

		// Token: 0x0600490D RID: 18701 RVA: 0x0014D320 File Offset: 0x0014B720
		private void FindEqualVersionMatch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, Action<bool> onFailure, Action<NativeTurnBasedMatch> onVersionMatch)
		{
			this.mTurnBasedManager.GetMatch(match.MatchId, delegate(TurnBasedManager.TurnBasedMatchResponse response)
			{
				using (NativeTurnBasedMatch nativeTurnBasedMatch = response.Match())
				{
					if (nativeTurnBasedMatch == null)
					{
						Logger.e(string.Format("Could not find match {0}", match.MatchId));
						onFailure(false);
					}
					else if (nativeTurnBasedMatch.Version() != match.Version)
					{
						Logger.e(string.Format("Attempted to update a stale version of the match. Expected version was {0} but current version is {1}.", match.Version, nativeTurnBasedMatch.Version()));
						onFailure(false);
					}
					else
					{
						onVersionMatch(nativeTurnBasedMatch);
					}
				}
			});
		}

		// Token: 0x0600490E RID: 18702 RVA: 0x0014D36C File Offset: 0x0014B76C
		private void FindEqualVersionMatchWithParticipant(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, string participantId, Action<bool> onFailure, Action<GooglePlayGames.Native.PInvoke.MultiplayerParticipant, NativeTurnBasedMatch> onFoundParticipantAndMatch)
		{
			this.FindEqualVersionMatch(match, onFailure, delegate(NativeTurnBasedMatch foundMatch)
			{
				if (participantId == null)
				{
					using (GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant = GooglePlayGames.Native.PInvoke.MultiplayerParticipant.AutomatchingSentinel())
					{
						onFoundParticipantAndMatch(multiplayerParticipant, foundMatch);
						return;
					}
				}
				using (GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant2 = foundMatch.ParticipantWithId(participantId))
				{
					if (multiplayerParticipant2 == null)
					{
						Logger.e(string.Format("Located match {0} but desired participant with ID {1} could not be found", match.MatchId, participantId));
						onFailure(false);
					}
					else
					{
						onFoundParticipantAndMatch(multiplayerParticipant2, foundMatch);
					}
				}
			});
		}

		// Token: 0x0600490F RID: 18703 RVA: 0x0014D3BA File Offset: 0x0014B7BA
		public int GetMaxMatchDataSize()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06004910 RID: 18704 RVA: 0x0014D3C4 File Offset: 0x0014B7C4
		public void Finish(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, byte[] data, MatchOutcome outcome, Action<bool> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatch(match, callback, delegate(NativeTurnBasedMatch foundMatch)
			{
				GooglePlayGames.Native.PInvoke.ParticipantResults participantResults = foundMatch.Results();
				foreach (string text in outcome.ParticipantIds)
				{
					Types.MatchResult matchResult = NativeTurnBasedMultiplayerClient.ResultToMatchResult(outcome.GetResultFor(text));
					uint placementFor = outcome.GetPlacementFor(text);
					if (participantResults.HasResultsForParticipant(text))
					{
						Types.MatchResult matchResult2 = participantResults.ResultsForParticipant(text);
						uint num = participantResults.PlacingForParticipant(text);
						if (matchResult != matchResult2 || placementFor != num)
						{
							Logger.e(string.Format("Attempted to override existing results for participant {0}: Placing {1}, Result {2}", text, num, matchResult2));
							callback(false);
							return;
						}
					}
					else
					{
						GooglePlayGames.Native.PInvoke.ParticipantResults participantResults2 = participantResults;
						participantResults = participantResults2.WithResult(text, placementFor, matchResult);
						participantResults2.Dispose();
					}
				}
				this.mTurnBasedManager.FinishMatchDuringMyTurn(foundMatch, data, participantResults, delegate(TurnBasedManager.TurnBasedMatchResponse response)
				{
					callback(response.RequestSucceeded());
				});
			});
		}

		// Token: 0x06004911 RID: 18705 RVA: 0x0014D41E File Offset: 0x0014B81E
		private static Types.MatchResult ResultToMatchResult(MatchOutcome.ParticipantResult result)
		{
			switch (result)
			{
			case MatchOutcome.ParticipantResult.None:
				return Types.MatchResult.NONE;
			case MatchOutcome.ParticipantResult.Win:
				return Types.MatchResult.WIN;
			case MatchOutcome.ParticipantResult.Loss:
				return Types.MatchResult.LOSS;
			case MatchOutcome.ParticipantResult.Tie:
				return Types.MatchResult.TIE;
			default:
				Logger.e("Received unknown ParticipantResult " + result);
				return Types.MatchResult.NONE;
			}
		}

		// Token: 0x06004912 RID: 18706 RVA: 0x0014D45C File Offset: 0x0014B85C
		public void AcknowledgeFinished(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, Action<bool> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatch(match, callback, delegate(NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.ConfirmPendingCompletion(foundMatch, delegate(TurnBasedManager.TurnBasedMatchResponse response)
				{
					callback(response.RequestSucceeded());
				});
			});
		}

		// Token: 0x06004913 RID: 18707 RVA: 0x0014D4A8 File Offset: 0x0014B8A8
		public void Leave(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, Action<bool> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatch(match, callback, delegate(NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.LeaveMatchDuringTheirTurn(foundMatch, delegate(CommonErrorStatus.MultiplayerStatus status)
				{
					callback(status > (CommonErrorStatus.MultiplayerStatus)0);
				});
			});
		}

		// Token: 0x06004914 RID: 18708 RVA: 0x0014D4F4 File Offset: 0x0014B8F4
		public void LeaveDuringTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, string pendingParticipantId, Action<bool> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatchWithParticipant(match, pendingParticipantId, callback, delegate(GooglePlayGames.Native.PInvoke.MultiplayerParticipant pendingParticipant, NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.LeaveDuringMyTurn(foundMatch, pendingParticipant, delegate(CommonErrorStatus.MultiplayerStatus status)
				{
					callback(status > (CommonErrorStatus.MultiplayerStatus)0);
				});
			});
		}

		// Token: 0x06004915 RID: 18709 RVA: 0x0014D540 File Offset: 0x0014B940
		public void Cancel(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, Action<bool> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool>(callback);
			this.FindEqualVersionMatch(match, callback, delegate(NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.CancelMatch(foundMatch, delegate(CommonErrorStatus.MultiplayerStatus status)
				{
					callback(status > (CommonErrorStatus.MultiplayerStatus)0);
				});
			});
		}

		// Token: 0x06004916 RID: 18710 RVA: 0x0014D58C File Offset: 0x0014B98C
		public void Rematch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, Action<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> callback)
		{
			callback = Callbacks.AsOnGameThreadCallback<bool, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>(callback);
			this.FindEqualVersionMatch(match, delegate(bool failed)
			{
				callback(false, null);
			}, delegate(NativeTurnBasedMatch foundMatch)
			{
				this.mTurnBasedManager.Rematch(foundMatch, this.BridgeMatchToUserCallback(delegate(UIStatus status, GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch m)
				{
					callback(status == UIStatus.Valid, m);
				}));
			});
		}

		// Token: 0x06004917 RID: 18711 RVA: 0x0014D5DD File Offset: 0x0014B9DD
		public void DeclineInvitation(string invitationId)
		{
			this.FindInvitationWithId(invitationId, delegate(GooglePlayGames.Native.PInvoke.MultiplayerInvitation invitation)
			{
				if (invitation == null)
				{
					return;
				}
				this.mTurnBasedManager.DeclineInvitation(invitation);
			});
		}

		// Token: 0x04004CD8 RID: 19672
		private readonly TurnBasedManager mTurnBasedManager;

		// Token: 0x04004CD9 RID: 19673
		private readonly NativeClient mNativeClient;

		// Token: 0x04004CDA RID: 19674
		private volatile Action<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch, bool> mMatchDelegate;
	}
}
