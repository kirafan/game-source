﻿using System;
using System.Collections.Generic;
using System.Linq;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF3 RID: 3571
	internal class NativeEventClient : IEventsClient
	{
		// Token: 0x06004827 RID: 18471 RVA: 0x00148E04 File Offset: 0x00147204
		internal NativeEventClient(EventManager manager)
		{
			this.mEventManager = Misc.CheckNotNull<EventManager>(manager);
		}

		// Token: 0x06004828 RID: 18472 RVA: 0x00148E18 File Offset: 0x00147218
		public void FetchAllEvents(DataSource source, Action<ResponseStatus, List<IEvent>> callback)
		{
			Misc.CheckNotNull<Action<ResponseStatus, List<IEvent>>>(callback);
			callback = CallbackUtils.ToOnGameThread<ResponseStatus, List<IEvent>>(callback);
			this.mEventManager.FetchAll(ConversionUtils.AsDataSource(source), delegate(EventManager.FetchAllResponse response)
			{
				ResponseStatus arg = ConversionUtils.ConvertResponseStatus(response.ResponseStatus());
				if (!response.RequestSucceeded())
				{
					callback(arg, new List<IEvent>());
				}
				else
				{
					callback(arg, response.Data().Cast<IEvent>().ToList<IEvent>());
				}
			});
		}

		// Token: 0x06004829 RID: 18473 RVA: 0x00148E6C File Offset: 0x0014726C
		public void FetchEvent(DataSource source, string eventId, Action<ResponseStatus, IEvent> callback)
		{
			Misc.CheckNotNull<string>(eventId);
			Misc.CheckNotNull<Action<ResponseStatus, IEvent>>(callback);
			this.mEventManager.Fetch(ConversionUtils.AsDataSource(source), eventId, delegate(EventManager.FetchResponse response)
			{
				ResponseStatus arg = ConversionUtils.ConvertResponseStatus(response.ResponseStatus());
				if (!response.RequestSucceeded())
				{
					callback(arg, null);
				}
				else
				{
					callback(arg, response.Data());
				}
			});
		}

		// Token: 0x0600482A RID: 18474 RVA: 0x00148EB7 File Offset: 0x001472B7
		public void IncrementEvent(string eventId, uint stepsToIncrement)
		{
			Misc.CheckNotNull<string>(eventId);
			this.mEventManager.Increment(eventId, stepsToIncrement);
		}

		// Token: 0x04004C98 RID: 19608
		private readonly EventManager mEventManager;
	}
}
