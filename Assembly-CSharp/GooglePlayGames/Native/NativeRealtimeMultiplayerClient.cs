﻿using System;
using System.Collections.Generic;
using System.Linq;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF9 RID: 3577
	public class NativeRealtimeMultiplayerClient : IRealTimeMultiplayerClient
	{
		// Token: 0x06004857 RID: 18519 RVA: 0x00149D70 File Offset: 0x00148170
		internal NativeRealtimeMultiplayerClient(NativeClient nativeClient, RealtimeManager manager)
		{
			this.mNativeClient = Misc.CheckNotNull<NativeClient>(nativeClient);
			this.mRealtimeManager = Misc.CheckNotNull<RealtimeManager>(manager);
			this.mCurrentSession = this.GetTerminatedSession();
			PlayGamesHelperObject.AddPauseCallback(new Action<bool>(this.HandleAppPausing));
		}

		// Token: 0x06004858 RID: 18520 RVA: 0x00149DC8 File Offset: 0x001481C8
		private NativeRealtimeMultiplayerClient.RoomSession GetTerminatedSession()
		{
			NativeRealtimeMultiplayerClient.RoomSession roomSession = new NativeRealtimeMultiplayerClient.RoomSession(this.mRealtimeManager, new NativeRealtimeMultiplayerClient.NoopListener());
			roomSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(roomSession), false);
			return roomSession;
		}

		// Token: 0x06004859 RID: 18521 RVA: 0x00149DF4 File Offset: 0x001481F4
		public void CreateQuickGame(uint minOpponents, uint maxOpponents, uint variant, RealTimeMultiplayerListener listener)
		{
			this.CreateQuickGame(minOpponents, maxOpponents, variant, 0UL, listener);
		}

		// Token: 0x0600485A RID: 18522 RVA: 0x00149E04 File Offset: 0x00148204
		public void CreateQuickGame(uint minOpponents, uint maxOpponents, uint variant, ulong exclusiveBitMask, RealTimeMultiplayerListener listener)
		{
			object obj = this.mSessionLock;
			lock (obj)
			{
				NativeRealtimeMultiplayerClient.RoomSession newSession = new NativeRealtimeMultiplayerClient.RoomSession(this.mRealtimeManager, listener);
				if (this.mCurrentSession.IsActive())
				{
					Logger.e("Received attempt to create a new room without cleaning up the old one.");
					newSession.LeaveRoom();
				}
				else
				{
					this.mCurrentSession = newSession;
					Logger.d("QuickGame: Setting MinPlayersToStart = " + minOpponents);
					this.mCurrentSession.MinPlayersToStart = minOpponents;
					using (RealtimeRoomConfigBuilder realtimeRoomConfigBuilder = RealtimeRoomConfigBuilder.Create())
					{
						RealtimeRoomConfig config = realtimeRoomConfigBuilder.SetMinimumAutomatchingPlayers(minOpponents).SetMaximumAutomatchingPlayers(maxOpponents).SetVariant(variant).SetExclusiveBitMask(exclusiveBitMask).Build();
						using (config)
						{
							using (GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper helper = NativeRealtimeMultiplayerClient.HelperForSession(newSession))
							{
								newSession.StartRoomCreation(this.mNativeClient.GetUserId(), delegate
								{
									this.mRealtimeManager.CreateRoom(config, helper, new Action<RealtimeManager.RealTimeRoomResponse>(newSession.HandleRoomResponse));
								});
							}
						}
					}
				}
			}
		}

		// Token: 0x0600485B RID: 18523 RVA: 0x00149FD4 File Offset: 0x001483D4
		private static GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper HelperForSession(NativeRealtimeMultiplayerClient.RoomSession session)
		{
			return GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper.Create().SetOnDataReceivedCallback(delegate(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant, byte[] data, bool isReliable)
			{
				session.OnDataReceived(room, participant, data, isReliable);
			}).SetOnParticipantStatusChangedCallback(delegate(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
				session.OnParticipantStatusChanged(room, participant);
			}).SetOnRoomConnectedSetChangedCallback(delegate(NativeRealTimeRoom room)
			{
				session.OnConnectedSetChanged(room);
			}).SetOnRoomStatusChangedCallback(delegate(NativeRealTimeRoom room)
			{
				session.OnRoomStatusChanged(room);
			});
		}

		// Token: 0x0600485C RID: 18524 RVA: 0x0014A037 File Offset: 0x00148437
		private void HandleAppPausing(bool paused)
		{
			if (paused)
			{
				Logger.d("Application is pausing, which disconnects the RTMP  client.  Leaving room.");
				this.LeaveRoom();
			}
		}

		// Token: 0x0600485D RID: 18525 RVA: 0x0014A050 File Offset: 0x00148450
		public void CreateWithInvitationScreen(uint minOpponents, uint maxOppponents, uint variant, RealTimeMultiplayerListener listener)
		{
			object obj = this.mSessionLock;
			lock (obj)
			{
				NativeRealtimeMultiplayerClient.RoomSession newRoom = new NativeRealtimeMultiplayerClient.RoomSession(this.mRealtimeManager, listener);
				if (this.mCurrentSession.IsActive())
				{
					Logger.e("Received attempt to create a new room without cleaning up the old one.");
					newRoom.LeaveRoom();
				}
				else
				{
					this.mCurrentSession = newRoom;
					this.mCurrentSession.ShowingUI = true;
					this.mRealtimeManager.ShowPlayerSelectUI(minOpponents, maxOppponents, true, delegate(PlayerSelectUIResponse response)
					{
						this.mCurrentSession.ShowingUI = false;
						if (response.Status() != CommonErrorStatus.UIStatus.VALID)
						{
							Logger.d("User did not complete invitation screen.");
							newRoom.LeaveRoom();
							return;
						}
						this.mCurrentSession.MinPlayersToStart = response.MinimumAutomatchingPlayers() + (uint)response.Count<string>() + 1U;
						using (RealtimeRoomConfigBuilder realtimeRoomConfigBuilder = RealtimeRoomConfigBuilder.Create())
						{
							realtimeRoomConfigBuilder.SetVariant(variant);
							realtimeRoomConfigBuilder.PopulateFromUIResponse(response);
							using (RealtimeRoomConfig config = realtimeRoomConfigBuilder.Build())
							{
								using (GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper helper = NativeRealtimeMultiplayerClient.HelperForSession(newRoom))
								{
									newRoom.StartRoomCreation(this.mNativeClient.GetUserId(), delegate
									{
										this.mRealtimeManager.CreateRoom(config, helper, new Action<RealtimeManager.RealTimeRoomResponse>(newRoom.HandleRoomResponse));
									});
								}
							}
						}
					});
				}
			}
		}

		// Token: 0x0600485E RID: 18526 RVA: 0x0014A11C File Offset: 0x0014851C
		public void ShowWaitingRoomUI()
		{
			object obj = this.mSessionLock;
			lock (obj)
			{
				this.mCurrentSession.ShowWaitingRoomUI();
			}
		}

		// Token: 0x0600485F RID: 18527 RVA: 0x0014A160 File Offset: 0x00148560
		public void GetAllInvitations(Action<Invitation[]> callback)
		{
			this.mRealtimeManager.FetchInvitations(delegate(RealtimeManager.FetchInvitationsResponse response)
			{
				if (!response.RequestSucceeded())
				{
					Logger.e("Couldn't load invitations.");
					callback(new Invitation[0]);
					return;
				}
				List<Invitation> list = new List<Invitation>();
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerInvitation multiplayerInvitation in response.Invitations())
				{
					using (multiplayerInvitation)
					{
						list.Add(multiplayerInvitation.AsInvitation());
					}
				}
				callback(list.ToArray());
			});
		}

		// Token: 0x06004860 RID: 18528 RVA: 0x0014A194 File Offset: 0x00148594
		public void AcceptFromInbox(RealTimeMultiplayerListener listener)
		{
			object obj = this.mSessionLock;
			lock (obj)
			{
				NativeRealtimeMultiplayerClient.<AcceptFromInbox>c__AnonStorey9 <AcceptFromInbox>c__AnonStorey = new NativeRealtimeMultiplayerClient.<AcceptFromInbox>c__AnonStorey9();
				<AcceptFromInbox>c__AnonStorey.$this = this;
				<AcceptFromInbox>c__AnonStorey.newRoom = new NativeRealtimeMultiplayerClient.RoomSession(this.mRealtimeManager, listener);
				if (this.mCurrentSession.IsActive())
				{
					Logger.e("Received attempt to accept invitation without cleaning up active session.");
					<AcceptFromInbox>c__AnonStorey.newRoom.LeaveRoom();
				}
				else
				{
					this.mCurrentSession = <AcceptFromInbox>c__AnonStorey.newRoom;
					this.mCurrentSession.ShowingUI = true;
					this.mRealtimeManager.ShowRoomInboxUI(delegate(RealtimeManager.RoomInboxUIResponse response)
					{
						NativeRealtimeMultiplayerClient.<AcceptFromInbox>c__AnonStorey9.<AcceptFromInbox>c__AnonStoreyA <AcceptFromInbox>c__AnonStoreyA = new NativeRealtimeMultiplayerClient.<AcceptFromInbox>c__AnonStorey9.<AcceptFromInbox>c__AnonStoreyA();
						<AcceptFromInbox>c__AnonStoreyA.<>f__ref$9 = <AcceptFromInbox>c__AnonStorey;
						<AcceptFromInbox>c__AnonStorey.$this.mCurrentSession.ShowingUI = false;
						if (response.ResponseStatus() != CommonErrorStatus.UIStatus.VALID)
						{
							Logger.d("User did not complete invitation screen.");
							<AcceptFromInbox>c__AnonStorey.newRoom.LeaveRoom();
							return;
						}
						<AcceptFromInbox>c__AnonStoreyA.invitation = response.Invitation();
						using (GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper helper = NativeRealtimeMultiplayerClient.HelperForSession(<AcceptFromInbox>c__AnonStorey.newRoom))
						{
							Logger.d("About to accept invitation " + <AcceptFromInbox>c__AnonStoreyA.invitation.Id());
							<AcceptFromInbox>c__AnonStorey.newRoom.StartRoomCreation(<AcceptFromInbox>c__AnonStorey.$this.mNativeClient.GetUserId(), delegate
							{
								<AcceptFromInbox>c__AnonStorey.mRealtimeManager.AcceptInvitation(<AcceptFromInbox>c__AnonStoreyA.invitation, helper, delegate(RealtimeManager.RealTimeRoomResponse acceptResponse)
								{
									using (<AcceptFromInbox>c__AnonStoreyA.invitation)
									{
										<AcceptFromInbox>c__AnonStorey.newRoom.HandleRoomResponse(acceptResponse);
										<AcceptFromInbox>c__AnonStorey.newRoom.SetInvitation(<AcceptFromInbox>c__AnonStoreyA.invitation.AsInvitation());
									}
								});
							});
						}
					});
				}
			}
		}

		// Token: 0x06004861 RID: 18529 RVA: 0x0014A248 File Offset: 0x00148648
		public void AcceptInvitation(string invitationId, RealTimeMultiplayerListener listener)
		{
			object obj = this.mSessionLock;
			lock (obj)
			{
				NativeRealtimeMultiplayerClient.RoomSession newRoom = new NativeRealtimeMultiplayerClient.RoomSession(this.mRealtimeManager, listener);
				if (this.mCurrentSession.IsActive())
				{
					Logger.e("Received attempt to accept invitation without cleaning up active session.");
					newRoom.LeaveRoom();
				}
				else
				{
					this.mCurrentSession = newRoom;
					this.mRealtimeManager.FetchInvitations(delegate(RealtimeManager.FetchInvitationsResponse response)
					{
						if (!response.RequestSucceeded())
						{
							Logger.e("Couldn't load invitations.");
							newRoom.LeaveRoom();
							return;
						}
						using (IEnumerator<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> enumerator = response.Invitations().GetEnumerator())
						{
							while (enumerator.MoveNext())
							{
								GooglePlayGames.Native.PInvoke.MultiplayerInvitation invitation = enumerator.Current;
								using (invitation)
								{
									if (invitation.Id().Equals(invitationId))
									{
										this.mCurrentSession.MinPlayersToStart = invitation.AutomatchingSlots() + invitation.ParticipantCount();
										Logger.d("Setting MinPlayersToStart with invitation to : " + this.mCurrentSession.MinPlayersToStart);
										using (GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper helper = NativeRealtimeMultiplayerClient.HelperForSession(newRoom))
										{
											newRoom.StartRoomCreation(this.mNativeClient.GetUserId(), delegate
											{
												this.mRealtimeManager.AcceptInvitation(invitation, helper, new Action<RealtimeManager.RealTimeRoomResponse>(newRoom.HandleRoomResponse));
											});
											return;
										}
									}
								}
							}
						}
						Logger.e("Room creation failed since we could not find invitation with ID " + invitationId);
						newRoom.LeaveRoom();
					});
				}
			}
		}

		// Token: 0x06004862 RID: 18530 RVA: 0x0014A304 File Offset: 0x00148704
		public Invitation GetInvitation()
		{
			return this.mCurrentSession.GetInvitation();
		}

		// Token: 0x06004863 RID: 18531 RVA: 0x0014A313 File Offset: 0x00148713
		public void LeaveRoom()
		{
			this.mCurrentSession.LeaveRoom();
		}

		// Token: 0x06004864 RID: 18532 RVA: 0x0014A322 File Offset: 0x00148722
		public void SendMessageToAll(bool reliable, byte[] data)
		{
			this.mCurrentSession.SendMessageToAll(reliable, data);
		}

		// Token: 0x06004865 RID: 18533 RVA: 0x0014A333 File Offset: 0x00148733
		public void SendMessageToAll(bool reliable, byte[] data, int offset, int length)
		{
			this.mCurrentSession.SendMessageToAll(reliable, data, offset, length);
		}

		// Token: 0x06004866 RID: 18534 RVA: 0x0014A347 File Offset: 0x00148747
		public void SendMessage(bool reliable, string participantId, byte[] data)
		{
			this.mCurrentSession.SendMessage(reliable, participantId, data);
		}

		// Token: 0x06004867 RID: 18535 RVA: 0x0014A359 File Offset: 0x00148759
		public void SendMessage(bool reliable, string participantId, byte[] data, int offset, int length)
		{
			this.mCurrentSession.SendMessage(reliable, participantId, data, offset, length);
		}

		// Token: 0x06004868 RID: 18536 RVA: 0x0014A36F File Offset: 0x0014876F
		public List<Participant> GetConnectedParticipants()
		{
			return this.mCurrentSession.GetConnectedParticipants();
		}

		// Token: 0x06004869 RID: 18537 RVA: 0x0014A37E File Offset: 0x0014877E
		public Participant GetSelf()
		{
			return this.mCurrentSession.GetSelf();
		}

		// Token: 0x0600486A RID: 18538 RVA: 0x0014A38D File Offset: 0x0014878D
		public Participant GetParticipant(string participantId)
		{
			return this.mCurrentSession.GetParticipant(participantId);
		}

		// Token: 0x0600486B RID: 18539 RVA: 0x0014A39D File Offset: 0x0014879D
		public bool IsRoomConnected()
		{
			return this.mCurrentSession.IsRoomConnected();
		}

		// Token: 0x0600486C RID: 18540 RVA: 0x0014A3AC File Offset: 0x001487AC
		public void DeclineInvitation(string invitationId)
		{
			this.mRealtimeManager.FetchInvitations(delegate(RealtimeManager.FetchInvitationsResponse response)
			{
				if (!response.RequestSucceeded())
				{
					Logger.e("Couldn't load invitations.");
					return;
				}
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerInvitation multiplayerInvitation in response.Invitations())
				{
					using (multiplayerInvitation)
					{
						if (multiplayerInvitation.Id().Equals(invitationId))
						{
							this.mRealtimeManager.DeclineInvitation(multiplayerInvitation);
						}
					}
				}
			});
		}

		// Token: 0x0600486D RID: 18541 RVA: 0x0014A3E4 File Offset: 0x001487E4
		private static T WithDefault<T>(T presented, T defaultValue) where T : class
		{
			return (presented == null) ? defaultValue : presented;
		}

		// Token: 0x04004CA1 RID: 19617
		private readonly object mSessionLock = new object();

		// Token: 0x04004CA2 RID: 19618
		private readonly NativeClient mNativeClient;

		// Token: 0x04004CA3 RID: 19619
		private readonly RealtimeManager mRealtimeManager;

		// Token: 0x04004CA4 RID: 19620
		private volatile NativeRealtimeMultiplayerClient.RoomSession mCurrentSession;

		// Token: 0x02000DFA RID: 3578
		private class NoopListener : RealTimeMultiplayerListener
		{
			// Token: 0x0600486F RID: 18543 RVA: 0x0014A400 File Offset: 0x00148800
			public void OnRoomSetupProgress(float percent)
			{
			}

			// Token: 0x06004870 RID: 18544 RVA: 0x0014A402 File Offset: 0x00148802
			public void OnRoomConnected(bool success)
			{
			}

			// Token: 0x06004871 RID: 18545 RVA: 0x0014A404 File Offset: 0x00148804
			public void OnLeftRoom()
			{
			}

			// Token: 0x06004872 RID: 18546 RVA: 0x0014A406 File Offset: 0x00148806
			public void OnParticipantLeft(Participant participant)
			{
			}

			// Token: 0x06004873 RID: 18547 RVA: 0x0014A408 File Offset: 0x00148808
			public void OnPeersConnected(string[] participantIds)
			{
			}

			// Token: 0x06004874 RID: 18548 RVA: 0x0014A40A File Offset: 0x0014880A
			public void OnPeersDisconnected(string[] participantIds)
			{
			}

			// Token: 0x06004875 RID: 18549 RVA: 0x0014A40C File Offset: 0x0014880C
			public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
			{
			}
		}

		// Token: 0x02000DFB RID: 3579
		private class RoomSession
		{
			// Token: 0x06004876 RID: 18550 RVA: 0x0014A410 File Offset: 0x00148810
			internal RoomSession(RealtimeManager manager, RealTimeMultiplayerListener listener)
			{
				this.mManager = Misc.CheckNotNull<RealtimeManager>(manager);
				this.mListener = new NativeRealtimeMultiplayerClient.OnGameThreadForwardingListener(listener);
				this.EnterState(new NativeRealtimeMultiplayerClient.BeforeRoomCreateStartedState(this), false);
				this.mStillPreRoomCreation = true;
			}

			// Token: 0x170004DD RID: 1245
			// (get) Token: 0x06004877 RID: 18551 RVA: 0x0014A45C File Offset: 0x0014885C
			// (set) Token: 0x06004878 RID: 18552 RVA: 0x0014A466 File Offset: 0x00148866
			internal bool ShowingUI
			{
				get
				{
					return this.mShowingUI;
				}
				set
				{
					this.mShowingUI = value;
				}
			}

			// Token: 0x170004DE RID: 1246
			// (get) Token: 0x06004879 RID: 18553 RVA: 0x0014A471 File Offset: 0x00148871
			// (set) Token: 0x0600487A RID: 18554 RVA: 0x0014A479 File Offset: 0x00148879
			internal uint MinPlayersToStart
			{
				get
				{
					return this.mMinPlayersToStart;
				}
				set
				{
					this.mMinPlayersToStart = value;
				}
			}

			// Token: 0x0600487B RID: 18555 RVA: 0x0014A482 File Offset: 0x00148882
			internal RealtimeManager Manager()
			{
				return this.mManager;
			}

			// Token: 0x0600487C RID: 18556 RVA: 0x0014A48A File Offset: 0x0014888A
			internal bool IsActive()
			{
				return this.mState.IsActive();
			}

			// Token: 0x0600487D RID: 18557 RVA: 0x0014A499 File Offset: 0x00148899
			internal string SelfPlayerId()
			{
				return this.mCurrentPlayerId;
			}

			// Token: 0x0600487E RID: 18558 RVA: 0x0014A4A3 File Offset: 0x001488A3
			public void SetInvitation(Invitation invitation)
			{
				this.mInvitation = invitation;
			}

			// Token: 0x0600487F RID: 18559 RVA: 0x0014A4AC File Offset: 0x001488AC
			public Invitation GetInvitation()
			{
				return this.mInvitation;
			}

			// Token: 0x06004880 RID: 18560 RVA: 0x0014A4B4 File Offset: 0x001488B4
			internal NativeRealtimeMultiplayerClient.OnGameThreadForwardingListener OnGameThreadListener()
			{
				return this.mListener;
			}

			// Token: 0x06004881 RID: 18561 RVA: 0x0014A4BC File Offset: 0x001488BC
			internal void EnterState(NativeRealtimeMultiplayerClient.State handler)
			{
				this.EnterState(handler, true);
			}

			// Token: 0x06004882 RID: 18562 RVA: 0x0014A4C8 File Offset: 0x001488C8
			internal void EnterState(NativeRealtimeMultiplayerClient.State handler, bool fireStateEnteredEvent)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					this.mState = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.State>(handler);
					if (fireStateEnteredEvent)
					{
						Logger.d("Entering state: " + handler.GetType().Name);
						this.mState.OnStateEntered();
					}
				}
			}

			// Token: 0x06004883 RID: 18563 RVA: 0x0014A53C File Offset: 0x0014893C
			internal void LeaveRoom()
			{
				if (!this.ShowingUI)
				{
					object obj = this.mLifecycleLock;
					lock (obj)
					{
						this.mState.LeaveRoom();
					}
				}
				else
				{
					Logger.d("Not leaving room since showing UI");
				}
			}

			// Token: 0x06004884 RID: 18564 RVA: 0x0014A59C File Offset: 0x0014899C
			internal void ShowWaitingRoomUI()
			{
				this.mState.ShowWaitingRoomUI(this.MinPlayersToStart);
			}

			// Token: 0x06004885 RID: 18565 RVA: 0x0014A5B4 File Offset: 0x001489B4
			internal void StartRoomCreation(string currentPlayerId, Action createRoom)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					if (!this.mStillPreRoomCreation)
					{
						Logger.e("Room creation started more than once, this shouldn't happen!");
					}
					else if (!this.mState.IsActive())
					{
						Logger.w("Received an attempt to create a room after the session was already torn down!");
					}
					else
					{
						this.mCurrentPlayerId = Misc.CheckNotNull<string>(currentPlayerId);
						this.mStillPreRoomCreation = false;
						this.EnterState(new NativeRealtimeMultiplayerClient.RoomCreationPendingState(this));
						createRoom();
					}
				}
			}

			// Token: 0x06004886 RID: 18566 RVA: 0x0014A650 File Offset: 0x00148A50
			internal void OnRoomStatusChanged(NativeRealTimeRoom room)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					this.mState.OnRoomStatusChanged(room);
				}
			}

			// Token: 0x06004887 RID: 18567 RVA: 0x0014A694 File Offset: 0x00148A94
			internal void OnConnectedSetChanged(NativeRealTimeRoom room)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					this.mState.OnConnectedSetChanged(room);
				}
			}

			// Token: 0x06004888 RID: 18568 RVA: 0x0014A6D8 File Offset: 0x00148AD8
			internal void OnParticipantStatusChanged(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					this.mState.OnParticipantStatusChanged(room, participant);
				}
			}

			// Token: 0x06004889 RID: 18569 RVA: 0x0014A720 File Offset: 0x00148B20
			internal void HandleRoomResponse(RealtimeManager.RealTimeRoomResponse response)
			{
				object obj = this.mLifecycleLock;
				lock (obj)
				{
					this.mState.HandleRoomResponse(response);
				}
			}

			// Token: 0x0600488A RID: 18570 RVA: 0x0014A764 File Offset: 0x00148B64
			internal void OnDataReceived(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant sender, byte[] data, bool isReliable)
			{
				this.mState.OnDataReceived(room, sender, data, isReliable);
			}

			// Token: 0x0600488B RID: 18571 RVA: 0x0014A778 File Offset: 0x00148B78
			internal void SendMessageToAll(bool reliable, byte[] data)
			{
				this.SendMessageToAll(reliable, data, 0, data.Length);
			}

			// Token: 0x0600488C RID: 18572 RVA: 0x0014A786 File Offset: 0x00148B86
			internal void SendMessageToAll(bool reliable, byte[] data, int offset, int length)
			{
				this.mState.SendToAll(data, offset, length, reliable);
			}

			// Token: 0x0600488D RID: 18573 RVA: 0x0014A79A File Offset: 0x00148B9A
			internal void SendMessage(bool reliable, string participantId, byte[] data)
			{
				this.SendMessage(reliable, participantId, data, 0, data.Length);
			}

			// Token: 0x0600488E RID: 18574 RVA: 0x0014A7A9 File Offset: 0x00148BA9
			internal void SendMessage(bool reliable, string participantId, byte[] data, int offset, int length)
			{
				this.mState.SendToSpecificRecipient(participantId, data, offset, length, reliable);
			}

			// Token: 0x0600488F RID: 18575 RVA: 0x0014A7BF File Offset: 0x00148BBF
			internal List<Participant> GetConnectedParticipants()
			{
				return this.mState.GetConnectedParticipants();
			}

			// Token: 0x06004890 RID: 18576 RVA: 0x0014A7CE File Offset: 0x00148BCE
			internal virtual Participant GetSelf()
			{
				return this.mState.GetSelf();
			}

			// Token: 0x06004891 RID: 18577 RVA: 0x0014A7DD File Offset: 0x00148BDD
			internal virtual Participant GetParticipant(string participantId)
			{
				return this.mState.GetParticipant(participantId);
			}

			// Token: 0x06004892 RID: 18578 RVA: 0x0014A7ED File Offset: 0x00148BED
			internal virtual bool IsRoomConnected()
			{
				return this.mState.IsRoomConnected();
			}

			// Token: 0x04004CA5 RID: 19621
			private readonly object mLifecycleLock = new object();

			// Token: 0x04004CA6 RID: 19622
			private readonly NativeRealtimeMultiplayerClient.OnGameThreadForwardingListener mListener;

			// Token: 0x04004CA7 RID: 19623
			private readonly RealtimeManager mManager;

			// Token: 0x04004CA8 RID: 19624
			private volatile string mCurrentPlayerId;

			// Token: 0x04004CA9 RID: 19625
			private volatile NativeRealtimeMultiplayerClient.State mState;

			// Token: 0x04004CAA RID: 19626
			private volatile bool mStillPreRoomCreation;

			// Token: 0x04004CAB RID: 19627
			private Invitation mInvitation;

			// Token: 0x04004CAC RID: 19628
			private volatile bool mShowingUI;

			// Token: 0x04004CAD RID: 19629
			private uint mMinPlayersToStart;
		}

		// Token: 0x02000DFC RID: 3580
		private class OnGameThreadForwardingListener
		{
			// Token: 0x06004893 RID: 18579 RVA: 0x0014A7FC File Offset: 0x00148BFC
			internal OnGameThreadForwardingListener(RealTimeMultiplayerListener listener)
			{
				this.mListener = Misc.CheckNotNull<RealTimeMultiplayerListener>(listener);
			}

			// Token: 0x06004894 RID: 18580 RVA: 0x0014A810 File Offset: 0x00148C10
			public void RoomSetupProgress(float percent)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnRoomSetupProgress(percent);
				});
			}

			// Token: 0x06004895 RID: 18581 RVA: 0x0014A844 File Offset: 0x00148C44
			public void RoomConnected(bool success)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnRoomConnected(success);
				});
			}

			// Token: 0x06004896 RID: 18582 RVA: 0x0014A876 File Offset: 0x00148C76
			public void LeftRoom()
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnLeftRoom();
				});
			}

			// Token: 0x06004897 RID: 18583 RVA: 0x0014A88C File Offset: 0x00148C8C
			public void PeersConnected(string[] participantIds)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnPeersConnected(participantIds);
				});
			}

			// Token: 0x06004898 RID: 18584 RVA: 0x0014A8C0 File Offset: 0x00148CC0
			public void PeersDisconnected(string[] participantIds)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnPeersDisconnected(participantIds);
				});
			}

			// Token: 0x06004899 RID: 18585 RVA: 0x0014A8F4 File Offset: 0x00148CF4
			public void RealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnRealTimeMessageReceived(isReliable, senderId, data);
				});
			}

			// Token: 0x0600489A RID: 18586 RVA: 0x0014A934 File Offset: 0x00148D34
			public void ParticipantLeft(Participant participant)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					this.mListener.OnParticipantLeft(participant);
				});
			}

			// Token: 0x04004CAE RID: 19630
			private readonly RealTimeMultiplayerListener mListener;
		}

		// Token: 0x02000DFD RID: 3581
		internal abstract class State
		{
			// Token: 0x0600489D RID: 18589 RVA: 0x0014AA47 File Offset: 0x00148E47
			internal virtual void HandleRoomResponse(RealtimeManager.RealTimeRoomResponse response)
			{
				Logger.d(base.GetType().Name + ".HandleRoomResponse: Defaulting to no-op.");
			}

			// Token: 0x0600489E RID: 18590 RVA: 0x0014AA63 File Offset: 0x00148E63
			internal virtual bool IsActive()
			{
				Logger.d(base.GetType().Name + ".IsNonPreemptable: Is preemptable by default.");
				return true;
			}

			// Token: 0x0600489F RID: 18591 RVA: 0x0014AA80 File Offset: 0x00148E80
			internal virtual void LeaveRoom()
			{
				Logger.d(base.GetType().Name + ".LeaveRoom: Defaulting to no-op.");
			}

			// Token: 0x060048A0 RID: 18592 RVA: 0x0014AA9C File Offset: 0x00148E9C
			internal virtual void ShowWaitingRoomUI(uint minimumParticipantsBeforeStarting)
			{
				Logger.d(base.GetType().Name + ".ShowWaitingRoomUI: Defaulting to no-op.");
			}

			// Token: 0x060048A1 RID: 18593 RVA: 0x0014AAB8 File Offset: 0x00148EB8
			internal virtual void OnStateEntered()
			{
				Logger.d(base.GetType().Name + ".OnStateEntered: Defaulting to no-op.");
			}

			// Token: 0x060048A2 RID: 18594 RVA: 0x0014AAD4 File Offset: 0x00148ED4
			internal virtual void OnRoomStatusChanged(NativeRealTimeRoom room)
			{
				Logger.d(base.GetType().Name + ".OnRoomStatusChanged: Defaulting to no-op.");
			}

			// Token: 0x060048A3 RID: 18595 RVA: 0x0014AAF0 File Offset: 0x00148EF0
			internal virtual void OnConnectedSetChanged(NativeRealTimeRoom room)
			{
				Logger.d(base.GetType().Name + ".OnConnectedSetChanged: Defaulting to no-op.");
			}

			// Token: 0x060048A4 RID: 18596 RVA: 0x0014AB0C File Offset: 0x00148F0C
			internal virtual void OnParticipantStatusChanged(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
				Logger.d(base.GetType().Name + ".OnParticipantStatusChanged: Defaulting to no-op.");
			}

			// Token: 0x060048A5 RID: 18597 RVA: 0x0014AB28 File Offset: 0x00148F28
			internal virtual void OnDataReceived(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant sender, byte[] data, bool isReliable)
			{
				Logger.d(base.GetType().Name + ".OnDataReceived: Defaulting to no-op.");
			}

			// Token: 0x060048A6 RID: 18598 RVA: 0x0014AB44 File Offset: 0x00148F44
			internal virtual void SendToSpecificRecipient(string recipientId, byte[] data, int offset, int length, bool isReliable)
			{
				Logger.d(base.GetType().Name + ".SendToSpecificRecipient: Defaulting to no-op.");
			}

			// Token: 0x060048A7 RID: 18599 RVA: 0x0014AB60 File Offset: 0x00148F60
			internal virtual void SendToAll(byte[] data, int offset, int length, bool isReliable)
			{
				Logger.d(base.GetType().Name + ".SendToApp: Defaulting to no-op.");
			}

			// Token: 0x060048A8 RID: 18600 RVA: 0x0014AB7C File Offset: 0x00148F7C
			internal virtual List<Participant> GetConnectedParticipants()
			{
				Logger.d(base.GetType().Name + ".GetConnectedParticipants: Returning empty connected participants");
				return new List<Participant>();
			}

			// Token: 0x060048A9 RID: 18601 RVA: 0x0014AB9D File Offset: 0x00148F9D
			internal virtual Participant GetSelf()
			{
				Logger.d(base.GetType().Name + ".GetSelf: Returning null self.");
				return null;
			}

			// Token: 0x060048AA RID: 18602 RVA: 0x0014ABBA File Offset: 0x00148FBA
			internal virtual Participant GetParticipant(string participantId)
			{
				Logger.d(base.GetType().Name + ".GetSelf: Returning null participant.");
				return null;
			}

			// Token: 0x060048AB RID: 18603 RVA: 0x0014ABD7 File Offset: 0x00148FD7
			internal virtual bool IsRoomConnected()
			{
				Logger.d(base.GetType().Name + ".IsRoomConnected: Returning room not connected.");
				return false;
			}
		}

		// Token: 0x02000DFE RID: 3582
		private abstract class MessagingEnabledState : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048AC RID: 18604 RVA: 0x0014ABF4 File Offset: 0x00148FF4
			internal MessagingEnabledState(NativeRealtimeMultiplayerClient.RoomSession session, NativeRealTimeRoom room)
			{
				this.mSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
				this.UpdateCurrentRoom(room);
			}

			// Token: 0x060048AD RID: 18605 RVA: 0x0014AC10 File Offset: 0x00149010
			internal void UpdateCurrentRoom(NativeRealTimeRoom room)
			{
				if (this.mRoom != null)
				{
					this.mRoom.Dispose();
				}
				this.mRoom = Misc.CheckNotNull<NativeRealTimeRoom>(room);
				this.mNativeParticipants = this.mRoom.Participants().ToDictionary((GooglePlayGames.Native.PInvoke.MultiplayerParticipant p) => p.Id());
				this.mParticipants = (from p in this.mNativeParticipants.Values
				select p.AsParticipant()).ToDictionary((Participant p) => p.ParticipantId);
			}

			// Token: 0x060048AE RID: 18606 RVA: 0x0014ACC7 File Offset: 0x001490C7
			internal sealed override void OnRoomStatusChanged(NativeRealTimeRoom room)
			{
				this.HandleRoomStatusChanged(room);
				this.UpdateCurrentRoom(room);
			}

			// Token: 0x060048AF RID: 18607 RVA: 0x0014ACD7 File Offset: 0x001490D7
			internal virtual void HandleRoomStatusChanged(NativeRealTimeRoom room)
			{
			}

			// Token: 0x060048B0 RID: 18608 RVA: 0x0014ACD9 File Offset: 0x001490D9
			internal sealed override void OnConnectedSetChanged(NativeRealTimeRoom room)
			{
				this.HandleConnectedSetChanged(room);
				this.UpdateCurrentRoom(room);
			}

			// Token: 0x060048B1 RID: 18609 RVA: 0x0014ACE9 File Offset: 0x001490E9
			internal virtual void HandleConnectedSetChanged(NativeRealTimeRoom room)
			{
			}

			// Token: 0x060048B2 RID: 18610 RVA: 0x0014ACEB File Offset: 0x001490EB
			internal sealed override void OnParticipantStatusChanged(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
				this.HandleParticipantStatusChanged(room, participant);
				this.UpdateCurrentRoom(room);
			}

			// Token: 0x060048B3 RID: 18611 RVA: 0x0014ACFC File Offset: 0x001490FC
			internal virtual void HandleParticipantStatusChanged(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
			}

			// Token: 0x060048B4 RID: 18612 RVA: 0x0014AD00 File Offset: 0x00149100
			internal sealed override List<Participant> GetConnectedParticipants()
			{
				List<Participant> list = (from p in this.mParticipants.Values
				where p.IsConnectedToRoom
				select p).ToList<Participant>();
				list.Sort();
				return list;
			}

			// Token: 0x060048B5 RID: 18613 RVA: 0x0014AD48 File Offset: 0x00149148
			internal override void SendToSpecificRecipient(string recipientId, byte[] data, int offset, int length, bool isReliable)
			{
				if (!this.mNativeParticipants.ContainsKey(recipientId))
				{
					Logger.e("Attempted to send message to unknown participant " + recipientId);
					return;
				}
				if (isReliable)
				{
					this.mSession.Manager().SendReliableMessage(this.mRoom, this.mNativeParticipants[recipientId], Misc.GetSubsetBytes(data, offset, length), null);
				}
				else
				{
					this.mSession.Manager().SendUnreliableMessageToSpecificParticipants(this.mRoom, new List<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
					{
						this.mNativeParticipants[recipientId]
					}, Misc.GetSubsetBytes(data, offset, length));
				}
			}

			// Token: 0x060048B6 RID: 18614 RVA: 0x0014ADE8 File Offset: 0x001491E8
			internal override void SendToAll(byte[] data, int offset, int length, bool isReliable)
			{
				byte[] subsetBytes = Misc.GetSubsetBytes(data, offset, length);
				if (isReliable)
				{
					foreach (string recipientId in this.mNativeParticipants.Keys)
					{
						this.SendToSpecificRecipient(recipientId, subsetBytes, 0, subsetBytes.Length, true);
					}
				}
				else
				{
					this.mSession.Manager().SendUnreliableMessageToAll(this.mRoom, subsetBytes);
				}
			}

			// Token: 0x060048B7 RID: 18615 RVA: 0x0014AE7C File Offset: 0x0014927C
			internal override void OnDataReceived(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant sender, byte[] data, bool isReliable)
			{
				this.mSession.OnGameThreadListener().RealTimeMessageReceived(isReliable, sender.Id(), data);
			}

			// Token: 0x04004CAF RID: 19631
			protected readonly NativeRealtimeMultiplayerClient.RoomSession mSession;

			// Token: 0x04004CB0 RID: 19632
			protected NativeRealTimeRoom mRoom;

			// Token: 0x04004CB1 RID: 19633
			protected Dictionary<string, GooglePlayGames.Native.PInvoke.MultiplayerParticipant> mNativeParticipants;

			// Token: 0x04004CB2 RID: 19634
			protected Dictionary<string, Participant> mParticipants;
		}

		// Token: 0x02000DFF RID: 3583
		private class BeforeRoomCreateStartedState : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048BC RID: 18620 RVA: 0x0014AEB7 File Offset: 0x001492B7
			internal BeforeRoomCreateStartedState(NativeRealtimeMultiplayerClient.RoomSession session)
			{
				this.mContainingSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
			}

			// Token: 0x060048BD RID: 18621 RVA: 0x0014AECB File Offset: 0x001492CB
			internal override void LeaveRoom()
			{
				Logger.d("Session was torn down before room was created.");
				this.mContainingSession.OnGameThreadListener().RoomConnected(false);
				this.mContainingSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(this.mContainingSession));
			}

			// Token: 0x04004CB7 RID: 19639
			private readonly NativeRealtimeMultiplayerClient.RoomSession mContainingSession;
		}

		// Token: 0x02000E00 RID: 3584
		private class RoomCreationPendingState : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048BE RID: 18622 RVA: 0x0014AEFE File Offset: 0x001492FE
			internal RoomCreationPendingState(NativeRealtimeMultiplayerClient.RoomSession session)
			{
				this.mContainingSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
			}

			// Token: 0x060048BF RID: 18623 RVA: 0x0014AF14 File Offset: 0x00149314
			internal override void HandleRoomResponse(RealtimeManager.RealTimeRoomResponse response)
			{
				if (!response.RequestSucceeded())
				{
					this.mContainingSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(this.mContainingSession));
					this.mContainingSession.OnGameThreadListener().RoomConnected(false);
					return;
				}
				this.mContainingSession.EnterState(new NativeRealtimeMultiplayerClient.ConnectingState(response.Room(), this.mContainingSession));
			}

			// Token: 0x060048C0 RID: 18624 RVA: 0x0014AF70 File Offset: 0x00149370
			internal override bool IsActive()
			{
				return true;
			}

			// Token: 0x060048C1 RID: 18625 RVA: 0x0014AF73 File Offset: 0x00149373
			internal override void LeaveRoom()
			{
				Logger.d("Received request to leave room during room creation, aborting creation.");
				this.mContainingSession.EnterState(new NativeRealtimeMultiplayerClient.AbortingRoomCreationState(this.mContainingSession));
			}

			// Token: 0x04004CB8 RID: 19640
			private readonly NativeRealtimeMultiplayerClient.RoomSession mContainingSession;
		}

		// Token: 0x02000E01 RID: 3585
		private class ConnectingState : NativeRealtimeMultiplayerClient.MessagingEnabledState
		{
			// Token: 0x060048C2 RID: 18626 RVA: 0x0014AF95 File Offset: 0x00149395
			internal ConnectingState(NativeRealTimeRoom room, NativeRealtimeMultiplayerClient.RoomSession session) : base(session, room)
			{
				this.mPercentPerParticipant = 80f / session.MinPlayersToStart;
			}

			// Token: 0x060048C3 RID: 18627 RVA: 0x0014AFC9 File Offset: 0x001493C9
			internal override void OnStateEntered()
			{
				this.mSession.OnGameThreadListener().RoomSetupProgress(this.mPercentComplete);
			}

			// Token: 0x060048C4 RID: 18628 RVA: 0x0014AFE4 File Offset: 0x001493E4
			internal override void HandleConnectedSetChanged(NativeRealTimeRoom room)
			{
				HashSet<string> hashSet = new HashSet<string>();
				if ((room.Status() == Types.RealTimeRoomStatus.AUTO_MATCHING || room.Status() == Types.RealTimeRoomStatus.CONNECTING) && this.mSession.MinPlayersToStart <= room.ParticipantCount())
				{
					this.mSession.MinPlayersToStart = this.mSession.MinPlayersToStart + room.ParticipantCount();
					this.mPercentPerParticipant = 80f / this.mSession.MinPlayersToStart;
				}
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant in room.Participants())
				{
					using (multiplayerParticipant)
					{
						if (multiplayerParticipant.IsConnectedToRoom())
						{
							hashSet.Add(multiplayerParticipant.Id());
						}
					}
				}
				if (this.mConnectedParticipants.Equals(hashSet))
				{
					Logger.w("Received connected set callback with unchanged connected set!");
					return;
				}
				IEnumerable<string> source = this.mConnectedParticipants.Except(hashSet);
				if (room.Status() == Types.RealTimeRoomStatus.DELETED)
				{
					Logger.e("Participants disconnected during room setup, failing. Participants were: " + string.Join(",", source.ToArray<string>()));
					this.mSession.OnGameThreadListener().RoomConnected(false);
					this.mSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(this.mSession));
					return;
				}
				IEnumerable<string> source2 = hashSet.Except(this.mConnectedParticipants);
				Logger.d("New participants connected: " + string.Join(",", source2.ToArray<string>()));
				if (room.Status() == Types.RealTimeRoomStatus.ACTIVE)
				{
					Logger.d("Fully connected! Transitioning to active state.");
					this.mSession.EnterState(new NativeRealtimeMultiplayerClient.ActiveState(room, this.mSession));
					this.mSession.OnGameThreadListener().RoomConnected(true);
					return;
				}
				this.mPercentComplete += this.mPercentPerParticipant * (float)source2.Count<string>();
				this.mConnectedParticipants = hashSet;
				this.mSession.OnGameThreadListener().RoomSetupProgress(this.mPercentComplete);
			}

			// Token: 0x060048C5 RID: 18629 RVA: 0x0014B1FC File Offset: 0x001495FC
			internal override void HandleParticipantStatusChanged(NativeRealTimeRoom room, GooglePlayGames.Native.PInvoke.MultiplayerParticipant participant)
			{
				if (!NativeRealtimeMultiplayerClient.ConnectingState.FailedStatuses.Contains(participant.Status()))
				{
					return;
				}
				this.mSession.OnGameThreadListener().ParticipantLeft(participant.AsParticipant());
				if (room.Status() != Types.RealTimeRoomStatus.CONNECTING && room.Status() != Types.RealTimeRoomStatus.AUTO_MATCHING)
				{
					this.LeaveRoom();
				}
			}

			// Token: 0x060048C6 RID: 18630 RVA: 0x0014B253 File Offset: 0x00149653
			internal override void LeaveRoom()
			{
				this.mSession.EnterState(new NativeRealtimeMultiplayerClient.LeavingRoom(this.mSession, this.mRoom, delegate()
				{
					this.mSession.OnGameThreadListener().RoomConnected(false);
				}));
			}

			// Token: 0x060048C7 RID: 18631 RVA: 0x0014B27D File Offset: 0x0014967D
			internal override void ShowWaitingRoomUI(uint minimumParticipantsBeforeStarting)
			{
				this.mSession.ShowingUI = true;
				this.mSession.Manager().ShowWaitingRoomUI(this.mRoom, minimumParticipantsBeforeStarting, delegate(RealtimeManager.WaitingRoomUIResponse response)
				{
					this.mSession.ShowingUI = false;
					Logger.d("ShowWaitingRoomUI Response: " + response.ResponseStatus());
					if (response.ResponseStatus() == CommonErrorStatus.UIStatus.VALID)
					{
						Logger.d(string.Concat(new object[]
						{
							"Connecting state ShowWaitingRoomUI: room pcount:",
							response.Room().ParticipantCount(),
							" status: ",
							response.Room().Status()
						}));
						if (response.Room().Status() == Types.RealTimeRoomStatus.ACTIVE)
						{
							this.mSession.EnterState(new NativeRealtimeMultiplayerClient.ActiveState(response.Room(), this.mSession));
						}
					}
					else if (response.ResponseStatus() == CommonErrorStatus.UIStatus.ERROR_LEFT_ROOM)
					{
						this.LeaveRoom();
					}
					else
					{
						this.mSession.OnGameThreadListener().RoomSetupProgress(this.mPercentComplete);
					}
				});
			}

			// Token: 0x04004CB9 RID: 19641
			private const float InitialPercentComplete = 20f;

			// Token: 0x04004CBA RID: 19642
			private static readonly HashSet<Types.ParticipantStatus> FailedStatuses = new HashSet<Types.ParticipantStatus>
			{
				Types.ParticipantStatus.DECLINED,
				Types.ParticipantStatus.LEFT
			};

			// Token: 0x04004CBB RID: 19643
			private HashSet<string> mConnectedParticipants = new HashSet<string>();

			// Token: 0x04004CBC RID: 19644
			private float mPercentComplete = 20f;

			// Token: 0x04004CBD RID: 19645
			private float mPercentPerParticipant;
		}

		// Token: 0x02000E02 RID: 3586
		private class ActiveState : NativeRealtimeMultiplayerClient.MessagingEnabledState
		{
			// Token: 0x060048CB RID: 18635 RVA: 0x0014B3D1 File Offset: 0x001497D1
			internal ActiveState(NativeRealTimeRoom room, NativeRealtimeMultiplayerClient.RoomSession session) : base(session, room)
			{
			}

			// Token: 0x060048CC RID: 18636 RVA: 0x0014B3DB File Offset: 0x001497DB
			internal override void OnStateEntered()
			{
				if (this.GetSelf() == null)
				{
					Logger.e("Room reached active state with unknown participant for the player");
					this.LeaveRoom();
				}
			}

			// Token: 0x060048CD RID: 18637 RVA: 0x0014B3F8 File Offset: 0x001497F8
			internal override bool IsRoomConnected()
			{
				return true;
			}

			// Token: 0x060048CE RID: 18638 RVA: 0x0014B3FB File Offset: 0x001497FB
			internal override Participant GetParticipant(string participantId)
			{
				if (!this.mParticipants.ContainsKey(participantId))
				{
					Logger.e("Attempted to retrieve unknown participant " + participantId);
					return null;
				}
				return this.mParticipants[participantId];
			}

			// Token: 0x060048CF RID: 18639 RVA: 0x0014B42C File Offset: 0x0014982C
			internal override Participant GetSelf()
			{
				foreach (Participant participant in this.mParticipants.Values)
				{
					if (participant.Player != null && participant.Player.id.Equals(this.mSession.SelfPlayerId()))
					{
						return participant;
					}
				}
				return null;
			}

			// Token: 0x060048D0 RID: 18640 RVA: 0x0014B4BC File Offset: 0x001498BC
			internal override void HandleConnectedSetChanged(NativeRealTimeRoom room)
			{
				List<string> list = new List<string>();
				List<string> list2 = new List<string>();
				Dictionary<string, GooglePlayGames.Native.PInvoke.MultiplayerParticipant> dictionary = room.Participants().ToDictionary((GooglePlayGames.Native.PInvoke.MultiplayerParticipant p) => p.Id());
				foreach (string text in this.mNativeParticipants.Keys)
				{
					GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant = dictionary[text];
					GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant2 = this.mNativeParticipants[text];
					if (!multiplayerParticipant.IsConnectedToRoom())
					{
						list2.Add(text);
					}
					if (!multiplayerParticipant2.IsConnectedToRoom() && multiplayerParticipant.IsConnectedToRoom())
					{
						list.Add(text);
					}
				}
				foreach (GooglePlayGames.Native.PInvoke.MultiplayerParticipant multiplayerParticipant3 in this.mNativeParticipants.Values)
				{
					multiplayerParticipant3.Dispose();
				}
				this.mNativeParticipants = dictionary;
				this.mParticipants = (from p in this.mNativeParticipants.Values
				select p.AsParticipant()).ToDictionary((Participant p) => p.ParticipantId);
				Logger.d("Updated participant statuses: " + string.Join(",", (from p in this.mParticipants.Values
				select p.ToString()).ToArray<string>()));
				if (list2.Contains(this.GetSelf().ParticipantId))
				{
					Logger.w("Player was disconnected from the multiplayer session.");
				}
				string selfId = this.GetSelf().ParticipantId;
				list = (from peerId in list
				where !peerId.Equals(selfId)
				select peerId).ToList<string>();
				list2 = (from peerId in list2
				where !peerId.Equals(selfId)
				select peerId).ToList<string>();
				if (list.Count > 0)
				{
					list.Sort();
					this.mSession.OnGameThreadListener().PeersConnected((from peer in list
					where !peer.Equals(selfId)
					select peer).ToArray<string>());
				}
				if (list2.Count > 0)
				{
					list2.Sort();
					this.mSession.OnGameThreadListener().PeersDisconnected((from peer in list2
					where !peer.Equals(selfId)
					select peer).ToArray<string>());
				}
			}

			// Token: 0x060048D1 RID: 18641 RVA: 0x0014B76C File Offset: 0x00149B6C
			internal override void LeaveRoom()
			{
				this.mSession.EnterState(new NativeRealtimeMultiplayerClient.LeavingRoom(this.mSession, this.mRoom, delegate()
				{
					this.mSession.OnGameThreadListener().LeftRoom();
				}));
			}
		}

		// Token: 0x02000E03 RID: 3587
		private class ShutdownState : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048D7 RID: 18647 RVA: 0x0014B814 File Offset: 0x00149C14
			internal ShutdownState(NativeRealtimeMultiplayerClient.RoomSession session)
			{
				this.mSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
			}

			// Token: 0x060048D8 RID: 18648 RVA: 0x0014B828 File Offset: 0x00149C28
			internal override bool IsActive()
			{
				return false;
			}

			// Token: 0x060048D9 RID: 18649 RVA: 0x0014B82B File Offset: 0x00149C2B
			internal override void LeaveRoom()
			{
				this.mSession.OnGameThreadListener().LeftRoom();
			}

			// Token: 0x04004CC2 RID: 19650
			private readonly NativeRealtimeMultiplayerClient.RoomSession mSession;
		}

		// Token: 0x02000E04 RID: 3588
		private class LeavingRoom : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048DA RID: 18650 RVA: 0x0014B83D File Offset: 0x00149C3D
			internal LeavingRoom(NativeRealtimeMultiplayerClient.RoomSession session, NativeRealTimeRoom room, Action leavingCompleteCallback)
			{
				this.mSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
				this.mRoomToLeave = Misc.CheckNotNull<NativeRealTimeRoom>(room);
				this.mLeavingCompleteCallback = Misc.CheckNotNull<Action>(leavingCompleteCallback);
			}

			// Token: 0x060048DB RID: 18651 RVA: 0x0014B869 File Offset: 0x00149C69
			internal override bool IsActive()
			{
				return false;
			}

			// Token: 0x060048DC RID: 18652 RVA: 0x0014B86C File Offset: 0x00149C6C
			internal override void OnStateEntered()
			{
				this.mSession.Manager().LeaveRoom(this.mRoomToLeave, delegate(CommonErrorStatus.ResponseStatus status)
				{
					this.mLeavingCompleteCallback();
					this.mSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(this.mSession));
				});
			}

			// Token: 0x04004CC3 RID: 19651
			private readonly NativeRealtimeMultiplayerClient.RoomSession mSession;

			// Token: 0x04004CC4 RID: 19652
			private readonly NativeRealTimeRoom mRoomToLeave;

			// Token: 0x04004CC5 RID: 19653
			private readonly Action mLeavingCompleteCallback;
		}

		// Token: 0x02000E05 RID: 3589
		private class AbortingRoomCreationState : NativeRealtimeMultiplayerClient.State
		{
			// Token: 0x060048DE RID: 18654 RVA: 0x0014B8B3 File Offset: 0x00149CB3
			internal AbortingRoomCreationState(NativeRealtimeMultiplayerClient.RoomSession session)
			{
				this.mSession = Misc.CheckNotNull<NativeRealtimeMultiplayerClient.RoomSession>(session);
			}

			// Token: 0x060048DF RID: 18655 RVA: 0x0014B8C7 File Offset: 0x00149CC7
			internal override bool IsActive()
			{
				return false;
			}

			// Token: 0x060048E0 RID: 18656 RVA: 0x0014B8CC File Offset: 0x00149CCC
			internal override void HandleRoomResponse(RealtimeManager.RealTimeRoomResponse response)
			{
				if (!response.RequestSucceeded())
				{
					this.mSession.EnterState(new NativeRealtimeMultiplayerClient.ShutdownState(this.mSession));
					this.mSession.OnGameThreadListener().RoomConnected(false);
					return;
				}
				this.mSession.EnterState(new NativeRealtimeMultiplayerClient.LeavingRoom(this.mSession, response.Room(), delegate()
				{
					this.mSession.OnGameThreadListener().RoomConnected(false);
				}));
			}

			// Token: 0x04004CC6 RID: 19654
			private readonly NativeRealtimeMultiplayerClient.RoomSession mSession;
		}
	}
}
