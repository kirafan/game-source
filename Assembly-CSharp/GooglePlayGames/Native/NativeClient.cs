﻿using System;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.BasicApi.Video;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF1 RID: 3569
	public class NativeClient : IPlayGamesClient
	{
		// Token: 0x060047EE RID: 18414 RVA: 0x0014709D File Offset: 0x0014549D
		internal NativeClient(PlayGamesClientConfiguration configuration, IClientImpl clientImpl)
		{
			PlayGamesHelperObject.CreateObject();
			this.mConfiguration = Misc.CheckNotNull<PlayGamesClientConfiguration>(configuration);
			this.clientImpl = clientImpl;
		}

		// Token: 0x060047EF RID: 18415 RVA: 0x001470D4 File Offset: 0x001454D4
		private GooglePlayGames.Native.PInvoke.GameServices GameServices()
		{
			object gameServicesLock = this.GameServicesLock;
			GooglePlayGames.Native.PInvoke.GameServices result;
			lock (gameServicesLock)
			{
				result = this.mServices;
			}
			return result;
		}

		// Token: 0x060047F0 RID: 18416 RVA: 0x00147114 File Offset: 0x00145514
		public void Authenticate(Action<bool, string> callback, bool silent)
		{
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				if (this.mAuthState == NativeClient.AuthState.Authenticated)
				{
					NativeClient.InvokeCallbackOnGameThread<bool, string>(callback, true, null);
					return;
				}
				if (this.mSilentAuthFailed && silent)
				{
					NativeClient.InvokeCallbackOnGameThread<bool, string>(callback, false, "silent auth failed");
					return;
				}
				if (callback != null)
				{
					if (silent)
					{
						this.mSilentAuthCallbacks = (Action<bool, string>)Delegate.Combine(this.mSilentAuthCallbacks, callback);
					}
					else
					{
						this.mPendingAuthCallbacks = (Action<bool, string>)Delegate.Combine(this.mPendingAuthCallbacks, callback);
					}
				}
			}
			this.friendsLoading = false;
			this.InitializeTokenClient();
			if (this.mTokenClient.NeedsToRun())
			{
				Debug.Log("Starting Auth with token client.");
				this.mTokenClient.FetchTokens(delegate(int result)
				{
					this.InitializeGameServices();
					if (result == 0)
					{
						this.GameServices().StartAuthorizationUI();
					}
					else
					{
						this.HandleAuthTransition(GooglePlayGames.Native.Cwrapper.Types.AuthOperation.SIGN_IN, (CommonErrorStatus.AuthStatus)result);
					}
				});
			}
			else
			{
				this.InitializeGameServices();
				if (!silent)
				{
					this.GameServices().StartAuthorizationUI();
				}
			}
		}

		// Token: 0x060047F1 RID: 18417 RVA: 0x00147230 File Offset: 0x00145630
		private static Action<T> AsOnGameThreadCallback<T>(Action<T> callback)
		{
			if (callback == null)
			{
				return delegate(T A_0)
				{
				};
			}
			return delegate(T result)
			{
				NativeClient.InvokeCallbackOnGameThread<T>(callback, result);
			};
		}

		// Token: 0x060047F2 RID: 18418 RVA: 0x00147270 File Offset: 0x00145670
		private static void InvokeCallbackOnGameThread<T, S>(Action<T, S> callback, T data, S msg)
		{
			if (callback == null)
			{
				return;
			}
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				GooglePlayGames.OurUtils.Logger.d("Invoking user callback on game thread");
				callback(data, msg);
			});
		}

		// Token: 0x060047F3 RID: 18419 RVA: 0x001472B8 File Offset: 0x001456B8
		private static void InvokeCallbackOnGameThread<T>(Action<T> callback, T data)
		{
			if (callback == null)
			{
				return;
			}
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				GooglePlayGames.OurUtils.Logger.d("Invoking user callback on game thread");
				callback(data);
			});
		}

		// Token: 0x060047F4 RID: 18420 RVA: 0x001472F8 File Offset: 0x001456F8
		private void InitializeGameServices()
		{
			object gameServicesLock = this.GameServicesLock;
			lock (gameServicesLock)
			{
				if (this.mServices == null)
				{
					using (GameServicesBuilder gameServicesBuilder = GameServicesBuilder.Create())
					{
						using (PlatformConfiguration platformConfiguration = this.clientImpl.CreatePlatformConfiguration(this.mConfiguration))
						{
							this.RegisterInvitationDelegate(this.mConfiguration.InvitationDelegate);
							gameServicesBuilder.SetOnAuthFinishedCallback(new GameServicesBuilder.AuthFinishedCallback(this.HandleAuthTransition));
							gameServicesBuilder.SetOnTurnBasedMatchEventCallback(delegate(GooglePlayGames.Native.Cwrapper.Types.MultiplayerEvent eventType, string matchId, NativeTurnBasedMatch match)
							{
								this.mTurnBasedClient.HandleMatchEvent(eventType, matchId, match);
							});
							gameServicesBuilder.SetOnMultiplayerInvitationEventCallback(new Action<GooglePlayGames.Native.Cwrapper.Types.MultiplayerEvent, string, GooglePlayGames.Native.PInvoke.MultiplayerInvitation>(this.HandleInvitation));
							if (this.mConfiguration.EnableSavedGames)
							{
								gameServicesBuilder.EnableSnapshots();
							}
							string[] scopes = this.mConfiguration.Scopes;
							for (int i = 0; i < scopes.Length; i++)
							{
								gameServicesBuilder.AddOauthScope(scopes[i]);
							}
							if (this.mConfiguration.IsHidingPopups)
							{
								gameServicesBuilder.SetShowConnectingPopup(false);
							}
							Debug.Log("Building GPG services, implicitly attempts silent auth");
							this.mAuthState = NativeClient.AuthState.SilentPending;
							this.mServices = gameServicesBuilder.Build(platformConfiguration);
							this.mEventsClient = new NativeEventClient(new GooglePlayGames.Native.PInvoke.EventManager(this.mServices));
							this.mQuestsClient = new NativeQuestClient(new GooglePlayGames.Native.PInvoke.QuestManager(this.mServices));
							this.mVideoClient = new NativeVideoClient(new GooglePlayGames.Native.PInvoke.VideoManager(this.mServices));
							this.mTurnBasedClient = new NativeTurnBasedMultiplayerClient(this, new TurnBasedManager(this.mServices));
							this.mTurnBasedClient.RegisterMatchDelegate(this.mConfiguration.MatchDelegate);
							this.mRealTimeClient = new NativeRealtimeMultiplayerClient(this, new RealtimeManager(this.mServices));
							if (this.mConfiguration.EnableSavedGames)
							{
								this.mSavedGameClient = new NativeSavedGameClient(new GooglePlayGames.Native.PInvoke.SnapshotManager(this.mServices));
							}
							else
							{
								this.mSavedGameClient = new UnsupportedSavedGamesClient("You must enable saved games before it can be used. See PlayGamesClientConfiguration.Builder.EnableSavedGames.");
							}
							this.mAuthState = NativeClient.AuthState.SilentPending;
							this.InitializeTokenClient();
						}
					}
				}
			}
		}

		// Token: 0x060047F5 RID: 18421 RVA: 0x00147574 File Offset: 0x00145974
		private void InitializeTokenClient()
		{
			if (this.mTokenClient != null)
			{
				return;
			}
			this.mTokenClient = this.clientImpl.CreateTokenClient(true);
			if (!GameInfo.WebClientIdInitialized() && (this.mConfiguration.IsRequestingIdToken || this.mConfiguration.IsRequestingAuthCode))
			{
				GooglePlayGames.OurUtils.Logger.e("Server Auth Code and ID Token require web clientId to configured.");
			}
			string[] scopes = this.mConfiguration.Scopes;
			this.mTokenClient.SetWebClientId(string.Empty);
			this.mTokenClient.SetRequestAuthCode(this.mConfiguration.IsRequestingAuthCode, this.mConfiguration.IsForcingRefresh);
			this.mTokenClient.SetRequestEmail(this.mConfiguration.IsRequestingEmail);
			this.mTokenClient.SetRequestIdToken(this.mConfiguration.IsRequestingIdToken);
			this.mTokenClient.SetHidePopups(this.mConfiguration.IsHidingPopups);
			this.mTokenClient.AddOauthScopes(scopes);
			this.mTokenClient.SetAccountName(this.mConfiguration.AccountName);
		}

		// Token: 0x060047F6 RID: 18422 RVA: 0x001476A8 File Offset: 0x00145AA8
		internal void HandleInvitation(GooglePlayGames.Native.Cwrapper.Types.MultiplayerEvent eventType, string invitationId, GooglePlayGames.Native.PInvoke.MultiplayerInvitation invitation)
		{
			Action<Invitation, bool> currentHandler = this.mInvitationDelegate;
			if (currentHandler == null)
			{
				GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
				{
					"Received ",
					eventType,
					" for invitation ",
					invitationId,
					" but no handler was registered."
				}));
				return;
			}
			if (eventType == GooglePlayGames.Native.Cwrapper.Types.MultiplayerEvent.REMOVED)
			{
				GooglePlayGames.OurUtils.Logger.d("Ignoring REMOVED for invitation " + invitationId);
				return;
			}
			bool shouldAutolaunch = eventType == GooglePlayGames.Native.Cwrapper.Types.MultiplayerEvent.UPDATED_FROM_APP_LAUNCH;
			Invitation invite = invitation.AsInvitation();
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				currentHandler(invite, shouldAutolaunch);
			});
		}

		// Token: 0x060047F7 RID: 18423 RVA: 0x00147749 File Offset: 0x00145B49
		public string GetUserEmail()
		{
			if (!this.IsAuthenticated())
			{
				Debug.Log("Cannot get API client - not authenticated");
				return null;
			}
			return this.mTokenClient.GetEmail();
		}

		// Token: 0x060047F8 RID: 18424 RVA: 0x0014776F File Offset: 0x00145B6F
		public string GetIdToken()
		{
			if (!this.IsAuthenticated())
			{
				Debug.Log("Cannot get API client - not authenticated");
				return null;
			}
			return this.mTokenClient.GetIdToken();
		}

		// Token: 0x060047F9 RID: 18425 RVA: 0x00147795 File Offset: 0x00145B95
		public string GetServerAuthCode()
		{
			if (!this.IsAuthenticated())
			{
				Debug.Log("Cannot get API client - not authenticated");
				return null;
			}
			return this.mTokenClient.GetAuthCode();
		}

		// Token: 0x060047FA RID: 18426 RVA: 0x001477BB File Offset: 0x00145BBB
		public void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback)
		{
			this.mTokenClient.GetAnotherServerAuthCode(reAuthenticateIfNeeded, callback);
		}

		// Token: 0x060047FB RID: 18427 RVA: 0x001477CC File Offset: 0x00145BCC
		public bool IsAuthenticated()
		{
			object authStateLock = this.AuthStateLock;
			bool result;
			lock (authStateLock)
			{
				result = (this.mAuthState == NativeClient.AuthState.Authenticated);
			}
			return result;
		}

		// Token: 0x060047FC RID: 18428 RVA: 0x00147810 File Offset: 0x00145C10
		public void LoadFriends(Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.d("Cannot loadFriends when not authenticated");
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					callback(false);
				});
				return;
			}
			if (this.mFriends != null)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					callback(true);
				});
				return;
			}
			this.mServices.PlayerManager().FetchFriends(delegate(ResponseStatus status, List<GooglePlayGames.BasicApi.Multiplayer.Player> players)
			{
				if (status == ResponseStatus.Success || status == ResponseStatus.SuccessWithStale)
				{
					this.mFriends = players;
					PlayGamesHelperObject.RunOnGameThread(delegate
					{
						callback(true);
					});
				}
				else
				{
					this.mFriends = new List<GooglePlayGames.BasicApi.Multiplayer.Player>();
					GooglePlayGames.OurUtils.Logger.e("Got " + status + " loading friends");
					PlayGamesHelperObject.RunOnGameThread(delegate
					{
						callback(false);
					});
				}
			});
		}

		// Token: 0x060047FD RID: 18429 RVA: 0x00147894 File Offset: 0x00145C94
		public IUserProfile[] GetFriends()
		{
			if (this.mFriends == null && !this.friendsLoading)
			{
				GooglePlayGames.OurUtils.Logger.w("Getting friends before they are loaded!!!");
				this.friendsLoading = true;
				this.LoadFriends(delegate(bool ok)
				{
					GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
					{
						"loading: ",
						ok,
						" mFriends = ",
						this.mFriends
					}));
					if (!ok)
					{
						GooglePlayGames.OurUtils.Logger.e("Friends list did not load successfully.  Disabling loading until re-authenticated");
					}
					this.friendsLoading = !ok;
				});
			}
			return (this.mFriends != null) ? this.mFriends.ToArray() : new IUserProfile[0];
		}

		// Token: 0x060047FE RID: 18430 RVA: 0x00147908 File Offset: 0x00145D08
		private void PopulateAchievements(uint authGeneration, GooglePlayGames.Native.PInvoke.AchievementManager.FetchAllResponse response)
		{
			if (authGeneration != this.mAuthGeneration)
			{
				GooglePlayGames.OurUtils.Logger.d("Received achievement callback after signout occurred, ignoring");
				return;
			}
			GooglePlayGames.OurUtils.Logger.d("Populating Achievements, status = " + response.Status());
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				if (response.Status() != CommonErrorStatus.ResponseStatus.VALID && response.Status() != CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
				{
					GooglePlayGames.OurUtils.Logger.e("Error retrieving achievements - check the log for more information. Failing signin.");
					Action<bool, string> action = this.mPendingAuthCallbacks;
					this.mPendingAuthCallbacks = null;
					if (action != null)
					{
						NativeClient.InvokeCallbackOnGameThread<bool, string>(action, false, "Cannot load achievements, Authenication failing");
					}
					this.SignOut();
					return;
				}
				Dictionary<string, GooglePlayGames.BasicApi.Achievement> dictionary = new Dictionary<string, GooglePlayGames.BasicApi.Achievement>();
				foreach (NativeAchievement nativeAchievement in response)
				{
					using (nativeAchievement)
					{
						dictionary[nativeAchievement.Id()] = nativeAchievement.AsAchievement();
					}
				}
				GooglePlayGames.OurUtils.Logger.d("Found " + dictionary.Count + " Achievements");
				this.mAchievements = dictionary;
			}
			GooglePlayGames.OurUtils.Logger.d("Maybe finish for Achievements");
			this.MaybeFinishAuthentication();
		}

		// Token: 0x060047FF RID: 18431 RVA: 0x00147A78 File Offset: 0x00145E78
		private void MaybeFinishAuthentication()
		{
			Action<bool, string> action = null;
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				if (this.mUser == null || this.mAchievements == null)
				{
					GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
					{
						"Auth not finished. User=",
						this.mUser,
						" achievements=",
						this.mAchievements
					}));
					return;
				}
				GooglePlayGames.OurUtils.Logger.d("Auth finished. Proceeding.");
				action = this.mPendingAuthCallbacks;
				this.mPendingAuthCallbacks = null;
				this.mAuthState = NativeClient.AuthState.Authenticated;
			}
			if (action != null)
			{
				GooglePlayGames.OurUtils.Logger.d("Invoking Callbacks: " + action);
				NativeClient.InvokeCallbackOnGameThread<bool, string>(action, true, null);
			}
		}

		// Token: 0x06004800 RID: 18432 RVA: 0x00147B48 File Offset: 0x00145F48
		private void PopulateUser(uint authGeneration, GooglePlayGames.Native.PInvoke.PlayerManager.FetchSelfResponse response)
		{
			GooglePlayGames.OurUtils.Logger.d("Populating User");
			if (authGeneration != this.mAuthGeneration)
			{
				GooglePlayGames.OurUtils.Logger.d("Received user callback after signout occurred, ignoring");
				return;
			}
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				if (response.Status() != CommonErrorStatus.ResponseStatus.VALID && response.Status() != CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
				{
					GooglePlayGames.OurUtils.Logger.e("Error retrieving user, signing out");
					Action<bool, string> action = this.mPendingAuthCallbacks;
					this.mPendingAuthCallbacks = null;
					if (action != null)
					{
						NativeClient.InvokeCallbackOnGameThread<bool, string>(action, false, "Cannot load user profile");
					}
					this.SignOut();
					return;
				}
				this.mUser = response.Self().AsPlayer();
				this.mFriends = null;
			}
			GooglePlayGames.OurUtils.Logger.d("Found User: " + this.mUser);
			GooglePlayGames.OurUtils.Logger.d("Maybe finish for User");
			this.MaybeFinishAuthentication();
		}

		// Token: 0x06004801 RID: 18433 RVA: 0x00147C38 File Offset: 0x00146038
		private void HandleAuthTransition(GooglePlayGames.Native.Cwrapper.Types.AuthOperation operation, CommonErrorStatus.AuthStatus status)
		{
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"Starting Auth Transition. Op: ",
				operation,
				" status: ",
				status
			}));
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				if (operation != GooglePlayGames.Native.Cwrapper.Types.AuthOperation.SIGN_IN)
				{
					if (operation != GooglePlayGames.Native.Cwrapper.Types.AuthOperation.SIGN_OUT)
					{
						GooglePlayGames.OurUtils.Logger.e("Unknown AuthOperation " + operation);
					}
					else
					{
						this.ToUnauthenticated();
					}
				}
				else if (status == CommonErrorStatus.AuthStatus.VALID)
				{
					if (this.mSilentAuthCallbacks != null)
					{
						this.mPendingAuthCallbacks = (Action<bool, string>)Delegate.Combine(this.mPendingAuthCallbacks, this.mSilentAuthCallbacks);
						this.mSilentAuthCallbacks = null;
					}
					uint currentAuthGeneration = this.mAuthGeneration;
					this.mServices.AchievementManager().FetchAll(delegate(GooglePlayGames.Native.PInvoke.AchievementManager.FetchAllResponse results)
					{
						this.PopulateAchievements(currentAuthGeneration, results);
					});
					this.mServices.PlayerManager().FetchSelf(delegate(GooglePlayGames.Native.PInvoke.PlayerManager.FetchSelfResponse results)
					{
						this.PopulateUser(currentAuthGeneration, results);
					});
				}
				else if (this.mAuthState == NativeClient.AuthState.SilentPending)
				{
					this.mSilentAuthFailed = true;
					this.mAuthState = NativeClient.AuthState.Unauthenticated;
					Action<bool, string> callback = this.mSilentAuthCallbacks;
					this.mSilentAuthCallbacks = null;
					GooglePlayGames.OurUtils.Logger.d("Invoking callbacks, AuthState changed from silentPending to Unauthenticated.");
					NativeClient.InvokeCallbackOnGameThread<bool, string>(callback, false, "silent auth failed");
					if (this.mPendingAuthCallbacks != null)
					{
						GooglePlayGames.OurUtils.Logger.d("there are pending auth callbacks - starting AuthUI");
						this.GameServices().StartAuthorizationUI();
					}
				}
				else
				{
					GooglePlayGames.OurUtils.Logger.d("AuthState == " + this.mAuthState + " calling auth callbacks with failure");
					this.UnpauseUnityPlayer();
					Action<bool, string> callback2 = this.mPendingAuthCallbacks;
					this.mPendingAuthCallbacks = null;
					NativeClient.InvokeCallbackOnGameThread<bool, string>(callback2, false, "Authentication failed");
				}
			}
		}

		// Token: 0x06004802 RID: 18434 RVA: 0x00147E34 File Offset: 0x00146234
		private void UnpauseUnityPlayer()
		{
		}

		// Token: 0x06004803 RID: 18435 RVA: 0x00147E38 File Offset: 0x00146238
		private void ToUnauthenticated()
		{
			object authStateLock = this.AuthStateLock;
			lock (authStateLock)
			{
				this.mUser = null;
				this.mFriends = null;
				this.mAchievements = null;
				this.mAuthState = NativeClient.AuthState.Unauthenticated;
				this.mTokenClient = this.clientImpl.CreateTokenClient(true);
				this.mAuthGeneration += 1U;
			}
		}

		// Token: 0x06004804 RID: 18436 RVA: 0x00147EB8 File Offset: 0x001462B8
		public void SignOut()
		{
			this.ToUnauthenticated();
			if (this.GameServices() == null)
			{
				return;
			}
			this.mTokenClient.Signout();
			this.GameServices().SignOut();
		}

		// Token: 0x06004805 RID: 18437 RVA: 0x00147EE4 File Offset: 0x001462E4
		public string GetUserId()
		{
			if (this.mUser == null)
			{
				return null;
			}
			return this.mUser.id;
		}

		// Token: 0x06004806 RID: 18438 RVA: 0x00147F02 File Offset: 0x00146302
		public string GetUserDisplayName()
		{
			if (this.mUser == null)
			{
				return null;
			}
			return this.mUser.userName;
		}

		// Token: 0x06004807 RID: 18439 RVA: 0x00147F20 File Offset: 0x00146320
		public string GetUserImageUrl()
		{
			if (this.mUser == null)
			{
				return null;
			}
			return this.mUser.AvatarURL;
		}

		// Token: 0x06004808 RID: 18440 RVA: 0x00147F40 File Offset: 0x00146340
		public void SetGravityForPopups(Gravity gravity)
		{
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				this.clientImpl.SetGravityForPopups(this.GetApiClient(), gravity);
			});
		}

		// Token: 0x06004809 RID: 18441 RVA: 0x00147F74 File Offset: 0x00146374
		public void GetPlayerStats(Action<CommonStatusCodes, GooglePlayGames.BasicApi.PlayerStats> callback)
		{
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				this.clientImpl.GetPlayerStats(this.GetApiClient(), callback);
			});
		}

		// Token: 0x0600480A RID: 18442 RVA: 0x00147FA8 File Offset: 0x001463A8
		public void LoadUsers(string[] userIds, Action<IUserProfile[]> callback)
		{
			this.mServices.PlayerManager().FetchList(userIds, delegate(NativePlayer[] nativeUsers)
			{
				IUserProfile[] users = new IUserProfile[nativeUsers.Length];
				for (int i = 0; i < users.Length; i++)
				{
					users[i] = nativeUsers[i].AsPlayer();
				}
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					callback(users);
				});
			});
		}

		// Token: 0x0600480B RID: 18443 RVA: 0x00147FDF File Offset: 0x001463DF
		public GooglePlayGames.BasicApi.Achievement GetAchievement(string achId)
		{
			if (this.mAchievements == null || !this.mAchievements.ContainsKey(achId))
			{
				return null;
			}
			return this.mAchievements[achId];
		}

		// Token: 0x0600480C RID: 18444 RVA: 0x00148014 File Offset: 0x00146414
		public void LoadAchievements(Action<GooglePlayGames.BasicApi.Achievement[]> callback)
		{
			GooglePlayGames.BasicApi.Achievement[] data = new GooglePlayGames.BasicApi.Achievement[this.mAchievements.Count];
			this.mAchievements.Values.CopyTo(data, 0);
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				callback(data);
			});
		}

		// Token: 0x0600480D RID: 18445 RVA: 0x00148070 File Offset: 0x00146470
		public void UnlockAchievement(string achId, Action<bool> callback)
		{
			this.UpdateAchievement("Unlock", achId, callback, (GooglePlayGames.BasicApi.Achievement a) => a.IsUnlocked, delegate(GooglePlayGames.BasicApi.Achievement a)
			{
				a.IsUnlocked = true;
				this.GameServices().AchievementManager().Unlock(achId);
			});
		}

		// Token: 0x0600480E RID: 18446 RVA: 0x001480CC File Offset: 0x001464CC
		public void RevealAchievement(string achId, Action<bool> callback)
		{
			this.UpdateAchievement("Reveal", achId, callback, (GooglePlayGames.BasicApi.Achievement a) => a.IsRevealed, delegate(GooglePlayGames.BasicApi.Achievement a)
			{
				a.IsRevealed = true;
				this.GameServices().AchievementManager().Reveal(achId);
			});
		}

		// Token: 0x0600480F RID: 18447 RVA: 0x00148128 File Offset: 0x00146528
		private void UpdateAchievement(string updateType, string achId, Action<bool> callback, Predicate<GooglePlayGames.BasicApi.Achievement> alreadyDone, Action<GooglePlayGames.BasicApi.Achievement> updateAchievment)
		{
			callback = NativeClient.AsOnGameThreadCallback<bool>(callback);
			Misc.CheckNotNull<string>(achId);
			this.InitializeGameServices();
			GooglePlayGames.BasicApi.Achievement achievement = this.GetAchievement(achId);
			if (achievement == null)
			{
				GooglePlayGames.OurUtils.Logger.d("Could not " + updateType + ", no achievement with ID " + achId);
				callback(false);
				return;
			}
			if (alreadyDone(achievement))
			{
				GooglePlayGames.OurUtils.Logger.d("Did not need to perform " + updateType + ": on achievement " + achId);
				callback(true);
				return;
			}
			GooglePlayGames.OurUtils.Logger.d("Performing " + updateType + " on " + achId);
			updateAchievment(achievement);
			this.GameServices().AchievementManager().Fetch(achId, delegate(GooglePlayGames.Native.PInvoke.AchievementManager.FetchResponse rsp)
			{
				if (rsp.Status() == CommonErrorStatus.ResponseStatus.VALID)
				{
					this.mAchievements.Remove(achId);
					this.mAchievements.Add(achId, rsp.Achievement().AsAchievement());
					callback(true);
				}
				else
				{
					GooglePlayGames.OurUtils.Logger.e(string.Concat(new object[]
					{
						"Cannot refresh achievement ",
						achId,
						": ",
						rsp.Status()
					}));
					callback(false);
				}
			});
		}

		// Token: 0x06004810 RID: 18448 RVA: 0x00148228 File Offset: 0x00146628
		public void IncrementAchievement(string achId, int steps, Action<bool> callback)
		{
			Misc.CheckNotNull<string>(achId);
			callback = NativeClient.AsOnGameThreadCallback<bool>(callback);
			this.InitializeGameServices();
			GooglePlayGames.BasicApi.Achievement achievement = this.GetAchievement(achId);
			if (achievement == null)
			{
				GooglePlayGames.OurUtils.Logger.e("Could not increment, no achievement with ID " + achId);
				callback(false);
				return;
			}
			if (!achievement.IsIncremental)
			{
				GooglePlayGames.OurUtils.Logger.e("Could not increment, achievement with ID " + achId + " was not incremental");
				callback(false);
				return;
			}
			if (steps < 0)
			{
				GooglePlayGames.OurUtils.Logger.e("Attempted to increment by negative steps");
				callback(false);
				return;
			}
			this.GameServices().AchievementManager().Increment(achId, Convert.ToUInt32(steps));
			this.GameServices().AchievementManager().Fetch(achId, delegate(GooglePlayGames.Native.PInvoke.AchievementManager.FetchResponse rsp)
			{
				if (rsp.Status() == CommonErrorStatus.ResponseStatus.VALID)
				{
					this.mAchievements.Remove(achId);
					this.mAchievements.Add(achId, rsp.Achievement().AsAchievement());
					callback(true);
				}
				else
				{
					GooglePlayGames.OurUtils.Logger.e(string.Concat(new object[]
					{
						"Cannot refresh achievement ",
						achId,
						": ",
						rsp.Status()
					}));
					callback(false);
				}
			});
		}

		// Token: 0x06004811 RID: 18449 RVA: 0x00148338 File Offset: 0x00146738
		public void SetStepsAtLeast(string achId, int steps, Action<bool> callback)
		{
			Misc.CheckNotNull<string>(achId);
			callback = NativeClient.AsOnGameThreadCallback<bool>(callback);
			this.InitializeGameServices();
			GooglePlayGames.BasicApi.Achievement achievement = this.GetAchievement(achId);
			if (achievement == null)
			{
				GooglePlayGames.OurUtils.Logger.e("Could not increment, no achievement with ID " + achId);
				callback(false);
				return;
			}
			if (!achievement.IsIncremental)
			{
				GooglePlayGames.OurUtils.Logger.e("Could not increment, achievement with ID " + achId + " is not incremental");
				callback(false);
				return;
			}
			if (steps < 0)
			{
				GooglePlayGames.OurUtils.Logger.e("Attempted to increment by negative steps");
				callback(false);
				return;
			}
			this.GameServices().AchievementManager().SetStepsAtLeast(achId, Convert.ToUInt32(steps));
			this.GameServices().AchievementManager().Fetch(achId, delegate(GooglePlayGames.Native.PInvoke.AchievementManager.FetchResponse rsp)
			{
				if (rsp.Status() == CommonErrorStatus.ResponseStatus.VALID)
				{
					this.mAchievements.Remove(achId);
					this.mAchievements.Add(achId, rsp.Achievement().AsAchievement());
					callback(true);
				}
				else
				{
					GooglePlayGames.OurUtils.Logger.e(string.Concat(new object[]
					{
						"Cannot refresh achievement ",
						achId,
						": ",
						rsp.Status()
					}));
					callback(false);
				}
			});
		}

		// Token: 0x06004812 RID: 18450 RVA: 0x00148448 File Offset: 0x00146848
		public void ShowAchievementsUI(Action<UIStatus> cb)
		{
			if (!this.IsAuthenticated())
			{
				return;
			}
			Action<CommonErrorStatus.UIStatus> callback = Callbacks.NoopUICallback;
			if (cb != null)
			{
				callback = delegate(CommonErrorStatus.UIStatus result)
				{
					cb((UIStatus)result);
				};
			}
			callback = NativeClient.AsOnGameThreadCallback<CommonErrorStatus.UIStatus>(callback);
			this.GameServices().AchievementManager().ShowAllUI(callback);
		}

		// Token: 0x06004813 RID: 18451 RVA: 0x001484A4 File Offset: 0x001468A4
		public int LeaderboardMaxResults()
		{
			return this.GameServices().LeaderboardManager().LeaderboardMaxResults;
		}

		// Token: 0x06004814 RID: 18452 RVA: 0x001484B8 File Offset: 0x001468B8
		public void ShowLeaderboardUI(string leaderboardId, LeaderboardTimeSpan span, Action<UIStatus> cb)
		{
			if (!this.IsAuthenticated())
			{
				return;
			}
			Action<CommonErrorStatus.UIStatus> callback = Callbacks.NoopUICallback;
			if (cb != null)
			{
				callback = delegate(CommonErrorStatus.UIStatus result)
				{
					cb((UIStatus)result);
				};
			}
			callback = NativeClient.AsOnGameThreadCallback<CommonErrorStatus.UIStatus>(callback);
			if (leaderboardId == null)
			{
				this.GameServices().LeaderboardManager().ShowAllUI(callback);
			}
			else
			{
				this.GameServices().LeaderboardManager().ShowUI(leaderboardId, span, callback);
			}
		}

		// Token: 0x06004815 RID: 18453 RVA: 0x00148532 File Offset: 0x00146932
		public void LoadScores(string leaderboardId, LeaderboardStart start, int rowCount, LeaderboardCollection collection, LeaderboardTimeSpan timeSpan, Action<LeaderboardScoreData> callback)
		{
			callback = NativeClient.AsOnGameThreadCallback<LeaderboardScoreData>(callback);
			this.GameServices().LeaderboardManager().LoadLeaderboardData(leaderboardId, start, rowCount, collection, timeSpan, this.mUser.id, callback);
		}

		// Token: 0x06004816 RID: 18454 RVA: 0x00148563 File Offset: 0x00146963
		public void LoadMoreScores(ScorePageToken token, int rowCount, Action<LeaderboardScoreData> callback)
		{
			callback = NativeClient.AsOnGameThreadCallback<LeaderboardScoreData>(callback);
			this.GameServices().LeaderboardManager().LoadScorePage(null, rowCount, token, callback);
		}

		// Token: 0x06004817 RID: 18455 RVA: 0x00148584 File Offset: 0x00146984
		public void SubmitScore(string leaderboardId, long score, Action<bool> callback)
		{
			callback = NativeClient.AsOnGameThreadCallback<bool>(callback);
			if (!this.IsAuthenticated())
			{
				callback(false);
			}
			this.InitializeGameServices();
			if (leaderboardId == null)
			{
				throw new ArgumentNullException("leaderboardId");
			}
			this.GameServices().LeaderboardManager().SubmitScore(leaderboardId, score, null);
			callback(true);
		}

		// Token: 0x06004818 RID: 18456 RVA: 0x001485DC File Offset: 0x001469DC
		public void SubmitScore(string leaderboardId, long score, string metadata, Action<bool> callback)
		{
			callback = NativeClient.AsOnGameThreadCallback<bool>(callback);
			if (!this.IsAuthenticated())
			{
				callback(false);
			}
			this.InitializeGameServices();
			if (leaderboardId == null)
			{
				throw new ArgumentNullException("leaderboardId");
			}
			this.GameServices().LeaderboardManager().SubmitScore(leaderboardId, score, metadata);
			callback(true);
		}

		// Token: 0x06004819 RID: 18457 RVA: 0x00148638 File Offset: 0x00146A38
		public IRealTimeMultiplayerClient GetRtmpClient()
		{
			if (!this.IsAuthenticated())
			{
				return null;
			}
			object gameServicesLock = this.GameServicesLock;
			IRealTimeMultiplayerClient result;
			lock (gameServicesLock)
			{
				result = this.mRealTimeClient;
			}
			return result;
		}

		// Token: 0x0600481A RID: 18458 RVA: 0x00148688 File Offset: 0x00146A88
		public ITurnBasedMultiplayerClient GetTbmpClient()
		{
			object gameServicesLock = this.GameServicesLock;
			ITurnBasedMultiplayerClient result;
			lock (gameServicesLock)
			{
				result = this.mTurnBasedClient;
			}
			return result;
		}

		// Token: 0x0600481B RID: 18459 RVA: 0x001486C8 File Offset: 0x00146AC8
		public ISavedGameClient GetSavedGameClient()
		{
			object gameServicesLock = this.GameServicesLock;
			ISavedGameClient result;
			lock (gameServicesLock)
			{
				result = this.mSavedGameClient;
			}
			return result;
		}

		// Token: 0x0600481C RID: 18460 RVA: 0x00148708 File Offset: 0x00146B08
		public IEventsClient GetEventsClient()
		{
			object gameServicesLock = this.GameServicesLock;
			IEventsClient result;
			lock (gameServicesLock)
			{
				result = this.mEventsClient;
			}
			return result;
		}

		// Token: 0x0600481D RID: 18461 RVA: 0x00148748 File Offset: 0x00146B48
		[Obsolete("Quests are being removed in 2018.")]
		public IQuestsClient GetQuestsClient()
		{
			object gameServicesLock = this.GameServicesLock;
			IQuestsClient result;
			lock (gameServicesLock)
			{
				result = this.mQuestsClient;
			}
			return result;
		}

		// Token: 0x0600481E RID: 18462 RVA: 0x00148788 File Offset: 0x00146B88
		public IVideoClient GetVideoClient()
		{
			object gameServicesLock = this.GameServicesLock;
			IVideoClient result;
			lock (gameServicesLock)
			{
				result = this.mVideoClient;
			}
			return result;
		}

		// Token: 0x0600481F RID: 18463 RVA: 0x001487C8 File Offset: 0x00146BC8
		public void RegisterInvitationDelegate(InvitationReceivedDelegate invitationDelegate)
		{
			if (invitationDelegate == null)
			{
				this.mInvitationDelegate = null;
			}
			else
			{
				this.mInvitationDelegate = Callbacks.AsOnGameThreadCallback<Invitation, bool>(delegate(Invitation invitation, bool autoAccept)
				{
					invitationDelegate(invitation, autoAccept);
				});
			}
		}

		// Token: 0x06004820 RID: 18464 RVA: 0x00148814 File Offset: 0x00146C14
		public IntPtr GetApiClient()
		{
			return InternalHooks.InternalHooks_GetApiClient(this.mServices.AsHandle());
		}

		// Token: 0x04004C7C RID: 19580
		private readonly IClientImpl clientImpl;

		// Token: 0x04004C7D RID: 19581
		private readonly object GameServicesLock = new object();

		// Token: 0x04004C7E RID: 19582
		private readonly object AuthStateLock = new object();

		// Token: 0x04004C7F RID: 19583
		private readonly PlayGamesClientConfiguration mConfiguration;

		// Token: 0x04004C80 RID: 19584
		private GooglePlayGames.Native.PInvoke.GameServices mServices;

		// Token: 0x04004C81 RID: 19585
		private volatile NativeTurnBasedMultiplayerClient mTurnBasedClient;

		// Token: 0x04004C82 RID: 19586
		private volatile NativeRealtimeMultiplayerClient mRealTimeClient;

		// Token: 0x04004C83 RID: 19587
		private volatile ISavedGameClient mSavedGameClient;

		// Token: 0x04004C84 RID: 19588
		private volatile IEventsClient mEventsClient;

		// Token: 0x04004C85 RID: 19589
		[Obsolete("Quests are being removed in 2018.")]
		private volatile IQuestsClient mQuestsClient;

		// Token: 0x04004C86 RID: 19590
		private volatile IVideoClient mVideoClient;

		// Token: 0x04004C87 RID: 19591
		private volatile TokenClient mTokenClient;

		// Token: 0x04004C88 RID: 19592
		private volatile Action<Invitation, bool> mInvitationDelegate;

		// Token: 0x04004C89 RID: 19593
		private volatile Dictionary<string, GooglePlayGames.BasicApi.Achievement> mAchievements;

		// Token: 0x04004C8A RID: 19594
		private volatile GooglePlayGames.BasicApi.Multiplayer.Player mUser;

		// Token: 0x04004C8B RID: 19595
		private volatile List<GooglePlayGames.BasicApi.Multiplayer.Player> mFriends;

		// Token: 0x04004C8C RID: 19596
		private volatile Action<bool, string> mPendingAuthCallbacks;

		// Token: 0x04004C8D RID: 19597
		private volatile Action<bool, string> mSilentAuthCallbacks;

		// Token: 0x04004C8E RID: 19598
		private volatile NativeClient.AuthState mAuthState;

		// Token: 0x04004C8F RID: 19599
		private volatile uint mAuthGeneration;

		// Token: 0x04004C90 RID: 19600
		private volatile bool mSilentAuthFailed;

		// Token: 0x04004C91 RID: 19601
		private volatile bool friendsLoading;

		// Token: 0x02000DF2 RID: 3570
		private enum AuthState
		{
			// Token: 0x04004C95 RID: 19605
			Unauthenticated,
			// Token: 0x04004C96 RID: 19606
			Authenticated,
			// Token: 0x04004C97 RID: 19607
			SilentPending
		}
	}
}
