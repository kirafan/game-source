﻿using System;
using System.Reflection;
using GooglePlayGames.OurUtils;
using UnityEngine;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF0 RID: 3568
	internal static class JavaUtils
	{
		// Token: 0x060047EB RID: 18411 RVA: 0x00146FDB File Offset: 0x001453DB
		internal static AndroidJavaObject JavaObjectFromPointer(IntPtr jobject)
		{
			if (jobject == IntPtr.Zero)
			{
				return null;
			}
			return (AndroidJavaObject)JavaUtils.IntPtrConstructor.Invoke(new object[]
			{
				jobject
			});
		}

		// Token: 0x060047EC RID: 18412 RVA: 0x00147010 File Offset: 0x00145410
		internal static AndroidJavaObject NullSafeCall(this AndroidJavaObject target, string methodName, params object[] args)
		{
			AndroidJavaObject result;
			try
			{
				result = target.Call<AndroidJavaObject>(methodName, args);
			}
			catch (Exception ex)
			{
				if (ex.Message.Contains("null"))
				{
					result = null;
				}
				else
				{
					GooglePlayGames.OurUtils.Logger.w("CallObjectMethod exception: " + ex);
					result = null;
				}
			}
			return result;
		}

		// Token: 0x04004C7B RID: 19579
		private static ConstructorInfo IntPtrConstructor = typeof(AndroidJavaObject).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[]
		{
			typeof(IntPtr)
		}, null);
	}
}
