﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000E06 RID: 3590
	internal class NativeSavedGameClient : ISavedGameClient
	{
		// Token: 0x060048E2 RID: 18658 RVA: 0x0014C138 File Offset: 0x0014A538
		internal NativeSavedGameClient(GooglePlayGames.Native.PInvoke.SnapshotManager manager)
		{
			this.mSnapshotManager = Misc.CheckNotNull<GooglePlayGames.Native.PInvoke.SnapshotManager>(manager);
		}

		// Token: 0x060048E3 RID: 18659 RVA: 0x0014C14C File Offset: 0x0014A54C
		public void OpenWithAutomaticConflictResolution(string filename, DataSource source, ConflictResolutionStrategy resolutionStrategy, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
		{
			Misc.CheckNotNull<string>(filename);
			Misc.CheckNotNull<Action<SavedGameRequestStatus, ISavedGameMetadata>>(callback);
			callback = NativeSavedGameClient.ToOnGameThread<SavedGameRequestStatus, ISavedGameMetadata>(callback);
			if (!NativeSavedGameClient.IsValidFilename(filename))
			{
				Logger.e("Received invalid filename: " + filename);
				callback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			this.OpenWithManualConflictResolution(filename, source, false, delegate(IConflictResolver resolver, ISavedGameMetadata original, byte[] originalData, ISavedGameMetadata unmerged, byte[] unmergedData)
			{
				switch (resolutionStrategy)
				{
				case ConflictResolutionStrategy.UseLongestPlaytime:
					if (original.TotalTimePlayed >= unmerged.TotalTimePlayed)
					{
						resolver.ChooseMetadata(original);
					}
					else
					{
						resolver.ChooseMetadata(unmerged);
					}
					return;
				case ConflictResolutionStrategy.UseOriginal:
					resolver.ChooseMetadata(original);
					return;
				case ConflictResolutionStrategy.UseUnmerged:
					resolver.ChooseMetadata(unmerged);
					return;
				default:
					Logger.e("Unhandled strategy " + resolutionStrategy);
					callback(SavedGameRequestStatus.InternalError, null);
					return;
				}
			}, callback);
		}

		// Token: 0x060048E4 RID: 18660 RVA: 0x0014C1D8 File Offset: 0x0014A5D8
		private ConflictCallback ToOnGameThread(ConflictCallback conflictCallback)
		{
			return delegate(IConflictResolver resolver, ISavedGameMetadata original, byte[] originalData, ISavedGameMetadata unmerged, byte[] unmergedData)
			{
				Logger.d("Invoking conflict callback");
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					conflictCallback(resolver, original, originalData, unmerged, unmergedData);
				});
			};
		}

		// Token: 0x060048E5 RID: 18661 RVA: 0x0014C200 File Offset: 0x0014A600
		public void OpenWithManualConflictResolution(string filename, DataSource source, bool prefetchDataOnConflict, ConflictCallback conflictCallback, Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback)
		{
			Misc.CheckNotNull<string>(filename);
			Misc.CheckNotNull<ConflictCallback>(conflictCallback);
			Misc.CheckNotNull<Action<SavedGameRequestStatus, ISavedGameMetadata>>(completedCallback);
			conflictCallback = this.ToOnGameThread(conflictCallback);
			completedCallback = NativeSavedGameClient.ToOnGameThread<SavedGameRequestStatus, ISavedGameMetadata>(completedCallback);
			if (!NativeSavedGameClient.IsValidFilename(filename))
			{
				Logger.e("Received invalid filename: " + filename);
				completedCallback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			this.InternalManualOpen(filename, source, prefetchDataOnConflict, conflictCallback, completedCallback);
		}

		// Token: 0x060048E6 RID: 18662 RVA: 0x0014C26C File Offset: 0x0014A66C
		private void InternalManualOpen(string filename, DataSource source, bool prefetchDataOnConflict, ConflictCallback conflictCallback, Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback)
		{
			NativeSavedGameClient.<InternalManualOpen>c__AnonStorey3 <InternalManualOpen>c__AnonStorey = new NativeSavedGameClient.<InternalManualOpen>c__AnonStorey3();
			<InternalManualOpen>c__AnonStorey.completedCallback = completedCallback;
			<InternalManualOpen>c__AnonStorey.filename = filename;
			<InternalManualOpen>c__AnonStorey.source = source;
			<InternalManualOpen>c__AnonStorey.prefetchDataOnConflict = prefetchDataOnConflict;
			<InternalManualOpen>c__AnonStorey.conflictCallback = conflictCallback;
			<InternalManualOpen>c__AnonStorey.$this = this;
			this.mSnapshotManager.Open(<InternalManualOpen>c__AnonStorey.filename, NativeSavedGameClient.AsDataSource(<InternalManualOpen>c__AnonStorey.source), Types.SnapshotConflictPolicy.MANUAL, delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.OpenResponse response)
			{
				if (!response.RequestSucceeded())
				{
					<InternalManualOpen>c__AnonStorey.completedCallback(NativeSavedGameClient.AsRequestStatus(response.ResponseStatus()), null);
				}
				else if (response.ResponseStatus() == CommonErrorStatus.SnapshotOpenStatus.VALID)
				{
					<InternalManualOpen>c__AnonStorey.completedCallback(SavedGameRequestStatus.Success, response.Data());
				}
				else if (response.ResponseStatus() == CommonErrorStatus.SnapshotOpenStatus.VALID_WITH_CONFLICT)
				{
					NativeSnapshotMetadata original = response.ConflictOriginal();
					NativeSnapshotMetadata unmerged = response.ConflictUnmerged();
					NativeSavedGameClient.NativeConflictResolver resolver = new NativeSavedGameClient.NativeConflictResolver(<InternalManualOpen>c__AnonStorey.$this.mSnapshotManager, response.ConflictId(), original, unmerged, <InternalManualOpen>c__AnonStorey.completedCallback, delegate()
					{
						<InternalManualOpen>c__AnonStorey.InternalManualOpen(<InternalManualOpen>c__AnonStorey.filename, <InternalManualOpen>c__AnonStorey.source, <InternalManualOpen>c__AnonStorey.prefetchDataOnConflict, <InternalManualOpen>c__AnonStorey.conflictCallback, <InternalManualOpen>c__AnonStorey.completedCallback);
					});
					if (!<InternalManualOpen>c__AnonStorey.prefetchDataOnConflict)
					{
						<InternalManualOpen>c__AnonStorey.conflictCallback(resolver, original, null, unmerged, null);
						return;
					}
					NativeSavedGameClient.Prefetcher @object = new NativeSavedGameClient.Prefetcher(delegate(byte[] originalData, byte[] unmergedData)
					{
						<InternalManualOpen>c__AnonStorey.conflictCallback(resolver, original, originalData, unmerged, unmergedData);
					}, <InternalManualOpen>c__AnonStorey.completedCallback);
					<InternalManualOpen>c__AnonStorey.$this.mSnapshotManager.Read(original, new Action<GooglePlayGames.Native.PInvoke.SnapshotManager.ReadResponse>(@object.OnOriginalDataRead));
					<InternalManualOpen>c__AnonStorey.$this.mSnapshotManager.Read(unmerged, new Action<GooglePlayGames.Native.PInvoke.SnapshotManager.ReadResponse>(@object.OnUnmergedDataRead));
				}
				else
				{
					Logger.e("Unhandled response status");
					<InternalManualOpen>c__AnonStorey.completedCallback(SavedGameRequestStatus.InternalError, null);
				}
			});
		}

		// Token: 0x060048E7 RID: 18663 RVA: 0x0014C2D4 File Offset: 0x0014A6D4
		public void ReadBinaryData(ISavedGameMetadata metadata, Action<SavedGameRequestStatus, byte[]> completedCallback)
		{
			Misc.CheckNotNull<ISavedGameMetadata>(metadata);
			Misc.CheckNotNull<Action<SavedGameRequestStatus, byte[]>>(completedCallback);
			completedCallback = NativeSavedGameClient.ToOnGameThread<SavedGameRequestStatus, byte[]>(completedCallback);
			NativeSnapshotMetadata nativeSnapshotMetadata = metadata as NativeSnapshotMetadata;
			if (nativeSnapshotMetadata == null)
			{
				Logger.e("Encountered metadata that was not generated by this ISavedGameClient");
				completedCallback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			if (!nativeSnapshotMetadata.IsOpen)
			{
				Logger.e("This method requires an open ISavedGameMetadata.");
				completedCallback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			this.mSnapshotManager.Read(nativeSnapshotMetadata, delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.ReadResponse response)
			{
				if (!response.RequestSucceeded())
				{
					completedCallback(NativeSavedGameClient.AsRequestStatus(response.ResponseStatus()), null);
				}
				else
				{
					completedCallback(SavedGameRequestStatus.Success, response.Data());
				}
			});
		}

		// Token: 0x060048E8 RID: 18664 RVA: 0x0014C374 File Offset: 0x0014A774
		public void ShowSelectSavedGameUI(string uiTitle, uint maxDisplayedSavedGames, bool showCreateSaveUI, bool showDeleteSaveUI, Action<SelectUIStatus, ISavedGameMetadata> callback)
		{
			Misc.CheckNotNull<string>(uiTitle);
			Misc.CheckNotNull<Action<SelectUIStatus, ISavedGameMetadata>>(callback);
			callback = NativeSavedGameClient.ToOnGameThread<SelectUIStatus, ISavedGameMetadata>(callback);
			if (maxDisplayedSavedGames <= 0U)
			{
				Logger.e("maxDisplayedSavedGames must be greater than 0");
				callback(SelectUIStatus.BadInputError, null);
				return;
			}
			this.mSnapshotManager.SnapshotSelectUI(showCreateSaveUI, showDeleteSaveUI, maxDisplayedSavedGames, uiTitle, delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.SnapshotSelectUIResponse response)
			{
				callback(NativeSavedGameClient.AsUIStatus(response.RequestStatus()), (!response.RequestSucceeded()) ? null : response.Data());
			});
		}

		// Token: 0x060048E9 RID: 18665 RVA: 0x0014C3F0 File Offset: 0x0014A7F0
		public void CommitUpdate(ISavedGameMetadata metadata, SavedGameMetadataUpdate updateForMetadata, byte[] updatedBinaryData, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
		{
			Misc.CheckNotNull<ISavedGameMetadata>(metadata);
			Misc.CheckNotNull<byte[]>(updatedBinaryData);
			Misc.CheckNotNull<Action<SavedGameRequestStatus, ISavedGameMetadata>>(callback);
			callback = NativeSavedGameClient.ToOnGameThread<SavedGameRequestStatus, ISavedGameMetadata>(callback);
			NativeSnapshotMetadata nativeSnapshotMetadata = metadata as NativeSnapshotMetadata;
			if (nativeSnapshotMetadata == null)
			{
				Logger.e("Encountered metadata that was not generated by this ISavedGameClient");
				callback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			if (!nativeSnapshotMetadata.IsOpen)
			{
				Logger.e("This method requires an open ISavedGameMetadata.");
				callback(SavedGameRequestStatus.BadInputError, null);
				return;
			}
			this.mSnapshotManager.Commit(nativeSnapshotMetadata, NativeSavedGameClient.AsMetadataChange(updateForMetadata), updatedBinaryData, delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.CommitResponse response)
			{
				if (!response.RequestSucceeded())
				{
					callback(NativeSavedGameClient.AsRequestStatus(response.ResponseStatus()), null);
				}
				else
				{
					callback(SavedGameRequestStatus.Success, response.Data());
				}
			});
		}

		// Token: 0x060048EA RID: 18666 RVA: 0x0014C4A0 File Offset: 0x0014A8A0
		public void FetchAllSavedGames(DataSource source, Action<SavedGameRequestStatus, List<ISavedGameMetadata>> callback)
		{
			Misc.CheckNotNull<Action<SavedGameRequestStatus, List<ISavedGameMetadata>>>(callback);
			callback = NativeSavedGameClient.ToOnGameThread<SavedGameRequestStatus, List<ISavedGameMetadata>>(callback);
			this.mSnapshotManager.FetchAll(NativeSavedGameClient.AsDataSource(source), delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.FetchAllResponse response)
			{
				if (!response.RequestSucceeded())
				{
					callback(NativeSavedGameClient.AsRequestStatus(response.ResponseStatus()), new List<ISavedGameMetadata>());
				}
				else
				{
					callback(SavedGameRequestStatus.Success, response.Data().Cast<ISavedGameMetadata>().ToList<ISavedGameMetadata>());
				}
			});
		}

		// Token: 0x060048EB RID: 18667 RVA: 0x0014C4F4 File Offset: 0x0014A8F4
		public void Delete(ISavedGameMetadata metadata)
		{
			Misc.CheckNotNull<ISavedGameMetadata>(metadata);
			this.mSnapshotManager.Delete((NativeSnapshotMetadata)metadata);
		}

		// Token: 0x060048EC RID: 18668 RVA: 0x0014C50E File Offset: 0x0014A90E
		internal static bool IsValidFilename(string filename)
		{
			return filename != null && NativeSavedGameClient.ValidFilenameRegex.IsMatch(filename);
		}

		// Token: 0x060048ED RID: 18669 RVA: 0x0014C523 File Offset: 0x0014A923
		private static Types.SnapshotConflictPolicy AsConflictPolicy(ConflictResolutionStrategy strategy)
		{
			switch (strategy)
			{
			case ConflictResolutionStrategy.UseLongestPlaytime:
				return Types.SnapshotConflictPolicy.LONGEST_PLAYTIME;
			case ConflictResolutionStrategy.UseOriginal:
				return Types.SnapshotConflictPolicy.LAST_KNOWN_GOOD;
			case ConflictResolutionStrategy.UseUnmerged:
				return Types.SnapshotConflictPolicy.MOST_RECENTLY_MODIFIED;
			default:
				throw new InvalidOperationException("Found unhandled strategy: " + strategy);
			}
		}

		// Token: 0x060048EE RID: 18670 RVA: 0x0014C557 File Offset: 0x0014A957
		private static SavedGameRequestStatus AsRequestStatus(CommonErrorStatus.SnapshotOpenStatus status)
		{
			switch (status + 5)
			{
			case (CommonErrorStatus.SnapshotOpenStatus)0:
				return SavedGameRequestStatus.TimeoutError;
			default:
				if (status != CommonErrorStatus.SnapshotOpenStatus.VALID)
				{
					Logger.e("Encountered unknown status: " + status);
					return SavedGameRequestStatus.InternalError;
				}
				return SavedGameRequestStatus.Success;
			case (CommonErrorStatus.SnapshotOpenStatus)2:
				return SavedGameRequestStatus.AuthenticationError;
			}
		}

		// Token: 0x060048EF RID: 18671 RVA: 0x0014C597 File Offset: 0x0014A997
		private static Types.DataSource AsDataSource(DataSource source)
		{
			if (source == DataSource.ReadCacheOrNetwork)
			{
				return Types.DataSource.CACHE_OR_NETWORK;
			}
			if (source != DataSource.ReadNetworkOnly)
			{
				throw new InvalidOperationException("Found unhandled DataSource: " + source);
			}
			return Types.DataSource.NETWORK_ONLY;
		}

		// Token: 0x060048F0 RID: 18672 RVA: 0x0014C5C4 File Offset: 0x0014A9C4
		private static SavedGameRequestStatus AsRequestStatus(CommonErrorStatus.ResponseStatus status)
		{
			switch (status + 5)
			{
			case (CommonErrorStatus.ResponseStatus)0:
				return SavedGameRequestStatus.TimeoutError;
			case CommonErrorStatus.ResponseStatus.VALID_BUT_STALE:
				Logger.e("User was not authorized (they were probably not logged in).");
				return SavedGameRequestStatus.AuthenticationError;
			case (CommonErrorStatus.ResponseStatus)3:
				return SavedGameRequestStatus.InternalError;
			case (CommonErrorStatus.ResponseStatus)4:
				Logger.e("User attempted to use the game without a valid license.");
				return SavedGameRequestStatus.AuthenticationError;
			case (CommonErrorStatus.ResponseStatus)6:
			case (CommonErrorStatus.ResponseStatus)7:
				return SavedGameRequestStatus.Success;
			}
			Logger.e("Unknown status: " + status);
			return SavedGameRequestStatus.InternalError;
		}

		// Token: 0x060048F1 RID: 18673 RVA: 0x0014C638 File Offset: 0x0014AA38
		private static SelectUIStatus AsUIStatus(CommonErrorStatus.UIStatus uiStatus)
		{
			switch (uiStatus + 6)
			{
			case (CommonErrorStatus.UIStatus)0:
				return SelectUIStatus.UserClosedUI;
			case CommonErrorStatus.UIStatus.VALID:
				return SelectUIStatus.TimeoutError;
			case (CommonErrorStatus.UIStatus)3:
				return SelectUIStatus.AuthenticationError;
			case (CommonErrorStatus.UIStatus)4:
				return SelectUIStatus.InternalError;
			case (CommonErrorStatus.UIStatus)7:
				return SelectUIStatus.SavedGameSelected;
			}
			Logger.e("Encountered unknown UI Status: " + uiStatus);
			return SelectUIStatus.InternalError;
		}

		// Token: 0x060048F2 RID: 18674 RVA: 0x0014C694 File Offset: 0x0014AA94
		private static NativeSnapshotMetadataChange AsMetadataChange(SavedGameMetadataUpdate update)
		{
			NativeSnapshotMetadataChange.Builder builder = new NativeSnapshotMetadataChange.Builder();
			if (update.IsCoverImageUpdated)
			{
				builder.SetCoverImageFromPngData(update.UpdatedPngCoverImage);
			}
			if (update.IsDescriptionUpdated)
			{
				builder.SetDescription(update.UpdatedDescription);
			}
			if (update.IsPlayedTimeUpdated)
			{
				builder.SetPlayedTime((ulong)update.UpdatedPlayedTime.Value.TotalMilliseconds);
			}
			return builder.Build();
		}

		// Token: 0x060048F3 RID: 18675 RVA: 0x0014C70C File Offset: 0x0014AB0C
		private static Action<T1, T2> ToOnGameThread<T1, T2>(Action<T1, T2> toConvert)
		{
			return delegate(T1 val1, T2 val2)
			{
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toConvert(val1, val2);
				});
			};
		}

		// Token: 0x04004CC7 RID: 19655
		private static readonly Regex ValidFilenameRegex = new Regex("\\A[a-zA-Z0-9-._~]{1,100}\\Z");

		// Token: 0x04004CC8 RID: 19656
		private readonly GooglePlayGames.Native.PInvoke.SnapshotManager mSnapshotManager;

		// Token: 0x02000E07 RID: 3591
		private class NativeConflictResolver : IConflictResolver
		{
			// Token: 0x060048F5 RID: 18677 RVA: 0x0014C744 File Offset: 0x0014AB44
			internal NativeConflictResolver(GooglePlayGames.Native.PInvoke.SnapshotManager manager, string conflictId, NativeSnapshotMetadata original, NativeSnapshotMetadata unmerged, Action<SavedGameRequestStatus, ISavedGameMetadata> completeCallback, Action retryOpen)
			{
				this.mManager = Misc.CheckNotNull<GooglePlayGames.Native.PInvoke.SnapshotManager>(manager);
				this.mConflictId = Misc.CheckNotNull<string>(conflictId);
				this.mOriginal = Misc.CheckNotNull<NativeSnapshotMetadata>(original);
				this.mUnmerged = Misc.CheckNotNull<NativeSnapshotMetadata>(unmerged);
				this.mCompleteCallback = Misc.CheckNotNull<Action<SavedGameRequestStatus, ISavedGameMetadata>>(completeCallback);
				this.mRetryFileOpen = Misc.CheckNotNull<Action>(retryOpen);
			}

			// Token: 0x060048F6 RID: 18678 RVA: 0x0014C7A4 File Offset: 0x0014ABA4
			public void ChooseMetadata(ISavedGameMetadata chosenMetadata)
			{
				NativeSnapshotMetadata nativeSnapshotMetadata = chosenMetadata as NativeSnapshotMetadata;
				if (nativeSnapshotMetadata != this.mOriginal && nativeSnapshotMetadata != this.mUnmerged)
				{
					Logger.e("Caller attempted to choose a version of the metadata that was not part of the conflict");
					this.mCompleteCallback(SavedGameRequestStatus.BadInputError, null);
					return;
				}
				this.mManager.Resolve(nativeSnapshotMetadata, new NativeSnapshotMetadataChange.Builder().Build(), this.mConflictId, delegate(GooglePlayGames.Native.PInvoke.SnapshotManager.CommitResponse response)
				{
					if (!response.RequestSucceeded())
					{
						this.mCompleteCallback(NativeSavedGameClient.AsRequestStatus(response.ResponseStatus()), null);
						return;
					}
					this.mRetryFileOpen();
				});
			}

			// Token: 0x04004CC9 RID: 19657
			private readonly GooglePlayGames.Native.PInvoke.SnapshotManager mManager;

			// Token: 0x04004CCA RID: 19658
			private readonly string mConflictId;

			// Token: 0x04004CCB RID: 19659
			private readonly NativeSnapshotMetadata mOriginal;

			// Token: 0x04004CCC RID: 19660
			private readonly NativeSnapshotMetadata mUnmerged;

			// Token: 0x04004CCD RID: 19661
			private readonly Action<SavedGameRequestStatus, ISavedGameMetadata> mCompleteCallback;

			// Token: 0x04004CCE RID: 19662
			private readonly Action mRetryFileOpen;
		}

		// Token: 0x02000E08 RID: 3592
		private class Prefetcher
		{
			// Token: 0x060048F8 RID: 18680 RVA: 0x0014C841 File Offset: 0x0014AC41
			internal Prefetcher(Action<byte[], byte[]> dataFetchedCallback, Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback)
			{
				this.mDataFetchedCallback = Misc.CheckNotNull<Action<byte[], byte[]>>(dataFetchedCallback);
				this.completedCallback = Misc.CheckNotNull<Action<SavedGameRequestStatus, ISavedGameMetadata>>(completedCallback);
			}

			// Token: 0x060048F9 RID: 18681 RVA: 0x0014C86C File Offset: 0x0014AC6C
			internal void OnOriginalDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager.ReadResponse readResponse)
			{
				object obj = this.mLock;
				lock (obj)
				{
					if (!readResponse.RequestSucceeded())
					{
						Logger.e("Encountered error while prefetching original data.");
						this.completedCallback(NativeSavedGameClient.AsRequestStatus(readResponse.ResponseStatus()), null);
						this.completedCallback = delegate(SavedGameRequestStatus A_0, ISavedGameMetadata A_1)
						{
						};
					}
					else
					{
						Logger.d("Successfully fetched original data");
						this.mOriginalDataFetched = true;
						this.mOriginalData = readResponse.Data();
						this.MaybeProceed();
					}
				}
			}

			// Token: 0x060048FA RID: 18682 RVA: 0x0014C91C File Offset: 0x0014AD1C
			internal void OnUnmergedDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager.ReadResponse readResponse)
			{
				object obj = this.mLock;
				lock (obj)
				{
					if (!readResponse.RequestSucceeded())
					{
						Logger.e("Encountered error while prefetching unmerged data.");
						this.completedCallback(NativeSavedGameClient.AsRequestStatus(readResponse.ResponseStatus()), null);
						this.completedCallback = delegate(SavedGameRequestStatus A_0, ISavedGameMetadata A_1)
						{
						};
					}
					else
					{
						Logger.d("Successfully fetched unmerged data");
						this.mUnmergedDataFetched = true;
						this.mUnmergedData = readResponse.Data();
						this.MaybeProceed();
					}
				}
			}

			// Token: 0x060048FB RID: 18683 RVA: 0x0014C9CC File Offset: 0x0014ADCC
			private void MaybeProceed()
			{
				if (this.mOriginalDataFetched && this.mUnmergedDataFetched)
				{
					Logger.d("Fetched data for original and unmerged, proceeding");
					this.mDataFetchedCallback(this.mOriginalData, this.mUnmergedData);
				}
				else
				{
					Logger.d(string.Concat(new object[]
					{
						"Not all data fetched - original:",
						this.mOriginalDataFetched,
						" unmerged:",
						this.mUnmergedDataFetched
					}));
				}
			}

			// Token: 0x04004CCF RID: 19663
			private readonly object mLock = new object();

			// Token: 0x04004CD0 RID: 19664
			private bool mOriginalDataFetched;

			// Token: 0x04004CD1 RID: 19665
			private byte[] mOriginalData;

			// Token: 0x04004CD2 RID: 19666
			private bool mUnmergedDataFetched;

			// Token: 0x04004CD3 RID: 19667
			private byte[] mUnmergedData;

			// Token: 0x04004CD4 RID: 19668
			private Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback;

			// Token: 0x04004CD5 RID: 19669
			private readonly Action<byte[], byte[]> mDataFetchedCallback;
		}
	}
}
