﻿using System;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D61 RID: 3425
	internal static class CommonErrorStatus
	{
		// Token: 0x02000D62 RID: 3426
		internal enum ResponseStatus
		{
			// Token: 0x04004B86 RID: 19334
			VALID = 1,
			// Token: 0x04004B87 RID: 19335
			VALID_BUT_STALE,
			// Token: 0x04004B88 RID: 19336
			ERROR_LICENSE_CHECK_FAILED = -1,
			// Token: 0x04004B89 RID: 19337
			ERROR_INTERNAL = -2,
			// Token: 0x04004B8A RID: 19338
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004B8B RID: 19339
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004B8C RID: 19340
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000D63 RID: 3427
		internal enum FlushStatus
		{
			// Token: 0x04004B8E RID: 19342
			FLUSHED = 4,
			// Token: 0x04004B8F RID: 19343
			ERROR_INTERNAL = -2,
			// Token: 0x04004B90 RID: 19344
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004B91 RID: 19345
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004B92 RID: 19346
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000D64 RID: 3428
		internal enum AuthStatus
		{
			// Token: 0x04004B94 RID: 19348
			VALID = 1,
			// Token: 0x04004B95 RID: 19349
			ERROR_INTERNAL = -2,
			// Token: 0x04004B96 RID: 19350
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004B97 RID: 19351
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004B98 RID: 19352
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000D65 RID: 3429
		internal enum UIStatus
		{
			// Token: 0x04004B9A RID: 19354
			VALID = 1,
			// Token: 0x04004B9B RID: 19355
			ERROR_INTERNAL = -2,
			// Token: 0x04004B9C RID: 19356
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004B9D RID: 19357
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004B9E RID: 19358
			ERROR_TIMEOUT = -5,
			// Token: 0x04004B9F RID: 19359
			ERROR_CANCELED = -6,
			// Token: 0x04004BA0 RID: 19360
			ERROR_UI_BUSY = -12,
			// Token: 0x04004BA1 RID: 19361
			ERROR_LEFT_ROOM = -18
		}

		// Token: 0x02000D66 RID: 3430
		internal enum MultiplayerStatus
		{
			// Token: 0x04004BA3 RID: 19363
			VALID = 1,
			// Token: 0x04004BA4 RID: 19364
			VALID_BUT_STALE,
			// Token: 0x04004BA5 RID: 19365
			ERROR_INTERNAL = -2,
			// Token: 0x04004BA6 RID: 19366
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BA7 RID: 19367
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BA8 RID: 19368
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BA9 RID: 19369
			ERROR_MATCH_ALREADY_REMATCHED = -7,
			// Token: 0x04004BAA RID: 19370
			ERROR_INACTIVE_MATCH = -8,
			// Token: 0x04004BAB RID: 19371
			ERROR_INVALID_RESULTS = -9,
			// Token: 0x04004BAC RID: 19372
			ERROR_INVALID_MATCH = -10,
			// Token: 0x04004BAD RID: 19373
			ERROR_MATCH_OUT_OF_DATE = -11,
			// Token: 0x04004BAE RID: 19374
			ERROR_REAL_TIME_ROOM_NOT_JOINED = -17
		}

		// Token: 0x02000D67 RID: 3431
		internal enum QuestAcceptStatus
		{
			// Token: 0x04004BB0 RID: 19376
			VALID = 1,
			// Token: 0x04004BB1 RID: 19377
			ERROR_INTERNAL = -2,
			// Token: 0x04004BB2 RID: 19378
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BB3 RID: 19379
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BB4 RID: 19380
			ERROR_QUEST_NO_LONGER_AVAILABLE = -13,
			// Token: 0x04004BB5 RID: 19381
			ERROR_QUEST_NOT_STARTED = -14
		}

		// Token: 0x02000D68 RID: 3432
		internal enum QuestClaimMilestoneStatus
		{
			// Token: 0x04004BB7 RID: 19383
			VALID = 1,
			// Token: 0x04004BB8 RID: 19384
			ERROR_INTERNAL = -2,
			// Token: 0x04004BB9 RID: 19385
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BBA RID: 19386
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BBB RID: 19387
			ERROR_MILESTONE_ALREADY_CLAIMED = -15,
			// Token: 0x04004BBC RID: 19388
			ERROR_MILESTONE_CLAIM_FAILED = -16
		}

		// Token: 0x02000D69 RID: 3433
		internal enum SnapshotOpenStatus
		{
			// Token: 0x04004BBE RID: 19390
			VALID = 1,
			// Token: 0x04004BBF RID: 19391
			VALID_WITH_CONFLICT = 3,
			// Token: 0x04004BC0 RID: 19392
			ERROR_INTERNAL = -2,
			// Token: 0x04004BC1 RID: 19393
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BC2 RID: 19394
			ERROR_TIMEOUT = -5
		}
	}
}
