﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D88 RID: 3464
	internal static class NearbyConnections
	{
		// Token: 0x060045E2 RID: 17890
		[DllImport("gpg")]
		internal static extern void NearbyConnections_StartDiscovery(HandleRef self, string service_id, long duration, IntPtr helper);

		// Token: 0x060045E3 RID: 17891
		[DllImport("gpg")]
		internal static extern void NearbyConnections_RejectConnectionRequest(HandleRef self, string remote_endpoint_id);

		// Token: 0x060045E4 RID: 17892
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Disconnect(HandleRef self, string remote_endpoint_id);

		// Token: 0x060045E5 RID: 17893
		[DllImport("gpg")]
		internal static extern void NearbyConnections_SendUnreliableMessage(HandleRef self, string remote_endpoint_id, byte[] payload, UIntPtr payload_size);

		// Token: 0x060045E6 RID: 17894
		[DllImport("gpg")]
		internal static extern UIntPtr NearbyConnections_GetLocalDeviceId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045E7 RID: 17895
		[DllImport("gpg")]
		internal static extern void NearbyConnections_StopAdvertising(HandleRef self);

		// Token: 0x060045E8 RID: 17896
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Dispose(HandleRef self);

		// Token: 0x060045E9 RID: 17897
		[DllImport("gpg")]
		internal static extern UIntPtr NearbyConnections_GetLocalEndpointId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045EA RID: 17898
		[DllImport("gpg")]
		internal static extern void NearbyConnections_SendReliableMessage(HandleRef self, string remote_endpoint_id, byte[] payload, UIntPtr payload_size);

		// Token: 0x060045EB RID: 17899
		[DllImport("gpg")]
		internal static extern void NearbyConnections_StopDiscovery(HandleRef self, string service_id);

		// Token: 0x060045EC RID: 17900
		[DllImport("gpg")]
		internal static extern void NearbyConnections_SendConnectionRequest(HandleRef self, string name, string remote_endpoint_id, byte[] payload, UIntPtr payload_size, NearbyConnectionTypes.ConnectionResponseCallback callback, IntPtr callback_arg, IntPtr helper);

		// Token: 0x060045ED RID: 17901
		[DllImport("gpg")]
		internal static extern void NearbyConnections_StartAdvertising(HandleRef self, string name, IntPtr[] app_identifiers, UIntPtr app_identifiers_size, long duration, NearbyConnectionTypes.StartAdvertisingCallback start_advertising_callback, IntPtr start_advertising_callback_arg, NearbyConnectionTypes.ConnectionRequestCallback request_callback, IntPtr request_callback_arg);

		// Token: 0x060045EE RID: 17902
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Stop(HandleRef self);

		// Token: 0x060045EF RID: 17903
		[DllImport("gpg")]
		internal static extern void NearbyConnections_AcceptConnectionRequest(HandleRef self, string remote_endpoint_id, byte[] payload, UIntPtr payload_size, IntPtr helper);
	}
}
