﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DC9 RID: 3529
	internal static class TurnBasedMatch
	{
		// Token: 0x0600475D RID: 18269
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatch_AutomatchingSlotsAvailable(HandleRef self);

		// Token: 0x0600475E RID: 18270
		[DllImport("gpg")]
		internal static extern ulong TurnBasedMatch_CreationTime(HandleRef self);

		// Token: 0x0600475F RID: 18271
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_Participants_Length(HandleRef self);

		// Token: 0x06004760 RID: 18272
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_Participants_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x06004761 RID: 18273
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatch_Version(HandleRef self);

		// Token: 0x06004762 RID: 18274
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_ParticipantResults(HandleRef self);

		// Token: 0x06004763 RID: 18275
		[DllImport("gpg")]
		internal static extern Types.MatchStatus TurnBasedMatch_Status(HandleRef self);

		// Token: 0x06004764 RID: 18276
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_Description(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004765 RID: 18277
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_PendingParticipant(HandleRef self);

		// Token: 0x06004766 RID: 18278
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatch_Variant(HandleRef self);

		// Token: 0x06004767 RID: 18279
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool TurnBasedMatch_HasPreviousMatchData(HandleRef self);

		// Token: 0x06004768 RID: 18280
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_Data(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004769 RID: 18281
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_LastUpdatingParticipant(HandleRef self);

		// Token: 0x0600476A RID: 18282
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool TurnBasedMatch_HasData(HandleRef self);

		// Token: 0x0600476B RID: 18283
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_SuggestedNextParticipant(HandleRef self);

		// Token: 0x0600476C RID: 18284
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_PreviousMatchData(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600476D RID: 18285
		[DllImport("gpg")]
		internal static extern ulong TurnBasedMatch_LastUpdateTime(HandleRef self);

		// Token: 0x0600476E RID: 18286
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_RematchId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600476F RID: 18287
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatch_Number(HandleRef self);

		// Token: 0x06004770 RID: 18288
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool TurnBasedMatch_HasRematchId(HandleRef self);

		// Token: 0x06004771 RID: 18289
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool TurnBasedMatch_Valid(HandleRef self);

		// Token: 0x06004772 RID: 18290
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatch_CreatingParticipant(HandleRef self);

		// Token: 0x06004773 RID: 18291
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatch_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004774 RID: 18292
		[DllImport("gpg")]
		internal static extern void TurnBasedMatch_Dispose(HandleRef self);
	}
}
