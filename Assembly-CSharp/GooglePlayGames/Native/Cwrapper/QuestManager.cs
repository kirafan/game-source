﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D96 RID: 3478
	internal static class QuestManager
	{
		// Token: 0x06004648 RID: 17992
		[DllImport("gpg")]
		internal static extern void QuestManager_FetchList(HandleRef self, Types.DataSource data_source, int fetch_flags, QuestManager.FetchListCallback callback, IntPtr callback_arg);

		// Token: 0x06004649 RID: 17993
		[DllImport("gpg")]
		internal static extern void QuestManager_Accept(HandleRef self, IntPtr quest, QuestManager.AcceptCallback callback, IntPtr callback_arg);

		// Token: 0x0600464A RID: 17994
		[DllImport("gpg")]
		internal static extern void QuestManager_ShowAllUI(HandleRef self, QuestManager.QuestUICallback callback, IntPtr callback_arg);

		// Token: 0x0600464B RID: 17995
		[DllImport("gpg")]
		internal static extern void QuestManager_ShowUI(HandleRef self, IntPtr quest, QuestManager.QuestUICallback callback, IntPtr callback_arg);

		// Token: 0x0600464C RID: 17996
		[DllImport("gpg")]
		internal static extern void QuestManager_ClaimMilestone(HandleRef self, IntPtr milestone, QuestManager.ClaimMilestoneCallback callback, IntPtr callback_arg);

		// Token: 0x0600464D RID: 17997
		[DllImport("gpg")]
		internal static extern void QuestManager_Fetch(HandleRef self, Types.DataSource data_source, string quest_id, QuestManager.FetchCallback callback, IntPtr callback_arg);

		// Token: 0x0600464E RID: 17998
		[DllImport("gpg")]
		internal static extern void QuestManager_FetchResponse_Dispose(HandleRef self);

		// Token: 0x0600464F RID: 17999
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus QuestManager_FetchResponse_GetStatus(HandleRef self);

		// Token: 0x06004650 RID: 18000
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_FetchResponse_GetData(HandleRef self);

		// Token: 0x06004651 RID: 18001
		[DllImport("gpg")]
		internal static extern void QuestManager_FetchListResponse_Dispose(HandleRef self);

		// Token: 0x06004652 RID: 18002
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus QuestManager_FetchListResponse_GetStatus(HandleRef self);

		// Token: 0x06004653 RID: 18003
		[DllImport("gpg")]
		internal static extern UIntPtr QuestManager_FetchListResponse_GetData_Length(HandleRef self);

		// Token: 0x06004654 RID: 18004
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_FetchListResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x06004655 RID: 18005
		[DllImport("gpg")]
		internal static extern void QuestManager_AcceptResponse_Dispose(HandleRef self);

		// Token: 0x06004656 RID: 18006
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.QuestAcceptStatus QuestManager_AcceptResponse_GetStatus(HandleRef self);

		// Token: 0x06004657 RID: 18007
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_AcceptResponse_GetAcceptedQuest(HandleRef self);

		// Token: 0x06004658 RID: 18008
		[DllImport("gpg")]
		internal static extern void QuestManager_ClaimMilestoneResponse_Dispose(HandleRef self);

		// Token: 0x06004659 RID: 18009
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.QuestClaimMilestoneStatus QuestManager_ClaimMilestoneResponse_GetStatus(HandleRef self);

		// Token: 0x0600465A RID: 18010
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(HandleRef self);

		// Token: 0x0600465B RID: 18011
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_ClaimMilestoneResponse_GetQuest(HandleRef self);

		// Token: 0x0600465C RID: 18012
		[DllImport("gpg")]
		internal static extern void QuestManager_QuestUIResponse_Dispose(HandleRef self);

		// Token: 0x0600465D RID: 18013
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus QuestManager_QuestUIResponse_GetStatus(HandleRef self);

		// Token: 0x0600465E RID: 18014
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_QuestUIResponse_GetAcceptedQuest(HandleRef self);

		// Token: 0x0600465F RID: 18015
		[DllImport("gpg")]
		internal static extern IntPtr QuestManager_QuestUIResponse_GetMilestoneToClaim(HandleRef self);

		// Token: 0x02000D97 RID: 3479
		// (Invoke) Token: 0x06004661 RID: 18017
		internal delegate void FetchCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D98 RID: 3480
		// (Invoke) Token: 0x06004665 RID: 18021
		internal delegate void FetchListCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D99 RID: 3481
		// (Invoke) Token: 0x06004669 RID: 18025
		internal delegate void AcceptCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D9A RID: 3482
		// (Invoke) Token: 0x0600466D RID: 18029
		internal delegate void ClaimMilestoneCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D9B RID: 3483
		// (Invoke) Token: 0x06004671 RID: 18033
		internal delegate void QuestUICallback(IntPtr arg0, IntPtr arg1);
	}
}
