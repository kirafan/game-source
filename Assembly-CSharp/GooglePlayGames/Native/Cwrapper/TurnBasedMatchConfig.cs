﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DCA RID: 3530
	internal static class TurnBasedMatchConfig
	{
		// Token: 0x06004775 RID: 18293
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatchConfig_PlayerIdsToInvite_Length(HandleRef self);

		// Token: 0x06004776 RID: 18294
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(HandleRef self, UIntPtr index, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004777 RID: 18295
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatchConfig_Variant(HandleRef self);

		// Token: 0x06004778 RID: 18296
		[DllImport("gpg")]
		internal static extern long TurnBasedMatchConfig_ExclusiveBitMask(HandleRef self);

		// Token: 0x06004779 RID: 18297
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool TurnBasedMatchConfig_Valid(HandleRef self);

		// Token: 0x0600477A RID: 18298
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatchConfig_MaximumAutomatchingPlayers(HandleRef self);

		// Token: 0x0600477B RID: 18299
		[DllImport("gpg")]
		internal static extern uint TurnBasedMatchConfig_MinimumAutomatchingPlayers(HandleRef self);

		// Token: 0x0600477C RID: 18300
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Dispose(HandleRef self);
	}
}
