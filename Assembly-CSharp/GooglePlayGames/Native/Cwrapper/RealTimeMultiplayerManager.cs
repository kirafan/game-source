﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DA4 RID: 3492
	internal static class RealTimeMultiplayerManager
	{
		// Token: 0x0600469E RID: 18078
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_CreateRealTimeRoom(HandleRef self, IntPtr config, IntPtr helper, RealTimeMultiplayerManager.RealTimeRoomCallback callback, IntPtr callback_arg);

		// Token: 0x0600469F RID: 18079
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_LeaveRoom(HandleRef self, IntPtr room, RealTimeMultiplayerManager.LeaveRoomCallback callback, IntPtr callback_arg);

		// Token: 0x060046A0 RID: 18080
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_SendUnreliableMessage(HandleRef self, IntPtr room, IntPtr[] participants, UIntPtr participants_size, byte[] data, UIntPtr data_size);

		// Token: 0x060046A1 RID: 18081
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_ShowWaitingRoomUI(HandleRef self, IntPtr room, uint min_participants_to_start, RealTimeMultiplayerManager.WaitingRoomUICallback callback, IntPtr callback_arg);

		// Token: 0x060046A2 RID: 18082
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_ShowPlayerSelectUI(HandleRef self, uint minimum_players, uint maximum_players, [MarshalAs(UnmanagedType.I1)] bool allow_automatch, RealTimeMultiplayerManager.PlayerSelectUICallback callback, IntPtr callback_arg);

		// Token: 0x060046A3 RID: 18083
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_DismissInvitation(HandleRef self, IntPtr invitation);

		// Token: 0x060046A4 RID: 18084
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_DeclineInvitation(HandleRef self, IntPtr invitation);

		// Token: 0x060046A5 RID: 18085
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_SendReliableMessage(HandleRef self, IntPtr room, IntPtr participant, byte[] data, UIntPtr data_size, RealTimeMultiplayerManager.SendReliableMessageCallback callback, IntPtr callback_arg);

		// Token: 0x060046A6 RID: 18086
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_AcceptInvitation(HandleRef self, IntPtr invitation, IntPtr helper, RealTimeMultiplayerManager.RealTimeRoomCallback callback, IntPtr callback_arg);

		// Token: 0x060046A7 RID: 18087
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_FetchInvitations(HandleRef self, RealTimeMultiplayerManager.FetchInvitationsCallback callback, IntPtr callback_arg);

		// Token: 0x060046A8 RID: 18088
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_SendUnreliableMessageToOthers(HandleRef self, IntPtr room, byte[] data, UIntPtr data_size);

		// Token: 0x060046A9 RID: 18089
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_ShowRoomInboxUI(HandleRef self, RealTimeMultiplayerManager.RoomInboxUICallback callback, IntPtr callback_arg);

		// Token: 0x060046AA RID: 18090
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(HandleRef self);

		// Token: 0x060046AB RID: 18091
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.MultiplayerStatus RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(HandleRef self);

		// Token: 0x060046AC RID: 18092
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(HandleRef self);

		// Token: 0x060046AD RID: 18093
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(HandleRef self);

		// Token: 0x060046AE RID: 18094
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(HandleRef self);

		// Token: 0x060046AF RID: 18095
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(HandleRef self);

		// Token: 0x060046B0 RID: 18096
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(HandleRef self);

		// Token: 0x060046B1 RID: 18097
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(HandleRef self);

		// Token: 0x060046B2 RID: 18098
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(HandleRef self);

		// Token: 0x060046B3 RID: 18099
		[DllImport("gpg")]
		internal static extern void RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(HandleRef self);

		// Token: 0x060046B4 RID: 18100
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(HandleRef self);

		// Token: 0x060046B5 RID: 18101
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(HandleRef self);

		// Token: 0x060046B6 RID: 18102
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x02000DA5 RID: 3493
		// (Invoke) Token: 0x060046B8 RID: 18104
		internal delegate void RealTimeRoomCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DA6 RID: 3494
		// (Invoke) Token: 0x060046BC RID: 18108
		internal delegate void LeaveRoomCallback(CommonErrorStatus.ResponseStatus arg0, IntPtr arg1);

		// Token: 0x02000DA7 RID: 3495
		// (Invoke) Token: 0x060046C0 RID: 18112
		internal delegate void SendReliableMessageCallback(CommonErrorStatus.MultiplayerStatus arg0, IntPtr arg1);

		// Token: 0x02000DA8 RID: 3496
		// (Invoke) Token: 0x060046C4 RID: 18116
		internal delegate void RoomInboxUICallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DA9 RID: 3497
		// (Invoke) Token: 0x060046C8 RID: 18120
		internal delegate void PlayerSelectUICallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DAA RID: 3498
		// (Invoke) Token: 0x060046CC RID: 18124
		internal delegate void WaitingRoomUICallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DAB RID: 3499
		// (Invoke) Token: 0x060046D0 RID: 18128
		internal delegate void FetchInvitationsCallback(IntPtr arg0, IntPtr arg1);
	}
}
