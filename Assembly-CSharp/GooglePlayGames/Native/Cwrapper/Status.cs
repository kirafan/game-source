﻿using System;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DBE RID: 3518
	internal static class Status
	{
		// Token: 0x02000DBF RID: 3519
		internal enum ResponseStatus
		{
			// Token: 0x04004BCF RID: 19407
			VALID = 1,
			// Token: 0x04004BD0 RID: 19408
			VALID_BUT_STALE,
			// Token: 0x04004BD1 RID: 19409
			ERROR_LICENSE_CHECK_FAILED = -1,
			// Token: 0x04004BD2 RID: 19410
			ERROR_INTERNAL = -2,
			// Token: 0x04004BD3 RID: 19411
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BD4 RID: 19412
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BD5 RID: 19413
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000DC0 RID: 3520
		internal enum FlushStatus
		{
			// Token: 0x04004BD7 RID: 19415
			FLUSHED = 4,
			// Token: 0x04004BD8 RID: 19416
			ERROR_INTERNAL = -2,
			// Token: 0x04004BD9 RID: 19417
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BDA RID: 19418
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BDB RID: 19419
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000DC1 RID: 3521
		internal enum AuthStatus
		{
			// Token: 0x04004BDD RID: 19421
			VALID = 1,
			// Token: 0x04004BDE RID: 19422
			ERROR_INTERNAL = -2,
			// Token: 0x04004BDF RID: 19423
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BE0 RID: 19424
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BE1 RID: 19425
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000DC2 RID: 3522
		internal enum UIStatus
		{
			// Token: 0x04004BE3 RID: 19427
			VALID = 1,
			// Token: 0x04004BE4 RID: 19428
			ERROR_INTERNAL = -2,
			// Token: 0x04004BE5 RID: 19429
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BE6 RID: 19430
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BE7 RID: 19431
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BE8 RID: 19432
			ERROR_CANCELED = -6,
			// Token: 0x04004BE9 RID: 19433
			ERROR_UI_BUSY = -12,
			// Token: 0x04004BEA RID: 19434
			ERROR_LEFT_ROOM = -18
		}

		// Token: 0x02000DC3 RID: 3523
		internal enum MultiplayerStatus
		{
			// Token: 0x04004BEC RID: 19436
			VALID = 1,
			// Token: 0x04004BED RID: 19437
			VALID_BUT_STALE,
			// Token: 0x04004BEE RID: 19438
			ERROR_INTERNAL = -2,
			// Token: 0x04004BEF RID: 19439
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BF0 RID: 19440
			ERROR_VERSION_UPDATE_REQUIRED = -4,
			// Token: 0x04004BF1 RID: 19441
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BF2 RID: 19442
			ERROR_MATCH_ALREADY_REMATCHED = -7,
			// Token: 0x04004BF3 RID: 19443
			ERROR_INACTIVE_MATCH = -8,
			// Token: 0x04004BF4 RID: 19444
			ERROR_INVALID_RESULTS = -9,
			// Token: 0x04004BF5 RID: 19445
			ERROR_INVALID_MATCH = -10,
			// Token: 0x04004BF6 RID: 19446
			ERROR_MATCH_OUT_OF_DATE = -11,
			// Token: 0x04004BF7 RID: 19447
			ERROR_REAL_TIME_ROOM_NOT_JOINED = -17
		}

		// Token: 0x02000DC4 RID: 3524
		internal enum QuestAcceptStatus
		{
			// Token: 0x04004BF9 RID: 19449
			VALID = 1,
			// Token: 0x04004BFA RID: 19450
			ERROR_INTERNAL = -2,
			// Token: 0x04004BFB RID: 19451
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004BFC RID: 19452
			ERROR_TIMEOUT = -5,
			// Token: 0x04004BFD RID: 19453
			ERROR_QUEST_NO_LONGER_AVAILABLE = -13,
			// Token: 0x04004BFE RID: 19454
			ERROR_QUEST_NOT_STARTED = -14
		}

		// Token: 0x02000DC5 RID: 3525
		internal enum QuestClaimMilestoneStatus
		{
			// Token: 0x04004C00 RID: 19456
			VALID = 1,
			// Token: 0x04004C01 RID: 19457
			ERROR_INTERNAL = -2,
			// Token: 0x04004C02 RID: 19458
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004C03 RID: 19459
			ERROR_TIMEOUT = -5,
			// Token: 0x04004C04 RID: 19460
			ERROR_MILESTONE_ALREADY_CLAIMED = -15,
			// Token: 0x04004C05 RID: 19461
			ERROR_MILESTONE_CLAIM_FAILED = -16
		}

		// Token: 0x02000DC6 RID: 3526
		internal enum CommonErrorStatus
		{
			// Token: 0x04004C07 RID: 19463
			ERROR_INTERNAL = -2,
			// Token: 0x04004C08 RID: 19464
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004C09 RID: 19465
			ERROR_TIMEOUT = -5
		}

		// Token: 0x02000DC7 RID: 3527
		internal enum SnapshotOpenStatus
		{
			// Token: 0x04004C0B RID: 19467
			VALID = 1,
			// Token: 0x04004C0C RID: 19468
			VALID_WITH_CONFLICT = 3,
			// Token: 0x04004C0D RID: 19469
			ERROR_INTERNAL = -2,
			// Token: 0x04004C0E RID: 19470
			ERROR_NOT_AUTHORIZED = -3,
			// Token: 0x04004C0F RID: 19471
			ERROR_TIMEOUT = -5
		}
	}
}
