﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DAE RID: 3502
	internal static class RealTimeRoomConfigBuilder
	{
		// Token: 0x060046E7 RID: 18151
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(HandleRef self, IntPtr response);

		// Token: 0x060046E8 RID: 18152
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_SetVariant(HandleRef self, uint variant);

		// Token: 0x060046E9 RID: 18153
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_AddPlayerToInvite(HandleRef self, string player_id);

		// Token: 0x060046EA RID: 18154
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeRoomConfig_Builder_Construct();

		// Token: 0x060046EB RID: 18155
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_SetExclusiveBitMask(HandleRef self, ulong exclusive_bit_mask);

		// Token: 0x060046EC RID: 18156
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(HandleRef self, uint maximum_automatching_players);

		// Token: 0x060046ED RID: 18157
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeRoomConfig_Builder_Create(HandleRef self);

		// Token: 0x060046EE RID: 18158
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(HandleRef self, uint minimum_automatching_players);

		// Token: 0x060046EF RID: 18159
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Builder_Dispose(HandleRef self);
	}
}
