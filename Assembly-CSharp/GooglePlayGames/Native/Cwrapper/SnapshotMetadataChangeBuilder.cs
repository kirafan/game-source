﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DBB RID: 3515
	internal static class SnapshotMetadataChangeBuilder
	{
		// Token: 0x0600474F RID: 18255
		[DllImport("gpg")]
		internal static extern void SnapshotMetadataChange_Builder_SetDescription(HandleRef self, string description);

		// Token: 0x06004750 RID: 18256
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotMetadataChange_Builder_Construct();

		// Token: 0x06004751 RID: 18257
		[DllImport("gpg")]
		internal static extern void SnapshotMetadataChange_Builder_SetPlayedTime(HandleRef self, ulong played_time);

		// Token: 0x06004752 RID: 18258
		[DllImport("gpg")]
		internal static extern void SnapshotMetadataChange_Builder_SetCoverImageFromPngData(HandleRef self, byte[] png_data, UIntPtr png_data_size);

		// Token: 0x06004753 RID: 18259
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotMetadataChange_Builder_Create(HandleRef self);

		// Token: 0x06004754 RID: 18260
		[DllImport("gpg")]
		internal static extern void SnapshotMetadataChange_Builder_Dispose(HandleRef self);
	}
}
