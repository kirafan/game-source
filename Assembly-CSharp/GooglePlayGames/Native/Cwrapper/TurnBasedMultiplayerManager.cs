﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DCC RID: 3532
	internal static class TurnBasedMultiplayerManager
	{
		// Token: 0x06004786 RID: 18310
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_ShowPlayerSelectUI(HandleRef self, uint minimum_players, uint maximum_players, [MarshalAs(UnmanagedType.I1)] bool allow_automatch, TurnBasedMultiplayerManager.PlayerSelectUICallback callback, IntPtr callback_arg);

		// Token: 0x06004787 RID: 18311
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_CancelMatch(HandleRef self, IntPtr match, TurnBasedMultiplayerManager.MultiplayerStatusCallback callback, IntPtr callback_arg);

		// Token: 0x06004788 RID: 18312
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_DismissMatch(HandleRef self, IntPtr match);

		// Token: 0x06004789 RID: 18313
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_ShowMatchInboxUI(HandleRef self, TurnBasedMultiplayerManager.MatchInboxUICallback callback, IntPtr callback_arg);

		// Token: 0x0600478A RID: 18314
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_SynchronizeData(HandleRef self);

		// Token: 0x0600478B RID: 18315
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_Rematch(HandleRef self, IntPtr match, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x0600478C RID: 18316
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_DismissInvitation(HandleRef self, IntPtr invitation);

		// Token: 0x0600478D RID: 18317
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_FetchMatch(HandleRef self, string match_id, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x0600478E RID: 18318
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_DeclineInvitation(HandleRef self, IntPtr invitation);

		// Token: 0x0600478F RID: 18319
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(HandleRef self, IntPtr match, byte[] match_data, UIntPtr match_data_size, IntPtr results, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x06004790 RID: 18320
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_FetchMatches(HandleRef self, TurnBasedMultiplayerManager.TurnBasedMatchesCallback callback, IntPtr callback_arg);

		// Token: 0x06004791 RID: 18321
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_CreateTurnBasedMatch(HandleRef self, IntPtr config, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x06004792 RID: 18322
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_AcceptInvitation(HandleRef self, IntPtr invitation, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x06004793 RID: 18323
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_TakeMyTurn(HandleRef self, IntPtr match, byte[] match_data, UIntPtr match_data_size, IntPtr results, IntPtr next_participant, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x06004794 RID: 18324
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_ConfirmPendingCompletion(HandleRef self, IntPtr match, TurnBasedMultiplayerManager.TurnBasedMatchCallback callback, IntPtr callback_arg);

		// Token: 0x06004795 RID: 18325
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(HandleRef self, IntPtr match, IntPtr next_participant, TurnBasedMultiplayerManager.MultiplayerStatusCallback callback, IntPtr callback_arg);

		// Token: 0x06004796 RID: 18326
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(HandleRef self, IntPtr match, TurnBasedMultiplayerManager.MultiplayerStatusCallback callback, IntPtr callback_arg);

		// Token: 0x06004797 RID: 18327
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(HandleRef self);

		// Token: 0x06004798 RID: 18328
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.MultiplayerStatus TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(HandleRef self);

		// Token: 0x06004799 RID: 18329
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(HandleRef self);

		// Token: 0x0600479A RID: 18330
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(HandleRef self);

		// Token: 0x0600479B RID: 18331
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.MultiplayerStatus TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(HandleRef self);

		// Token: 0x0600479C RID: 18332
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(HandleRef self);

		// Token: 0x0600479D RID: 18333
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x0600479E RID: 18334
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(HandleRef self);

		// Token: 0x0600479F RID: 18335
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060047A0 RID: 18336
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(HandleRef self);

		// Token: 0x060047A1 RID: 18337
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060047A2 RID: 18338
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(HandleRef self);

		// Token: 0x060047A3 RID: 18339
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060047A4 RID: 18340
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(HandleRef self);

		// Token: 0x060047A5 RID: 18341
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(HandleRef self);

		// Token: 0x060047A6 RID: 18342
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(HandleRef self);

		// Token: 0x060047A7 RID: 18343
		[DllImport("gpg")]
		internal static extern void TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(HandleRef self);

		// Token: 0x060047A8 RID: 18344
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(HandleRef self);

		// Token: 0x060047A9 RID: 18345
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(HandleRef self);

		// Token: 0x060047AA RID: 18346
		[DllImport("gpg")]
		internal static extern UIntPtr TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(HandleRef self, UIntPtr index, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060047AB RID: 18347
		[DllImport("gpg")]
		internal static extern uint TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(HandleRef self);

		// Token: 0x060047AC RID: 18348
		[DllImport("gpg")]
		internal static extern uint TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(HandleRef self);

		// Token: 0x02000DCD RID: 3533
		// (Invoke) Token: 0x060047AE RID: 18350
		internal delegate void TurnBasedMatchCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DCE RID: 3534
		// (Invoke) Token: 0x060047B2 RID: 18354
		internal delegate void MultiplayerStatusCallback(CommonErrorStatus.MultiplayerStatus arg0, IntPtr arg1);

		// Token: 0x02000DCF RID: 3535
		// (Invoke) Token: 0x060047B6 RID: 18358
		internal delegate void TurnBasedMatchesCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DD0 RID: 3536
		// (Invoke) Token: 0x060047BA RID: 18362
		internal delegate void MatchInboxUICallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DD1 RID: 3537
		// (Invoke) Token: 0x060047BE RID: 18366
		internal delegate void PlayerSelectUICallback(IntPtr arg0, IntPtr arg1);
	}
}
