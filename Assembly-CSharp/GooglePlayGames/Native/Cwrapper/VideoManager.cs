﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DEC RID: 3564
	internal static class VideoManager
	{
		// Token: 0x060047CF RID: 18383
		[DllImport("gpg")]
		internal static extern void VideoManager_GetCaptureCapabilities(HandleRef self, VideoManager.CaptureCapabilitiesCallback callback, IntPtr callback_arg);

		// Token: 0x060047D0 RID: 18384
		[DllImport("gpg")]
		internal static extern void VideoManager_ShowCaptureOverlay(HandleRef self);

		// Token: 0x060047D1 RID: 18385
		[DllImport("gpg")]
		internal static extern void VideoManager_GetCaptureState(HandleRef self, VideoManager.CaptureStateCallback callback, IntPtr callback_arg);

		// Token: 0x060047D2 RID: 18386
		[DllImport("gpg")]
		internal static extern void VideoManager_IsCaptureAvailable(HandleRef self, Types.VideoCaptureMode capture_mode, VideoManager.IsCaptureAvailableCallback callback, IntPtr callback_arg);

		// Token: 0x060047D3 RID: 18387
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoManager_IsCaptureSupported(HandleRef self);

		// Token: 0x060047D4 RID: 18388
		[DllImport("gpg")]
		internal static extern void VideoManager_RegisterCaptureOverlayStateChangedListener(HandleRef self, IntPtr helper);

		// Token: 0x060047D5 RID: 18389
		[DllImport("gpg")]
		internal static extern void VideoManager_UnregisterCaptureOverlayStateChangedListener(HandleRef self);

		// Token: 0x060047D6 RID: 18390
		[DllImport("gpg")]
		internal static extern void VideoManager_GetCaptureCapabilitiesResponse_Dispose(HandleRef self);

		// Token: 0x060047D7 RID: 18391
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus VideoManager_GetCaptureCapabilitiesResponse_GetStatus(HandleRef self);

		// Token: 0x060047D8 RID: 18392
		[DllImport("gpg")]
		internal static extern IntPtr VideoManager_GetCaptureCapabilitiesResponse_GetVideoCapabilities(HandleRef self);

		// Token: 0x060047D9 RID: 18393
		[DllImport("gpg")]
		internal static extern void VideoManager_GetCaptureStateResponse_Dispose(HandleRef self);

		// Token: 0x060047DA RID: 18394
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus VideoManager_GetCaptureStateResponse_GetStatus(HandleRef self);

		// Token: 0x060047DB RID: 18395
		[DllImport("gpg")]
		internal static extern IntPtr VideoManager_GetCaptureStateResponse_GetVideoCaptureState(HandleRef self);

		// Token: 0x060047DC RID: 18396
		[DllImport("gpg")]
		internal static extern void VideoManager_IsCaptureAvailableResponse_Dispose(HandleRef self);

		// Token: 0x060047DD RID: 18397
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus VideoManager_IsCaptureAvailableResponse_GetStatus(HandleRef self);

		// Token: 0x060047DE RID: 18398
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoManager_IsCaptureAvailableResponse_GetIsCaptureAvailable(HandleRef self);

		// Token: 0x02000DED RID: 3565
		// (Invoke) Token: 0x060047E0 RID: 18400
		internal delegate void CaptureCapabilitiesCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DEE RID: 3566
		// (Invoke) Token: 0x060047E4 RID: 18404
		internal delegate void CaptureStateCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DEF RID: 3567
		// (Invoke) Token: 0x060047E8 RID: 18408
		internal delegate void IsCaptureAvailableCallback(IntPtr arg0, IntPtr arg1);
	}
}
