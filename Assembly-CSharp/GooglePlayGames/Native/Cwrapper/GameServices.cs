﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D71 RID: 3441
	internal static class GameServices
	{
		// Token: 0x06004556 RID: 17750
		[DllImport("gpg")]
		internal static extern void GameServices_Flush(HandleRef self, GameServices.FlushCallback callback, IntPtr callback_arg);

		// Token: 0x06004557 RID: 17751
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool GameServices_IsAuthorized(HandleRef self);

		// Token: 0x06004558 RID: 17752
		[DllImport("gpg")]
		internal static extern void GameServices_Dispose(HandleRef self);

		// Token: 0x06004559 RID: 17753
		[DllImport("gpg")]
		internal static extern void GameServices_SignOut(HandleRef self);

		// Token: 0x0600455A RID: 17754
		[DllImport("gpg")]
		internal static extern void GameServices_StartAuthorizationUI(HandleRef self);

		// Token: 0x02000D72 RID: 3442
		// (Invoke) Token: 0x0600455C RID: 17756
		internal delegate void FlushCallback(CommonErrorStatus.FlushStatus arg0, IntPtr arg1);
	}
}
