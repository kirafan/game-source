﻿using System;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DD2 RID: 3538
	internal static class Types
	{
		// Token: 0x02000DD3 RID: 3539
		internal enum DataSource
		{
			// Token: 0x04004C12 RID: 19474
			CACHE_OR_NETWORK = 1,
			// Token: 0x04004C13 RID: 19475
			NETWORK_ONLY
		}

		// Token: 0x02000DD4 RID: 3540
		internal enum LogLevel
		{
			// Token: 0x04004C15 RID: 19477
			VERBOSE = 1,
			// Token: 0x04004C16 RID: 19478
			INFO,
			// Token: 0x04004C17 RID: 19479
			WARNING,
			// Token: 0x04004C18 RID: 19480
			ERROR
		}

		// Token: 0x02000DD5 RID: 3541
		internal enum AuthOperation
		{
			// Token: 0x04004C1A RID: 19482
			SIGN_IN = 1,
			// Token: 0x04004C1B RID: 19483
			SIGN_OUT
		}

		// Token: 0x02000DD6 RID: 3542
		internal enum ImageResolution
		{
			// Token: 0x04004C1D RID: 19485
			ICON = 1,
			// Token: 0x04004C1E RID: 19486
			HI_RES
		}

		// Token: 0x02000DD7 RID: 3543
		internal enum AchievementType
		{
			// Token: 0x04004C20 RID: 19488
			STANDARD = 1,
			// Token: 0x04004C21 RID: 19489
			INCREMENTAL
		}

		// Token: 0x02000DD8 RID: 3544
		internal enum AchievementState
		{
			// Token: 0x04004C23 RID: 19491
			HIDDEN = 1,
			// Token: 0x04004C24 RID: 19492
			REVEALED,
			// Token: 0x04004C25 RID: 19493
			UNLOCKED
		}

		// Token: 0x02000DD9 RID: 3545
		internal enum EventVisibility
		{
			// Token: 0x04004C27 RID: 19495
			HIDDEN = 1,
			// Token: 0x04004C28 RID: 19496
			REVEALED
		}

		// Token: 0x02000DDA RID: 3546
		internal enum LeaderboardOrder
		{
			// Token: 0x04004C2A RID: 19498
			LARGER_IS_BETTER = 1,
			// Token: 0x04004C2B RID: 19499
			SMALLER_IS_BETTER
		}

		// Token: 0x02000DDB RID: 3547
		internal enum LeaderboardStart
		{
			// Token: 0x04004C2D RID: 19501
			TOP_SCORES = 1,
			// Token: 0x04004C2E RID: 19502
			PLAYER_CENTERED
		}

		// Token: 0x02000DDC RID: 3548
		internal enum LeaderboardTimeSpan
		{
			// Token: 0x04004C30 RID: 19504
			DAILY = 1,
			// Token: 0x04004C31 RID: 19505
			WEEKLY,
			// Token: 0x04004C32 RID: 19506
			ALL_TIME
		}

		// Token: 0x02000DDD RID: 3549
		internal enum LeaderboardCollection
		{
			// Token: 0x04004C34 RID: 19508
			PUBLIC = 1,
			// Token: 0x04004C35 RID: 19509
			SOCIAL
		}

		// Token: 0x02000DDE RID: 3550
		internal enum ParticipantStatus
		{
			// Token: 0x04004C37 RID: 19511
			INVITED = 1,
			// Token: 0x04004C38 RID: 19512
			JOINED,
			// Token: 0x04004C39 RID: 19513
			DECLINED,
			// Token: 0x04004C3A RID: 19514
			LEFT,
			// Token: 0x04004C3B RID: 19515
			NOT_INVITED_YET,
			// Token: 0x04004C3C RID: 19516
			FINISHED,
			// Token: 0x04004C3D RID: 19517
			UNRESPONSIVE
		}

		// Token: 0x02000DDF RID: 3551
		internal enum MatchResult
		{
			// Token: 0x04004C3F RID: 19519
			DISAGREED = 1,
			// Token: 0x04004C40 RID: 19520
			DISCONNECTED,
			// Token: 0x04004C41 RID: 19521
			LOSS,
			// Token: 0x04004C42 RID: 19522
			NONE,
			// Token: 0x04004C43 RID: 19523
			TIE,
			// Token: 0x04004C44 RID: 19524
			WIN
		}

		// Token: 0x02000DE0 RID: 3552
		internal enum MatchStatus
		{
			// Token: 0x04004C46 RID: 19526
			INVITED = 1,
			// Token: 0x04004C47 RID: 19527
			THEIR_TURN,
			// Token: 0x04004C48 RID: 19528
			MY_TURN,
			// Token: 0x04004C49 RID: 19529
			PENDING_COMPLETION,
			// Token: 0x04004C4A RID: 19530
			COMPLETED,
			// Token: 0x04004C4B RID: 19531
			CANCELED,
			// Token: 0x04004C4C RID: 19532
			EXPIRED
		}

		// Token: 0x02000DE1 RID: 3553
		internal enum QuestState
		{
			// Token: 0x04004C4E RID: 19534
			UPCOMING = 1,
			// Token: 0x04004C4F RID: 19535
			OPEN,
			// Token: 0x04004C50 RID: 19536
			ACCEPTED,
			// Token: 0x04004C51 RID: 19537
			COMPLETED,
			// Token: 0x04004C52 RID: 19538
			EXPIRED,
			// Token: 0x04004C53 RID: 19539
			FAILED
		}

		// Token: 0x02000DE2 RID: 3554
		internal enum QuestMilestoneState
		{
			// Token: 0x04004C55 RID: 19541
			NOT_STARTED = 1,
			// Token: 0x04004C56 RID: 19542
			NOT_COMPLETED,
			// Token: 0x04004C57 RID: 19543
			COMPLETED_NOT_CLAIMED,
			// Token: 0x04004C58 RID: 19544
			CLAIMED
		}

		// Token: 0x02000DE3 RID: 3555
		internal enum MultiplayerEvent
		{
			// Token: 0x04004C5A RID: 19546
			UPDATED = 1,
			// Token: 0x04004C5B RID: 19547
			UPDATED_FROM_APP_LAUNCH,
			// Token: 0x04004C5C RID: 19548
			REMOVED
		}

		// Token: 0x02000DE4 RID: 3556
		internal enum MultiplayerInvitationType
		{
			// Token: 0x04004C5E RID: 19550
			TURN_BASED = 1,
			// Token: 0x04004C5F RID: 19551
			REAL_TIME
		}

		// Token: 0x02000DE5 RID: 3557
		internal enum RealTimeRoomStatus
		{
			// Token: 0x04004C61 RID: 19553
			INVITING = 1,
			// Token: 0x04004C62 RID: 19554
			CONNECTING,
			// Token: 0x04004C63 RID: 19555
			AUTO_MATCHING,
			// Token: 0x04004C64 RID: 19556
			ACTIVE,
			// Token: 0x04004C65 RID: 19557
			DELETED
		}

		// Token: 0x02000DE6 RID: 3558
		internal enum SnapshotConflictPolicy
		{
			// Token: 0x04004C67 RID: 19559
			MANUAL = 1,
			// Token: 0x04004C68 RID: 19560
			LONGEST_PLAYTIME,
			// Token: 0x04004C69 RID: 19561
			LAST_KNOWN_GOOD,
			// Token: 0x04004C6A RID: 19562
			MOST_RECENTLY_MODIFIED
		}

		// Token: 0x02000DE7 RID: 3559
		internal enum VideoCaptureMode
		{
			// Token: 0x04004C6C RID: 19564
			UNKNOWN = -1,
			// Token: 0x04004C6D RID: 19565
			FILE,
			// Token: 0x04004C6E RID: 19566
			STREAM
		}

		// Token: 0x02000DE8 RID: 3560
		internal enum VideoQualityLevel
		{
			// Token: 0x04004C70 RID: 19568
			UNKNOWN = -1,
			// Token: 0x04004C71 RID: 19569
			SD,
			// Token: 0x04004C72 RID: 19570
			HD,
			// Token: 0x04004C73 RID: 19571
			XHD,
			// Token: 0x04004C74 RID: 19572
			FULLHD
		}

		// Token: 0x02000DE9 RID: 3561
		internal enum VideoCaptureOverlayState
		{
			// Token: 0x04004C76 RID: 19574
			UNKNOWN = -1,
			// Token: 0x04004C77 RID: 19575
			SHOWN = 1,
			// Token: 0x04004C78 RID: 19576
			STARTED,
			// Token: 0x04004C79 RID: 19577
			STOPPED,
			// Token: 0x04004C7A RID: 19578
			DISMISSED
		}
	}
}
