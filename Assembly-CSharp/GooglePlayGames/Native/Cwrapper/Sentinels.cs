﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DB2 RID: 3506
	internal static class Sentinels
	{
		// Token: 0x06004710 RID: 18192
		[DllImport("gpg")]
		internal static extern IntPtr Sentinels_AutomatchingParticipant();
	}
}
