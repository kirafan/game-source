﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DB0 RID: 3504
	internal static class ScorePage
	{
		// Token: 0x060046F5 RID: 18165
		[DllImport("gpg")]
		internal static extern void ScorePage_Dispose(HandleRef self);

		// Token: 0x060046F6 RID: 18166
		[DllImport("gpg")]
		internal static extern Types.LeaderboardTimeSpan ScorePage_TimeSpan(HandleRef self);

		// Token: 0x060046F7 RID: 18167
		[DllImport("gpg")]
		internal static extern UIntPtr ScorePage_LeaderboardId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060046F8 RID: 18168
		[DllImport("gpg")]
		internal static extern Types.LeaderboardCollection ScorePage_Collection(HandleRef self);

		// Token: 0x060046F9 RID: 18169
		[DllImport("gpg")]
		internal static extern Types.LeaderboardStart ScorePage_Start(HandleRef self);

		// Token: 0x060046FA RID: 18170
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScorePage_Valid(HandleRef self);

		// Token: 0x060046FB RID: 18171
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScorePage_HasPreviousScorePage(HandleRef self);

		// Token: 0x060046FC RID: 18172
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScorePage_HasNextScorePage(HandleRef self);

		// Token: 0x060046FD RID: 18173
		[DllImport("gpg")]
		internal static extern IntPtr ScorePage_PreviousScorePageToken(HandleRef self);

		// Token: 0x060046FE RID: 18174
		[DllImport("gpg")]
		internal static extern IntPtr ScorePage_NextScorePageToken(HandleRef self);

		// Token: 0x060046FF RID: 18175
		[DllImport("gpg")]
		internal static extern UIntPtr ScorePage_Entries_Length(HandleRef self);

		// Token: 0x06004700 RID: 18176
		[DllImport("gpg")]
		internal static extern IntPtr ScorePage_Entries_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x06004701 RID: 18177
		[DllImport("gpg")]
		internal static extern void ScorePage_Entry_Dispose(HandleRef self);

		// Token: 0x06004702 RID: 18178
		[DllImport("gpg")]
		internal static extern UIntPtr ScorePage_Entry_PlayerId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004703 RID: 18179
		[DllImport("gpg")]
		internal static extern ulong ScorePage_Entry_LastModified(HandleRef self);

		// Token: 0x06004704 RID: 18180
		[DllImport("gpg")]
		internal static extern IntPtr ScorePage_Entry_Score(HandleRef self);

		// Token: 0x06004705 RID: 18181
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScorePage_Entry_Valid(HandleRef self);

		// Token: 0x06004706 RID: 18182
		[DllImport("gpg")]
		internal static extern ulong ScorePage_Entry_LastModifiedTime(HandleRef self);

		// Token: 0x06004707 RID: 18183
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScorePage_ScorePageToken_Valid(HandleRef self);

		// Token: 0x06004708 RID: 18184
		[DllImport("gpg")]
		internal static extern void ScorePage_ScorePageToken_Dispose(HandleRef self);
	}
}
