﻿using System;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D8C RID: 3468
	internal static class NearbyConnectionsStatus
	{
		// Token: 0x02000D8D RID: 3469
		internal enum InitializationStatus
		{
			// Token: 0x04004BCB RID: 19403
			VALID = 1,
			// Token: 0x04004BCC RID: 19404
			ERROR_INTERNAL = -2,
			// Token: 0x04004BCD RID: 19405
			ERROR_VERSION_UPDATE_REQUIRED = -4
		}
	}
}
