﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D54 RID: 3412
	internal static class AndroidPlatformConfiguration
	{
		// Token: 0x060044EF RID: 17647
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_SetOnLaunchedWithSnapshot(HandleRef self, AndroidPlatformConfiguration.OnLaunchedWithSnapshotCallback callback, IntPtr callback_arg);

		// Token: 0x060044F0 RID: 17648
		[DllImport("gpg")]
		internal static extern IntPtr AndroidPlatformConfiguration_Construct();

		// Token: 0x060044F1 RID: 17649
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_SetOptionalIntentHandlerForUI(HandleRef self, AndroidPlatformConfiguration.IntentHandler intent_handler, IntPtr intent_handler_arg);

		// Token: 0x060044F2 RID: 17650
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_Dispose(HandleRef self);

		// Token: 0x060044F3 RID: 17651
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool AndroidPlatformConfiguration_Valid(HandleRef self);

		// Token: 0x060044F4 RID: 17652
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_SetActivity(HandleRef self, IntPtr android_app_activity);

		// Token: 0x060044F5 RID: 17653
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_SetOnLaunchedWithQuest(HandleRef self, AndroidPlatformConfiguration.OnLaunchedWithQuestCallback callback, IntPtr callback_arg);

		// Token: 0x060044F6 RID: 17654
		[DllImport("gpg")]
		internal static extern void AndroidPlatformConfiguration_SetOptionalViewForPopups(HandleRef self, IntPtr android_view);

		// Token: 0x02000D55 RID: 3413
		// (Invoke) Token: 0x060044F8 RID: 17656
		internal delegate void IntentHandler(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D56 RID: 3414
		// (Invoke) Token: 0x060044FC RID: 17660
		internal delegate void OnLaunchedWithSnapshotCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D57 RID: 3415
		// (Invoke) Token: 0x06004500 RID: 17664
		internal delegate void OnLaunchedWithQuestCallback(IntPtr arg0, IntPtr arg1);
	}
}
