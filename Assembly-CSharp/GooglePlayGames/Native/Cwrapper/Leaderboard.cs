﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D75 RID: 3445
	internal static class Leaderboard
	{
		// Token: 0x06004565 RID: 17765
		[DllImport("gpg")]
		internal static extern UIntPtr Leaderboard_Name(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004566 RID: 17766
		[DllImport("gpg")]
		internal static extern UIntPtr Leaderboard_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004567 RID: 17767
		[DllImport("gpg")]
		internal static extern UIntPtr Leaderboard_IconUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004568 RID: 17768
		[DllImport("gpg")]
		internal static extern void Leaderboard_Dispose(HandleRef self);

		// Token: 0x06004569 RID: 17769
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Leaderboard_Valid(HandleRef self);

		// Token: 0x0600456A RID: 17770
		[DllImport("gpg")]
		internal static extern Types.LeaderboardOrder Leaderboard_Order(HandleRef self);
	}
}
