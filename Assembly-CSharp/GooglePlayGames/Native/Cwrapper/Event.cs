﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D6D RID: 3437
	internal static class Event
	{
		// Token: 0x0600453C RID: 17724
		[DllImport("gpg")]
		internal static extern ulong Event_Count(HandleRef self);

		// Token: 0x0600453D RID: 17725
		[DllImport("gpg")]
		internal static extern UIntPtr Event_Description(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600453E RID: 17726
		[DllImport("gpg")]
		internal static extern UIntPtr Event_ImageUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600453F RID: 17727
		[DllImport("gpg")]
		internal static extern Types.EventVisibility Event_Visibility(HandleRef self);

		// Token: 0x06004540 RID: 17728
		[DllImport("gpg")]
		internal static extern UIntPtr Event_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004541 RID: 17729
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Event_Valid(HandleRef self);

		// Token: 0x06004542 RID: 17730
		[DllImport("gpg")]
		internal static extern void Event_Dispose(HandleRef self);

		// Token: 0x06004543 RID: 17731
		[DllImport("gpg")]
		internal static extern IntPtr Event_Copy(HandleRef self);

		// Token: 0x06004544 RID: 17732
		[DllImport("gpg")]
		internal static extern UIntPtr Event_Name(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
