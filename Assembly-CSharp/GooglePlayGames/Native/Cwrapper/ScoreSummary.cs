﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DB1 RID: 3505
	internal static class ScoreSummary
	{
		// Token: 0x06004709 RID: 18185
		[DllImport("gpg")]
		internal static extern ulong ScoreSummary_ApproximateNumberOfScores(HandleRef self);

		// Token: 0x0600470A RID: 18186
		[DllImport("gpg")]
		internal static extern Types.LeaderboardTimeSpan ScoreSummary_TimeSpan(HandleRef self);

		// Token: 0x0600470B RID: 18187
		[DllImport("gpg")]
		internal static extern UIntPtr ScoreSummary_LeaderboardId(HandleRef self, [In] [Out] char[] out_arg, UIntPtr out_size);

		// Token: 0x0600470C RID: 18188
		[DllImport("gpg")]
		internal static extern Types.LeaderboardCollection ScoreSummary_Collection(HandleRef self);

		// Token: 0x0600470D RID: 18189
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ScoreSummary_Valid(HandleRef self);

		// Token: 0x0600470E RID: 18190
		[DllImport("gpg")]
		internal static extern IntPtr ScoreSummary_CurrentPlayerScore(HandleRef self);

		// Token: 0x0600470F RID: 18191
		[DllImport("gpg")]
		internal static extern void ScoreSummary_Dispose(HandleRef self);
	}
}
