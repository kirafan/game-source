﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D9D RID: 3485
	internal static class RealTimeEventListenerHelper
	{
		// Token: 0x0600467E RID: 18046
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(HandleRef self, RealTimeEventListenerHelper.OnParticipantStatusChangedCallback callback, IntPtr callback_arg);

		// Token: 0x0600467F RID: 18047
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeEventListenerHelper_Construct();

		// Token: 0x06004680 RID: 18048
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(HandleRef self, RealTimeEventListenerHelper.OnP2PDisconnectedCallback callback, IntPtr callback_arg);

		// Token: 0x06004681 RID: 18049
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnDataReceivedCallback(HandleRef self, RealTimeEventListenerHelper.OnDataReceivedCallback callback, IntPtr callback_arg);

		// Token: 0x06004682 RID: 18050
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(HandleRef self, RealTimeEventListenerHelper.OnRoomStatusChangedCallback callback, IntPtr callback_arg);

		// Token: 0x06004683 RID: 18051
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnP2PConnectedCallback(HandleRef self, RealTimeEventListenerHelper.OnP2PConnectedCallback callback, IntPtr callback_arg);

		// Token: 0x06004684 RID: 18052
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(HandleRef self, RealTimeEventListenerHelper.OnRoomConnectedSetChangedCallback callback, IntPtr callback_arg);

		// Token: 0x06004685 RID: 18053
		[DllImport("gpg")]
		internal static extern void RealTimeEventListenerHelper_Dispose(HandleRef self);

		// Token: 0x02000D9E RID: 3486
		// (Invoke) Token: 0x06004687 RID: 18055
		internal delegate void OnRoomStatusChangedCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D9F RID: 3487
		// (Invoke) Token: 0x0600468B RID: 18059
		internal delegate void OnRoomConnectedSetChangedCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DA0 RID: 3488
		// (Invoke) Token: 0x0600468F RID: 18063
		internal delegate void OnP2PConnectedCallback(IntPtr arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000DA1 RID: 3489
		// (Invoke) Token: 0x06004693 RID: 18067
		internal delegate void OnP2PDisconnectedCallback(IntPtr arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000DA2 RID: 3490
		// (Invoke) Token: 0x06004697 RID: 18071
		internal delegate void OnParticipantStatusChangedCallback(IntPtr arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000DA3 RID: 3491
		// (Invoke) Token: 0x0600469B RID: 18075
		internal delegate void OnDataReceivedCallback(IntPtr arg0, IntPtr arg1, IntPtr arg2, UIntPtr arg3, [MarshalAs(UnmanagedType.I1)] bool arg4, IntPtr arg5);
	}
}
