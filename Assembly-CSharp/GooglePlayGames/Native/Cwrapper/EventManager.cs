﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D6E RID: 3438
	internal static class EventManager
	{
		// Token: 0x06004545 RID: 17733
		[DllImport("gpg")]
		internal static extern void EventManager_FetchAll(HandleRef self, Types.DataSource data_source, EventManager.FetchAllCallback callback, IntPtr callback_arg);

		// Token: 0x06004546 RID: 17734
		[DllImport("gpg")]
		internal static extern void EventManager_Fetch(HandleRef self, Types.DataSource data_source, string event_id, EventManager.FetchCallback callback, IntPtr callback_arg);

		// Token: 0x06004547 RID: 17735
		[DllImport("gpg")]
		internal static extern void EventManager_Increment(HandleRef self, string event_id, uint steps);

		// Token: 0x06004548 RID: 17736
		[DllImport("gpg")]
		internal static extern void EventManager_FetchAllResponse_Dispose(HandleRef self);

		// Token: 0x06004549 RID: 17737
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus EventManager_FetchAllResponse_GetStatus(HandleRef self);

		// Token: 0x0600454A RID: 17738
		[DllImport("gpg")]
		internal static extern UIntPtr EventManager_FetchAllResponse_GetData(HandleRef self, IntPtr[] out_arg, UIntPtr out_size);

		// Token: 0x0600454B RID: 17739
		[DllImport("gpg")]
		internal static extern void EventManager_FetchResponse_Dispose(HandleRef self);

		// Token: 0x0600454C RID: 17740
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus EventManager_FetchResponse_GetStatus(HandleRef self);

		// Token: 0x0600454D RID: 17741
		[DllImport("gpg")]
		internal static extern IntPtr EventManager_FetchResponse_GetData(HandleRef self);

		// Token: 0x02000D6F RID: 3439
		// (Invoke) Token: 0x0600454F RID: 17743
		internal delegate void FetchAllCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D70 RID: 3440
		// (Invoke) Token: 0x06004553 RID: 17747
		internal delegate void FetchCallback(IntPtr arg0, IntPtr arg1);
	}
}
