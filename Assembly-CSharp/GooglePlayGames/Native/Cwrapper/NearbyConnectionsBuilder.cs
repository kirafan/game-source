﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D89 RID: 3465
	internal static class NearbyConnectionsBuilder
	{
		// Token: 0x060045F0 RID: 17904
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Builder_SetOnInitializationFinished(HandleRef self, NearbyConnectionsBuilder.OnInitializationFinishedCallback callback, IntPtr callback_arg);

		// Token: 0x060045F1 RID: 17905
		[DllImport("gpg")]
		internal static extern IntPtr NearbyConnections_Builder_Construct();

		// Token: 0x060045F2 RID: 17906
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Builder_SetClientId(HandleRef self, long client_id);

		// Token: 0x060045F3 RID: 17907
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Builder_SetOnLog(HandleRef self, NearbyConnectionsBuilder.OnLogCallback callback, IntPtr callback_arg, Types.LogLevel min_level);

		// Token: 0x060045F4 RID: 17908
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Builder_SetDefaultOnLog(HandleRef self, Types.LogLevel min_level);

		// Token: 0x060045F5 RID: 17909
		[DllImport("gpg")]
		internal static extern IntPtr NearbyConnections_Builder_Create(HandleRef self, IntPtr platform);

		// Token: 0x060045F6 RID: 17910
		[DllImport("gpg")]
		internal static extern void NearbyConnections_Builder_Dispose(HandleRef self);

		// Token: 0x02000D8A RID: 3466
		// (Invoke) Token: 0x060045F8 RID: 17912
		internal delegate void OnInitializationFinishedCallback(NearbyConnectionsStatus.InitializationStatus arg0, IntPtr arg1);

		// Token: 0x02000D8B RID: 3467
		// (Invoke) Token: 0x060045FC RID: 17916
		internal delegate void OnLogCallback(Types.LogLevel arg0, string arg1, IntPtr arg2);
	}
}
