﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D58 RID: 3416
	internal static class Builder
	{
		// Token: 0x06004503 RID: 17667
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnAuthActionStarted(HandleRef self, Builder.OnAuthActionStartedCallback callback, IntPtr callback_arg);

		// Token: 0x06004504 RID: 17668
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_AddOauthScope(HandleRef self, string scope);

		// Token: 0x06004505 RID: 17669
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetLogging(HandleRef self, Builder.OnLogCallback callback, IntPtr callback_arg, Types.LogLevel min_level);

		// Token: 0x06004506 RID: 17670
		[DllImport("gpg")]
		internal static extern IntPtr GameServices_Builder_Construct();

		// Token: 0x06004507 RID: 17671
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_EnableSnapshots(HandleRef self);

		// Token: 0x06004508 RID: 17672
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnLog(HandleRef self, Builder.OnLogCallback callback, IntPtr callback_arg, Types.LogLevel min_level);

		// Token: 0x06004509 RID: 17673
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetDefaultOnLog(HandleRef self, Types.LogLevel min_level);

		// Token: 0x0600450A RID: 17674
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnAuthActionFinished(HandleRef self, Builder.OnAuthActionFinishedCallback callback, IntPtr callback_arg);

		// Token: 0x0600450B RID: 17675
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnTurnBasedMatchEvent(HandleRef self, Builder.OnTurnBasedMatchEventCallback callback, IntPtr callback_arg);

		// Token: 0x0600450C RID: 17676
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnQuestCompleted(HandleRef self, Builder.OnQuestCompletedCallback callback, IntPtr callback_arg);

		// Token: 0x0600450D RID: 17677
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetOnMultiplayerInvitationEvent(HandleRef self, Builder.OnMultiplayerInvitationEventCallback callback, IntPtr callback_arg);

		// Token: 0x0600450E RID: 17678
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_SetShowConnectingPopup(HandleRef self, bool flag);

		// Token: 0x0600450F RID: 17679
		[DllImport("gpg")]
		internal static extern IntPtr GameServices_Builder_Create(HandleRef self, IntPtr platform);

		// Token: 0x06004510 RID: 17680
		[DllImport("gpg")]
		internal static extern void GameServices_Builder_Dispose(HandleRef self);

		// Token: 0x02000D59 RID: 3417
		// (Invoke) Token: 0x06004512 RID: 17682
		internal delegate void OnLogCallback(Types.LogLevel arg0, string arg1, IntPtr arg2);

		// Token: 0x02000D5A RID: 3418
		// (Invoke) Token: 0x06004516 RID: 17686
		internal delegate void OnAuthActionStartedCallback(Types.AuthOperation arg0, IntPtr arg1);

		// Token: 0x02000D5B RID: 3419
		// (Invoke) Token: 0x0600451A RID: 17690
		internal delegate void OnAuthActionFinishedCallback(Types.AuthOperation arg0, CommonErrorStatus.AuthStatus arg1, IntPtr arg2);

		// Token: 0x02000D5C RID: 3420
		// (Invoke) Token: 0x0600451E RID: 17694
		internal delegate void OnMultiplayerInvitationEventCallback(Types.MultiplayerEvent arg0, string arg1, IntPtr arg2, IntPtr arg3);

		// Token: 0x02000D5D RID: 3421
		// (Invoke) Token: 0x06004522 RID: 17698
		internal delegate void OnTurnBasedMatchEventCallback(Types.MultiplayerEvent arg0, string arg1, IntPtr arg2, IntPtr arg3);

		// Token: 0x02000D5E RID: 3422
		// (Invoke) Token: 0x06004526 RID: 17702
		internal delegate void OnQuestCompletedCallback(IntPtr arg0, IntPtr arg1);
	}
}
