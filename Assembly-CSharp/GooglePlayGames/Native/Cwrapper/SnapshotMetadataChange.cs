﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DBA RID: 3514
	internal static class SnapshotMetadataChange
	{
		// Token: 0x06004747 RID: 18247
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotMetadataChange_Description(HandleRef self, [In] [Out] char[] out_arg, UIntPtr out_size);

		// Token: 0x06004748 RID: 18248
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotMetadataChange_Image(HandleRef self);

		// Token: 0x06004749 RID: 18249
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadataChange_PlayedTimeIsChanged(HandleRef self);

		// Token: 0x0600474A RID: 18250
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadataChange_Valid(HandleRef self);

		// Token: 0x0600474B RID: 18251
		[DllImport("gpg")]
		internal static extern ulong SnapshotMetadataChange_PlayedTime(HandleRef self);

		// Token: 0x0600474C RID: 18252
		[DllImport("gpg")]
		internal static extern void SnapshotMetadataChange_Dispose(HandleRef self);

		// Token: 0x0600474D RID: 18253
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadataChange_ImageIsChanged(HandleRef self);

		// Token: 0x0600474E RID: 18254
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadataChange_DescriptionIsChanged(HandleRef self);
	}
}
