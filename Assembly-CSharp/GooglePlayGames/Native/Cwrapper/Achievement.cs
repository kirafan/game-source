﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D4F RID: 3407
	internal static class Achievement
	{
		// Token: 0x060044C7 RID: 17607
		[DllImport("gpg")]
		internal static extern uint Achievement_TotalSteps(HandleRef self);

		// Token: 0x060044C8 RID: 17608
		[DllImport("gpg")]
		internal static extern UIntPtr Achievement_Description(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060044C9 RID: 17609
		[DllImport("gpg")]
		internal static extern void Achievement_Dispose(HandleRef self);

		// Token: 0x060044CA RID: 17610
		[DllImport("gpg")]
		internal static extern UIntPtr Achievement_UnlockedIconUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060044CB RID: 17611
		[DllImport("gpg")]
		internal static extern ulong Achievement_LastModifiedTime(HandleRef self);

		// Token: 0x060044CC RID: 17612
		[DllImport("gpg")]
		internal static extern UIntPtr Achievement_RevealedIconUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060044CD RID: 17613
		[DllImport("gpg")]
		internal static extern uint Achievement_CurrentSteps(HandleRef self);

		// Token: 0x060044CE RID: 17614
		[DllImport("gpg")]
		internal static extern Types.AchievementState Achievement_State(HandleRef self);

		// Token: 0x060044CF RID: 17615
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Achievement_Valid(HandleRef self);

		// Token: 0x060044D0 RID: 17616
		[DllImport("gpg")]
		internal static extern ulong Achievement_LastModified(HandleRef self);

		// Token: 0x060044D1 RID: 17617
		[DllImport("gpg")]
		internal static extern ulong Achievement_XP(HandleRef self);

		// Token: 0x060044D2 RID: 17618
		[DllImport("gpg")]
		internal static extern Types.AchievementType Achievement_Type(HandleRef self);

		// Token: 0x060044D3 RID: 17619
		[DllImport("gpg")]
		internal static extern UIntPtr Achievement_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060044D4 RID: 17620
		[DllImport("gpg")]
		internal static extern UIntPtr Achievement_Name(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
