﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D83 RID: 3459
	internal static class NearbyConnectionTypes
	{
		// Token: 0x060045C3 RID: 17859
		[DllImport("gpg")]
		internal static extern void AppIdentifier_Dispose(HandleRef self);

		// Token: 0x060045C4 RID: 17860
		[DllImport("gpg")]
		internal static extern UIntPtr AppIdentifier_GetIdentifier(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045C5 RID: 17861
		[DllImport("gpg")]
		internal static extern void StartAdvertisingResult_Dispose(HandleRef self);

		// Token: 0x060045C6 RID: 17862
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I4)]
		internal static extern int StartAdvertisingResult_GetStatus(HandleRef self);

		// Token: 0x060045C7 RID: 17863
		[DllImport("gpg")]
		internal static extern UIntPtr StartAdvertisingResult_GetLocalEndpointName(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045C8 RID: 17864
		[DllImport("gpg")]
		internal static extern void EndpointDetails_Dispose(HandleRef self);

		// Token: 0x060045C9 RID: 17865
		[DllImport("gpg")]
		internal static extern UIntPtr EndpointDetails_GetEndpointId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045CA RID: 17866
		[DllImport("gpg")]
		internal static extern UIntPtr EndpointDetails_GetDeviceId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045CB RID: 17867
		[DllImport("gpg")]
		internal static extern UIntPtr EndpointDetails_GetName(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045CC RID: 17868
		[DllImport("gpg")]
		internal static extern UIntPtr EndpointDetails_GetServiceId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045CD RID: 17869
		[DllImport("gpg")]
		internal static extern void ConnectionRequest_Dispose(HandleRef self);

		// Token: 0x060045CE RID: 17870
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionRequest_GetRemoteEndpointId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045CF RID: 17871
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionRequest_GetRemoteDeviceId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045D0 RID: 17872
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionRequest_GetRemoteEndpointName(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045D1 RID: 17873
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionRequest_GetPayload(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045D2 RID: 17874
		[DllImport("gpg")]
		internal static extern void ConnectionResponse_Dispose(HandleRef self);

		// Token: 0x060045D3 RID: 17875
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionResponse_GetRemoteEndpointId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045D4 RID: 17876
		[DllImport("gpg")]
		internal static extern NearbyConnectionTypes.ConnectionResponse_ResponseCode ConnectionResponse_GetStatus(HandleRef self);

		// Token: 0x060045D5 RID: 17877
		[DllImport("gpg")]
		internal static extern UIntPtr ConnectionResponse_GetPayload(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x02000D84 RID: 3460
		internal enum ConnectionResponse_ResponseCode
		{
			// Token: 0x04004BC4 RID: 19396
			ACCEPTED = 1,
			// Token: 0x04004BC5 RID: 19397
			REJECTED,
			// Token: 0x04004BC6 RID: 19398
			ERROR_INTERNAL = -1,
			// Token: 0x04004BC7 RID: 19399
			ERROR_NETWORK_NOT_CONNECTED = -2,
			// Token: 0x04004BC8 RID: 19400
			ERROR_ENDPOINT_ALREADY_CONNECTED = -3,
			// Token: 0x04004BC9 RID: 19401
			ERROR_ENDPOINT_NOT_CONNECTED = -4
		}

		// Token: 0x02000D85 RID: 3461
		// (Invoke) Token: 0x060045D7 RID: 17879
		internal delegate void ConnectionRequestCallback(long arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000D86 RID: 3462
		// (Invoke) Token: 0x060045DB RID: 17883
		internal delegate void StartAdvertisingCallback(long arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000D87 RID: 3463
		// (Invoke) Token: 0x060045DF RID: 17887
		internal delegate void ConnectionResponseCallback(long arg0, IntPtr arg1, IntPtr arg2);
	}
}
