﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D9C RID: 3484
	internal static class QuestMilestone
	{
		// Token: 0x06004674 RID: 18036
		[DllImport("gpg")]
		internal static extern UIntPtr QuestMilestone_EventId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004675 RID: 18037
		[DllImport("gpg")]
		internal static extern ulong QuestMilestone_CurrentCount(HandleRef self);

		// Token: 0x06004676 RID: 18038
		[DllImport("gpg")]
		internal static extern IntPtr QuestMilestone_Copy(HandleRef self);

		// Token: 0x06004677 RID: 18039
		[DllImport("gpg")]
		internal static extern void QuestMilestone_Dispose(HandleRef self);

		// Token: 0x06004678 RID: 18040
		[DllImport("gpg")]
		internal static extern ulong QuestMilestone_TargetCount(HandleRef self);

		// Token: 0x06004679 RID: 18041
		[DllImport("gpg")]
		internal static extern UIntPtr QuestMilestone_QuestId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600467A RID: 18042
		[DllImport("gpg")]
		internal static extern UIntPtr QuestMilestone_CompletionRewardData(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600467B RID: 18043
		[DllImport("gpg")]
		internal static extern Types.QuestMilestoneState QuestMilestone_State(HandleRef self);

		// Token: 0x0600467C RID: 18044
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool QuestMilestone_Valid(HandleRef self);

		// Token: 0x0600467D RID: 18045
		[DllImport("gpg")]
		internal static extern UIntPtr QuestMilestone_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
