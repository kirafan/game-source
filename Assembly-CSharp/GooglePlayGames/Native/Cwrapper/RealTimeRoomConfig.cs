﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DAD RID: 3501
	internal static class RealTimeRoomConfig
	{
		// Token: 0x060046DF RID: 18143
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeRoomConfig_PlayerIdsToInvite_Length(HandleRef self);

		// Token: 0x060046E0 RID: 18144
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeRoomConfig_PlayerIdsToInvite_GetElement(HandleRef self, UIntPtr index, [In] [Out] char[] out_arg, UIntPtr out_size);

		// Token: 0x060046E1 RID: 18145
		[DllImport("gpg")]
		internal static extern uint RealTimeRoomConfig_Variant(HandleRef self);

		// Token: 0x060046E2 RID: 18146
		[DllImport("gpg")]
		internal static extern long RealTimeRoomConfig_ExclusiveBitMask(HandleRef self);

		// Token: 0x060046E3 RID: 18147
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool RealTimeRoomConfig_Valid(HandleRef self);

		// Token: 0x060046E4 RID: 18148
		[DllImport("gpg")]
		internal static extern uint RealTimeRoomConfig_MaximumAutomatchingPlayers(HandleRef self);

		// Token: 0x060046E5 RID: 18149
		[DllImport("gpg")]
		internal static extern uint RealTimeRoomConfig_MinimumAutomatchingPlayers(HandleRef self);

		// Token: 0x060046E6 RID: 18150
		[DllImport("gpg")]
		internal static extern void RealTimeRoomConfig_Dispose(HandleRef self);
	}
}
