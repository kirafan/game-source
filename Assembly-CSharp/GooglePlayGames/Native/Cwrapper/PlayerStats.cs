﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D94 RID: 3476
	internal static class PlayerStats
	{
		// Token: 0x0600462B RID: 17963
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_Valid(HandleRef self);

		// Token: 0x0600462C RID: 17964
		[DllImport("gpg")]
		internal static extern void PlayerStats_Dispose(HandleRef self);

		// Token: 0x0600462D RID: 17965
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasAverageSessionLength(HandleRef self);

		// Token: 0x0600462E RID: 17966
		[DllImport("gpg")]
		internal static extern float PlayerStats_AverageSessionLength(HandleRef self);

		// Token: 0x0600462F RID: 17967
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasChurnProbability(HandleRef self);

		// Token: 0x06004630 RID: 17968
		[DllImport("gpg")]
		internal static extern float PlayerStats_ChurnProbability(HandleRef self);

		// Token: 0x06004631 RID: 17969
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasDaysSinceLastPlayed(HandleRef self);

		// Token: 0x06004632 RID: 17970
		[DllImport("gpg")]
		internal static extern int PlayerStats_DaysSinceLastPlayed(HandleRef self);

		// Token: 0x06004633 RID: 17971
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasNumberOfPurchases(HandleRef self);

		// Token: 0x06004634 RID: 17972
		[DllImport("gpg")]
		internal static extern int PlayerStats_NumberOfPurchases(HandleRef self);

		// Token: 0x06004635 RID: 17973
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasNumberOfSessions(HandleRef self);

		// Token: 0x06004636 RID: 17974
		[DllImport("gpg")]
		internal static extern int PlayerStats_NumberOfSessions(HandleRef self);

		// Token: 0x06004637 RID: 17975
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasSessionPercentile(HandleRef self);

		// Token: 0x06004638 RID: 17976
		[DllImport("gpg")]
		internal static extern float PlayerStats_SessionPercentile(HandleRef self);

		// Token: 0x06004639 RID: 17977
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool PlayerStats_HasSpendPercentile(HandleRef self);

		// Token: 0x0600463A RID: 17978
		[DllImport("gpg")]
		internal static extern float PlayerStats_SpendPercentile(HandleRef self);
	}
}
