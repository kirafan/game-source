﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D8F RID: 3471
	internal static class Player
	{
		// Token: 0x06004605 RID: 17925
		[DllImport("gpg")]
		internal static extern IntPtr Player_CurrentLevel(HandleRef self);

		// Token: 0x06004606 RID: 17926
		[DllImport("gpg")]
		internal static extern UIntPtr Player_Name(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004607 RID: 17927
		[DllImport("gpg")]
		internal static extern void Player_Dispose(HandleRef self);

		// Token: 0x06004608 RID: 17928
		[DllImport("gpg")]
		internal static extern UIntPtr Player_AvatarUrl(HandleRef self, Types.ImageResolution resolution, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004609 RID: 17929
		[DllImport("gpg")]
		internal static extern ulong Player_LastLevelUpTime(HandleRef self);

		// Token: 0x0600460A RID: 17930
		[DllImport("gpg")]
		internal static extern UIntPtr Player_Title(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600460B RID: 17931
		[DllImport("gpg")]
		internal static extern ulong Player_CurrentXP(HandleRef self);

		// Token: 0x0600460C RID: 17932
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Player_Valid(HandleRef self);

		// Token: 0x0600460D RID: 17933
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Player_HasLevelInfo(HandleRef self);

		// Token: 0x0600460E RID: 17934
		[DllImport("gpg")]
		internal static extern IntPtr Player_NextLevel(HandleRef self);

		// Token: 0x0600460F RID: 17935
		[DllImport("gpg")]
		internal static extern UIntPtr Player_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
