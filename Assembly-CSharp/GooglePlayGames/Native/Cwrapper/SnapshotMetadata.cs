﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DB9 RID: 3513
	internal static class SnapshotMetadata
	{
		// Token: 0x0600473F RID: 18239
		[DllImport("gpg")]
		internal static extern void SnapshotMetadata_Dispose(HandleRef self);

		// Token: 0x06004740 RID: 18240
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotMetadata_CoverImageURL(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004741 RID: 18241
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotMetadata_Description(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004742 RID: 18242
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadata_IsOpen(HandleRef self);

		// Token: 0x06004743 RID: 18243
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotMetadata_FileName(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004744 RID: 18244
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool SnapshotMetadata_Valid(HandleRef self);

		// Token: 0x06004745 RID: 18245
		[DllImport("gpg")]
		internal static extern long SnapshotMetadata_PlayedTime(HandleRef self);

		// Token: 0x06004746 RID: 18246
		[DllImport("gpg")]
		internal static extern long SnapshotMetadata_LastModifiedTime(HandleRef self);
	}
}
