﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D90 RID: 3472
	internal static class PlayerManager
	{
		// Token: 0x06004610 RID: 17936
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchInvitable(HandleRef self, Types.DataSource data_source, PlayerManager.FetchListCallback callback, IntPtr callback_arg);

		// Token: 0x06004611 RID: 17937
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchConnected(HandleRef self, Types.DataSource data_source, PlayerManager.FetchListCallback callback, IntPtr callback_arg);

		// Token: 0x06004612 RID: 17938
		[DllImport("gpg")]
		internal static extern void PlayerManager_Fetch(HandleRef self, Types.DataSource data_source, string player_id, PlayerManager.FetchCallback callback, IntPtr callback_arg);

		// Token: 0x06004613 RID: 17939
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchRecentlyPlayed(HandleRef self, Types.DataSource data_source, PlayerManager.FetchListCallback callback, IntPtr callback_arg);

		// Token: 0x06004614 RID: 17940
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchSelf(HandleRef self, Types.DataSource data_source, PlayerManager.FetchSelfCallback callback, IntPtr callback_arg);

		// Token: 0x06004615 RID: 17941
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchSelfResponse_Dispose(HandleRef self);

		// Token: 0x06004616 RID: 17942
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus PlayerManager_FetchSelfResponse_GetStatus(HandleRef self);

		// Token: 0x06004617 RID: 17943
		[DllImport("gpg")]
		internal static extern IntPtr PlayerManager_FetchSelfResponse_GetData(HandleRef self);

		// Token: 0x06004618 RID: 17944
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchResponse_Dispose(HandleRef self);

		// Token: 0x06004619 RID: 17945
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus PlayerManager_FetchResponse_GetStatus(HandleRef self);

		// Token: 0x0600461A RID: 17946
		[DllImport("gpg")]
		internal static extern IntPtr PlayerManager_FetchResponse_GetData(HandleRef self);

		// Token: 0x0600461B RID: 17947
		[DllImport("gpg")]
		internal static extern void PlayerManager_FetchListResponse_Dispose(HandleRef self);

		// Token: 0x0600461C RID: 17948
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus PlayerManager_FetchListResponse_GetStatus(HandleRef self);

		// Token: 0x0600461D RID: 17949
		[DllImport("gpg")]
		internal static extern UIntPtr PlayerManager_FetchListResponse_GetData_Length(HandleRef self);

		// Token: 0x0600461E RID: 17950
		[DllImport("gpg")]
		internal static extern IntPtr PlayerManager_FetchListResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x02000D91 RID: 3473
		// (Invoke) Token: 0x06004620 RID: 17952
		internal delegate void FetchSelfCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D92 RID: 3474
		// (Invoke) Token: 0x06004624 RID: 17956
		internal delegate void FetchCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D93 RID: 3475
		// (Invoke) Token: 0x06004628 RID: 17960
		internal delegate void FetchListCallback(IntPtr arg0, IntPtr arg1);
	}
}
