﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DEB RID: 3563
	internal static class VideoCaptureState
	{
		// Token: 0x060047C8 RID: 18376
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCaptureState_IsCapturing(HandleRef self);

		// Token: 0x060047C9 RID: 18377
		[DllImport("gpg")]
		internal static extern Types.VideoCaptureMode VideoCaptureState_CaptureMode(HandleRef self);

		// Token: 0x060047CA RID: 18378
		[DllImport("gpg")]
		internal static extern Types.VideoQualityLevel VideoCaptureState_QualityLevel(HandleRef self);

		// Token: 0x060047CB RID: 18379
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCaptureState_IsOverlayVisible(HandleRef self);

		// Token: 0x060047CC RID: 18380
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCaptureState_IsPaused(HandleRef self);

		// Token: 0x060047CD RID: 18381
		[DllImport("gpg")]
		internal static extern void VideoCaptureState_Dispose(HandleRef self);

		// Token: 0x060047CE RID: 18382
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCaptureState_Valid(HandleRef self);
	}
}
