﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D8E RID: 3470
	internal static class ParticipantResults
	{
		// Token: 0x060045FF RID: 17919
		[DllImport("gpg")]
		internal static extern IntPtr ParticipantResults_WithResult(HandleRef self, string participant_id, uint placing, Types.MatchResult result);

		// Token: 0x06004600 RID: 17920
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ParticipantResults_Valid(HandleRef self);

		// Token: 0x06004601 RID: 17921
		[DllImport("gpg")]
		internal static extern Types.MatchResult ParticipantResults_MatchResultForParticipant(HandleRef self, string participant_id);

		// Token: 0x06004602 RID: 17922
		[DllImport("gpg")]
		internal static extern uint ParticipantResults_PlaceForParticipant(HandleRef self, string participant_id);

		// Token: 0x06004603 RID: 17923
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool ParticipantResults_HasResultsForParticipant(HandleRef self, string participant_id);

		// Token: 0x06004604 RID: 17924
		[DllImport("gpg")]
		internal static extern void ParticipantResults_Dispose(HandleRef self);
	}
}
