﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D5F RID: 3423
	internal static class CaptureOverlayStateListenerHelper
	{
		// Token: 0x06004529 RID: 17705
		[DllImport("gpg")]
		internal static extern void CaptureOverlayStateListenerHelper_SetOnCaptureOverlayStateChangedCallback(HandleRef self, CaptureOverlayStateListenerHelper.OnCaptureOverlayStateChangedCallback callback, IntPtr callback_arg);

		// Token: 0x0600452A RID: 17706
		[DllImport("gpg")]
		internal static extern IntPtr CaptureOverlayStateListenerHelper_Construct();

		// Token: 0x0600452B RID: 17707
		[DllImport("gpg")]
		internal static extern void CaptureOverlayStateListenerHelper_Dispose(HandleRef self);

		// Token: 0x02000D60 RID: 3424
		// (Invoke) Token: 0x0600452D RID: 17709
		internal delegate void OnCaptureOverlayStateChangedCallback(Types.VideoCaptureOverlayState arg0, IntPtr arg1);
	}
}
