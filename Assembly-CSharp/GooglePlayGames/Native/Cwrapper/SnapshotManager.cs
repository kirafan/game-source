﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DB3 RID: 3507
	internal static class SnapshotManager
	{
		// Token: 0x06004711 RID: 18193
		[DllImport("gpg")]
		internal static extern void SnapshotManager_FetchAll(HandleRef self, Types.DataSource data_source, SnapshotManager.FetchAllCallback callback, IntPtr callback_arg);

		// Token: 0x06004712 RID: 18194
		[DllImport("gpg")]
		internal static extern void SnapshotManager_ShowSelectUIOperation(HandleRef self, [MarshalAs(UnmanagedType.I1)] bool allow_create, [MarshalAs(UnmanagedType.I1)] bool allow_delete, uint max_snapshots, string title, SnapshotManager.SnapshotSelectUICallback callback, IntPtr callback_arg);

		// Token: 0x06004713 RID: 18195
		[DllImport("gpg")]
		internal static extern void SnapshotManager_Read(HandleRef self, IntPtr snapshot_metadata, SnapshotManager.ReadCallback callback, IntPtr callback_arg);

		// Token: 0x06004714 RID: 18196
		[DllImport("gpg")]
		internal static extern void SnapshotManager_Commit(HandleRef self, IntPtr snapshot_metadata, IntPtr metadata_change, byte[] data, UIntPtr data_size, SnapshotManager.CommitCallback callback, IntPtr callback_arg);

		// Token: 0x06004715 RID: 18197
		[DllImport("gpg")]
		internal static extern void SnapshotManager_Open(HandleRef self, Types.DataSource data_source, string file_name, Types.SnapshotConflictPolicy conflict_policy, SnapshotManager.OpenCallback callback, IntPtr callback_arg);

		// Token: 0x06004716 RID: 18198
		[DllImport("gpg")]
		internal static extern void SnapshotManager_ResolveConflict(HandleRef self, IntPtr snapshot_metadata, IntPtr metadata_change, string conflict_id, SnapshotManager.CommitCallback callback, IntPtr callback_arg);

		// Token: 0x06004717 RID: 18199
		[DllImport("gpg")]
		internal static extern void SnapshotManager_Delete(HandleRef self, IntPtr snapshot_metadata);

		// Token: 0x06004718 RID: 18200
		[DllImport("gpg")]
		internal static extern void SnapshotManager_FetchAllResponse_Dispose(HandleRef self);

		// Token: 0x06004719 RID: 18201
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus SnapshotManager_FetchAllResponse_GetStatus(HandleRef self);

		// Token: 0x0600471A RID: 18202
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotManager_FetchAllResponse_GetData_Length(HandleRef self);

		// Token: 0x0600471B RID: 18203
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_FetchAllResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x0600471C RID: 18204
		[DllImport("gpg")]
		internal static extern void SnapshotManager_OpenResponse_Dispose(HandleRef self);

		// Token: 0x0600471D RID: 18205
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.SnapshotOpenStatus SnapshotManager_OpenResponse_GetStatus(HandleRef self);

		// Token: 0x0600471E RID: 18206
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_OpenResponse_GetData(HandleRef self);

		// Token: 0x0600471F RID: 18207
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotManager_OpenResponse_GetConflictId(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004720 RID: 18208
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_OpenResponse_GetConflictOriginal(HandleRef self);

		// Token: 0x06004721 RID: 18209
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_OpenResponse_GetConflictUnmerged(HandleRef self);

		// Token: 0x06004722 RID: 18210
		[DllImport("gpg")]
		internal static extern void SnapshotManager_CommitResponse_Dispose(HandleRef self);

		// Token: 0x06004723 RID: 18211
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus SnapshotManager_CommitResponse_GetStatus(HandleRef self);

		// Token: 0x06004724 RID: 18212
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_CommitResponse_GetData(HandleRef self);

		// Token: 0x06004725 RID: 18213
		[DllImport("gpg")]
		internal static extern void SnapshotManager_ReadResponse_Dispose(HandleRef self);

		// Token: 0x06004726 RID: 18214
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus SnapshotManager_ReadResponse_GetStatus(HandleRef self);

		// Token: 0x06004727 RID: 18215
		[DllImport("gpg")]
		internal static extern UIntPtr SnapshotManager_ReadResponse_GetData(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004728 RID: 18216
		[DllImport("gpg")]
		internal static extern void SnapshotManager_SnapshotSelectUIResponse_Dispose(HandleRef self);

		// Token: 0x06004729 RID: 18217
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.UIStatus SnapshotManager_SnapshotSelectUIResponse_GetStatus(HandleRef self);

		// Token: 0x0600472A RID: 18218
		[DllImport("gpg")]
		internal static extern IntPtr SnapshotManager_SnapshotSelectUIResponse_GetData(HandleRef self);

		// Token: 0x02000DB4 RID: 3508
		// (Invoke) Token: 0x0600472C RID: 18220
		internal delegate void FetchAllCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DB5 RID: 3509
		// (Invoke) Token: 0x06004730 RID: 18224
		internal delegate void OpenCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DB6 RID: 3510
		// (Invoke) Token: 0x06004734 RID: 18228
		internal delegate void CommitCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DB7 RID: 3511
		// (Invoke) Token: 0x06004738 RID: 18232
		internal delegate void ReadCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000DB8 RID: 3512
		// (Invoke) Token: 0x0600473C RID: 18236
		internal delegate void SnapshotSelectUICallback(IntPtr arg0, IntPtr arg1);
	}
}
