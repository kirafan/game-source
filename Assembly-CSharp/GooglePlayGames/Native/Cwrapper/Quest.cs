﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D95 RID: 3477
	internal static class Quest
	{
		// Token: 0x0600463B RID: 17979
		[DllImport("gpg")]
		internal static extern UIntPtr Quest_Description(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600463C RID: 17980
		[DllImport("gpg")]
		internal static extern UIntPtr Quest_BannerUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600463D RID: 17981
		[DllImport("gpg")]
		internal static extern long Quest_ExpirationTime(HandleRef self);

		// Token: 0x0600463E RID: 17982
		[DllImport("gpg")]
		internal static extern UIntPtr Quest_IconUrl(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x0600463F RID: 17983
		[DllImport("gpg")]
		internal static extern Types.QuestState Quest_State(HandleRef self);

		// Token: 0x06004640 RID: 17984
		[DllImport("gpg")]
		internal static extern IntPtr Quest_Copy(HandleRef self);

		// Token: 0x06004641 RID: 17985
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Quest_Valid(HandleRef self);

		// Token: 0x06004642 RID: 17986
		[DllImport("gpg")]
		internal static extern long Quest_StartTime(HandleRef self);

		// Token: 0x06004643 RID: 17987
		[DllImport("gpg")]
		internal static extern void Quest_Dispose(HandleRef self);

		// Token: 0x06004644 RID: 17988
		[DllImport("gpg")]
		internal static extern IntPtr Quest_CurrentMilestone(HandleRef self);

		// Token: 0x06004645 RID: 17989
		[DllImport("gpg")]
		internal static extern long Quest_AcceptedTime(HandleRef self);

		// Token: 0x06004646 RID: 17990
		[DllImport("gpg")]
		internal static extern UIntPtr Quest_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x06004647 RID: 17991
		[DllImport("gpg")]
		internal static extern UIntPtr Quest_Name(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
