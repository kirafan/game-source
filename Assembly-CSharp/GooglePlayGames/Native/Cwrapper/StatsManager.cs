﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DBC RID: 3516
	internal static class StatsManager
	{
		// Token: 0x06004755 RID: 18261
		[DllImport("gpg")]
		internal static extern void StatsManager_FetchForPlayer(HandleRef self, Types.DataSource data_source, StatsManager.FetchForPlayerCallback callback, IntPtr callback_arg);

		// Token: 0x06004756 RID: 18262
		[DllImport("gpg")]
		internal static extern void StatsManager_FetchForPlayerResponse_Dispose(HandleRef self);

		// Token: 0x06004757 RID: 18263
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus StatsManager_FetchForPlayerResponse_GetStatus(HandleRef self);

		// Token: 0x06004758 RID: 18264
		[DllImport("gpg")]
		internal static extern IntPtr StatsManager_FetchForPlayerResponse_GetData(HandleRef self);

		// Token: 0x02000DBD RID: 3517
		// (Invoke) Token: 0x0600475A RID: 18266
		internal delegate void FetchForPlayerCallback(IntPtr arg0, IntPtr arg1);
	}
}
