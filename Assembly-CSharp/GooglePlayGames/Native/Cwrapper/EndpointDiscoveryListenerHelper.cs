﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D6A RID: 3434
	internal static class EndpointDiscoveryListenerHelper
	{
		// Token: 0x06004530 RID: 17712
		[DllImport("gpg")]
		internal static extern IntPtr EndpointDiscoveryListenerHelper_Construct();

		// Token: 0x06004531 RID: 17713
		[DllImport("gpg")]
		internal static extern void EndpointDiscoveryListenerHelper_SetOnEndpointLostCallback(HandleRef self, EndpointDiscoveryListenerHelper.OnEndpointLostCallback callback, IntPtr callback_arg);

		// Token: 0x06004532 RID: 17714
		[DllImport("gpg")]
		internal static extern void EndpointDiscoveryListenerHelper_SetOnEndpointFoundCallback(HandleRef self, EndpointDiscoveryListenerHelper.OnEndpointFoundCallback callback, IntPtr callback_arg);

		// Token: 0x06004533 RID: 17715
		[DllImport("gpg")]
		internal static extern void EndpointDiscoveryListenerHelper_Dispose(HandleRef self);

		// Token: 0x02000D6B RID: 3435
		// (Invoke) Token: 0x06004535 RID: 17717
		internal delegate void OnEndpointFoundCallback(long arg0, IntPtr arg1, IntPtr arg2);

		// Token: 0x02000D6C RID: 3436
		// (Invoke) Token: 0x06004539 RID: 17721
		internal delegate void OnEndpointLostCallback(long arg0, string arg1, IntPtr arg2);
	}
}
