﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D74 RID: 3444
	internal static class IosPlatformConfiguration
	{
		// Token: 0x06004561 RID: 17761
		[DllImport("gpg")]
		internal static extern IntPtr IosPlatformConfiguration_Construct();

		// Token: 0x06004562 RID: 17762
		[DllImport("gpg")]
		internal static extern void IosPlatformConfiguration_Dispose(HandleRef self);

		// Token: 0x06004563 RID: 17763
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool IosPlatformConfiguration_Valid(HandleRef self);

		// Token: 0x06004564 RID: 17764
		[DllImport("gpg")]
		internal static extern void IosPlatformConfiguration_SetClientID(HandleRef self, string client_id);
	}
}
