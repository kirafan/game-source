﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D82 RID: 3458
	internal static class MultiplayerParticipant
	{
		// Token: 0x060045B7 RID: 17847
		[DllImport("gpg")]
		internal static extern Types.ParticipantStatus MultiplayerParticipant_Status(HandleRef self);

		// Token: 0x060045B8 RID: 17848
		[DllImport("gpg")]
		internal static extern uint MultiplayerParticipant_MatchRank(HandleRef self);

		// Token: 0x060045B9 RID: 17849
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool MultiplayerParticipant_IsConnectedToRoom(HandleRef self);

		// Token: 0x060045BA RID: 17850
		[DllImport("gpg")]
		internal static extern UIntPtr MultiplayerParticipant_DisplayName(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045BB RID: 17851
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool MultiplayerParticipant_HasPlayer(HandleRef self);

		// Token: 0x060045BC RID: 17852
		[DllImport("gpg")]
		internal static extern UIntPtr MultiplayerParticipant_AvatarUrl(HandleRef self, Types.ImageResolution resolution, [In] [Out] char[] out_arg, UIntPtr out_size);

		// Token: 0x060045BD RID: 17853
		[DllImport("gpg")]
		internal static extern Types.MatchResult MultiplayerParticipant_MatchResult(HandleRef self);

		// Token: 0x060045BE RID: 17854
		[DllImport("gpg")]
		internal static extern IntPtr MultiplayerParticipant_Player(HandleRef self);

		// Token: 0x060045BF RID: 17855
		[DllImport("gpg")]
		internal static extern void MultiplayerParticipant_Dispose(HandleRef self);

		// Token: 0x060045C0 RID: 17856
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool MultiplayerParticipant_Valid(HandleRef self);

		// Token: 0x060045C1 RID: 17857
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool MultiplayerParticipant_HasMatchResult(HandleRef self);

		// Token: 0x060045C2 RID: 17858
		[DllImport("gpg")]
		internal static extern UIntPtr MultiplayerParticipant_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
