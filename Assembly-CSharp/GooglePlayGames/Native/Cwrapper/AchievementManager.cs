﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D50 RID: 3408
	internal static class AchievementManager
	{
		// Token: 0x060044D5 RID: 17621
		[DllImport("gpg")]
		internal static extern void AchievementManager_FetchAll(HandleRef self, Types.DataSource data_source, AchievementManager.FetchAllCallback callback, IntPtr callback_arg);

		// Token: 0x060044D6 RID: 17622
		[DllImport("gpg")]
		internal static extern void AchievementManager_Reveal(HandleRef self, string achievement_id);

		// Token: 0x060044D7 RID: 17623
		[DllImport("gpg")]
		internal static extern void AchievementManager_Unlock(HandleRef self, string achievement_id);

		// Token: 0x060044D8 RID: 17624
		[DllImport("gpg")]
		internal static extern void AchievementManager_ShowAllUI(HandleRef self, AchievementManager.ShowAllUICallback callback, IntPtr callback_arg);

		// Token: 0x060044D9 RID: 17625
		[DllImport("gpg")]
		internal static extern void AchievementManager_SetStepsAtLeast(HandleRef self, string achievement_id, uint steps);

		// Token: 0x060044DA RID: 17626
		[DllImport("gpg")]
		internal static extern void AchievementManager_Increment(HandleRef self, string achievement_id, uint steps);

		// Token: 0x060044DB RID: 17627
		[DllImport("gpg")]
		internal static extern void AchievementManager_Fetch(HandleRef self, Types.DataSource data_source, string achievement_id, AchievementManager.FetchCallback callback, IntPtr callback_arg);

		// Token: 0x060044DC RID: 17628
		[DllImport("gpg")]
		internal static extern void AchievementManager_FetchAllResponse_Dispose(HandleRef self);

		// Token: 0x060044DD RID: 17629
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus AchievementManager_FetchAllResponse_GetStatus(HandleRef self);

		// Token: 0x060044DE RID: 17630
		[DllImport("gpg")]
		internal static extern UIntPtr AchievementManager_FetchAllResponse_GetData_Length(HandleRef self);

		// Token: 0x060044DF RID: 17631
		[DllImport("gpg")]
		internal static extern IntPtr AchievementManager_FetchAllResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060044E0 RID: 17632
		[DllImport("gpg")]
		internal static extern void AchievementManager_FetchResponse_Dispose(HandleRef self);

		// Token: 0x060044E1 RID: 17633
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus AchievementManager_FetchResponse_GetStatus(HandleRef self);

		// Token: 0x060044E2 RID: 17634
		[DllImport("gpg")]
		internal static extern IntPtr AchievementManager_FetchResponse_GetData(HandleRef self);

		// Token: 0x02000D51 RID: 3409
		// (Invoke) Token: 0x060044E4 RID: 17636
		internal delegate void FetchAllCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D52 RID: 3410
		// (Invoke) Token: 0x060044E8 RID: 17640
		internal delegate void FetchCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D53 RID: 3411
		// (Invoke) Token: 0x060044EC RID: 17644
		internal delegate void ShowAllUICallback(CommonErrorStatus.UIStatus arg0, IntPtr arg1);
	}
}
