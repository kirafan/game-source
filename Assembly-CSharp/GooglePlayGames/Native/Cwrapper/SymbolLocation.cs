﻿using System;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DC8 RID: 3528
	internal static class SymbolLocation
	{
		// Token: 0x04004C10 RID: 19472
		internal const string NativeSymbolLocation = "gpg";
	}
}
