﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DEA RID: 3562
	internal static class VideoCapabilities
	{
		// Token: 0x060047C1 RID: 18369
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_IsCameraSupported(HandleRef self);

		// Token: 0x060047C2 RID: 18370
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_IsMicSupported(HandleRef self);

		// Token: 0x060047C3 RID: 18371
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_IsWriteStorageSupported(HandleRef self);

		// Token: 0x060047C4 RID: 18372
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_SupportsCaptureMode(HandleRef self, Types.VideoCaptureMode capture_mode);

		// Token: 0x060047C5 RID: 18373
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_SupportsQualityLevel(HandleRef self, Types.VideoQualityLevel quality_level);

		// Token: 0x060047C6 RID: 18374
		[DllImport("gpg")]
		internal static extern void VideoCapabilities_Dispose(HandleRef self);

		// Token: 0x060047C7 RID: 18375
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool VideoCapabilities_Valid(HandleRef self);
	}
}
