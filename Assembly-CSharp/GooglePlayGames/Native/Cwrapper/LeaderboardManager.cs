﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D76 RID: 3446
	internal static class LeaderboardManager
	{
		// Token: 0x0600456B RID: 17771
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchAll(HandleRef self, Types.DataSource data_source, LeaderboardManager.FetchAllCallback callback, IntPtr callback_arg);

		// Token: 0x0600456C RID: 17772
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchScoreSummary(HandleRef self, Types.DataSource data_source, string leaderboard_id, Types.LeaderboardTimeSpan time_span, Types.LeaderboardCollection collection, LeaderboardManager.FetchScoreSummaryCallback callback, IntPtr callback_arg);

		// Token: 0x0600456D RID: 17773
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_ScorePageToken(HandleRef self, string leaderboard_id, Types.LeaderboardStart start, Types.LeaderboardTimeSpan time_span, Types.LeaderboardCollection collection);

		// Token: 0x0600456E RID: 17774
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_ShowAllUI(HandleRef self, LeaderboardManager.ShowAllUICallback callback, IntPtr callback_arg);

		// Token: 0x0600456F RID: 17775
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchScorePage(HandleRef self, Types.DataSource data_source, IntPtr token, uint max_results, LeaderboardManager.FetchScorePageCallback callback, IntPtr callback_arg);

		// Token: 0x06004570 RID: 17776
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchAllScoreSummaries(HandleRef self, Types.DataSource data_source, string leaderboard_id, LeaderboardManager.FetchAllScoreSummariesCallback callback, IntPtr callback_arg);

		// Token: 0x06004571 RID: 17777
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_ShowUI(HandleRef self, string leaderboard_id, Types.LeaderboardTimeSpan time_span, LeaderboardManager.ShowUICallback callback, IntPtr callback_arg);

		// Token: 0x06004572 RID: 17778
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_Fetch(HandleRef self, Types.DataSource data_source, string leaderboard_id, LeaderboardManager.FetchCallback callback, IntPtr callback_arg);

		// Token: 0x06004573 RID: 17779
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_SubmitScore(HandleRef self, string leaderboard_id, ulong score, string metadata);

		// Token: 0x06004574 RID: 17780
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchResponse_Dispose(HandleRef self);

		// Token: 0x06004575 RID: 17781
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus LeaderboardManager_FetchResponse_GetStatus(HandleRef self);

		// Token: 0x06004576 RID: 17782
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_FetchResponse_GetData(HandleRef self);

		// Token: 0x06004577 RID: 17783
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchAllResponse_Dispose(HandleRef self);

		// Token: 0x06004578 RID: 17784
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus LeaderboardManager_FetchAllResponse_GetStatus(HandleRef self);

		// Token: 0x06004579 RID: 17785
		[DllImport("gpg")]
		internal static extern UIntPtr LeaderboardManager_FetchAllResponse_GetData_Length(HandleRef self);

		// Token: 0x0600457A RID: 17786
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_FetchAllResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x0600457B RID: 17787
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchScorePageResponse_Dispose(HandleRef self);

		// Token: 0x0600457C RID: 17788
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus LeaderboardManager_FetchScorePageResponse_GetStatus(HandleRef self);

		// Token: 0x0600457D RID: 17789
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_FetchScorePageResponse_GetData(HandleRef self);

		// Token: 0x0600457E RID: 17790
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchScoreSummaryResponse_Dispose(HandleRef self);

		// Token: 0x0600457F RID: 17791
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus LeaderboardManager_FetchScoreSummaryResponse_GetStatus(HandleRef self);

		// Token: 0x06004580 RID: 17792
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_FetchScoreSummaryResponse_GetData(HandleRef self);

		// Token: 0x06004581 RID: 17793
		[DllImport("gpg")]
		internal static extern void LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(HandleRef self);

		// Token: 0x06004582 RID: 17794
		[DllImport("gpg")]
		internal static extern CommonErrorStatus.ResponseStatus LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(HandleRef self);

		// Token: 0x06004583 RID: 17795
		[DllImport("gpg")]
		internal static extern UIntPtr LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(HandleRef self);

		// Token: 0x06004584 RID: 17796
		[DllImport("gpg")]
		internal static extern IntPtr LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x02000D77 RID: 3447
		// (Invoke) Token: 0x06004586 RID: 17798
		internal delegate void FetchCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D78 RID: 3448
		// (Invoke) Token: 0x0600458A RID: 17802
		internal delegate void FetchAllCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D79 RID: 3449
		// (Invoke) Token: 0x0600458E RID: 17806
		internal delegate void FetchScorePageCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D7A RID: 3450
		// (Invoke) Token: 0x06004592 RID: 17810
		internal delegate void FetchScoreSummaryCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D7B RID: 3451
		// (Invoke) Token: 0x06004596 RID: 17814
		internal delegate void FetchAllScoreSummariesCallback(IntPtr arg0, IntPtr arg1);

		// Token: 0x02000D7C RID: 3452
		// (Invoke) Token: 0x0600459A RID: 17818
		internal delegate void ShowAllUICallback(CommonErrorStatus.UIStatus arg0, IntPtr arg1);

		// Token: 0x02000D7D RID: 3453
		// (Invoke) Token: 0x0600459E RID: 17822
		internal delegate void ShowUICallback(CommonErrorStatus.UIStatus arg0, IntPtr arg1);
	}
}
