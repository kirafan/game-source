﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D7E RID: 3454
	internal static class MessageListenerHelper
	{
		// Token: 0x060045A1 RID: 17825
		[DllImport("gpg")]
		internal static extern void MessageListenerHelper_SetOnMessageReceivedCallback(HandleRef self, MessageListenerHelper.OnMessageReceivedCallback callback, IntPtr callback_arg);

		// Token: 0x060045A2 RID: 17826
		[DllImport("gpg")]
		internal static extern void MessageListenerHelper_SetOnDisconnectedCallback(HandleRef self, MessageListenerHelper.OnDisconnectedCallback callback, IntPtr callback_arg);

		// Token: 0x060045A3 RID: 17827
		[DllImport("gpg")]
		internal static extern IntPtr MessageListenerHelper_Construct();

		// Token: 0x060045A4 RID: 17828
		[DllImport("gpg")]
		internal static extern void MessageListenerHelper_Dispose(HandleRef self);

		// Token: 0x02000D7F RID: 3455
		// (Invoke) Token: 0x060045A6 RID: 17830
		internal delegate void OnMessageReceivedCallback(long arg0, string arg1, IntPtr arg2, UIntPtr arg3, [MarshalAs(UnmanagedType.I1)] bool arg4, IntPtr arg5);

		// Token: 0x02000D80 RID: 3456
		// (Invoke) Token: 0x060045AA RID: 17834
		internal delegate void OnDisconnectedCallback(long arg0, string arg1, IntPtr arg2);
	}
}
