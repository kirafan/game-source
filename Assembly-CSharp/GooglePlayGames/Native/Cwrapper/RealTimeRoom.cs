﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DAC RID: 3500
	internal static class RealTimeRoom
	{
		// Token: 0x060046D3 RID: 18131
		[DllImport("gpg")]
		internal static extern Types.RealTimeRoomStatus RealTimeRoom_Status(HandleRef self);

		// Token: 0x060046D4 RID: 18132
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeRoom_Description(HandleRef self, [In] [Out] char[] out_arg, UIntPtr out_size);

		// Token: 0x060046D5 RID: 18133
		[DllImport("gpg")]
		internal static extern uint RealTimeRoom_Variant(HandleRef self);

		// Token: 0x060046D6 RID: 18134
		[DllImport("gpg")]
		internal static extern ulong RealTimeRoom_CreationTime(HandleRef self);

		// Token: 0x060046D7 RID: 18135
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeRoom_Participants_Length(HandleRef self);

		// Token: 0x060046D8 RID: 18136
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeRoom_Participants_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060046D9 RID: 18137
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool RealTimeRoom_Valid(HandleRef self);

		// Token: 0x060046DA RID: 18138
		[DllImport("gpg")]
		internal static extern uint RealTimeRoom_RemainingAutomatchingSlots(HandleRef self);

		// Token: 0x060046DB RID: 18139
		[DllImport("gpg")]
		internal static extern ulong RealTimeRoom_AutomatchWaitEstimate(HandleRef self);

		// Token: 0x060046DC RID: 18140
		[DllImport("gpg")]
		internal static extern IntPtr RealTimeRoom_CreatingParticipant(HandleRef self);

		// Token: 0x060046DD RID: 18141
		[DllImport("gpg")]
		internal static extern UIntPtr RealTimeRoom_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060046DE RID: 18142
		[DllImport("gpg")]
		internal static extern void RealTimeRoom_Dispose(HandleRef self);
	}
}
