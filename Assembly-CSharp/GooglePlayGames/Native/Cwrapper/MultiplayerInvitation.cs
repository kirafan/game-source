﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000D81 RID: 3457
	internal static class MultiplayerInvitation
	{
		// Token: 0x060045AD RID: 17837
		[DllImport("gpg")]
		internal static extern uint MultiplayerInvitation_AutomatchingSlotsAvailable(HandleRef self);

		// Token: 0x060045AE RID: 17838
		[DllImport("gpg")]
		internal static extern IntPtr MultiplayerInvitation_InvitingParticipant(HandleRef self);

		// Token: 0x060045AF RID: 17839
		[DllImport("gpg")]
		internal static extern uint MultiplayerInvitation_Variant(HandleRef self);

		// Token: 0x060045B0 RID: 17840
		[DllImport("gpg")]
		internal static extern ulong MultiplayerInvitation_CreationTime(HandleRef self);

		// Token: 0x060045B1 RID: 17841
		[DllImport("gpg")]
		internal static extern UIntPtr MultiplayerInvitation_Participants_Length(HandleRef self);

		// Token: 0x060045B2 RID: 17842
		[DllImport("gpg")]
		internal static extern IntPtr MultiplayerInvitation_Participants_GetElement(HandleRef self, UIntPtr index);

		// Token: 0x060045B3 RID: 17843
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool MultiplayerInvitation_Valid(HandleRef self);

		// Token: 0x060045B4 RID: 17844
		[DllImport("gpg")]
		internal static extern Types.MultiplayerInvitationType MultiplayerInvitation_Type(HandleRef self);

		// Token: 0x060045B5 RID: 17845
		[DllImport("gpg")]
		internal static extern UIntPtr MultiplayerInvitation_Id(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);

		// Token: 0x060045B6 RID: 17846
		[DllImport("gpg")]
		internal static extern void MultiplayerInvitation_Dispose(HandleRef self);
	}
}
