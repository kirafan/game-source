﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DCB RID: 3531
	internal static class TurnBasedMatchConfigBuilder
	{
		// Token: 0x0600477D RID: 18301
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(HandleRef self, IntPtr response);

		// Token: 0x0600477E RID: 18302
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_SetVariant(HandleRef self, uint variant);

		// Token: 0x0600477F RID: 18303
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_AddPlayerToInvite(HandleRef self, string player_id);

		// Token: 0x06004780 RID: 18304
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatchConfig_Builder_Construct();

		// Token: 0x06004781 RID: 18305
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_SetExclusiveBitMask(HandleRef self, ulong exclusive_bit_mask);

		// Token: 0x06004782 RID: 18306
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(HandleRef self, uint maximum_automatching_players);

		// Token: 0x06004783 RID: 18307
		[DllImport("gpg")]
		internal static extern IntPtr TurnBasedMatchConfig_Builder_Create(HandleRef self);

		// Token: 0x06004784 RID: 18308
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(HandleRef self, uint minimum_automatching_players);

		// Token: 0x06004785 RID: 18309
		[DllImport("gpg")]
		internal static extern void TurnBasedMatchConfig_Builder_Dispose(HandleRef self);
	}
}
