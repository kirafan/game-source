﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.Cwrapper
{
	// Token: 0x02000DAF RID: 3503
	internal static class Score
	{
		// Token: 0x060046F0 RID: 18160
		[DllImport("gpg")]
		internal static extern ulong Score_Value(HandleRef self);

		// Token: 0x060046F1 RID: 18161
		[DllImport("gpg")]
		[return: MarshalAs(UnmanagedType.I1)]
		internal static extern bool Score_Valid(HandleRef self);

		// Token: 0x060046F2 RID: 18162
		[DllImport("gpg")]
		internal static extern ulong Score_Rank(HandleRef self);

		// Token: 0x060046F3 RID: 18163
		[DllImport("gpg")]
		internal static extern void Score_Dispose(HandleRef self);

		// Token: 0x060046F4 RID: 18164
		[DllImport("gpg")]
		internal static extern UIntPtr Score_Metadata(HandleRef self, [In] [Out] byte[] out_arg, UIntPtr out_size);
	}
}
