﻿using System;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.Cwrapper;
using UnityEngine;

namespace GooglePlayGames.Native
{
	// Token: 0x02000D4E RID: 3406
	internal static class ConversionUtils
	{
		// Token: 0x060044BF RID: 17599 RVA: 0x00146D50 File Offset: 0x00145150
		internal static ResponseStatus ConvertResponseStatus(CommonErrorStatus.ResponseStatus status)
		{
			switch (status + 5)
			{
			case (CommonErrorStatus.ResponseStatus)0:
				return ResponseStatus.Timeout;
			case CommonErrorStatus.ResponseStatus.VALID:
				return ResponseStatus.VersionUpdateRequired;
			case CommonErrorStatus.ResponseStatus.VALID_BUT_STALE:
				return ResponseStatus.NotAuthorized;
			case (CommonErrorStatus.ResponseStatus)3:
				return ResponseStatus.InternalError;
			case (CommonErrorStatus.ResponseStatus)4:
				return ResponseStatus.LicenseCheckFailed;
			case (CommonErrorStatus.ResponseStatus)6:
				return ResponseStatus.Success;
			case (CommonErrorStatus.ResponseStatus)7:
				return ResponseStatus.SuccessWithStale;
			}
			throw new InvalidOperationException("Unknown status: " + status);
		}

		// Token: 0x060044C0 RID: 17600 RVA: 0x00146DB4 File Offset: 0x001451B4
		internal static CommonStatusCodes ConvertResponseStatusToCommonStatus(CommonErrorStatus.ResponseStatus status)
		{
			switch (status + 5)
			{
			case (CommonErrorStatus.ResponseStatus)0:
				return CommonStatusCodes.Timeout;
			case CommonErrorStatus.ResponseStatus.VALID:
				return CommonStatusCodes.ServiceVersionUpdateRequired;
			case CommonErrorStatus.ResponseStatus.VALID_BUT_STALE:
				return CommonStatusCodes.AuthApiAccessForbidden;
			case (CommonErrorStatus.ResponseStatus)3:
				return CommonStatusCodes.InternalError;
			case (CommonErrorStatus.ResponseStatus)4:
				return CommonStatusCodes.LicenseCheckFailed;
			case (CommonErrorStatus.ResponseStatus)6:
				return CommonStatusCodes.Success;
			case (CommonErrorStatus.ResponseStatus)7:
				return CommonStatusCodes.SuccessCached;
			}
			Debug.LogWarning("Unknown ResponseStatus: " + status + ", defaulting to CommonStatusCodes.Error");
			return CommonStatusCodes.Error;
		}

		// Token: 0x060044C1 RID: 17601 RVA: 0x00146E20 File Offset: 0x00145220
		internal static UIStatus ConvertUIStatus(CommonErrorStatus.UIStatus status)
		{
			switch (status + 6)
			{
			case (CommonErrorStatus.UIStatus)0:
				return UIStatus.UserClosedUI;
			case CommonErrorStatus.UIStatus.VALID:
				return UIStatus.Timeout;
			case (CommonErrorStatus.UIStatus)2:
				return UIStatus.VersionUpdateRequired;
			case (CommonErrorStatus.UIStatus)3:
				return UIStatus.NotAuthorized;
			case (CommonErrorStatus.UIStatus)4:
				return UIStatus.InternalError;
			default:
				if (status != CommonErrorStatus.UIStatus.ERROR_UI_BUSY)
				{
					throw new InvalidOperationException("Unknown status: " + status);
				}
				return UIStatus.UiBusy;
			case (CommonErrorStatus.UIStatus)7:
				return UIStatus.Valid;
			}
		}

		// Token: 0x060044C2 RID: 17602 RVA: 0x00146E8B File Offset: 0x0014528B
		internal static GooglePlayGames.Native.Cwrapper.Types.DataSource AsDataSource(DataSource source)
		{
			if (source == DataSource.ReadCacheOrNetwork)
			{
				return GooglePlayGames.Native.Cwrapper.Types.DataSource.CACHE_OR_NETWORK;
			}
			if (source != DataSource.ReadNetworkOnly)
			{
				throw new InvalidOperationException("Found unhandled DataSource: " + source);
			}
			return GooglePlayGames.Native.Cwrapper.Types.DataSource.NETWORK_ONLY;
		}

		// Token: 0x060044C3 RID: 17603 RVA: 0x00146EB8 File Offset: 0x001452B8
		internal static GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode ConvertVideoCaptureMode(VideoCaptureMode captureMode)
		{
			switch (captureMode + 1)
			{
			case VideoCaptureMode.File:
				return GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.UNKNOWN;
			case VideoCaptureMode.Stream:
				return GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.FILE;
			case (VideoCaptureMode)2:
				return GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.STREAM;
			default:
				Debug.LogWarning("Unknown VideoCaptureMode: " + captureMode + ", defaulting to Types.VideoCaptureMode.UNKNOWN.");
				return GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.UNKNOWN;
			}
		}

		// Token: 0x060044C4 RID: 17604 RVA: 0x00146EF4 File Offset: 0x001452F4
		internal static VideoCaptureMode ConvertNativeVideoCaptureMode(GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode nativeCaptureMode)
		{
			switch (nativeCaptureMode + 1)
			{
			case GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.FILE:
				return VideoCaptureMode.Unknown;
			case GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode.STREAM:
				return VideoCaptureMode.File;
			case (GooglePlayGames.Native.Cwrapper.Types.VideoCaptureMode)2:
				return VideoCaptureMode.Stream;
			default:
				Debug.LogWarning("Unknown Types.VideoCaptureMode: " + nativeCaptureMode + ", defaulting to VideoCaptureMode.Unknown.");
				return VideoCaptureMode.Unknown;
			}
		}

		// Token: 0x060044C5 RID: 17605 RVA: 0x00146F30 File Offset: 0x00145330
		internal static VideoQualityLevel ConvertNativeVideoQualityLevel(GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel nativeQualityLevel)
		{
			switch (nativeQualityLevel + 1)
			{
			case GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel.SD:
				return VideoQualityLevel.Unknown;
			case GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel.HD:
				return VideoQualityLevel.SD;
			case GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel.XHD:
				return VideoQualityLevel.HD;
			case GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel.FULLHD:
				return VideoQualityLevel.XHD;
			case (GooglePlayGames.Native.Cwrapper.Types.VideoQualityLevel)4:
				return VideoQualityLevel.FullHD;
			default:
				Debug.LogWarning("Unknown Types.VideoQualityLevel: " + nativeQualityLevel + ", defaulting to VideoQualityLevel.Unknown.");
				return VideoQualityLevel.Unknown;
			}
		}

		// Token: 0x060044C6 RID: 17606 RVA: 0x00146F84 File Offset: 0x00145384
		internal static VideoCaptureOverlayState ConvertNativeVideoCaptureOverlayState(GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState nativeOverlayState)
		{
			switch (nativeOverlayState + 1)
			{
			case (GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState)0:
				return VideoCaptureOverlayState.Unknown;
			case GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState.STARTED:
				return VideoCaptureOverlayState.Shown;
			case GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState.STOPPED:
				return VideoCaptureOverlayState.Started;
			case GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState.DISMISSED:
				return VideoCaptureOverlayState.Stopped;
			case (GooglePlayGames.Native.Cwrapper.Types.VideoCaptureOverlayState)5:
				return VideoCaptureOverlayState.Dismissed;
			}
			Debug.LogWarning("Unknown Types.VideoCaptureOverlayState: " + nativeOverlayState + ", defaulting to VideoCaptureOverlayState.Unknown.");
			return VideoCaptureOverlayState.Unknown;
		}
	}
}
