﻿using System;
using System.Runtime.CompilerServices;
using GooglePlayGames.Android;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;
using UnityEngine;

namespace GooglePlayGames.Native
{
	// Token: 0x02000DF4 RID: 3572
	public class NativeNearbyConnectionClientFactory
	{
		// Token: 0x0600482C RID: 18476 RVA: 0x00148F88 File Offset: 0x00147388
		internal static NearbyConnectionsManager GetManager()
		{
			return NativeNearbyConnectionClientFactory.sManager;
		}

		// Token: 0x0600482D RID: 18477 RVA: 0x00148F91 File Offset: 0x00147391
		public static void Create(Action<INearbyConnectionClient> callback)
		{
			if (NativeNearbyConnectionClientFactory.sManager == null)
			{
				NativeNearbyConnectionClientFactory.sCreationCallback = callback;
				NativeNearbyConnectionClientFactory.InitializeFactory();
			}
			else
			{
				callback(new NativeNearbyConnectionsClient(NativeNearbyConnectionClientFactory.GetManager()));
			}
		}

		// Token: 0x0600482E RID: 18478 RVA: 0x00148FC0 File Offset: 0x001473C0
		internal static void InitializeFactory()
		{
			PlayGamesHelperObject.CreateObject();
			NearbyConnectionsManager.ReadServiceId();
			NearbyConnectionsManagerBuilder nearbyConnectionsManagerBuilder = new NearbyConnectionsManagerBuilder();
			NearbyConnectionsManagerBuilder nearbyConnectionsManagerBuilder2 = nearbyConnectionsManagerBuilder;
			if (NativeNearbyConnectionClientFactory.<>f__mg$cache0 == null)
			{
				NativeNearbyConnectionClientFactory.<>f__mg$cache0 = new Action<NearbyConnectionsStatus.InitializationStatus>(NativeNearbyConnectionClientFactory.OnManagerInitialized);
			}
			nearbyConnectionsManagerBuilder2.SetOnInitializationFinished(NativeNearbyConnectionClientFactory.<>f__mg$cache0);
			PlatformConfiguration configuration = new AndroidClient().CreatePlatformConfiguration(PlayGamesClientConfiguration.DefaultConfiguration);
			Debug.Log("Building manager Now");
			NativeNearbyConnectionClientFactory.sManager = nearbyConnectionsManagerBuilder.Build(configuration);
		}

		// Token: 0x0600482F RID: 18479 RVA: 0x0014902C File Offset: 0x0014742C
		internal static void OnManagerInitialized(NearbyConnectionsStatus.InitializationStatus status)
		{
			Debug.Log(string.Concat(new object[]
			{
				"Nearby Init Complete: ",
				status,
				" sManager = ",
				NativeNearbyConnectionClientFactory.sManager
			}));
			if (status == NearbyConnectionsStatus.InitializationStatus.VALID)
			{
				if (NativeNearbyConnectionClientFactory.sCreationCallback != null)
				{
					NativeNearbyConnectionClientFactory.sCreationCallback(new NativeNearbyConnectionsClient(NativeNearbyConnectionClientFactory.GetManager()));
					NativeNearbyConnectionClientFactory.sCreationCallback = null;
				}
			}
			else
			{
				Debug.LogError("ERROR: NearbyConnectionManager not initialized: " + status);
			}
		}

		// Token: 0x04004C99 RID: 19609
		private static volatile NearbyConnectionsManager sManager;

		// Token: 0x04004C9A RID: 19610
		private static Action<INearbyConnectionClient> sCreationCallback;

		// Token: 0x04004C9B RID: 19611
		[CompilerGenerated]
		private static Action<NearbyConnectionsStatus.InitializationStatus> <>f__mg$cache0;
	}
}
