﻿using System;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000E6C RID: 3692
	internal class UnsupportedSavedGamesClient : ISavedGameClient
	{
		// Token: 0x06004C60 RID: 19552 RVA: 0x001549B1 File Offset: 0x00152DB1
		public UnsupportedSavedGamesClient(string message)
		{
			this.mMessage = Misc.CheckNotNull<string>(message);
		}

		// Token: 0x06004C61 RID: 19553 RVA: 0x001549C5 File Offset: 0x00152DC5
		public void OpenWithAutomaticConflictResolution(string filename, DataSource source, ConflictResolutionStrategy resolutionStrategy, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C62 RID: 19554 RVA: 0x001549D2 File Offset: 0x00152DD2
		public void OpenWithManualConflictResolution(string filename, DataSource source, bool prefetchDataOnConflict, ConflictCallback conflictCallback, Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C63 RID: 19555 RVA: 0x001549DF File Offset: 0x00152DDF
		public void ReadBinaryData(ISavedGameMetadata metadata, Action<SavedGameRequestStatus, byte[]> completedCallback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C64 RID: 19556 RVA: 0x001549EC File Offset: 0x00152DEC
		public void ShowSelectSavedGameUI(string uiTitle, uint maxDisplayedSavedGames, bool showCreateSaveUI, bool showDeleteSaveUI, Action<SelectUIStatus, ISavedGameMetadata> callback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C65 RID: 19557 RVA: 0x001549F9 File Offset: 0x00152DF9
		public void CommitUpdate(ISavedGameMetadata metadata, SavedGameMetadataUpdate updateForMetadata, byte[] updatedBinaryData, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C66 RID: 19558 RVA: 0x00154A06 File Offset: 0x00152E06
		public void FetchAllSavedGames(DataSource source, Action<SavedGameRequestStatus, List<ISavedGameMetadata>> callback)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x06004C67 RID: 19559 RVA: 0x00154A13 File Offset: 0x00152E13
		public void Delete(ISavedGameMetadata metadata)
		{
			throw new NotImplementedException(this.mMessage);
		}

		// Token: 0x04004D66 RID: 19814
		private readonly string mMessage;
	}
}
