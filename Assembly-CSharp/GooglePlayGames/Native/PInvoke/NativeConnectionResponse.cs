﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E26 RID: 3622
	internal class NativeConnectionResponse : BaseReferenceHolder
	{
		// Token: 0x060049FD RID: 18941 RVA: 0x00150248 File Offset: 0x0014E648
		internal NativeConnectionResponse(IntPtr pointer) : base(pointer)
		{
		}

		// Token: 0x060049FE RID: 18942 RVA: 0x00150251 File Offset: 0x0014E651
		internal string RemoteEndpointId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionResponse_GetRemoteEndpointId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049FF RID: 18943 RVA: 0x00150264 File Offset: 0x0014E664
		internal NearbyConnectionTypes.ConnectionResponse_ResponseCode ResponseCode()
		{
			return NearbyConnectionTypes.ConnectionResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004A00 RID: 18944 RVA: 0x00150271 File Offset: 0x0014E671
		internal byte[] Payload()
		{
			return PInvokeUtilities.OutParamsToArray<byte>((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionResponse_GetPayload(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004A01 RID: 18945 RVA: 0x00150284 File Offset: 0x0014E684
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionTypes.ConnectionResponse_Dispose(selfPointer);
		}

		// Token: 0x06004A02 RID: 18946 RVA: 0x0015028C File Offset: 0x0014E68C
		internal ConnectionResponse AsResponse(long localClientId)
		{
			NearbyConnectionTypes.ConnectionResponse_ResponseCode connectionResponse_ResponseCode = this.ResponseCode();
			switch (connectionResponse_ResponseCode + 4)
			{
			case (NearbyConnectionTypes.ConnectionResponse_ResponseCode)0:
				return ConnectionResponse.EndpointNotConnected(localClientId, this.RemoteEndpointId());
			case NearbyConnectionTypes.ConnectionResponse_ResponseCode.ACCEPTED:
				return ConnectionResponse.AlreadyConnected(localClientId, this.RemoteEndpointId());
			case NearbyConnectionTypes.ConnectionResponse_ResponseCode.REJECTED:
				return ConnectionResponse.NetworkNotConnected(localClientId, this.RemoteEndpointId());
			case (NearbyConnectionTypes.ConnectionResponse_ResponseCode)3:
				return ConnectionResponse.InternalError(localClientId, this.RemoteEndpointId());
			case (NearbyConnectionTypes.ConnectionResponse_ResponseCode)5:
				return ConnectionResponse.Accepted(localClientId, this.RemoteEndpointId(), this.Payload());
			case (NearbyConnectionTypes.ConnectionResponse_ResponseCode)6:
				return ConnectionResponse.Rejected(localClientId, this.RemoteEndpointId());
			}
			throw new InvalidOperationException("Found connection response of unknown type: " + this.ResponseCode());
		}

		// Token: 0x06004A03 RID: 18947 RVA: 0x00150337 File Offset: 0x0014E737
		internal static NativeConnectionResponse FromPointer(IntPtr pointer)
		{
			if (pointer == IntPtr.Zero)
			{
				return null;
			}
			return new NativeConnectionResponse(pointer);
		}
	}
}
