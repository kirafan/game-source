﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E23 RID: 3619
	internal class NativeAchievement : BaseReferenceHolder
	{
		// Token: 0x060049D8 RID: 18904 RVA: 0x0014FF1B File Offset: 0x0014E31B
		internal NativeAchievement(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049D9 RID: 18905 RVA: 0x0014FF24 File Offset: 0x0014E324
		internal uint CurrentSteps()
		{
			return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_CurrentSteps(base.SelfPtr());
		}

		// Token: 0x060049DA RID: 18906 RVA: 0x0014FF31 File Offset: 0x0014E331
		internal string Description()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Description(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x060049DB RID: 18907 RVA: 0x0014FF44 File Offset: 0x0014E344
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Id(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x060049DC RID: 18908 RVA: 0x0014FF57 File Offset: 0x0014E357
		internal string Name()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Name(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x060049DD RID: 18909 RVA: 0x0014FF6A File Offset: 0x0014E36A
		internal Types.AchievementState State()
		{
			return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_State(base.SelfPtr());
		}

		// Token: 0x060049DE RID: 18910 RVA: 0x0014FF77 File Offset: 0x0014E377
		internal uint TotalSteps()
		{
			return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_TotalSteps(base.SelfPtr());
		}

		// Token: 0x060049DF RID: 18911 RVA: 0x0014FF84 File Offset: 0x0014E384
		internal Types.AchievementType Type()
		{
			return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Type(base.SelfPtr());
		}

		// Token: 0x060049E0 RID: 18912 RVA: 0x0014FF91 File Offset: 0x0014E391
		internal ulong LastModifiedTime()
		{
			if (GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Valid(base.SelfPtr()))
			{
				return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_LastModifiedTime(base.SelfPtr());
			}
			return 0UL;
		}

		// Token: 0x060049E1 RID: 18913 RVA: 0x0014FFB1 File Offset: 0x0014E3B1
		internal ulong getXP()
		{
			return GooglePlayGames.Native.Cwrapper.Achievement.Achievement_XP(base.SelfPtr());
		}

		// Token: 0x060049E2 RID: 18914 RVA: 0x0014FFBE File Offset: 0x0014E3BE
		internal string getRevealedImageUrl()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Achievement.Achievement_RevealedIconUrl(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x060049E3 RID: 18915 RVA: 0x0014FFD1 File Offset: 0x0014E3D1
		internal string getUnlockedImageUrl()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Achievement.Achievement_UnlockedIconUrl(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x060049E4 RID: 18916 RVA: 0x0014FFE4 File Offset: 0x0014E3E4
		protected override void CallDispose(HandleRef selfPointer)
		{
			GooglePlayGames.Native.Cwrapper.Achievement.Achievement_Dispose(selfPointer);
		}

		// Token: 0x060049E5 RID: 18917 RVA: 0x0014FFEC File Offset: 0x0014E3EC
		internal GooglePlayGames.BasicApi.Achievement AsAchievement()
		{
			GooglePlayGames.BasicApi.Achievement achievement = new GooglePlayGames.BasicApi.Achievement();
			achievement.Id = this.Id();
			achievement.Name = this.Name();
			achievement.Description = this.Description();
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			ulong num = this.LastModifiedTime();
			if (num == 18446744073709551615UL)
			{
				num = 0UL;
			}
			achievement.LastModifiedTime = dateTime.AddMilliseconds(num);
			achievement.Points = this.getXP();
			achievement.RevealedImageUrl = this.getRevealedImageUrl();
			achievement.UnlockedImageUrl = this.getUnlockedImageUrl();
			if (this.Type() == Types.AchievementType.INCREMENTAL)
			{
				achievement.IsIncremental = true;
				achievement.CurrentSteps = (int)this.CurrentSteps();
				achievement.TotalSteps = (int)this.TotalSteps();
			}
			achievement.IsRevealed = (this.State() == Types.AchievementState.REVEALED || this.State() == Types.AchievementState.UNLOCKED);
			achievement.IsUnlocked = (this.State() == Types.AchievementState.UNLOCKED);
			return achievement;
		}

		// Token: 0x04004CFE RID: 19710
		private const ulong MinusOne = 18446744073709551615UL;
	}
}
