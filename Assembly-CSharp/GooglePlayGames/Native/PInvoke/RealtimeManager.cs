﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E52 RID: 3666
	internal class RealtimeManager
	{
		// Token: 0x06004B89 RID: 19337 RVA: 0x00153064 File Offset: 0x00151464
		internal RealtimeManager(GameServices gameServices)
		{
			this.mGameServices = Misc.CheckNotNull<GameServices>(gameServices);
		}

		// Token: 0x06004B8A RID: 19338 RVA: 0x00153078 File Offset: 0x00151478
		internal void CreateRoom(RealtimeRoomConfig config, RealTimeEventListenerHelper helper, Action<RealtimeManager.RealTimeRoomResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr config2 = config.AsPointer();
			IntPtr helper2 = helper.AsPointer();
			if (RealtimeManager.<>f__mg$cache0 == null)
			{
				RealtimeManager.<>f__mg$cache0 = new RealTimeMultiplayerManager.RealTimeRoomCallback(RealtimeManager.InternalRealTimeRoomCallback);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_CreateRealTimeRoom(self, config2, helper2, RealtimeManager.<>f__mg$cache0, RealtimeManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004B8B RID: 19339 RVA: 0x001530C4 File Offset: 0x001514C4
		internal void ShowPlayerSelectUI(uint minimumPlayers, uint maxiumPlayers, bool allowAutomatching, Action<PlayerSelectUIResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (RealtimeManager.<>f__mg$cache2 == null)
			{
				RealtimeManager.<>f__mg$cache2 = new RealTimeMultiplayerManager.PlayerSelectUICallback(RealtimeManager.InternalPlayerSelectUIcallback);
			}
			RealTimeMultiplayerManager.PlayerSelectUICallback callback2 = RealtimeManager.<>f__mg$cache2;
			if (RealtimeManager.<>f__mg$cache1 == null)
			{
				RealtimeManager.<>f__mg$cache1 = new Func<IntPtr, PlayerSelectUIResponse>(PlayerSelectUIResponse.FromPointer);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_ShowPlayerSelectUI(self, minimumPlayers, maxiumPlayers, allowAutomatching, callback2, Callbacks.ToIntPtr<PlayerSelectUIResponse>(callback, RealtimeManager.<>f__mg$cache1));
		}

		// Token: 0x06004B8C RID: 19340 RVA: 0x00153125 File Offset: 0x00151525
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.PlayerSelectUICallback))]
		internal static void InternalPlayerSelectUIcallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealtimeManager#PlayerSelectUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B8D RID: 19341 RVA: 0x00153134 File Offset: 0x00151534
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.RealTimeRoomCallback))]
		internal static void InternalRealTimeRoomCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealtimeManager#InternalRealTimeRoomCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B8E RID: 19342 RVA: 0x00153143 File Offset: 0x00151543
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.RoomInboxUICallback))]
		internal static void InternalRoomInboxUICallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealtimeManager#InternalRoomInboxUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B8F RID: 19343 RVA: 0x00153154 File Offset: 0x00151554
		internal void ShowRoomInboxUI(Action<RealtimeManager.RoomInboxUIResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (RealtimeManager.<>f__mg$cache4 == null)
			{
				RealtimeManager.<>f__mg$cache4 = new RealTimeMultiplayerManager.RoomInboxUICallback(RealtimeManager.InternalRoomInboxUICallback);
			}
			RealTimeMultiplayerManager.RoomInboxUICallback callback2 = RealtimeManager.<>f__mg$cache4;
			if (RealtimeManager.<>f__mg$cache3 == null)
			{
				RealtimeManager.<>f__mg$cache3 = new Func<IntPtr, RealtimeManager.RoomInboxUIResponse>(RealtimeManager.RoomInboxUIResponse.FromPointer);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_ShowRoomInboxUI(self, callback2, Callbacks.ToIntPtr<RealtimeManager.RoomInboxUIResponse>(callback, RealtimeManager.<>f__mg$cache3));
		}

		// Token: 0x06004B90 RID: 19344 RVA: 0x001531B4 File Offset: 0x001515B4
		internal void ShowWaitingRoomUI(NativeRealTimeRoom room, uint minimumParticipantsBeforeStarting, Action<RealtimeManager.WaitingRoomUIResponse> callback)
		{
			Misc.CheckNotNull<NativeRealTimeRoom>(room);
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr room2 = room.AsPointer();
			if (RealtimeManager.<>f__mg$cache6 == null)
			{
				RealtimeManager.<>f__mg$cache6 = new RealTimeMultiplayerManager.WaitingRoomUICallback(RealtimeManager.InternalWaitingRoomUICallback);
			}
			RealTimeMultiplayerManager.WaitingRoomUICallback callback2 = RealtimeManager.<>f__mg$cache6;
			if (RealtimeManager.<>f__mg$cache5 == null)
			{
				RealtimeManager.<>f__mg$cache5 = new Func<IntPtr, RealtimeManager.WaitingRoomUIResponse>(RealtimeManager.WaitingRoomUIResponse.FromPointer);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_ShowWaitingRoomUI(self, room2, minimumParticipantsBeforeStarting, callback2, Callbacks.ToIntPtr<RealtimeManager.WaitingRoomUIResponse>(callback, RealtimeManager.<>f__mg$cache5));
		}

		// Token: 0x06004B91 RID: 19345 RVA: 0x0015321F File Offset: 0x0015161F
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.WaitingRoomUICallback))]
		internal static void InternalWaitingRoomUICallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealtimeManager#InternalWaitingRoomUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B92 RID: 19346 RVA: 0x0015322E File Offset: 0x0015162E
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.FetchInvitationsCallback))]
		internal static void InternalFetchInvitationsCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealtimeManager#InternalFetchInvitationsCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B93 RID: 19347 RVA: 0x00153240 File Offset: 0x00151640
		internal void FetchInvitations(Action<RealtimeManager.FetchInvitationsResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (RealtimeManager.<>f__mg$cache8 == null)
			{
				RealtimeManager.<>f__mg$cache8 = new RealTimeMultiplayerManager.FetchInvitationsCallback(RealtimeManager.InternalFetchInvitationsCallback);
			}
			RealTimeMultiplayerManager.FetchInvitationsCallback callback2 = RealtimeManager.<>f__mg$cache8;
			if (RealtimeManager.<>f__mg$cache7 == null)
			{
				RealtimeManager.<>f__mg$cache7 = new Func<IntPtr, RealtimeManager.FetchInvitationsResponse>(RealtimeManager.FetchInvitationsResponse.FromPointer);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_FetchInvitations(self, callback2, Callbacks.ToIntPtr<RealtimeManager.FetchInvitationsResponse>(callback, RealtimeManager.<>f__mg$cache7));
		}

		// Token: 0x06004B94 RID: 19348 RVA: 0x001532A0 File Offset: 0x001516A0
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.LeaveRoomCallback))]
		internal static void InternalLeaveRoomCallback(CommonErrorStatus.ResponseStatus response, IntPtr data)
		{
			Logger.d("Entering internal callback for InternalLeaveRoomCallback");
			Action<CommonErrorStatus.ResponseStatus> action = Callbacks.IntPtrToTempCallback<Action<CommonErrorStatus.ResponseStatus>>(data);
			if (action == null)
			{
				return;
			}
			try
			{
				action(response);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalLeaveRoomCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004B95 RID: 19349 RVA: 0x001532F8 File Offset: 0x001516F8
		internal void LeaveRoom(NativeRealTimeRoom room, Action<CommonErrorStatus.ResponseStatus> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr room2 = room.AsPointer();
			if (RealtimeManager.<>f__mg$cache9 == null)
			{
				RealtimeManager.<>f__mg$cache9 = new RealTimeMultiplayerManager.LeaveRoomCallback(RealtimeManager.InternalLeaveRoomCallback);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_LeaveRoom(self, room2, RealtimeManager.<>f__mg$cache9, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004B96 RID: 19350 RVA: 0x00153334 File Offset: 0x00151734
		internal void AcceptInvitation(MultiplayerInvitation invitation, RealTimeEventListenerHelper listener, Action<RealtimeManager.RealTimeRoomResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr invitation2 = invitation.AsPointer();
			IntPtr helper = listener.AsPointer();
			if (RealtimeManager.<>f__mg$cacheA == null)
			{
				RealtimeManager.<>f__mg$cacheA = new RealTimeMultiplayerManager.RealTimeRoomCallback(RealtimeManager.InternalRealTimeRoomCallback);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_AcceptInvitation(self, invitation2, helper, RealtimeManager.<>f__mg$cacheA, RealtimeManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004B97 RID: 19351 RVA: 0x00153380 File Offset: 0x00151780
		internal void DeclineInvitation(MultiplayerInvitation invitation)
		{
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_DeclineInvitation(this.mGameServices.AsHandle(), invitation.AsPointer());
		}

		// Token: 0x06004B98 RID: 19352 RVA: 0x00153398 File Offset: 0x00151798
		internal void SendReliableMessage(NativeRealTimeRoom room, MultiplayerParticipant participant, byte[] data, Action<CommonErrorStatus.MultiplayerStatus> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr room2 = room.AsPointer();
			IntPtr participant2 = participant.AsPointer();
			UIntPtr data_size = PInvokeUtilities.ArrayToSizeT<byte>(data);
			if (RealtimeManager.<>f__mg$cacheB == null)
			{
				RealtimeManager.<>f__mg$cacheB = new RealTimeMultiplayerManager.SendReliableMessageCallback(RealtimeManager.InternalSendReliableMessageCallback);
			}
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_SendReliableMessage(self, room2, participant2, data, data_size, RealtimeManager.<>f__mg$cacheB, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004B99 RID: 19353 RVA: 0x001533EC File Offset: 0x001517EC
		[MonoPInvokeCallback(typeof(RealTimeMultiplayerManager.SendReliableMessageCallback))]
		internal static void InternalSendReliableMessageCallback(CommonErrorStatus.MultiplayerStatus response, IntPtr data)
		{
			Logger.d("Entering internal callback for InternalSendReliableMessageCallback " + response);
			Action<CommonErrorStatus.MultiplayerStatus> action = Callbacks.IntPtrToTempCallback<Action<CommonErrorStatus.MultiplayerStatus>>(data);
			if (action == null)
			{
				return;
			}
			try
			{
				action(response);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalSendReliableMessageCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004B9A RID: 19354 RVA: 0x00153450 File Offset: 0x00151850
		internal void SendUnreliableMessageToAll(NativeRealTimeRoom room, byte[] data)
		{
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_SendUnreliableMessageToOthers(this.mGameServices.AsHandle(), room.AsPointer(), data, PInvokeUtilities.ArrayToSizeT<byte>(data));
		}

		// Token: 0x06004B9B RID: 19355 RVA: 0x00153470 File Offset: 0x00151870
		internal void SendUnreliableMessageToSpecificParticipants(NativeRealTimeRoom room, List<MultiplayerParticipant> recipients, byte[] data)
		{
			RealTimeMultiplayerManager.RealTimeMultiplayerManager_SendUnreliableMessage(this.mGameServices.AsHandle(), room.AsPointer(), (from r in recipients
			select r.AsPointer()).ToArray<IntPtr>(), new UIntPtr((ulong)recipients.LongCount<MultiplayerParticipant>()), data, PInvokeUtilities.ArrayToSizeT<byte>(data));
		}

		// Token: 0x06004B9C RID: 19356 RVA: 0x001534CD File Offset: 0x001518CD
		private static IntPtr ToCallbackPointer(Action<RealtimeManager.RealTimeRoomResponse> callback)
		{
			if (RealtimeManager.<>f__mg$cacheC == null)
			{
				RealtimeManager.<>f__mg$cacheC = new Func<IntPtr, RealtimeManager.RealTimeRoomResponse>(RealtimeManager.RealTimeRoomResponse.FromPointer);
			}
			return Callbacks.ToIntPtr<RealtimeManager.RealTimeRoomResponse>(callback, RealtimeManager.<>f__mg$cacheC);
		}

		// Token: 0x04004D2E RID: 19758
		private readonly GameServices mGameServices;

		// Token: 0x04004D2F RID: 19759
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.RealTimeRoomCallback <>f__mg$cache0;

		// Token: 0x04004D30 RID: 19760
		[CompilerGenerated]
		private static Func<IntPtr, PlayerSelectUIResponse> <>f__mg$cache1;

		// Token: 0x04004D31 RID: 19761
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.PlayerSelectUICallback <>f__mg$cache2;

		// Token: 0x04004D32 RID: 19762
		[CompilerGenerated]
		private static Func<IntPtr, RealtimeManager.RoomInboxUIResponse> <>f__mg$cache3;

		// Token: 0x04004D33 RID: 19763
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.RoomInboxUICallback <>f__mg$cache4;

		// Token: 0x04004D34 RID: 19764
		[CompilerGenerated]
		private static Func<IntPtr, RealtimeManager.WaitingRoomUIResponse> <>f__mg$cache5;

		// Token: 0x04004D35 RID: 19765
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.WaitingRoomUICallback <>f__mg$cache6;

		// Token: 0x04004D36 RID: 19766
		[CompilerGenerated]
		private static Func<IntPtr, RealtimeManager.FetchInvitationsResponse> <>f__mg$cache7;

		// Token: 0x04004D37 RID: 19767
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.FetchInvitationsCallback <>f__mg$cache8;

		// Token: 0x04004D38 RID: 19768
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.LeaveRoomCallback <>f__mg$cache9;

		// Token: 0x04004D39 RID: 19769
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.RealTimeRoomCallback <>f__mg$cacheA;

		// Token: 0x04004D3A RID: 19770
		[CompilerGenerated]
		private static RealTimeMultiplayerManager.SendReliableMessageCallback <>f__mg$cacheB;

		// Token: 0x04004D3C RID: 19772
		[CompilerGenerated]
		private static Func<IntPtr, RealtimeManager.RealTimeRoomResponse> <>f__mg$cacheC;

		// Token: 0x02000E53 RID: 3667
		internal class RealTimeRoomResponse : BaseReferenceHolder
		{
			// Token: 0x06004B9E RID: 19358 RVA: 0x001534FA File Offset: 0x001518FA
			internal RealTimeRoomResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B9F RID: 19359 RVA: 0x00153503 File Offset: 0x00151903
			internal CommonErrorStatus.MultiplayerStatus ResponseStatus()
			{
				return RealTimeMultiplayerManager.RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BA0 RID: 19360 RVA: 0x00153510 File Offset: 0x00151910
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.MultiplayerStatus)0;
			}

			// Token: 0x06004BA1 RID: 19361 RVA: 0x0015351B File Offset: 0x0015191B
			internal NativeRealTimeRoom Room()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				return new NativeRealTimeRoom(RealTimeMultiplayerManager.RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(base.SelfPtr()));
			}

			// Token: 0x06004BA2 RID: 19362 RVA: 0x0015353A File Offset: 0x0015193A
			protected override void CallDispose(HandleRef selfPointer)
			{
				RealTimeMultiplayerManager.RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BA3 RID: 19363 RVA: 0x00153542 File Offset: 0x00151942
			internal static RealtimeManager.RealTimeRoomResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new RealtimeManager.RealTimeRoomResponse(pointer);
			}
		}

		// Token: 0x02000E54 RID: 3668
		internal class RoomInboxUIResponse : BaseReferenceHolder
		{
			// Token: 0x06004BA4 RID: 19364 RVA: 0x00153568 File Offset: 0x00151968
			internal RoomInboxUIResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BA5 RID: 19365 RVA: 0x00153571 File Offset: 0x00151971
			internal CommonErrorStatus.UIStatus ResponseStatus()
			{
				return RealTimeMultiplayerManager.RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BA6 RID: 19366 RVA: 0x0015357E File Offset: 0x0015197E
			internal MultiplayerInvitation Invitation()
			{
				if (this.ResponseStatus() != CommonErrorStatus.UIStatus.VALID)
				{
					return null;
				}
				return new MultiplayerInvitation(RealTimeMultiplayerManager.RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(base.SelfPtr()));
			}

			// Token: 0x06004BA7 RID: 19367 RVA: 0x0015359E File Offset: 0x0015199E
			protected override void CallDispose(HandleRef selfPointer)
			{
				RealTimeMultiplayerManager.RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BA8 RID: 19368 RVA: 0x001535A6 File Offset: 0x001519A6
			internal static RealtimeManager.RoomInboxUIResponse FromPointer(IntPtr pointer)
			{
				if (PInvokeUtilities.IsNull(pointer))
				{
					return null;
				}
				return new RealtimeManager.RoomInboxUIResponse(pointer);
			}
		}

		// Token: 0x02000E55 RID: 3669
		internal class WaitingRoomUIResponse : BaseReferenceHolder
		{
			// Token: 0x06004BA9 RID: 19369 RVA: 0x001535BB File Offset: 0x001519BB
			internal WaitingRoomUIResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BAA RID: 19370 RVA: 0x001535C4 File Offset: 0x001519C4
			internal CommonErrorStatus.UIStatus ResponseStatus()
			{
				return RealTimeMultiplayerManager.RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BAB RID: 19371 RVA: 0x001535D1 File Offset: 0x001519D1
			internal NativeRealTimeRoom Room()
			{
				if (this.ResponseStatus() != CommonErrorStatus.UIStatus.VALID)
				{
					return null;
				}
				return new NativeRealTimeRoom(RealTimeMultiplayerManager.RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(base.SelfPtr()));
			}

			// Token: 0x06004BAC RID: 19372 RVA: 0x001535F1 File Offset: 0x001519F1
			protected override void CallDispose(HandleRef selfPointer)
			{
				RealTimeMultiplayerManager.RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BAD RID: 19373 RVA: 0x001535F9 File Offset: 0x001519F9
			internal static RealtimeManager.WaitingRoomUIResponse FromPointer(IntPtr pointer)
			{
				if (PInvokeUtilities.IsNull(pointer))
				{
					return null;
				}
				return new RealtimeManager.WaitingRoomUIResponse(pointer);
			}
		}

		// Token: 0x02000E56 RID: 3670
		internal class FetchInvitationsResponse : BaseReferenceHolder
		{
			// Token: 0x06004BAE RID: 19374 RVA: 0x0015360E File Offset: 0x00151A0E
			internal FetchInvitationsResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BAF RID: 19375 RVA: 0x00153617 File Offset: 0x00151A17
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004BB0 RID: 19376 RVA: 0x00153622 File Offset: 0x00151A22
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return RealTimeMultiplayerManager.RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BB1 RID: 19377 RVA: 0x0015362F File Offset: 0x00151A2F
			internal IEnumerable<MultiplayerInvitation> Invitations()
			{
				return PInvokeUtilities.ToEnumerable<MultiplayerInvitation>(RealTimeMultiplayerManager.RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(base.SelfPtr()), (UIntPtr index) => new MultiplayerInvitation(RealTimeMultiplayerManager.RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004BB2 RID: 19378 RVA: 0x0015364D File Offset: 0x00151A4D
			protected override void CallDispose(HandleRef selfPointer)
			{
				RealTimeMultiplayerManager.RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BB3 RID: 19379 RVA: 0x00153655 File Offset: 0x00151A55
			internal static RealtimeManager.FetchInvitationsResponse FromPointer(IntPtr pointer)
			{
				if (PInvokeUtilities.IsNull(pointer))
				{
					return null;
				}
				return new RealtimeManager.FetchInvitationsResponse(pointer);
			}
		}
	}
}
