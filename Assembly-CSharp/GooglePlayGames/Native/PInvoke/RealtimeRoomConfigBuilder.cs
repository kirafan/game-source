﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E58 RID: 3672
	internal class RealtimeRoomConfigBuilder : BaseReferenceHolder
	{
		// Token: 0x06004BB8 RID: 19384 RVA: 0x001536B4 File Offset: 0x00151AB4
		internal RealtimeRoomConfigBuilder(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004BB9 RID: 19385 RVA: 0x001536BD File Offset: 0x00151ABD
		internal RealtimeRoomConfigBuilder PopulateFromUIResponse(PlayerSelectUIResponse response)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(base.SelfPtr(), response.AsPointer());
			return this;
		}

		// Token: 0x06004BBA RID: 19386 RVA: 0x001536D4 File Offset: 0x00151AD4
		internal RealtimeRoomConfigBuilder SetVariant(uint variantValue)
		{
			uint variant = (variantValue != 0U) ? variantValue : uint.MaxValue;
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_SetVariant(base.SelfPtr(), variant);
			return this;
		}

		// Token: 0x06004BBB RID: 19387 RVA: 0x001536FC File Offset: 0x00151AFC
		internal RealtimeRoomConfigBuilder AddInvitedPlayer(string playerId)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_AddPlayerToInvite(base.SelfPtr(), playerId);
			return this;
		}

		// Token: 0x06004BBC RID: 19388 RVA: 0x0015370B File Offset: 0x00151B0B
		internal RealtimeRoomConfigBuilder SetExclusiveBitMask(ulong bitmask)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_SetExclusiveBitMask(base.SelfPtr(), bitmask);
			return this;
		}

		// Token: 0x06004BBD RID: 19389 RVA: 0x0015371A File Offset: 0x00151B1A
		internal RealtimeRoomConfigBuilder SetMinimumAutomatchingPlayers(uint minimum)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(base.SelfPtr(), minimum);
			return this;
		}

		// Token: 0x06004BBE RID: 19390 RVA: 0x00153729 File Offset: 0x00151B29
		internal RealtimeRoomConfigBuilder SetMaximumAutomatchingPlayers(uint maximum)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(base.SelfPtr(), maximum);
			return this;
		}

		// Token: 0x06004BBF RID: 19391 RVA: 0x00153738 File Offset: 0x00151B38
		internal RealtimeRoomConfig Build()
		{
			return new RealtimeRoomConfig(RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_Create(base.SelfPtr()));
		}

		// Token: 0x06004BC0 RID: 19392 RVA: 0x0015374A File Offset: 0x00151B4A
		protected override void CallDispose(HandleRef selfPointer)
		{
			RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_Dispose(selfPointer);
		}

		// Token: 0x06004BC1 RID: 19393 RVA: 0x00153752 File Offset: 0x00151B52
		internal static RealtimeRoomConfigBuilder Create()
		{
			return new RealtimeRoomConfigBuilder(RealTimeRoomConfigBuilder.RealTimeRoomConfig_Builder_Construct());
		}
	}
}
