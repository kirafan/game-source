﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E2E RID: 3630
	internal class NativePlayerStats : BaseReferenceHolder
	{
		// Token: 0x06004A3D RID: 19005 RVA: 0x001508B1 File Offset: 0x0014ECB1
		internal NativePlayerStats(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004A3E RID: 19006 RVA: 0x001508BA File Offset: 0x0014ECBA
		internal bool Valid()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_Valid(base.SelfPtr());
		}

		// Token: 0x06004A3F RID: 19007 RVA: 0x001508C7 File Offset: 0x0014ECC7
		internal bool HasAverageSessionLength()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasAverageSessionLength(base.SelfPtr());
		}

		// Token: 0x06004A40 RID: 19008 RVA: 0x001508D4 File Offset: 0x0014ECD4
		internal float AverageSessionLength()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_AverageSessionLength(base.SelfPtr());
		}

		// Token: 0x06004A41 RID: 19009 RVA: 0x001508E1 File Offset: 0x0014ECE1
		internal bool HasChurnProbability()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasChurnProbability(base.SelfPtr());
		}

		// Token: 0x06004A42 RID: 19010 RVA: 0x001508EE File Offset: 0x0014ECEE
		internal float ChurnProbability()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_ChurnProbability(base.SelfPtr());
		}

		// Token: 0x06004A43 RID: 19011 RVA: 0x001508FB File Offset: 0x0014ECFB
		internal bool HasDaysSinceLastPlayed()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasDaysSinceLastPlayed(base.SelfPtr());
		}

		// Token: 0x06004A44 RID: 19012 RVA: 0x00150908 File Offset: 0x0014ED08
		internal int DaysSinceLastPlayed()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_DaysSinceLastPlayed(base.SelfPtr());
		}

		// Token: 0x06004A45 RID: 19013 RVA: 0x00150915 File Offset: 0x0014ED15
		internal bool HasNumberOfPurchases()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasNumberOfPurchases(base.SelfPtr());
		}

		// Token: 0x06004A46 RID: 19014 RVA: 0x00150922 File Offset: 0x0014ED22
		internal int NumberOfPurchases()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_NumberOfPurchases(base.SelfPtr());
		}

		// Token: 0x06004A47 RID: 19015 RVA: 0x0015092F File Offset: 0x0014ED2F
		internal bool HasNumberOfSessions()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasNumberOfSessions(base.SelfPtr());
		}

		// Token: 0x06004A48 RID: 19016 RVA: 0x0015093C File Offset: 0x0014ED3C
		internal int NumberOfSessions()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_NumberOfSessions(base.SelfPtr());
		}

		// Token: 0x06004A49 RID: 19017 RVA: 0x00150949 File Offset: 0x0014ED49
		internal bool HasSessionPercentile()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasSessionPercentile(base.SelfPtr());
		}

		// Token: 0x06004A4A RID: 19018 RVA: 0x00150956 File Offset: 0x0014ED56
		internal float SessionPercentile()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_SessionPercentile(base.SelfPtr());
		}

		// Token: 0x06004A4B RID: 19019 RVA: 0x00150963 File Offset: 0x0014ED63
		internal bool HasSpendPercentile()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_HasSpendPercentile(base.SelfPtr());
		}

		// Token: 0x06004A4C RID: 19020 RVA: 0x00150970 File Offset: 0x0014ED70
		internal float SpendPercentile()
		{
			return GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_SpendPercentile(base.SelfPtr());
		}

		// Token: 0x06004A4D RID: 19021 RVA: 0x0015097D File Offset: 0x0014ED7D
		protected override void CallDispose(HandleRef selfPointer)
		{
			GooglePlayGames.Native.Cwrapper.PlayerStats.PlayerStats_Dispose(selfPointer);
		}

		// Token: 0x06004A4E RID: 19022 RVA: 0x00150988 File Offset: 0x0014ED88
		internal GooglePlayGames.BasicApi.PlayerStats AsPlayerStats()
		{
			GooglePlayGames.BasicApi.PlayerStats playerStats = new GooglePlayGames.BasicApi.PlayerStats();
			playerStats.Valid = this.Valid();
			if (this.Valid())
			{
				playerStats.AvgSessonLength = this.AverageSessionLength();
				playerStats.ChurnProbability = this.ChurnProbability();
				playerStats.DaysSinceLastPlayed = this.DaysSinceLastPlayed();
				playerStats.NumberOfPurchases = this.NumberOfPurchases();
				playerStats.NumberOfSessions = this.NumberOfSessions();
				playerStats.SessPercentile = this.SessionPercentile();
				playerStats.SpendPercentile = this.SpendPercentile();
				playerStats.SpendProbability = -1f;
			}
			return playerStats;
		}
	}
}
