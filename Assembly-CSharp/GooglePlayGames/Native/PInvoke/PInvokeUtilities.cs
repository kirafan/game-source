﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E40 RID: 3648
	internal static class PInvokeUtilities
	{
		// Token: 0x06004B0A RID: 19210 RVA: 0x00151E42 File Offset: 0x00150242
		internal static HandleRef CheckNonNull(HandleRef reference)
		{
			if (PInvokeUtilities.IsNull(reference))
			{
				throw new InvalidOperationException();
			}
			return reference;
		}

		// Token: 0x06004B0B RID: 19211 RVA: 0x00151E56 File Offset: 0x00150256
		internal static bool IsNull(HandleRef reference)
		{
			return PInvokeUtilities.IsNull(HandleRef.ToIntPtr(reference));
		}

		// Token: 0x06004B0C RID: 19212 RVA: 0x00151E63 File Offset: 0x00150263
		internal static bool IsNull(IntPtr pointer)
		{
			return pointer.Equals(IntPtr.Zero);
		}

		// Token: 0x06004B0D RID: 19213 RVA: 0x00151E7C File Offset: 0x0015027C
		internal static DateTime FromMillisSinceUnixEpoch(long millisSinceEpoch)
		{
			return PInvokeUtilities.UnixEpoch.Add(TimeSpan.FromMilliseconds((double)millisSinceEpoch));
		}

		// Token: 0x06004B0E RID: 19214 RVA: 0x00151EA0 File Offset: 0x001502A0
		internal static string OutParamsToString(PInvokeUtilities.OutStringMethod outStringMethod)
		{
			UIntPtr out_size = outStringMethod(null, UIntPtr.Zero);
			if (out_size.Equals(UIntPtr.Zero))
			{
				return null;
			}
			string result = null;
			try
			{
				byte[] array = new byte[out_size.ToUInt32()];
				outStringMethod(array, out_size);
				result = Encoding.UTF8.GetString(array, 0, (int)(out_size.ToUInt32() - 1U));
			}
			catch (Exception arg)
			{
				Debug.LogError("Exception creating string from char array: " + arg);
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x06004B0F RID: 19215 RVA: 0x00151F3C File Offset: 0x0015033C
		internal static T[] OutParamsToArray<T>(PInvokeUtilities.OutMethod<T> outMethod)
		{
			UIntPtr out_size = outMethod(null, UIntPtr.Zero);
			if (out_size.Equals(UIntPtr.Zero))
			{
				return new T[0];
			}
			T[] array = new T[out_size.ToUInt64()];
			outMethod(array, out_size);
			return array;
		}

		// Token: 0x06004B10 RID: 19216 RVA: 0x00151F94 File Offset: 0x00150394
		internal static IEnumerable<T> ToEnumerable<T>(UIntPtr size, Func<UIntPtr, T> getElement)
		{
			for (ulong i = 0UL; i < size.ToUInt64(); i += 1UL)
			{
				yield return getElement(new UIntPtr(i));
			}
			yield break;
		}

		// Token: 0x06004B11 RID: 19217 RVA: 0x00151FBE File Offset: 0x001503BE
		internal static IEnumerator<T> ToEnumerator<T>(UIntPtr size, Func<UIntPtr, T> getElement)
		{
			return PInvokeUtilities.ToEnumerable<T>(size, getElement).GetEnumerator();
		}

		// Token: 0x06004B12 RID: 19218 RVA: 0x00151FCC File Offset: 0x001503CC
		internal static UIntPtr ArrayToSizeT<T>(T[] array)
		{
			if (array == null)
			{
				return UIntPtr.Zero;
			}
			return new UIntPtr((ulong)((long)array.Length));
		}

		// Token: 0x06004B13 RID: 19219 RVA: 0x00151FE4 File Offset: 0x001503E4
		internal static long ToMilliseconds(TimeSpan span)
		{
			double totalMilliseconds = span.TotalMilliseconds;
			if (totalMilliseconds > 9.223372036854776E+18)
			{
				return long.MaxValue;
			}
			if (totalMilliseconds < -9.223372036854776E+18)
			{
				return long.MinValue;
			}
			return Convert.ToInt64(totalMilliseconds);
		}

		// Token: 0x04004D10 RID: 19728
		private static readonly DateTime UnixEpoch = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);

		// Token: 0x02000E41 RID: 3649
		// (Invoke) Token: 0x06004B16 RID: 19222
		internal delegate UIntPtr OutStringMethod([In] [Out] byte[] out_bytes, UIntPtr out_size);

		// Token: 0x02000E42 RID: 3650
		// (Invoke) Token: 0x06004B1A RID: 19226
		internal delegate UIntPtr OutMethod<T>([In] [Out] T[] out_bytes, UIntPtr out_size);
	}
}
