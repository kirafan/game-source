﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E45 RID: 3653
	internal class PlayerManager
	{
		// Token: 0x06004B25 RID: 19237 RVA: 0x001521B4 File Offset: 0x001505B4
		internal PlayerManager(GameServices services)
		{
			this.mGameServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x06004B26 RID: 19238 RVA: 0x001521C8 File Offset: 0x001505C8
		internal void FetchSelf(Action<PlayerManager.FetchSelfResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (PlayerManager.<>f__mg$cache1 == null)
			{
				PlayerManager.<>f__mg$cache1 = new PlayerManager.FetchSelfCallback(PlayerManager.InternalFetchSelfCallback);
			}
			PlayerManager.FetchSelfCallback callback2 = PlayerManager.<>f__mg$cache1;
			if (PlayerManager.<>f__mg$cache0 == null)
			{
				PlayerManager.<>f__mg$cache0 = new Func<IntPtr, PlayerManager.FetchSelfResponse>(PlayerManager.FetchSelfResponse.FromPointer);
			}
			PlayerManager.PlayerManager_FetchSelf(self, data_source, callback2, Callbacks.ToIntPtr<PlayerManager.FetchSelfResponse>(callback, PlayerManager.<>f__mg$cache0));
		}

		// Token: 0x06004B27 RID: 19239 RVA: 0x00152226 File Offset: 0x00150626
		[MonoPInvokeCallback(typeof(PlayerManager.FetchSelfCallback))]
		private static void InternalFetchSelfCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("PlayerManager#InternalFetchSelfCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B28 RID: 19240 RVA: 0x00152238 File Offset: 0x00150638
		internal void FetchList(string[] userIds, Action<NativePlayer[]> callback)
		{
			PlayerManager.FetchResponseCollector coll = new PlayerManager.FetchResponseCollector();
			coll.pendingCount = userIds.Length;
			coll.callback = callback;
			foreach (string text in userIds)
			{
				HandleRef self = this.mGameServices.AsHandle();
				Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
				string player_id = text;
				if (PlayerManager.<>f__mg$cache3 == null)
				{
					PlayerManager.<>f__mg$cache3 = new PlayerManager.FetchCallback(PlayerManager.InternalFetchCallback);
				}
				PlayerManager.FetchCallback callback2 = PlayerManager.<>f__mg$cache3;
				Action<PlayerManager.FetchResponse> callback3 = delegate(PlayerManager.FetchResponse rsp)
				{
					this.HandleFetchResponse(coll, rsp);
				};
				if (PlayerManager.<>f__mg$cache2 == null)
				{
					PlayerManager.<>f__mg$cache2 = new Func<IntPtr, PlayerManager.FetchResponse>(PlayerManager.FetchResponse.FromPointer);
				}
				PlayerManager.PlayerManager_Fetch(self, data_source, player_id, callback2, Callbacks.ToIntPtr<PlayerManager.FetchResponse>(callback3, PlayerManager.<>f__mg$cache2));
			}
		}

		// Token: 0x06004B29 RID: 19241 RVA: 0x001522EE File Offset: 0x001506EE
		[MonoPInvokeCallback(typeof(PlayerManager.FetchCallback))]
		private static void InternalFetchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("PlayerManager#InternalFetchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B2A RID: 19242 RVA: 0x00152300 File Offset: 0x00150700
		internal void HandleFetchResponse(PlayerManager.FetchResponseCollector collector, PlayerManager.FetchResponse resp)
		{
			if (resp.Status() == CommonErrorStatus.ResponseStatus.VALID || resp.Status() == CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
			{
				NativePlayer player = resp.GetPlayer();
				collector.results.Add(player);
			}
			collector.pendingCount--;
			if (collector.pendingCount == 0)
			{
				collector.callback(collector.results.ToArray());
			}
		}

		// Token: 0x06004B2B RID: 19243 RVA: 0x00152368 File Offset: 0x00150768
		internal void FetchFriends(Action<ResponseStatus, List<GooglePlayGames.BasicApi.Multiplayer.Player>> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (PlayerManager.<>f__mg$cache5 == null)
			{
				PlayerManager.<>f__mg$cache5 = new PlayerManager.FetchListCallback(PlayerManager.InternalFetchConnectedCallback);
			}
			PlayerManager.FetchListCallback callback2 = PlayerManager.<>f__mg$cache5;
			Action<PlayerManager.FetchListResponse> callback3 = delegate(PlayerManager.FetchListResponse rsp)
			{
				this.HandleFetchCollected(rsp, callback);
			};
			if (PlayerManager.<>f__mg$cache4 == null)
			{
				PlayerManager.<>f__mg$cache4 = new Func<IntPtr, PlayerManager.FetchListResponse>(PlayerManager.FetchListResponse.FromPointer);
			}
			PlayerManager.PlayerManager_FetchConnected(self, data_source, callback2, Callbacks.ToIntPtr<PlayerManager.FetchListResponse>(callback3, PlayerManager.<>f__mg$cache4));
		}

		// Token: 0x06004B2C RID: 19244 RVA: 0x001523E5 File Offset: 0x001507E5
		[MonoPInvokeCallback(typeof(PlayerManager.FetchListCallback))]
		private static void InternalFetchConnectedCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("PlayerManager#InternalFetchConnectedCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B2D RID: 19245 RVA: 0x001523F4 File Offset: 0x001507F4
		internal void HandleFetchCollected(PlayerManager.FetchListResponse rsp, Action<ResponseStatus, List<GooglePlayGames.BasicApi.Multiplayer.Player>> callback)
		{
			List<GooglePlayGames.BasicApi.Multiplayer.Player> list = new List<GooglePlayGames.BasicApi.Multiplayer.Player>();
			if (rsp.Status() == CommonErrorStatus.ResponseStatus.VALID || rsp.Status() == CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
			{
				Logger.d("Got " + rsp.Length().ToUInt64() + " players");
				foreach (NativePlayer nativePlayer in rsp)
				{
					list.Add(nativePlayer.AsPlayer());
				}
			}
			callback((ResponseStatus)rsp.Status(), list);
		}

		// Token: 0x04004D11 RID: 19729
		private readonly GameServices mGameServices;

		// Token: 0x04004D12 RID: 19730
		[CompilerGenerated]
		private static Func<IntPtr, PlayerManager.FetchSelfResponse> <>f__mg$cache0;

		// Token: 0x04004D13 RID: 19731
		[CompilerGenerated]
		private static PlayerManager.FetchSelfCallback <>f__mg$cache1;

		// Token: 0x04004D14 RID: 19732
		[CompilerGenerated]
		private static Func<IntPtr, PlayerManager.FetchResponse> <>f__mg$cache2;

		// Token: 0x04004D15 RID: 19733
		[CompilerGenerated]
		private static PlayerManager.FetchCallback <>f__mg$cache3;

		// Token: 0x04004D16 RID: 19734
		[CompilerGenerated]
		private static Func<IntPtr, PlayerManager.FetchListResponse> <>f__mg$cache4;

		// Token: 0x04004D17 RID: 19735
		[CompilerGenerated]
		private static PlayerManager.FetchListCallback <>f__mg$cache5;

		// Token: 0x02000E46 RID: 3654
		internal class FetchListResponse : BaseReferenceHolder, IEnumerable<NativePlayer>, IEnumerable
		{
			// Token: 0x06004B2E RID: 19246 RVA: 0x001524A0 File Offset: 0x001508A0
			internal FetchListResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B2F RID: 19247 RVA: 0x001524A9 File Offset: 0x001508A9
			protected override void CallDispose(HandleRef selfPointer)
			{
				PlayerManager.PlayerManager_FetchListResponse_Dispose(base.SelfPtr());
			}

			// Token: 0x06004B30 RID: 19248 RVA: 0x001524B6 File Offset: 0x001508B6
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return PlayerManager.PlayerManager_FetchListResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B31 RID: 19249 RVA: 0x001524C3 File Offset: 0x001508C3
			public IEnumerator<NativePlayer> GetEnumerator()
			{
				return PInvokeUtilities.ToEnumerator<NativePlayer>(this.Length(), (UIntPtr index) => this.GetElement(index));
			}

			// Token: 0x06004B32 RID: 19250 RVA: 0x001524DC File Offset: 0x001508DC
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06004B33 RID: 19251 RVA: 0x001524E4 File Offset: 0x001508E4
			internal UIntPtr Length()
			{
				return PlayerManager.PlayerManager_FetchListResponse_GetData_Length(base.SelfPtr());
			}

			// Token: 0x06004B34 RID: 19252 RVA: 0x001524F4 File Offset: 0x001508F4
			internal NativePlayer GetElement(UIntPtr index)
			{
				if (index.ToUInt64() >= this.Length().ToUInt64())
				{
					throw new ArgumentOutOfRangeException();
				}
				return new NativePlayer(PlayerManager.PlayerManager_FetchListResponse_GetData_GetElement(base.SelfPtr(), index));
			}

			// Token: 0x06004B35 RID: 19253 RVA: 0x00152532 File Offset: 0x00150932
			internal static PlayerManager.FetchListResponse FromPointer(IntPtr selfPointer)
			{
				if (PInvokeUtilities.IsNull(selfPointer))
				{
					return null;
				}
				return new PlayerManager.FetchListResponse(selfPointer);
			}
		}

		// Token: 0x02000E47 RID: 3655
		internal class FetchResponseCollector
		{
			// Token: 0x04004D18 RID: 19736
			internal int pendingCount;

			// Token: 0x04004D19 RID: 19737
			internal List<NativePlayer> results = new List<NativePlayer>();

			// Token: 0x04004D1A RID: 19738
			internal Action<NativePlayer[]> callback;
		}

		// Token: 0x02000E48 RID: 3656
		internal class FetchResponse : BaseReferenceHolder
		{
			// Token: 0x06004B38 RID: 19256 RVA: 0x00152563 File Offset: 0x00150963
			internal FetchResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B39 RID: 19257 RVA: 0x0015256C File Offset: 0x0015096C
			protected override void CallDispose(HandleRef selfPointer)
			{
				PlayerManager.PlayerManager_FetchResponse_Dispose(base.SelfPtr());
			}

			// Token: 0x06004B3A RID: 19258 RVA: 0x00152579 File Offset: 0x00150979
			internal NativePlayer GetPlayer()
			{
				return new NativePlayer(PlayerManager.PlayerManager_FetchResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004B3B RID: 19259 RVA: 0x0015258B File Offset: 0x0015098B
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return PlayerManager.PlayerManager_FetchResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B3C RID: 19260 RVA: 0x00152598 File Offset: 0x00150998
			internal static PlayerManager.FetchResponse FromPointer(IntPtr selfPointer)
			{
				if (PInvokeUtilities.IsNull(selfPointer))
				{
					return null;
				}
				return new PlayerManager.FetchResponse(selfPointer);
			}
		}

		// Token: 0x02000E49 RID: 3657
		internal class FetchSelfResponse : BaseReferenceHolder
		{
			// Token: 0x06004B3D RID: 19261 RVA: 0x001525AD File Offset: 0x001509AD
			internal FetchSelfResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B3E RID: 19262 RVA: 0x001525B6 File Offset: 0x001509B6
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return PlayerManager.PlayerManager_FetchSelfResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B3F RID: 19263 RVA: 0x001525C3 File Offset: 0x001509C3
			internal NativePlayer Self()
			{
				return new NativePlayer(PlayerManager.PlayerManager_FetchSelfResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004B40 RID: 19264 RVA: 0x001525D5 File Offset: 0x001509D5
			protected override void CallDispose(HandleRef selfPointer)
			{
				PlayerManager.PlayerManager_FetchSelfResponse_Dispose(base.SelfPtr());
			}

			// Token: 0x06004B41 RID: 19265 RVA: 0x001525E2 File Offset: 0x001509E2
			internal static PlayerManager.FetchSelfResponse FromPointer(IntPtr selfPointer)
			{
				if (PInvokeUtilities.IsNull(selfPointer))
				{
					return null;
				}
				return new PlayerManager.FetchSelfResponse(selfPointer);
			}
		}
	}
}
