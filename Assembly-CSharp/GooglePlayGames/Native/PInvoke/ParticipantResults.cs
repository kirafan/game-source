﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E43 RID: 3651
	internal class ParticipantResults : BaseReferenceHolder
	{
		// Token: 0x06004B1D RID: 19229 RVA: 0x00152164 File Offset: 0x00150564
		internal ParticipantResults(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004B1E RID: 19230 RVA: 0x0015216D File Offset: 0x0015056D
		internal bool HasResultsForParticipant(string participantId)
		{
			return ParticipantResults.ParticipantResults_HasResultsForParticipant(base.SelfPtr(), participantId);
		}

		// Token: 0x06004B1F RID: 19231 RVA: 0x0015217B File Offset: 0x0015057B
		internal uint PlacingForParticipant(string participantId)
		{
			return ParticipantResults.ParticipantResults_PlaceForParticipant(base.SelfPtr(), participantId);
		}

		// Token: 0x06004B20 RID: 19232 RVA: 0x00152189 File Offset: 0x00150589
		internal Types.MatchResult ResultsForParticipant(string participantId)
		{
			return ParticipantResults.ParticipantResults_MatchResultForParticipant(base.SelfPtr(), participantId);
		}

		// Token: 0x06004B21 RID: 19233 RVA: 0x00152197 File Offset: 0x00150597
		internal ParticipantResults WithResult(string participantId, uint placing, Types.MatchResult result)
		{
			return new ParticipantResults(ParticipantResults.ParticipantResults_WithResult(base.SelfPtr(), participantId, placing, result));
		}

		// Token: 0x06004B22 RID: 19234 RVA: 0x001521AC File Offset: 0x001505AC
		protected override void CallDispose(HandleRef selfPointer)
		{
			ParticipantResults.ParticipantResults_Dispose(selfPointer);
		}
	}
}
