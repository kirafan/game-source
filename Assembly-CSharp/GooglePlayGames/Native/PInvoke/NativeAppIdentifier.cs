﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E24 RID: 3620
	internal class NativeAppIdentifier : BaseReferenceHolder
	{
		// Token: 0x060049EB RID: 18923 RVA: 0x00150120 File Offset: 0x0014E520
		internal NativeAppIdentifier(IntPtr pointer) : base(pointer)
		{
		}

		// Token: 0x060049EC RID: 18924
		[DllImport("gpg")]
		internal static extern IntPtr NearbyUtils_ConstructAppIdentifier(string appId);

		// Token: 0x060049ED RID: 18925 RVA: 0x00150129 File Offset: 0x0014E529
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.AppIdentifier_GetIdentifier(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049EE RID: 18926 RVA: 0x0015013C File Offset: 0x0014E53C
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionTypes.AppIdentifier_Dispose(selfPointer);
		}

		// Token: 0x060049EF RID: 18927 RVA: 0x00150144 File Offset: 0x0014E544
		internal static NativeAppIdentifier FromString(string appId)
		{
			return new NativeAppIdentifier(NativeAppIdentifier.NearbyUtils_ConstructAppIdentifier(appId));
		}
	}
}
