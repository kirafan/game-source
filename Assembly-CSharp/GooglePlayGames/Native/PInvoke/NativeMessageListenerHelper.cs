﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E2B RID: 3627
	internal class NativeMessageListenerHelper : BaseReferenceHolder
	{
		// Token: 0x06004A2A RID: 18986 RVA: 0x00150703 File Offset: 0x0014EB03
		internal NativeMessageListenerHelper() : base(MessageListenerHelper.MessageListenerHelper_Construct())
		{
		}

		// Token: 0x06004A2B RID: 18987 RVA: 0x00150710 File Offset: 0x0014EB10
		protected override void CallDispose(HandleRef selfPointer)
		{
			MessageListenerHelper.MessageListenerHelper_Dispose(selfPointer);
		}

		// Token: 0x06004A2C RID: 18988 RVA: 0x00150718 File Offset: 0x0014EB18
		internal void SetOnMessageReceivedCallback(NativeMessageListenerHelper.OnMessageReceived callback)
		{
			HandleRef self = base.SelfPtr();
			if (NativeMessageListenerHelper.<>f__mg$cache0 == null)
			{
				NativeMessageListenerHelper.<>f__mg$cache0 = new MessageListenerHelper.OnMessageReceivedCallback(NativeMessageListenerHelper.InternalOnMessageReceivedCallback);
			}
			MessageListenerHelper.MessageListenerHelper_SetOnMessageReceivedCallback(self, NativeMessageListenerHelper.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004A2D RID: 18989 RVA: 0x00150748 File Offset: 0x0014EB48
		[MonoPInvokeCallback(typeof(MessageListenerHelper.OnMessageReceivedCallback))]
		private static void InternalOnMessageReceivedCallback(long id, string name, IntPtr data, UIntPtr dataLength, bool isReliable, IntPtr userData)
		{
			NativeMessageListenerHelper.OnMessageReceived onMessageReceived = Callbacks.IntPtrToPermanentCallback<NativeMessageListenerHelper.OnMessageReceived>(userData);
			if (onMessageReceived == null)
			{
				return;
			}
			try
			{
				onMessageReceived(id, name, Callbacks.IntPtrAndSizeToByteArray(data, dataLength), isReliable);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing NativeMessageListenerHelper#InternalOnMessageReceivedCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004A2E RID: 18990 RVA: 0x001507A0 File Offset: 0x0014EBA0
		internal void SetOnDisconnectedCallback(Action<long, string> callback)
		{
			HandleRef self = base.SelfPtr();
			if (NativeMessageListenerHelper.<>f__mg$cache1 == null)
			{
				NativeMessageListenerHelper.<>f__mg$cache1 = new MessageListenerHelper.OnDisconnectedCallback(NativeMessageListenerHelper.InternalOnDisconnectedCallback);
			}
			MessageListenerHelper.MessageListenerHelper_SetOnDisconnectedCallback(self, NativeMessageListenerHelper.<>f__mg$cache1, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004A2F RID: 18991 RVA: 0x001507D0 File Offset: 0x0014EBD0
		[MonoPInvokeCallback(typeof(MessageListenerHelper.OnDisconnectedCallback))]
		private static void InternalOnDisconnectedCallback(long id, string lostEndpointId, IntPtr userData)
		{
			Action<long, string> action = Callbacks.IntPtrToPermanentCallback<Action<long, string>>(userData);
			if (action == null)
			{
				return;
			}
			try
			{
				action(id, lostEndpointId);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing NativeMessageListenerHelper#InternalOnDisconnectedCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x04004D02 RID: 19714
		[CompilerGenerated]
		private static MessageListenerHelper.OnMessageReceivedCallback <>f__mg$cache0;

		// Token: 0x04004D03 RID: 19715
		[CompilerGenerated]
		private static MessageListenerHelper.OnDisconnectedCallback <>f__mg$cache1;

		// Token: 0x02000E2C RID: 3628
		// (Invoke) Token: 0x06004A31 RID: 18993
		internal delegate void OnMessageReceived(long localClientId, string remoteEndpointId, byte[] data, bool isReliable);
	}
}
