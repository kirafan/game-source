﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E59 RID: 3673
	internal class SnapshotManager
	{
		// Token: 0x06004BC2 RID: 19394 RVA: 0x0015375E File Offset: 0x00151B5E
		internal SnapshotManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x06004BC3 RID: 19395 RVA: 0x00153774 File Offset: 0x00151B74
		internal void FetchAll(Types.DataSource source, Action<SnapshotManager.FetchAllResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (SnapshotManager.<>f__mg$cache1 == null)
			{
				SnapshotManager.<>f__mg$cache1 = new SnapshotManager.FetchAllCallback(SnapshotManager.InternalFetchAllCallback);
			}
			SnapshotManager.FetchAllCallback callback2 = SnapshotManager.<>f__mg$cache1;
			if (SnapshotManager.<>f__mg$cache0 == null)
			{
				SnapshotManager.<>f__mg$cache0 = new Func<IntPtr, SnapshotManager.FetchAllResponse>(SnapshotManager.FetchAllResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_FetchAll(self, source, callback2, Callbacks.ToIntPtr<SnapshotManager.FetchAllResponse>(callback, SnapshotManager.<>f__mg$cache0));
		}

		// Token: 0x06004BC4 RID: 19396 RVA: 0x001537D2 File Offset: 0x00151BD2
		[MonoPInvokeCallback(typeof(SnapshotManager.FetchAllCallback))]
		internal static void InternalFetchAllCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("SnapshotManager#FetchAllCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004BC5 RID: 19397 RVA: 0x001537E4 File Offset: 0x00151BE4
		internal void SnapshotSelectUI(bool allowCreate, bool allowDelete, uint maxSnapshots, string uiTitle, Action<SnapshotManager.SnapshotSelectUIResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (SnapshotManager.<>f__mg$cache3 == null)
			{
				SnapshotManager.<>f__mg$cache3 = new SnapshotManager.SnapshotSelectUICallback(SnapshotManager.InternalSnapshotSelectUICallback);
			}
			SnapshotManager.SnapshotSelectUICallback callback2 = SnapshotManager.<>f__mg$cache3;
			if (SnapshotManager.<>f__mg$cache2 == null)
			{
				SnapshotManager.<>f__mg$cache2 = new Func<IntPtr, SnapshotManager.SnapshotSelectUIResponse>(SnapshotManager.SnapshotSelectUIResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_ShowSelectUIOperation(self, allowCreate, allowDelete, maxSnapshots, uiTitle, callback2, Callbacks.ToIntPtr<SnapshotManager.SnapshotSelectUIResponse>(callback, SnapshotManager.<>f__mg$cache2));
		}

		// Token: 0x06004BC6 RID: 19398 RVA: 0x00153847 File Offset: 0x00151C47
		[MonoPInvokeCallback(typeof(SnapshotManager.SnapshotSelectUICallback))]
		internal static void InternalSnapshotSelectUICallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("SnapshotManager#SnapshotSelectUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004BC7 RID: 19399 RVA: 0x00153858 File Offset: 0x00151C58
		internal void Open(string fileName, Types.DataSource source, Types.SnapshotConflictPolicy conflictPolicy, Action<SnapshotManager.OpenResponse> callback)
		{
			Misc.CheckNotNull<string>(fileName);
			Misc.CheckNotNull<Action<SnapshotManager.OpenResponse>>(callback);
			HandleRef self = this.mServices.AsHandle();
			if (SnapshotManager.<>f__mg$cache5 == null)
			{
				SnapshotManager.<>f__mg$cache5 = new SnapshotManager.OpenCallback(SnapshotManager.InternalOpenCallback);
			}
			SnapshotManager.OpenCallback callback2 = SnapshotManager.<>f__mg$cache5;
			if (SnapshotManager.<>f__mg$cache4 == null)
			{
				SnapshotManager.<>f__mg$cache4 = new Func<IntPtr, SnapshotManager.OpenResponse>(SnapshotManager.OpenResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_Open(self, source, fileName, conflictPolicy, callback2, Callbacks.ToIntPtr<SnapshotManager.OpenResponse>(callback, SnapshotManager.<>f__mg$cache4));
		}

		// Token: 0x06004BC8 RID: 19400 RVA: 0x001538C8 File Offset: 0x00151CC8
		[MonoPInvokeCallback(typeof(SnapshotManager.OpenCallback))]
		internal static void InternalOpenCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("SnapshotManager#OpenCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004BC9 RID: 19401 RVA: 0x001538D8 File Offset: 0x00151CD8
		internal void Commit(NativeSnapshotMetadata metadata, NativeSnapshotMetadataChange metadataChange, byte[] updatedData, Action<SnapshotManager.CommitResponse> callback)
		{
			Misc.CheckNotNull<NativeSnapshotMetadata>(metadata);
			Misc.CheckNotNull<NativeSnapshotMetadataChange>(metadataChange);
			HandleRef self = this.mServices.AsHandle();
			IntPtr snapshot_metadata = metadata.AsPointer();
			IntPtr metadata_change = metadataChange.AsPointer();
			UIntPtr data_size = new UIntPtr((ulong)((long)updatedData.Length));
			if (SnapshotManager.<>f__mg$cache7 == null)
			{
				SnapshotManager.<>f__mg$cache7 = new SnapshotManager.CommitCallback(SnapshotManager.InternalCommitCallback);
			}
			SnapshotManager.CommitCallback callback2 = SnapshotManager.<>f__mg$cache7;
			if (SnapshotManager.<>f__mg$cache6 == null)
			{
				SnapshotManager.<>f__mg$cache6 = new Func<IntPtr, SnapshotManager.CommitResponse>(SnapshotManager.CommitResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_Commit(self, snapshot_metadata, metadata_change, updatedData, data_size, callback2, Callbacks.ToIntPtr<SnapshotManager.CommitResponse>(callback, SnapshotManager.<>f__mg$cache6));
		}

		// Token: 0x06004BCA RID: 19402 RVA: 0x0015395C File Offset: 0x00151D5C
		internal void Resolve(NativeSnapshotMetadata metadata, NativeSnapshotMetadataChange metadataChange, string conflictId, Action<SnapshotManager.CommitResponse> callback)
		{
			Misc.CheckNotNull<NativeSnapshotMetadata>(metadata);
			Misc.CheckNotNull<NativeSnapshotMetadataChange>(metadataChange);
			Misc.CheckNotNull<string>(conflictId);
			HandleRef self = this.mServices.AsHandle();
			IntPtr snapshot_metadata = metadata.AsPointer();
			IntPtr metadata_change = metadataChange.AsPointer();
			if (SnapshotManager.<>f__mg$cache9 == null)
			{
				SnapshotManager.<>f__mg$cache9 = new SnapshotManager.CommitCallback(SnapshotManager.InternalCommitCallback);
			}
			SnapshotManager.CommitCallback callback2 = SnapshotManager.<>f__mg$cache9;
			if (SnapshotManager.<>f__mg$cache8 == null)
			{
				SnapshotManager.<>f__mg$cache8 = new Func<IntPtr, SnapshotManager.CommitResponse>(SnapshotManager.CommitResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_ResolveConflict(self, snapshot_metadata, metadata_change, conflictId, callback2, Callbacks.ToIntPtr<SnapshotManager.CommitResponse>(callback, SnapshotManager.<>f__mg$cache8));
		}

		// Token: 0x06004BCB RID: 19403 RVA: 0x001539DC File Offset: 0x00151DDC
		[MonoPInvokeCallback(typeof(SnapshotManager.CommitCallback))]
		internal static void InternalCommitCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("SnapshotManager#CommitCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004BCC RID: 19404 RVA: 0x001539EB File Offset: 0x00151DEB
		internal void Delete(NativeSnapshotMetadata metadata)
		{
			Misc.CheckNotNull<NativeSnapshotMetadata>(metadata);
			SnapshotManager.SnapshotManager_Delete(this.mServices.AsHandle(), metadata.AsPointer());
		}

		// Token: 0x06004BCD RID: 19405 RVA: 0x00153A0C File Offset: 0x00151E0C
		internal void Read(NativeSnapshotMetadata metadata, Action<SnapshotManager.ReadResponse> callback)
		{
			Misc.CheckNotNull<NativeSnapshotMetadata>(metadata);
			Misc.CheckNotNull<Action<SnapshotManager.ReadResponse>>(callback);
			HandleRef self = this.mServices.AsHandle();
			IntPtr snapshot_metadata = metadata.AsPointer();
			if (SnapshotManager.<>f__mg$cacheB == null)
			{
				SnapshotManager.<>f__mg$cacheB = new SnapshotManager.ReadCallback(SnapshotManager.InternalReadCallback);
			}
			SnapshotManager.ReadCallback callback2 = SnapshotManager.<>f__mg$cacheB;
			if (SnapshotManager.<>f__mg$cacheA == null)
			{
				SnapshotManager.<>f__mg$cacheA = new Func<IntPtr, SnapshotManager.ReadResponse>(SnapshotManager.ReadResponse.FromPointer);
			}
			SnapshotManager.SnapshotManager_Read(self, snapshot_metadata, callback2, Callbacks.ToIntPtr<SnapshotManager.ReadResponse>(callback, SnapshotManager.<>f__mg$cacheA));
		}

		// Token: 0x06004BCE RID: 19406 RVA: 0x00153A7D File Offset: 0x00151E7D
		[MonoPInvokeCallback(typeof(SnapshotManager.ReadCallback))]
		internal static void InternalReadCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("SnapshotManager#ReadCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x04004D3D RID: 19773
		private readonly GameServices mServices;

		// Token: 0x04004D3E RID: 19774
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.FetchAllResponse> <>f__mg$cache0;

		// Token: 0x04004D3F RID: 19775
		[CompilerGenerated]
		private static SnapshotManager.FetchAllCallback <>f__mg$cache1;

		// Token: 0x04004D40 RID: 19776
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.SnapshotSelectUIResponse> <>f__mg$cache2;

		// Token: 0x04004D41 RID: 19777
		[CompilerGenerated]
		private static SnapshotManager.SnapshotSelectUICallback <>f__mg$cache3;

		// Token: 0x04004D42 RID: 19778
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.OpenResponse> <>f__mg$cache4;

		// Token: 0x04004D43 RID: 19779
		[CompilerGenerated]
		private static SnapshotManager.OpenCallback <>f__mg$cache5;

		// Token: 0x04004D44 RID: 19780
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.CommitResponse> <>f__mg$cache6;

		// Token: 0x04004D45 RID: 19781
		[CompilerGenerated]
		private static SnapshotManager.CommitCallback <>f__mg$cache7;

		// Token: 0x04004D46 RID: 19782
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.CommitResponse> <>f__mg$cache8;

		// Token: 0x04004D47 RID: 19783
		[CompilerGenerated]
		private static SnapshotManager.CommitCallback <>f__mg$cache9;

		// Token: 0x04004D48 RID: 19784
		[CompilerGenerated]
		private static Func<IntPtr, SnapshotManager.ReadResponse> <>f__mg$cacheA;

		// Token: 0x04004D49 RID: 19785
		[CompilerGenerated]
		private static SnapshotManager.ReadCallback <>f__mg$cacheB;

		// Token: 0x02000E5A RID: 3674
		internal class OpenResponse : BaseReferenceHolder
		{
			// Token: 0x06004BCF RID: 19407 RVA: 0x00153A8C File Offset: 0x00151E8C
			internal OpenResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BD0 RID: 19408 RVA: 0x00153A95 File Offset: 0x00151E95
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.SnapshotOpenStatus)0;
			}

			// Token: 0x06004BD1 RID: 19409 RVA: 0x00153AA0 File Offset: 0x00151EA0
			internal CommonErrorStatus.SnapshotOpenStatus ResponseStatus()
			{
				return SnapshotManager.SnapshotManager_OpenResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BD2 RID: 19410 RVA: 0x00153AAD File Offset: 0x00151EAD
			internal string ConflictId()
			{
				if (this.ResponseStatus() != CommonErrorStatus.SnapshotOpenStatus.VALID_WITH_CONFLICT)
				{
					throw new InvalidOperationException("OpenResponse did not have a conflict");
				}
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => SnapshotManager.SnapshotManager_OpenResponse_GetConflictId(base.SelfPtr(), out_string, out_size));
			}

			// Token: 0x06004BD3 RID: 19411 RVA: 0x00153AD7 File Offset: 0x00151ED7
			internal NativeSnapshotMetadata Data()
			{
				if (this.ResponseStatus() != CommonErrorStatus.SnapshotOpenStatus.VALID)
				{
					throw new InvalidOperationException("OpenResponse had a conflict");
				}
				return new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_OpenResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004BD4 RID: 19412 RVA: 0x00153B00 File Offset: 0x00151F00
			internal NativeSnapshotMetadata ConflictOriginal()
			{
				if (this.ResponseStatus() != CommonErrorStatus.SnapshotOpenStatus.VALID_WITH_CONFLICT)
				{
					throw new InvalidOperationException("OpenResponse did not have a conflict");
				}
				return new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_OpenResponse_GetConflictOriginal(base.SelfPtr()));
			}

			// Token: 0x06004BD5 RID: 19413 RVA: 0x00153B29 File Offset: 0x00151F29
			internal NativeSnapshotMetadata ConflictUnmerged()
			{
				if (this.ResponseStatus() != CommonErrorStatus.SnapshotOpenStatus.VALID_WITH_CONFLICT)
				{
					throw new InvalidOperationException("OpenResponse did not have a conflict");
				}
				return new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_OpenResponse_GetConflictUnmerged(base.SelfPtr()));
			}

			// Token: 0x06004BD6 RID: 19414 RVA: 0x00153B52 File Offset: 0x00151F52
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotManager.SnapshotManager_OpenResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BD7 RID: 19415 RVA: 0x00153B5A File Offset: 0x00151F5A
			internal static SnapshotManager.OpenResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new SnapshotManager.OpenResponse(pointer);
			}
		}

		// Token: 0x02000E5B RID: 3675
		internal class FetchAllResponse : BaseReferenceHolder
		{
			// Token: 0x06004BD9 RID: 19417 RVA: 0x00153B8F File Offset: 0x00151F8F
			internal FetchAllResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BDA RID: 19418 RVA: 0x00153B98 File Offset: 0x00151F98
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return SnapshotManager.SnapshotManager_FetchAllResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BDB RID: 19419 RVA: 0x00153BA5 File Offset: 0x00151FA5
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004BDC RID: 19420 RVA: 0x00153BB0 File Offset: 0x00151FB0
			internal IEnumerable<NativeSnapshotMetadata> Data()
			{
				return PInvokeUtilities.ToEnumerable<NativeSnapshotMetadata>(SnapshotManager.SnapshotManager_FetchAllResponse_GetData_Length(base.SelfPtr()), (UIntPtr index) => new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_FetchAllResponse_GetData_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004BDD RID: 19421 RVA: 0x00153BCE File Offset: 0x00151FCE
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotManager.SnapshotManager_FetchAllResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BDE RID: 19422 RVA: 0x00153BD6 File Offset: 0x00151FD6
			internal static SnapshotManager.FetchAllResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new SnapshotManager.FetchAllResponse(pointer);
			}
		}

		// Token: 0x02000E5C RID: 3676
		internal class CommitResponse : BaseReferenceHolder
		{
			// Token: 0x06004BE0 RID: 19424 RVA: 0x00153C0F File Offset: 0x0015200F
			internal CommitResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BE1 RID: 19425 RVA: 0x00153C18 File Offset: 0x00152018
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return SnapshotManager.SnapshotManager_CommitResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BE2 RID: 19426 RVA: 0x00153C25 File Offset: 0x00152025
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004BE3 RID: 19427 RVA: 0x00153C30 File Offset: 0x00152030
			internal NativeSnapshotMetadata Data()
			{
				if (!this.RequestSucceeded())
				{
					throw new InvalidOperationException("Request did not succeed");
				}
				return new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_CommitResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004BE4 RID: 19428 RVA: 0x00153C58 File Offset: 0x00152058
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotManager.SnapshotManager_CommitResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BE5 RID: 19429 RVA: 0x00153C60 File Offset: 0x00152060
			internal static SnapshotManager.CommitResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new SnapshotManager.CommitResponse(pointer);
			}
		}

		// Token: 0x02000E5D RID: 3677
		internal class ReadResponse : BaseReferenceHolder
		{
			// Token: 0x06004BE6 RID: 19430 RVA: 0x00153C86 File Offset: 0x00152086
			internal ReadResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BE7 RID: 19431 RVA: 0x00153C8F File Offset: 0x0015208F
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return SnapshotManager.SnapshotManager_CommitResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BE8 RID: 19432 RVA: 0x00153C9C File Offset: 0x0015209C
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004BE9 RID: 19433 RVA: 0x00153CA7 File Offset: 0x001520A7
			internal byte[] Data()
			{
				if (!this.RequestSucceeded())
				{
					throw new InvalidOperationException("Request did not succeed");
				}
				return PInvokeUtilities.OutParamsToArray<byte>((byte[] out_bytes, UIntPtr out_size) => SnapshotManager.SnapshotManager_ReadResponse_GetData(base.SelfPtr(), out_bytes, out_size));
			}

			// Token: 0x06004BEA RID: 19434 RVA: 0x00153CD0 File Offset: 0x001520D0
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotManager.SnapshotManager_ReadResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BEB RID: 19435 RVA: 0x00153CD8 File Offset: 0x001520D8
			internal static SnapshotManager.ReadResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new SnapshotManager.ReadResponse(pointer);
			}
		}

		// Token: 0x02000E5E RID: 3678
		internal class SnapshotSelectUIResponse : BaseReferenceHolder
		{
			// Token: 0x06004BED RID: 19437 RVA: 0x00153D0D File Offset: 0x0015210D
			internal SnapshotSelectUIResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BEE RID: 19438 RVA: 0x00153D16 File Offset: 0x00152116
			internal CommonErrorStatus.UIStatus RequestStatus()
			{
				return SnapshotManager.SnapshotManager_SnapshotSelectUIResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BEF RID: 19439 RVA: 0x00153D23 File Offset: 0x00152123
			internal bool RequestSucceeded()
			{
				return this.RequestStatus() > (CommonErrorStatus.UIStatus)0;
			}

			// Token: 0x06004BF0 RID: 19440 RVA: 0x00153D2E File Offset: 0x0015212E
			internal NativeSnapshotMetadata Data()
			{
				if (!this.RequestSucceeded())
				{
					throw new InvalidOperationException("Request did not succeed");
				}
				return new NativeSnapshotMetadata(SnapshotManager.SnapshotManager_SnapshotSelectUIResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004BF1 RID: 19441 RVA: 0x00153D56 File Offset: 0x00152156
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotManager.SnapshotManager_SnapshotSelectUIResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BF2 RID: 19442 RVA: 0x00153D5E File Offset: 0x0015215E
			internal static SnapshotManager.SnapshotSelectUIResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new SnapshotManager.SnapshotSelectUIResponse(pointer);
			}
		}
	}
}
