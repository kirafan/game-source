﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E11 RID: 3601
	internal static class Callbacks
	{
		// Token: 0x06004951 RID: 18769 RVA: 0x0014E940 File Offset: 0x0014CD40
		internal static IntPtr ToIntPtr<T>(Action<T> callback, Func<IntPtr, T> conversionFunction) where T : BaseReferenceHolder
		{
			Action<IntPtr> callback2 = delegate(IntPtr result)
			{
				using (T t = conversionFunction(result))
				{
					if (callback != null)
					{
						callback(t);
					}
				}
			};
			return Callbacks.ToIntPtr(callback2);
		}

		// Token: 0x06004952 RID: 18770 RVA: 0x0014E974 File Offset: 0x0014CD74
		internal static IntPtr ToIntPtr<T, P>(Action<T, P> callback, Func<IntPtr, P> conversionFunction) where P : BaseReferenceHolder
		{
			Action<T, IntPtr> callback2 = delegate(T param1, IntPtr param2)
			{
				using (P p = conversionFunction(param2))
				{
					if (callback != null)
					{
						callback(param1, p);
					}
				}
			};
			return Callbacks.ToIntPtr(callback2);
		}

		// Token: 0x06004953 RID: 18771 RVA: 0x0014E9A8 File Offset: 0x0014CDA8
		internal static IntPtr ToIntPtr(Delegate callback)
		{
			if (callback == null)
			{
				return IntPtr.Zero;
			}
			GCHandle value = GCHandle.Alloc(callback);
			return GCHandle.ToIntPtr(value);
		}

		// Token: 0x06004954 RID: 18772 RVA: 0x0014E9CE File Offset: 0x0014CDCE
		internal static T IntPtrToTempCallback<T>(IntPtr handle) where T : class
		{
			return Callbacks.IntPtrToCallback<T>(handle, true);
		}

		// Token: 0x06004955 RID: 18773 RVA: 0x0014E9D8 File Offset: 0x0014CDD8
		private static T IntPtrToCallback<T>(IntPtr handle, bool unpinHandle) where T : class
		{
			if (PInvokeUtilities.IsNull(handle))
			{
				return (T)((object)null);
			}
			GCHandle gchandle = GCHandle.FromIntPtr(handle);
			T result;
			try
			{
				result = (T)((object)gchandle.Target);
			}
			catch (InvalidCastException ex)
			{
				Logger.e(string.Concat(new object[]
				{
					"GC Handle pointed to unexpected type: ",
					gchandle.Target.ToString(),
					". Expected ",
					typeof(T)
				}));
				throw ex;
			}
			finally
			{
				if (unpinHandle)
				{
					gchandle.Free();
				}
			}
			return result;
		}

		// Token: 0x06004956 RID: 18774 RVA: 0x0014EA7C File Offset: 0x0014CE7C
		internal static T IntPtrToPermanentCallback<T>(IntPtr handle) where T : class
		{
			return Callbacks.IntPtrToCallback<T>(handle, false);
		}

		// Token: 0x06004957 RID: 18775 RVA: 0x0014EA88 File Offset: 0x0014CE88
		[MonoPInvokeCallback(typeof(Callbacks.ShowUICallbackInternal))]
		internal static void InternalShowUICallback(CommonErrorStatus.UIStatus status, IntPtr data)
		{
			Logger.d("Showing UI Internal callback: " + status);
			Action<CommonErrorStatus.UIStatus> action = Callbacks.IntPtrToTempCallback<Action<CommonErrorStatus.UIStatus>>(data);
			try
			{
				action(status);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalShowAllUICallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004958 RID: 18776 RVA: 0x0014EAE4 File Offset: 0x0014CEE4
		internal static void PerformInternalCallback(string callbackName, Callbacks.Type callbackType, IntPtr response, IntPtr userData)
		{
			Logger.d("Entering internal callback for " + callbackName);
			Action<IntPtr> action = (callbackType != Callbacks.Type.Permanent) ? Callbacks.IntPtrToTempCallback<Action<IntPtr>>(userData) : Callbacks.IntPtrToPermanentCallback<Action<IntPtr>>(userData);
			if (action == null)
			{
				return;
			}
			try
			{
				action(response);
			}
			catch (Exception ex)
			{
				Logger.e(string.Concat(new object[]
				{
					"Error encountered executing ",
					callbackName,
					". Smothering to avoid passing exception into Native: ",
					ex
				}));
			}
		}

		// Token: 0x06004959 RID: 18777 RVA: 0x0014EB6C File Offset: 0x0014CF6C
		internal static void PerformInternalCallback<T>(string callbackName, Callbacks.Type callbackType, T param1, IntPtr param2, IntPtr userData)
		{
			Logger.d("Entering internal callback for " + callbackName);
			Action<T, IntPtr> action = null;
			try
			{
				action = ((callbackType != Callbacks.Type.Permanent) ? Callbacks.IntPtrToTempCallback<Action<T, IntPtr>>(userData) : Callbacks.IntPtrToPermanentCallback<Action<T, IntPtr>>(userData));
			}
			catch (Exception ex)
			{
				Logger.e(string.Concat(new object[]
				{
					"Error encountered converting ",
					callbackName,
					". Smothering to avoid passing exception into Native: ",
					ex
				}));
				return;
			}
			Logger.d("Internal Callback converted to action");
			if (action == null)
			{
				return;
			}
			try
			{
				action(param1, param2);
			}
			catch (Exception ex2)
			{
				Logger.e(string.Concat(new object[]
				{
					"Error encountered executing ",
					callbackName,
					". Smothering to avoid passing exception into Native: ",
					ex2
				}));
			}
		}

		// Token: 0x0600495A RID: 18778 RVA: 0x0014EC40 File Offset: 0x0014D040
		internal static Action<T> AsOnGameThreadCallback<T>(Action<T> toInvokeOnGameThread)
		{
			return delegate(T result)
			{
				if (toInvokeOnGameThread == null)
				{
					return;
				}
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toInvokeOnGameThread(result);
				});
			};
		}

		// Token: 0x0600495B RID: 18779 RVA: 0x0014EC68 File Offset: 0x0014D068
		internal static Action<T1, T2> AsOnGameThreadCallback<T1, T2>(Action<T1, T2> toInvokeOnGameThread)
		{
			return delegate(T1 result1, T2 result2)
			{
				if (toInvokeOnGameThread == null)
				{
					return;
				}
				PlayGamesHelperObject.RunOnGameThread(delegate
				{
					toInvokeOnGameThread(result1, result2);
				});
			};
		}

		// Token: 0x0600495C RID: 18780 RVA: 0x0014EC8E File Offset: 0x0014D08E
		internal static void AsCoroutine(IEnumerator routine)
		{
			PlayGamesHelperObject.RunCoroutine(routine);
		}

		// Token: 0x0600495D RID: 18781 RVA: 0x0014EC98 File Offset: 0x0014D098
		internal static byte[] IntPtrAndSizeToByteArray(IntPtr data, UIntPtr dataLength)
		{
			if (dataLength.ToUInt64() == 0UL)
			{
				return null;
			}
			byte[] array = new byte[dataLength.ToUInt32()];
			Marshal.Copy(data, array, 0, (int)dataLength.ToUInt32());
			return array;
		}

		// Token: 0x04004CE5 RID: 19685
		internal static readonly Action<CommonErrorStatus.UIStatus> NoopUICallback = delegate(CommonErrorStatus.UIStatus status)
		{
			Logger.d("Received UI callback: " + status);
		};

		// Token: 0x02000E12 RID: 3602
		// (Invoke) Token: 0x06004961 RID: 18785
		internal delegate void ShowUICallbackInternal(CommonErrorStatus.UIStatus status, IntPtr data);

		// Token: 0x02000E13 RID: 3603
		internal enum Type
		{
			// Token: 0x04004CE7 RID: 19687
			Permanent,
			// Token: 0x04004CE8 RID: 19688
			Temporary
		}
	}
}
