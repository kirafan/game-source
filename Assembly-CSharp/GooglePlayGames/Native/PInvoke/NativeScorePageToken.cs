﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E35 RID: 3637
	internal class NativeScorePageToken : BaseReferenceHolder
	{
		// Token: 0x06004A9D RID: 19101 RVA: 0x0015119E File Offset: 0x0014F59E
		internal NativeScorePageToken(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004A9E RID: 19102 RVA: 0x001511A7 File Offset: 0x0014F5A7
		protected override void CallDispose(HandleRef selfPointer)
		{
			ScorePage.ScorePage_ScorePageToken_Dispose(selfPointer);
		}
	}
}
