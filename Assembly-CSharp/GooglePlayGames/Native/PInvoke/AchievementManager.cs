﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E0B RID: 3595
	internal class AchievementManager
	{
		// Token: 0x06004923 RID: 18723 RVA: 0x0014E48B File Offset: 0x0014C88B
		internal AchievementManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x06004924 RID: 18724 RVA: 0x0014E49F File Offset: 0x0014C89F
		internal void ShowAllUI(Action<CommonErrorStatus.UIStatus> callback)
		{
			Misc.CheckNotNull<Action<CommonErrorStatus.UIStatus>>(callback);
			HandleRef self = this.mServices.AsHandle();
			if (AchievementManager.<>f__mg$cache0 == null)
			{
				AchievementManager.<>f__mg$cache0 = new AchievementManager.ShowAllUICallback(Callbacks.InternalShowUICallback);
			}
			AchievementManager.AchievementManager_ShowAllUI(self, AchievementManager.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004925 RID: 18725 RVA: 0x0014E4DC File Offset: 0x0014C8DC
		internal void FetchAll(Action<AchievementManager.FetchAllResponse> callback)
		{
			Misc.CheckNotNull<Action<AchievementManager.FetchAllResponse>>(callback);
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (AchievementManager.<>f__mg$cache2 == null)
			{
				AchievementManager.<>f__mg$cache2 = new AchievementManager.FetchAllCallback(AchievementManager.InternalFetchAllCallback);
			}
			AchievementManager.FetchAllCallback callback2 = AchievementManager.<>f__mg$cache2;
			if (AchievementManager.<>f__mg$cache1 == null)
			{
				AchievementManager.<>f__mg$cache1 = new Func<IntPtr, AchievementManager.FetchAllResponse>(AchievementManager.FetchAllResponse.FromPointer);
			}
			AchievementManager.AchievementManager_FetchAll(self, data_source, callback2, Callbacks.ToIntPtr<AchievementManager.FetchAllResponse>(callback, AchievementManager.<>f__mg$cache1));
		}

		// Token: 0x06004926 RID: 18726 RVA: 0x0014E541 File Offset: 0x0014C941
		[MonoPInvokeCallback(typeof(AchievementManager.FetchAllCallback))]
		private static void InternalFetchAllCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("AchievementManager#InternalFetchAllCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004927 RID: 18727 RVA: 0x0014E550 File Offset: 0x0014C950
		internal void Fetch(string achId, Action<AchievementManager.FetchResponse> callback)
		{
			Misc.CheckNotNull<string>(achId);
			Misc.CheckNotNull<Action<AchievementManager.FetchResponse>>(callback);
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (AchievementManager.<>f__mg$cache4 == null)
			{
				AchievementManager.<>f__mg$cache4 = new AchievementManager.FetchCallback(AchievementManager.InternalFetchCallback);
			}
			AchievementManager.FetchCallback callback2 = AchievementManager.<>f__mg$cache4;
			if (AchievementManager.<>f__mg$cache3 == null)
			{
				AchievementManager.<>f__mg$cache3 = new Func<IntPtr, AchievementManager.FetchResponse>(AchievementManager.FetchResponse.FromPointer);
			}
			AchievementManager.AchievementManager_Fetch(self, data_source, achId, callback2, Callbacks.ToIntPtr<AchievementManager.FetchResponse>(callback, AchievementManager.<>f__mg$cache3));
		}

		// Token: 0x06004928 RID: 18728 RVA: 0x0014E5BD File Offset: 0x0014C9BD
		[MonoPInvokeCallback(typeof(AchievementManager.FetchCallback))]
		private static void InternalFetchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("AchievementManager#InternalFetchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004929 RID: 18729 RVA: 0x0014E5CC File Offset: 0x0014C9CC
		internal void Increment(string achievementId, uint numSteps)
		{
			Misc.CheckNotNull<string>(achievementId);
			AchievementManager.AchievementManager_Increment(this.mServices.AsHandle(), achievementId, numSteps);
		}

		// Token: 0x0600492A RID: 18730 RVA: 0x0014E5E7 File Offset: 0x0014C9E7
		internal void SetStepsAtLeast(string achivementId, uint numSteps)
		{
			Misc.CheckNotNull<string>(achivementId);
			AchievementManager.AchievementManager_SetStepsAtLeast(this.mServices.AsHandle(), achivementId, numSteps);
		}

		// Token: 0x0600492B RID: 18731 RVA: 0x0014E602 File Offset: 0x0014CA02
		internal void Reveal(string achievementId)
		{
			Misc.CheckNotNull<string>(achievementId);
			AchievementManager.AchievementManager_Reveal(this.mServices.AsHandle(), achievementId);
		}

		// Token: 0x0600492C RID: 18732 RVA: 0x0014E61C File Offset: 0x0014CA1C
		internal void Unlock(string achievementId)
		{
			Misc.CheckNotNull<string>(achievementId);
			AchievementManager.AchievementManager_Unlock(this.mServices.AsHandle(), achievementId);
		}

		// Token: 0x04004CDC RID: 19676
		private readonly GameServices mServices;

		// Token: 0x04004CDD RID: 19677
		[CompilerGenerated]
		private static AchievementManager.ShowAllUICallback <>f__mg$cache0;

		// Token: 0x04004CDE RID: 19678
		[CompilerGenerated]
		private static Func<IntPtr, AchievementManager.FetchAllResponse> <>f__mg$cache1;

		// Token: 0x04004CDF RID: 19679
		[CompilerGenerated]
		private static AchievementManager.FetchAllCallback <>f__mg$cache2;

		// Token: 0x04004CE0 RID: 19680
		[CompilerGenerated]
		private static Func<IntPtr, AchievementManager.FetchResponse> <>f__mg$cache3;

		// Token: 0x04004CE1 RID: 19681
		[CompilerGenerated]
		private static AchievementManager.FetchCallback <>f__mg$cache4;

		// Token: 0x02000E0C RID: 3596
		internal class FetchResponse : BaseReferenceHolder
		{
			// Token: 0x0600492D RID: 18733 RVA: 0x0014E77B File Offset: 0x0014CB7B
			internal FetchResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x0600492E RID: 18734 RVA: 0x0014E784 File Offset: 0x0014CB84
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return AchievementManager.AchievementManager_FetchResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x0600492F RID: 18735 RVA: 0x0014E794 File Offset: 0x0014CB94
			internal NativeAchievement Achievement()
			{
				IntPtr selfPointer = AchievementManager.AchievementManager_FetchResponse_GetData(base.SelfPtr());
				return new NativeAchievement(selfPointer);
			}

			// Token: 0x06004930 RID: 18736 RVA: 0x0014E7B3 File Offset: 0x0014CBB3
			protected override void CallDispose(HandleRef selfPointer)
			{
				AchievementManager.AchievementManager_FetchResponse_Dispose(selfPointer);
			}

			// Token: 0x06004931 RID: 18737 RVA: 0x0014E7BB File Offset: 0x0014CBBB
			internal static AchievementManager.FetchResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new AchievementManager.FetchResponse(pointer);
			}
		}

		// Token: 0x02000E0D RID: 3597
		internal class FetchAllResponse : BaseReferenceHolder, IEnumerable<NativeAchievement>, IEnumerable
		{
			// Token: 0x06004932 RID: 18738 RVA: 0x0014E7E1 File Offset: 0x0014CBE1
			internal FetchAllResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004933 RID: 18739 RVA: 0x0014E7EA File Offset: 0x0014CBEA
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return AchievementManager.AchievementManager_FetchAllResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004934 RID: 18740 RVA: 0x0014E7F7 File Offset: 0x0014CBF7
			private UIntPtr Length()
			{
				return AchievementManager.AchievementManager_FetchAllResponse_GetData_Length(base.SelfPtr());
			}

			// Token: 0x06004935 RID: 18741 RVA: 0x0014E804 File Offset: 0x0014CC04
			private NativeAchievement GetElement(UIntPtr index)
			{
				if (index.ToUInt64() >= this.Length().ToUInt64())
				{
					throw new ArgumentOutOfRangeException();
				}
				return new NativeAchievement(AchievementManager.AchievementManager_FetchAllResponse_GetData_GetElement(base.SelfPtr(), index));
			}

			// Token: 0x06004936 RID: 18742 RVA: 0x0014E842 File Offset: 0x0014CC42
			public IEnumerator<NativeAchievement> GetEnumerator()
			{
				return PInvokeUtilities.ToEnumerator<NativeAchievement>(AchievementManager.AchievementManager_FetchAllResponse_GetData_Length(base.SelfPtr()), (UIntPtr index) => this.GetElement(index));
			}

			// Token: 0x06004937 RID: 18743 RVA: 0x0014E860 File Offset: 0x0014CC60
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06004938 RID: 18744 RVA: 0x0014E868 File Offset: 0x0014CC68
			protected override void CallDispose(HandleRef selfPointer)
			{
				AchievementManager.AchievementManager_FetchAllResponse_Dispose(selfPointer);
			}

			// Token: 0x06004939 RID: 18745 RVA: 0x0014E870 File Offset: 0x0014CC70
			internal static AchievementManager.FetchAllResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new AchievementManager.FetchAllResponse(pointer);
			}
		}
	}
}
