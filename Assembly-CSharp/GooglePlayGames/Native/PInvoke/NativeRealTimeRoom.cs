﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E31 RID: 3633
	internal class NativeRealTimeRoom : BaseReferenceHolder
	{
		// Token: 0x06004A73 RID: 19059 RVA: 0x00150E10 File Offset: 0x0014F210
		internal NativeRealTimeRoom(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004A74 RID: 19060 RVA: 0x00150E19 File Offset: 0x0014F219
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => RealTimeRoom.RealTimeRoom_Id(base.SelfPtr(), out_string, size));
		}

		// Token: 0x06004A75 RID: 19061 RVA: 0x00150E2C File Offset: 0x0014F22C
		internal IEnumerable<MultiplayerParticipant> Participants()
		{
			return PInvokeUtilities.ToEnumerable<MultiplayerParticipant>(RealTimeRoom.RealTimeRoom_Participants_Length(base.SelfPtr()), (UIntPtr index) => new MultiplayerParticipant(RealTimeRoom.RealTimeRoom_Participants_GetElement(base.SelfPtr(), index)));
		}

		// Token: 0x06004A76 RID: 19062 RVA: 0x00150E4C File Offset: 0x0014F24C
		internal uint ParticipantCount()
		{
			return RealTimeRoom.RealTimeRoom_Participants_Length(base.SelfPtr()).ToUInt32();
		}

		// Token: 0x06004A77 RID: 19063 RVA: 0x00150E6C File Offset: 0x0014F26C
		internal Types.RealTimeRoomStatus Status()
		{
			return RealTimeRoom.RealTimeRoom_Status(base.SelfPtr());
		}

		// Token: 0x06004A78 RID: 19064 RVA: 0x00150E79 File Offset: 0x0014F279
		protected override void CallDispose(HandleRef selfPointer)
		{
			RealTimeRoom.RealTimeRoom_Dispose(selfPointer);
		}

		// Token: 0x06004A79 RID: 19065 RVA: 0x00150E81 File Offset: 0x0014F281
		internal static NativeRealTimeRoom FromPointer(IntPtr selfPointer)
		{
			if (selfPointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeRealTimeRoom(selfPointer);
		}
	}
}
