﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E20 RID: 3616
	internal class FetchScoreSummaryResponse : BaseReferenceHolder
	{
		// Token: 0x060049B9 RID: 18873 RVA: 0x0014FBEC File Offset: 0x0014DFEC
		internal FetchScoreSummaryResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049BA RID: 18874 RVA: 0x0014FBF5 File Offset: 0x0014DFF5
		protected override void CallDispose(HandleRef selfPointer)
		{
			LeaderboardManager.LeaderboardManager_FetchScoreSummaryResponse_Dispose(selfPointer);
		}

		// Token: 0x060049BB RID: 18875 RVA: 0x0014FBFD File Offset: 0x0014DFFD
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return LeaderboardManager.LeaderboardManager_FetchScoreSummaryResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x060049BC RID: 18876 RVA: 0x0014FC0A File Offset: 0x0014E00A
		internal NativeScoreSummary GetScoreSummary()
		{
			return NativeScoreSummary.FromPointer(LeaderboardManager.LeaderboardManager_FetchScoreSummaryResponse_GetData(base.SelfPtr()));
		}

		// Token: 0x060049BD RID: 18877 RVA: 0x0014FC1C File Offset: 0x0014E01C
		internal static FetchScoreSummaryResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new FetchScoreSummaryResponse(pointer);
		}
	}
}
