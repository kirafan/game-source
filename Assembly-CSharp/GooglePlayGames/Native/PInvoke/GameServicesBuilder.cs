﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E19 RID: 3609
	internal class GameServicesBuilder : BaseReferenceHolder
	{
		// Token: 0x06004987 RID: 18823 RVA: 0x0014F1C7 File Offset: 0x0014D5C7
		private GameServicesBuilder(IntPtr selfPointer) : base(selfPointer)
		{
			InternalHooks.InternalHooks_ConfigureForUnityPlugin(base.SelfPtr(), "0.9.41");
		}

		// Token: 0x06004988 RID: 18824 RVA: 0x0014F1E0 File Offset: 0x0014D5E0
		internal void SetOnAuthFinishedCallback(GameServicesBuilder.AuthFinishedCallback callback)
		{
			HandleRef self = base.SelfPtr();
			if (GameServicesBuilder.<>f__mg$cache0 == null)
			{
				GameServicesBuilder.<>f__mg$cache0 = new Builder.OnAuthActionFinishedCallback(GameServicesBuilder.InternalAuthFinishedCallback);
			}
			Builder.GameServices_Builder_SetOnAuthActionFinished(self, GameServicesBuilder.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004989 RID: 18825 RVA: 0x0014F210 File Offset: 0x0014D610
		internal void EnableSnapshots()
		{
			Builder.GameServices_Builder_EnableSnapshots(base.SelfPtr());
		}

		// Token: 0x0600498A RID: 18826 RVA: 0x0014F21D File Offset: 0x0014D61D
		internal void AddOauthScope(string scope)
		{
			Builder.GameServices_Builder_AddOauthScope(base.SelfPtr(), scope);
		}

		// Token: 0x0600498B RID: 18827 RVA: 0x0014F22C File Offset: 0x0014D62C
		[MonoPInvokeCallback(typeof(Builder.OnAuthActionFinishedCallback))]
		private static void InternalAuthFinishedCallback(Types.AuthOperation op, CommonErrorStatus.AuthStatus status, IntPtr data)
		{
			GameServicesBuilder.AuthFinishedCallback authFinishedCallback = Callbacks.IntPtrToPermanentCallback<GameServicesBuilder.AuthFinishedCallback>(data);
			if (authFinishedCallback == null)
			{
				return;
			}
			try
			{
				authFinishedCallback(op, status);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalAuthFinishedCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x0600498C RID: 18828 RVA: 0x0014F27C File Offset: 0x0014D67C
		internal void SetOnAuthStartedCallback(GameServicesBuilder.AuthStartedCallback callback)
		{
			HandleRef self = base.SelfPtr();
			if (GameServicesBuilder.<>f__mg$cache1 == null)
			{
				GameServicesBuilder.<>f__mg$cache1 = new Builder.OnAuthActionStartedCallback(GameServicesBuilder.InternalAuthStartedCallback);
			}
			Builder.GameServices_Builder_SetOnAuthActionStarted(self, GameServicesBuilder.<>f__mg$cache1, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x0600498D RID: 18829 RVA: 0x0014F2AC File Offset: 0x0014D6AC
		[MonoPInvokeCallback(typeof(Builder.OnAuthActionStartedCallback))]
		private static void InternalAuthStartedCallback(Types.AuthOperation op, IntPtr data)
		{
			GameServicesBuilder.AuthStartedCallback authStartedCallback = Callbacks.IntPtrToPermanentCallback<GameServicesBuilder.AuthStartedCallback>(data);
			try
			{
				if (authStartedCallback != null)
				{
					authStartedCallback(op);
				}
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalAuthStartedCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x0600498E RID: 18830 RVA: 0x0014F2F8 File Offset: 0x0014D6F8
		internal void SetShowConnectingPopup(bool flag)
		{
			Builder.GameServices_Builder_SetShowConnectingPopup(base.SelfPtr(), flag);
		}

		// Token: 0x0600498F RID: 18831 RVA: 0x0014F306 File Offset: 0x0014D706
		protected override void CallDispose(HandleRef selfPointer)
		{
			Builder.GameServices_Builder_Dispose(selfPointer);
		}

		// Token: 0x06004990 RID: 18832 RVA: 0x0014F310 File Offset: 0x0014D710
		[MonoPInvokeCallback(typeof(Builder.OnTurnBasedMatchEventCallback))]
		private static void InternalOnTurnBasedMatchEventCallback(Types.MultiplayerEvent eventType, string matchId, IntPtr match, IntPtr userData)
		{
			Action<Types.MultiplayerEvent, string, NativeTurnBasedMatch> action = Callbacks.IntPtrToPermanentCallback<Action<Types.MultiplayerEvent, string, NativeTurnBasedMatch>>(userData);
			using (NativeTurnBasedMatch nativeTurnBasedMatch = NativeTurnBasedMatch.FromPointer(match))
			{
				try
				{
					if (action != null)
					{
						action(eventType, matchId, nativeTurnBasedMatch);
					}
				}
				catch (Exception arg)
				{
					Logger.e("Error encountered executing InternalOnTurnBasedMatchEventCallback. Smothering to avoid passing exception into Native: " + arg);
				}
			}
		}

		// Token: 0x06004991 RID: 18833 RVA: 0x0014F384 File Offset: 0x0014D784
		internal void SetOnTurnBasedMatchEventCallback(Action<Types.MultiplayerEvent, string, NativeTurnBasedMatch> callback)
		{
			IntPtr callback_arg = Callbacks.ToIntPtr(callback);
			HandleRef self = base.SelfPtr();
			if (GameServicesBuilder.<>f__mg$cache2 == null)
			{
				GameServicesBuilder.<>f__mg$cache2 = new Builder.OnTurnBasedMatchEventCallback(GameServicesBuilder.InternalOnTurnBasedMatchEventCallback);
			}
			Builder.GameServices_Builder_SetOnTurnBasedMatchEvent(self, GameServicesBuilder.<>f__mg$cache2, callback_arg);
		}

		// Token: 0x06004992 RID: 18834 RVA: 0x0014F3C4 File Offset: 0x0014D7C4
		[MonoPInvokeCallback(typeof(Builder.OnMultiplayerInvitationEventCallback))]
		private static void InternalOnMultiplayerInvitationEventCallback(Types.MultiplayerEvent eventType, string matchId, IntPtr match, IntPtr userData)
		{
			Action<Types.MultiplayerEvent, string, MultiplayerInvitation> action = Callbacks.IntPtrToPermanentCallback<Action<Types.MultiplayerEvent, string, MultiplayerInvitation>>(userData);
			using (MultiplayerInvitation multiplayerInvitation = MultiplayerInvitation.FromPointer(match))
			{
				try
				{
					if (action != null)
					{
						action(eventType, matchId, multiplayerInvitation);
					}
				}
				catch (Exception arg)
				{
					Logger.e("Error encountered executing InternalOnMultiplayerInvitationEventCallback. Smothering to avoid passing exception into Native: " + arg);
				}
			}
		}

		// Token: 0x06004993 RID: 18835 RVA: 0x0014F438 File Offset: 0x0014D838
		internal void SetOnMultiplayerInvitationEventCallback(Action<Types.MultiplayerEvent, string, MultiplayerInvitation> callback)
		{
			IntPtr callback_arg = Callbacks.ToIntPtr(callback);
			HandleRef self = base.SelfPtr();
			if (GameServicesBuilder.<>f__mg$cache3 == null)
			{
				GameServicesBuilder.<>f__mg$cache3 = new Builder.OnMultiplayerInvitationEventCallback(GameServicesBuilder.InternalOnMultiplayerInvitationEventCallback);
			}
			Builder.GameServices_Builder_SetOnMultiplayerInvitationEvent(self, GameServicesBuilder.<>f__mg$cache3, callback_arg);
		}

		// Token: 0x06004994 RID: 18836 RVA: 0x0014F478 File Offset: 0x0014D878
		internal GameServices Build(PlatformConfiguration configRef)
		{
			IntPtr selfPointer = Builder.GameServices_Builder_Create(base.SelfPtr(), HandleRef.ToIntPtr(configRef.AsHandle()));
			if (selfPointer.Equals(IntPtr.Zero))
			{
				throw new InvalidOperationException("There was an error creating a GameServices object. Check for log errors from GamesNativeSDK");
			}
			return new GameServices(selfPointer);
		}

		// Token: 0x06004995 RID: 18837 RVA: 0x0014F4CC File Offset: 0x0014D8CC
		internal static GameServicesBuilder Create()
		{
			IntPtr selfPointer = Builder.GameServices_Builder_Construct();
			return new GameServicesBuilder(selfPointer);
		}

		// Token: 0x04004CF0 RID: 19696
		[CompilerGenerated]
		private static Builder.OnAuthActionFinishedCallback <>f__mg$cache0;

		// Token: 0x04004CF1 RID: 19697
		[CompilerGenerated]
		private static Builder.OnAuthActionStartedCallback <>f__mg$cache1;

		// Token: 0x04004CF2 RID: 19698
		[CompilerGenerated]
		private static Builder.OnTurnBasedMatchEventCallback <>f__mg$cache2;

		// Token: 0x04004CF3 RID: 19699
		[CompilerGenerated]
		private static Builder.OnMultiplayerInvitationEventCallback <>f__mg$cache3;

		// Token: 0x02000E1A RID: 3610
		// (Invoke) Token: 0x06004997 RID: 18839
		internal delegate void AuthFinishedCallback(Types.AuthOperation operation, CommonErrorStatus.AuthStatus status);

		// Token: 0x02000E1B RID: 3611
		// (Invoke) Token: 0x0600499B RID: 18843
		internal delegate void AuthStartedCallback(Types.AuthOperation operation);
	}
}
