﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E28 RID: 3624
	internal class NativeEndpointDiscoveryListenerHelper : BaseReferenceHolder
	{
		// Token: 0x06004A12 RID: 18962 RVA: 0x0015044D File Offset: 0x0014E84D
		internal NativeEndpointDiscoveryListenerHelper() : base(EndpointDiscoveryListenerHelper.EndpointDiscoveryListenerHelper_Construct())
		{
		}

		// Token: 0x06004A13 RID: 18963 RVA: 0x0015045A File Offset: 0x0014E85A
		protected override void CallDispose(HandleRef selfPointer)
		{
			EndpointDiscoveryListenerHelper.EndpointDiscoveryListenerHelper_Dispose(selfPointer);
		}

		// Token: 0x06004A14 RID: 18964 RVA: 0x00150464 File Offset: 0x0014E864
		internal void SetOnEndpointFound(Action<long, NativeEndpointDetails> callback)
		{
			HandleRef self = base.SelfPtr();
			if (NativeEndpointDiscoveryListenerHelper.<>f__mg$cache1 == null)
			{
				NativeEndpointDiscoveryListenerHelper.<>f__mg$cache1 = new EndpointDiscoveryListenerHelper.OnEndpointFoundCallback(NativeEndpointDiscoveryListenerHelper.InternalOnEndpointFoundCallback);
			}
			EndpointDiscoveryListenerHelper.OnEndpointFoundCallback callback2 = NativeEndpointDiscoveryListenerHelper.<>f__mg$cache1;
			if (NativeEndpointDiscoveryListenerHelper.<>f__mg$cache0 == null)
			{
				NativeEndpointDiscoveryListenerHelper.<>f__mg$cache0 = new Func<IntPtr, NativeEndpointDetails>(NativeEndpointDetails.FromPointer);
			}
			EndpointDiscoveryListenerHelper.EndpointDiscoveryListenerHelper_SetOnEndpointFoundCallback(self, callback2, Callbacks.ToIntPtr<long, NativeEndpointDetails>(callback, NativeEndpointDiscoveryListenerHelper.<>f__mg$cache0));
		}

		// Token: 0x06004A15 RID: 18965 RVA: 0x001504BC File Offset: 0x0014E8BC
		[MonoPInvokeCallback(typeof(EndpointDiscoveryListenerHelper.OnEndpointFoundCallback))]
		private static void InternalOnEndpointFoundCallback(long id, IntPtr data, IntPtr userData)
		{
			Callbacks.PerformInternalCallback<long>("NativeEndpointDiscoveryListenerHelper#InternalOnEndpointFoundCallback", Callbacks.Type.Permanent, id, data, userData);
		}

		// Token: 0x06004A16 RID: 18966 RVA: 0x001504CC File Offset: 0x0014E8CC
		internal void SetOnEndpointLostCallback(Action<long, string> callback)
		{
			HandleRef self = base.SelfPtr();
			if (NativeEndpointDiscoveryListenerHelper.<>f__mg$cache2 == null)
			{
				NativeEndpointDiscoveryListenerHelper.<>f__mg$cache2 = new EndpointDiscoveryListenerHelper.OnEndpointLostCallback(NativeEndpointDiscoveryListenerHelper.InternalOnEndpointLostCallback);
			}
			EndpointDiscoveryListenerHelper.EndpointDiscoveryListenerHelper_SetOnEndpointLostCallback(self, NativeEndpointDiscoveryListenerHelper.<>f__mg$cache2, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004A17 RID: 18967 RVA: 0x001504FC File Offset: 0x0014E8FC
		[MonoPInvokeCallback(typeof(EndpointDiscoveryListenerHelper.OnEndpointLostCallback))]
		private static void InternalOnEndpointLostCallback(long id, string lostEndpointId, IntPtr userData)
		{
			Action<long, string> action = Callbacks.IntPtrToPermanentCallback<Action<long, string>>(userData);
			if (action == null)
			{
				return;
			}
			try
			{
				action(id, lostEndpointId);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing NativeEndpointDiscoveryListenerHelper#InternalOnEndpointLostCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x04004CFF RID: 19711
		[CompilerGenerated]
		private static Func<IntPtr, NativeEndpointDetails> <>f__mg$cache0;

		// Token: 0x04004D00 RID: 19712
		[CompilerGenerated]
		private static EndpointDiscoveryListenerHelper.OnEndpointFoundCallback <>f__mg$cache1;

		// Token: 0x04004D01 RID: 19713
		[CompilerGenerated]
		private static EndpointDiscoveryListenerHelper.OnEndpointLostCallback <>f__mg$cache2;
	}
}
