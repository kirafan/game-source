﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E2F RID: 3631
	internal class NativeQuest : BaseReferenceHolder, IQuest
	{
		// Token: 0x06004A4F RID: 19023 RVA: 0x00150A12 File Offset: 0x0014EE12
		internal NativeQuest(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06004A50 RID: 19024 RVA: 0x00150A1B File Offset: 0x0014EE1B
		public string Id
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Quest.Quest_Id(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06004A51 RID: 19025 RVA: 0x00150A2E File Offset: 0x0014EE2E
		public string Name
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Quest.Quest_Name(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06004A52 RID: 19026 RVA: 0x00150A41 File Offset: 0x0014EE41
		public string Description
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Quest.Quest_Description(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x06004A53 RID: 19027 RVA: 0x00150A54 File Offset: 0x0014EE54
		public string BannerUrl
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Quest.Quest_BannerUrl(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x06004A54 RID: 19028 RVA: 0x00150A67 File Offset: 0x0014EE67
		public string IconUrl
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Quest.Quest_IconUrl(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x06004A55 RID: 19029 RVA: 0x00150A7A File Offset: 0x0014EE7A
		public DateTime StartTime
		{
			get
			{
				return PInvokeUtilities.FromMillisSinceUnixEpoch(Quest.Quest_StartTime(base.SelfPtr()));
			}
		}

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x06004A56 RID: 19030 RVA: 0x00150A8C File Offset: 0x0014EE8C
		public DateTime ExpirationTime
		{
			get
			{
				return PInvokeUtilities.FromMillisSinceUnixEpoch(Quest.Quest_ExpirationTime(base.SelfPtr()));
			}
		}

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x06004A57 RID: 19031 RVA: 0x00150AA0 File Offset: 0x0014EEA0
		public DateTime? AcceptedTime
		{
			get
			{
				long num = Quest.Quest_AcceptedTime(base.SelfPtr());
				if (num == 0L)
				{
					return null;
				}
				return new DateTime?(PInvokeUtilities.FromMillisSinceUnixEpoch(num));
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x06004A58 RID: 19032 RVA: 0x00150AD6 File Offset: 0x0014EED6
		[Obsolete("Quests are being removed in 2018.")]
		public IQuestMilestone Milestone
		{
			get
			{
				if (this.mCachedMilestone == null)
				{
					this.mCachedMilestone = NativeQuestMilestone.FromPointer(Quest.Quest_CurrentMilestone(base.SelfPtr()));
				}
				return this.mCachedMilestone;
			}
		}

		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x06004A59 RID: 19033 RVA: 0x00150B08 File Offset: 0x0014EF08
		public QuestState State
		{
			get
			{
				Types.QuestState questState = Quest.Quest_State(base.SelfPtr());
				switch (questState)
				{
				case Types.QuestState.UPCOMING:
					return QuestState.Upcoming;
				case Types.QuestState.OPEN:
					return QuestState.Open;
				case Types.QuestState.ACCEPTED:
					return QuestState.Accepted;
				case Types.QuestState.COMPLETED:
					return QuestState.Completed;
				case Types.QuestState.EXPIRED:
					return QuestState.Expired;
				case Types.QuestState.FAILED:
					return QuestState.Failed;
				default:
					throw new InvalidOperationException("Unknown state: " + questState);
				}
			}
		}

		// Token: 0x06004A5A RID: 19034 RVA: 0x00150B67 File Offset: 0x0014EF67
		internal bool Valid()
		{
			return Quest.Quest_Valid(base.SelfPtr());
		}

		// Token: 0x06004A5B RID: 19035 RVA: 0x00150B74 File Offset: 0x0014EF74
		protected override void CallDispose(HandleRef selfPointer)
		{
			Quest.Quest_Dispose(selfPointer);
		}

		// Token: 0x06004A5C RID: 19036 RVA: 0x00150B7C File Offset: 0x0014EF7C
		public override string ToString()
		{
			if (base.IsDisposed())
			{
				return "[NativeQuest: DELETED]";
			}
			return string.Format("[NativeQuest: Id={0}, Name={1}, Description={2}, BannerUrl={3}, IconUrl={4}, State={5}, StartTime={6}, ExpirationTime={7}, AcceptedTime={8}]", new object[]
			{
				this.Id,
				this.Name,
				this.Description,
				this.BannerUrl,
				this.IconUrl,
				this.State,
				this.StartTime,
				this.ExpirationTime,
				this.AcceptedTime
			});
		}

		// Token: 0x06004A5D RID: 19037 RVA: 0x00150C10 File Offset: 0x0014F010
		internal static NativeQuest FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeQuest(pointer);
		}

		// Token: 0x04004D04 RID: 19716
		[Obsolete("Quests are being removed in 2018.")]
		private volatile NativeQuestMilestone mCachedMilestone;
	}
}
