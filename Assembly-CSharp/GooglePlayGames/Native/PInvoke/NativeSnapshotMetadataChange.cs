﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E38 RID: 3640
	internal class NativeSnapshotMetadataChange : BaseReferenceHolder
	{
		// Token: 0x06004AB0 RID: 19120 RVA: 0x0015134D File Offset: 0x0014F74D
		internal NativeSnapshotMetadataChange(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004AB1 RID: 19121 RVA: 0x00151356 File Offset: 0x0014F756
		protected override void CallDispose(HandleRef selfPointer)
		{
			SnapshotMetadataChange.SnapshotMetadataChange_Dispose(selfPointer);
		}

		// Token: 0x06004AB2 RID: 19122 RVA: 0x0015135E File Offset: 0x0014F75E
		internal static NativeSnapshotMetadataChange FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeSnapshotMetadataChange(pointer);
		}

		// Token: 0x02000E39 RID: 3641
		internal class Builder : BaseReferenceHolder
		{
			// Token: 0x06004AB3 RID: 19123 RVA: 0x00151384 File Offset: 0x0014F784
			internal Builder() : base(SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_Construct())
			{
			}

			// Token: 0x06004AB4 RID: 19124 RVA: 0x00151391 File Offset: 0x0014F791
			protected override void CallDispose(HandleRef selfPointer)
			{
				SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_Dispose(selfPointer);
			}

			// Token: 0x06004AB5 RID: 19125 RVA: 0x00151399 File Offset: 0x0014F799
			internal NativeSnapshotMetadataChange.Builder SetDescription(string description)
			{
				SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_SetDescription(base.SelfPtr(), description);
				return this;
			}

			// Token: 0x06004AB6 RID: 19126 RVA: 0x001513A8 File Offset: 0x0014F7A8
			internal NativeSnapshotMetadataChange.Builder SetPlayedTime(ulong playedTime)
			{
				SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_SetPlayedTime(base.SelfPtr(), playedTime);
				return this;
			}

			// Token: 0x06004AB7 RID: 19127 RVA: 0x001513B7 File Offset: 0x0014F7B7
			internal NativeSnapshotMetadataChange.Builder SetCoverImageFromPngData(byte[] pngData)
			{
				Misc.CheckNotNull<byte[]>(pngData);
				SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_SetCoverImageFromPngData(base.SelfPtr(), pngData, new UIntPtr((ulong)pngData.LongLength));
				return this;
			}

			// Token: 0x06004AB8 RID: 19128 RVA: 0x001513D8 File Offset: 0x0014F7D8
			internal NativeSnapshotMetadataChange Build()
			{
				return NativeSnapshotMetadataChange.FromPointer(SnapshotMetadataChangeBuilder.SnapshotMetadataChange_Builder_Create(base.SelfPtr()));
			}
		}
	}
}
