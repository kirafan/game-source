﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E57 RID: 3671
	internal class RealtimeRoomConfig : BaseReferenceHolder
	{
		// Token: 0x06004BB5 RID: 19381 RVA: 0x0015367D File Offset: 0x00151A7D
		internal RealtimeRoomConfig(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004BB6 RID: 19382 RVA: 0x00153686 File Offset: 0x00151A86
		protected override void CallDispose(HandleRef selfPointer)
		{
			RealTimeRoomConfig.RealTimeRoomConfig_Dispose(selfPointer);
		}

		// Token: 0x06004BB7 RID: 19383 RVA: 0x0015368E File Offset: 0x00151A8E
		internal static RealtimeRoomConfig FromPointer(IntPtr selfPointer)
		{
			if (selfPointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new RealtimeRoomConfig(selfPointer);
		}
	}
}
