﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E10 RID: 3600
	internal abstract class BaseReferenceHolder : IDisposable
	{
		// Token: 0x06004946 RID: 18758 RVA: 0x0014E636 File Offset: 0x0014CA36
		public BaseReferenceHolder(IntPtr pointer)
		{
			this.mSelfPointer = PInvokeUtilities.CheckNonNull(new HandleRef(this, pointer));
		}

		// Token: 0x06004947 RID: 18759 RVA: 0x0014E650 File Offset: 0x0014CA50
		protected bool IsDisposed()
		{
			return PInvokeUtilities.IsNull(this.mSelfPointer);
		}

		// Token: 0x06004948 RID: 18760 RVA: 0x0014E65D File Offset: 0x0014CA5D
		protected HandleRef SelfPtr()
		{
			if (this.IsDisposed())
			{
				throw new InvalidOperationException("Attempted to use object after it was cleaned up");
			}
			return this.mSelfPointer;
		}

		// Token: 0x06004949 RID: 18761
		protected abstract void CallDispose(HandleRef selfPointer);

		// Token: 0x0600494A RID: 18762 RVA: 0x0014E67C File Offset: 0x0014CA7C
		~BaseReferenceHolder()
		{
			this.Dispose(true);
		}

		// Token: 0x0600494B RID: 18763 RVA: 0x0014E6AC File Offset: 0x0014CAAC
		public void Dispose()
		{
			this.Dispose(false);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600494C RID: 18764 RVA: 0x0014E6BC File Offset: 0x0014CABC
		internal IntPtr AsPointer()
		{
			return this.SelfPtr().Handle;
		}

		// Token: 0x0600494D RID: 18765 RVA: 0x0014E6D8 File Offset: 0x0014CAD8
		private void Dispose(bool fromFinalizer)
		{
			if ((fromFinalizer || !BaseReferenceHolder._refs.ContainsKey(this.mSelfPointer)) && !PInvokeUtilities.IsNull(this.mSelfPointer))
			{
				this.CallDispose(this.mSelfPointer);
				this.mSelfPointer = new HandleRef(this, IntPtr.Zero);
			}
		}

		// Token: 0x0600494E RID: 18766 RVA: 0x0014E72D File Offset: 0x0014CB2D
		internal void ReferToMe()
		{
			BaseReferenceHolder._refs[this.SelfPtr()] = this;
		}

		// Token: 0x0600494F RID: 18767 RVA: 0x0014E740 File Offset: 0x0014CB40
		internal void ForgetMe()
		{
			if (BaseReferenceHolder._refs.ContainsKey(this.SelfPtr()))
			{
				BaseReferenceHolder._refs.Remove(this.SelfPtr());
				this.Dispose(false);
			}
		}

		// Token: 0x04004CE3 RID: 19683
		private static Dictionary<HandleRef, BaseReferenceHolder> _refs = new Dictionary<HandleRef, BaseReferenceHolder>();

		// Token: 0x04004CE4 RID: 19684
		private HandleRef mSelfPointer;
	}
}
