﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E4A RID: 3658
	internal class PlayerSelectUIResponse : BaseReferenceHolder, IEnumerable<string>, IEnumerable
	{
		// Token: 0x06004B42 RID: 19266 RVA: 0x0015262F File Offset: 0x00150A2F
		internal PlayerSelectUIResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004B43 RID: 19267 RVA: 0x00152638 File Offset: 0x00150A38
		internal CommonErrorStatus.UIStatus Status()
		{
			return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004B44 RID: 19268 RVA: 0x00152648 File Offset: 0x00150A48
		private string PlayerIdAtIndex(UIntPtr index)
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(this.SelfPtr(), index, out_string, size));
		}

		// Token: 0x06004B45 RID: 19269 RVA: 0x0015267A File Offset: 0x00150A7A
		public IEnumerator<string> GetEnumerator()
		{
			return PInvokeUtilities.ToEnumerator<string>(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(base.SelfPtr()), new Func<UIntPtr, string>(this.PlayerIdAtIndex));
		}

		// Token: 0x06004B46 RID: 19270 RVA: 0x00152698 File Offset: 0x00150A98
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06004B47 RID: 19271 RVA: 0x001526A0 File Offset: 0x00150AA0
		internal uint MinimumAutomatchingPlayers()
		{
			return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(base.SelfPtr());
		}

		// Token: 0x06004B48 RID: 19272 RVA: 0x001526AD File Offset: 0x00150AAD
		internal uint MaximumAutomatchingPlayers()
		{
			return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(base.SelfPtr());
		}

		// Token: 0x06004B49 RID: 19273 RVA: 0x001526BA File Offset: 0x00150ABA
		protected override void CallDispose(HandleRef selfPointer)
		{
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(selfPointer);
		}

		// Token: 0x06004B4A RID: 19274 RVA: 0x001526C2 File Offset: 0x00150AC2
		internal static PlayerSelectUIResponse FromPointer(IntPtr pointer)
		{
			if (PInvokeUtilities.IsNull(pointer))
			{
				return null;
			}
			return new PlayerSelectUIResponse(pointer);
		}
	}
}
