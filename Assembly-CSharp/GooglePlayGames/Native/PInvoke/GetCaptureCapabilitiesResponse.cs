﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E69 RID: 3689
	internal class GetCaptureCapabilitiesResponse : BaseReferenceHolder
	{
		// Token: 0x06004C4E RID: 19534 RVA: 0x00154889 File Offset: 0x00152C89
		internal GetCaptureCapabilitiesResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004C4F RID: 19535 RVA: 0x00154892 File Offset: 0x00152C92
		protected override void CallDispose(HandleRef selfPointer)
		{
			VideoManager.VideoManager_GetCaptureCapabilitiesResponse_Dispose(base.SelfPtr());
		}

		// Token: 0x06004C50 RID: 19536 RVA: 0x0015489F File Offset: 0x00152C9F
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return VideoManager.VideoManager_GetCaptureCapabilitiesResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004C51 RID: 19537 RVA: 0x001548AC File Offset: 0x00152CAC
		internal bool RequestSucceeded()
		{
			return this.GetStatus() > (CommonErrorStatus.ResponseStatus)0;
		}

		// Token: 0x06004C52 RID: 19538 RVA: 0x001548B7 File Offset: 0x00152CB7
		internal NativeVideoCapabilities GetData()
		{
			return NativeVideoCapabilities.FromPointer(VideoManager.VideoManager_GetCaptureCapabilitiesResponse_GetVideoCapabilities(base.SelfPtr()));
		}

		// Token: 0x06004C53 RID: 19539 RVA: 0x001548C9 File Offset: 0x00152CC9
		internal static GetCaptureCapabilitiesResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new GetCaptureCapabilitiesResponse(pointer);
		}
	}
}
