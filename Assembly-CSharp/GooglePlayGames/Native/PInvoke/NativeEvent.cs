﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E29 RID: 3625
	internal class NativeEvent : BaseReferenceHolder, IEvent
	{
		// Token: 0x06004A18 RID: 18968 RVA: 0x0015054C File Offset: 0x0014E94C
		internal NativeEvent(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x06004A19 RID: 18969 RVA: 0x00150555 File Offset: 0x0014E955
		public string Id
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Event.Event_Id(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x06004A1A RID: 18970 RVA: 0x00150568 File Offset: 0x0014E968
		public string Name
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Event.Event_Name(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E2 RID: 1250
		// (get) Token: 0x06004A1B RID: 18971 RVA: 0x0015057B File Offset: 0x0014E97B
		public string Description
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Event.Event_Description(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E3 RID: 1251
		// (get) Token: 0x06004A1C RID: 18972 RVA: 0x0015058E File Offset: 0x0014E98E
		public string ImageUrl
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Event.Event_ImageUrl(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x06004A1D RID: 18973 RVA: 0x001505A1 File Offset: 0x0014E9A1
		public ulong CurrentCount
		{
			get
			{
				return Event.Event_Count(base.SelfPtr());
			}
		}

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x06004A1E RID: 18974 RVA: 0x001505B0 File Offset: 0x0014E9B0
		public EventVisibility Visibility
		{
			get
			{
				Types.EventVisibility eventVisibility = Event.Event_Visibility(base.SelfPtr());
				if (eventVisibility == Types.EventVisibility.HIDDEN)
				{
					return EventVisibility.Hidden;
				}
				if (eventVisibility != Types.EventVisibility.REVEALED)
				{
					throw new InvalidOperationException("Unknown visibility: " + eventVisibility);
				}
				return EventVisibility.Revealed;
			}
		}

		// Token: 0x06004A1F RID: 18975 RVA: 0x001505F5 File Offset: 0x0014E9F5
		protected override void CallDispose(HandleRef selfPointer)
		{
			Event.Event_Dispose(selfPointer);
		}

		// Token: 0x06004A20 RID: 18976 RVA: 0x00150600 File Offset: 0x0014EA00
		public override string ToString()
		{
			if (base.IsDisposed())
			{
				return "[NativeEvent: DELETED]";
			}
			return string.Format("[NativeEvent: Id={0}, Name={1}, Description={2}, ImageUrl={3}, CurrentCount={4}, Visibility={5}]", new object[]
			{
				this.Id,
				this.Name,
				this.Description,
				this.ImageUrl,
				this.CurrentCount,
				this.Visibility
			});
		}
	}
}
