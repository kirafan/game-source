﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E67 RID: 3687
	internal class TurnBasedMatchConfigBuilder : BaseReferenceHolder
	{
		// Token: 0x06004C37 RID: 19511 RVA: 0x0015464A File Offset: 0x00152A4A
		private TurnBasedMatchConfigBuilder(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004C38 RID: 19512 RVA: 0x00154653 File Offset: 0x00152A53
		internal TurnBasedMatchConfigBuilder PopulateFromUIResponse(PlayerSelectUIResponse response)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(base.SelfPtr(), response.AsPointer());
			return this;
		}

		// Token: 0x06004C39 RID: 19513 RVA: 0x00154667 File Offset: 0x00152A67
		internal TurnBasedMatchConfigBuilder SetVariant(uint variant)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_SetVariant(base.SelfPtr(), variant);
			return this;
		}

		// Token: 0x06004C3A RID: 19514 RVA: 0x00154676 File Offset: 0x00152A76
		internal TurnBasedMatchConfigBuilder AddInvitedPlayer(string playerId)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_AddPlayerToInvite(base.SelfPtr(), playerId);
			return this;
		}

		// Token: 0x06004C3B RID: 19515 RVA: 0x00154685 File Offset: 0x00152A85
		internal TurnBasedMatchConfigBuilder SetExclusiveBitMask(ulong bitmask)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_SetExclusiveBitMask(base.SelfPtr(), bitmask);
			return this;
		}

		// Token: 0x06004C3C RID: 19516 RVA: 0x00154694 File Offset: 0x00152A94
		internal TurnBasedMatchConfigBuilder SetMinimumAutomatchingPlayers(uint minimum)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(base.SelfPtr(), minimum);
			return this;
		}

		// Token: 0x06004C3D RID: 19517 RVA: 0x001546A3 File Offset: 0x00152AA3
		internal TurnBasedMatchConfigBuilder SetMaximumAutomatchingPlayers(uint maximum)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(base.SelfPtr(), maximum);
			return this;
		}

		// Token: 0x06004C3E RID: 19518 RVA: 0x001546B2 File Offset: 0x00152AB2
		internal TurnBasedMatchConfig Build()
		{
			return new TurnBasedMatchConfig(TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_Create(base.SelfPtr()));
		}

		// Token: 0x06004C3F RID: 19519 RVA: 0x001546C4 File Offset: 0x00152AC4
		protected override void CallDispose(HandleRef selfPointer)
		{
			TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_Dispose(selfPointer);
		}

		// Token: 0x06004C40 RID: 19520 RVA: 0x001546CC File Offset: 0x00152ACC
		internal static TurnBasedMatchConfigBuilder Create()
		{
			return new TurnBasedMatchConfigBuilder(TurnBasedMatchConfigBuilder.TurnBasedMatchConfig_Builder_Construct());
		}
	}
}
