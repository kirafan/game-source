﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E32 RID: 3634
	internal class NativeScore : BaseReferenceHolder
	{
		// Token: 0x06004A7C RID: 19068 RVA: 0x00150EC9 File Offset: 0x0014F2C9
		internal NativeScore(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004A7D RID: 19069 RVA: 0x00150ED2 File Offset: 0x0014F2D2
		protected override void CallDispose(HandleRef selfPointer)
		{
			Score.Score_Dispose(base.SelfPtr());
		}

		// Token: 0x06004A7E RID: 19070 RVA: 0x00150EDF File Offset: 0x0014F2DF
		internal ulong GetDate()
		{
			return ulong.MaxValue;
		}

		// Token: 0x06004A7F RID: 19071 RVA: 0x00150EE3 File Offset: 0x0014F2E3
		internal string GetMetadata()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Score.Score_Metadata(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A80 RID: 19072 RVA: 0x00150EF6 File Offset: 0x0014F2F6
		internal ulong GetRank()
		{
			return Score.Score_Rank(base.SelfPtr());
		}

		// Token: 0x06004A81 RID: 19073 RVA: 0x00150F03 File Offset: 0x0014F303
		internal ulong GetValue()
		{
			return Score.Score_Value(base.SelfPtr());
		}

		// Token: 0x06004A82 RID: 19074 RVA: 0x00150F10 File Offset: 0x0014F310
		internal PlayGamesScore AsScore(string leaderboardId, string selfPlayerId)
		{
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			ulong num = this.GetDate();
			if (num == 18446744073709551615UL)
			{
				num = 0UL;
			}
			DateTime date = dateTime.AddMilliseconds(num);
			return new PlayGamesScore(date, leaderboardId, this.GetRank(), selfPlayerId, this.GetValue(), this.GetMetadata());
		}

		// Token: 0x06004A83 RID: 19075 RVA: 0x00150F69 File Offset: 0x0014F369
		internal static NativeScore FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeScore(pointer);
		}

		// Token: 0x04004D05 RID: 19717
		private const ulong MinusOne = 18446744073709551615UL;
	}
}
