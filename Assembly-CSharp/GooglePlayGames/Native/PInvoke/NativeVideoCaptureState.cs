﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3D RID: 3645
	internal class NativeVideoCaptureState : BaseReferenceHolder
	{
		// Token: 0x06004AE1 RID: 19169 RVA: 0x00151903 File Offset: 0x0014FD03
		internal NativeVideoCaptureState(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004AE2 RID: 19170 RVA: 0x0015190C File Offset: 0x0014FD0C
		protected override void CallDispose(HandleRef selfPointer)
		{
			VideoCaptureState.VideoCaptureState_Dispose(selfPointer);
		}

		// Token: 0x06004AE3 RID: 19171 RVA: 0x00151914 File Offset: 0x0014FD14
		internal bool IsCapturing()
		{
			return VideoCaptureState.VideoCaptureState_IsCapturing(base.SelfPtr());
		}

		// Token: 0x06004AE4 RID: 19172 RVA: 0x00151921 File Offset: 0x0014FD21
		internal Types.VideoCaptureMode CaptureMode()
		{
			return VideoCaptureState.VideoCaptureState_CaptureMode(base.SelfPtr());
		}

		// Token: 0x06004AE5 RID: 19173 RVA: 0x0015192E File Offset: 0x0014FD2E
		internal Types.VideoQualityLevel QualityLevel()
		{
			return VideoCaptureState.VideoCaptureState_QualityLevel(base.SelfPtr());
		}

		// Token: 0x06004AE6 RID: 19174 RVA: 0x0015193B File Offset: 0x0014FD3B
		internal bool IsOverlayVisible()
		{
			return VideoCaptureState.VideoCaptureState_IsOverlayVisible(base.SelfPtr());
		}

		// Token: 0x06004AE7 RID: 19175 RVA: 0x00151948 File Offset: 0x0014FD48
		internal bool IsPaused()
		{
			return VideoCaptureState.VideoCaptureState_IsPaused(base.SelfPtr());
		}

		// Token: 0x06004AE8 RID: 19176 RVA: 0x00151955 File Offset: 0x0014FD55
		internal static NativeVideoCaptureState FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeVideoCaptureState(pointer);
		}
	}
}
