﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E1C RID: 3612
	internal sealed class IosPlatformConfiguration : PlatformConfiguration
	{
		// Token: 0x0600499E RID: 18846 RVA: 0x0014F4E5 File Offset: 0x0014D8E5
		private IosPlatformConfiguration(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x0600499F RID: 18847 RVA: 0x0014F4EE File Offset: 0x0014D8EE
		internal void SetClientId(string clientId)
		{
			Misc.CheckNotNull<string>(clientId);
			IosPlatformConfiguration.IosPlatformConfiguration_SetClientID(base.SelfPtr(), clientId);
		}

		// Token: 0x060049A0 RID: 18848 RVA: 0x0014F503 File Offset: 0x0014D903
		protected override void CallDispose(HandleRef selfPointer)
		{
			IosPlatformConfiguration.IosPlatformConfiguration_Dispose(selfPointer);
		}

		// Token: 0x060049A1 RID: 18849 RVA: 0x0014F50B File Offset: 0x0014D90B
		internal static IosPlatformConfiguration Create()
		{
			return new IosPlatformConfiguration(IosPlatformConfiguration.IosPlatformConfiguration_Construct());
		}
	}
}
