﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E5F RID: 3679
	internal class StatsManager
	{
		// Token: 0x06004BF3 RID: 19443 RVA: 0x00153D84 File Offset: 0x00152184
		internal StatsManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x06004BF4 RID: 19444 RVA: 0x00153D98 File Offset: 0x00152198
		internal void FetchForPlayer(Action<StatsManager.FetchForPlayerResponse> callback)
		{
			Misc.CheckNotNull<Action<StatsManager.FetchForPlayerResponse>>(callback);
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (StatsManager.<>f__mg$cache1 == null)
			{
				StatsManager.<>f__mg$cache1 = new StatsManager.FetchForPlayerCallback(StatsManager.InternalFetchForPlayerCallback);
			}
			StatsManager.FetchForPlayerCallback callback2 = StatsManager.<>f__mg$cache1;
			if (StatsManager.<>f__mg$cache0 == null)
			{
				StatsManager.<>f__mg$cache0 = new Func<IntPtr, StatsManager.FetchForPlayerResponse>(StatsManager.FetchForPlayerResponse.FromPointer);
			}
			StatsManager.StatsManager_FetchForPlayer(self, data_source, callback2, Callbacks.ToIntPtr<StatsManager.FetchForPlayerResponse>(callback, StatsManager.<>f__mg$cache0));
		}

		// Token: 0x06004BF5 RID: 19445 RVA: 0x00153DFD File Offset: 0x001521FD
		[MonoPInvokeCallback(typeof(StatsManager.FetchForPlayerCallback))]
		private static void InternalFetchForPlayerCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("StatsManager#InternalFetchForPlayerCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x04004D4A RID: 19786
		private readonly GameServices mServices;

		// Token: 0x04004D4B RID: 19787
		[CompilerGenerated]
		private static Func<IntPtr, StatsManager.FetchForPlayerResponse> <>f__mg$cache0;

		// Token: 0x04004D4C RID: 19788
		[CompilerGenerated]
		private static StatsManager.FetchForPlayerCallback <>f__mg$cache1;

		// Token: 0x02000E60 RID: 3680
		internal class FetchForPlayerResponse : BaseReferenceHolder
		{
			// Token: 0x06004BF6 RID: 19446 RVA: 0x00153E0C File Offset: 0x0015220C
			internal FetchForPlayerResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004BF7 RID: 19447 RVA: 0x00153E15 File Offset: 0x00152215
			internal CommonErrorStatus.ResponseStatus Status()
			{
				return StatsManager.StatsManager_FetchForPlayerResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004BF8 RID: 19448 RVA: 0x00153E24 File Offset: 0x00152224
			internal NativePlayerStats PlayerStats()
			{
				IntPtr selfPointer = StatsManager.StatsManager_FetchForPlayerResponse_GetData(base.SelfPtr());
				return new NativePlayerStats(selfPointer);
			}

			// Token: 0x06004BF9 RID: 19449 RVA: 0x00153E43 File Offset: 0x00152243
			protected override void CallDispose(HandleRef selfPointer)
			{
				StatsManager.StatsManager_FetchForPlayerResponse_Dispose(selfPointer);
			}

			// Token: 0x06004BFA RID: 19450 RVA: 0x00153E4B File Offset: 0x0015224B
			internal static StatsManager.FetchForPlayerResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new StatsManager.FetchForPlayerResponse(pointer);
			}
		}
	}
}
