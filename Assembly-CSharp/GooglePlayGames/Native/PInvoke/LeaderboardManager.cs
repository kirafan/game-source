﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E1D RID: 3613
	internal class LeaderboardManager
	{
		// Token: 0x060049A2 RID: 18850 RVA: 0x0014F517 File Offset: 0x0014D917
		internal LeaderboardManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x170004DF RID: 1247
		// (get) Token: 0x060049A3 RID: 18851 RVA: 0x0014F52B File Offset: 0x0014D92B
		internal int LeaderboardMaxResults
		{
			get
			{
				return 25;
			}
		}

		// Token: 0x060049A4 RID: 18852 RVA: 0x0014F530 File Offset: 0x0014D930
		internal void SubmitScore(string leaderboardId, long score, string metadata)
		{
			Misc.CheckNotNull<string>(leaderboardId, "leaderboardId");
			Logger.d(string.Concat(new object[]
			{
				"Native Submitting score: ",
				score,
				" for lb ",
				leaderboardId,
				" with metadata: ",
				metadata
			}));
			LeaderboardManager.LeaderboardManager_SubmitScore(this.mServices.AsHandle(), leaderboardId, (ulong)score, metadata ?? string.Empty);
		}

		// Token: 0x060049A5 RID: 18853 RVA: 0x0014F5A1 File Offset: 0x0014D9A1
		internal void ShowAllUI(Action<CommonErrorStatus.UIStatus> callback)
		{
			Misc.CheckNotNull<Action<CommonErrorStatus.UIStatus>>(callback);
			HandleRef self = this.mServices.AsHandle();
			if (LeaderboardManager.<>f__mg$cache0 == null)
			{
				LeaderboardManager.<>f__mg$cache0 = new LeaderboardManager.ShowAllUICallback(Callbacks.InternalShowUICallback);
			}
			LeaderboardManager.LeaderboardManager_ShowAllUI(self, LeaderboardManager.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x060049A6 RID: 18854 RVA: 0x0014F5DD File Offset: 0x0014D9DD
		internal void ShowUI(string leaderboardId, LeaderboardTimeSpan span, Action<CommonErrorStatus.UIStatus> callback)
		{
			Misc.CheckNotNull<Action<CommonErrorStatus.UIStatus>>(callback);
			HandleRef self = this.mServices.AsHandle();
			if (LeaderboardManager.<>f__mg$cache1 == null)
			{
				LeaderboardManager.<>f__mg$cache1 = new LeaderboardManager.ShowUICallback(Callbacks.InternalShowUICallback);
			}
			LeaderboardManager.LeaderboardManager_ShowUI(self, leaderboardId, (Types.LeaderboardTimeSpan)span, LeaderboardManager.<>f__mg$cache1, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x060049A7 RID: 18855 RVA: 0x0014F61C File Offset: 0x0014DA1C
		public void LoadLeaderboardData(string leaderboardId, LeaderboardStart start, int rowCount, LeaderboardCollection collection, LeaderboardTimeSpan timeSpan, string playerId, Action<LeaderboardScoreData> callback)
		{
			NativeScorePageToken internalObject = new NativeScorePageToken(LeaderboardManager.LeaderboardManager_ScorePageToken(this.mServices.AsHandle(), leaderboardId, (Types.LeaderboardStart)start, (Types.LeaderboardTimeSpan)timeSpan, (Types.LeaderboardCollection)collection));
			ScorePageToken token = new ScorePageToken(internalObject, leaderboardId, collection, timeSpan);
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			if (LeaderboardManager.<>f__mg$cache3 == null)
			{
				LeaderboardManager.<>f__mg$cache3 = new LeaderboardManager.FetchCallback(LeaderboardManager.InternalFetchCallback);
			}
			LeaderboardManager.FetchCallback callback2 = LeaderboardManager.<>f__mg$cache3;
			Action<FetchResponse> callback3 = delegate(FetchResponse rsp)
			{
				this.HandleFetch(token, rsp, playerId, rowCount, callback);
			};
			if (LeaderboardManager.<>f__mg$cache2 == null)
			{
				LeaderboardManager.<>f__mg$cache2 = new Func<IntPtr, FetchResponse>(FetchResponse.FromPointer);
			}
			LeaderboardManager.LeaderboardManager_Fetch(self, data_source, leaderboardId, callback2, Callbacks.ToIntPtr<FetchResponse>(callback3, LeaderboardManager.<>f__mg$cache2));
		}

		// Token: 0x060049A8 RID: 18856 RVA: 0x0014F6D7 File Offset: 0x0014DAD7
		[MonoPInvokeCallback(typeof(LeaderboardManager.FetchCallback))]
		private static void InternalFetchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("LeaderboardManager#InternalFetchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x060049A9 RID: 18857 RVA: 0x0014F6E8 File Offset: 0x0014DAE8
		internal void HandleFetch(ScorePageToken token, FetchResponse response, string selfPlayerId, int maxResults, Action<LeaderboardScoreData> callback)
		{
			LeaderboardScoreData data = new LeaderboardScoreData(token.LeaderboardId, (ResponseStatus)response.GetStatus());
			if (response.GetStatus() != CommonErrorStatus.ResponseStatus.VALID && response.GetStatus() != CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
			{
				Logger.w("Error returned from fetch: " + response.GetStatus());
				callback(data);
				return;
			}
			data.Title = response.Leaderboard().Title();
			data.Id = token.LeaderboardId;
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			string leaderboardId = token.LeaderboardId;
			Types.LeaderboardTimeSpan timeSpan = (Types.LeaderboardTimeSpan)token.TimeSpan;
			Types.LeaderboardCollection collection = (Types.LeaderboardCollection)token.Collection;
			if (LeaderboardManager.<>f__mg$cache5 == null)
			{
				LeaderboardManager.<>f__mg$cache5 = new LeaderboardManager.FetchScoreSummaryCallback(LeaderboardManager.InternalFetchSummaryCallback);
			}
			LeaderboardManager.FetchScoreSummaryCallback callback2 = LeaderboardManager.<>f__mg$cache5;
			Action<FetchScoreSummaryResponse> callback3 = delegate(FetchScoreSummaryResponse rsp)
			{
				this.HandleFetchScoreSummary(data, rsp, selfPlayerId, maxResults, token, callback);
			};
			if (LeaderboardManager.<>f__mg$cache4 == null)
			{
				LeaderboardManager.<>f__mg$cache4 = new Func<IntPtr, FetchScoreSummaryResponse>(FetchScoreSummaryResponse.FromPointer);
			}
			LeaderboardManager.LeaderboardManager_FetchScoreSummary(self, data_source, leaderboardId, timeSpan, collection, callback2, Callbacks.ToIntPtr<FetchScoreSummaryResponse>(callback3, LeaderboardManager.<>f__mg$cache4));
		}

		// Token: 0x060049AA RID: 18858 RVA: 0x0014F829 File Offset: 0x0014DC29
		[MonoPInvokeCallback(typeof(LeaderboardManager.FetchScoreSummaryCallback))]
		private static void InternalFetchSummaryCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("LeaderboardManager#InternalFetchSummaryCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x060049AB RID: 18859 RVA: 0x0014F838 File Offset: 0x0014DC38
		internal void HandleFetchScoreSummary(LeaderboardScoreData data, FetchScoreSummaryResponse response, string selfPlayerId, int maxResults, ScorePageToken token, Action<LeaderboardScoreData> callback)
		{
			if (response.GetStatus() != CommonErrorStatus.ResponseStatus.VALID && response.GetStatus() != CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
			{
				Logger.w("Error returned from fetchScoreSummary: " + response);
				data.Status = (ResponseStatus)response.GetStatus();
				callback(data);
				return;
			}
			NativeScoreSummary scoreSummary = response.GetScoreSummary();
			data.ApproximateCount = scoreSummary.ApproximateResults();
			data.PlayerScore = scoreSummary.LocalUserScore().AsScore(data.Id, selfPlayerId);
			if (maxResults <= 0)
			{
				callback(data);
				return;
			}
			this.LoadScorePage(data, maxResults, token, callback);
		}

		// Token: 0x060049AC RID: 18860 RVA: 0x0014F8CC File Offset: 0x0014DCCC
		public void LoadScorePage(LeaderboardScoreData data, int maxResults, ScorePageToken token, Action<LeaderboardScoreData> callback)
		{
			if (data == null)
			{
				data = new LeaderboardScoreData(token.LeaderboardId);
			}
			NativeScorePageToken nativeScorePageToken = (NativeScorePageToken)token.InternalObject;
			HandleRef self = this.mServices.AsHandle();
			Types.DataSource data_source = Types.DataSource.CACHE_OR_NETWORK;
			IntPtr token2 = nativeScorePageToken.AsPointer();
			if (LeaderboardManager.<>f__mg$cache7 == null)
			{
				LeaderboardManager.<>f__mg$cache7 = new LeaderboardManager.FetchScorePageCallback(LeaderboardManager.InternalFetchScorePage);
			}
			LeaderboardManager.FetchScorePageCallback callback2 = LeaderboardManager.<>f__mg$cache7;
			Action<FetchScorePageResponse> callback3 = delegate(FetchScorePageResponse rsp)
			{
				this.HandleFetchScorePage(data, token, rsp, callback);
			};
			if (LeaderboardManager.<>f__mg$cache6 == null)
			{
				LeaderboardManager.<>f__mg$cache6 = new Func<IntPtr, FetchScorePageResponse>(FetchScorePageResponse.FromPointer);
			}
			LeaderboardManager.LeaderboardManager_FetchScorePage(self, data_source, token2, (uint)maxResults, callback2, Callbacks.ToIntPtr<FetchScorePageResponse>(callback3, LeaderboardManager.<>f__mg$cache6));
		}

		// Token: 0x060049AD RID: 18861 RVA: 0x0014F991 File Offset: 0x0014DD91
		[MonoPInvokeCallback(typeof(LeaderboardManager.FetchScorePageCallback))]
		private static void InternalFetchScorePage(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("LeaderboardManager#InternalFetchScorePage", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x060049AE RID: 18862 RVA: 0x0014F9A0 File Offset: 0x0014DDA0
		internal void HandleFetchScorePage(LeaderboardScoreData data, ScorePageToken token, FetchScorePageResponse rsp, Action<LeaderboardScoreData> callback)
		{
			data.Status = (ResponseStatus)rsp.GetStatus();
			if (rsp.GetStatus() != CommonErrorStatus.ResponseStatus.VALID && rsp.GetStatus() != CommonErrorStatus.ResponseStatus.VALID_BUT_STALE)
			{
				callback(data);
			}
			NativeScorePage scorePage = rsp.GetScorePage();
			if (!scorePage.Valid())
			{
				callback(data);
			}
			if (scorePage.HasNextScorePage())
			{
				data.NextPageToken = new ScorePageToken(scorePage.GetNextScorePageToken(), token.LeaderboardId, token.Collection, token.TimeSpan);
			}
			if (scorePage.HasPrevScorePage())
			{
				data.PrevPageToken = new ScorePageToken(scorePage.GetPreviousScorePageToken(), token.LeaderboardId, token.Collection, token.TimeSpan);
			}
			foreach (NativeScoreEntry nativeScoreEntry in scorePage)
			{
				data.AddScore(nativeScoreEntry.AsScore(data.Id));
			}
			callback(data);
		}

		// Token: 0x04004CF4 RID: 19700
		private readonly GameServices mServices;

		// Token: 0x04004CF5 RID: 19701
		[CompilerGenerated]
		private static LeaderboardManager.ShowAllUICallback <>f__mg$cache0;

		// Token: 0x04004CF6 RID: 19702
		[CompilerGenerated]
		private static LeaderboardManager.ShowUICallback <>f__mg$cache1;

		// Token: 0x04004CF7 RID: 19703
		[CompilerGenerated]
		private static Func<IntPtr, FetchResponse> <>f__mg$cache2;

		// Token: 0x04004CF8 RID: 19704
		[CompilerGenerated]
		private static LeaderboardManager.FetchCallback <>f__mg$cache3;

		// Token: 0x04004CF9 RID: 19705
		[CompilerGenerated]
		private static Func<IntPtr, FetchScoreSummaryResponse> <>f__mg$cache4;

		// Token: 0x04004CFA RID: 19706
		[CompilerGenerated]
		private static LeaderboardManager.FetchScoreSummaryCallback <>f__mg$cache5;

		// Token: 0x04004CFB RID: 19707
		[CompilerGenerated]
		private static Func<IntPtr, FetchScorePageResponse> <>f__mg$cache6;

		// Token: 0x04004CFC RID: 19708
		[CompilerGenerated]
		private static LeaderboardManager.FetchScorePageCallback <>f__mg$cache7;
	}
}
