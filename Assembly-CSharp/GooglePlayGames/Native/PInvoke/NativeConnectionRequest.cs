﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E25 RID: 3621
	internal class NativeConnectionRequest : BaseReferenceHolder
	{
		// Token: 0x060049F1 RID: 18929 RVA: 0x00150160 File Offset: 0x0014E560
		internal NativeConnectionRequest(IntPtr pointer) : base(pointer)
		{
		}

		// Token: 0x060049F2 RID: 18930 RVA: 0x00150169 File Offset: 0x0014E569
		internal string RemoteEndpointId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionRequest_GetRemoteEndpointId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049F3 RID: 18931 RVA: 0x0015017C File Offset: 0x0014E57C
		internal string RemoteDeviceId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionRequest_GetRemoteDeviceId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049F4 RID: 18932 RVA: 0x0015018F File Offset: 0x0014E58F
		internal string RemoteEndpointName()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionRequest_GetRemoteEndpointName(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049F5 RID: 18933 RVA: 0x001501A2 File Offset: 0x0014E5A2
		internal byte[] Payload()
		{
			return PInvokeUtilities.OutParamsToArray<byte>((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.ConnectionRequest_GetPayload(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x060049F6 RID: 18934 RVA: 0x001501B5 File Offset: 0x0014E5B5
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionTypes.ConnectionRequest_Dispose(selfPointer);
		}

		// Token: 0x060049F7 RID: 18935 RVA: 0x001501C0 File Offset: 0x0014E5C0
		internal ConnectionRequest AsRequest()
		{
			ConnectionRequest result = new ConnectionRequest(this.RemoteEndpointId(), this.RemoteDeviceId(), this.RemoteEndpointName(), NearbyConnectionsManager.ServiceId, this.Payload());
			return result;
		}

		// Token: 0x060049F8 RID: 18936 RVA: 0x001501F2 File Offset: 0x0014E5F2
		internal static NativeConnectionRequest FromPointer(IntPtr pointer)
		{
			if (pointer == IntPtr.Zero)
			{
				return null;
			}
			return new NativeConnectionRequest(pointer);
		}
	}
}
