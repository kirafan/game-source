﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E68 RID: 3688
	internal class VideoManager
	{
		// Token: 0x06004C41 RID: 19521 RVA: 0x001546D8 File Offset: 0x00152AD8
		internal VideoManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06004C42 RID: 19522 RVA: 0x001546EC File Offset: 0x00152AEC
		internal int NumCaptureModes
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06004C43 RID: 19523 RVA: 0x001546EF File Offset: 0x00152AEF
		internal int NumQualityLevels
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x06004C44 RID: 19524 RVA: 0x001546F4 File Offset: 0x00152AF4
		internal void GetCaptureCapabilities(Action<GetCaptureCapabilitiesResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (VideoManager.<>f__mg$cache1 == null)
			{
				VideoManager.<>f__mg$cache1 = new VideoManager.CaptureCapabilitiesCallback(VideoManager.InternalCaptureCapabilitiesCallback);
			}
			VideoManager.CaptureCapabilitiesCallback callback2 = VideoManager.<>f__mg$cache1;
			if (VideoManager.<>f__mg$cache0 == null)
			{
				VideoManager.<>f__mg$cache0 = new Func<IntPtr, GetCaptureCapabilitiesResponse>(GetCaptureCapabilitiesResponse.FromPointer);
			}
			VideoManager.VideoManager_GetCaptureCapabilities(self, callback2, Callbacks.ToIntPtr<GetCaptureCapabilitiesResponse>(callback, VideoManager.<>f__mg$cache0));
		}

		// Token: 0x06004C45 RID: 19525 RVA: 0x00154751 File Offset: 0x00152B51
		[MonoPInvokeCallback(typeof(VideoManager.CaptureCapabilitiesCallback))]
		internal static void InternalCaptureCapabilitiesCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("VideoManager#CaptureCapabilitiesCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C46 RID: 19526 RVA: 0x00154760 File Offset: 0x00152B60
		internal void ShowCaptureOverlay()
		{
			VideoManager.VideoManager_ShowCaptureOverlay(this.mServices.AsHandle());
		}

		// Token: 0x06004C47 RID: 19527 RVA: 0x00154774 File Offset: 0x00152B74
		internal void GetCaptureState(Action<GetCaptureStateResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (VideoManager.<>f__mg$cache3 == null)
			{
				VideoManager.<>f__mg$cache3 = new VideoManager.CaptureStateCallback(VideoManager.InternalCaptureStateCallback);
			}
			VideoManager.CaptureStateCallback callback2 = VideoManager.<>f__mg$cache3;
			if (VideoManager.<>f__mg$cache2 == null)
			{
				VideoManager.<>f__mg$cache2 = new Func<IntPtr, GetCaptureStateResponse>(GetCaptureStateResponse.FromPointer);
			}
			VideoManager.VideoManager_GetCaptureState(self, callback2, Callbacks.ToIntPtr<GetCaptureStateResponse>(callback, VideoManager.<>f__mg$cache2));
		}

		// Token: 0x06004C48 RID: 19528 RVA: 0x001547D1 File Offset: 0x00152BD1
		[MonoPInvokeCallback(typeof(VideoManager.CaptureStateCallback))]
		internal static void InternalCaptureStateCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("VideoManager#CaptureStateCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C49 RID: 19529 RVA: 0x001547E0 File Offset: 0x00152BE0
		internal void IsCaptureAvailable(Types.VideoCaptureMode captureMode, Action<IsCaptureAvailableResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (VideoManager.<>f__mg$cache5 == null)
			{
				VideoManager.<>f__mg$cache5 = new VideoManager.IsCaptureAvailableCallback(VideoManager.InternalIsCaptureAvailableCallback);
			}
			VideoManager.IsCaptureAvailableCallback callback2 = VideoManager.<>f__mg$cache5;
			if (VideoManager.<>f__mg$cache4 == null)
			{
				VideoManager.<>f__mg$cache4 = new Func<IntPtr, IsCaptureAvailableResponse>(IsCaptureAvailableResponse.FromPointer);
			}
			VideoManager.VideoManager_IsCaptureAvailable(self, captureMode, callback2, Callbacks.ToIntPtr<IsCaptureAvailableResponse>(callback, VideoManager.<>f__mg$cache4));
		}

		// Token: 0x06004C4A RID: 19530 RVA: 0x0015483E File Offset: 0x00152C3E
		[MonoPInvokeCallback(typeof(VideoManager.IsCaptureAvailableCallback))]
		internal static void InternalIsCaptureAvailableCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("VideoManager#IsCaptureAvailableCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C4B RID: 19531 RVA: 0x0015484D File Offset: 0x00152C4D
		internal bool IsCaptureSupported()
		{
			return VideoManager.VideoManager_IsCaptureSupported(this.mServices.AsHandle());
		}

		// Token: 0x06004C4C RID: 19532 RVA: 0x0015485F File Offset: 0x00152C5F
		internal void RegisterCaptureOverlayStateChangedListener(CaptureOverlayStateListenerHelper helper)
		{
			VideoManager.VideoManager_RegisterCaptureOverlayStateChangedListener(this.mServices.AsHandle(), helper.AsPointer());
		}

		// Token: 0x06004C4D RID: 19533 RVA: 0x00154877 File Offset: 0x00152C77
		internal void UnregisterCaptureOverlayStateChangedListener()
		{
			VideoManager.VideoManager_UnregisterCaptureOverlayStateChangedListener(this.mServices.AsHandle());
		}

		// Token: 0x04004D5F RID: 19807
		private readonly GameServices mServices;

		// Token: 0x04004D60 RID: 19808
		[CompilerGenerated]
		private static Func<IntPtr, GetCaptureCapabilitiesResponse> <>f__mg$cache0;

		// Token: 0x04004D61 RID: 19809
		[CompilerGenerated]
		private static VideoManager.CaptureCapabilitiesCallback <>f__mg$cache1;

		// Token: 0x04004D62 RID: 19810
		[CompilerGenerated]
		private static Func<IntPtr, GetCaptureStateResponse> <>f__mg$cache2;

		// Token: 0x04004D63 RID: 19811
		[CompilerGenerated]
		private static VideoManager.CaptureStateCallback <>f__mg$cache3;

		// Token: 0x04004D64 RID: 19812
		[CompilerGenerated]
		private static Func<IntPtr, IsCaptureAvailableResponse> <>f__mg$cache4;

		// Token: 0x04004D65 RID: 19813
		[CompilerGenerated]
		private static VideoManager.IsCaptureAvailableCallback <>f__mg$cache5;
	}
}
