﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E34 RID: 3636
	internal class NativeScorePage : BaseReferenceHolder
	{
		// Token: 0x06004A8C RID: 19084 RVA: 0x00151060 File Offset: 0x0014F460
		internal NativeScorePage(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004A8D RID: 19085 RVA: 0x00151069 File Offset: 0x0014F469
		protected override void CallDispose(HandleRef selfPointer)
		{
			ScorePage.ScorePage_Dispose(selfPointer);
		}

		// Token: 0x06004A8E RID: 19086 RVA: 0x00151071 File Offset: 0x0014F471
		internal Types.LeaderboardCollection GetCollection()
		{
			return ScorePage.ScorePage_Collection(base.SelfPtr());
		}

		// Token: 0x06004A8F RID: 19087 RVA: 0x0015107E File Offset: 0x0014F47E
		private UIntPtr Length()
		{
			return ScorePage.ScorePage_Entries_Length(base.SelfPtr());
		}

		// Token: 0x06004A90 RID: 19088 RVA: 0x0015108C File Offset: 0x0014F48C
		private NativeScoreEntry GetElement(UIntPtr index)
		{
			if (index.ToUInt64() >= this.Length().ToUInt64())
			{
				throw new ArgumentOutOfRangeException();
			}
			return new NativeScoreEntry(ScorePage.ScorePage_Entries_GetElement(base.SelfPtr(), index));
		}

		// Token: 0x06004A91 RID: 19089 RVA: 0x001510CA File Offset: 0x0014F4CA
		public IEnumerator<NativeScoreEntry> GetEnumerator()
		{
			return PInvokeUtilities.ToEnumerator<NativeScoreEntry>(ScorePage.ScorePage_Entries_Length(base.SelfPtr()), (UIntPtr index) => this.GetElement(index));
		}

		// Token: 0x06004A92 RID: 19090 RVA: 0x001510E8 File Offset: 0x0014F4E8
		internal bool HasNextScorePage()
		{
			return ScorePage.ScorePage_HasNextScorePage(base.SelfPtr());
		}

		// Token: 0x06004A93 RID: 19091 RVA: 0x001510F5 File Offset: 0x0014F4F5
		internal bool HasPrevScorePage()
		{
			return ScorePage.ScorePage_HasPreviousScorePage(base.SelfPtr());
		}

		// Token: 0x06004A94 RID: 19092 RVA: 0x00151102 File Offset: 0x0014F502
		internal NativeScorePageToken GetNextScorePageToken()
		{
			return new NativeScorePageToken(ScorePage.ScorePage_NextScorePageToken(base.SelfPtr()));
		}

		// Token: 0x06004A95 RID: 19093 RVA: 0x00151114 File Offset: 0x0014F514
		internal NativeScorePageToken GetPreviousScorePageToken()
		{
			return new NativeScorePageToken(ScorePage.ScorePage_PreviousScorePageToken(base.SelfPtr()));
		}

		// Token: 0x06004A96 RID: 19094 RVA: 0x00151126 File Offset: 0x0014F526
		internal bool Valid()
		{
			return ScorePage.ScorePage_Valid(base.SelfPtr());
		}

		// Token: 0x06004A97 RID: 19095 RVA: 0x00151133 File Offset: 0x0014F533
		internal Types.LeaderboardTimeSpan GetTimeSpan()
		{
			return ScorePage.ScorePage_TimeSpan(base.SelfPtr());
		}

		// Token: 0x06004A98 RID: 19096 RVA: 0x00151140 File Offset: 0x0014F540
		internal Types.LeaderboardStart GetStart()
		{
			return ScorePage.ScorePage_Start(base.SelfPtr());
		}

		// Token: 0x06004A99 RID: 19097 RVA: 0x0015114D File Offset: 0x0014F54D
		internal string GetLeaderboardId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => ScorePage.ScorePage_LeaderboardId(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A9A RID: 19098 RVA: 0x00151160 File Offset: 0x0014F560
		internal static NativeScorePage FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeScorePage(pointer);
		}
	}
}
