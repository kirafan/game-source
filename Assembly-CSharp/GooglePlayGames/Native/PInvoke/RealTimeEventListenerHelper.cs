﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E51 RID: 3665
	internal class RealTimeEventListenerHelper : BaseReferenceHolder
	{
		// Token: 0x06004B78 RID: 19320 RVA: 0x00152CA5 File Offset: 0x001510A5
		internal RealTimeEventListenerHelper(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004B79 RID: 19321 RVA: 0x00152CAE File Offset: 0x001510AE
		protected override void CallDispose(HandleRef selfPointer)
		{
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_Dispose(selfPointer);
		}

		// Token: 0x06004B7A RID: 19322 RVA: 0x00152CB6 File Offset: 0x001510B6
		internal RealTimeEventListenerHelper SetOnRoomStatusChangedCallback(Action<NativeRealTimeRoom> callback)
		{
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache0 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache0 = new RealTimeEventListenerHelper.OnRoomStatusChangedCallback(RealTimeEventListenerHelper.InternalOnRoomStatusChangedCallback);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache0, RealTimeEventListenerHelper.ToCallbackPointer(callback));
			return this;
		}

		// Token: 0x06004B7B RID: 19323 RVA: 0x00152CE7 File Offset: 0x001510E7
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnRoomStatusChangedCallback))]
		internal static void InternalOnRoomStatusChangedCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealTimeEventListenerHelper#InternalOnRoomStatusChangedCallback", Callbacks.Type.Permanent, response, data);
		}

		// Token: 0x06004B7C RID: 19324 RVA: 0x00152CF6 File Offset: 0x001510F6
		internal RealTimeEventListenerHelper SetOnRoomConnectedSetChangedCallback(Action<NativeRealTimeRoom> callback)
		{
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache1 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache1 = new RealTimeEventListenerHelper.OnRoomConnectedSetChangedCallback(RealTimeEventListenerHelper.InternalOnRoomConnectedSetChangedCallback);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache1, RealTimeEventListenerHelper.ToCallbackPointer(callback));
			return this;
		}

		// Token: 0x06004B7D RID: 19325 RVA: 0x00152D27 File Offset: 0x00151127
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnRoomConnectedSetChangedCallback))]
		internal static void InternalOnRoomConnectedSetChangedCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("RealTimeEventListenerHelper#InternalOnRoomConnectedSetChangedCallback", Callbacks.Type.Permanent, response, data);
		}

		// Token: 0x06004B7E RID: 19326 RVA: 0x00152D36 File Offset: 0x00151136
		internal RealTimeEventListenerHelper SetOnP2PConnectedCallback(Action<NativeRealTimeRoom, MultiplayerParticipant> callback)
		{
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache2 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache2 = new RealTimeEventListenerHelper.OnP2PConnectedCallback(RealTimeEventListenerHelper.InternalOnP2PConnectedCallback);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnP2PConnectedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache2, Callbacks.ToIntPtr(callback));
			return this;
		}

		// Token: 0x06004B7F RID: 19327 RVA: 0x00152D67 File Offset: 0x00151167
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnP2PConnectedCallback))]
		internal static void InternalOnP2PConnectedCallback(IntPtr room, IntPtr participant, IntPtr data)
		{
			RealTimeEventListenerHelper.PerformRoomAndParticipantCallback("InternalOnP2PConnectedCallback", room, participant, data);
		}

		// Token: 0x06004B80 RID: 19328 RVA: 0x00152D76 File Offset: 0x00151176
		internal RealTimeEventListenerHelper SetOnP2PDisconnectedCallback(Action<NativeRealTimeRoom, MultiplayerParticipant> callback)
		{
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache3 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache3 = new RealTimeEventListenerHelper.OnP2PDisconnectedCallback(RealTimeEventListenerHelper.InternalOnP2PDisconnectedCallback);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache3, Callbacks.ToIntPtr(callback));
			return this;
		}

		// Token: 0x06004B81 RID: 19329 RVA: 0x00152DA7 File Offset: 0x001511A7
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnP2PDisconnectedCallback))]
		internal static void InternalOnP2PDisconnectedCallback(IntPtr room, IntPtr participant, IntPtr data)
		{
			RealTimeEventListenerHelper.PerformRoomAndParticipantCallback("InternalOnP2PDisconnectedCallback", room, participant, data);
		}

		// Token: 0x06004B82 RID: 19330 RVA: 0x00152DB6 File Offset: 0x001511B6
		internal RealTimeEventListenerHelper SetOnParticipantStatusChangedCallback(Action<NativeRealTimeRoom, MultiplayerParticipant> callback)
		{
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache4 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache4 = new RealTimeEventListenerHelper.OnParticipantStatusChangedCallback(RealTimeEventListenerHelper.InternalOnParticipantStatusChangedCallback);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache4, Callbacks.ToIntPtr(callback));
			return this;
		}

		// Token: 0x06004B83 RID: 19331 RVA: 0x00152DE7 File Offset: 0x001511E7
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnParticipantStatusChangedCallback))]
		internal static void InternalOnParticipantStatusChangedCallback(IntPtr room, IntPtr participant, IntPtr data)
		{
			RealTimeEventListenerHelper.PerformRoomAndParticipantCallback("InternalOnParticipantStatusChangedCallback", room, participant, data);
		}

		// Token: 0x06004B84 RID: 19332 RVA: 0x00152DF8 File Offset: 0x001511F8
		internal static void PerformRoomAndParticipantCallback(string callbackName, IntPtr room, IntPtr participant, IntPtr data)
		{
			Logger.d("Entering " + callbackName);
			try
			{
				NativeRealTimeRoom arg = NativeRealTimeRoom.FromPointer(room);
				using (MultiplayerParticipant multiplayerParticipant = MultiplayerParticipant.FromPointer(participant))
				{
					Action<NativeRealTimeRoom, MultiplayerParticipant> action = Callbacks.IntPtrToPermanentCallback<Action<NativeRealTimeRoom, MultiplayerParticipant>>(data);
					if (action != null)
					{
						action(arg, multiplayerParticipant);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.e(string.Concat(new object[]
				{
					"Error encountered executing ",
					callbackName,
					". Smothering to avoid passing exception into Native: ",
					ex
				}));
			}
		}

		// Token: 0x06004B85 RID: 19333 RVA: 0x00152E9C File Offset: 0x0015129C
		internal RealTimeEventListenerHelper SetOnDataReceivedCallback(Action<NativeRealTimeRoom, MultiplayerParticipant, byte[], bool> callback)
		{
			IntPtr callback_arg = Callbacks.ToIntPtr(callback);
			Logger.d("OnData Callback has addr: " + callback_arg.ToInt64());
			HandleRef self = base.SelfPtr();
			if (RealTimeEventListenerHelper.<>f__mg$cache5 == null)
			{
				RealTimeEventListenerHelper.<>f__mg$cache5 = new RealTimeEventListenerHelper.OnDataReceivedCallback(RealTimeEventListenerHelper.InternalOnDataReceived);
			}
			RealTimeEventListenerHelper.RealTimeEventListenerHelper_SetOnDataReceivedCallback(self, RealTimeEventListenerHelper.<>f__mg$cache5, callback_arg);
			return this;
		}

		// Token: 0x06004B86 RID: 19334 RVA: 0x00152EF8 File Offset: 0x001512F8
		[MonoPInvokeCallback(typeof(RealTimeEventListenerHelper.OnDataReceivedCallback))]
		internal static void InternalOnDataReceived(IntPtr room, IntPtr participant, IntPtr data, UIntPtr dataLength, bool isReliable, IntPtr userData)
		{
			Logger.d("Entering InternalOnDataReceived: " + userData.ToInt64());
			Action<NativeRealTimeRoom, MultiplayerParticipant, byte[], bool> action = Callbacks.IntPtrToPermanentCallback<Action<NativeRealTimeRoom, MultiplayerParticipant, byte[], bool>>(userData);
			using (NativeRealTimeRoom nativeRealTimeRoom = NativeRealTimeRoom.FromPointer(room))
			{
				using (MultiplayerParticipant multiplayerParticipant = MultiplayerParticipant.FromPointer(participant))
				{
					if (action != null)
					{
						byte[] array = null;
						if (dataLength.ToUInt64() != 0UL)
						{
							array = new byte[dataLength.ToUInt32()];
							Marshal.Copy(data, array, 0, (int)dataLength.ToUInt32());
						}
						try
						{
							action(nativeRealTimeRoom, multiplayerParticipant, array, isReliable);
						}
						catch (Exception arg)
						{
							Logger.e("Error encountered executing InternalOnDataReceived. Smothering to avoid passing exception into Native: " + arg);
						}
					}
				}
			}
		}

		// Token: 0x06004B87 RID: 19335 RVA: 0x00152FE4 File Offset: 0x001513E4
		private static IntPtr ToCallbackPointer(Action<NativeRealTimeRoom> callback)
		{
			Action<IntPtr> callback2 = delegate(IntPtr result)
			{
				NativeRealTimeRoom nativeRealTimeRoom = NativeRealTimeRoom.FromPointer(result);
				if (callback != null)
				{
					callback(nativeRealTimeRoom);
				}
				else if (nativeRealTimeRoom != null)
				{
					nativeRealTimeRoom.Dispose();
				}
			};
			return Callbacks.ToIntPtr(callback2);
		}

		// Token: 0x06004B88 RID: 19336 RVA: 0x00153011 File Offset: 0x00151411
		internal static RealTimeEventListenerHelper Create()
		{
			return new RealTimeEventListenerHelper(RealTimeEventListenerHelper.RealTimeEventListenerHelper_Construct());
		}

		// Token: 0x04004D28 RID: 19752
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnRoomStatusChangedCallback <>f__mg$cache0;

		// Token: 0x04004D29 RID: 19753
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnRoomConnectedSetChangedCallback <>f__mg$cache1;

		// Token: 0x04004D2A RID: 19754
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnP2PConnectedCallback <>f__mg$cache2;

		// Token: 0x04004D2B RID: 19755
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnP2PDisconnectedCallback <>f__mg$cache3;

		// Token: 0x04004D2C RID: 19756
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnParticipantStatusChangedCallback <>f__mg$cache4;

		// Token: 0x04004D2D RID: 19757
		[CompilerGenerated]
		private static RealTimeEventListenerHelper.OnDataReceivedCallback <>f__mg$cache5;
	}
}
