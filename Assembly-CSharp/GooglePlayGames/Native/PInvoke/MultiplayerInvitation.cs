﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E21 RID: 3617
	internal class MultiplayerInvitation : BaseReferenceHolder
	{
		// Token: 0x060049BE RID: 18878 RVA: 0x0014FC42 File Offset: 0x0014E042
		internal MultiplayerInvitation(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049BF RID: 18879 RVA: 0x0014FC4C File Offset: 0x0014E04C
		internal MultiplayerParticipant Inviter()
		{
			MultiplayerParticipant multiplayerParticipant = new MultiplayerParticipant(MultiplayerInvitation.MultiplayerInvitation_InvitingParticipant(base.SelfPtr()));
			if (!multiplayerParticipant.Valid())
			{
				multiplayerParticipant.Dispose();
				return null;
			}
			return multiplayerParticipant;
		}

		// Token: 0x060049C0 RID: 18880 RVA: 0x0014FC7E File Offset: 0x0014E07E
		internal uint Variant()
		{
			return MultiplayerInvitation.MultiplayerInvitation_Variant(base.SelfPtr());
		}

		// Token: 0x060049C1 RID: 18881 RVA: 0x0014FC8B File Offset: 0x0014E08B
		internal Types.MultiplayerInvitationType Type()
		{
			return MultiplayerInvitation.MultiplayerInvitation_Type(base.SelfPtr());
		}

		// Token: 0x060049C2 RID: 18882 RVA: 0x0014FC98 File Offset: 0x0014E098
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => MultiplayerInvitation.MultiplayerInvitation_Id(base.SelfPtr(), out_string, size));
		}

		// Token: 0x060049C3 RID: 18883 RVA: 0x0014FCAB File Offset: 0x0014E0AB
		protected override void CallDispose(HandleRef selfPointer)
		{
			MultiplayerInvitation.MultiplayerInvitation_Dispose(selfPointer);
		}

		// Token: 0x060049C4 RID: 18884 RVA: 0x0014FCB3 File Offset: 0x0014E0B3
		internal uint AutomatchingSlots()
		{
			return MultiplayerInvitation.MultiplayerInvitation_AutomatchingSlotsAvailable(base.SelfPtr());
		}

		// Token: 0x060049C5 RID: 18885 RVA: 0x0014FCC0 File Offset: 0x0014E0C0
		internal uint ParticipantCount()
		{
			return MultiplayerInvitation.MultiplayerInvitation_Participants_Length(base.SelfPtr()).ToUInt32();
		}

		// Token: 0x060049C6 RID: 18886 RVA: 0x0014FCE0 File Offset: 0x0014E0E0
		private static Invitation.InvType ToInvType(Types.MultiplayerInvitationType invitationType)
		{
			if (invitationType == Types.MultiplayerInvitationType.REAL_TIME)
			{
				return Invitation.InvType.RealTime;
			}
			if (invitationType != Types.MultiplayerInvitationType.TURN_BASED)
			{
				Logger.d("Found unknown invitation type: " + invitationType);
				return Invitation.InvType.Unknown;
			}
			return Invitation.InvType.TurnBased;
		}

		// Token: 0x060049C7 RID: 18887 RVA: 0x0014FD10 File Offset: 0x0014E110
		internal Invitation AsInvitation()
		{
			Invitation.InvType invType = MultiplayerInvitation.ToInvType(this.Type());
			string invId = this.Id();
			int variant = (int)this.Variant();
			Participant inviter;
			using (MultiplayerParticipant multiplayerParticipant = this.Inviter())
			{
				inviter = ((multiplayerParticipant != null) ? multiplayerParticipant.AsParticipant() : null);
			}
			return new Invitation(invType, invId, inviter, variant);
		}

		// Token: 0x060049C8 RID: 18888 RVA: 0x0014FD84 File Offset: 0x0014E184
		internal static MultiplayerInvitation FromPointer(IntPtr selfPointer)
		{
			if (PInvokeUtilities.IsNull(selfPointer))
			{
				return null;
			}
			return new MultiplayerInvitation(selfPointer);
		}
	}
}
