﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E27 RID: 3623
	internal class NativeEndpointDetails : BaseReferenceHolder
	{
		// Token: 0x06004A06 RID: 18950 RVA: 0x0015036F File Offset: 0x0014E76F
		internal NativeEndpointDetails(IntPtr pointer) : base(pointer)
		{
		}

		// Token: 0x06004A07 RID: 18951 RVA: 0x00150378 File Offset: 0x0014E778
		internal string EndpointId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.EndpointDetails_GetEndpointId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004A08 RID: 18952 RVA: 0x0015038B File Offset: 0x0014E78B
		internal string DeviceId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.EndpointDetails_GetDeviceId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004A09 RID: 18953 RVA: 0x0015039E File Offset: 0x0014E79E
		internal string Name()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.EndpointDetails_GetName(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004A0A RID: 18954 RVA: 0x001503B1 File Offset: 0x0014E7B1
		internal string ServiceId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.EndpointDetails_GetServiceId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004A0B RID: 18955 RVA: 0x001503C4 File Offset: 0x0014E7C4
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionTypes.EndpointDetails_Dispose(selfPointer);
		}

		// Token: 0x06004A0C RID: 18956 RVA: 0x001503CC File Offset: 0x0014E7CC
		internal EndpointDetails ToDetails()
		{
			return new EndpointDetails(this.EndpointId(), this.DeviceId(), this.Name(), this.ServiceId());
		}

		// Token: 0x06004A0D RID: 18957 RVA: 0x001503EB File Offset: 0x0014E7EB
		internal static NativeEndpointDetails FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeEndpointDetails(pointer);
		}
	}
}
