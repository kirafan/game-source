﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3C RID: 3644
	internal class NativeVideoCapabilities : BaseReferenceHolder
	{
		// Token: 0x06004AD9 RID: 19161 RVA: 0x00151889 File Offset: 0x0014FC89
		internal NativeVideoCapabilities(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004ADA RID: 19162 RVA: 0x00151892 File Offset: 0x0014FC92
		protected override void CallDispose(HandleRef selfPointer)
		{
			VideoCapabilities.VideoCapabilities_Dispose(selfPointer);
		}

		// Token: 0x06004ADB RID: 19163 RVA: 0x0015189A File Offset: 0x0014FC9A
		internal bool IsCameraSupported()
		{
			return VideoCapabilities.VideoCapabilities_IsCameraSupported(base.SelfPtr());
		}

		// Token: 0x06004ADC RID: 19164 RVA: 0x001518A7 File Offset: 0x0014FCA7
		internal bool IsMicSupported()
		{
			return VideoCapabilities.VideoCapabilities_IsMicSupported(base.SelfPtr());
		}

		// Token: 0x06004ADD RID: 19165 RVA: 0x001518B4 File Offset: 0x0014FCB4
		internal bool IsWriteStorageSupported()
		{
			return VideoCapabilities.VideoCapabilities_IsWriteStorageSupported(base.SelfPtr());
		}

		// Token: 0x06004ADE RID: 19166 RVA: 0x001518C1 File Offset: 0x0014FCC1
		internal bool SupportsCaptureMode(Types.VideoCaptureMode captureMode)
		{
			return VideoCapabilities.VideoCapabilities_SupportsCaptureMode(base.SelfPtr(), captureMode);
		}

		// Token: 0x06004ADF RID: 19167 RVA: 0x001518CF File Offset: 0x0014FCCF
		internal bool SupportsQualityLevel(Types.VideoQualityLevel qualityLevel)
		{
			return VideoCapabilities.VideoCapabilities_SupportsQualityLevel(base.SelfPtr(), qualityLevel);
		}

		// Token: 0x06004AE0 RID: 19168 RVA: 0x001518DD File Offset: 0x0014FCDD
		internal static NativeVideoCapabilities FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeVideoCapabilities(pointer);
		}
	}
}
