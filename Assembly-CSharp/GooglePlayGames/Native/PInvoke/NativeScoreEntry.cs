﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E33 RID: 3635
	internal class NativeScoreEntry : BaseReferenceHolder
	{
		// Token: 0x06004A85 RID: 19077 RVA: 0x00150F9E File Offset: 0x0014F39E
		internal NativeScoreEntry(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004A86 RID: 19078 RVA: 0x00150FA7 File Offset: 0x0014F3A7
		protected override void CallDispose(HandleRef selfPointer)
		{
			ScorePage.ScorePage_Entry_Dispose(selfPointer);
		}

		// Token: 0x06004A87 RID: 19079 RVA: 0x00150FAF File Offset: 0x0014F3AF
		internal ulong GetLastModifiedTime()
		{
			return ScorePage.ScorePage_Entry_LastModifiedTime(base.SelfPtr());
		}

		// Token: 0x06004A88 RID: 19080 RVA: 0x00150FBC File Offset: 0x0014F3BC
		internal string GetPlayerId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => ScorePage.ScorePage_Entry_PlayerId(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A89 RID: 19081 RVA: 0x00150FCF File Offset: 0x0014F3CF
		internal NativeScore GetScore()
		{
			return new NativeScore(ScorePage.ScorePage_Entry_Score(base.SelfPtr()));
		}

		// Token: 0x06004A8A RID: 19082 RVA: 0x00150FE4 File Offset: 0x0014F3E4
		internal PlayGamesScore AsScore(string leaderboardId)
		{
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			ulong num = this.GetLastModifiedTime();
			if (num == 18446744073709551615UL)
			{
				num = 0UL;
			}
			DateTime date = dateTime.AddMilliseconds(num);
			return new PlayGamesScore(date, leaderboardId, this.GetScore().GetRank(), this.GetPlayerId(), this.GetScore().GetValue(), this.GetScore().GetMetadata());
		}

		// Token: 0x04004D06 RID: 19718
		private const ulong MinusOne = 18446744073709551615UL;
	}
}
