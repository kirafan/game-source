﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using UnityEngine;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3E RID: 3646
	internal class NearbyConnectionsManager : BaseReferenceHolder
	{
		// Token: 0x06004AE9 RID: 19177 RVA: 0x0015197B File Offset: 0x0014FD7B
		internal NearbyConnectionsManager(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004AEA RID: 19178 RVA: 0x00151984 File Offset: 0x0014FD84
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnections.NearbyConnections_Dispose(selfPointer);
		}

		// Token: 0x06004AEB RID: 19179 RVA: 0x0015198C File Offset: 0x0014FD8C
		internal void SendUnreliable(string remoteEndpointId, byte[] payload)
		{
			NearbyConnections.NearbyConnections_SendUnreliableMessage(base.SelfPtr(), remoteEndpointId, payload, new UIntPtr((ulong)((long)payload.Length)));
		}

		// Token: 0x06004AEC RID: 19180 RVA: 0x001519A4 File Offset: 0x0014FDA4
		internal void SendReliable(string remoteEndpointId, byte[] payload)
		{
			NearbyConnections.NearbyConnections_SendReliableMessage(base.SelfPtr(), remoteEndpointId, payload, new UIntPtr((ulong)((long)payload.Length)));
		}

		// Token: 0x06004AED RID: 19181 RVA: 0x001519BC File Offset: 0x0014FDBC
		internal void StartAdvertising(string name, List<NativeAppIdentifier> appIds, long advertisingDuration, Action<long, NativeStartAdvertisingResult> advertisingCallback, Action<long, NativeConnectionRequest> connectionRequestCallback)
		{
			HandleRef self = base.SelfPtr();
			IntPtr[] app_identifiers = (from id in appIds
			select id.AsPointer()).ToArray<IntPtr>();
			UIntPtr app_identifiers_size = new UIntPtr((ulong)((long)appIds.Count));
			if (NearbyConnectionsManager.<>f__mg$cache2 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache2 = new NearbyConnectionTypes.StartAdvertisingCallback(NearbyConnectionsManager.InternalStartAdvertisingCallback);
			}
			NearbyConnectionTypes.StartAdvertisingCallback start_advertising_callback = NearbyConnectionsManager.<>f__mg$cache2;
			if (NearbyConnectionsManager.<>f__mg$cache0 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache0 = new Func<IntPtr, NativeStartAdvertisingResult>(NativeStartAdvertisingResult.FromPointer);
			}
			IntPtr start_advertising_callback_arg = Callbacks.ToIntPtr<long, NativeStartAdvertisingResult>(advertisingCallback, NearbyConnectionsManager.<>f__mg$cache0);
			if (NearbyConnectionsManager.<>f__mg$cache3 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache3 = new NearbyConnectionTypes.ConnectionRequestCallback(NearbyConnectionsManager.InternalConnectionRequestCallback);
			}
			NearbyConnectionTypes.ConnectionRequestCallback request_callback = NearbyConnectionsManager.<>f__mg$cache3;
			if (NearbyConnectionsManager.<>f__mg$cache1 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache1 = new Func<IntPtr, NativeConnectionRequest>(NativeConnectionRequest.FromPointer);
			}
			NearbyConnections.NearbyConnections_StartAdvertising(self, name, app_identifiers, app_identifiers_size, advertisingDuration, start_advertising_callback, start_advertising_callback_arg, request_callback, Callbacks.ToIntPtr<long, NativeConnectionRequest>(connectionRequestCallback, NearbyConnectionsManager.<>f__mg$cache1));
		}

		// Token: 0x06004AEE RID: 19182 RVA: 0x00151A8C File Offset: 0x0014FE8C
		[MonoPInvokeCallback(typeof(NearbyConnectionTypes.StartAdvertisingCallback))]
		private static void InternalStartAdvertisingCallback(long id, IntPtr result, IntPtr userData)
		{
			Callbacks.PerformInternalCallback<long>("NearbyConnectionsManager#InternalStartAdvertisingCallback", Callbacks.Type.Permanent, id, result, userData);
		}

		// Token: 0x06004AEF RID: 19183 RVA: 0x00151A9C File Offset: 0x0014FE9C
		[MonoPInvokeCallback(typeof(NearbyConnectionTypes.ConnectionRequestCallback))]
		private static void InternalConnectionRequestCallback(long id, IntPtr result, IntPtr userData)
		{
			Callbacks.PerformInternalCallback<long>("NearbyConnectionsManager#InternalConnectionRequestCallback", Callbacks.Type.Permanent, id, result, userData);
		}

		// Token: 0x06004AF0 RID: 19184 RVA: 0x00151AAC File Offset: 0x0014FEAC
		internal void StopAdvertising()
		{
			NearbyConnections.NearbyConnections_StopAdvertising(base.SelfPtr());
		}

		// Token: 0x06004AF1 RID: 19185 RVA: 0x00151ABC File Offset: 0x0014FEBC
		internal void SendConnectionRequest(string name, string remoteEndpointId, byte[] payload, Action<long, NativeConnectionResponse> callback, NativeMessageListenerHelper listener)
		{
			HandleRef self = base.SelfPtr();
			UIntPtr payload_size = new UIntPtr((ulong)((long)payload.Length));
			if (NearbyConnectionsManager.<>f__mg$cache5 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache5 = new NearbyConnectionTypes.ConnectionResponseCallback(NearbyConnectionsManager.InternalConnectResponseCallback);
			}
			NearbyConnectionTypes.ConnectionResponseCallback callback2 = NearbyConnectionsManager.<>f__mg$cache5;
			if (NearbyConnectionsManager.<>f__mg$cache4 == null)
			{
				NearbyConnectionsManager.<>f__mg$cache4 = new Func<IntPtr, NativeConnectionResponse>(NativeConnectionResponse.FromPointer);
			}
			NearbyConnections.NearbyConnections_SendConnectionRequest(self, name, remoteEndpointId, payload, payload_size, callback2, Callbacks.ToIntPtr<long, NativeConnectionResponse>(callback, NearbyConnectionsManager.<>f__mg$cache4), listener.AsPointer());
		}

		// Token: 0x06004AF2 RID: 19186 RVA: 0x00151B28 File Offset: 0x0014FF28
		[MonoPInvokeCallback(typeof(NearbyConnectionTypes.ConnectionResponseCallback))]
		private static void InternalConnectResponseCallback(long localClientId, IntPtr response, IntPtr userData)
		{
			Callbacks.PerformInternalCallback<long>("NearbyConnectionManager#InternalConnectResponseCallback", Callbacks.Type.Temporary, localClientId, response, userData);
		}

		// Token: 0x06004AF3 RID: 19187 RVA: 0x00151B38 File Offset: 0x0014FF38
		internal void AcceptConnectionRequest(string remoteEndpointId, byte[] payload, NativeMessageListenerHelper listener)
		{
			NearbyConnections.NearbyConnections_AcceptConnectionRequest(base.SelfPtr(), remoteEndpointId, payload, new UIntPtr((ulong)((long)payload.Length)), listener.AsPointer());
		}

		// Token: 0x06004AF4 RID: 19188 RVA: 0x00151B56 File Offset: 0x0014FF56
		internal void DisconnectFromEndpoint(string remoteEndpointId)
		{
			NearbyConnections.NearbyConnections_Disconnect(base.SelfPtr(), remoteEndpointId);
		}

		// Token: 0x06004AF5 RID: 19189 RVA: 0x00151B64 File Offset: 0x0014FF64
		internal void StopAllConnections()
		{
			NearbyConnections.NearbyConnections_Stop(base.SelfPtr());
		}

		// Token: 0x06004AF6 RID: 19190 RVA: 0x00151B71 File Offset: 0x0014FF71
		internal void StartDiscovery(string serviceId, long duration, NativeEndpointDiscoveryListenerHelper listener)
		{
			NearbyConnections.NearbyConnections_StartDiscovery(base.SelfPtr(), serviceId, duration, listener.AsPointer());
		}

		// Token: 0x06004AF7 RID: 19191 RVA: 0x00151B86 File Offset: 0x0014FF86
		internal void StopDiscovery(string serviceId)
		{
			NearbyConnections.NearbyConnections_StopDiscovery(base.SelfPtr(), serviceId);
		}

		// Token: 0x06004AF8 RID: 19192 RVA: 0x00151B94 File Offset: 0x0014FF94
		internal void RejectConnectionRequest(string remoteEndpointId)
		{
			NearbyConnections.NearbyConnections_RejectConnectionRequest(base.SelfPtr(), remoteEndpointId);
		}

		// Token: 0x06004AF9 RID: 19193 RVA: 0x00151BA2 File Offset: 0x0014FFA2
		internal void Shutdown()
		{
			NearbyConnections.NearbyConnections_Stop(base.SelfPtr());
		}

		// Token: 0x06004AFA RID: 19194 RVA: 0x00151BAF File Offset: 0x0014FFAF
		internal string LocalEndpointId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnections.NearbyConnections_GetLocalEndpointId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004AFB RID: 19195 RVA: 0x00151BC2 File Offset: 0x0014FFC2
		internal string LocalDeviceId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnections.NearbyConnections_GetLocalDeviceId(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06004AFC RID: 19196 RVA: 0x00151BD8 File Offset: 0x0014FFD8
		public string AppBundleId
		{
			get
			{
				string result;
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
				{
					AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
					result = @static.Call<string>("getPackageName", new object[0]);
				}
				return result;
			}
		}

		// Token: 0x06004AFD RID: 19197 RVA: 0x00151C34 File Offset: 0x00150034
		internal static string ReadServiceId()
		{
			Debug.Log("Initializing ServiceId property!!!!");
			string result;
			using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			{
				using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
				{
					string text = @static.Call<string>("getPackageName", new object[0]);
					AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getPackageManager", new object[0]);
					AndroidJavaObject androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getApplicationInfo", new object[]
					{
						text,
						128
					});
					AndroidJavaObject androidJavaObject3 = androidJavaObject2.Get<AndroidJavaObject>("metaData");
					string text2 = androidJavaObject3.Call<string>("getString", new object[]
					{
						"com.google.android.gms.nearby.connection.SERVICE_ID"
					});
					Debug.Log("SystemId from Manifest: " + text2);
					result = text2;
				}
			}
			return result;
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06004AFE RID: 19198 RVA: 0x00151D28 File Offset: 0x00150128
		public static string ServiceId
		{
			get
			{
				return NearbyConnectionsManager.sServiceId;
			}
		}

		// Token: 0x04004D07 RID: 19719
		private static readonly string sServiceId = NearbyConnectionsManager.ReadServiceId();

		// Token: 0x04004D08 RID: 19720
		[CompilerGenerated]
		private static Func<IntPtr, NativeStartAdvertisingResult> <>f__mg$cache0;

		// Token: 0x04004D09 RID: 19721
		[CompilerGenerated]
		private static Func<IntPtr, NativeConnectionRequest> <>f__mg$cache1;

		// Token: 0x04004D0A RID: 19722
		[CompilerGenerated]
		private static NearbyConnectionTypes.StartAdvertisingCallback <>f__mg$cache2;

		// Token: 0x04004D0B RID: 19723
		[CompilerGenerated]
		private static NearbyConnectionTypes.ConnectionRequestCallback <>f__mg$cache3;

		// Token: 0x04004D0D RID: 19725
		[CompilerGenerated]
		private static Func<IntPtr, NativeConnectionResponse> <>f__mg$cache4;

		// Token: 0x04004D0E RID: 19726
		[CompilerGenerated]
		private static NearbyConnectionTypes.ConnectionResponseCallback <>f__mg$cache5;
	}
}
