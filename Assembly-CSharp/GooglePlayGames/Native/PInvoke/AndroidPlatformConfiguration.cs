﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E0E RID: 3598
	internal sealed class AndroidPlatformConfiguration : PlatformConfiguration
	{
		// Token: 0x0600493B RID: 18747 RVA: 0x0014E8B0 File Offset: 0x0014CCB0
		private AndroidPlatformConfiguration(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x0600493C RID: 18748 RVA: 0x0014E8B9 File Offset: 0x0014CCB9
		internal void SetActivity(IntPtr activity)
		{
			AndroidPlatformConfiguration.AndroidPlatformConfiguration_SetActivity(base.SelfPtr(), activity);
		}

		// Token: 0x0600493D RID: 18749 RVA: 0x0014E8C7 File Offset: 0x0014CCC7
		internal void SetOptionalIntentHandlerForUI(Action<IntPtr> intentHandler)
		{
			Misc.CheckNotNull<Action<IntPtr>>(intentHandler);
			HandleRef self = base.SelfPtr();
			if (AndroidPlatformConfiguration.<>f__mg$cache0 == null)
			{
				AndroidPlatformConfiguration.<>f__mg$cache0 = new AndroidPlatformConfiguration.IntentHandler(AndroidPlatformConfiguration.InternalIntentHandler);
			}
			AndroidPlatformConfiguration.AndroidPlatformConfiguration_SetOptionalIntentHandlerForUI(self, AndroidPlatformConfiguration.<>f__mg$cache0, Callbacks.ToIntPtr(intentHandler));
		}

		// Token: 0x0600493E RID: 18750 RVA: 0x0014E8FE File Offset: 0x0014CCFE
		internal void SetOptionalViewForPopups(IntPtr view)
		{
			AndroidPlatformConfiguration.AndroidPlatformConfiguration_SetOptionalViewForPopups(base.SelfPtr(), view);
		}

		// Token: 0x0600493F RID: 18751 RVA: 0x0014E90C File Offset: 0x0014CD0C
		protected override void CallDispose(HandleRef selfPointer)
		{
			AndroidPlatformConfiguration.AndroidPlatformConfiguration_Dispose(selfPointer);
		}

		// Token: 0x06004940 RID: 18752 RVA: 0x0014E914 File Offset: 0x0014CD14
		[MonoPInvokeCallback(typeof(AndroidPlatformConfiguration.IntentHandlerInternal))]
		private static void InternalIntentHandler(IntPtr intent, IntPtr userData)
		{
			Callbacks.PerformInternalCallback("AndroidPlatformConfiguration#InternalIntentHandler", Callbacks.Type.Permanent, intent, userData);
		}

		// Token: 0x06004941 RID: 18753 RVA: 0x0014E924 File Offset: 0x0014CD24
		internal static AndroidPlatformConfiguration Create()
		{
			IntPtr selfPointer = AndroidPlatformConfiguration.AndroidPlatformConfiguration_Construct();
			return new AndroidPlatformConfiguration(selfPointer);
		}

		// Token: 0x04004CE2 RID: 19682
		[CompilerGenerated]
		private static AndroidPlatformConfiguration.IntentHandler <>f__mg$cache0;

		// Token: 0x02000E0F RID: 3599
		// (Invoke) Token: 0x06004943 RID: 18755
		private delegate void IntentHandlerInternal(IntPtr intent, IntPtr userData);
	}
}
