﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E1E RID: 3614
	internal class FetchScorePageResponse : BaseReferenceHolder
	{
		// Token: 0x060049AF RID: 18863 RVA: 0x0014FB36 File Offset: 0x0014DF36
		internal FetchScorePageResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049B0 RID: 18864 RVA: 0x0014FB3F File Offset: 0x0014DF3F
		protected override void CallDispose(HandleRef selfPointer)
		{
			LeaderboardManager.LeaderboardManager_FetchScorePageResponse_Dispose(base.SelfPtr());
		}

		// Token: 0x060049B1 RID: 18865 RVA: 0x0014FB4C File Offset: 0x0014DF4C
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return LeaderboardManager.LeaderboardManager_FetchScorePageResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x060049B2 RID: 18866 RVA: 0x0014FB59 File Offset: 0x0014DF59
		internal NativeScorePage GetScorePage()
		{
			return NativeScorePage.FromPointer(LeaderboardManager.LeaderboardManager_FetchScorePageResponse_GetData(base.SelfPtr()));
		}

		// Token: 0x060049B3 RID: 18867 RVA: 0x0014FB6B File Offset: 0x0014DF6B
		internal static FetchScorePageResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new FetchScorePageResponse(pointer);
		}
	}
}
