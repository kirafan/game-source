﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E30 RID: 3632
	[Obsolete("Quests are being removed in 2018.")]
	internal class NativeQuestMilestone : BaseReferenceHolder, IQuestMilestone
	{
		// Token: 0x06004A63 RID: 19043 RVA: 0x00150C81 File Offset: 0x0014F081
		internal NativeQuestMilestone(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x06004A64 RID: 19044 RVA: 0x00150C8A File Offset: 0x0014F08A
		public string Id
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => QuestMilestone.QuestMilestone_Id(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x06004A65 RID: 19045 RVA: 0x00150C9D File Offset: 0x0014F09D
		public string EventId
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => QuestMilestone.QuestMilestone_EventId(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06004A66 RID: 19046 RVA: 0x00150CB0 File Offset: 0x0014F0B0
		public string QuestId
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => QuestMilestone.QuestMilestone_QuestId(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x06004A67 RID: 19047 RVA: 0x00150CC3 File Offset: 0x0014F0C3
		public ulong CurrentCount
		{
			get
			{
				return QuestMilestone.QuestMilestone_CurrentCount(base.SelfPtr());
			}
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x06004A68 RID: 19048 RVA: 0x00150CD0 File Offset: 0x0014F0D0
		public ulong TargetCount
		{
			get
			{
				return QuestMilestone.QuestMilestone_TargetCount(base.SelfPtr());
			}
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x06004A69 RID: 19049 RVA: 0x00150CDD File Offset: 0x0014F0DD
		public byte[] CompletionRewardData
		{
			get
			{
				return PInvokeUtilities.OutParamsToArray<byte>((byte[] out_bytes, UIntPtr out_size) => QuestMilestone.QuestMilestone_CompletionRewardData(base.SelfPtr(), out_bytes, out_size));
			}
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x06004A6A RID: 19050 RVA: 0x00150CF0 File Offset: 0x0014F0F0
		public MilestoneState State
		{
			get
			{
				Types.QuestMilestoneState questMilestoneState = QuestMilestone.QuestMilestone_State(base.SelfPtr());
				switch (questMilestoneState)
				{
				case Types.QuestMilestoneState.NOT_STARTED:
					return MilestoneState.NotStarted;
				case Types.QuestMilestoneState.NOT_COMPLETED:
					return MilestoneState.NotCompleted;
				case Types.QuestMilestoneState.COMPLETED_NOT_CLAIMED:
					return MilestoneState.CompletedNotClaimed;
				case Types.QuestMilestoneState.CLAIMED:
					return MilestoneState.Claimed;
				default:
					throw new InvalidOperationException("Unknown state: " + questMilestoneState);
				}
			}
		}

		// Token: 0x06004A6B RID: 19051 RVA: 0x00150D43 File Offset: 0x0014F143
		internal bool Valid()
		{
			return QuestMilestone.QuestMilestone_Valid(base.SelfPtr());
		}

		// Token: 0x06004A6C RID: 19052 RVA: 0x00150D50 File Offset: 0x0014F150
		protected override void CallDispose(HandleRef selfPointer)
		{
			QuestMilestone.QuestMilestone_Dispose(selfPointer);
		}

		// Token: 0x06004A6D RID: 19053 RVA: 0x00150D58 File Offset: 0x0014F158
		public override string ToString()
		{
			return string.Format("[NativeQuestMilestone: Id={0}, EventId={1}, QuestId={2}, CurrentCount={3}, TargetCount={4}, State={5}]", new object[]
			{
				this.Id,
				this.EventId,
				this.QuestId,
				this.CurrentCount,
				this.TargetCount,
				this.State
			});
		}

		// Token: 0x06004A6E RID: 19054 RVA: 0x00150DBA File Offset: 0x0014F1BA
		internal static NativeQuestMilestone FromPointer(IntPtr pointer)
		{
			if (pointer == IntPtr.Zero)
			{
				return null;
			}
			return new NativeQuestMilestone(pointer);
		}
	}
}
