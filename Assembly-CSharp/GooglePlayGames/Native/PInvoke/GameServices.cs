﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E18 RID: 3608
	internal class GameServices : BaseReferenceHolder
	{
		// Token: 0x0600497D RID: 18813 RVA: 0x0014F167 File Offset: 0x0014D567
		internal GameServices(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x0600497E RID: 18814 RVA: 0x0014F170 File Offset: 0x0014D570
		internal bool IsAuthenticated()
		{
			return GameServices.GameServices_IsAuthorized(base.SelfPtr());
		}

		// Token: 0x0600497F RID: 18815 RVA: 0x0014F17D File Offset: 0x0014D57D
		internal void SignOut()
		{
			GameServices.GameServices_SignOut(base.SelfPtr());
		}

		// Token: 0x06004980 RID: 18816 RVA: 0x0014F18A File Offset: 0x0014D58A
		internal void StartAuthorizationUI()
		{
			GameServices.GameServices_StartAuthorizationUI(base.SelfPtr());
		}

		// Token: 0x06004981 RID: 18817 RVA: 0x0014F197 File Offset: 0x0014D597
		public AchievementManager AchievementManager()
		{
			return new AchievementManager(this);
		}

		// Token: 0x06004982 RID: 18818 RVA: 0x0014F19F File Offset: 0x0014D59F
		public LeaderboardManager LeaderboardManager()
		{
			return new LeaderboardManager(this);
		}

		// Token: 0x06004983 RID: 18819 RVA: 0x0014F1A7 File Offset: 0x0014D5A7
		public PlayerManager PlayerManager()
		{
			return new PlayerManager(this);
		}

		// Token: 0x06004984 RID: 18820 RVA: 0x0014F1AF File Offset: 0x0014D5AF
		public StatsManager StatsManager()
		{
			return new StatsManager(this);
		}

		// Token: 0x06004985 RID: 18821 RVA: 0x0014F1B7 File Offset: 0x0014D5B7
		internal HandleRef AsHandle()
		{
			return base.SelfPtr();
		}

		// Token: 0x06004986 RID: 18822 RVA: 0x0014F1BF File Offset: 0x0014D5BF
		protected override void CallDispose(HandleRef selfPointer)
		{
			GameServices.GameServices_Dispose(selfPointer);
		}
	}
}
