﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E15 RID: 3605
	internal class EventManager
	{
		// Token: 0x06004969 RID: 18793 RVA: 0x0014EF44 File Offset: 0x0014D344
		internal EventManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x0600496A RID: 18794 RVA: 0x0014EF58 File Offset: 0x0014D358
		internal void FetchAll(Types.DataSource source, Action<EventManager.FetchAllResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (EventManager.<>f__mg$cache1 == null)
			{
				EventManager.<>f__mg$cache1 = new EventManager.FetchAllCallback(EventManager.InternalFetchAllCallback);
			}
			EventManager.FetchAllCallback callback2 = EventManager.<>f__mg$cache1;
			if (EventManager.<>f__mg$cache0 == null)
			{
				EventManager.<>f__mg$cache0 = new Func<IntPtr, EventManager.FetchAllResponse>(EventManager.FetchAllResponse.FromPointer);
			}
			EventManager.EventManager_FetchAll(self, source, callback2, Callbacks.ToIntPtr<EventManager.FetchAllResponse>(callback, EventManager.<>f__mg$cache0));
		}

		// Token: 0x0600496B RID: 18795 RVA: 0x0014EFB6 File Offset: 0x0014D3B6
		[MonoPInvokeCallback(typeof(EventManager.FetchAllCallback))]
		internal static void InternalFetchAllCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("EventManager#FetchAllCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x0600496C RID: 18796 RVA: 0x0014EFC8 File Offset: 0x0014D3C8
		internal void Fetch(Types.DataSource source, string eventId, Action<EventManager.FetchResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (EventManager.<>f__mg$cache3 == null)
			{
				EventManager.<>f__mg$cache3 = new EventManager.FetchCallback(EventManager.InternalFetchCallback);
			}
			EventManager.FetchCallback callback2 = EventManager.<>f__mg$cache3;
			if (EventManager.<>f__mg$cache2 == null)
			{
				EventManager.<>f__mg$cache2 = new Func<IntPtr, EventManager.FetchResponse>(EventManager.FetchResponse.FromPointer);
			}
			EventManager.EventManager_Fetch(self, source, eventId, callback2, Callbacks.ToIntPtr<EventManager.FetchResponse>(callback, EventManager.<>f__mg$cache2));
		}

		// Token: 0x0600496D RID: 18797 RVA: 0x0014F027 File Offset: 0x0014D427
		[MonoPInvokeCallback(typeof(EventManager.FetchCallback))]
		internal static void InternalFetchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("EventManager#FetchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x0600496E RID: 18798 RVA: 0x0014F036 File Offset: 0x0014D436
		internal void Increment(string eventId, uint steps)
		{
			EventManager.EventManager_Increment(this.mServices.AsHandle(), eventId, steps);
		}

		// Token: 0x04004CEA RID: 19690
		private readonly GameServices mServices;

		// Token: 0x04004CEB RID: 19691
		[CompilerGenerated]
		private static Func<IntPtr, EventManager.FetchAllResponse> <>f__mg$cache0;

		// Token: 0x04004CEC RID: 19692
		[CompilerGenerated]
		private static EventManager.FetchAllCallback <>f__mg$cache1;

		// Token: 0x04004CED RID: 19693
		[CompilerGenerated]
		private static Func<IntPtr, EventManager.FetchResponse> <>f__mg$cache2;

		// Token: 0x04004CEE RID: 19694
		[CompilerGenerated]
		private static EventManager.FetchCallback <>f__mg$cache3;

		// Token: 0x02000E16 RID: 3606
		internal class FetchResponse : BaseReferenceHolder
		{
			// Token: 0x0600496F RID: 18799 RVA: 0x0014F04A File Offset: 0x0014D44A
			internal FetchResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004970 RID: 18800 RVA: 0x0014F053 File Offset: 0x0014D453
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return EventManager.EventManager_FetchResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004971 RID: 18801 RVA: 0x0014F060 File Offset: 0x0014D460
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004972 RID: 18802 RVA: 0x0014F06B File Offset: 0x0014D46B
			internal NativeEvent Data()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				return new NativeEvent(EventManager.EventManager_FetchResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004973 RID: 18803 RVA: 0x0014F08A File Offset: 0x0014D48A
			protected override void CallDispose(HandleRef selfPointer)
			{
				EventManager.EventManager_FetchResponse_Dispose(selfPointer);
			}

			// Token: 0x06004974 RID: 18804 RVA: 0x0014F092 File Offset: 0x0014D492
			internal static EventManager.FetchResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new EventManager.FetchResponse(pointer);
			}
		}

		// Token: 0x02000E17 RID: 3607
		internal class FetchAllResponse : BaseReferenceHolder
		{
			// Token: 0x06004975 RID: 18805 RVA: 0x0014F0B8 File Offset: 0x0014D4B8
			internal FetchAllResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004976 RID: 18806 RVA: 0x0014F0C1 File Offset: 0x0014D4C1
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return EventManager.EventManager_FetchAllResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004977 RID: 18807 RVA: 0x0014F0D0 File Offset: 0x0014D4D0
			internal List<NativeEvent> Data()
			{
				IntPtr[] source = PInvokeUtilities.OutParamsToArray<IntPtr>((IntPtr[] out_arg, UIntPtr out_size) => EventManager.EventManager_FetchAllResponse_GetData(base.SelfPtr(), out_arg, out_size));
				return (from ptr in source
				select new NativeEvent(ptr)).ToList<NativeEvent>();
			}

			// Token: 0x06004978 RID: 18808 RVA: 0x0014F117 File Offset: 0x0014D517
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004979 RID: 18809 RVA: 0x0014F122 File Offset: 0x0014D522
			protected override void CallDispose(HandleRef selfPointer)
			{
				EventManager.EventManager_FetchAllResponse_Dispose(selfPointer);
			}

			// Token: 0x0600497A RID: 18810 RVA: 0x0014F12A File Offset: 0x0014D52A
			internal static EventManager.FetchAllResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new EventManager.FetchAllResponse(pointer);
			}
		}
	}
}
