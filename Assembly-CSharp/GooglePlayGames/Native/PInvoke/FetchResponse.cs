﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E1F RID: 3615
	internal class FetchResponse : BaseReferenceHolder
	{
		// Token: 0x060049B4 RID: 18868 RVA: 0x0014FB91 File Offset: 0x0014DF91
		internal FetchResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049B5 RID: 18869 RVA: 0x0014FB9A File Offset: 0x0014DF9A
		protected override void CallDispose(HandleRef selfPointer)
		{
			LeaderboardManager.LeaderboardManager_FetchResponse_Dispose(base.SelfPtr());
		}

		// Token: 0x060049B6 RID: 18870 RVA: 0x0014FBA7 File Offset: 0x0014DFA7
		internal NativeLeaderboard Leaderboard()
		{
			return NativeLeaderboard.FromPointer(LeaderboardManager.LeaderboardManager_FetchResponse_GetData(base.SelfPtr()));
		}

		// Token: 0x060049B7 RID: 18871 RVA: 0x0014FBB9 File Offset: 0x0014DFB9
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return LeaderboardManager.LeaderboardManager_FetchResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x060049B8 RID: 18872 RVA: 0x0014FBC6 File Offset: 0x0014DFC6
		internal static FetchResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new FetchResponse(pointer);
		}
	}
}
