﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E22 RID: 3618
	internal class MultiplayerParticipant : BaseReferenceHolder
	{
		// Token: 0x060049CA RID: 18890 RVA: 0x0014FDA8 File Offset: 0x0014E1A8
		internal MultiplayerParticipant(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x060049CB RID: 18891 RVA: 0x0014FDB1 File Offset: 0x0014E1B1
		internal Types.ParticipantStatus Status()
		{
			return MultiplayerParticipant.MultiplayerParticipant_Status(base.SelfPtr());
		}

		// Token: 0x060049CC RID: 18892 RVA: 0x0014FDBE File Offset: 0x0014E1BE
		internal bool IsConnectedToRoom()
		{
			return MultiplayerParticipant.MultiplayerParticipant_IsConnectedToRoom(base.SelfPtr()) || this.Status() == Types.ParticipantStatus.JOINED;
		}

		// Token: 0x060049CD RID: 18893 RVA: 0x0014FDDC File Offset: 0x0014E1DC
		internal string DisplayName()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => MultiplayerParticipant.MultiplayerParticipant_DisplayName(base.SelfPtr(), out_string, size));
		}

		// Token: 0x060049CE RID: 18894 RVA: 0x0014FDEF File Offset: 0x0014E1EF
		internal NativePlayer Player()
		{
			if (!MultiplayerParticipant.MultiplayerParticipant_HasPlayer(base.SelfPtr()))
			{
				return null;
			}
			return new NativePlayer(MultiplayerParticipant.MultiplayerParticipant_Player(base.SelfPtr()));
		}

		// Token: 0x060049CF RID: 18895 RVA: 0x0014FE13 File Offset: 0x0014E213
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => MultiplayerParticipant.MultiplayerParticipant_Id(base.SelfPtr(), out_string, size));
		}

		// Token: 0x060049D0 RID: 18896 RVA: 0x0014FE26 File Offset: 0x0014E226
		internal bool Valid()
		{
			return MultiplayerParticipant.MultiplayerParticipant_Valid(base.SelfPtr());
		}

		// Token: 0x060049D1 RID: 18897 RVA: 0x0014FE33 File Offset: 0x0014E233
		protected override void CallDispose(HandleRef selfPointer)
		{
			MultiplayerParticipant.MultiplayerParticipant_Dispose(selfPointer);
		}

		// Token: 0x060049D2 RID: 18898 RVA: 0x0014FE3C File Offset: 0x0014E23C
		internal Participant AsParticipant()
		{
			NativePlayer nativePlayer = this.Player();
			return new Participant(this.DisplayName(), this.Id(), MultiplayerParticipant.StatusConversion[this.Status()], (nativePlayer != null) ? nativePlayer.AsPlayer() : null, this.IsConnectedToRoom());
		}

		// Token: 0x060049D3 RID: 18899 RVA: 0x0014FE89 File Offset: 0x0014E289
		internal static MultiplayerParticipant FromPointer(IntPtr pointer)
		{
			if (PInvokeUtilities.IsNull(pointer))
			{
				return null;
			}
			return new MultiplayerParticipant(pointer);
		}

		// Token: 0x060049D4 RID: 18900 RVA: 0x0014FE9E File Offset: 0x0014E29E
		internal static MultiplayerParticipant AutomatchingSentinel()
		{
			return new MultiplayerParticipant(Sentinels.Sentinels_AutomatchingParticipant());
		}

		// Token: 0x04004CFD RID: 19709
		private static readonly Dictionary<Types.ParticipantStatus, Participant.ParticipantStatus> StatusConversion = new Dictionary<Types.ParticipantStatus, Participant.ParticipantStatus>
		{
			{
				Types.ParticipantStatus.INVITED,
				Participant.ParticipantStatus.Invited
			},
			{
				Types.ParticipantStatus.JOINED,
				Participant.ParticipantStatus.Joined
			},
			{
				Types.ParticipantStatus.DECLINED,
				Participant.ParticipantStatus.Declined
			},
			{
				Types.ParticipantStatus.LEFT,
				Participant.ParticipantStatus.Left
			},
			{
				Types.ParticipantStatus.NOT_INVITED_YET,
				Participant.ParticipantStatus.NotInvitedYet
			},
			{
				Types.ParticipantStatus.FINISHED,
				Participant.ParticipantStatus.Finished
			},
			{
				Types.ParticipantStatus.UNRESPONSIVE,
				Participant.ParticipantStatus.Unresponsive
			}
		};
	}
}
