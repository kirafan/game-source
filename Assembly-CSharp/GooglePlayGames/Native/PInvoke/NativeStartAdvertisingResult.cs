﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3A RID: 3642
	internal class NativeStartAdvertisingResult : BaseReferenceHolder
	{
		// Token: 0x06004AB9 RID: 19129 RVA: 0x001513EA File Offset: 0x0014F7EA
		internal NativeStartAdvertisingResult(IntPtr pointer) : base(pointer)
		{
		}

		// Token: 0x06004ABA RID: 19130 RVA: 0x001513F3 File Offset: 0x0014F7F3
		internal int GetStatus()
		{
			return NearbyConnectionTypes.StartAdvertisingResult_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004ABB RID: 19131 RVA: 0x00151400 File Offset: 0x0014F800
		internal string LocalEndpointName()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_arg, UIntPtr out_size) => NearbyConnectionTypes.StartAdvertisingResult_GetLocalEndpointName(base.SelfPtr(), out_arg, out_size));
		}

		// Token: 0x06004ABC RID: 19132 RVA: 0x00151413 File Offset: 0x0014F813
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionTypes.StartAdvertisingResult_Dispose(selfPointer);
		}

		// Token: 0x06004ABD RID: 19133 RVA: 0x0015141B File Offset: 0x0014F81B
		internal AdvertisingResult AsResult()
		{
			return new AdvertisingResult((ResponseStatus)Enum.ToObject(typeof(ResponseStatus), this.GetStatus()), this.LocalEndpointName());
		}

		// Token: 0x06004ABE RID: 19134 RVA: 0x00151442 File Offset: 0x0014F842
		internal static NativeStartAdvertisingResult FromPointer(IntPtr pointer)
		{
			if (pointer == IntPtr.Zero)
			{
				return null;
			}
			return new NativeStartAdvertisingResult(pointer);
		}
	}
}
