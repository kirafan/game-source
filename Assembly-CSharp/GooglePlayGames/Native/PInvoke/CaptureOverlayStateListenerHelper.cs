﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E14 RID: 3604
	internal class CaptureOverlayStateListenerHelper : BaseReferenceHolder
	{
		// Token: 0x06004964 RID: 18788 RVA: 0x0014EEAB File Offset: 0x0014D2AB
		internal CaptureOverlayStateListenerHelper(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004965 RID: 18789 RVA: 0x0014EEB4 File Offset: 0x0014D2B4
		protected override void CallDispose(HandleRef selfPointer)
		{
			CaptureOverlayStateListenerHelper.CaptureOverlayStateListenerHelper_Dispose(selfPointer);
		}

		// Token: 0x06004966 RID: 18790 RVA: 0x0014EEBC File Offset: 0x0014D2BC
		internal CaptureOverlayStateListenerHelper SetOnCaptureOverlayStateChangedCallback(Action<Types.VideoCaptureOverlayState> callback)
		{
			HandleRef self = base.SelfPtr();
			if (CaptureOverlayStateListenerHelper.<>f__mg$cache0 == null)
			{
				CaptureOverlayStateListenerHelper.<>f__mg$cache0 = new CaptureOverlayStateListenerHelper.OnCaptureOverlayStateChangedCallback(CaptureOverlayStateListenerHelper.InternalOnCaptureOverlayStateChangedCallback);
			}
			CaptureOverlayStateListenerHelper.CaptureOverlayStateListenerHelper_SetOnCaptureOverlayStateChangedCallback(self, CaptureOverlayStateListenerHelper.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
			return this;
		}

		// Token: 0x06004967 RID: 18791 RVA: 0x0014EEF0 File Offset: 0x0014D2F0
		[MonoPInvokeCallback(typeof(CaptureOverlayStateListenerHelper.OnCaptureOverlayStateChangedCallback))]
		internal static void InternalOnCaptureOverlayStateChangedCallback(Types.VideoCaptureOverlayState response, IntPtr data)
		{
			Action<Types.VideoCaptureOverlayState> action = Callbacks.IntPtrToPermanentCallback<Action<Types.VideoCaptureOverlayState>>(data);
			try
			{
				action(response);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalOnCaptureOverlayStateChangedCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004968 RID: 18792 RVA: 0x0014EF38 File Offset: 0x0014D338
		internal static CaptureOverlayStateListenerHelper Create()
		{
			return new CaptureOverlayStateListenerHelper(CaptureOverlayStateListenerHelper.CaptureOverlayStateListenerHelper_Construct());
		}

		// Token: 0x04004CE9 RID: 19689
		[CompilerGenerated]
		private static CaptureOverlayStateListenerHelper.OnCaptureOverlayStateChangedCallback <>f__mg$cache0;
	}
}
