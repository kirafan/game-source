﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E2D RID: 3629
	internal class NativePlayer : BaseReferenceHolder
	{
		// Token: 0x06004A34 RID: 18996 RVA: 0x00150820 File Offset: 0x0014EC20
		internal NativePlayer(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004A35 RID: 18997 RVA: 0x00150829 File Offset: 0x0014EC29
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Player.Player_Id(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A36 RID: 18998 RVA: 0x0015083C File Offset: 0x0014EC3C
		internal string Name()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Player.Player_Name(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A37 RID: 18999 RVA: 0x0015084F File Offset: 0x0014EC4F
		internal string AvatarURL()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => GooglePlayGames.Native.Cwrapper.Player.Player_AvatarUrl(base.SelfPtr(), Types.ImageResolution.ICON, out_string, out_size));
		}

		// Token: 0x06004A38 RID: 19000 RVA: 0x00150862 File Offset: 0x0014EC62
		protected override void CallDispose(HandleRef selfPointer)
		{
			GooglePlayGames.Native.Cwrapper.Player.Player_Dispose(selfPointer);
		}

		// Token: 0x06004A39 RID: 19001 RVA: 0x0015086A File Offset: 0x0014EC6A
		internal GooglePlayGames.BasicApi.Multiplayer.Player AsPlayer()
		{
			return new GooglePlayGames.BasicApi.Multiplayer.Player(this.Name(), this.Id(), this.AvatarURL());
		}
	}
}
