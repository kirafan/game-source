﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E37 RID: 3639
	internal class NativeSnapshotMetadata : BaseReferenceHolder, ISavedGameMetadata
	{
		// Token: 0x06004AA4 RID: 19108 RVA: 0x00151205 File Offset: 0x0014F605
		internal NativeSnapshotMetadata(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x06004AA5 RID: 19109 RVA: 0x0015120E File Offset: 0x0014F60E
		public bool IsOpen
		{
			get
			{
				return SnapshotMetadata.SnapshotMetadata_IsOpen(base.SelfPtr());
			}
		}

		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x06004AA6 RID: 19110 RVA: 0x0015121B File Offset: 0x0014F61B
		public string Filename
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => SnapshotMetadata.SnapshotMetadata_FileName(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x06004AA7 RID: 19111 RVA: 0x0015122E File Offset: 0x0014F62E
		public string Description
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => SnapshotMetadata.SnapshotMetadata_Description(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x06004AA8 RID: 19112 RVA: 0x00151241 File Offset: 0x0014F641
		public string CoverImageURL
		{
			get
			{
				return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => SnapshotMetadata.SnapshotMetadata_CoverImageURL(base.SelfPtr(), out_string, out_size));
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06004AA9 RID: 19113 RVA: 0x00151254 File Offset: 0x0014F654
		public TimeSpan TotalTimePlayed
		{
			get
			{
				long num = SnapshotMetadata.SnapshotMetadata_PlayedTime(base.SelfPtr());
				if (num < 0L)
				{
					return TimeSpan.FromMilliseconds(0.0);
				}
				return TimeSpan.FromMilliseconds((double)num);
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06004AAA RID: 19114 RVA: 0x0015128B File Offset: 0x0014F68B
		public DateTime LastModifiedTimestamp
		{
			get
			{
				return PInvokeUtilities.FromMillisSinceUnixEpoch(SnapshotMetadata.SnapshotMetadata_LastModifiedTime(base.SelfPtr()));
			}
		}

		// Token: 0x06004AAB RID: 19115 RVA: 0x001512A0 File Offset: 0x0014F6A0
		public override string ToString()
		{
			if (base.IsDisposed())
			{
				return "[NativeSnapshotMetadata: DELETED]";
			}
			return string.Format("[NativeSnapshotMetadata: IsOpen={0}, Filename={1}, Description={2}, CoverImageUrl={3}, TotalTimePlayed={4}, LastModifiedTimestamp={5}]", new object[]
			{
				this.IsOpen,
				this.Filename,
				this.Description,
				this.CoverImageURL,
				this.TotalTimePlayed,
				this.LastModifiedTimestamp
			});
		}

		// Token: 0x06004AAC RID: 19116 RVA: 0x00151313 File Offset: 0x0014F713
		protected override void CallDispose(HandleRef selfPointer)
		{
			SnapshotMetadata.SnapshotMetadata_Dispose(base.SelfPtr());
		}
	}
}
