﻿using System;
using System.Runtime.InteropServices;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E44 RID: 3652
	internal abstract class PlatformConfiguration : BaseReferenceHolder
	{
		// Token: 0x06004B23 RID: 19235 RVA: 0x0014E89F File Offset: 0x0014CC9F
		protected PlatformConfiguration(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004B24 RID: 19236 RVA: 0x0014E8A8 File Offset: 0x0014CCA8
		internal HandleRef AsHandle()
		{
			return base.SelfPtr();
		}
	}
}
