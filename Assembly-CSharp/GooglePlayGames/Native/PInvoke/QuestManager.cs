﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E4B RID: 3659
	[Obsolete("Quests are being removed in 2018.")]
	internal class QuestManager
	{
		// Token: 0x06004B4B RID: 19275 RVA: 0x001526F9 File Offset: 0x00150AF9
		[Obsolete("Quests are being removed in 2018.")]
		internal QuestManager(GameServices services)
		{
			this.mServices = Misc.CheckNotNull<GameServices>(services);
		}

		// Token: 0x06004B4C RID: 19276 RVA: 0x00152710 File Offset: 0x00150B10
		internal void Fetch(Types.DataSource source, string questId, Action<QuestManager.FetchResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (QuestManager.<>f__mg$cache1 == null)
			{
				QuestManager.<>f__mg$cache1 = new QuestManager.FetchCallback(QuestManager.InternalFetchCallback);
			}
			QuestManager.FetchCallback callback2 = QuestManager.<>f__mg$cache1;
			if (QuestManager.<>f__mg$cache0 == null)
			{
				QuestManager.<>f__mg$cache0 = new Func<IntPtr, QuestManager.FetchResponse>(QuestManager.FetchResponse.FromPointer);
			}
			QuestManager.QuestManager_Fetch(self, source, questId, callback2, Callbacks.ToIntPtr<QuestManager.FetchResponse>(callback, QuestManager.<>f__mg$cache0));
		}

		// Token: 0x06004B4D RID: 19277 RVA: 0x0015276F File Offset: 0x00150B6F
		[MonoPInvokeCallback(typeof(QuestManager.FetchCallback))]
		internal static void InternalFetchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("QuestManager#FetchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B4E RID: 19278 RVA: 0x00152780 File Offset: 0x00150B80
		internal void FetchList(Types.DataSource source, int fetchFlags, Action<QuestManager.FetchListResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (QuestManager.<>f__mg$cache3 == null)
			{
				QuestManager.<>f__mg$cache3 = new QuestManager.FetchListCallback(QuestManager.InternalFetchListCallback);
			}
			QuestManager.FetchListCallback callback2 = QuestManager.<>f__mg$cache3;
			if (QuestManager.<>f__mg$cache2 == null)
			{
				QuestManager.<>f__mg$cache2 = new Func<IntPtr, QuestManager.FetchListResponse>(QuestManager.FetchListResponse.FromPointer);
			}
			QuestManager.QuestManager_FetchList(self, source, fetchFlags, callback2, Callbacks.ToIntPtr<QuestManager.FetchListResponse>(callback, QuestManager.<>f__mg$cache2));
		}

		// Token: 0x06004B4F RID: 19279 RVA: 0x001527DF File Offset: 0x00150BDF
		[MonoPInvokeCallback(typeof(QuestManager.FetchListCallback))]
		internal static void InternalFetchListCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("QuestManager#FetchListCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B50 RID: 19280 RVA: 0x001527F0 File Offset: 0x00150BF0
		internal void ShowAllQuestUI(Action<QuestManager.QuestUIResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			if (QuestManager.<>f__mg$cache5 == null)
			{
				QuestManager.<>f__mg$cache5 = new QuestManager.QuestUICallback(QuestManager.InternalQuestUICallback);
			}
			QuestManager.QuestUICallback callback2 = QuestManager.<>f__mg$cache5;
			if (QuestManager.<>f__mg$cache4 == null)
			{
				QuestManager.<>f__mg$cache4 = new Func<IntPtr, QuestManager.QuestUIResponse>(QuestManager.QuestUIResponse.FromPointer);
			}
			QuestManager.QuestManager_ShowAllUI(self, callback2, Callbacks.ToIntPtr<QuestManager.QuestUIResponse>(callback, QuestManager.<>f__mg$cache4));
		}

		// Token: 0x06004B51 RID: 19281 RVA: 0x00152850 File Offset: 0x00150C50
		internal void ShowQuestUI(NativeQuest quest, Action<QuestManager.QuestUIResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			IntPtr quest2 = quest.AsPointer();
			if (QuestManager.<>f__mg$cache7 == null)
			{
				QuestManager.<>f__mg$cache7 = new QuestManager.QuestUICallback(QuestManager.InternalQuestUICallback);
			}
			QuestManager.QuestUICallback callback2 = QuestManager.<>f__mg$cache7;
			if (QuestManager.<>f__mg$cache6 == null)
			{
				QuestManager.<>f__mg$cache6 = new Func<IntPtr, QuestManager.QuestUIResponse>(QuestManager.QuestUIResponse.FromPointer);
			}
			QuestManager.QuestManager_ShowUI(self, quest2, callback2, Callbacks.ToIntPtr<QuestManager.QuestUIResponse>(callback, QuestManager.<>f__mg$cache6));
		}

		// Token: 0x06004B52 RID: 19282 RVA: 0x001528B3 File Offset: 0x00150CB3
		[MonoPInvokeCallback(typeof(QuestManager.QuestUICallback))]
		internal static void InternalQuestUICallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("QuestManager#QuestUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B53 RID: 19283 RVA: 0x001528C4 File Offset: 0x00150CC4
		internal void Accept(NativeQuest quest, Action<QuestManager.AcceptResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			IntPtr quest2 = quest.AsPointer();
			if (QuestManager.<>f__mg$cache9 == null)
			{
				QuestManager.<>f__mg$cache9 = new QuestManager.AcceptCallback(QuestManager.InternalAcceptCallback);
			}
			QuestManager.AcceptCallback callback2 = QuestManager.<>f__mg$cache9;
			if (QuestManager.<>f__mg$cache8 == null)
			{
				QuestManager.<>f__mg$cache8 = new Func<IntPtr, QuestManager.AcceptResponse>(QuestManager.AcceptResponse.FromPointer);
			}
			QuestManager.QuestManager_Accept(self, quest2, callback2, Callbacks.ToIntPtr<QuestManager.AcceptResponse>(callback, QuestManager.<>f__mg$cache8));
		}

		// Token: 0x06004B54 RID: 19284 RVA: 0x00152927 File Offset: 0x00150D27
		[MonoPInvokeCallback(typeof(QuestManager.AcceptCallback))]
		internal static void InternalAcceptCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("QuestManager#AcceptCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004B55 RID: 19285 RVA: 0x00152938 File Offset: 0x00150D38
		[Obsolete("Quests are being removed in 2018.")]
		internal void ClaimMilestone(NativeQuestMilestone milestone, Action<QuestManager.ClaimMilestoneResponse> callback)
		{
			HandleRef self = this.mServices.AsHandle();
			IntPtr milestone2 = milestone.AsPointer();
			if (QuestManager.<>f__mg$cacheB == null)
			{
				QuestManager.<>f__mg$cacheB = new QuestManager.ClaimMilestoneCallback(QuestManager.InternalClaimMilestoneCallback);
			}
			QuestManager.ClaimMilestoneCallback callback2 = QuestManager.<>f__mg$cacheB;
			if (QuestManager.<>f__mg$cacheA == null)
			{
				QuestManager.<>f__mg$cacheA = new Func<IntPtr, QuestManager.ClaimMilestoneResponse>(QuestManager.ClaimMilestoneResponse.FromPointer);
			}
			QuestManager.QuestManager_ClaimMilestone(self, milestone2, callback2, Callbacks.ToIntPtr<QuestManager.ClaimMilestoneResponse>(callback, QuestManager.<>f__mg$cacheA));
		}

		// Token: 0x06004B56 RID: 19286 RVA: 0x0015299B File Offset: 0x00150D9B
		[MonoPInvokeCallback(typeof(QuestManager.ClaimMilestoneCallback))]
		internal static void InternalClaimMilestoneCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("QuestManager#ClaimMilestoneCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x04004D1B RID: 19739
		private readonly GameServices mServices;

		// Token: 0x04004D1C RID: 19740
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.FetchResponse> <>f__mg$cache0;

		// Token: 0x04004D1D RID: 19741
		[CompilerGenerated]
		private static QuestManager.FetchCallback <>f__mg$cache1;

		// Token: 0x04004D1E RID: 19742
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.FetchListResponse> <>f__mg$cache2;

		// Token: 0x04004D1F RID: 19743
		[CompilerGenerated]
		private static QuestManager.FetchListCallback <>f__mg$cache3;

		// Token: 0x04004D20 RID: 19744
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.QuestUIResponse> <>f__mg$cache4;

		// Token: 0x04004D21 RID: 19745
		[CompilerGenerated]
		private static QuestManager.QuestUICallback <>f__mg$cache5;

		// Token: 0x04004D22 RID: 19746
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.QuestUIResponse> <>f__mg$cache6;

		// Token: 0x04004D23 RID: 19747
		[CompilerGenerated]
		private static QuestManager.QuestUICallback <>f__mg$cache7;

		// Token: 0x04004D24 RID: 19748
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.AcceptResponse> <>f__mg$cache8;

		// Token: 0x04004D25 RID: 19749
		[CompilerGenerated]
		private static QuestManager.AcceptCallback <>f__mg$cache9;

		// Token: 0x04004D26 RID: 19750
		[CompilerGenerated]
		private static Func<IntPtr, QuestManager.ClaimMilestoneResponse> <>f__mg$cacheA;

		// Token: 0x04004D27 RID: 19751
		[CompilerGenerated]
		private static QuestManager.ClaimMilestoneCallback <>f__mg$cacheB;

		// Token: 0x02000E4C RID: 3660
		internal class FetchResponse : BaseReferenceHolder
		{
			// Token: 0x06004B57 RID: 19287 RVA: 0x001529AA File Offset: 0x00150DAA
			internal FetchResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B58 RID: 19288 RVA: 0x001529B3 File Offset: 0x00150DB3
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return QuestManager.QuestManager_FetchResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B59 RID: 19289 RVA: 0x001529C0 File Offset: 0x00150DC0
			internal NativeQuest Data()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				return new NativeQuest(QuestManager.QuestManager_FetchResponse_GetData(base.SelfPtr()));
			}

			// Token: 0x06004B5A RID: 19290 RVA: 0x001529DF File Offset: 0x00150DDF
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004B5B RID: 19291 RVA: 0x001529EA File Offset: 0x00150DEA
			protected override void CallDispose(HandleRef selfPointer)
			{
				QuestManager.QuestManager_FetchResponse_Dispose(selfPointer);
			}

			// Token: 0x06004B5C RID: 19292 RVA: 0x001529F2 File Offset: 0x00150DF2
			internal static QuestManager.FetchResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new QuestManager.FetchResponse(pointer);
			}
		}

		// Token: 0x02000E4D RID: 3661
		internal class FetchListResponse : BaseReferenceHolder
		{
			// Token: 0x06004B5D RID: 19293 RVA: 0x00152A18 File Offset: 0x00150E18
			internal FetchListResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B5E RID: 19294 RVA: 0x00152A21 File Offset: 0x00150E21
			internal CommonErrorStatus.ResponseStatus ResponseStatus()
			{
				return QuestManager.QuestManager_FetchListResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B5F RID: 19295 RVA: 0x00152A2E File Offset: 0x00150E2E
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.ResponseStatus)0;
			}

			// Token: 0x06004B60 RID: 19296 RVA: 0x00152A39 File Offset: 0x00150E39
			internal IEnumerable<NativeQuest> Data()
			{
				return PInvokeUtilities.ToEnumerable<NativeQuest>(QuestManager.QuestManager_FetchListResponse_GetData_Length(base.SelfPtr()), (UIntPtr index) => new NativeQuest(QuestManager.QuestManager_FetchListResponse_GetData_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004B61 RID: 19297 RVA: 0x00152A57 File Offset: 0x00150E57
			protected override void CallDispose(HandleRef selfPointer)
			{
				QuestManager.QuestManager_FetchListResponse_Dispose(selfPointer);
			}

			// Token: 0x06004B62 RID: 19298 RVA: 0x00152A5F File Offset: 0x00150E5F
			internal static QuestManager.FetchListResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new QuestManager.FetchListResponse(pointer);
			}
		}

		// Token: 0x02000E4E RID: 3662
		internal class ClaimMilestoneResponse : BaseReferenceHolder
		{
			// Token: 0x06004B64 RID: 19300 RVA: 0x00152A98 File Offset: 0x00150E98
			internal ClaimMilestoneResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B65 RID: 19301 RVA: 0x00152AA1 File Offset: 0x00150EA1
			internal CommonErrorStatus.QuestClaimMilestoneStatus ResponseStatus()
			{
				return QuestManager.QuestManager_ClaimMilestoneResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B66 RID: 19302 RVA: 0x00152AB0 File Offset: 0x00150EB0
			internal NativeQuest Quest()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				NativeQuest nativeQuest = new NativeQuest(QuestManager.QuestManager_ClaimMilestoneResponse_GetQuest(base.SelfPtr()));
				if (nativeQuest.Valid())
				{
					return nativeQuest;
				}
				nativeQuest.Dispose();
				return null;
			}

			// Token: 0x06004B67 RID: 19303 RVA: 0x00152AF0 File Offset: 0x00150EF0
			internal NativeQuestMilestone ClaimedMilestone()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				NativeQuestMilestone nativeQuestMilestone = new NativeQuestMilestone(QuestManager.QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(base.SelfPtr()));
				if (nativeQuestMilestone.Valid())
				{
					return nativeQuestMilestone;
				}
				nativeQuestMilestone.Dispose();
				return null;
			}

			// Token: 0x06004B68 RID: 19304 RVA: 0x00152B2F File Offset: 0x00150F2F
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.QuestClaimMilestoneStatus)0;
			}

			// Token: 0x06004B69 RID: 19305 RVA: 0x00152B3A File Offset: 0x00150F3A
			protected override void CallDispose(HandleRef selfPointer)
			{
				QuestManager.QuestManager_ClaimMilestoneResponse_Dispose(selfPointer);
			}

			// Token: 0x06004B6A RID: 19306 RVA: 0x00152B42 File Offset: 0x00150F42
			internal static QuestManager.ClaimMilestoneResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new QuestManager.ClaimMilestoneResponse(pointer);
			}
		}

		// Token: 0x02000E4F RID: 3663
		internal class AcceptResponse : BaseReferenceHolder
		{
			// Token: 0x06004B6B RID: 19307 RVA: 0x00152B68 File Offset: 0x00150F68
			internal AcceptResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B6C RID: 19308 RVA: 0x00152B71 File Offset: 0x00150F71
			internal CommonErrorStatus.QuestAcceptStatus ResponseStatus()
			{
				return QuestManager.QuestManager_AcceptResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B6D RID: 19309 RVA: 0x00152B7E File Offset: 0x00150F7E
			internal NativeQuest AcceptedQuest()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				return new NativeQuest(QuestManager.QuestManager_AcceptResponse_GetAcceptedQuest(base.SelfPtr()));
			}

			// Token: 0x06004B6E RID: 19310 RVA: 0x00152B9D File Offset: 0x00150F9D
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.QuestAcceptStatus)0;
			}

			// Token: 0x06004B6F RID: 19311 RVA: 0x00152BA8 File Offset: 0x00150FA8
			protected override void CallDispose(HandleRef selfPointer)
			{
				QuestManager.QuestManager_AcceptResponse_Dispose(selfPointer);
			}

			// Token: 0x06004B70 RID: 19312 RVA: 0x00152BB0 File Offset: 0x00150FB0
			internal static QuestManager.AcceptResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new QuestManager.AcceptResponse(pointer);
			}
		}

		// Token: 0x02000E50 RID: 3664
		internal class QuestUIResponse : BaseReferenceHolder
		{
			// Token: 0x06004B71 RID: 19313 RVA: 0x00152BD6 File Offset: 0x00150FD6
			internal QuestUIResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004B72 RID: 19314 RVA: 0x00152BDF File Offset: 0x00150FDF
			internal CommonErrorStatus.UIStatus RequestStatus()
			{
				return QuestManager.QuestManager_QuestUIResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004B73 RID: 19315 RVA: 0x00152BEC File Offset: 0x00150FEC
			internal bool RequestSucceeded()
			{
				return this.RequestStatus() > (CommonErrorStatus.UIStatus)0;
			}

			// Token: 0x06004B74 RID: 19316 RVA: 0x00152BF8 File Offset: 0x00150FF8
			internal NativeQuest AcceptedQuest()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				NativeQuest nativeQuest = new NativeQuest(QuestManager.QuestManager_QuestUIResponse_GetAcceptedQuest(base.SelfPtr()));
				if (nativeQuest.Valid())
				{
					return nativeQuest;
				}
				nativeQuest.Dispose();
				return null;
			}

			// Token: 0x06004B75 RID: 19317 RVA: 0x00152C38 File Offset: 0x00151038
			internal NativeQuestMilestone MilestoneToClaim()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				NativeQuestMilestone nativeQuestMilestone = new NativeQuestMilestone(QuestManager.QuestManager_QuestUIResponse_GetMilestoneToClaim(base.SelfPtr()));
				if (nativeQuestMilestone.Valid())
				{
					return nativeQuestMilestone;
				}
				nativeQuestMilestone.Dispose();
				return null;
			}

			// Token: 0x06004B76 RID: 19318 RVA: 0x00152C77 File Offset: 0x00151077
			protected override void CallDispose(HandleRef selfPointer)
			{
				QuestManager.QuestManager_QuestUIResponse_Dispose(selfPointer);
			}

			// Token: 0x06004B77 RID: 19319 RVA: 0x00152C7F File Offset: 0x0015107F
			internal static QuestManager.QuestUIResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new QuestManager.QuestUIResponse(pointer);
			}
		}
	}
}
