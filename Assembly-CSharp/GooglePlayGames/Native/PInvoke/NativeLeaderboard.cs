﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E2A RID: 3626
	internal class NativeLeaderboard : BaseReferenceHolder
	{
		// Token: 0x06004A25 RID: 18981 RVA: 0x001506AA File Offset: 0x0014EAAA
		internal NativeLeaderboard(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004A26 RID: 18982 RVA: 0x001506B3 File Offset: 0x0014EAB3
		protected override void CallDispose(HandleRef selfPointer)
		{
			Leaderboard.Leaderboard_Dispose(selfPointer);
		}

		// Token: 0x06004A27 RID: 18983 RVA: 0x001506BB File Offset: 0x0014EABB
		internal string Title()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr out_size) => Leaderboard.Leaderboard_Name(base.SelfPtr(), out_string, out_size));
		}

		// Token: 0x06004A28 RID: 18984 RVA: 0x001506CE File Offset: 0x0014EACE
		internal static NativeLeaderboard FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeLeaderboard(pointer);
		}
	}
}
