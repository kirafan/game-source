﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3F RID: 3647
	internal class NearbyConnectionsManagerBuilder : BaseReferenceHolder
	{
		// Token: 0x06004B03 RID: 19203 RVA: 0x00151D61 File Offset: 0x00150161
		internal NearbyConnectionsManagerBuilder() : base(NearbyConnectionsBuilder.NearbyConnections_Builder_Construct())
		{
		}

		// Token: 0x06004B04 RID: 19204 RVA: 0x00151D6E File Offset: 0x0015016E
		internal NearbyConnectionsManagerBuilder SetOnInitializationFinished(Action<NearbyConnectionsStatus.InitializationStatus> callback)
		{
			HandleRef self = base.SelfPtr();
			if (NearbyConnectionsManagerBuilder.<>f__mg$cache0 == null)
			{
				NearbyConnectionsManagerBuilder.<>f__mg$cache0 = new NearbyConnectionsBuilder.OnInitializationFinishedCallback(NearbyConnectionsManagerBuilder.InternalOnInitializationFinishedCallback);
			}
			NearbyConnectionsBuilder.NearbyConnections_Builder_SetOnInitializationFinished(self, NearbyConnectionsManagerBuilder.<>f__mg$cache0, Callbacks.ToIntPtr(callback));
			return this;
		}

		// Token: 0x06004B05 RID: 19205 RVA: 0x00151DA0 File Offset: 0x001501A0
		[MonoPInvokeCallback(typeof(NearbyConnectionsBuilder.OnInitializationFinishedCallback))]
		private static void InternalOnInitializationFinishedCallback(NearbyConnectionsStatus.InitializationStatus status, IntPtr userData)
		{
			Action<NearbyConnectionsStatus.InitializationStatus> action = Callbacks.IntPtrToPermanentCallback<Action<NearbyConnectionsStatus.InitializationStatus>>(userData);
			if (action == null)
			{
				Logger.w("Callback for Initialization is null. Received status: " + status);
				return;
			}
			try
			{
				action(status);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing NearbyConnectionsManagerBuilder#InternalOnInitializationFinishedCallback. Smothering exception: " + arg);
			}
		}

		// Token: 0x06004B06 RID: 19206 RVA: 0x00151E04 File Offset: 0x00150204
		internal NearbyConnectionsManagerBuilder SetLocalClientId(long localClientId)
		{
			NearbyConnectionsBuilder.NearbyConnections_Builder_SetClientId(base.SelfPtr(), localClientId);
			return this;
		}

		// Token: 0x06004B07 RID: 19207 RVA: 0x00151E13 File Offset: 0x00150213
		internal NearbyConnectionsManagerBuilder SetDefaultLogLevel(Types.LogLevel minLevel)
		{
			NearbyConnectionsBuilder.NearbyConnections_Builder_SetDefaultOnLog(base.SelfPtr(), minLevel);
			return this;
		}

		// Token: 0x06004B08 RID: 19208 RVA: 0x00151E22 File Offset: 0x00150222
		internal NearbyConnectionsManager Build(PlatformConfiguration configuration)
		{
			return new NearbyConnectionsManager(NearbyConnectionsBuilder.NearbyConnections_Builder_Create(base.SelfPtr(), configuration.AsPointer()));
		}

		// Token: 0x06004B09 RID: 19209 RVA: 0x00151E3A File Offset: 0x0015023A
		protected override void CallDispose(HandleRef selfPointer)
		{
			NearbyConnectionsBuilder.NearbyConnections_Builder_Dispose(selfPointer);
		}

		// Token: 0x04004D0F RID: 19727
		[CompilerGenerated]
		private static NearbyConnectionsBuilder.OnInitializationFinishedCallback <>f__mg$cache0;
	}
}
