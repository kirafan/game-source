﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E3B RID: 3643
	internal class NativeTurnBasedMatch : BaseReferenceHolder
	{
		// Token: 0x06004AC0 RID: 19136 RVA: 0x0015146B File Offset: 0x0014F86B
		internal NativeTurnBasedMatch(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004AC1 RID: 19137 RVA: 0x00151474 File Offset: 0x0014F874
		internal uint AvailableAutomatchSlots()
		{
			return GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_AutomatchingSlotsAvailable(base.SelfPtr());
		}

		// Token: 0x06004AC2 RID: 19138 RVA: 0x00151481 File Offset: 0x0014F881
		internal ulong CreationTime()
		{
			return GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_CreationTime(base.SelfPtr());
		}

		// Token: 0x06004AC3 RID: 19139 RVA: 0x0015148E File Offset: 0x0014F88E
		internal IEnumerable<MultiplayerParticipant> Participants()
		{
			return PInvokeUtilities.ToEnumerable<MultiplayerParticipant>(GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Participants_Length(base.SelfPtr()), (UIntPtr index) => new MultiplayerParticipant(GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Participants_GetElement(base.SelfPtr(), index)));
		}

		// Token: 0x06004AC4 RID: 19140 RVA: 0x001514AC File Offset: 0x0014F8AC
		internal uint Version()
		{
			return GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Version(base.SelfPtr());
		}

		// Token: 0x06004AC5 RID: 19141 RVA: 0x001514B9 File Offset: 0x0014F8B9
		internal uint Variant()
		{
			return GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Variant(base.SelfPtr());
		}

		// Token: 0x06004AC6 RID: 19142 RVA: 0x001514C6 File Offset: 0x0014F8C6
		internal ParticipantResults Results()
		{
			return new ParticipantResults(GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_ParticipantResults(base.SelfPtr()));
		}

		// Token: 0x06004AC7 RID: 19143 RVA: 0x001514D8 File Offset: 0x0014F8D8
		internal MultiplayerParticipant ParticipantWithId(string participantId)
		{
			foreach (MultiplayerParticipant multiplayerParticipant in this.Participants())
			{
				if (multiplayerParticipant.Id().Equals(participantId))
				{
					return multiplayerParticipant;
				}
				multiplayerParticipant.Dispose();
			}
			return null;
		}

		// Token: 0x06004AC8 RID: 19144 RVA: 0x0015154C File Offset: 0x0014F94C
		internal MultiplayerParticipant PendingParticipant()
		{
			MultiplayerParticipant multiplayerParticipant = new MultiplayerParticipant(GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_PendingParticipant(base.SelfPtr()));
			if (!multiplayerParticipant.Valid())
			{
				multiplayerParticipant.Dispose();
				return null;
			}
			return multiplayerParticipant;
		}

		// Token: 0x06004AC9 RID: 19145 RVA: 0x0015157E File Offset: 0x0014F97E
		internal Types.MatchStatus MatchStatus()
		{
			return GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Status(base.SelfPtr());
		}

		// Token: 0x06004ACA RID: 19146 RVA: 0x0015158B File Offset: 0x0014F98B
		internal string Description()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Description(base.SelfPtr(), out_string, size));
		}

		// Token: 0x06004ACB RID: 19147 RVA: 0x001515A0 File Offset: 0x0014F9A0
		internal bool HasRematchId()
		{
			string text = this.RematchId();
			return string.IsNullOrEmpty(text) || !text.Equals("(null)");
		}

		// Token: 0x06004ACC RID: 19148 RVA: 0x001515D0 File Offset: 0x0014F9D0
		internal string RematchId()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_RematchId(base.SelfPtr(), out_string, size));
		}

		// Token: 0x06004ACD RID: 19149 RVA: 0x001515E3 File Offset: 0x0014F9E3
		internal byte[] Data()
		{
			if (!GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_HasData(base.SelfPtr()))
			{
				Logger.d("Match has no data.");
				return null;
			}
			return PInvokeUtilities.OutParamsToArray<byte>((byte[] bytes, UIntPtr size) => GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Data(base.SelfPtr(), bytes, size));
		}

		// Token: 0x06004ACE RID: 19150 RVA: 0x00151612 File Offset: 0x0014FA12
		internal string Id()
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Id(base.SelfPtr(), out_string, size));
		}

		// Token: 0x06004ACF RID: 19151 RVA: 0x00151625 File Offset: 0x0014FA25
		protected override void CallDispose(HandleRef selfPointer)
		{
			GooglePlayGames.Native.Cwrapper.TurnBasedMatch.TurnBasedMatch_Dispose(selfPointer);
		}

		// Token: 0x06004AD0 RID: 19152 RVA: 0x00151630 File Offset: 0x0014FA30
		internal GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch AsTurnBasedMatch(string selfPlayerId)
		{
			List<Participant> list = new List<Participant>();
			string selfParticipantId = null;
			string pendingParticipantId = null;
			using (MultiplayerParticipant multiplayerParticipant = this.PendingParticipant())
			{
				if (multiplayerParticipant != null)
				{
					pendingParticipantId = multiplayerParticipant.Id();
				}
			}
			foreach (MultiplayerParticipant multiplayerParticipant2 in this.Participants())
			{
				using (multiplayerParticipant2)
				{
					using (NativePlayer nativePlayer = multiplayerParticipant2.Player())
					{
						if (nativePlayer != null && nativePlayer.Id().Equals(selfPlayerId))
						{
							selfParticipantId = multiplayerParticipant2.Id();
						}
					}
					list.Add(multiplayerParticipant2.AsParticipant());
				}
			}
			bool canRematch = this.MatchStatus() == Types.MatchStatus.COMPLETED && !this.HasRematchId();
			return new GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch(this.Id(), this.Data(), canRematch, selfParticipantId, list, this.AvailableAutomatchSlots(), pendingParticipantId, NativeTurnBasedMatch.ToTurnStatus(this.MatchStatus()), NativeTurnBasedMatch.ToMatchStatus(pendingParticipantId, this.MatchStatus()), this.Variant(), this.Version());
		}

		// Token: 0x06004AD1 RID: 19153 RVA: 0x00151798 File Offset: 0x0014FB98
		private static GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus ToTurnStatus(Types.MatchStatus status)
		{
			switch (status)
			{
			case Types.MatchStatus.INVITED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Invited;
			case Types.MatchStatus.THEIR_TURN:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.TheirTurn;
			case Types.MatchStatus.MY_TURN:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.MyTurn;
			case Types.MatchStatus.PENDING_COMPLETION:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Complete;
			case Types.MatchStatus.COMPLETED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Complete;
			case Types.MatchStatus.CANCELED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Complete;
			case Types.MatchStatus.EXPIRED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Complete;
			default:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchTurnStatus.Unknown;
			}
		}

		// Token: 0x06004AD2 RID: 19154 RVA: 0x001517D4 File Offset: 0x0014FBD4
		private static GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus ToMatchStatus(string pendingParticipantId, Types.MatchStatus status)
		{
			switch (status)
			{
			case Types.MatchStatus.INVITED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Active;
			case Types.MatchStatus.THEIR_TURN:
				return (pendingParticipantId != null) ? GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Active : GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.AutoMatching;
			case Types.MatchStatus.MY_TURN:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Active;
			case Types.MatchStatus.PENDING_COMPLETION:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Complete;
			case Types.MatchStatus.COMPLETED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Complete;
			case Types.MatchStatus.CANCELED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Cancelled;
			case Types.MatchStatus.EXPIRED:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Expired;
			default:
				return GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch.MatchStatus.Unknown;
			}
		}

		// Token: 0x06004AD3 RID: 19155 RVA: 0x00151825 File Offset: 0x0014FC25
		internal static NativeTurnBasedMatch FromPointer(IntPtr selfPointer)
		{
			if (PInvokeUtilities.IsNull(selfPointer))
			{
				return null;
			}
			return new NativeTurnBasedMatch(selfPointer);
		}
	}
}
