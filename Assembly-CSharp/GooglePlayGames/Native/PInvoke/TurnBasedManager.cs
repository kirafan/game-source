﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E61 RID: 3681
	internal class TurnBasedManager
	{
		// Token: 0x06004BFB RID: 19451 RVA: 0x00153E71 File Offset: 0x00152271
		internal TurnBasedManager(GameServices services)
		{
			this.mGameServices = services;
		}

		// Token: 0x06004BFC RID: 19452 RVA: 0x00153E80 File Offset: 0x00152280
		internal void GetMatch(string matchId, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (TurnBasedManager.<>f__mg$cache0 == null)
			{
				TurnBasedManager.<>f__mg$cache0 = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_FetchMatch(self, matchId, TurnBasedManager.<>f__mg$cache0, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004BFD RID: 19453 RVA: 0x00153EB6 File Offset: 0x001522B6
		[MonoPInvokeCallback(typeof(TurnBasedMultiplayerManager.TurnBasedMatchCallback))]
		internal static void InternalTurnBasedMatchCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("TurnBasedManager#InternalTurnBasedMatchCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004BFE RID: 19454 RVA: 0x00153EC5 File Offset: 0x001522C5
		internal void CreateMatch(TurnBasedMatchConfig config, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr config2 = config.AsPointer();
			if (TurnBasedManager.<>f__mg$cache1 == null)
			{
				TurnBasedManager.<>f__mg$cache1 = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_CreateTurnBasedMatch(self, config2, TurnBasedManager.<>f__mg$cache1, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004BFF RID: 19455 RVA: 0x00153F00 File Offset: 0x00152300
		internal void ShowPlayerSelectUI(uint minimumPlayers, uint maxiumPlayers, bool allowAutomatching, Action<PlayerSelectUIResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (TurnBasedManager.<>f__mg$cache3 == null)
			{
				TurnBasedManager.<>f__mg$cache3 = new TurnBasedMultiplayerManager.PlayerSelectUICallback(TurnBasedManager.InternalPlayerSelectUIcallback);
			}
			TurnBasedMultiplayerManager.PlayerSelectUICallback callback2 = TurnBasedManager.<>f__mg$cache3;
			if (TurnBasedManager.<>f__mg$cache2 == null)
			{
				TurnBasedManager.<>f__mg$cache2 = new Func<IntPtr, PlayerSelectUIResponse>(PlayerSelectUIResponse.FromPointer);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_ShowPlayerSelectUI(self, minimumPlayers, maxiumPlayers, allowAutomatching, callback2, Callbacks.ToIntPtr<PlayerSelectUIResponse>(callback, TurnBasedManager.<>f__mg$cache2));
		}

		// Token: 0x06004C00 RID: 19456 RVA: 0x00153F61 File Offset: 0x00152361
		[MonoPInvokeCallback(typeof(TurnBasedMultiplayerManager.PlayerSelectUICallback))]
		internal static void InternalPlayerSelectUIcallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("TurnBasedManager#PlayerSelectUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C01 RID: 19457 RVA: 0x00153F70 File Offset: 0x00152370
		internal void GetAllTurnbasedMatches(Action<TurnBasedManager.TurnBasedMatchesResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (TurnBasedManager.<>f__mg$cache5 == null)
			{
				TurnBasedManager.<>f__mg$cache5 = new TurnBasedMultiplayerManager.TurnBasedMatchesCallback(TurnBasedManager.InternalTurnBasedMatchesCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMatchesCallback callback2 = TurnBasedManager.<>f__mg$cache5;
			if (TurnBasedManager.<>f__mg$cache4 == null)
			{
				TurnBasedManager.<>f__mg$cache4 = new Func<IntPtr, TurnBasedManager.TurnBasedMatchesResponse>(TurnBasedManager.TurnBasedMatchesResponse.FromPointer);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_FetchMatches(self, callback2, Callbacks.ToIntPtr<TurnBasedManager.TurnBasedMatchesResponse>(callback, TurnBasedManager.<>f__mg$cache4));
		}

		// Token: 0x06004C02 RID: 19458 RVA: 0x00153FCD File Offset: 0x001523CD
		[MonoPInvokeCallback(typeof(TurnBasedMultiplayerManager.TurnBasedMatchesCallback))]
		internal static void InternalTurnBasedMatchesCallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("TurnBasedManager#TurnBasedMatchesCallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C03 RID: 19459 RVA: 0x00153FDC File Offset: 0x001523DC
		internal void AcceptInvitation(MultiplayerInvitation invitation, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			Logger.d("Accepting invitation: " + invitation.AsPointer().ToInt64());
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr invitation2 = invitation.AsPointer();
			if (TurnBasedManager.<>f__mg$cache6 == null)
			{
				TurnBasedManager.<>f__mg$cache6 = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_AcceptInvitation(self, invitation2, TurnBasedManager.<>f__mg$cache6, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004C04 RID: 19460 RVA: 0x00154044 File Offset: 0x00152444
		internal void DeclineInvitation(MultiplayerInvitation invitation)
		{
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_DeclineInvitation(this.mGameServices.AsHandle(), invitation.AsPointer());
		}

		// Token: 0x06004C05 RID: 19461 RVA: 0x0015405C File Offset: 0x0015245C
		internal void TakeTurn(NativeTurnBasedMatch match, byte[] data, MultiplayerParticipant nextParticipant, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			UIntPtr match_data_size = new UIntPtr((uint)data.Length);
			IntPtr results = match.Results().AsPointer();
			IntPtr next_participant = nextParticipant.AsPointer();
			if (TurnBasedManager.<>f__mg$cache7 == null)
			{
				TurnBasedManager.<>f__mg$cache7 = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TakeMyTurn(self, match2, data, match_data_size, results, next_participant, TurnBasedManager.<>f__mg$cache7, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004C06 RID: 19462 RVA: 0x001540BD File Offset: 0x001524BD
		[MonoPInvokeCallback(typeof(TurnBasedMultiplayerManager.MatchInboxUICallback))]
		internal static void InternalMatchInboxUICallback(IntPtr response, IntPtr data)
		{
			Callbacks.PerformInternalCallback("TurnBasedManager#MatchInboxUICallback", Callbacks.Type.Temporary, response, data);
		}

		// Token: 0x06004C07 RID: 19463 RVA: 0x001540CC File Offset: 0x001524CC
		internal void ShowInboxUI(Action<TurnBasedManager.MatchInboxUIResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			if (TurnBasedManager.<>f__mg$cache9 == null)
			{
				TurnBasedManager.<>f__mg$cache9 = new TurnBasedMultiplayerManager.MatchInboxUICallback(TurnBasedManager.InternalMatchInboxUICallback);
			}
			TurnBasedMultiplayerManager.MatchInboxUICallback callback2 = TurnBasedManager.<>f__mg$cache9;
			if (TurnBasedManager.<>f__mg$cache8 == null)
			{
				TurnBasedManager.<>f__mg$cache8 = new Func<IntPtr, TurnBasedManager.MatchInboxUIResponse>(TurnBasedManager.MatchInboxUIResponse.FromPointer);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_ShowMatchInboxUI(self, callback2, Callbacks.ToIntPtr<TurnBasedManager.MatchInboxUIResponse>(callback, TurnBasedManager.<>f__mg$cache8));
		}

		// Token: 0x06004C08 RID: 19464 RVA: 0x0015412C File Offset: 0x0015252C
		[MonoPInvokeCallback(typeof(TurnBasedMultiplayerManager.MultiplayerStatusCallback))]
		internal static void InternalMultiplayerStatusCallback(CommonErrorStatus.MultiplayerStatus status, IntPtr data)
		{
			Logger.d("InternalMultiplayerStatusCallback: " + status);
			Action<CommonErrorStatus.MultiplayerStatus> action = Callbacks.IntPtrToTempCallback<Action<CommonErrorStatus.MultiplayerStatus>>(data);
			try
			{
				action(status);
			}
			catch (Exception arg)
			{
				Logger.e("Error encountered executing InternalMultiplayerStatusCallback. Smothering to avoid passing exception into Native: " + arg);
			}
		}

		// Token: 0x06004C09 RID: 19465 RVA: 0x00154188 File Offset: 0x00152588
		internal void LeaveDuringMyTurn(NativeTurnBasedMatch match, MultiplayerParticipant nextParticipant, Action<CommonErrorStatus.MultiplayerStatus> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			IntPtr next_participant = nextParticipant.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheA == null)
			{
				TurnBasedManager.<>f__mg$cacheA = new TurnBasedMultiplayerManager.MultiplayerStatusCallback(TurnBasedManager.InternalMultiplayerStatusCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(self, match2, next_participant, TurnBasedManager.<>f__mg$cacheA, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004C0A RID: 19466 RVA: 0x001541D4 File Offset: 0x001525D4
		internal void FinishMatchDuringMyTurn(NativeTurnBasedMatch match, byte[] data, ParticipantResults results, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			UIntPtr match_data_size = new UIntPtr((uint)data.Length);
			IntPtr results2 = results.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheB == null)
			{
				TurnBasedManager.<>f__mg$cacheB = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(self, match2, data, match_data_size, results2, TurnBasedManager.<>f__mg$cacheB, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004C0B RID: 19467 RVA: 0x0015422A File Offset: 0x0015262A
		internal void ConfirmPendingCompletion(NativeTurnBasedMatch match, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheC == null)
			{
				TurnBasedManager.<>f__mg$cacheC = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_ConfirmPendingCompletion(self, match2, TurnBasedManager.<>f__mg$cacheC, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004C0C RID: 19468 RVA: 0x00154265 File Offset: 0x00152665
		internal void LeaveMatchDuringTheirTurn(NativeTurnBasedMatch match, Action<CommonErrorStatus.MultiplayerStatus> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheD == null)
			{
				TurnBasedManager.<>f__mg$cacheD = new TurnBasedMultiplayerManager.MultiplayerStatusCallback(TurnBasedManager.InternalMultiplayerStatusCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(self, match2, TurnBasedManager.<>f__mg$cacheD, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004C0D RID: 19469 RVA: 0x001542A0 File Offset: 0x001526A0
		internal void CancelMatch(NativeTurnBasedMatch match, Action<CommonErrorStatus.MultiplayerStatus> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheE == null)
			{
				TurnBasedManager.<>f__mg$cacheE = new TurnBasedMultiplayerManager.MultiplayerStatusCallback(TurnBasedManager.InternalMultiplayerStatusCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_CancelMatch(self, match2, TurnBasedManager.<>f__mg$cacheE, Callbacks.ToIntPtr(callback));
		}

		// Token: 0x06004C0E RID: 19470 RVA: 0x001542DB File Offset: 0x001526DB
		internal void Rematch(NativeTurnBasedMatch match, Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			HandleRef self = this.mGameServices.AsHandle();
			IntPtr match2 = match.AsPointer();
			if (TurnBasedManager.<>f__mg$cacheF == null)
			{
				TurnBasedManager.<>f__mg$cacheF = new TurnBasedMultiplayerManager.TurnBasedMatchCallback(TurnBasedManager.InternalTurnBasedMatchCallback);
			}
			TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_Rematch(self, match2, TurnBasedManager.<>f__mg$cacheF, TurnBasedManager.ToCallbackPointer(callback));
		}

		// Token: 0x06004C0F RID: 19471 RVA: 0x00154316 File Offset: 0x00152716
		private static IntPtr ToCallbackPointer(Action<TurnBasedManager.TurnBasedMatchResponse> callback)
		{
			if (TurnBasedManager.<>f__mg$cache10 == null)
			{
				TurnBasedManager.<>f__mg$cache10 = new Func<IntPtr, TurnBasedManager.TurnBasedMatchResponse>(TurnBasedManager.TurnBasedMatchResponse.FromPointer);
			}
			return Callbacks.ToIntPtr<TurnBasedManager.TurnBasedMatchResponse>(callback, TurnBasedManager.<>f__mg$cache10);
		}

		// Token: 0x04004D4D RID: 19789
		private readonly GameServices mGameServices;

		// Token: 0x04004D4E RID: 19790
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cache0;

		// Token: 0x04004D4F RID: 19791
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cache1;

		// Token: 0x04004D50 RID: 19792
		[CompilerGenerated]
		private static Func<IntPtr, PlayerSelectUIResponse> <>f__mg$cache2;

		// Token: 0x04004D51 RID: 19793
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.PlayerSelectUICallback <>f__mg$cache3;

		// Token: 0x04004D52 RID: 19794
		[CompilerGenerated]
		private static Func<IntPtr, TurnBasedManager.TurnBasedMatchesResponse> <>f__mg$cache4;

		// Token: 0x04004D53 RID: 19795
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchesCallback <>f__mg$cache5;

		// Token: 0x04004D54 RID: 19796
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cache6;

		// Token: 0x04004D55 RID: 19797
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cache7;

		// Token: 0x04004D56 RID: 19798
		[CompilerGenerated]
		private static Func<IntPtr, TurnBasedManager.MatchInboxUIResponse> <>f__mg$cache8;

		// Token: 0x04004D57 RID: 19799
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.MatchInboxUICallback <>f__mg$cache9;

		// Token: 0x04004D58 RID: 19800
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.MultiplayerStatusCallback <>f__mg$cacheA;

		// Token: 0x04004D59 RID: 19801
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cacheB;

		// Token: 0x04004D5A RID: 19802
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cacheC;

		// Token: 0x04004D5B RID: 19803
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.MultiplayerStatusCallback <>f__mg$cacheD;

		// Token: 0x04004D5C RID: 19804
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.MultiplayerStatusCallback <>f__mg$cacheE;

		// Token: 0x04004D5D RID: 19805
		[CompilerGenerated]
		private static TurnBasedMultiplayerManager.TurnBasedMatchCallback <>f__mg$cacheF;

		// Token: 0x04004D5E RID: 19806
		[CompilerGenerated]
		private static Func<IntPtr, TurnBasedManager.TurnBasedMatchResponse> <>f__mg$cache10;

		// Token: 0x02000E62 RID: 3682
		// (Invoke) Token: 0x06004C11 RID: 19473
		internal delegate void TurnBasedMatchCallback(TurnBasedManager.TurnBasedMatchResponse response);

		// Token: 0x02000E63 RID: 3683
		internal class MatchInboxUIResponse : BaseReferenceHolder
		{
			// Token: 0x06004C14 RID: 19476 RVA: 0x0015433B File Offset: 0x0015273B
			internal MatchInboxUIResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004C15 RID: 19477 RVA: 0x00154344 File Offset: 0x00152744
			internal CommonErrorStatus.UIStatus UiStatus()
			{
				return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004C16 RID: 19478 RVA: 0x00154351 File Offset: 0x00152751
			internal NativeTurnBasedMatch Match()
			{
				if (this.UiStatus() != CommonErrorStatus.UIStatus.VALID)
				{
					return null;
				}
				return new NativeTurnBasedMatch(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(base.SelfPtr()));
			}

			// Token: 0x06004C17 RID: 19479 RVA: 0x00154371 File Offset: 0x00152771
			protected override void CallDispose(HandleRef selfPointer)
			{
				TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(selfPointer);
			}

			// Token: 0x06004C18 RID: 19480 RVA: 0x00154379 File Offset: 0x00152779
			internal static TurnBasedManager.MatchInboxUIResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new TurnBasedManager.MatchInboxUIResponse(pointer);
			}
		}

		// Token: 0x02000E64 RID: 3684
		internal class TurnBasedMatchResponse : BaseReferenceHolder
		{
			// Token: 0x06004C19 RID: 19481 RVA: 0x0015439F File Offset: 0x0015279F
			internal TurnBasedMatchResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004C1A RID: 19482 RVA: 0x001543A8 File Offset: 0x001527A8
			internal CommonErrorStatus.MultiplayerStatus ResponseStatus()
			{
				return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004C1B RID: 19483 RVA: 0x001543B5 File Offset: 0x001527B5
			internal bool RequestSucceeded()
			{
				return this.ResponseStatus() > (CommonErrorStatus.MultiplayerStatus)0;
			}

			// Token: 0x06004C1C RID: 19484 RVA: 0x001543C0 File Offset: 0x001527C0
			internal NativeTurnBasedMatch Match()
			{
				if (!this.RequestSucceeded())
				{
					return null;
				}
				return new NativeTurnBasedMatch(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(base.SelfPtr()));
			}

			// Token: 0x06004C1D RID: 19485 RVA: 0x001543DF File Offset: 0x001527DF
			protected override void CallDispose(HandleRef selfPointer)
			{
				TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(selfPointer);
			}

			// Token: 0x06004C1E RID: 19486 RVA: 0x001543E7 File Offset: 0x001527E7
			internal static TurnBasedManager.TurnBasedMatchResponse FromPointer(IntPtr pointer)
			{
				if (pointer.Equals(IntPtr.Zero))
				{
					return null;
				}
				return new TurnBasedManager.TurnBasedMatchResponse(pointer);
			}
		}

		// Token: 0x02000E65 RID: 3685
		internal class TurnBasedMatchesResponse : BaseReferenceHolder
		{
			// Token: 0x06004C1F RID: 19487 RVA: 0x0015440D File Offset: 0x0015280D
			internal TurnBasedMatchesResponse(IntPtr selfPointer) : base(selfPointer)
			{
			}

			// Token: 0x06004C20 RID: 19488 RVA: 0x00154416 File Offset: 0x00152816
			protected override void CallDispose(HandleRef selfPointer)
			{
				TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(base.SelfPtr());
			}

			// Token: 0x06004C21 RID: 19489 RVA: 0x00154423 File Offset: 0x00152823
			internal CommonErrorStatus.MultiplayerStatus Status()
			{
				return TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(base.SelfPtr());
			}

			// Token: 0x06004C22 RID: 19490 RVA: 0x00154430 File Offset: 0x00152830
			internal IEnumerable<MultiplayerInvitation> Invitations()
			{
				return PInvokeUtilities.ToEnumerable<MultiplayerInvitation>(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(base.SelfPtr()), (UIntPtr index) => new MultiplayerInvitation(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004C23 RID: 19491 RVA: 0x00154450 File Offset: 0x00152850
			internal int InvitationCount()
			{
				return (int)TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(base.SelfPtr()).ToUInt32();
			}

			// Token: 0x06004C24 RID: 19492 RVA: 0x00154470 File Offset: 0x00152870
			internal IEnumerable<NativeTurnBasedMatch> MyTurnMatches()
			{
				return PInvokeUtilities.ToEnumerable<NativeTurnBasedMatch>(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(base.SelfPtr()), (UIntPtr index) => new NativeTurnBasedMatch(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004C25 RID: 19493 RVA: 0x00154490 File Offset: 0x00152890
			internal int MyTurnMatchesCount()
			{
				return (int)TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(base.SelfPtr()).ToUInt32();
			}

			// Token: 0x06004C26 RID: 19494 RVA: 0x001544B0 File Offset: 0x001528B0
			internal IEnumerable<NativeTurnBasedMatch> TheirTurnMatches()
			{
				return PInvokeUtilities.ToEnumerable<NativeTurnBasedMatch>(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(base.SelfPtr()), (UIntPtr index) => new NativeTurnBasedMatch(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004C27 RID: 19495 RVA: 0x001544D0 File Offset: 0x001528D0
			internal int TheirTurnMatchesCount()
			{
				return (int)TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(base.SelfPtr()).ToUInt32();
			}

			// Token: 0x06004C28 RID: 19496 RVA: 0x001544F0 File Offset: 0x001528F0
			internal IEnumerable<NativeTurnBasedMatch> CompletedMatches()
			{
				return PInvokeUtilities.ToEnumerable<NativeTurnBasedMatch>(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(base.SelfPtr()), (UIntPtr index) => new NativeTurnBasedMatch(TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(base.SelfPtr(), index)));
			}

			// Token: 0x06004C29 RID: 19497 RVA: 0x00154510 File Offset: 0x00152910
			internal int CompletedMatchesCount()
			{
				return (int)TurnBasedMultiplayerManager.TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(base.SelfPtr()).ToUInt32();
			}

			// Token: 0x06004C2A RID: 19498 RVA: 0x00154530 File Offset: 0x00152930
			internal static TurnBasedManager.TurnBasedMatchesResponse FromPointer(IntPtr pointer)
			{
				if (PInvokeUtilities.IsNull(pointer))
				{
					return null;
				}
				return new TurnBasedManager.TurnBasedMatchesResponse(pointer);
			}
		}
	}
}
