﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E6B RID: 3691
	internal class IsCaptureAvailableResponse : BaseReferenceHolder
	{
		// Token: 0x06004C5A RID: 19546 RVA: 0x00154955 File Offset: 0x00152D55
		internal IsCaptureAvailableResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004C5B RID: 19547 RVA: 0x0015495E File Offset: 0x00152D5E
		protected override void CallDispose(HandleRef selfPointer)
		{
			VideoManager.VideoManager_IsCaptureAvailableResponse_Dispose(selfPointer);
		}

		// Token: 0x06004C5C RID: 19548 RVA: 0x00154966 File Offset: 0x00152D66
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return VideoManager.VideoManager_IsCaptureAvailableResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004C5D RID: 19549 RVA: 0x00154973 File Offset: 0x00152D73
		internal bool RequestSucceeded()
		{
			return this.GetStatus() > (CommonErrorStatus.ResponseStatus)0;
		}

		// Token: 0x06004C5E RID: 19550 RVA: 0x0015497E File Offset: 0x00152D7E
		internal bool IsCaptureAvailable()
		{
			return VideoManager.VideoManager_IsCaptureAvailableResponse_GetIsCaptureAvailable(base.SelfPtr());
		}

		// Token: 0x06004C5F RID: 19551 RVA: 0x0015498B File Offset: 0x00152D8B
		internal static IsCaptureAvailableResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new IsCaptureAvailableResponse(pointer);
		}
	}
}
