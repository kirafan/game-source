﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E6A RID: 3690
	internal class GetCaptureStateResponse : BaseReferenceHolder
	{
		// Token: 0x06004C54 RID: 19540 RVA: 0x001548EF File Offset: 0x00152CEF
		internal GetCaptureStateResponse(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004C55 RID: 19541 RVA: 0x001548F8 File Offset: 0x00152CF8
		protected override void CallDispose(HandleRef selfPointer)
		{
			VideoManager.VideoManager_GetCaptureStateResponse_Dispose(base.SelfPtr());
		}

		// Token: 0x06004C56 RID: 19542 RVA: 0x00154905 File Offset: 0x00152D05
		internal NativeVideoCaptureState GetData()
		{
			return NativeVideoCaptureState.FromPointer(VideoManager.VideoManager_GetCaptureStateResponse_GetVideoCaptureState(base.SelfPtr()));
		}

		// Token: 0x06004C57 RID: 19543 RVA: 0x00154917 File Offset: 0x00152D17
		internal CommonErrorStatus.ResponseStatus GetStatus()
		{
			return VideoManager.VideoManager_GetCaptureStateResponse_GetStatus(base.SelfPtr());
		}

		// Token: 0x06004C58 RID: 19544 RVA: 0x00154924 File Offset: 0x00152D24
		internal bool RequestSucceeded()
		{
			return this.GetStatus() > (CommonErrorStatus.ResponseStatus)0;
		}

		// Token: 0x06004C59 RID: 19545 RVA: 0x0015492F File Offset: 0x00152D2F
		internal static GetCaptureStateResponse FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new GetCaptureStateResponse(pointer);
		}
	}
}
