﻿using System;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E36 RID: 3638
	internal class NativeScoreSummary : BaseReferenceHolder
	{
		// Token: 0x06004A9F RID: 19103 RVA: 0x001511AF File Offset: 0x0014F5AF
		internal NativeScoreSummary(IntPtr selfPtr) : base(selfPtr)
		{
		}

		// Token: 0x06004AA0 RID: 19104 RVA: 0x001511B8 File Offset: 0x0014F5B8
		protected override void CallDispose(HandleRef selfPointer)
		{
			ScoreSummary.ScoreSummary_Dispose(selfPointer);
		}

		// Token: 0x06004AA1 RID: 19105 RVA: 0x001511C0 File Offset: 0x0014F5C0
		internal ulong ApproximateResults()
		{
			return ScoreSummary.ScoreSummary_ApproximateNumberOfScores(base.SelfPtr());
		}

		// Token: 0x06004AA2 RID: 19106 RVA: 0x001511CD File Offset: 0x0014F5CD
		internal NativeScore LocalUserScore()
		{
			return NativeScore.FromPointer(ScoreSummary.ScoreSummary_CurrentPlayerScore(base.SelfPtr()));
		}

		// Token: 0x06004AA3 RID: 19107 RVA: 0x001511DF File Offset: 0x0014F5DF
		internal static NativeScoreSummary FromPointer(IntPtr pointer)
		{
			if (pointer.Equals(IntPtr.Zero))
			{
				return null;
			}
			return new NativeScoreSummary(pointer);
		}
	}
}
