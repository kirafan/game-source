﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GooglePlayGames.Native.Cwrapper;

namespace GooglePlayGames.Native.PInvoke
{
	// Token: 0x02000E66 RID: 3686
	internal class TurnBasedMatchConfig : BaseReferenceHolder
	{
		// Token: 0x06004C2F RID: 19503 RVA: 0x00154591 File Offset: 0x00152991
		internal TurnBasedMatchConfig(IntPtr selfPointer) : base(selfPointer)
		{
		}

		// Token: 0x06004C30 RID: 19504 RVA: 0x0015459C File Offset: 0x0015299C
		private string PlayerIdAtIndex(UIntPtr index)
		{
			return PInvokeUtilities.OutParamsToString((byte[] out_string, UIntPtr size) => TurnBasedMatchConfig.TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(this.SelfPtr(), index, out_string, size));
		}

		// Token: 0x06004C31 RID: 19505 RVA: 0x001545CE File Offset: 0x001529CE
		internal IEnumerator<string> PlayerIdsToInvite()
		{
			return PInvokeUtilities.ToEnumerator<string>(TurnBasedMatchConfig.TurnBasedMatchConfig_PlayerIdsToInvite_Length(base.SelfPtr()), new Func<UIntPtr, string>(this.PlayerIdAtIndex));
		}

		// Token: 0x06004C32 RID: 19506 RVA: 0x001545EC File Offset: 0x001529EC
		internal uint Variant()
		{
			return TurnBasedMatchConfig.TurnBasedMatchConfig_Variant(base.SelfPtr());
		}

		// Token: 0x06004C33 RID: 19507 RVA: 0x001545F9 File Offset: 0x001529F9
		internal long ExclusiveBitMask()
		{
			return TurnBasedMatchConfig.TurnBasedMatchConfig_ExclusiveBitMask(base.SelfPtr());
		}

		// Token: 0x06004C34 RID: 19508 RVA: 0x00154606 File Offset: 0x00152A06
		internal uint MinimumAutomatchingPlayers()
		{
			return TurnBasedMatchConfig.TurnBasedMatchConfig_MinimumAutomatchingPlayers(base.SelfPtr());
		}

		// Token: 0x06004C35 RID: 19509 RVA: 0x00154613 File Offset: 0x00152A13
		internal uint MaximumAutomatchingPlayers()
		{
			return TurnBasedMatchConfig.TurnBasedMatchConfig_MaximumAutomatchingPlayers(base.SelfPtr());
		}

		// Token: 0x06004C36 RID: 19510 RVA: 0x00154620 File Offset: 0x00152A20
		protected override void CallDispose(HandleRef selfPointer)
		{
			TurnBasedMatchConfig.TurnBasedMatchConfig_Dispose(selfPointer);
		}
	}
}
