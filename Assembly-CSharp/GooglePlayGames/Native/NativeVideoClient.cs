﻿using System;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Video;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.Native
{
	// Token: 0x02000E0A RID: 3594
	internal class NativeVideoClient : IVideoClient
	{
		// Token: 0x06004919 RID: 18713 RVA: 0x0014E125 File Offset: 0x0014C525
		internal NativeVideoClient(GooglePlayGames.Native.PInvoke.VideoManager manager)
		{
			this.mManager = Misc.CheckNotNull<GooglePlayGames.Native.PInvoke.VideoManager>(manager);
		}

		// Token: 0x0600491A RID: 18714 RVA: 0x0014E13C File Offset: 0x0014C53C
		public void GetCaptureCapabilities(Action<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCapabilities> callback)
		{
			Misc.CheckNotNull<Action<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCapabilities>>(callback);
			callback = CallbackUtils.ToOnGameThread<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCapabilities>(callback);
			this.mManager.GetCaptureCapabilities(delegate(GetCaptureCapabilitiesResponse response)
			{
				ResponseStatus arg = ConversionUtils.ConvertResponseStatus(response.GetStatus());
				if (!response.RequestSucceeded())
				{
					callback(arg, null);
				}
				else
				{
					callback(arg, this.FromNativeVideoCapabilities(response.GetData()));
				}
			});
		}

		// Token: 0x0600491B RID: 18715 RVA: 0x0014E194 File Offset: 0x0014C594
		private GooglePlayGames.BasicApi.Video.VideoCapabilities FromNativeVideoCapabilities(NativeVideoCapabilities capabilities)
		{
			bool[] array = new bool[this.mManager.NumCaptureModes];
			array[0] = capabilities.SupportsCaptureMode(Types.VideoCaptureMode.FILE);
			array[1] = capabilities.SupportsCaptureMode(Types.VideoCaptureMode.STREAM);
			bool[] array2 = new bool[this.mManager.NumQualityLevels];
			array2[0] = capabilities.SupportsQualityLevel(Types.VideoQualityLevel.SD);
			array2[1] = capabilities.SupportsQualityLevel(Types.VideoQualityLevel.HD);
			array2[2] = capabilities.SupportsQualityLevel(Types.VideoQualityLevel.XHD);
			array2[3] = capabilities.SupportsQualityLevel(Types.VideoQualityLevel.FULLHD);
			return new GooglePlayGames.BasicApi.Video.VideoCapabilities(capabilities.IsCameraSupported(), capabilities.IsMicSupported(), capabilities.IsWriteStorageSupported(), array, array2);
		}

		// Token: 0x0600491C RID: 18716 RVA: 0x0014E218 File Offset: 0x0014C618
		public void ShowCaptureOverlay()
		{
			this.mManager.ShowCaptureOverlay();
		}

		// Token: 0x0600491D RID: 18717 RVA: 0x0014E228 File Offset: 0x0014C628
		public void GetCaptureState(Action<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCaptureState> callback)
		{
			Misc.CheckNotNull<Action<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCaptureState>>(callback);
			callback = CallbackUtils.ToOnGameThread<ResponseStatus, GooglePlayGames.BasicApi.Video.VideoCaptureState>(callback);
			this.mManager.GetCaptureState(delegate(GetCaptureStateResponse response)
			{
				ResponseStatus arg = ConversionUtils.ConvertResponseStatus(response.GetStatus());
				if (!response.RequestSucceeded())
				{
					callback(arg, null);
				}
				else
				{
					callback(arg, this.FromNativeVideoCaptureState(response.GetData()));
				}
			});
		}

		// Token: 0x0600491E RID: 18718 RVA: 0x0014E27D File Offset: 0x0014C67D
		private GooglePlayGames.BasicApi.Video.VideoCaptureState FromNativeVideoCaptureState(NativeVideoCaptureState captureState)
		{
			return new GooglePlayGames.BasicApi.Video.VideoCaptureState(captureState.IsCapturing(), ConversionUtils.ConvertNativeVideoCaptureMode(captureState.CaptureMode()), ConversionUtils.ConvertNativeVideoQualityLevel(captureState.QualityLevel()), captureState.IsOverlayVisible(), captureState.IsPaused());
		}

		// Token: 0x0600491F RID: 18719 RVA: 0x0014E2AC File Offset: 0x0014C6AC
		public void IsCaptureAvailable(VideoCaptureMode captureMode, Action<ResponseStatus, bool> callback)
		{
			Misc.CheckNotNull<Action<ResponseStatus, bool>>(callback);
			callback = CallbackUtils.ToOnGameThread<ResponseStatus, bool>(callback);
			this.mManager.IsCaptureAvailable(ConversionUtils.ConvertVideoCaptureMode(captureMode), delegate(IsCaptureAvailableResponse response)
			{
				ResponseStatus arg = ConversionUtils.ConvertResponseStatus(response.GetStatus());
				if (!response.RequestSucceeded())
				{
					callback(arg, false);
				}
				else
				{
					callback(arg, response.IsCaptureAvailable());
				}
			});
		}

		// Token: 0x06004920 RID: 18720 RVA: 0x0014E300 File Offset: 0x0014C700
		public bool IsCaptureSupported()
		{
			return this.mManager.IsCaptureSupported();
		}

		// Token: 0x06004921 RID: 18721 RVA: 0x0014E310 File Offset: 0x0014C710
		public void RegisterCaptureOverlayStateChangedListener(CaptureOverlayStateListener listener)
		{
			Misc.CheckNotNull<CaptureOverlayStateListener>(listener);
			GooglePlayGames.Native.PInvoke.CaptureOverlayStateListenerHelper helper = GooglePlayGames.Native.PInvoke.CaptureOverlayStateListenerHelper.Create().SetOnCaptureOverlayStateChangedCallback(delegate(Types.VideoCaptureOverlayState response)
			{
				listener.OnCaptureOverlayStateChanged(ConversionUtils.ConvertNativeVideoCaptureOverlayState(response));
			});
			this.mManager.RegisterCaptureOverlayStateChangedListener(helper);
		}

		// Token: 0x06004922 RID: 18722 RVA: 0x0014E359 File Offset: 0x0014C759
		public void UnregisterCaptureOverlayStateChangedListener()
		{
			this.mManager.UnregisterCaptureOverlayStateChangedListener();
		}

		// Token: 0x04004CDB RID: 19675
		private readonly GooglePlayGames.Native.PInvoke.VideoManager mManager;
	}
}
