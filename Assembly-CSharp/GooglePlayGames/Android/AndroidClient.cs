﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Com.Google.Android.Gms.Games;
using Com.Google.Android.Gms.Games.Stats;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.PInvoke;
using GooglePlayGames.OurUtils;
using UnityEngine;

namespace GooglePlayGames.Android
{
	// Token: 0x02000D36 RID: 3382
	internal class AndroidClient : IClientImpl
	{
		// Token: 0x060043E2 RID: 17378 RVA: 0x00144AE4 File Offset: 0x00142EE4
		public PlatformConfiguration CreatePlatformConfiguration(PlayGamesClientConfiguration clientConfig)
		{
			AndroidPlatformConfiguration androidPlatformConfiguration = AndroidPlatformConfiguration.Create();
			using (AndroidJavaObject activity = AndroidTokenClient.GetActivity())
			{
				androidPlatformConfiguration.SetActivity(activity.GetRawObject());
				androidPlatformConfiguration.SetOptionalIntentHandlerForUI(delegate(IntPtr intent)
				{
					IntPtr intentRef = AndroidJNI.NewGlobalRef(intent);
					PlayGamesHelperObject.RunOnGameThread(delegate
					{
						try
						{
							AndroidClient.LaunchBridgeIntent(intentRef);
						}
						finally
						{
							AndroidJNI.DeleteGlobalRef(intentRef);
						}
					});
				});
				if (clientConfig.IsHidingPopups)
				{
					androidPlatformConfiguration.SetOptionalViewForPopups(this.CreateHiddenView(activity.GetRawObject()));
				}
			}
			return androidPlatformConfiguration;
		}

		// Token: 0x060043E3 RID: 17379 RVA: 0x00144B70 File Offset: 0x00142F70
		public TokenClient CreateTokenClient(bool reset)
		{
			if (this.tokenClient == null)
			{
				this.tokenClient = new AndroidTokenClient();
			}
			else if (reset)
			{
				this.tokenClient.Signout();
			}
			return this.tokenClient;
		}

		// Token: 0x060043E4 RID: 17380 RVA: 0x00144BA4 File Offset: 0x00142FA4
		private IntPtr CreateHiddenView(IntPtr activity)
		{
			if (AndroidClient.invisible == null || AndroidClient.invisible.GetRawObject() == IntPtr.Zero)
			{
				AndroidClient.invisible = new AndroidJavaObject("android.view.View", new object[]
				{
					activity
				});
				AndroidClient.invisible.Call("setVisibility", new object[]
				{
					4
				});
				AndroidClient.invisible.Call("setClickable", new object[]
				{
					false
				});
			}
			return AndroidClient.invisible.GetRawObject();
		}

		// Token: 0x060043E5 RID: 17381 RVA: 0x00144C38 File Offset: 0x00143038
		private static void LaunchBridgeIntent(IntPtr bridgedIntent)
		{
			object[] args = new object[2];
			jvalue[] array = AndroidJNIHelper.CreateJNIArgArray(args);
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.google.games.bridge.NativeBridgeActivity"))
				{
					using (AndroidJavaObject activity = AndroidTokenClient.GetActivity())
					{
						IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(androidJavaClass.GetRawClass(), "launchBridgeIntent", "(Landroid/app/Activity;Landroid/content/Intent;)V");
						array[0].l = activity.GetRawObject();
						array[1].l = bridgedIntent;
						AndroidJNI.CallStaticVoidMethod(androidJavaClass.GetRawClass(), staticMethodID, array);
					}
				}
			}
			catch (Exception ex)
			{
				GooglePlayGames.OurUtils.Logger.e("Exception launching bridge intent: " + ex.Message);
				GooglePlayGames.OurUtils.Logger.e(ex.ToString());
			}
			finally
			{
				AndroidJNIHelper.DeleteJNIArgArray(args, array);
			}
		}

		// Token: 0x060043E6 RID: 17382 RVA: 0x00144D38 File Offset: 0x00143138
		public void Signout()
		{
			if (this.tokenClient != null)
			{
				this.tokenClient.Signout();
			}
		}

		// Token: 0x060043E7 RID: 17383 RVA: 0x00144D50 File Offset: 0x00143150
		public void GetPlayerStats(IntPtr apiClient, Action<CommonStatusCodes, GooglePlayGames.BasicApi.PlayerStats> callback)
		{
			GoogleApiClient arg_GoogleApiClient_ = new GoogleApiClient(apiClient);
			AndroidClient.StatsResultCallback resultCallback;
			try
			{
				resultCallback = new AndroidClient.StatsResultCallback(delegate(int result, Com.Google.Android.Gms.Games.Stats.PlayerStats stats)
				{
					Debug.Log("Result for getStats: " + result);
					GooglePlayGames.BasicApi.PlayerStats playerStats = null;
					if (stats != null)
					{
						playerStats = new GooglePlayGames.BasicApi.PlayerStats();
						playerStats.AvgSessonLength = stats.getAverageSessionLength();
						playerStats.DaysSinceLastPlayed = stats.getDaysSinceLastPlayed();
						playerStats.NumberOfPurchases = stats.getNumberOfPurchases();
						playerStats.NumberOfSessions = stats.getNumberOfSessions();
						playerStats.SessPercentile = stats.getSessionPercentile();
						playerStats.SpendPercentile = stats.getSpendPercentile();
						playerStats.ChurnProbability = stats.getChurnProbability();
						playerStats.SpendProbability = stats.getSpendProbability();
						playerStats.HighSpenderProbability = stats.getHighSpenderProbability();
						playerStats.TotalSpendNext28Days = stats.getTotalSpendNext28Days();
					}
					callback((CommonStatusCodes)result, playerStats);
				});
			}
			catch (Exception exception)
			{
				Debug.LogException(exception);
				callback(CommonStatusCodes.DeveloperError, null);
				return;
			}
			PendingResult<Stats_LoadPlayerStatsResultObject> pendingResult = Games.Stats.loadPlayerStats(arg_GoogleApiClient_, true);
			pendingResult.setResultCallback(resultCallback);
		}

		// Token: 0x060043E8 RID: 17384 RVA: 0x00144DC8 File Offset: 0x001431C8
		public void SetGravityForPopups(IntPtr apiClient, Gravity gravity)
		{
			GoogleApiClient arg_GoogleApiClient_ = new GoogleApiClient(apiClient);
			Games.setGravityForPopups(arg_GoogleApiClient_, (int)(gravity | Gravity.CENTER_HORIZONTAL));
		}

		// Token: 0x04004B60 RID: 19296
		internal const string BridgeActivityClass = "com.google.games.bridge.NativeBridgeActivity";

		// Token: 0x04004B61 RID: 19297
		private const string LaunchBridgeMethod = "launchBridgeIntent";

		// Token: 0x04004B62 RID: 19298
		private const string LaunchBridgeSignature = "(Landroid/app/Activity;Landroid/content/Intent;)V";

		// Token: 0x04004B63 RID: 19299
		private TokenClient tokenClient;

		// Token: 0x04004B64 RID: 19300
		private static AndroidJavaObject invisible;

		// Token: 0x02000D37 RID: 3383
		private class StatsResultCallback : ResultCallbackProxy<Stats_LoadPlayerStatsResultObject>
		{
			// Token: 0x060043EA RID: 17386 RVA: 0x00144ED3 File Offset: 0x001432D3
			public StatsResultCallback(Action<int, Com.Google.Android.Gms.Games.Stats.PlayerStats> callback)
			{
				this.callback = callback;
			}

			// Token: 0x060043EB RID: 17387 RVA: 0x00144EE2 File Offset: 0x001432E2
			public override void OnResult(Stats_LoadPlayerStatsResultObject arg_Result_1)
			{
				this.callback(arg_Result_1.getStatus().getStatusCode(), arg_Result_1.getPlayerStats());
			}

			// Token: 0x04004B66 RID: 19302
			private Action<int, Com.Google.Android.Gms.Games.Stats.PlayerStats> callback;
		}
	}
}
