﻿using System;
using Com.Google.Android.Gms.Common.Api;
using Google.Developers;

namespace GooglePlayGames.Android
{
	// Token: 0x02000D39 RID: 3385
	internal class TokenResult : JavaObjWrapper, Result
	{
		// Token: 0x060043FE RID: 17406 RVA: 0x00145D99 File Offset: 0x00144199
		public TokenResult(IntPtr ptr) : base(ptr)
		{
		}

		// Token: 0x060043FF RID: 17407 RVA: 0x00145DA4 File Offset: 0x001441A4
		public Status getStatus()
		{
			IntPtr ptr = base.InvokeCall<IntPtr>("getStatus", "()Lcom/google/android/gms/common/api/Status;", new object[0]);
			return new Status(ptr);
		}

		// Token: 0x06004400 RID: 17408 RVA: 0x00145DCE File Offset: 0x001441CE
		public int getStatusCode()
		{
			return base.InvokeCall<int>("getStatusCode", "()I", new object[0]);
		}

		// Token: 0x06004401 RID: 17409 RVA: 0x00145DE6 File Offset: 0x001441E6
		public string getAuthCode()
		{
			return base.InvokeCall<string>("getAuthCode", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x06004402 RID: 17410 RVA: 0x00145DFE File Offset: 0x001441FE
		public string getEmail()
		{
			return base.InvokeCall<string>("getEmail", "()Ljava/lang/String;", new object[0]);
		}

		// Token: 0x06004403 RID: 17411 RVA: 0x00145E16 File Offset: 0x00144216
		public string getIdToken()
		{
			return base.InvokeCall<string>("getIdToken", "()Ljava/lang/String;", new object[0]);
		}
	}
}
