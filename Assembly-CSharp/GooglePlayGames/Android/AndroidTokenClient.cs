﻿using System;
using System.Collections.Generic;
using Com.Google.Android.Gms.Common.Api;
using GooglePlayGames.OurUtils;
using UnityEngine;

namespace GooglePlayGames.Android
{
	// Token: 0x02000D38 RID: 3384
	internal class AndroidTokenClient : TokenClient
	{
		// Token: 0x060043ED RID: 17389 RVA: 0x0014500C File Offset: 0x0014340C
		public static AndroidJavaObject GetActivity()
		{
			AndroidJavaObject @static;
			using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			{
				@static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return @static;
		}

		// Token: 0x060043EE RID: 17390 RVA: 0x00145054 File Offset: 0x00143454
		public void SetRequestAuthCode(bool flag, bool forceRefresh)
		{
			this.requestAuthCode = flag;
			this.forceRefresh = forceRefresh;
		}

		// Token: 0x060043EF RID: 17391 RVA: 0x00145064 File Offset: 0x00143464
		public void SetRequestEmail(bool flag)
		{
			this.requestEmail = flag;
		}

		// Token: 0x060043F0 RID: 17392 RVA: 0x0014506D File Offset: 0x0014346D
		public void SetRequestIdToken(bool flag)
		{
			this.requestIdToken = flag;
		}

		// Token: 0x060043F1 RID: 17393 RVA: 0x00145076 File Offset: 0x00143476
		public void SetWebClientId(string webClientId)
		{
			this.webClientId = webClientId;
		}

		// Token: 0x060043F2 RID: 17394 RVA: 0x0014507F File Offset: 0x0014347F
		public void SetHidePopups(bool flag)
		{
			this.hidePopups = flag;
		}

		// Token: 0x060043F3 RID: 17395 RVA: 0x00145088 File Offset: 0x00143488
		public void SetAccountName(string accountName)
		{
			this.accountName = accountName;
		}

		// Token: 0x060043F4 RID: 17396 RVA: 0x00145091 File Offset: 0x00143491
		public void AddOauthScopes(string[] scopes)
		{
			if (scopes != null)
			{
				if (this.oauthScopes == null)
				{
					this.oauthScopes = new List<string>();
				}
				this.oauthScopes.AddRange(scopes);
			}
		}

		// Token: 0x060043F5 RID: 17397 RVA: 0x001450BB File Offset: 0x001434BB
		public void Signout()
		{
			this.authCode = null;
			this.email = null;
			this.idToken = null;
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				Debug.Log("Calling Signout in token client");
				AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.google.games.bridge.TokenFragment");
				androidJavaClass.CallStatic("signOut", new object[]
				{
					AndroidTokenClient.GetActivity()
				});
			});
		}

		// Token: 0x060043F6 RID: 17398 RVA: 0x001450F4 File Offset: 0x001434F4
		public bool NeedsToRun()
		{
			return this.requestAuthCode || this.requestEmail || this.requestIdToken;
		}

		// Token: 0x060043F7 RID: 17399 RVA: 0x00145118 File Offset: 0x00143518
		public void FetchTokens(Action<int> callback)
		{
			PlayGamesHelperObject.RunOnGameThread(delegate
			{
				this.DoFetchToken(callback);
			});
		}

		// Token: 0x060043F8 RID: 17400 RVA: 0x0014514C File Offset: 0x0014354C
		internal void DoFetchToken(Action<int> callback)
		{
			object[] args = new object[9];
			jvalue[] array = AndroidJNIHelper.CreateJNIArgArray(args);
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.google.games.bridge.TokenFragment"))
				{
					using (AndroidJavaObject activity = AndroidTokenClient.GetActivity())
					{
						IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(androidJavaClass.GetRawClass(), "fetchToken", "(Landroid/app/Activity;ZZZLjava/lang/String;Z[Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;");
						array[0].l = activity.GetRawObject();
						array[1].z = this.requestAuthCode;
						array[2].z = this.requestEmail;
						array[3].z = this.requestIdToken;
						array[4].l = AndroidJNI.NewStringUTF(this.webClientId);
						array[5].z = this.forceRefresh;
						array[6].l = AndroidJNIHelper.ConvertToJNIArray(this.oauthScopes.ToArray());
						array[7].z = this.hidePopups;
						array[8].l = AndroidJNI.NewStringUTF(this.accountName);
						IntPtr ptr = AndroidJNI.CallStaticObjectMethod(androidJavaClass.GetRawClass(), staticMethodID, array);
						PendingResult<TokenResult> pendingResult = new PendingResult<TokenResult>(ptr);
						pendingResult.setResultCallback(new TokenResultCallback(delegate(int rc, string authCode, string email, string idToken)
						{
							this.authCode = authCode;
							this.email = email;
							this.idToken = idToken;
							callback(rc);
						}));
					}
				}
			}
			catch (Exception ex)
			{
				GooglePlayGames.OurUtils.Logger.e("Exception launching token request: " + ex.Message);
				GooglePlayGames.OurUtils.Logger.e(ex.ToString());
			}
			finally
			{
				AndroidJNIHelper.DeleteJNIArgArray(args, array);
			}
		}

		// Token: 0x060043F9 RID: 17401 RVA: 0x00145350 File Offset: 0x00143750
		public string GetEmail()
		{
			return this.email;
		}

		// Token: 0x060043FA RID: 17402 RVA: 0x00145358 File Offset: 0x00143758
		public string GetAuthCode()
		{
			return this.authCode;
		}

		// Token: 0x060043FB RID: 17403 RVA: 0x00145360 File Offset: 0x00143760
		public void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback)
		{
			object[] args = new object[3];
			jvalue[] array = AndroidJNIHelper.CreateJNIArgArray(args);
			try
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.google.games.bridge.TokenFragment"))
				{
					using (AndroidJavaObject activity = AndroidTokenClient.GetActivity())
					{
						IntPtr staticMethodID = AndroidJNI.GetStaticMethodID(androidJavaClass.GetRawClass(), "getAnotherAuthCode", "(Landroid/app/Activity;ZLjava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;");
						array[0].l = activity.GetRawObject();
						array[1].z = reAuthenticateIfNeeded;
						array[2].l = AndroidJNI.NewStringUTF(this.webClientId);
						IntPtr ptr = AndroidJNI.CallStaticObjectMethod(androidJavaClass.GetRawClass(), staticMethodID, array);
						PendingResult<TokenResult> pendingResult = new PendingResult<TokenResult>(ptr);
						pendingResult.setResultCallback(new TokenResultCallback(delegate(int rc, string authCode, string email, string idToken)
						{
							this.authCode = authCode;
							callback(authCode);
						}));
					}
				}
			}
			catch (Exception ex)
			{
				GooglePlayGames.OurUtils.Logger.e("Exception launching auth code request: " + ex.Message);
				GooglePlayGames.OurUtils.Logger.e(ex.ToString());
			}
			finally
			{
				AndroidJNIHelper.DeleteJNIArgArray(args, array);
			}
		}

		// Token: 0x060043FC RID: 17404 RVA: 0x001454B0 File Offset: 0x001438B0
		public string GetIdToken()
		{
			return this.idToken;
		}

		// Token: 0x04004B67 RID: 19303
		private const string TokenFragmentClass = "com.google.games.bridge.TokenFragment";

		// Token: 0x04004B68 RID: 19304
		private const string FetchTokenSignature = "(Landroid/app/Activity;ZZZLjava/lang/String;Z[Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;";

		// Token: 0x04004B69 RID: 19305
		private const string FetchTokenMethod = "fetchToken";

		// Token: 0x04004B6A RID: 19306
		private const string GetAnotherAuthCodeMethod = "getAnotherAuthCode";

		// Token: 0x04004B6B RID: 19307
		private const string GetAnotherAuthCodeSignature = "(Landroid/app/Activity;ZLjava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;";

		// Token: 0x04004B6C RID: 19308
		private bool requestEmail;

		// Token: 0x04004B6D RID: 19309
		private bool requestAuthCode;

		// Token: 0x04004B6E RID: 19310
		private bool requestIdToken;

		// Token: 0x04004B6F RID: 19311
		private List<string> oauthScopes;

		// Token: 0x04004B70 RID: 19312
		private string webClientId;

		// Token: 0x04004B71 RID: 19313
		private bool forceRefresh;

		// Token: 0x04004B72 RID: 19314
		private bool hidePopups;

		// Token: 0x04004B73 RID: 19315
		private string accountName;

		// Token: 0x04004B74 RID: 19316
		private string email;

		// Token: 0x04004B75 RID: 19317
		private string authCode;

		// Token: 0x04004B76 RID: 19318
		private string idToken;
	}
}
