﻿using System;
using Com.Google.Android.Gms.Common.Api;

namespace GooglePlayGames.Android
{
	// Token: 0x02000D3A RID: 3386
	internal class TokenResultCallback : ResultCallbackProxy<TokenResult>
	{
		// Token: 0x06004404 RID: 17412 RVA: 0x00145E2E File Offset: 0x0014422E
		public TokenResultCallback(Action<int, string, string, string> callback)
		{
			this.callback = callback;
		}

		// Token: 0x06004405 RID: 17413 RVA: 0x00145E3D File Offset: 0x0014423D
		public override void OnResult(TokenResult arg_Result_1)
		{
			if (this.callback != null)
			{
				this.callback(arg_Result_1.getStatusCode(), arg_Result_1.getAuthCode(), arg_Result_1.getEmail(), arg_Result_1.getIdToken());
			}
		}

		// Token: 0x06004406 RID: 17414 RVA: 0x00145E6D File Offset: 0x0014426D
		public string toString()
		{
			return this.ToString();
		}

		// Token: 0x04004B78 RID: 19320
		private Action<int, string, string, string> callback;
	}
}
