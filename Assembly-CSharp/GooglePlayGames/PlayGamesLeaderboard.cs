﻿using System;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D2D RID: 3373
	public class PlayGamesLeaderboard : ILeaderboard
	{
		// Token: 0x06004341 RID: 17217 RVA: 0x00142D3A File Offset: 0x0014113A
		public PlayGamesLeaderboard(string id)
		{
			this.mId = id;
		}

		// Token: 0x06004342 RID: 17218 RVA: 0x00142D54 File Offset: 0x00141154
		public void SetUserFilter(string[] userIDs)
		{
			this.mFilteredUserIds = userIDs;
		}

		// Token: 0x06004343 RID: 17219 RVA: 0x00142D5D File Offset: 0x0014115D
		public void LoadScores(Action<bool> callback)
		{
			PlayGamesPlatform.Instance.LoadScores(this, callback);
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06004344 RID: 17220 RVA: 0x00142D6B File Offset: 0x0014116B
		// (set) Token: 0x06004345 RID: 17221 RVA: 0x00142D73 File Offset: 0x00141173
		public bool loading
		{
			get
			{
				return this.mLoading;
			}
			internal set
			{
				this.mLoading = value;
			}
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x06004346 RID: 17222 RVA: 0x00142D7C File Offset: 0x0014117C
		// (set) Token: 0x06004347 RID: 17223 RVA: 0x00142D84 File Offset: 0x00141184
		public string id
		{
			get
			{
				return this.mId;
			}
			set
			{
				this.mId = value;
			}
		}

		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x06004348 RID: 17224 RVA: 0x00142D8D File Offset: 0x0014118D
		// (set) Token: 0x06004349 RID: 17225 RVA: 0x00142D95 File Offset: 0x00141195
		public UserScope userScope
		{
			get
			{
				return this.mUserScope;
			}
			set
			{
				this.mUserScope = value;
			}
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x0600434A RID: 17226 RVA: 0x00142D9E File Offset: 0x0014119E
		// (set) Token: 0x0600434B RID: 17227 RVA: 0x00142DA6 File Offset: 0x001411A6
		public Range range
		{
			get
			{
				return this.mRange;
			}
			set
			{
				this.mRange = value;
			}
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x0600434C RID: 17228 RVA: 0x00142DAF File Offset: 0x001411AF
		// (set) Token: 0x0600434D RID: 17229 RVA: 0x00142DB7 File Offset: 0x001411B7
		public TimeScope timeScope
		{
			get
			{
				return this.mTimeScope;
			}
			set
			{
				this.mTimeScope = value;
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x0600434E RID: 17230 RVA: 0x00142DC0 File Offset: 0x001411C0
		public IScore localUserScore
		{
			get
			{
				return this.mLocalUserScore;
			}
		}

		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x0600434F RID: 17231 RVA: 0x00142DC8 File Offset: 0x001411C8
		public uint maxRange
		{
			get
			{
				return this.mMaxRange;
			}
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06004350 RID: 17232 RVA: 0x00142DD0 File Offset: 0x001411D0
		public IScore[] scores
		{
			get
			{
				PlayGamesScore[] array = new PlayGamesScore[this.mScoreList.Count];
				this.mScoreList.CopyTo(array);
				return array;
			}
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06004351 RID: 17233 RVA: 0x00142DFB File Offset: 0x001411FB
		public string title
		{
			get
			{
				return this.mTitle;
			}
		}

		// Token: 0x06004352 RID: 17234 RVA: 0x00142E04 File Offset: 0x00141204
		internal bool SetFromData(LeaderboardScoreData data)
		{
			if (data.Valid)
			{
				Debug.Log("Setting leaderboard from: " + data);
				this.SetMaxRange(data.ApproximateCount);
				this.SetTitle(data.Title);
				this.SetLocalUserScore((PlayGamesScore)data.PlayerScore);
				foreach (IScore score in data.Scores)
				{
					this.AddScore((PlayGamesScore)score);
				}
				this.mLoading = (data.Scores.Length == 0 || this.HasAllScores());
			}
			return data.Valid;
		}

		// Token: 0x06004353 RID: 17235 RVA: 0x00142EA3 File Offset: 0x001412A3
		internal void SetMaxRange(ulong val)
		{
			this.mMaxRange = (uint)val;
		}

		// Token: 0x06004354 RID: 17236 RVA: 0x00142EAD File Offset: 0x001412AD
		internal void SetTitle(string value)
		{
			this.mTitle = value;
		}

		// Token: 0x06004355 RID: 17237 RVA: 0x00142EB6 File Offset: 0x001412B6
		internal void SetLocalUserScore(PlayGamesScore score)
		{
			this.mLocalUserScore = score;
		}

		// Token: 0x06004356 RID: 17238 RVA: 0x00142EC0 File Offset: 0x001412C0
		internal int AddScore(PlayGamesScore score)
		{
			if (this.mFilteredUserIds == null || this.mFilteredUserIds.Length == 0)
			{
				this.mScoreList.Add(score);
			}
			else
			{
				foreach (string text in this.mFilteredUserIds)
				{
					if (text.Equals(score.userID))
					{
						return this.mScoreList.Count;
					}
				}
				this.mScoreList.Add(score);
			}
			return this.mScoreList.Count;
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x06004357 RID: 17239 RVA: 0x00142F49 File Offset: 0x00141349
		public int ScoreCount
		{
			get
			{
				return this.mScoreList.Count;
			}
		}

		// Token: 0x06004358 RID: 17240 RVA: 0x00142F56 File Offset: 0x00141356
		internal bool HasAllScores()
		{
			return this.mScoreList.Count >= this.mRange.count || (long)this.mScoreList.Count >= (long)((ulong)this.maxRange);
		}

		// Token: 0x04004B37 RID: 19255
		private string mId;

		// Token: 0x04004B38 RID: 19256
		private UserScope mUserScope;

		// Token: 0x04004B39 RID: 19257
		private Range mRange;

		// Token: 0x04004B3A RID: 19258
		private TimeScope mTimeScope;

		// Token: 0x04004B3B RID: 19259
		private string[] mFilteredUserIds;

		// Token: 0x04004B3C RID: 19260
		private bool mLoading;

		// Token: 0x04004B3D RID: 19261
		private IScore mLocalUserScore;

		// Token: 0x04004B3E RID: 19262
		private uint mMaxRange;

		// Token: 0x04004B3F RID: 19263
		private List<PlayGamesScore> mScoreList = new List<PlayGamesScore>();

		// Token: 0x04004B40 RID: 19264
		private string mTitle;
	}
}
