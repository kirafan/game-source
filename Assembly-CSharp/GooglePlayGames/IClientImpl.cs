﻿using System;
using GooglePlayGames.BasicApi;
using GooglePlayGames.Native.PInvoke;

namespace GooglePlayGames
{
	// Token: 0x02000D4C RID: 3404
	internal interface IClientImpl
	{
		// Token: 0x060044B5 RID: 17589
		PlatformConfiguration CreatePlatformConfiguration(PlayGamesClientConfiguration clientConfig);

		// Token: 0x060044B6 RID: 17590
		TokenClient CreateTokenClient(bool reset);

		// Token: 0x060044B7 RID: 17591
		void GetPlayerStats(IntPtr apiClientPtr, Action<CommonStatusCodes, PlayerStats> callback);

		// Token: 0x060044B8 RID: 17592
		void SetGravityForPopups(IntPtr apiClient, Gravity gravity);
	}
}
