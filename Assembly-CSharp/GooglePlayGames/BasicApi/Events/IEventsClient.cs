﻿using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.Events
{
	// Token: 0x02000CF3 RID: 3315
	public interface IEventsClient
	{
		// Token: 0x060041B4 RID: 16820
		void FetchAllEvents(DataSource source, Action<ResponseStatus, List<IEvent>> callback);

		// Token: 0x060041B5 RID: 16821
		void FetchEvent(DataSource source, string eventId, Action<ResponseStatus, IEvent> callback);

		// Token: 0x060041B6 RID: 16822
		void IncrementEvent(string eventId, uint stepsToIncrement);
	}
}
