﻿using System;

namespace GooglePlayGames.BasicApi.Events
{
	// Token: 0x02000CF1 RID: 3313
	public enum EventVisibility
	{
		// Token: 0x04004A41 RID: 19009
		Hidden = 1,
		// Token: 0x04004A42 RID: 19010
		Revealed
	}
}
