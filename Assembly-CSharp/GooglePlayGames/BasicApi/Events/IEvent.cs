﻿using System;

namespace GooglePlayGames.BasicApi.Events
{
	// Token: 0x02000CF2 RID: 3314
	public interface IEvent
	{
		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x060041AE RID: 16814
		string Id { get; }

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x060041AF RID: 16815
		string Name { get; }

		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x060041B0 RID: 16816
		string Description { get; }

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x060041B1 RID: 16817
		string ImageUrl { get; }

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x060041B2 RID: 16818
		ulong CurrentCount { get; }

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x060041B3 RID: 16819
		EventVisibility Visibility { get; }
	}
}
