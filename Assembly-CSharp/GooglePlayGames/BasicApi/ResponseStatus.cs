﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE6 RID: 3302
	public enum ResponseStatus
	{
		// Token: 0x04004A10 RID: 18960
		Success = 1,
		// Token: 0x04004A11 RID: 18961
		SuccessWithStale,
		// Token: 0x04004A12 RID: 18962
		LicenseCheckFailed = -1,
		// Token: 0x04004A13 RID: 18963
		InternalError = -2,
		// Token: 0x04004A14 RID: 18964
		NotAuthorized = -3,
		// Token: 0x04004A15 RID: 18965
		VersionUpdateRequired = -4,
		// Token: 0x04004A16 RID: 18966
		Timeout = -5
	}
}
