﻿using System;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.BasicApi.Video;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CF4 RID: 3316
	public interface IPlayGamesClient
	{
		// Token: 0x060041B7 RID: 16823
		void Authenticate(Action<bool, string> callback, bool silent);

		// Token: 0x060041B8 RID: 16824
		bool IsAuthenticated();

		// Token: 0x060041B9 RID: 16825
		void SignOut();

		// Token: 0x060041BA RID: 16826
		string GetUserId();

		// Token: 0x060041BB RID: 16827
		void LoadFriends(Action<bool> callback);

		// Token: 0x060041BC RID: 16828
		string GetUserDisplayName();

		// Token: 0x060041BD RID: 16829
		string GetIdToken();

		// Token: 0x060041BE RID: 16830
		string GetServerAuthCode();

		// Token: 0x060041BF RID: 16831
		void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback);

		// Token: 0x060041C0 RID: 16832
		string GetUserEmail();

		// Token: 0x060041C1 RID: 16833
		string GetUserImageUrl();

		// Token: 0x060041C2 RID: 16834
		void GetPlayerStats(Action<CommonStatusCodes, PlayerStats> callback);

		// Token: 0x060041C3 RID: 16835
		void LoadUsers(string[] userIds, Action<IUserProfile[]> callback);

		// Token: 0x060041C4 RID: 16836
		Achievement GetAchievement(string achievementId);

		// Token: 0x060041C5 RID: 16837
		void LoadAchievements(Action<Achievement[]> callback);

		// Token: 0x060041C6 RID: 16838
		void UnlockAchievement(string achievementId, Action<bool> successOrFailureCalllback);

		// Token: 0x060041C7 RID: 16839
		void RevealAchievement(string achievementId, Action<bool> successOrFailureCalllback);

		// Token: 0x060041C8 RID: 16840
		void IncrementAchievement(string achievementId, int steps, Action<bool> successOrFailureCalllback);

		// Token: 0x060041C9 RID: 16841
		void SetStepsAtLeast(string achId, int steps, Action<bool> callback);

		// Token: 0x060041CA RID: 16842
		void ShowAchievementsUI(Action<UIStatus> callback);

		// Token: 0x060041CB RID: 16843
		void ShowLeaderboardUI(string leaderboardId, LeaderboardTimeSpan span, Action<UIStatus> callback);

		// Token: 0x060041CC RID: 16844
		void LoadScores(string leaderboardId, LeaderboardStart start, int rowCount, LeaderboardCollection collection, LeaderboardTimeSpan timeSpan, Action<LeaderboardScoreData> callback);

		// Token: 0x060041CD RID: 16845
		void LoadMoreScores(ScorePageToken token, int rowCount, Action<LeaderboardScoreData> callback);

		// Token: 0x060041CE RID: 16846
		int LeaderboardMaxResults();

		// Token: 0x060041CF RID: 16847
		void SubmitScore(string leaderboardId, long score, Action<bool> successOrFailureCalllback);

		// Token: 0x060041D0 RID: 16848
		void SubmitScore(string leaderboardId, long score, string metadata, Action<bool> successOrFailureCalllback);

		// Token: 0x060041D1 RID: 16849
		IRealTimeMultiplayerClient GetRtmpClient();

		// Token: 0x060041D2 RID: 16850
		ITurnBasedMultiplayerClient GetTbmpClient();

		// Token: 0x060041D3 RID: 16851
		ISavedGameClient GetSavedGameClient();

		// Token: 0x060041D4 RID: 16852
		IEventsClient GetEventsClient();

		// Token: 0x060041D5 RID: 16853
		[Obsolete("Quests are being removed in 2018.")]
		IQuestsClient GetQuestsClient();

		// Token: 0x060041D6 RID: 16854
		IVideoClient GetVideoClient();

		// Token: 0x060041D7 RID: 16855
		void RegisterInvitationDelegate(InvitationReceivedDelegate invitationDelegate);

		// Token: 0x060041D8 RID: 16856
		IUserProfile[] GetFriends();

		// Token: 0x060041D9 RID: 16857
		IntPtr GetApiClient();

		// Token: 0x060041DA RID: 16858
		void SetGravityForPopups(Gravity gravity);
	}
}
