﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CFE RID: 3326
	public class Participant : IComparable<Participant>
	{
		// Token: 0x06004229 RID: 16937 RVA: 0x00141940 File Offset: 0x0013FD40
		internal Participant(string displayName, string participantId, Participant.ParticipantStatus status, Player player, bool connectedToRoom)
		{
			this.mDisplayName = displayName;
			this.mParticipantId = participantId;
			this.mStatus = status;
			this.mPlayer = player;
			this.mIsConnectedToRoom = connectedToRoom;
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x0600422A RID: 16938 RVA: 0x00141995 File Offset: 0x0013FD95
		public string DisplayName
		{
			get
			{
				return this.mDisplayName;
			}
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x0600422B RID: 16939 RVA: 0x0014199D File Offset: 0x0013FD9D
		public string ParticipantId
		{
			get
			{
				return this.mParticipantId;
			}
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x0600422C RID: 16940 RVA: 0x001419A5 File Offset: 0x0013FDA5
		public Participant.ParticipantStatus Status
		{
			get
			{
				return this.mStatus;
			}
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x0600422D RID: 16941 RVA: 0x001419AD File Offset: 0x0013FDAD
		public Player Player
		{
			get
			{
				return this.mPlayer;
			}
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x0600422E RID: 16942 RVA: 0x001419B5 File Offset: 0x0013FDB5
		public bool IsConnectedToRoom
		{
			get
			{
				return this.mIsConnectedToRoom;
			}
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x0600422F RID: 16943 RVA: 0x001419BD File Offset: 0x0013FDBD
		public bool IsAutomatch
		{
			get
			{
				return this.mPlayer == null;
			}
		}

		// Token: 0x06004230 RID: 16944 RVA: 0x001419C8 File Offset: 0x0013FDC8
		public override string ToString()
		{
			return string.Format("[Participant: '{0}' (id {1}), status={2}, player={3}, connected={4}]", new object[]
			{
				this.mDisplayName,
				this.mParticipantId,
				this.mStatus.ToString(),
				(this.mPlayer != null) ? this.mPlayer.ToString() : "NULL",
				this.mIsConnectedToRoom
			});
		}

		// Token: 0x06004231 RID: 16945 RVA: 0x00141A3C File Offset: 0x0013FE3C
		public int CompareTo(Participant other)
		{
			return this.mParticipantId.CompareTo(other.mParticipantId);
		}

		// Token: 0x06004232 RID: 16946 RVA: 0x00141A50 File Offset: 0x0013FE50
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(Participant))
			{
				return false;
			}
			Participant participant = (Participant)obj;
			return this.mParticipantId.Equals(participant.mParticipantId);
		}

		// Token: 0x06004233 RID: 16947 RVA: 0x00141AA2 File Offset: 0x0013FEA2
		public override int GetHashCode()
		{
			return (this.mParticipantId == null) ? 0 : this.mParticipantId.GetHashCode();
		}

		// Token: 0x04004A5D RID: 19037
		private string mDisplayName = string.Empty;

		// Token: 0x04004A5E RID: 19038
		private string mParticipantId = string.Empty;

		// Token: 0x04004A5F RID: 19039
		private Participant.ParticipantStatus mStatus = Participant.ParticipantStatus.Unknown;

		// Token: 0x04004A60 RID: 19040
		private Player mPlayer;

		// Token: 0x04004A61 RID: 19041
		private bool mIsConnectedToRoom;

		// Token: 0x02000CFF RID: 3327
		public enum ParticipantStatus
		{
			// Token: 0x04004A63 RID: 19043
			NotInvitedYet,
			// Token: 0x04004A64 RID: 19044
			Invited,
			// Token: 0x04004A65 RID: 19045
			Joined,
			// Token: 0x04004A66 RID: 19046
			Declined,
			// Token: 0x04004A67 RID: 19047
			Left,
			// Token: 0x04004A68 RID: 19048
			Finished,
			// Token: 0x04004A69 RID: 19049
			Unresponsive,
			// Token: 0x04004A6A RID: 19050
			Unknown
		}
	}
}
