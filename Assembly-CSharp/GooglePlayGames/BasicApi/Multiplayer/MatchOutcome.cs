﻿using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CFC RID: 3324
	public class MatchOutcome
	{
		// Token: 0x06004222 RID: 16930 RVA: 0x0014180D File Offset: 0x0013FC0D
		public void SetParticipantResult(string participantId, MatchOutcome.ParticipantResult result, uint placement)
		{
			if (!this.mParticipantIds.Contains(participantId))
			{
				this.mParticipantIds.Add(participantId);
			}
			this.mPlacements[participantId] = placement;
			this.mResults[participantId] = result;
		}

		// Token: 0x06004223 RID: 16931 RVA: 0x00141846 File Offset: 0x0013FC46
		public void SetParticipantResult(string participantId, MatchOutcome.ParticipantResult result)
		{
			this.SetParticipantResult(participantId, result, 0U);
		}

		// Token: 0x06004224 RID: 16932 RVA: 0x00141851 File Offset: 0x0013FC51
		public void SetParticipantResult(string participantId, uint placement)
		{
			this.SetParticipantResult(participantId, MatchOutcome.ParticipantResult.Unset, placement);
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06004225 RID: 16933 RVA: 0x0014185C File Offset: 0x0013FC5C
		public List<string> ParticipantIds
		{
			get
			{
				return this.mParticipantIds;
			}
		}

		// Token: 0x06004226 RID: 16934 RVA: 0x00141864 File Offset: 0x0013FC64
		public MatchOutcome.ParticipantResult GetResultFor(string participantId)
		{
			return (!this.mResults.ContainsKey(participantId)) ? MatchOutcome.ParticipantResult.Unset : this.mResults[participantId];
		}

		// Token: 0x06004227 RID: 16935 RVA: 0x00141889 File Offset: 0x0013FC89
		public uint GetPlacementFor(string participantId)
		{
			return (!this.mPlacements.ContainsKey(participantId)) ? 0U : this.mPlacements[participantId];
		}

		// Token: 0x06004228 RID: 16936 RVA: 0x001418B0 File Offset: 0x0013FCB0
		public override string ToString()
		{
			string str = "[MatchOutcome";
			foreach (string text in this.mParticipantIds)
			{
				str += string.Format(" {0}->({1},{2})", text, this.GetResultFor(text), this.GetPlacementFor(text));
			}
			return str + "]";
		}

		// Token: 0x04004A53 RID: 19027
		public const uint PlacementUnset = 0U;

		// Token: 0x04004A54 RID: 19028
		private List<string> mParticipantIds = new List<string>();

		// Token: 0x04004A55 RID: 19029
		private Dictionary<string, uint> mPlacements = new Dictionary<string, uint>();

		// Token: 0x04004A56 RID: 19030
		private Dictionary<string, MatchOutcome.ParticipantResult> mResults = new Dictionary<string, MatchOutcome.ParticipantResult>();

		// Token: 0x02000CFD RID: 3325
		public enum ParticipantResult
		{
			// Token: 0x04004A58 RID: 19032
			Unset = -1,
			// Token: 0x04004A59 RID: 19033
			None,
			// Token: 0x04004A5A RID: 19034
			Win,
			// Token: 0x04004A5B RID: 19035
			Loss,
			// Token: 0x04004A5C RID: 19036
			Tie
		}
	}
}
