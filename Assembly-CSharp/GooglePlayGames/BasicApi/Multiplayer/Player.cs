﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000D00 RID: 3328
	public class Player : PlayGamesUserProfile
	{
		// Token: 0x06004234 RID: 16948 RVA: 0x00141D96 File Offset: 0x00140196
		internal Player(string displayName, string playerId, string avatarUrl) : base(displayName, playerId, avatarUrl)
		{
		}
	}
}
