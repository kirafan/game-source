﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CF9 RID: 3321
	// (Invoke) Token: 0x06004218 RID: 16920
	public delegate void MatchDelegate(TurnBasedMatch match, bool shouldAutoLaunch);
}
