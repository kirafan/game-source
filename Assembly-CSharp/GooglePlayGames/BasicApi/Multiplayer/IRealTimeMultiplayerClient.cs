﻿using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CF7 RID: 3319
	public interface IRealTimeMultiplayerClient
	{
		// Token: 0x060041F3 RID: 16883
		void CreateQuickGame(uint minOpponents, uint maxOpponents, uint variant, RealTimeMultiplayerListener listener);

		// Token: 0x060041F4 RID: 16884
		void CreateQuickGame(uint minOpponents, uint maxOpponents, uint variant, ulong exclusiveBitMask, RealTimeMultiplayerListener listener);

		// Token: 0x060041F5 RID: 16885
		void CreateWithInvitationScreen(uint minOpponents, uint maxOppponents, uint variant, RealTimeMultiplayerListener listener);

		// Token: 0x060041F6 RID: 16886
		void ShowWaitingRoomUI();

		// Token: 0x060041F7 RID: 16887
		void GetAllInvitations(Action<Invitation[]> callback);

		// Token: 0x060041F8 RID: 16888
		void AcceptFromInbox(RealTimeMultiplayerListener listener);

		// Token: 0x060041F9 RID: 16889
		void AcceptInvitation(string invitationId, RealTimeMultiplayerListener listener);

		// Token: 0x060041FA RID: 16890
		void SendMessageToAll(bool reliable, byte[] data);

		// Token: 0x060041FB RID: 16891
		void SendMessageToAll(bool reliable, byte[] data, int offset, int length);

		// Token: 0x060041FC RID: 16892
		void SendMessage(bool reliable, string participantId, byte[] data);

		// Token: 0x060041FD RID: 16893
		void SendMessage(bool reliable, string participantId, byte[] data, int offset, int length);

		// Token: 0x060041FE RID: 16894
		List<Participant> GetConnectedParticipants();

		// Token: 0x060041FF RID: 16895
		Participant GetSelf();

		// Token: 0x06004200 RID: 16896
		Participant GetParticipant(string participantId);

		// Token: 0x06004201 RID: 16897
		Invitation GetInvitation();

		// Token: 0x06004202 RID: 16898
		void LeaveRoom();

		// Token: 0x06004203 RID: 16899
		bool IsRoomConnected();

		// Token: 0x06004204 RID: 16900
		void DeclineInvitation(string invitationId);
	}
}
