﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CFA RID: 3322
	public class Invitation
	{
		// Token: 0x0600421B RID: 16923 RVA: 0x0014175F File Offset: 0x0013FB5F
		internal Invitation(Invitation.InvType invType, string invId, Participant inviter, int variant)
		{
			this.mInvitationType = invType;
			this.mInvitationId = invId;
			this.mInviter = inviter;
			this.mVariant = variant;
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x0600421C RID: 16924 RVA: 0x00141784 File Offset: 0x0013FB84
		public Invitation.InvType InvitationType
		{
			get
			{
				return this.mInvitationType;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x0600421D RID: 16925 RVA: 0x0014178C File Offset: 0x0013FB8C
		public string InvitationId
		{
			get
			{
				return this.mInvitationId;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x0600421E RID: 16926 RVA: 0x00141794 File Offset: 0x0013FB94
		public Participant Inviter
		{
			get
			{
				return this.mInviter;
			}
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x0600421F RID: 16927 RVA: 0x0014179C File Offset: 0x0013FB9C
		public int Variant
		{
			get
			{
				return this.mVariant;
			}
		}

		// Token: 0x06004220 RID: 16928 RVA: 0x001417A4 File Offset: 0x0013FBA4
		public override string ToString()
		{
			return string.Format("[Invitation: InvitationType={0}, InvitationId={1}, Inviter={2}, Variant={3}]", new object[]
			{
				this.InvitationType,
				this.InvitationId,
				this.Inviter,
				this.Variant
			});
		}

		// Token: 0x04004A4B RID: 19019
		private Invitation.InvType mInvitationType;

		// Token: 0x04004A4C RID: 19020
		private string mInvitationId;

		// Token: 0x04004A4D RID: 19021
		private Participant mInviter;

		// Token: 0x04004A4E RID: 19022
		private int mVariant;

		// Token: 0x02000CFB RID: 3323
		public enum InvType
		{
			// Token: 0x04004A50 RID: 19024
			RealTime,
			// Token: 0x04004A51 RID: 19025
			TurnBased,
			// Token: 0x04004A52 RID: 19026
			Unknown
		}
	}
}
