﻿using System;
using System.Collections.Generic;
using System.Linq;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000D02 RID: 3330
	public class TurnBasedMatch
	{
		// Token: 0x0600423C RID: 16956 RVA: 0x00141DA4 File Offset: 0x001401A4
		internal TurnBasedMatch(string matchId, byte[] data, bool canRematch, string selfParticipantId, List<Participant> participants, uint availableAutomatchSlots, string pendingParticipantId, TurnBasedMatch.MatchTurnStatus turnStatus, TurnBasedMatch.MatchStatus matchStatus, uint variant, uint version)
		{
			this.mMatchId = matchId;
			this.mData = data;
			this.mCanRematch = canRematch;
			this.mSelfParticipantId = selfParticipantId;
			this.mParticipants = participants;
			this.mParticipants.Sort();
			this.mAvailableAutomatchSlots = availableAutomatchSlots;
			this.mPendingParticipantId = pendingParticipantId;
			this.mTurnStatus = turnStatus;
			this.mMatchStatus = matchStatus;
			this.mVariant = variant;
			this.mVersion = version;
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x0600423D RID: 16957 RVA: 0x00141E17 File Offset: 0x00140217
		public string MatchId
		{
			get
			{
				return this.mMatchId;
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x0600423E RID: 16958 RVA: 0x00141E1F File Offset: 0x0014021F
		public byte[] Data
		{
			get
			{
				return this.mData;
			}
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x0600423F RID: 16959 RVA: 0x00141E27 File Offset: 0x00140227
		public bool CanRematch
		{
			get
			{
				return this.mCanRematch;
			}
		}

		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x06004240 RID: 16960 RVA: 0x00141E2F File Offset: 0x0014022F
		public string SelfParticipantId
		{
			get
			{
				return this.mSelfParticipantId;
			}
		}

		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x06004241 RID: 16961 RVA: 0x00141E37 File Offset: 0x00140237
		public Participant Self
		{
			get
			{
				return this.GetParticipant(this.mSelfParticipantId);
			}
		}

		// Token: 0x06004242 RID: 16962 RVA: 0x00141E48 File Offset: 0x00140248
		public Participant GetParticipant(string participantId)
		{
			foreach (Participant participant in this.mParticipants)
			{
				if (participant.ParticipantId.Equals(participantId))
				{
					return participant;
				}
			}
			Logger.w("Participant not found in turn-based match: " + participantId);
			return null;
		}

		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x06004243 RID: 16963 RVA: 0x00141EC8 File Offset: 0x001402C8
		public List<Participant> Participants
		{
			get
			{
				return this.mParticipants;
			}
		}

		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x06004244 RID: 16964 RVA: 0x00141ED0 File Offset: 0x001402D0
		public string PendingParticipantId
		{
			get
			{
				return this.mPendingParticipantId;
			}
		}

		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x06004245 RID: 16965 RVA: 0x00141ED8 File Offset: 0x001402D8
		public Participant PendingParticipant
		{
			get
			{
				return (this.mPendingParticipantId != null) ? this.GetParticipant(this.mPendingParticipantId) : null;
			}
		}

		// Token: 0x1700041F RID: 1055
		// (get) Token: 0x06004246 RID: 16966 RVA: 0x00141EF7 File Offset: 0x001402F7
		public TurnBasedMatch.MatchTurnStatus TurnStatus
		{
			get
			{
				return this.mTurnStatus;
			}
		}

		// Token: 0x17000420 RID: 1056
		// (get) Token: 0x06004247 RID: 16967 RVA: 0x00141EFF File Offset: 0x001402FF
		public TurnBasedMatch.MatchStatus Status
		{
			get
			{
				return this.mMatchStatus;
			}
		}

		// Token: 0x17000421 RID: 1057
		// (get) Token: 0x06004248 RID: 16968 RVA: 0x00141F07 File Offset: 0x00140307
		public uint Variant
		{
			get
			{
				return this.mVariant;
			}
		}

		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x06004249 RID: 16969 RVA: 0x00141F0F File Offset: 0x0014030F
		public uint Version
		{
			get
			{
				return this.mVersion;
			}
		}

		// Token: 0x17000423 RID: 1059
		// (get) Token: 0x0600424A RID: 16970 RVA: 0x00141F17 File Offset: 0x00140317
		public uint AvailableAutomatchSlots
		{
			get
			{
				return this.mAvailableAutomatchSlots;
			}
		}

		// Token: 0x0600424B RID: 16971 RVA: 0x00141F20 File Offset: 0x00140320
		public override string ToString()
		{
			string format = "[TurnBasedMatch: mMatchId={0}, mData={1}, mCanRematch={2}, mSelfParticipantId={3}, mParticipants={4}, mPendingParticipantId={5}, mTurnStatus={6}, mMatchStatus={7}, mVariant={8}, mVersion={9}]";
			object[] array = new object[10];
			array[0] = this.mMatchId;
			array[1] = this.mData;
			array[2] = this.mCanRematch;
			array[3] = this.mSelfParticipantId;
			array[4] = string.Join(",", (from p in this.mParticipants
			select p.ToString()).ToArray<string>());
			array[5] = this.mPendingParticipantId;
			array[6] = this.mTurnStatus;
			array[7] = this.mMatchStatus;
			array[8] = this.mVariant;
			array[9] = this.mVersion;
			return string.Format(format, array);
		}

		// Token: 0x04004A6B RID: 19051
		private string mMatchId;

		// Token: 0x04004A6C RID: 19052
		private byte[] mData;

		// Token: 0x04004A6D RID: 19053
		private bool mCanRematch;

		// Token: 0x04004A6E RID: 19054
		private uint mAvailableAutomatchSlots;

		// Token: 0x04004A6F RID: 19055
		private string mSelfParticipantId;

		// Token: 0x04004A70 RID: 19056
		private List<Participant> mParticipants;

		// Token: 0x04004A71 RID: 19057
		private string mPendingParticipantId;

		// Token: 0x04004A72 RID: 19058
		private TurnBasedMatch.MatchTurnStatus mTurnStatus;

		// Token: 0x04004A73 RID: 19059
		private TurnBasedMatch.MatchStatus mMatchStatus;

		// Token: 0x04004A74 RID: 19060
		private uint mVariant;

		// Token: 0x04004A75 RID: 19061
		private uint mVersion;

		// Token: 0x02000D03 RID: 3331
		public enum MatchStatus
		{
			// Token: 0x04004A78 RID: 19064
			Active,
			// Token: 0x04004A79 RID: 19065
			AutoMatching,
			// Token: 0x04004A7A RID: 19066
			Cancelled,
			// Token: 0x04004A7B RID: 19067
			Complete,
			// Token: 0x04004A7C RID: 19068
			Expired,
			// Token: 0x04004A7D RID: 19069
			Unknown,
			// Token: 0x04004A7E RID: 19070
			Deleted
		}

		// Token: 0x02000D04 RID: 3332
		public enum MatchTurnStatus
		{
			// Token: 0x04004A80 RID: 19072
			Complete,
			// Token: 0x04004A81 RID: 19073
			Invited,
			// Token: 0x04004A82 RID: 19074
			MyTurn,
			// Token: 0x04004A83 RID: 19075
			TheirTurn,
			// Token: 0x04004A84 RID: 19076
			Unknown
		}
	}
}
