﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000CF8 RID: 3320
	public interface ITurnBasedMultiplayerClient
	{
		// Token: 0x06004205 RID: 16901
		void CreateQuickMatch(uint minOpponents, uint maxOpponents, uint variant, Action<bool, TurnBasedMatch> callback);

		// Token: 0x06004206 RID: 16902
		void CreateQuickMatch(uint minOpponents, uint maxOpponents, uint variant, ulong exclusiveBitmask, Action<bool, TurnBasedMatch> callback);

		// Token: 0x06004207 RID: 16903
		void CreateWithInvitationScreen(uint minOpponents, uint maxOpponents, uint variant, Action<bool, TurnBasedMatch> callback);

		// Token: 0x06004208 RID: 16904
		void CreateWithInvitationScreen(uint minOpponents, uint maxOpponents, uint variant, Action<UIStatus, TurnBasedMatch> callback);

		// Token: 0x06004209 RID: 16905
		void GetAllInvitations(Action<Invitation[]> callback);

		// Token: 0x0600420A RID: 16906
		void GetAllMatches(Action<TurnBasedMatch[]> callback);

		// Token: 0x0600420B RID: 16907
		void AcceptFromInbox(Action<bool, TurnBasedMatch> callback);

		// Token: 0x0600420C RID: 16908
		void AcceptInvitation(string invitationId, Action<bool, TurnBasedMatch> callback);

		// Token: 0x0600420D RID: 16909
		void RegisterMatchDelegate(MatchDelegate del);

		// Token: 0x0600420E RID: 16910
		void TakeTurn(TurnBasedMatch match, byte[] data, string pendingParticipantId, Action<bool> callback);

		// Token: 0x0600420F RID: 16911
		int GetMaxMatchDataSize();

		// Token: 0x06004210 RID: 16912
		void Finish(TurnBasedMatch match, byte[] data, MatchOutcome outcome, Action<bool> callback);

		// Token: 0x06004211 RID: 16913
		void AcknowledgeFinished(TurnBasedMatch match, Action<bool> callback);

		// Token: 0x06004212 RID: 16914
		void Leave(TurnBasedMatch match, Action<bool> callback);

		// Token: 0x06004213 RID: 16915
		void LeaveDuringTurn(TurnBasedMatch match, string pendingParticipantId, Action<bool> callback);

		// Token: 0x06004214 RID: 16916
		void Cancel(TurnBasedMatch match, Action<bool> callback);

		// Token: 0x06004215 RID: 16917
		void Rematch(TurnBasedMatch match, Action<bool, TurnBasedMatch> callback);

		// Token: 0x06004216 RID: 16918
		void DeclineInvitation(string invitationId);
	}
}
