﻿using System;

namespace GooglePlayGames.BasicApi.Multiplayer
{
	// Token: 0x02000D01 RID: 3329
	public interface RealTimeMultiplayerListener
	{
		// Token: 0x06004235 RID: 16949
		void OnRoomSetupProgress(float percent);

		// Token: 0x06004236 RID: 16950
		void OnRoomConnected(bool success);

		// Token: 0x06004237 RID: 16951
		void OnLeftRoom();

		// Token: 0x06004238 RID: 16952
		void OnParticipantLeft(Participant participant);

		// Token: 0x06004239 RID: 16953
		void OnPeersConnected(string[] participantIds);

		// Token: 0x0600423A RID: 16954
		void OnPeersDisconnected(string[] participantIds);

		// Token: 0x0600423B RID: 16955
		void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data);
	}
}
