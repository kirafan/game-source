﻿using System;
using System.Collections.Generic;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000D10 RID: 3344
	public struct PlayGamesClientConfiguration
	{
		// Token: 0x0600428F RID: 17039 RVA: 0x00142270 File Offset: 0x00140670
		private PlayGamesClientConfiguration(PlayGamesClientConfiguration.Builder builder)
		{
			this.mEnableSavedGames = builder.HasEnableSaveGames();
			this.mInvitationDelegate = builder.GetInvitationDelegate();
			this.mMatchDelegate = builder.GetMatchDelegate();
			this.mScopes = builder.getScopes();
			this.mHidePopups = builder.IsHidingPopups();
			this.mRequestAuthCode = builder.IsRequestingAuthCode();
			this.mForceRefresh = builder.IsForcingRefresh();
			this.mRequestEmail = builder.IsRequestingEmail();
			this.mRequestIdToken = builder.IsRequestingIdToken();
			this.mAccountName = builder.GetAccountName();
		}

		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x06004290 RID: 17040 RVA: 0x001422F5 File Offset: 0x001406F5
		public bool EnableSavedGames
		{
			get
			{
				return this.mEnableSavedGames;
			}
		}

		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x06004291 RID: 17041 RVA: 0x001422FD File Offset: 0x001406FD
		public bool IsHidingPopups
		{
			get
			{
				return this.mHidePopups;
			}
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x06004292 RID: 17042 RVA: 0x00142305 File Offset: 0x00140705
		public bool IsRequestingAuthCode
		{
			get
			{
				return this.mRequestAuthCode;
			}
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x06004293 RID: 17043 RVA: 0x0014230D File Offset: 0x0014070D
		public bool IsForcingRefresh
		{
			get
			{
				return this.mForceRefresh;
			}
		}

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x06004294 RID: 17044 RVA: 0x00142315 File Offset: 0x00140715
		public bool IsRequestingEmail
		{
			get
			{
				return this.mRequestEmail;
			}
		}

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x06004295 RID: 17045 RVA: 0x0014231D File Offset: 0x0014071D
		public bool IsRequestingIdToken
		{
			get
			{
				return this.mRequestIdToken;
			}
		}

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06004296 RID: 17046 RVA: 0x00142325 File Offset: 0x00140725
		public string AccountName
		{
			get
			{
				return this.mAccountName;
			}
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06004297 RID: 17047 RVA: 0x0014232D File Offset: 0x0014072D
		public string[] Scopes
		{
			get
			{
				return this.mScopes;
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x06004298 RID: 17048 RVA: 0x00142335 File Offset: 0x00140735
		public InvitationReceivedDelegate InvitationDelegate
		{
			get
			{
				return this.mInvitationDelegate;
			}
		}

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x06004299 RID: 17049 RVA: 0x0014233D File Offset: 0x0014073D
		public MatchDelegate MatchDelegate
		{
			get
			{
				return this.mMatchDelegate;
			}
		}

		// Token: 0x04004AA1 RID: 19105
		public static readonly PlayGamesClientConfiguration DefaultConfiguration = new PlayGamesClientConfiguration.Builder().Build();

		// Token: 0x04004AA2 RID: 19106
		private readonly bool mEnableSavedGames;

		// Token: 0x04004AA3 RID: 19107
		private readonly string[] mScopes;

		// Token: 0x04004AA4 RID: 19108
		private readonly bool mRequestAuthCode;

		// Token: 0x04004AA5 RID: 19109
		private readonly bool mForceRefresh;

		// Token: 0x04004AA6 RID: 19110
		private readonly bool mHidePopups;

		// Token: 0x04004AA7 RID: 19111
		private readonly bool mRequestEmail;

		// Token: 0x04004AA8 RID: 19112
		private readonly bool mRequestIdToken;

		// Token: 0x04004AA9 RID: 19113
		private readonly string mAccountName;

		// Token: 0x04004AAA RID: 19114
		private readonly InvitationReceivedDelegate mInvitationDelegate;

		// Token: 0x04004AAB RID: 19115
		private readonly MatchDelegate mMatchDelegate;

		// Token: 0x02000D11 RID: 3345
		public class Builder
		{
			// Token: 0x0600429C RID: 17052 RVA: 0x001423B1 File Offset: 0x001407B1
			public PlayGamesClientConfiguration.Builder EnableSavedGames()
			{
				this.mEnableSaveGames = true;
				return this;
			}

			// Token: 0x0600429D RID: 17053 RVA: 0x001423BB File Offset: 0x001407BB
			public PlayGamesClientConfiguration.Builder EnableHidePopups()
			{
				this.mHidePopups = true;
				return this;
			}

			// Token: 0x0600429E RID: 17054 RVA: 0x001423C5 File Offset: 0x001407C5
			public PlayGamesClientConfiguration.Builder RequestServerAuthCode(bool forceRefresh)
			{
				this.mRequestAuthCode = true;
				this.mForceRefresh = forceRefresh;
				return this;
			}

			// Token: 0x0600429F RID: 17055 RVA: 0x001423D6 File Offset: 0x001407D6
			public PlayGamesClientConfiguration.Builder RequestEmail()
			{
				this.mRequestEmail = true;
				return this;
			}

			// Token: 0x060042A0 RID: 17056 RVA: 0x001423E0 File Offset: 0x001407E0
			public PlayGamesClientConfiguration.Builder RequestIdToken()
			{
				this.mRequestIdToken = true;
				return this;
			}

			// Token: 0x060042A1 RID: 17057 RVA: 0x001423EA File Offset: 0x001407EA
			public PlayGamesClientConfiguration.Builder SetAccountName(string accountName)
			{
				this.mAccountName = accountName;
				return this;
			}

			// Token: 0x060042A2 RID: 17058 RVA: 0x001423F4 File Offset: 0x001407F4
			public PlayGamesClientConfiguration.Builder AddOauthScope(string scope)
			{
				if (this.mScopes == null)
				{
					this.mScopes = new List<string>();
				}
				this.mScopes.Add(scope);
				return this;
			}

			// Token: 0x060042A3 RID: 17059 RVA: 0x00142419 File Offset: 0x00140819
			public PlayGamesClientConfiguration.Builder WithInvitationDelegate(InvitationReceivedDelegate invitationDelegate)
			{
				this.mInvitationDelegate = Misc.CheckNotNull<InvitationReceivedDelegate>(invitationDelegate);
				return this;
			}

			// Token: 0x060042A4 RID: 17060 RVA: 0x00142428 File Offset: 0x00140828
			public PlayGamesClientConfiguration.Builder WithMatchDelegate(MatchDelegate matchDelegate)
			{
				this.mMatchDelegate = Misc.CheckNotNull<MatchDelegate>(matchDelegate);
				return this;
			}

			// Token: 0x060042A5 RID: 17061 RVA: 0x00142437 File Offset: 0x00140837
			public PlayGamesClientConfiguration Build()
			{
				return new PlayGamesClientConfiguration(this);
			}

			// Token: 0x060042A6 RID: 17062 RVA: 0x0014243F File Offset: 0x0014083F
			internal bool HasEnableSaveGames()
			{
				return this.mEnableSaveGames;
			}

			// Token: 0x060042A7 RID: 17063 RVA: 0x00142447 File Offset: 0x00140847
			internal bool IsRequestingAuthCode()
			{
				return this.mRequestAuthCode;
			}

			// Token: 0x060042A8 RID: 17064 RVA: 0x0014244F File Offset: 0x0014084F
			internal bool IsHidingPopups()
			{
				return this.mHidePopups;
			}

			// Token: 0x060042A9 RID: 17065 RVA: 0x00142457 File Offset: 0x00140857
			internal bool IsForcingRefresh()
			{
				return this.mForceRefresh;
			}

			// Token: 0x060042AA RID: 17066 RVA: 0x0014245F File Offset: 0x0014085F
			internal bool IsRequestingEmail()
			{
				return this.mRequestEmail;
			}

			// Token: 0x060042AB RID: 17067 RVA: 0x00142467 File Offset: 0x00140867
			internal bool IsRequestingIdToken()
			{
				return this.mRequestIdToken;
			}

			// Token: 0x060042AC RID: 17068 RVA: 0x0014246F File Offset: 0x0014086F
			internal string GetAccountName()
			{
				return this.mAccountName;
			}

			// Token: 0x060042AD RID: 17069 RVA: 0x00142477 File Offset: 0x00140877
			internal string[] getScopes()
			{
				return (this.mScopes != null) ? this.mScopes.ToArray() : new string[0];
			}

			// Token: 0x060042AE RID: 17070 RVA: 0x0014249A File Offset: 0x0014089A
			internal MatchDelegate GetMatchDelegate()
			{
				return this.mMatchDelegate;
			}

			// Token: 0x060042AF RID: 17071 RVA: 0x001424A2 File Offset: 0x001408A2
			internal InvitationReceivedDelegate GetInvitationDelegate()
			{
				return this.mInvitationDelegate;
			}

			// Token: 0x04004AAC RID: 19116
			private bool mEnableSaveGames;

			// Token: 0x04004AAD RID: 19117
			private List<string> mScopes;

			// Token: 0x04004AAE RID: 19118
			private bool mHidePopups;

			// Token: 0x04004AAF RID: 19119
			private bool mRequestAuthCode;

			// Token: 0x04004AB0 RID: 19120
			private bool mForceRefresh;

			// Token: 0x04004AB1 RID: 19121
			private bool mRequestEmail;

			// Token: 0x04004AB2 RID: 19122
			private bool mRequestIdToken;

			// Token: 0x04004AB3 RID: 19123
			private string mAccountName;

			// Token: 0x04004AB4 RID: 19124
			private InvitationReceivedDelegate mInvitationDelegate = delegate(Invitation A_0, bool A_1)
			{
			};

			// Token: 0x04004AB5 RID: 19125
			private MatchDelegate mMatchDelegate = delegate(TurnBasedMatch A_0, bool A_1)
			{
			};
		}
	}
}
