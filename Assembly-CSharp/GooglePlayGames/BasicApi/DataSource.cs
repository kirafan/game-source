﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE5 RID: 3301
	public enum DataSource
	{
		// Token: 0x04004A0D RID: 18957
		ReadCacheOrNetwork,
		// Token: 0x04004A0E RID: 18958
		ReadNetworkOnly
	}
}
