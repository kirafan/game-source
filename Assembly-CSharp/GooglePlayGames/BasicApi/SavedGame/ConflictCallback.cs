﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D1F RID: 3359
	// (Invoke) Token: 0x060042EB RID: 17131
	public delegate void ConflictCallback(IConflictResolver resolver, ISavedGameMetadata original, byte[] originalData, ISavedGameMetadata unmerged, byte[] unmergedData);
}
