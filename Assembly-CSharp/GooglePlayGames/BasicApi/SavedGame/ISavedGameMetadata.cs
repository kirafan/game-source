﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D22 RID: 3362
	public interface ISavedGameMetadata
	{
		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x060042F6 RID: 17142
		bool IsOpen { get; }

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x060042F7 RID: 17143
		string Filename { get; }

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x060042F8 RID: 17144
		string Description { get; }

		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x060042F9 RID: 17145
		string CoverImageURL { get; }

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x060042FA RID: 17146
		TimeSpan TotalTimePlayed { get; }

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x060042FB RID: 17147
		DateTime LastModifiedTimestamp { get; }
	}
}
