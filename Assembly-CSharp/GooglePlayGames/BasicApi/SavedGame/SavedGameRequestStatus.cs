﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D1D RID: 3357
	public enum SavedGameRequestStatus
	{
		// Token: 0x04004AF9 RID: 19193
		Success = 1,
		// Token: 0x04004AFA RID: 19194
		TimeoutError = -1,
		// Token: 0x04004AFB RID: 19195
		InternalError = -2,
		// Token: 0x04004AFC RID: 19196
		AuthenticationError = -3,
		// Token: 0x04004AFD RID: 19197
		BadInputError = -4
	}
}
