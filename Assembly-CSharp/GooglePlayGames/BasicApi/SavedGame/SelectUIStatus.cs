﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D1E RID: 3358
	public enum SelectUIStatus
	{
		// Token: 0x04004AFF RID: 19199
		SavedGameSelected = 1,
		// Token: 0x04004B00 RID: 19200
		UserClosedUI,
		// Token: 0x04004B01 RID: 19201
		InternalError = -1,
		// Token: 0x04004B02 RID: 19202
		TimeoutError = -2,
		// Token: 0x04004B03 RID: 19203
		AuthenticationError = -3,
		// Token: 0x04004B04 RID: 19204
		BadInputError = -4
	}
}
