﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D23 RID: 3363
	public struct SavedGameMetadataUpdate
	{
		// Token: 0x060042FC RID: 17148 RVA: 0x0014262C File Offset: 0x00140A2C
		private SavedGameMetadataUpdate(SavedGameMetadataUpdate.Builder builder)
		{
			this.mDescriptionUpdated = builder.mDescriptionUpdated;
			this.mNewDescription = builder.mNewDescription;
			this.mCoverImageUpdated = builder.mCoverImageUpdated;
			this.mNewPngCoverImage = builder.mNewPngCoverImage;
			this.mNewPlayedTime = builder.mNewPlayedTime;
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x060042FD RID: 17149 RVA: 0x0014267A File Offset: 0x00140A7A
		public bool IsDescriptionUpdated
		{
			get
			{
				return this.mDescriptionUpdated;
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x060042FE RID: 17150 RVA: 0x00142682 File Offset: 0x00140A82
		public string UpdatedDescription
		{
			get
			{
				return this.mNewDescription;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x060042FF RID: 17151 RVA: 0x0014268A File Offset: 0x00140A8A
		public bool IsCoverImageUpdated
		{
			get
			{
				return this.mCoverImageUpdated;
			}
		}

		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x06004300 RID: 17152 RVA: 0x00142692 File Offset: 0x00140A92
		public byte[] UpdatedPngCoverImage
		{
			get
			{
				return this.mNewPngCoverImage;
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06004301 RID: 17153 RVA: 0x0014269C File Offset: 0x00140A9C
		public bool IsPlayedTimeUpdated
		{
			get
			{
				return this.mNewPlayedTime != null;
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x06004302 RID: 17154 RVA: 0x001426B7 File Offset: 0x00140AB7
		public TimeSpan? UpdatedPlayedTime
		{
			get
			{
				return this.mNewPlayedTime;
			}
		}

		// Token: 0x04004B05 RID: 19205
		private readonly bool mDescriptionUpdated;

		// Token: 0x04004B06 RID: 19206
		private readonly string mNewDescription;

		// Token: 0x04004B07 RID: 19207
		private readonly bool mCoverImageUpdated;

		// Token: 0x04004B08 RID: 19208
		private readonly byte[] mNewPngCoverImage;

		// Token: 0x04004B09 RID: 19209
		private readonly TimeSpan? mNewPlayedTime;

		// Token: 0x02000D24 RID: 3364
		public struct Builder
		{
			// Token: 0x06004303 RID: 17155 RVA: 0x001426BF File Offset: 0x00140ABF
			public SavedGameMetadataUpdate.Builder WithUpdatedDescription(string description)
			{
				this.mNewDescription = Misc.CheckNotNull<string>(description);
				this.mDescriptionUpdated = true;
				return this;
			}

			// Token: 0x06004304 RID: 17156 RVA: 0x001426DA File Offset: 0x00140ADA
			public SavedGameMetadataUpdate.Builder WithUpdatedPngCoverImage(byte[] newPngCoverImage)
			{
				this.mCoverImageUpdated = true;
				this.mNewPngCoverImage = newPngCoverImage;
				return this;
			}

			// Token: 0x06004305 RID: 17157 RVA: 0x001426F0 File Offset: 0x00140AF0
			public SavedGameMetadataUpdate.Builder WithUpdatedPlayedTime(TimeSpan newPlayedTime)
			{
				if (newPlayedTime.TotalMilliseconds > 1.8446744073709552E+19)
				{
					throw new InvalidOperationException("Timespans longer than ulong.MaxValue milliseconds are not allowed");
				}
				this.mNewPlayedTime = new TimeSpan?(newPlayedTime);
				return this;
			}

			// Token: 0x06004306 RID: 17158 RVA: 0x00142724 File Offset: 0x00140B24
			public SavedGameMetadataUpdate Build()
			{
				return new SavedGameMetadataUpdate(this);
			}

			// Token: 0x04004B0A RID: 19210
			internal bool mDescriptionUpdated;

			// Token: 0x04004B0B RID: 19211
			internal string mNewDescription;

			// Token: 0x04004B0C RID: 19212
			internal bool mCoverImageUpdated;

			// Token: 0x04004B0D RID: 19213
			internal byte[] mNewPngCoverImage;

			// Token: 0x04004B0E RID: 19214
			internal TimeSpan? mNewPlayedTime;
		}
	}
}
