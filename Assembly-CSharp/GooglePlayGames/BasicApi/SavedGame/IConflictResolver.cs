﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D21 RID: 3361
	public interface IConflictResolver
	{
		// Token: 0x060042F5 RID: 17141
		void ChooseMetadata(ISavedGameMetadata chosenMetadata);
	}
}
