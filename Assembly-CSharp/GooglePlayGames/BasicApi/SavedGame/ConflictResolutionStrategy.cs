﻿using System;

namespace GooglePlayGames.BasicApi.SavedGame
{
	// Token: 0x02000D1C RID: 3356
	public enum ConflictResolutionStrategy
	{
		// Token: 0x04004AF5 RID: 19189
		UseLongestPlaytime,
		// Token: 0x04004AF6 RID: 19190
		UseOriginal,
		// Token: 0x04004AF7 RID: 19191
		UseUnmerged
	}
}
