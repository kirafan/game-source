﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CEF RID: 3311
	public class CommonTypesUtil
	{
		// Token: 0x06004185 RID: 16773 RVA: 0x0014140A File Offset: 0x0013F80A
		public static bool StatusIsSuccess(ResponseStatus status)
		{
			return status > (ResponseStatus)0;
		}
	}
}
