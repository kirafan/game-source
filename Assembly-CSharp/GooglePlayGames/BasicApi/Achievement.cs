﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE3 RID: 3299
	public class Achievement
	{
		// Token: 0x0600416A RID: 16746 RVA: 0x00141254 File Offset: 0x0013F654
		public override string ToString()
		{
			return string.Format("[Achievement] id={0}, name={1}, desc={2}, type={3}, revealed={4}, unlocked={5}, steps={6}/{7}", new object[]
			{
				this.mId,
				this.mName,
				this.mDescription,
				(!this.mIsIncremental) ? "STANDARD" : "INCREMENTAL",
				this.mIsRevealed,
				this.mIsUnlocked,
				this.mCurrentSteps,
				this.mTotalSteps
			});
		}

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x0600416B RID: 16747 RVA: 0x001412E1 File Offset: 0x0013F6E1
		// (set) Token: 0x0600416C RID: 16748 RVA: 0x001412E9 File Offset: 0x0013F6E9
		public bool IsIncremental
		{
			get
			{
				return this.mIsIncremental;
			}
			set
			{
				this.mIsIncremental = value;
			}
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x0600416D RID: 16749 RVA: 0x001412F2 File Offset: 0x0013F6F2
		// (set) Token: 0x0600416E RID: 16750 RVA: 0x001412FA File Offset: 0x0013F6FA
		public int CurrentSteps
		{
			get
			{
				return this.mCurrentSteps;
			}
			set
			{
				this.mCurrentSteps = value;
			}
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x0600416F RID: 16751 RVA: 0x00141303 File Offset: 0x0013F703
		// (set) Token: 0x06004170 RID: 16752 RVA: 0x0014130B File Offset: 0x0013F70B
		public int TotalSteps
		{
			get
			{
				return this.mTotalSteps;
			}
			set
			{
				this.mTotalSteps = value;
			}
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06004171 RID: 16753 RVA: 0x00141314 File Offset: 0x0013F714
		// (set) Token: 0x06004172 RID: 16754 RVA: 0x0014131C File Offset: 0x0013F71C
		public bool IsUnlocked
		{
			get
			{
				return this.mIsUnlocked;
			}
			set
			{
				this.mIsUnlocked = value;
			}
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06004173 RID: 16755 RVA: 0x00141325 File Offset: 0x0013F725
		// (set) Token: 0x06004174 RID: 16756 RVA: 0x0014132D File Offset: 0x0013F72D
		public bool IsRevealed
		{
			get
			{
				return this.mIsRevealed;
			}
			set
			{
				this.mIsRevealed = value;
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06004175 RID: 16757 RVA: 0x00141336 File Offset: 0x0013F736
		// (set) Token: 0x06004176 RID: 16758 RVA: 0x0014133E File Offset: 0x0013F73E
		public string Id
		{
			get
			{
				return this.mId;
			}
			set
			{
				this.mId = value;
			}
		}

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x06004177 RID: 16759 RVA: 0x00141347 File Offset: 0x0013F747
		// (set) Token: 0x06004178 RID: 16760 RVA: 0x0014134F File Offset: 0x0013F74F
		public string Description
		{
			get
			{
				return this.mDescription;
			}
			set
			{
				this.mDescription = value;
			}
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06004179 RID: 16761 RVA: 0x00141358 File Offset: 0x0013F758
		// (set) Token: 0x0600417A RID: 16762 RVA: 0x00141360 File Offset: 0x0013F760
		public string Name
		{
			get
			{
				return this.mName;
			}
			set
			{
				this.mName = value;
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x0600417B RID: 16763 RVA: 0x0014136C File Offset: 0x0013F76C
		// (set) Token: 0x0600417C RID: 16764 RVA: 0x00141390 File Offset: 0x0013F790
		public DateTime LastModifiedTime
		{
			get
			{
				return Achievement.UnixEpoch.AddMilliseconds((double)this.mLastModifiedTime);
			}
			set
			{
				this.mLastModifiedTime = (long)(value - Achievement.UnixEpoch).TotalMilliseconds;
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x0600417D RID: 16765 RVA: 0x001413B7 File Offset: 0x0013F7B7
		// (set) Token: 0x0600417E RID: 16766 RVA: 0x001413BF File Offset: 0x0013F7BF
		public ulong Points
		{
			get
			{
				return this.mPoints;
			}
			set
			{
				this.mPoints = value;
			}
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x0600417F RID: 16767 RVA: 0x001413C8 File Offset: 0x0013F7C8
		// (set) Token: 0x06004180 RID: 16768 RVA: 0x001413D0 File Offset: 0x0013F7D0
		public string RevealedImageUrl
		{
			get
			{
				return this.mRevealedImageUrl;
			}
			set
			{
				this.mRevealedImageUrl = value;
			}
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06004181 RID: 16769 RVA: 0x001413D9 File Offset: 0x0013F7D9
		// (set) Token: 0x06004182 RID: 16770 RVA: 0x001413E1 File Offset: 0x0013F7E1
		public string UnlockedImageUrl
		{
			get
			{
				return this.mUnlockedImageUrl;
			}
			set
			{
				this.mUnlockedImageUrl = value;
			}
		}

		// Token: 0x040049E6 RID: 18918
		private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

		// Token: 0x040049E7 RID: 18919
		private string mId = string.Empty;

		// Token: 0x040049E8 RID: 18920
		private bool mIsIncremental;

		// Token: 0x040049E9 RID: 18921
		private bool mIsRevealed;

		// Token: 0x040049EA RID: 18922
		private bool mIsUnlocked;

		// Token: 0x040049EB RID: 18923
		private int mCurrentSteps;

		// Token: 0x040049EC RID: 18924
		private int mTotalSteps;

		// Token: 0x040049ED RID: 18925
		private string mDescription = string.Empty;

		// Token: 0x040049EE RID: 18926
		private string mName = string.Empty;

		// Token: 0x040049EF RID: 18927
		private long mLastModifiedTime;

		// Token: 0x040049F0 RID: 18928
		private ulong mPoints;

		// Token: 0x040049F1 RID: 18929
		private string mRevealedImageUrl;

		// Token: 0x040049F2 RID: 18930
		private string mUnlockedImageUrl;
	}
}
