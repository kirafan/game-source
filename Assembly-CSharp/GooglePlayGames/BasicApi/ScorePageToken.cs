﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000D25 RID: 3365
	public class ScorePageToken
	{
		// Token: 0x06004307 RID: 17159 RVA: 0x00142731 File Offset: 0x00140B31
		internal ScorePageToken(object internalObject, string id, LeaderboardCollection collection, LeaderboardTimeSpan timespan)
		{
			this.mInternalObject = internalObject;
			this.mId = id;
			this.mCollection = collection;
			this.mTimespan = timespan;
		}

		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x06004308 RID: 17160 RVA: 0x00142756 File Offset: 0x00140B56
		public LeaderboardCollection Collection
		{
			get
			{
				return this.mCollection;
			}
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06004309 RID: 17161 RVA: 0x0014275E File Offset: 0x00140B5E
		public LeaderboardTimeSpan TimeSpan
		{
			get
			{
				return this.mTimespan;
			}
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x0600430A RID: 17162 RVA: 0x00142766 File Offset: 0x00140B66
		public string LeaderboardId
		{
			get
			{
				return this.mId;
			}
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x0600430B RID: 17163 RVA: 0x0014276E File Offset: 0x00140B6E
		internal object InternalObject
		{
			get
			{
				return this.mInternalObject;
			}
		}

		// Token: 0x04004B0F RID: 19215
		private string mId;

		// Token: 0x04004B10 RID: 19216
		private object mInternalObject;

		// Token: 0x04004B11 RID: 19217
		private LeaderboardCollection mCollection;

		// Token: 0x04004B12 RID: 19218
		private LeaderboardTimeSpan mTimespan;
	}
}
