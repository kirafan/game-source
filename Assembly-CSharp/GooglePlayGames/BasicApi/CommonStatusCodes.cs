﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE4 RID: 3300
	public enum CommonStatusCodes
	{
		// Token: 0x040049F4 RID: 18932
		SuccessCached = -1,
		// Token: 0x040049F5 RID: 18933
		Success,
		// Token: 0x040049F6 RID: 18934
		ServiceMissing,
		// Token: 0x040049F7 RID: 18935
		ServiceVersionUpdateRequired,
		// Token: 0x040049F8 RID: 18936
		ServiceDisabled,
		// Token: 0x040049F9 RID: 18937
		SignInRequired,
		// Token: 0x040049FA RID: 18938
		InvalidAccount,
		// Token: 0x040049FB RID: 18939
		ResolutionRequired,
		// Token: 0x040049FC RID: 18940
		NetworkError,
		// Token: 0x040049FD RID: 18941
		InternalError,
		// Token: 0x040049FE RID: 18942
		ServiceInvalid,
		// Token: 0x040049FF RID: 18943
		DeveloperError,
		// Token: 0x04004A00 RID: 18944
		LicenseCheckFailed,
		// Token: 0x04004A01 RID: 18945
		Error = 13,
		// Token: 0x04004A02 RID: 18946
		Interrupted,
		// Token: 0x04004A03 RID: 18947
		Timeout,
		// Token: 0x04004A04 RID: 18948
		Canceled,
		// Token: 0x04004A05 RID: 18949
		ApiNotConnected,
		// Token: 0x04004A06 RID: 18950
		AuthApiInvalidCredentials = 3000,
		// Token: 0x04004A07 RID: 18951
		AuthApiAccessForbidden,
		// Token: 0x04004A08 RID: 18952
		AuthApiClientError,
		// Token: 0x04004A09 RID: 18953
		AuthApiServerError,
		// Token: 0x04004A0A RID: 18954
		AuthTokenError,
		// Token: 0x04004A0B RID: 18955
		AuthUrlResolution
	}
}
