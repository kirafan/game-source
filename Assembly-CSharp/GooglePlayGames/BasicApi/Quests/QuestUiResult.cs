﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D1A RID: 3354
	public enum QuestUiResult
	{
		// Token: 0x04004AEB RID: 19179
		UserRequestsQuestAcceptance,
		// Token: 0x04004AEC RID: 19180
		UserRequestsMilestoneClaiming,
		// Token: 0x04004AED RID: 19181
		BadInput,
		// Token: 0x04004AEE RID: 19182
		InternalError,
		// Token: 0x04004AEF RID: 19183
		UserCanceled,
		// Token: 0x04004AF0 RID: 19184
		NotAuthorized,
		// Token: 0x04004AF1 RID: 19185
		VersionUpdateRequired,
		// Token: 0x04004AF2 RID: 19186
		Timeout,
		// Token: 0x04004AF3 RID: 19187
		UiBusy
	}
}
