﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D19 RID: 3353
	public enum QuestClaimMilestoneStatus
	{
		// Token: 0x04004AE3 RID: 19171
		Success,
		// Token: 0x04004AE4 RID: 19172
		BadInput,
		// Token: 0x04004AE5 RID: 19173
		InternalError,
		// Token: 0x04004AE6 RID: 19174
		NotAuthorized,
		// Token: 0x04004AE7 RID: 19175
		Timeout,
		// Token: 0x04004AE8 RID: 19176
		MilestoneAlreadyClaimed,
		// Token: 0x04004AE9 RID: 19177
		MilestoneClaimFailed
	}
}
