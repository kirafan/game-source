﻿using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D1B RID: 3355
	[Obsolete("Quests are being removed in 2018.")]
	public interface IQuestsClient
	{
		// Token: 0x060042E4 RID: 17124
		void Fetch(DataSource source, string questId, Action<ResponseStatus, IQuest> callback);

		// Token: 0x060042E5 RID: 17125
		void FetchMatchingState(DataSource source, QuestFetchFlags flags, Action<ResponseStatus, List<IQuest>> callback);

		// Token: 0x060042E6 RID: 17126
		void ShowAllQuestsUI(Action<QuestUiResult, IQuest, IQuestMilestone> callback);

		// Token: 0x060042E7 RID: 17127
		void ShowSpecificQuestUI(IQuest quest, Action<QuestUiResult, IQuest, IQuestMilestone> callback);

		// Token: 0x060042E8 RID: 17128
		void Accept(IQuest quest, Action<QuestAcceptStatus, IQuest> callback);

		// Token: 0x060042E9 RID: 17129
		void ClaimMilestone(IQuestMilestone milestone, Action<QuestClaimMilestoneStatus, IQuest, IQuestMilestone> callback);
	}
}
