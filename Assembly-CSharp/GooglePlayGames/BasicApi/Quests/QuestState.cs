﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D13 RID: 3347
	public enum QuestState
	{
		// Token: 0x04004AC5 RID: 19141
		Upcoming = 1,
		// Token: 0x04004AC6 RID: 19142
		Open,
		// Token: 0x04004AC7 RID: 19143
		Accepted,
		// Token: 0x04004AC8 RID: 19144
		Completed,
		// Token: 0x04004AC9 RID: 19145
		Expired,
		// Token: 0x04004ACA RID: 19146
		Failed
	}
}
