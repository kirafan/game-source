﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D18 RID: 3352
	public enum QuestAcceptStatus
	{
		// Token: 0x04004ADB RID: 19163
		Success,
		// Token: 0x04004ADC RID: 19164
		BadInput,
		// Token: 0x04004ADD RID: 19165
		InternalError,
		// Token: 0x04004ADE RID: 19166
		NotAuthorized,
		// Token: 0x04004ADF RID: 19167
		Timeout,
		// Token: 0x04004AE0 RID: 19168
		QuestNoLongerAvailable,
		// Token: 0x04004AE1 RID: 19169
		QuestNotStarted
	}
}
