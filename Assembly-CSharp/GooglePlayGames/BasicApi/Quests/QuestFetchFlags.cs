﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D17 RID: 3351
	[Flags]
	public enum QuestFetchFlags
	{
		// Token: 0x04004AD1 RID: 19153
		Upcoming = 1,
		// Token: 0x04004AD2 RID: 19154
		Open = 2,
		// Token: 0x04004AD3 RID: 19155
		Accepted = 4,
		// Token: 0x04004AD4 RID: 19156
		Completed = 8,
		// Token: 0x04004AD5 RID: 19157
		CompletedNotClaimed = 16,
		// Token: 0x04004AD6 RID: 19158
		Expired = 32,
		// Token: 0x04004AD7 RID: 19159
		EndingSoon = 64,
		// Token: 0x04004AD8 RID: 19160
		Failed = 128,
		// Token: 0x04004AD9 RID: 19161
		All = -1
	}
}
