﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D16 RID: 3350
	[Obsolete("Quests are being removed in 2018.")]
	public interface IQuestMilestone
	{
		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x060042DD RID: 17117
		string Id { get; }

		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x060042DE RID: 17118
		string EventId { get; }

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x060042DF RID: 17119
		string QuestId { get; }

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x060042E0 RID: 17120
		ulong CurrentCount { get; }

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x060042E1 RID: 17121
		ulong TargetCount { get; }

		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x060042E2 RID: 17122
		byte[] CompletionRewardData { get; }

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x060042E3 RID: 17123
		MilestoneState State { get; }
	}
}
