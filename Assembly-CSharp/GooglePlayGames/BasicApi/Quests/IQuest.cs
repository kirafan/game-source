﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D14 RID: 3348
	public interface IQuest
	{
		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x060042D3 RID: 17107
		string Id { get; }

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x060042D4 RID: 17108
		string Name { get; }

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x060042D5 RID: 17109
		string Description { get; }

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x060042D6 RID: 17110
		string BannerUrl { get; }

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x060042D7 RID: 17111
		string IconUrl { get; }

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x060042D8 RID: 17112
		DateTime StartTime { get; }

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x060042D9 RID: 17113
		DateTime ExpirationTime { get; }

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x060042DA RID: 17114
		DateTime? AcceptedTime { get; }

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x060042DB RID: 17115
		[Obsolete("Quests are being removed in 2018.")]
		IQuestMilestone Milestone { get; }

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x060042DC RID: 17116
		QuestState State { get; }
	}
}
