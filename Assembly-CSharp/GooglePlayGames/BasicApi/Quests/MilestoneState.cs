﻿using System;

namespace GooglePlayGames.BasicApi.Quests
{
	// Token: 0x02000D15 RID: 3349
	public enum MilestoneState
	{
		// Token: 0x04004ACC RID: 19148
		NotStarted = 1,
		// Token: 0x04004ACD RID: 19149
		NotCompleted,
		// Token: 0x04004ACE RID: 19150
		CompletedNotClaimed,
		// Token: 0x04004ACF RID: 19151
		Claimed
	}
}
