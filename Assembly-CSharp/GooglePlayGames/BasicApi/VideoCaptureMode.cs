﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CEB RID: 3307
	public enum VideoCaptureMode
	{
		// Token: 0x04004A2B RID: 18987
		Unknown = -1,
		// Token: 0x04004A2C RID: 18988
		File,
		// Token: 0x04004A2D RID: 18989
		Stream
	}
}
