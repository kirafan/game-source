﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE9 RID: 3305
	public enum LeaderboardTimeSpan
	{
		// Token: 0x04004A24 RID: 18980
		Daily = 1,
		// Token: 0x04004A25 RID: 18981
		Weekly,
		// Token: 0x04004A26 RID: 18982
		AllTime
	}
}
