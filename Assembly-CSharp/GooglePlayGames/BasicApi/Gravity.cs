﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CEE RID: 3310
	public enum Gravity
	{
		// Token: 0x04004A3B RID: 19003
		TOP = 48,
		// Token: 0x04004A3C RID: 19004
		BOTTOM = 80,
		// Token: 0x04004A3D RID: 19005
		LEFT = 3,
		// Token: 0x04004A3E RID: 19006
		RIGHT = 5,
		// Token: 0x04004A3F RID: 19007
		CENTER_HORIZONTAL = 1
	}
}
