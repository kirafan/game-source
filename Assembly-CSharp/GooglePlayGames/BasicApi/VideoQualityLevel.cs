﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CEC RID: 3308
	public enum VideoQualityLevel
	{
		// Token: 0x04004A2F RID: 18991
		Unknown = -1,
		// Token: 0x04004A30 RID: 18992
		SD,
		// Token: 0x04004A31 RID: 18993
		HD,
		// Token: 0x04004A32 RID: 18994
		XHD,
		// Token: 0x04004A33 RID: 18995
		FullHD
	}
}
