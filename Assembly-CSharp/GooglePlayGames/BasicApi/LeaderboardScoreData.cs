﻿using System;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CF6 RID: 3318
	public class LeaderboardScoreData
	{
		// Token: 0x060041DF RID: 16863 RVA: 0x0014162D File Offset: 0x0013FA2D
		internal LeaderboardScoreData(string leaderboardId)
		{
			this.mId = leaderboardId;
		}

		// Token: 0x060041E0 RID: 16864 RVA: 0x00141647 File Offset: 0x0013FA47
		internal LeaderboardScoreData(string leaderboardId, ResponseStatus status)
		{
			this.mId = leaderboardId;
			this.mStatus = status;
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x060041E1 RID: 16865 RVA: 0x00141668 File Offset: 0x0013FA68
		public bool Valid
		{
			get
			{
				return this.mStatus == ResponseStatus.Success || this.mStatus == ResponseStatus.SuccessWithStale;
			}
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x060041E2 RID: 16866 RVA: 0x00141682 File Offset: 0x0013FA82
		// (set) Token: 0x060041E3 RID: 16867 RVA: 0x0014168A File Offset: 0x0013FA8A
		public ResponseStatus Status
		{
			get
			{
				return this.mStatus;
			}
			internal set
			{
				this.mStatus = value;
			}
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x060041E4 RID: 16868 RVA: 0x00141693 File Offset: 0x0013FA93
		// (set) Token: 0x060041E5 RID: 16869 RVA: 0x0014169B File Offset: 0x0013FA9B
		public ulong ApproximateCount
		{
			get
			{
				return this.mApproxCount;
			}
			internal set
			{
				this.mApproxCount = value;
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x060041E6 RID: 16870 RVA: 0x001416A4 File Offset: 0x0013FAA4
		// (set) Token: 0x060041E7 RID: 16871 RVA: 0x001416AC File Offset: 0x0013FAAC
		public string Title
		{
			get
			{
				return this.mTitle;
			}
			internal set
			{
				this.mTitle = value;
			}
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x060041E8 RID: 16872 RVA: 0x001416B5 File Offset: 0x0013FAB5
		// (set) Token: 0x060041E9 RID: 16873 RVA: 0x001416BD File Offset: 0x0013FABD
		public string Id
		{
			get
			{
				return this.mId;
			}
			internal set
			{
				this.mId = value;
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x060041EA RID: 16874 RVA: 0x001416C6 File Offset: 0x0013FAC6
		// (set) Token: 0x060041EB RID: 16875 RVA: 0x001416CE File Offset: 0x0013FACE
		public IScore PlayerScore
		{
			get
			{
				return this.mPlayerScore;
			}
			internal set
			{
				this.mPlayerScore = value;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x060041EC RID: 16876 RVA: 0x001416D7 File Offset: 0x0013FAD7
		public IScore[] Scores
		{
			get
			{
				return this.mScores.ToArray();
			}
		}

		// Token: 0x060041ED RID: 16877 RVA: 0x001416E4 File Offset: 0x0013FAE4
		internal int AddScore(PlayGamesScore score)
		{
			this.mScores.Add(score);
			return this.mScores.Count;
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x060041EE RID: 16878 RVA: 0x001416FD File Offset: 0x0013FAFD
		// (set) Token: 0x060041EF RID: 16879 RVA: 0x00141705 File Offset: 0x0013FB05
		public ScorePageToken PrevPageToken
		{
			get
			{
				return this.mPrevPage;
			}
			internal set
			{
				this.mPrevPage = value;
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x060041F0 RID: 16880 RVA: 0x0014170E File Offset: 0x0013FB0E
		// (set) Token: 0x060041F1 RID: 16881 RVA: 0x00141716 File Offset: 0x0013FB16
		public ScorePageToken NextPageToken
		{
			get
			{
				return this.mNextPage;
			}
			internal set
			{
				this.mNextPage = value;
			}
		}

		// Token: 0x060041F2 RID: 16882 RVA: 0x0014171F File Offset: 0x0013FB1F
		public override string ToString()
		{
			return string.Format("[LeaderboardScoreData: mId={0},  mStatus={1}, mApproxCount={2}, mTitle={3}]", new object[]
			{
				this.mId,
				this.mStatus,
				this.mApproxCount,
				this.mTitle
			});
		}

		// Token: 0x04004A43 RID: 19011
		private string mId;

		// Token: 0x04004A44 RID: 19012
		private ResponseStatus mStatus;

		// Token: 0x04004A45 RID: 19013
		private ulong mApproxCount;

		// Token: 0x04004A46 RID: 19014
		private string mTitle;

		// Token: 0x04004A47 RID: 19015
		private IScore mPlayerScore;

		// Token: 0x04004A48 RID: 19016
		private ScorePageToken mPrevPage;

		// Token: 0x04004A49 RID: 19017
		private ScorePageToken mNextPage;

		// Token: 0x04004A4A RID: 19018
		private List<PlayGamesScore> mScores = new List<PlayGamesScore>();
	}
}
