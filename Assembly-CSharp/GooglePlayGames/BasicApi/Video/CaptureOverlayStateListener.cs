﻿using System;

namespace GooglePlayGames.BasicApi.Video
{
	// Token: 0x02000D26 RID: 3366
	public interface CaptureOverlayStateListener
	{
		// Token: 0x0600430C RID: 17164
		void OnCaptureOverlayStateChanged(VideoCaptureOverlayState overlayState);
	}
}
