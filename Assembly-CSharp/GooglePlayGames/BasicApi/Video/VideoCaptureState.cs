﻿using System;

namespace GooglePlayGames.BasicApi.Video
{
	// Token: 0x02000D28 RID: 3368
	public class VideoCaptureState
	{
		// Token: 0x06004316 RID: 17174 RVA: 0x001428D1 File Offset: 0x00140CD1
		internal VideoCaptureState(bool isCapturing, VideoCaptureMode captureMode, VideoQualityLevel qualityLevel, bool isOverlayVisible, bool isPaused)
		{
			this.mIsCapturing = isCapturing;
			this.mCaptureMode = captureMode;
			this.mQualityLevel = qualityLevel;
			this.mIsOverlayVisible = isOverlayVisible;
			this.mIsPaused = isPaused;
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06004317 RID: 17175 RVA: 0x001428FE File Offset: 0x00140CFE
		public bool IsCapturing
		{
			get
			{
				return this.mIsCapturing;
			}
		}

		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06004318 RID: 17176 RVA: 0x00142906 File Offset: 0x00140D06
		public VideoCaptureMode CaptureMode
		{
			get
			{
				return this.mCaptureMode;
			}
		}

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06004319 RID: 17177 RVA: 0x0014290E File Offset: 0x00140D0E
		public VideoQualityLevel QualityLevel
		{
			get
			{
				return this.mQualityLevel;
			}
		}

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x0600431A RID: 17178 RVA: 0x00142916 File Offset: 0x00140D16
		public bool IsOverlayVisible
		{
			get
			{
				return this.mIsOverlayVisible;
			}
		}

		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x0600431B RID: 17179 RVA: 0x0014291E File Offset: 0x00140D1E
		public bool IsPaused
		{
			get
			{
				return this.mIsPaused;
			}
		}

		// Token: 0x0600431C RID: 17180 RVA: 0x00142928 File Offset: 0x00140D28
		public override string ToString()
		{
			return string.Format("[VideoCaptureState: mIsCapturing={0}, mCaptureMode={1}, mQualityLevel={2}, mIsOverlayVisible={3}, mIsPaused={4}]", new object[]
			{
				this.mIsCapturing,
				this.mCaptureMode.ToString(),
				this.mQualityLevel.ToString(),
				this.mIsOverlayVisible,
				this.mIsPaused
			});
		}

		// Token: 0x04004B1A RID: 19226
		private bool mIsCapturing;

		// Token: 0x04004B1B RID: 19227
		private VideoCaptureMode mCaptureMode;

		// Token: 0x04004B1C RID: 19228
		private VideoQualityLevel mQualityLevel;

		// Token: 0x04004B1D RID: 19229
		private bool mIsOverlayVisible;

		// Token: 0x04004B1E RID: 19230
		private bool mIsPaused;
	}
}
