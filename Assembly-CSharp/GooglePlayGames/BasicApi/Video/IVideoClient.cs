﻿using System;

namespace GooglePlayGames.BasicApi.Video
{
	// Token: 0x02000D29 RID: 3369
	public interface IVideoClient
	{
		// Token: 0x0600431D RID: 17181
		void GetCaptureCapabilities(Action<ResponseStatus, VideoCapabilities> callback);

		// Token: 0x0600431E RID: 17182
		void ShowCaptureOverlay();

		// Token: 0x0600431F RID: 17183
		void GetCaptureState(Action<ResponseStatus, VideoCaptureState> callback);

		// Token: 0x06004320 RID: 17184
		void IsCaptureAvailable(VideoCaptureMode captureMode, Action<ResponseStatus, bool> callback);

		// Token: 0x06004321 RID: 17185
		bool IsCaptureSupported();

		// Token: 0x06004322 RID: 17186
		void RegisterCaptureOverlayStateChangedListener(CaptureOverlayStateListener listener);

		// Token: 0x06004323 RID: 17187
		void UnregisterCaptureOverlayStateChangedListener();
	}
}
