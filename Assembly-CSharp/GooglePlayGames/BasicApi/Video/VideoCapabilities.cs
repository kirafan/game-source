﻿using System;
using System.Linq;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Video
{
	// Token: 0x02000D27 RID: 3367
	public class VideoCapabilities
	{
		// Token: 0x0600430D RID: 17165 RVA: 0x00142776 File Offset: 0x00140B76
		internal VideoCapabilities(bool isCameraSupported, bool isMicSupported, bool isWriteStorageSupported, bool[] captureModesSupported, bool[] qualityLevelsSupported)
		{
			this.mIsCameraSupported = isCameraSupported;
			this.mIsMicSupported = isMicSupported;
			this.mIsWriteStorageSupported = isWriteStorageSupported;
			this.mCaptureModesSupported = captureModesSupported;
			this.mQualityLevelsSupported = qualityLevelsSupported;
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x0600430E RID: 17166 RVA: 0x001427A3 File Offset: 0x00140BA3
		public bool IsCameraSupported
		{
			get
			{
				return this.mIsCameraSupported;
			}
		}

		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x0600430F RID: 17167 RVA: 0x001427AB File Offset: 0x00140BAB
		public bool IsMicSupported
		{
			get
			{
				return this.mIsMicSupported;
			}
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06004310 RID: 17168 RVA: 0x001427B3 File Offset: 0x00140BB3
		public bool IsWriteStorageSupported
		{
			get
			{
				return this.mIsWriteStorageSupported;
			}
		}

		// Token: 0x06004311 RID: 17169 RVA: 0x001427BB File Offset: 0x00140BBB
		public bool SupportsCaptureMode(VideoCaptureMode captureMode)
		{
			if (captureMode != VideoCaptureMode.Unknown)
			{
				return this.mCaptureModesSupported[(int)captureMode];
			}
			Logger.w("SupportsCaptureMode called with an unknown captureMode.");
			return false;
		}

		// Token: 0x06004312 RID: 17170 RVA: 0x001427D8 File Offset: 0x00140BD8
		public bool SupportsQualityLevel(VideoQualityLevel qualityLevel)
		{
			if (qualityLevel != VideoQualityLevel.Unknown)
			{
				return this.mQualityLevelsSupported[(int)qualityLevel];
			}
			Logger.w("SupportsCaptureMode called with an unknown qualityLevel.");
			return false;
		}

		// Token: 0x06004313 RID: 17171 RVA: 0x001427F8 File Offset: 0x00140BF8
		public override string ToString()
		{
			string format = "[VideoCapabilities: mIsCameraSupported={0}, mIsMicSupported={1}, mIsWriteStorageSupported={2}, mCaptureModesSupported={3}, mQualityLevelsSupported={4}]";
			object[] array = new object[5];
			array[0] = this.mIsCameraSupported;
			array[1] = this.mIsMicSupported;
			array[2] = this.mIsWriteStorageSupported;
			array[3] = string.Join(",", (from p in this.mCaptureModesSupported
			select p.ToString()).ToArray<string>());
			array[4] = string.Join(",", (from p in this.mQualityLevelsSupported
			select p.ToString()).ToArray<string>());
			return string.Format(format, array);
		}

		// Token: 0x04004B13 RID: 19219
		private bool mIsCameraSupported;

		// Token: 0x04004B14 RID: 19220
		private bool mIsMicSupported;

		// Token: 0x04004B15 RID: 19221
		private bool mIsWriteStorageSupported;

		// Token: 0x04004B16 RID: 19222
		private bool[] mCaptureModesSupported;

		// Token: 0x04004B17 RID: 19223
		private bool[] mQualityLevelsSupported;
	}
}
