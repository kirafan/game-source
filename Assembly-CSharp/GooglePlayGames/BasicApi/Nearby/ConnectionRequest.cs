﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D06 RID: 3334
	public struct ConnectionRequest
	{
		// Token: 0x06004251 RID: 16977 RVA: 0x0014201B File Offset: 0x0014041B
		public ConnectionRequest(string remoteEndpointId, string remoteDeviceId, string remoteEndpointName, string serviceId, byte[] payload)
		{
			Logger.d("Constructing ConnectionRequest");
			this.mRemoteEndpoint = new EndpointDetails(remoteEndpointId, remoteDeviceId, remoteEndpointName, serviceId);
			this.mPayload = Misc.CheckNotNull<byte[]>(payload);
		}

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x06004252 RID: 16978 RVA: 0x00142044 File Offset: 0x00140444
		public EndpointDetails RemoteEndpoint
		{
			get
			{
				return this.mRemoteEndpoint;
			}
		}

		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x06004253 RID: 16979 RVA: 0x0014204C File Offset: 0x0014044C
		public byte[] Payload
		{
			get
			{
				return this.mPayload;
			}
		}

		// Token: 0x04004A87 RID: 19079
		private readonly EndpointDetails mRemoteEndpoint;

		// Token: 0x04004A88 RID: 19080
		private readonly byte[] mPayload;
	}
}
