﻿using System;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0C RID: 3340
	public interface IMessageListener
	{
		// Token: 0x06004288 RID: 17032
		void OnMessageReceived(string remoteEndpointId, byte[] data, bool isReliableMessage);

		// Token: 0x06004289 RID: 17033
		void OnRemoteEndpointDisconnected(string remoteEndpointId);
	}
}
