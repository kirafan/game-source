﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0A RID: 3338
	public struct EndpointDetails
	{
		// Token: 0x06004272 RID: 17010 RVA: 0x001421F7 File Offset: 0x001405F7
		public EndpointDetails(string endpointId, string deviceId, string name, string serviceId)
		{
			this.mEndpointId = Misc.CheckNotNull<string>(endpointId);
			this.mDeviceId = Misc.CheckNotNull<string>(deviceId);
			this.mName = Misc.CheckNotNull<string>(name);
			this.mServiceId = Misc.CheckNotNull<string>(serviceId);
		}

		// Token: 0x1700042D RID: 1069
		// (get) Token: 0x06004273 RID: 17011 RVA: 0x0014222A File Offset: 0x0014062A
		public string EndpointId
		{
			get
			{
				return this.mEndpointId;
			}
		}

		// Token: 0x1700042E RID: 1070
		// (get) Token: 0x06004274 RID: 17012 RVA: 0x00142232 File Offset: 0x00140632
		public string DeviceId
		{
			get
			{
				return this.mDeviceId;
			}
		}

		// Token: 0x1700042F RID: 1071
		// (get) Token: 0x06004275 RID: 17013 RVA: 0x0014223A File Offset: 0x0014063A
		public string Name
		{
			get
			{
				return this.mName;
			}
		}

		// Token: 0x17000430 RID: 1072
		// (get) Token: 0x06004276 RID: 17014 RVA: 0x00142242 File Offset: 0x00140642
		public string ServiceId
		{
			get
			{
				return this.mServiceId;
			}
		}

		// Token: 0x04004A95 RID: 19093
		private readonly string mEndpointId;

		// Token: 0x04004A96 RID: 19094
		private readonly string mDeviceId;

		// Token: 0x04004A97 RID: 19095
		private readonly string mName;

		// Token: 0x04004A98 RID: 19096
		private readonly string mServiceId;
	}
}
