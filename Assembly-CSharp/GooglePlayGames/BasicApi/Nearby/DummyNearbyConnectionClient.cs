﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D09 RID: 3337
	public class DummyNearbyConnectionClient : INearbyConnectionClient
	{
		// Token: 0x06004261 RID: 16993 RVA: 0x00142108 File Offset: 0x00140508
		public int MaxUnreliableMessagePayloadLength()
		{
			return 1168;
		}

		// Token: 0x06004262 RID: 16994 RVA: 0x0014210F File Offset: 0x0014050F
		public int MaxReliableMessagePayloadLength()
		{
			return 4096;
		}

		// Token: 0x06004263 RID: 16995 RVA: 0x00142116 File Offset: 0x00140516
		public void SendReliable(List<string> recipientEndpointIds, byte[] payload)
		{
			Debug.LogError("SendReliable called from dummy implementation");
		}

		// Token: 0x06004264 RID: 16996 RVA: 0x00142122 File Offset: 0x00140522
		public void SendUnreliable(List<string> recipientEndpointIds, byte[] payload)
		{
			Debug.LogError("SendUnreliable called from dummy implementation");
		}

		// Token: 0x06004265 RID: 16997 RVA: 0x00142130 File Offset: 0x00140530
		public void StartAdvertising(string name, List<string> appIdentifiers, TimeSpan? advertisingDuration, Action<AdvertisingResult> resultCallback, Action<ConnectionRequest> connectionRequestCallback)
		{
			AdvertisingResult obj = new AdvertisingResult(ResponseStatus.LicenseCheckFailed, string.Empty);
			resultCallback(obj);
		}

		// Token: 0x06004266 RID: 16998 RVA: 0x00142152 File Offset: 0x00140552
		public void StopAdvertising()
		{
			Debug.LogError("StopAvertising in dummy implementation called");
		}

		// Token: 0x06004267 RID: 16999 RVA: 0x00142160 File Offset: 0x00140560
		public void SendConnectionRequest(string name, string remoteEndpointId, byte[] payload, Action<ConnectionResponse> responseCallback, IMessageListener listener)
		{
			Debug.LogError("SendConnectionRequest called from dummy implementation");
			if (responseCallback != null)
			{
				ConnectionResponse obj = ConnectionResponse.Rejected(0L, string.Empty);
				responseCallback(obj);
			}
		}

		// Token: 0x06004268 RID: 17000 RVA: 0x00142193 File Offset: 0x00140593
		public void AcceptConnectionRequest(string remoteEndpointId, byte[] payload, IMessageListener listener)
		{
			Debug.LogError("AcceptConnectionRequest in dummy implementation called");
		}

		// Token: 0x06004269 RID: 17001 RVA: 0x0014219F File Offset: 0x0014059F
		public void StartDiscovery(string serviceId, TimeSpan? advertisingTimeout, IDiscoveryListener listener)
		{
			Debug.LogError("StartDiscovery in dummy implementation called");
		}

		// Token: 0x0600426A RID: 17002 RVA: 0x001421AB File Offset: 0x001405AB
		public void StopDiscovery(string serviceId)
		{
			Debug.LogError("StopDiscovery in dummy implementation called");
		}

		// Token: 0x0600426B RID: 17003 RVA: 0x001421B7 File Offset: 0x001405B7
		public void RejectConnectionRequest(string requestingEndpointId)
		{
			Debug.LogError("RejectConnectionRequest in dummy implementation called");
		}

		// Token: 0x0600426C RID: 17004 RVA: 0x001421C3 File Offset: 0x001405C3
		public void DisconnectFromEndpoint(string remoteEndpointId)
		{
			Debug.LogError("DisconnectFromEndpoint in dummy implementation called");
		}

		// Token: 0x0600426D RID: 17005 RVA: 0x001421CF File Offset: 0x001405CF
		public void StopAllConnections()
		{
			Debug.LogError("StopAllConnections in dummy implementation called");
		}

		// Token: 0x0600426E RID: 17006 RVA: 0x001421DB File Offset: 0x001405DB
		public string LocalEndpointId()
		{
			return string.Empty;
		}

		// Token: 0x0600426F RID: 17007 RVA: 0x001421E2 File Offset: 0x001405E2
		public string LocalDeviceId()
		{
			return "DummyDevice";
		}

		// Token: 0x06004270 RID: 17008 RVA: 0x001421E9 File Offset: 0x001405E9
		public string GetAppBundleId()
		{
			return "dummy.bundle.id";
		}

		// Token: 0x06004271 RID: 17009 RVA: 0x001421F0 File Offset: 0x001405F0
		public string GetServiceId()
		{
			return "dummy.service.id";
		}
	}
}
