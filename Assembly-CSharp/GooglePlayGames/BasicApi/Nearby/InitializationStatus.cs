﻿using System;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0E RID: 3342
	public enum InitializationStatus
	{
		// Token: 0x04004A9A RID: 19098
		Success,
		// Token: 0x04004A9B RID: 19099
		VersionUpdateRequired,
		// Token: 0x04004A9C RID: 19100
		InternalError
	}
}
