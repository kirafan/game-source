﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D07 RID: 3335
	public struct ConnectionResponse
	{
		// Token: 0x06004254 RID: 16980 RVA: 0x00142054 File Offset: 0x00140454
		private ConnectionResponse(long localClientId, string remoteEndpointId, ConnectionResponse.Status code, byte[] payload)
		{
			this.mLocalClientId = localClientId;
			this.mRemoteEndpointId = Misc.CheckNotNull<string>(remoteEndpointId);
			this.mResponseStatus = code;
			this.mPayload = Misc.CheckNotNull<byte[]>(payload);
		}

		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x06004255 RID: 16981 RVA: 0x0014207D File Offset: 0x0014047D
		public long LocalClientId
		{
			get
			{
				return this.mLocalClientId;
			}
		}

		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x06004256 RID: 16982 RVA: 0x00142085 File Offset: 0x00140485
		public string RemoteEndpointId
		{
			get
			{
				return this.mRemoteEndpointId;
			}
		}

		// Token: 0x1700042B RID: 1067
		// (get) Token: 0x06004257 RID: 16983 RVA: 0x0014208D File Offset: 0x0014048D
		public ConnectionResponse.Status ResponseStatus
		{
			get
			{
				return this.mResponseStatus;
			}
		}

		// Token: 0x1700042C RID: 1068
		// (get) Token: 0x06004258 RID: 16984 RVA: 0x00142095 File Offset: 0x00140495
		public byte[] Payload
		{
			get
			{
				return this.mPayload;
			}
		}

		// Token: 0x06004259 RID: 16985 RVA: 0x0014209D File Offset: 0x0014049D
		public static ConnectionResponse Rejected(long localClientId, string remoteEndpointId)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.Rejected, ConnectionResponse.EmptyPayload);
		}

		// Token: 0x0600425A RID: 16986 RVA: 0x001420AC File Offset: 0x001404AC
		public static ConnectionResponse NetworkNotConnected(long localClientId, string remoteEndpointId)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.ErrorNetworkNotConnected, ConnectionResponse.EmptyPayload);
		}

		// Token: 0x0600425B RID: 16987 RVA: 0x001420BB File Offset: 0x001404BB
		public static ConnectionResponse InternalError(long localClientId, string remoteEndpointId)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.ErrorInternal, ConnectionResponse.EmptyPayload);
		}

		// Token: 0x0600425C RID: 16988 RVA: 0x001420CA File Offset: 0x001404CA
		public static ConnectionResponse EndpointNotConnected(long localClientId, string remoteEndpointId)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.ErrorEndpointNotConnected, ConnectionResponse.EmptyPayload);
		}

		// Token: 0x0600425D RID: 16989 RVA: 0x001420D9 File Offset: 0x001404D9
		public static ConnectionResponse Accepted(long localClientId, string remoteEndpointId, byte[] payload)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.Accepted, payload);
		}

		// Token: 0x0600425E RID: 16990 RVA: 0x001420E4 File Offset: 0x001404E4
		public static ConnectionResponse AlreadyConnected(long localClientId, string remoteEndpointId)
		{
			return new ConnectionResponse(localClientId, remoteEndpointId, ConnectionResponse.Status.ErrorAlreadyConnected, ConnectionResponse.EmptyPayload);
		}

		// Token: 0x04004A89 RID: 19081
		private static readonly byte[] EmptyPayload = new byte[0];

		// Token: 0x04004A8A RID: 19082
		private readonly long mLocalClientId;

		// Token: 0x04004A8B RID: 19083
		private readonly string mRemoteEndpointId;

		// Token: 0x04004A8C RID: 19084
		private readonly ConnectionResponse.Status mResponseStatus;

		// Token: 0x04004A8D RID: 19085
		private readonly byte[] mPayload;

		// Token: 0x02000D08 RID: 3336
		public enum Status
		{
			// Token: 0x04004A8F RID: 19087
			Accepted,
			// Token: 0x04004A90 RID: 19088
			Rejected,
			// Token: 0x04004A91 RID: 19089
			ErrorInternal,
			// Token: 0x04004A92 RID: 19090
			ErrorNetworkNotConnected,
			// Token: 0x04004A93 RID: 19091
			ErrorEndpointNotConnected,
			// Token: 0x04004A94 RID: 19092
			ErrorAlreadyConnected
		}
	}
}
