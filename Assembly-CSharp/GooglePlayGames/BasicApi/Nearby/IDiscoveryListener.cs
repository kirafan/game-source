﻿using System;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0D RID: 3341
	public interface IDiscoveryListener
	{
		// Token: 0x0600428A RID: 17034
		void OnEndpointFound(EndpointDetails discoveredEndpoint);

		// Token: 0x0600428B RID: 17035
		void OnEndpointLost(string lostEndpointId);
	}
}
