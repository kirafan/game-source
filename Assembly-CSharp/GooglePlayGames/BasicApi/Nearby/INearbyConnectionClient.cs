﻿using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0B RID: 3339
	public interface INearbyConnectionClient
	{
		// Token: 0x06004277 RID: 17015
		int MaxUnreliableMessagePayloadLength();

		// Token: 0x06004278 RID: 17016
		int MaxReliableMessagePayloadLength();

		// Token: 0x06004279 RID: 17017
		void SendReliable(List<string> recipientEndpointIds, byte[] payload);

		// Token: 0x0600427A RID: 17018
		void SendUnreliable(List<string> recipientEndpointIds, byte[] payload);

		// Token: 0x0600427B RID: 17019
		void StartAdvertising(string name, List<string> appIdentifiers, TimeSpan? advertisingDuration, Action<AdvertisingResult> resultCallback, Action<ConnectionRequest> connectionRequestCallback);

		// Token: 0x0600427C RID: 17020
		void StopAdvertising();

		// Token: 0x0600427D RID: 17021
		void SendConnectionRequest(string name, string remoteEndpointId, byte[] payload, Action<ConnectionResponse> responseCallback, IMessageListener listener);

		// Token: 0x0600427E RID: 17022
		void AcceptConnectionRequest(string remoteEndpointId, byte[] payload, IMessageListener listener);

		// Token: 0x0600427F RID: 17023
		void StartDiscovery(string serviceId, TimeSpan? advertisingTimeout, IDiscoveryListener listener);

		// Token: 0x06004280 RID: 17024
		void StopDiscovery(string serviceId);

		// Token: 0x06004281 RID: 17025
		void RejectConnectionRequest(string requestingEndpointId);

		// Token: 0x06004282 RID: 17026
		void DisconnectFromEndpoint(string remoteEndpointId);

		// Token: 0x06004283 RID: 17027
		void StopAllConnections();

		// Token: 0x06004284 RID: 17028
		string LocalEndpointId();

		// Token: 0x06004285 RID: 17029
		string LocalDeviceId();

		// Token: 0x06004286 RID: 17030
		string GetAppBundleId();

		// Token: 0x06004287 RID: 17031
		string GetServiceId();
	}
}
