﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D05 RID: 3333
	public struct AdvertisingResult
	{
		// Token: 0x0600424D RID: 16973 RVA: 0x00141FEB File Offset: 0x001403EB
		public AdvertisingResult(ResponseStatus status, string localEndpointName)
		{
			this.mStatus = status;
			this.mLocalEndpointName = Misc.CheckNotNull<string>(localEndpointName);
		}

		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x0600424E RID: 16974 RVA: 0x00142000 File Offset: 0x00140400
		public bool Succeeded
		{
			get
			{
				return this.mStatus == ResponseStatus.Success;
			}
		}

		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x0600424F RID: 16975 RVA: 0x0014200B File Offset: 0x0014040B
		public ResponseStatus Status
		{
			get
			{
				return this.mStatus;
			}
		}

		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06004250 RID: 16976 RVA: 0x00142013 File Offset: 0x00140413
		public string LocalEndpointName
		{
			get
			{
				return this.mLocalEndpointName;
			}
		}

		// Token: 0x04004A85 RID: 19077
		private readonly ResponseStatus mStatus;

		// Token: 0x04004A86 RID: 19078
		private readonly string mLocalEndpointName;
	}
}
