﻿using System;
using GooglePlayGames.OurUtils;

namespace GooglePlayGames.BasicApi.Nearby
{
	// Token: 0x02000D0F RID: 3343
	public struct NearbyConnectionConfiguration
	{
		// Token: 0x0600428C RID: 17036 RVA: 0x0014224A File Offset: 0x0014064A
		public NearbyConnectionConfiguration(Action<InitializationStatus> callback, long localClientId)
		{
			this.mInitializationCallback = Misc.CheckNotNull<Action<InitializationStatus>>(callback);
			this.mLocalClientId = localClientId;
		}

		// Token: 0x17000431 RID: 1073
		// (get) Token: 0x0600428D RID: 17037 RVA: 0x0014225F File Offset: 0x0014065F
		public long LocalClientId
		{
			get
			{
				return this.mLocalClientId;
			}
		}

		// Token: 0x17000432 RID: 1074
		// (get) Token: 0x0600428E RID: 17038 RVA: 0x00142267 File Offset: 0x00140667
		public Action<InitializationStatus> InitializationCallback
		{
			get
			{
				return this.mInitializationCallback;
			}
		}

		// Token: 0x04004A9D RID: 19101
		public const int MaxUnreliableMessagePayloadLength = 1168;

		// Token: 0x04004A9E RID: 19102
		public const int MaxReliableMessagePayloadLength = 4096;

		// Token: 0x04004A9F RID: 19103
		private readonly Action<InitializationStatus> mInitializationCallback;

		// Token: 0x04004AA0 RID: 19104
		private readonly long mLocalClientId;
	}
}
