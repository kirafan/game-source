﻿using System;
using GooglePlayGames.BasicApi.Multiplayer;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CF5 RID: 3317
	// (Invoke) Token: 0x060041DC RID: 16860
	public delegate void InvitationReceivedDelegate(Invitation invitation, bool shouldAutoAccept);
}
