﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CED RID: 3309
	public enum VideoCaptureOverlayState
	{
		// Token: 0x04004A35 RID: 18997
		Unknown = -1,
		// Token: 0x04004A36 RID: 18998
		Shown = 1,
		// Token: 0x04004A37 RID: 18999
		Started,
		// Token: 0x04004A38 RID: 19000
		Stopped,
		// Token: 0x04004A39 RID: 19001
		Dismissed
	}
}
