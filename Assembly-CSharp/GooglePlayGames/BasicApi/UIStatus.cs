﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CE7 RID: 3303
	public enum UIStatus
	{
		// Token: 0x04004A18 RID: 18968
		Valid = 1,
		// Token: 0x04004A19 RID: 18969
		InternalError = -2,
		// Token: 0x04004A1A RID: 18970
		NotAuthorized = -3,
		// Token: 0x04004A1B RID: 18971
		VersionUpdateRequired = -4,
		// Token: 0x04004A1C RID: 18972
		Timeout = -5,
		// Token: 0x04004A1D RID: 18973
		UserClosedUI = -6,
		// Token: 0x04004A1E RID: 18974
		UiBusy = -12,
		// Token: 0x04004A1F RID: 18975
		LeftRoom = -18
	}
}
