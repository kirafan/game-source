﻿using System;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000D12 RID: 3346
	public class PlayerStats
	{
		// Token: 0x060042B2 RID: 17074 RVA: 0x001424AE File Offset: 0x001408AE
		public PlayerStats()
		{
			this.Valid = false;
		}

		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x060042B3 RID: 17075 RVA: 0x001424BD File Offset: 0x001408BD
		// (set) Token: 0x060042B4 RID: 17076 RVA: 0x001424C5 File Offset: 0x001408C5
		public bool Valid { get; set; }

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x060042B5 RID: 17077 RVA: 0x001424CE File Offset: 0x001408CE
		// (set) Token: 0x060042B6 RID: 17078 RVA: 0x001424D6 File Offset: 0x001408D6
		public int NumberOfPurchases { get; set; }

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x060042B7 RID: 17079 RVA: 0x001424DF File Offset: 0x001408DF
		// (set) Token: 0x060042B8 RID: 17080 RVA: 0x001424E7 File Offset: 0x001408E7
		public float AvgSessonLength { get; set; }

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x060042B9 RID: 17081 RVA: 0x001424F0 File Offset: 0x001408F0
		// (set) Token: 0x060042BA RID: 17082 RVA: 0x001424F8 File Offset: 0x001408F8
		public int DaysSinceLastPlayed { get; set; }

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x060042BB RID: 17083 RVA: 0x00142501 File Offset: 0x00140901
		// (set) Token: 0x060042BC RID: 17084 RVA: 0x00142509 File Offset: 0x00140909
		public int NumberOfSessions { get; set; }

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x060042BD RID: 17085 RVA: 0x00142512 File Offset: 0x00140912
		// (set) Token: 0x060042BE RID: 17086 RVA: 0x0014251A File Offset: 0x0014091A
		public float SessPercentile { get; set; }

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x060042BF RID: 17087 RVA: 0x00142523 File Offset: 0x00140923
		// (set) Token: 0x060042C0 RID: 17088 RVA: 0x0014252B File Offset: 0x0014092B
		public float SpendPercentile { get; set; }

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x060042C1 RID: 17089 RVA: 0x00142534 File Offset: 0x00140934
		// (set) Token: 0x060042C2 RID: 17090 RVA: 0x0014253C File Offset: 0x0014093C
		public float SpendProbability { get; set; }

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x060042C3 RID: 17091 RVA: 0x00142545 File Offset: 0x00140945
		// (set) Token: 0x060042C4 RID: 17092 RVA: 0x0014254D File Offset: 0x0014094D
		public float ChurnProbability { get; set; }

		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x060042C5 RID: 17093 RVA: 0x00142556 File Offset: 0x00140956
		// (set) Token: 0x060042C6 RID: 17094 RVA: 0x0014255E File Offset: 0x0014095E
		public float HighSpenderProbability { get; set; }

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x060042C7 RID: 17095 RVA: 0x00142567 File Offset: 0x00140967
		// (set) Token: 0x060042C8 RID: 17096 RVA: 0x0014256F File Offset: 0x0014096F
		public float TotalSpendNext28Days { get; set; }

		// Token: 0x060042C9 RID: 17097 RVA: 0x00142578 File Offset: 0x00140978
		public bool HasNumberOfPurchases()
		{
			return this.NumberOfPurchases != (int)PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CA RID: 17098 RVA: 0x0014258B File Offset: 0x0014098B
		public bool HasAvgSessonLength()
		{
			return this.AvgSessonLength != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CB RID: 17099 RVA: 0x0014259D File Offset: 0x0014099D
		public bool HasDaysSinceLastPlayed()
		{
			return this.DaysSinceLastPlayed != (int)PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CC RID: 17100 RVA: 0x001425B0 File Offset: 0x001409B0
		public bool HasNumberOfSessions()
		{
			return this.NumberOfSessions != (int)PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CD RID: 17101 RVA: 0x001425C3 File Offset: 0x001409C3
		public bool HasSessPercentile()
		{
			return this.SessPercentile != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CE RID: 17102 RVA: 0x001425D5 File Offset: 0x001409D5
		public bool HasSpendPercentile()
		{
			return this.SpendPercentile != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042CF RID: 17103 RVA: 0x001425E7 File Offset: 0x001409E7
		public bool HasChurnProbability()
		{
			return this.ChurnProbability != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042D0 RID: 17104 RVA: 0x001425F9 File Offset: 0x001409F9
		public bool HasHighSpenderProbability()
		{
			return this.HighSpenderProbability != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x060042D1 RID: 17105 RVA: 0x0014260B File Offset: 0x00140A0B
		public bool HasTotalSpendNext28Days()
		{
			return this.TotalSpendNext28Days != PlayerStats.UNSET_VALUE;
		}

		// Token: 0x04004AB8 RID: 19128
		private static float UNSET_VALUE = -1f;
	}
}
