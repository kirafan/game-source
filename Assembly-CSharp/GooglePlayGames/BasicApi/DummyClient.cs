﻿using System;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.BasicApi.Video;
using GooglePlayGames.OurUtils;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames.BasicApi
{
	// Token: 0x02000CF0 RID: 3312
	public class DummyClient : IPlayGamesClient
	{
		// Token: 0x06004187 RID: 16775 RVA: 0x00141418 File Offset: 0x0013F818
		public void Authenticate(Action<bool, string> callback, bool silent)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false, "Not implemented on this platform");
			}
		}

		// Token: 0x06004188 RID: 16776 RVA: 0x00141431 File Offset: 0x0013F831
		public bool IsAuthenticated()
		{
			DummyClient.LogUsage();
			return false;
		}

		// Token: 0x06004189 RID: 16777 RVA: 0x00141439 File Offset: 0x0013F839
		public void SignOut()
		{
			DummyClient.LogUsage();
		}

		// Token: 0x0600418A RID: 16778 RVA: 0x00141440 File Offset: 0x0013F840
		public string GetIdToken()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x0600418B RID: 16779 RVA: 0x00141448 File Offset: 0x0013F848
		public string GetUserId()
		{
			DummyClient.LogUsage();
			return "DummyID";
		}

		// Token: 0x0600418C RID: 16780 RVA: 0x00141454 File Offset: 0x0013F854
		public string GetServerAuthCode()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x0600418D RID: 16781 RVA: 0x0014145C File Offset: 0x0013F85C
		public void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback)
		{
			DummyClient.LogUsage();
			callback(null);
		}

		// Token: 0x0600418E RID: 16782 RVA: 0x0014146A File Offset: 0x0013F86A
		public string GetUserEmail()
		{
			return string.Empty;
		}

		// Token: 0x0600418F RID: 16783 RVA: 0x00141471 File Offset: 0x0013F871
		public void GetPlayerStats(Action<CommonStatusCodes, PlayerStats> callback)
		{
			DummyClient.LogUsage();
			callback(CommonStatusCodes.ApiNotConnected, new PlayerStats());
		}

		// Token: 0x06004190 RID: 16784 RVA: 0x00141485 File Offset: 0x0013F885
		public string GetUserDisplayName()
		{
			DummyClient.LogUsage();
			return "Player";
		}

		// Token: 0x06004191 RID: 16785 RVA: 0x00141491 File Offset: 0x0013F891
		public string GetUserImageUrl()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x06004192 RID: 16786 RVA: 0x00141499 File Offset: 0x0013F899
		public void LoadUsers(string[] userIds, Action<IUserProfile[]> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(null);
			}
		}

		// Token: 0x06004193 RID: 16787 RVA: 0x001414AD File Offset: 0x0013F8AD
		public void LoadAchievements(Action<Achievement[]> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(null);
			}
		}

		// Token: 0x06004194 RID: 16788 RVA: 0x001414C1 File Offset: 0x0013F8C1
		public Achievement GetAchievement(string achId)
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x06004195 RID: 16789 RVA: 0x001414C9 File Offset: 0x0013F8C9
		public void UnlockAchievement(string achId, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x06004196 RID: 16790 RVA: 0x001414DD File Offset: 0x0013F8DD
		public void RevealAchievement(string achId, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x06004197 RID: 16791 RVA: 0x001414F1 File Offset: 0x0013F8F1
		public void IncrementAchievement(string achId, int steps, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x06004198 RID: 16792 RVA: 0x00141505 File Offset: 0x0013F905
		public void SetStepsAtLeast(string achId, int steps, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x06004199 RID: 16793 RVA: 0x00141519 File Offset: 0x0013F919
		public void ShowAchievementsUI(Action<UIStatus> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(UIStatus.VersionUpdateRequired);
			}
		}

		// Token: 0x0600419A RID: 16794 RVA: 0x0014152E File Offset: 0x0013F92E
		public void ShowLeaderboardUI(string leaderboardId, LeaderboardTimeSpan span, Action<UIStatus> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(UIStatus.VersionUpdateRequired);
			}
		}

		// Token: 0x0600419B RID: 16795 RVA: 0x00141543 File Offset: 0x0013F943
		public int LeaderboardMaxResults()
		{
			return 25;
		}

		// Token: 0x0600419C RID: 16796 RVA: 0x00141547 File Offset: 0x0013F947
		public void LoadScores(string leaderboardId, LeaderboardStart start, int rowCount, LeaderboardCollection collection, LeaderboardTimeSpan timeSpan, Action<LeaderboardScoreData> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(new LeaderboardScoreData(leaderboardId, ResponseStatus.LicenseCheckFailed));
			}
		}

		// Token: 0x0600419D RID: 16797 RVA: 0x00141563 File Offset: 0x0013F963
		public void LoadMoreScores(ScorePageToken token, int rowCount, Action<LeaderboardScoreData> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(new LeaderboardScoreData(token.LeaderboardId, ResponseStatus.LicenseCheckFailed));
			}
		}

		// Token: 0x0600419E RID: 16798 RVA: 0x00141582 File Offset: 0x0013F982
		public void SubmitScore(string leaderboardId, long score, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x0600419F RID: 16799 RVA: 0x00141596 File Offset: 0x0013F996
		public void SubmitScore(string leaderboardId, long score, string metadata, Action<bool> callback)
		{
			DummyClient.LogUsage();
			if (callback != null)
			{
				callback(false);
			}
		}

		// Token: 0x060041A0 RID: 16800 RVA: 0x001415AC File Offset: 0x0013F9AC
		public IRealTimeMultiplayerClient GetRtmpClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A1 RID: 16801 RVA: 0x001415B4 File Offset: 0x0013F9B4
		public ITurnBasedMultiplayerClient GetTbmpClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A2 RID: 16802 RVA: 0x001415BC File Offset: 0x0013F9BC
		public ISavedGameClient GetSavedGameClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A3 RID: 16803 RVA: 0x001415C4 File Offset: 0x0013F9C4
		public IEventsClient GetEventsClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A4 RID: 16804 RVA: 0x001415CC File Offset: 0x0013F9CC
		[Obsolete("Quests are being removed in 2018.")]
		public IQuestsClient GetQuestsClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A5 RID: 16805 RVA: 0x001415D4 File Offset: 0x0013F9D4
		public IVideoClient GetVideoClient()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A6 RID: 16806 RVA: 0x001415DC File Offset: 0x0013F9DC
		public void RegisterInvitationDelegate(InvitationReceivedDelegate invitationDelegate)
		{
			DummyClient.LogUsage();
		}

		// Token: 0x060041A7 RID: 16807 RVA: 0x001415E3 File Offset: 0x0013F9E3
		public Invitation GetInvitationFromNotification()
		{
			DummyClient.LogUsage();
			return null;
		}

		// Token: 0x060041A8 RID: 16808 RVA: 0x001415EB File Offset: 0x0013F9EB
		public bool HasInvitationFromNotification()
		{
			DummyClient.LogUsage();
			return false;
		}

		// Token: 0x060041A9 RID: 16809 RVA: 0x001415F3 File Offset: 0x0013F9F3
		public void LoadFriends(Action<bool> callback)
		{
			DummyClient.LogUsage();
			callback(false);
		}

		// Token: 0x060041AA RID: 16810 RVA: 0x00141601 File Offset: 0x0013FA01
		public IUserProfile[] GetFriends()
		{
			DummyClient.LogUsage();
			return new IUserProfile[0];
		}

		// Token: 0x060041AB RID: 16811 RVA: 0x0014160E File Offset: 0x0013FA0E
		public IntPtr GetApiClient()
		{
			DummyClient.LogUsage();
			return IntPtr.Zero;
		}

		// Token: 0x060041AC RID: 16812 RVA: 0x0014161A File Offset: 0x0013FA1A
		public void SetGravityForPopups(Gravity gravity)
		{
			DummyClient.LogUsage();
		}

		// Token: 0x060041AD RID: 16813 RVA: 0x00141621 File Offset: 0x0013FA21
		private static void LogUsage()
		{
			Logger.d("Received method call on DummyClient - using stub implementation.");
		}
	}
}
