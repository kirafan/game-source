﻿using System;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Events;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.BasicApi.Quests;
using GooglePlayGames.BasicApi.SavedGame;
using GooglePlayGames.BasicApi.Video;
using GooglePlayGames.OurUtils;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D2F RID: 3375
	public class PlayGamesPlatform : ISocialPlatform
	{
		// Token: 0x0600436A RID: 17258 RVA: 0x00143241 File Offset: 0x00141641
		internal PlayGamesPlatform(IPlayGamesClient client)
		{
			this.mClient = Misc.CheckNotNull<IPlayGamesClient>(client);
			this.mLocalUser = new PlayGamesLocalUser(this);
			this.mConfiguration = PlayGamesClientConfiguration.DefaultConfiguration;
		}

		// Token: 0x0600436B RID: 17259 RVA: 0x00143277 File Offset: 0x00141677
		private PlayGamesPlatform(PlayGamesClientConfiguration configuration)
		{
			GooglePlayGames.OurUtils.Logger.w("Creating new PlayGamesPlatform");
			this.mLocalUser = new PlayGamesLocalUser(this);
			this.mConfiguration = configuration;
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x0600436C RID: 17260 RVA: 0x001432A7 File Offset: 0x001416A7
		// (set) Token: 0x0600436D RID: 17261 RVA: 0x001432AE File Offset: 0x001416AE
		public static bool DebugLogEnabled
		{
			get
			{
				return GooglePlayGames.OurUtils.Logger.DebugLogEnabled;
			}
			set
			{
				GooglePlayGames.OurUtils.Logger.DebugLogEnabled = value;
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x0600436E RID: 17262 RVA: 0x001432B6 File Offset: 0x001416B6
		public static PlayGamesPlatform Instance
		{
			get
			{
				if (PlayGamesPlatform.sInstance == null)
				{
					GooglePlayGames.OurUtils.Logger.d("Instance was not initialized, using default configuration.");
					PlayGamesPlatform.InitializeInstance(PlayGamesClientConfiguration.DefaultConfiguration);
				}
				return PlayGamesPlatform.sInstance;
			}
		}

		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x0600436F RID: 17263 RVA: 0x001432DF File Offset: 0x001416DF
		public static INearbyConnectionClient Nearby
		{
			get
			{
				if (PlayGamesPlatform.sNearbyConnectionClient == null && !PlayGamesPlatform.sNearbyInitializePending)
				{
					PlayGamesPlatform.sNearbyInitializePending = true;
					PlayGamesPlatform.InitializeNearby(null);
				}
				return PlayGamesPlatform.sNearbyConnectionClient;
			}
		}

		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x06004370 RID: 17264 RVA: 0x0014330E File Offset: 0x0014170E
		public IRealTimeMultiplayerClient RealTime
		{
			get
			{
				return this.mClient.GetRtmpClient();
			}
		}

		// Token: 0x17000495 RID: 1173
		// (get) Token: 0x06004371 RID: 17265 RVA: 0x0014331B File Offset: 0x0014171B
		public ITurnBasedMultiplayerClient TurnBased
		{
			get
			{
				return this.mClient.GetTbmpClient();
			}
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x06004372 RID: 17266 RVA: 0x00143328 File Offset: 0x00141728
		public ISavedGameClient SavedGame
		{
			get
			{
				return this.mClient.GetSavedGameClient();
			}
		}

		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x06004373 RID: 17267 RVA: 0x00143335 File Offset: 0x00141735
		public IEventsClient Events
		{
			get
			{
				return this.mClient.GetEventsClient();
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x06004374 RID: 17268 RVA: 0x00143342 File Offset: 0x00141742
		[Obsolete("Quests are being removed in 2018.")]
		public IQuestsClient Quests
		{
			get
			{
				return this.mClient.GetQuestsClient();
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x06004375 RID: 17269 RVA: 0x0014334F File Offset: 0x0014174F
		public IVideoClient Video
		{
			get
			{
				return this.mClient.GetVideoClient();
			}
		}

		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x06004376 RID: 17270 RVA: 0x0014335C File Offset: 0x0014175C
		public ILocalUser localUser
		{
			get
			{
				return this.mLocalUser;
			}
		}

		// Token: 0x06004377 RID: 17271 RVA: 0x00143364 File Offset: 0x00141764
		public static void InitializeInstance(PlayGamesClientConfiguration configuration)
		{
			if (PlayGamesPlatform.sInstance != null)
			{
				GooglePlayGames.OurUtils.Logger.w("PlayGamesPlatform already initialized. Ignoring this call.");
				return;
			}
			PlayGamesPlatform.sInstance = new PlayGamesPlatform(configuration);
		}

		// Token: 0x06004378 RID: 17272 RVA: 0x0014338C File Offset: 0x0014178C
		public static void InitializeNearby(Action<INearbyConnectionClient> callback)
		{
			Debug.Log("Calling InitializeNearby!");
			if (PlayGamesPlatform.sNearbyConnectionClient == null)
			{
				NearbyConnectionClientFactory.Create(delegate(INearbyConnectionClient client)
				{
					Debug.Log("Nearby Client Created!!");
					PlayGamesPlatform.sNearbyConnectionClient = client;
					if (callback != null)
					{
						callback(client);
					}
					else
					{
						Debug.Log("Initialize Nearby callback is null");
					}
				});
			}
			else if (callback != null)
			{
				Debug.Log("Nearby Already initialized: calling callback directly");
				callback(PlayGamesPlatform.sNearbyConnectionClient);
			}
			else
			{
				Debug.Log("Nearby Already initialized");
			}
		}

		// Token: 0x06004379 RID: 17273 RVA: 0x00143408 File Offset: 0x00141808
		public static PlayGamesPlatform Activate()
		{
			GooglePlayGames.OurUtils.Logger.d("Activating PlayGamesPlatform.");
			Social.Active = PlayGamesPlatform.Instance;
			GooglePlayGames.OurUtils.Logger.d("PlayGamesPlatform activated: " + Social.Active);
			return PlayGamesPlatform.Instance;
		}

		// Token: 0x0600437A RID: 17274 RVA: 0x00143437 File Offset: 0x00141837
		public IntPtr GetApiClient()
		{
			return this.mClient.GetApiClient();
		}

		// Token: 0x0600437B RID: 17275 RVA: 0x00143444 File Offset: 0x00141844
		public void SetGravityForPopups(Gravity gravity)
		{
			this.mClient.SetGravityForPopups(gravity);
		}

		// Token: 0x0600437C RID: 17276 RVA: 0x00143452 File Offset: 0x00141852
		public void AddIdMapping(string fromId, string toId)
		{
			this.mIdMap[fromId] = toId;
		}

		// Token: 0x0600437D RID: 17277 RVA: 0x00143461 File Offset: 0x00141861
		public void Authenticate(Action<bool> callback)
		{
			this.Authenticate(callback, false);
		}

		// Token: 0x0600437E RID: 17278 RVA: 0x0014346B File Offset: 0x0014186B
		public void Authenticate(Action<bool, string> callback)
		{
			this.Authenticate(callback, false);
		}

		// Token: 0x0600437F RID: 17279 RVA: 0x00143478 File Offset: 0x00141878
		public void Authenticate(Action<bool> callback, bool silent)
		{
			this.Authenticate(delegate(bool success, string msg)
			{
				callback(success);
			}, silent);
		}

		// Token: 0x06004380 RID: 17280 RVA: 0x001434A5 File Offset: 0x001418A5
		public void Authenticate(Action<bool, string> callback, bool silent)
		{
			if (this.mClient == null)
			{
				GooglePlayGames.OurUtils.Logger.d("Creating platform-specific Play Games client.");
				this.mClient = PlayGamesClientFactory.GetPlatformPlayGamesClient(this.mConfiguration);
			}
			this.mClient.Authenticate(callback, silent);
		}

		// Token: 0x06004381 RID: 17281 RVA: 0x001434DA File Offset: 0x001418DA
		public void Authenticate(ILocalUser unused, Action<bool> callback)
		{
			this.Authenticate(callback, false);
		}

		// Token: 0x06004382 RID: 17282 RVA: 0x001434E4 File Offset: 0x001418E4
		public void Authenticate(ILocalUser unused, Action<bool, string> callback)
		{
			this.Authenticate(callback, false);
		}

		// Token: 0x06004383 RID: 17283 RVA: 0x001434EE File Offset: 0x001418EE
		public bool IsAuthenticated()
		{
			return this.mClient != null && this.mClient.IsAuthenticated();
		}

		// Token: 0x06004384 RID: 17284 RVA: 0x00143509 File Offset: 0x00141909
		public void SignOut()
		{
			if (this.mClient != null)
			{
				this.mClient.SignOut();
			}
			this.mLocalUser = new PlayGamesLocalUser(this);
		}

		// Token: 0x06004385 RID: 17285 RVA: 0x0014352D File Offset: 0x0014192D
		public void LoadUsers(string[] userIds, Action<IUserProfile[]> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("GetUserId() can only be called after authentication.");
				callback(new IUserProfile[0]);
				return;
			}
			this.mClient.LoadUsers(userIds, callback);
		}

		// Token: 0x06004386 RID: 17286 RVA: 0x0014355E File Offset: 0x0014195E
		public string GetUserId()
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("GetUserId() can only be called after authentication.");
				return "0";
			}
			return this.mClient.GetUserId();
		}

		// Token: 0x06004387 RID: 17287 RVA: 0x00143586 File Offset: 0x00141986
		public string GetIdToken()
		{
			if (this.mClient != null)
			{
				return this.mClient.GetIdToken();
			}
			GooglePlayGames.OurUtils.Logger.e("No client available, returning null.");
			return null;
		}

		// Token: 0x06004388 RID: 17288 RVA: 0x001435AA File Offset: 0x001419AA
		public string GetServerAuthCode()
		{
			if (this.mClient != null && this.mClient.IsAuthenticated())
			{
				return this.mClient.GetServerAuthCode();
			}
			return null;
		}

		// Token: 0x06004389 RID: 17289 RVA: 0x001435D4 File Offset: 0x001419D4
		public void GetAnotherServerAuthCode(bool reAuthenticateIfNeeded, Action<string> callback)
		{
			if (this.mClient != null && this.mClient.IsAuthenticated())
			{
				this.mClient.GetAnotherServerAuthCode(reAuthenticateIfNeeded, callback);
			}
			else if (this.mClient != null && reAuthenticateIfNeeded)
			{
				this.mClient.Authenticate(delegate(bool success, string msg)
				{
					if (success)
					{
						callback(this.mClient.GetServerAuthCode());
					}
					else
					{
						GooglePlayGames.OurUtils.Logger.e("Re-authentication failed: " + msg);
						callback(null);
					}
				}, false);
			}
			else
			{
				GooglePlayGames.OurUtils.Logger.e("Cannot call GetAnotherServerAuthCode: not authenticated");
				callback(null);
			}
		}

		// Token: 0x0600438A RID: 17290 RVA: 0x0014366B File Offset: 0x00141A6B
		public string GetUserEmail()
		{
			return this.mClient.GetUserEmail();
		}

		// Token: 0x0600438B RID: 17291 RVA: 0x00143678 File Offset: 0x00141A78
		public void GetPlayerStats(Action<CommonStatusCodes, PlayerStats> callback)
		{
			if (this.mClient != null && this.mClient.IsAuthenticated())
			{
				this.mClient.GetPlayerStats(callback);
			}
			else
			{
				GooglePlayGames.OurUtils.Logger.e("GetPlayerStats can only be called after authentication.");
				callback(CommonStatusCodes.SignInRequired, new PlayerStats());
			}
		}

		// Token: 0x0600438C RID: 17292 RVA: 0x001436C7 File Offset: 0x00141AC7
		public Achievement GetAchievement(string achievementId)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("GetAchievement can only be called after authentication.");
				return null;
			}
			return this.mClient.GetAchievement(achievementId);
		}

		// Token: 0x0600438D RID: 17293 RVA: 0x001436EC File Offset: 0x00141AEC
		public string GetUserDisplayName()
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("GetUserDisplayName can only be called after authentication.");
				return string.Empty;
			}
			return this.mClient.GetUserDisplayName();
		}

		// Token: 0x0600438E RID: 17294 RVA: 0x00143714 File Offset: 0x00141B14
		public string GetUserImageUrl()
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("GetUserImageUrl can only be called after authentication.");
				return null;
			}
			return this.mClient.GetUserImageUrl();
		}

		// Token: 0x0600438F RID: 17295 RVA: 0x00143738 File Offset: 0x00141B38
		public void ReportProgress(string achievementID, double progress, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("ReportProgress can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"ReportProgress, ",
				achievementID,
				", ",
				progress
			}));
			achievementID = this.MapId(achievementID);
			if (progress < 1E-06)
			{
				GooglePlayGames.OurUtils.Logger.d("Progress 0.00 interpreted as request to reveal.");
				this.mClient.RevealAchievement(achievementID, callback);
				return;
			}
			int num = 0;
			int num2 = 0;
			Achievement achievement = this.mClient.GetAchievement(achievementID);
			bool flag;
			if (achievement == null)
			{
				GooglePlayGames.OurUtils.Logger.w("Unable to locate achievement " + achievementID);
				GooglePlayGames.OurUtils.Logger.w("As a quick fix, assuming it's standard.");
				flag = false;
			}
			else
			{
				flag = achievement.IsIncremental;
				num = achievement.CurrentSteps;
				num2 = achievement.TotalSteps;
				GooglePlayGames.OurUtils.Logger.d("Achievement is " + ((!flag) ? "STANDARD" : "INCREMENTAL"));
				if (flag)
				{
					GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
					{
						"Current steps: ",
						num,
						"/",
						num2
					}));
				}
			}
			if (flag)
			{
				GooglePlayGames.OurUtils.Logger.d("Progress " + progress + " interpreted as incremental target (approximate).");
				if (progress >= 0.0 && progress <= 1.0)
				{
					GooglePlayGames.OurUtils.Logger.w("Progress " + progress + " is less than or equal to 1. You might be trying to use values in the range of [0,1], while values are expected to be within the range [0,100]. If you are using the latter, you can safely ignore this message.");
				}
				int num3 = (int)Math.Round(progress / 100.0 * (double)num2);
				int num4 = num3 - num;
				GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
				{
					"Target steps: ",
					num3,
					", cur steps:",
					num
				}));
				GooglePlayGames.OurUtils.Logger.d("Steps to increment: " + num4);
				if (num4 >= 0)
				{
					this.mClient.IncrementAchievement(achievementID, num4, callback);
				}
			}
			else if (progress >= 100.0)
			{
				GooglePlayGames.OurUtils.Logger.d("Progress " + progress + " interpreted as UNLOCK.");
				this.mClient.UnlockAchievement(achievementID, callback);
			}
			else
			{
				GooglePlayGames.OurUtils.Logger.d("Progress " + progress + " not enough to unlock non-incremental achievement.");
			}
		}

		// Token: 0x06004390 RID: 17296 RVA: 0x001439A0 File Offset: 0x00141DA0
		public void RevealAchievement(string achievementID, Action<bool> callback = null)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("RevealAchievement can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d("RevealAchievement: " + achievementID);
			achievementID = this.MapId(achievementID);
			this.mClient.RevealAchievement(achievementID, callback);
		}

		// Token: 0x06004391 RID: 17297 RVA: 0x001439F8 File Offset: 0x00141DF8
		public void UnlockAchievement(string achievementID, Action<bool> callback = null)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("UnlockAchievement can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d("UnlockAchievement: " + achievementID);
			achievementID = this.MapId(achievementID);
			this.mClient.UnlockAchievement(achievementID, callback);
		}

		// Token: 0x06004392 RID: 17298 RVA: 0x00143A50 File Offset: 0x00141E50
		public void IncrementAchievement(string achievementID, int steps, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("IncrementAchievement can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"IncrementAchievement: ",
				achievementID,
				", steps ",
				steps
			}));
			achievementID = this.MapId(achievementID);
			this.mClient.IncrementAchievement(achievementID, steps, callback);
		}

		// Token: 0x06004393 RID: 17299 RVA: 0x00143AC4 File Offset: 0x00141EC4
		public void SetStepsAtLeast(string achievementID, int steps, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("SetStepsAtLeast can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"SetStepsAtLeast: ",
				achievementID,
				", steps ",
				steps
			}));
			achievementID = this.MapId(achievementID);
			this.mClient.SetStepsAtLeast(achievementID, steps, callback);
		}

		// Token: 0x06004394 RID: 17300 RVA: 0x00143B38 File Offset: 0x00141F38
		public void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadAchievementDescriptions can only be called after authentication.");
				if (callback != null)
				{
					callback(null);
				}
				return;
			}
			this.mClient.LoadAchievements(delegate(Achievement[] ach)
			{
				IAchievementDescription[] array = new IAchievementDescription[ach.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new PlayGamesAchievement(ach[i]);
				}
				callback(array);
			});
		}

		// Token: 0x06004395 RID: 17301 RVA: 0x00143B98 File Offset: 0x00141F98
		public void LoadAchievements(Action<IAchievement[]> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadAchievements can only be called after authentication.");
				callback(null);
				return;
			}
			this.mClient.LoadAchievements(delegate(Achievement[] ach)
			{
				IAchievement[] array = new IAchievement[ach.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new PlayGamesAchievement(ach[i]);
				}
				callback(array);
			});
		}

		// Token: 0x06004396 RID: 17302 RVA: 0x00143BEB File Offset: 0x00141FEB
		public IAchievement CreateAchievement()
		{
			return new PlayGamesAchievement();
		}

		// Token: 0x06004397 RID: 17303 RVA: 0x00143BF4 File Offset: 0x00141FF4
		public void ReportScore(long score, string board, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("ReportScore can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"ReportScore: score=",
				score,
				", board=",
				board
			}));
			string leaderboardId = this.MapId(board);
			this.mClient.SubmitScore(leaderboardId, score, callback);
		}

		// Token: 0x06004398 RID: 17304 RVA: 0x00143C68 File Offset: 0x00142068
		public void ReportScore(long score, string board, string metadata, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("ReportScore can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"ReportScore: score=",
				score,
				", board=",
				board,
				" metadata=",
				metadata
			}));
			string leaderboardId = this.MapId(board);
			this.mClient.SubmitScore(leaderboardId, score, metadata, callback);
		}

		// Token: 0x06004399 RID: 17305 RVA: 0x00143CEC File Offset: 0x001420EC
		public void LoadScores(string leaderboardId, Action<IScore[]> callback)
		{
			this.LoadScores(leaderboardId, LeaderboardStart.PlayerCentered, this.mClient.LeaderboardMaxResults(), LeaderboardCollection.Public, LeaderboardTimeSpan.AllTime, delegate(LeaderboardScoreData scoreData)
			{
				callback(scoreData.Scores);
			});
		}

		// Token: 0x0600439A RID: 17306 RVA: 0x00143D27 File Offset: 0x00142127
		public void LoadScores(string leaderboardId, LeaderboardStart start, int rowCount, LeaderboardCollection collection, LeaderboardTimeSpan timeSpan, Action<LeaderboardScoreData> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadScores can only be called after authentication.");
				callback(new LeaderboardScoreData(leaderboardId, ResponseStatus.NotAuthorized));
				return;
			}
			this.mClient.LoadScores(leaderboardId, start, rowCount, collection, timeSpan, callback);
		}

		// Token: 0x0600439B RID: 17307 RVA: 0x00143D62 File Offset: 0x00142162
		public void LoadMoreScores(ScorePageToken token, int rowCount, Action<LeaderboardScoreData> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadMoreScores can only be called after authentication.");
				callback(new LeaderboardScoreData(token.LeaderboardId, ResponseStatus.NotAuthorized));
				return;
			}
			this.mClient.LoadMoreScores(token, rowCount, callback);
		}

		// Token: 0x0600439C RID: 17308 RVA: 0x00143D9B File Offset: 0x0014219B
		public ILeaderboard CreateLeaderboard()
		{
			return new PlayGamesLeaderboard(this.mDefaultLbUi);
		}

		// Token: 0x0600439D RID: 17309 RVA: 0x00143DA8 File Offset: 0x001421A8
		public void ShowAchievementsUI()
		{
			this.ShowAchievementsUI(null);
		}

		// Token: 0x0600439E RID: 17310 RVA: 0x00143DB1 File Offset: 0x001421B1
		public void ShowAchievementsUI(Action<UIStatus> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("ShowAchievementsUI can only be called after authentication.");
				return;
			}
			GooglePlayGames.OurUtils.Logger.d("ShowAchievementsUI callback is " + callback);
			this.mClient.ShowAchievementsUI(callback);
		}

		// Token: 0x0600439F RID: 17311 RVA: 0x00143DE5 File Offset: 0x001421E5
		public void ShowLeaderboardUI()
		{
			GooglePlayGames.OurUtils.Logger.d("ShowLeaderboardUI with default ID");
			this.ShowLeaderboardUI(this.MapId(this.mDefaultLbUi), null);
		}

		// Token: 0x060043A0 RID: 17312 RVA: 0x00143E04 File Offset: 0x00142204
		public void ShowLeaderboardUI(string leaderboardId)
		{
			if (leaderboardId != null)
			{
				leaderboardId = this.MapId(leaderboardId);
			}
			this.mClient.ShowLeaderboardUI(leaderboardId, LeaderboardTimeSpan.AllTime, null);
		}

		// Token: 0x060043A1 RID: 17313 RVA: 0x00143E23 File Offset: 0x00142223
		public void ShowLeaderboardUI(string leaderboardId, Action<UIStatus> callback)
		{
			this.ShowLeaderboardUI(leaderboardId, LeaderboardTimeSpan.AllTime, callback);
		}

		// Token: 0x060043A2 RID: 17314 RVA: 0x00143E30 File Offset: 0x00142230
		public void ShowLeaderboardUI(string leaderboardId, LeaderboardTimeSpan span, Action<UIStatus> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("ShowLeaderboardUI can only be called after authentication.");
				if (callback != null)
				{
					callback(UIStatus.NotAuthorized);
				}
				return;
			}
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"ShowLeaderboardUI, lbId=",
				leaderboardId,
				" callback is ",
				callback
			}));
			this.mClient.ShowLeaderboardUI(leaderboardId, span, callback);
		}

		// Token: 0x060043A3 RID: 17315 RVA: 0x00143E97 File Offset: 0x00142297
		public void SetDefaultLeaderboardForUI(string lbid)
		{
			GooglePlayGames.OurUtils.Logger.d("SetDefaultLeaderboardForUI: " + lbid);
			if (lbid != null)
			{
				lbid = this.MapId(lbid);
			}
			this.mDefaultLbUi = lbid;
		}

		// Token: 0x060043A4 RID: 17316 RVA: 0x00143EBF File Offset: 0x001422BF
		public void LoadFriends(ILocalUser user, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadScores can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			this.mClient.LoadFriends(callback);
		}

		// Token: 0x060043A5 RID: 17317 RVA: 0x00143EF0 File Offset: 0x001422F0
		public void LoadScores(ILeaderboard board, Action<bool> callback)
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.e("LoadScores can only be called after authentication.");
				if (callback != null)
				{
					callback(false);
				}
				return;
			}
			LeaderboardTimeSpan timeSpan;
			switch (board.timeScope)
			{
			case TimeScope.Today:
				timeSpan = LeaderboardTimeSpan.Daily;
				break;
			case TimeScope.Week:
				timeSpan = LeaderboardTimeSpan.Weekly;
				break;
			case TimeScope.AllTime:
				timeSpan = LeaderboardTimeSpan.AllTime;
				break;
			default:
				timeSpan = LeaderboardTimeSpan.AllTime;
				break;
			}
			((PlayGamesLeaderboard)board).loading = true;
			GooglePlayGames.OurUtils.Logger.d(string.Concat(new object[]
			{
				"LoadScores, board=",
				board,
				" callback is ",
				callback
			}));
			this.mClient.LoadScores(board.id, LeaderboardStart.PlayerCentered, (board.range.count <= 0) ? this.mClient.LeaderboardMaxResults() : board.range.count, (board.userScope != UserScope.FriendsOnly) ? LeaderboardCollection.Public : LeaderboardCollection.Social, timeSpan, delegate(LeaderboardScoreData scoreData)
			{
				this.HandleLoadingScores((PlayGamesLeaderboard)board, scoreData, callback);
			});
		}

		// Token: 0x060043A6 RID: 17318 RVA: 0x00144040 File Offset: 0x00142440
		public bool GetLoading(ILeaderboard board)
		{
			return board != null && board.loading;
		}

		// Token: 0x060043A7 RID: 17319 RVA: 0x00144051 File Offset: 0x00142451
		public void RegisterInvitationDelegate(InvitationReceivedDelegate deleg)
		{
			this.mClient.RegisterInvitationDelegate(deleg);
		}

		// Token: 0x060043A8 RID: 17320 RVA: 0x00144060 File Offset: 0x00142460
		internal void HandleLoadingScores(PlayGamesLeaderboard board, LeaderboardScoreData scoreData, Action<bool> callback)
		{
			bool flag = board.SetFromData(scoreData);
			if (flag && !board.HasAllScores() && scoreData.NextPageToken != null)
			{
				int rowCount = board.range.count - board.ScoreCount;
				this.mClient.LoadMoreScores(scoreData.NextPageToken, rowCount, delegate(LeaderboardScoreData nextScoreData)
				{
					this.HandleLoadingScores(board, nextScoreData, callback);
				});
			}
			else
			{
				callback(flag);
			}
		}

		// Token: 0x060043A9 RID: 17321 RVA: 0x00144105 File Offset: 0x00142505
		internal IUserProfile[] GetFriends()
		{
			if (!this.IsAuthenticated())
			{
				GooglePlayGames.OurUtils.Logger.d("Cannot get friends when not authenticated!");
				return new IUserProfile[0];
			}
			return this.mClient.GetFriends();
		}

		// Token: 0x060043AA RID: 17322 RVA: 0x00144130 File Offset: 0x00142530
		private string MapId(string id)
		{
			if (id == null)
			{
				return null;
			}
			if (this.mIdMap.ContainsKey(id))
			{
				string text = this.mIdMap[id];
				GooglePlayGames.OurUtils.Logger.d("Mapping alias " + id + " to ID " + text);
				return text;
			}
			return id;
		}

		// Token: 0x04004B44 RID: 19268
		private static volatile PlayGamesPlatform sInstance;

		// Token: 0x04004B45 RID: 19269
		private static volatile bool sNearbyInitializePending;

		// Token: 0x04004B46 RID: 19270
		private static volatile INearbyConnectionClient sNearbyConnectionClient;

		// Token: 0x04004B47 RID: 19271
		private readonly PlayGamesClientConfiguration mConfiguration;

		// Token: 0x04004B48 RID: 19272
		private PlayGamesLocalUser mLocalUser;

		// Token: 0x04004B49 RID: 19273
		private IPlayGamesClient mClient;

		// Token: 0x04004B4A RID: 19274
		private string mDefaultLbUi;

		// Token: 0x04004B4B RID: 19275
		private Dictionary<string, string> mIdMap = new Dictionary<string, string>();
	}
}
