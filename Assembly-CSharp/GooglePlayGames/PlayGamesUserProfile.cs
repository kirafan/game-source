﻿using System;
using System.Collections;
using GooglePlayGames.OurUtils;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D31 RID: 3377
	public class PlayGamesUserProfile : IUserProfile
	{
		// Token: 0x060043B7 RID: 17335 RVA: 0x00141AC0 File Offset: 0x0013FEC0
		internal PlayGamesUserProfile(string displayName, string playerId, string avatarUrl)
		{
			this.mDisplayName = displayName;
			this.mPlayerId = playerId;
			this.mAvatarUrl = avatarUrl;
			this.mImageLoading = false;
		}

		// Token: 0x060043B8 RID: 17336 RVA: 0x00141AE6 File Offset: 0x0013FEE6
		protected void ResetIdentity(string displayName, string playerId, string avatarUrl)
		{
			this.mDisplayName = displayName;
			this.mPlayerId = playerId;
			if (this.mAvatarUrl != avatarUrl)
			{
				this.mImage = null;
				this.mAvatarUrl = avatarUrl;
			}
			this.mImageLoading = false;
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x060043B9 RID: 17337 RVA: 0x00141B1E File Offset: 0x0013FF1E
		public string userName
		{
			get
			{
				return this.mDisplayName;
			}
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x060043BA RID: 17338 RVA: 0x00141B26 File Offset: 0x0013FF26
		public string id
		{
			get
			{
				return this.mPlayerId;
			}
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x060043BB RID: 17339 RVA: 0x00141B2E File Offset: 0x0013FF2E
		public bool isFriend
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x060043BC RID: 17340 RVA: 0x00141B31 File Offset: 0x0013FF31
		public UserState state
		{
			get
			{
				return UserState.Online;
			}
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x060043BD RID: 17341 RVA: 0x00141B34 File Offset: 0x0013FF34
		public Texture2D image
		{
			get
			{
				if (!this.mImageLoading && this.mImage == null && !string.IsNullOrEmpty(this.AvatarURL))
				{
					Debug.Log("Starting to load image: " + this.AvatarURL);
					this.mImageLoading = true;
					PlayGamesHelperObject.RunCoroutine(this.LoadImage());
				}
				return this.mImage;
			}
		}

		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x060043BE RID: 17342 RVA: 0x00141B9E File Offset: 0x0013FF9E
		public string AvatarURL
		{
			get
			{
				return this.mAvatarUrl;
			}
		}

		// Token: 0x060043BF RID: 17343 RVA: 0x00141BA8 File Offset: 0x0013FFA8
		internal IEnumerator LoadImage()
		{
			if (!string.IsNullOrEmpty(this.AvatarURL))
			{
				WWW www = new WWW(this.AvatarURL);
				while (!www.isDone)
				{
					yield return null;
				}
				if (www.error == null)
				{
					this.mImage = www.texture;
				}
				else
				{
					this.mImage = Texture2D.blackTexture;
					Debug.Log("Error downloading image: " + www.error);
				}
				this.mImageLoading = false;
			}
			else
			{
				Debug.Log("No URL found.");
				this.mImage = Texture2D.blackTexture;
				this.mImageLoading = false;
			}
			yield break;
		}

		// Token: 0x060043C0 RID: 17344 RVA: 0x00141BC4 File Offset: 0x0013FFC4
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			PlayGamesUserProfile playGamesUserProfile = obj as PlayGamesUserProfile;
			return playGamesUserProfile != null && StringComparer.Ordinal.Equals(this.mPlayerId, playGamesUserProfile.mPlayerId);
		}

		// Token: 0x060043C1 RID: 17345 RVA: 0x00141C0C File Offset: 0x0014000C
		public override int GetHashCode()
		{
			return typeof(PlayGamesUserProfile).GetHashCode() ^ this.mPlayerId.GetHashCode();
		}

		// Token: 0x060043C2 RID: 17346 RVA: 0x00141C29 File Offset: 0x00140029
		public override string ToString()
		{
			return string.Format("[Player: '{0}' (id {1})]", this.mDisplayName, this.mPlayerId);
		}

		// Token: 0x04004B52 RID: 19282
		private string mDisplayName;

		// Token: 0x04004B53 RID: 19283
		private string mPlayerId;

		// Token: 0x04004B54 RID: 19284
		private string mAvatarUrl;

		// Token: 0x04004B55 RID: 19285
		private volatile bool mImageLoading;

		// Token: 0x04004B56 RID: 19286
		private Texture2D mImage;
	}
}
