﻿using System;
using GooglePlayGames.BasicApi.Nearby;
using GooglePlayGames.Native;
using GooglePlayGames.Native.Cwrapper;
using GooglePlayGames.OurUtils;
using UnityEngine;

namespace GooglePlayGames
{
	// Token: 0x02000E6D RID: 3693
	public static class NearbyConnectionClientFactory
	{
		// Token: 0x06004C68 RID: 19560 RVA: 0x00154A20 File Offset: 0x00152E20
		public static void Create(Action<INearbyConnectionClient> callback)
		{
			if (Application.isEditor)
			{
				GooglePlayGames.OurUtils.Logger.d("Creating INearbyConnection in editor, using DummyClient.");
				callback(new DummyNearbyConnectionClient());
			}
			GooglePlayGames.OurUtils.Logger.d("Creating real INearbyConnectionClient");
			NativeNearbyConnectionClientFactory.Create(callback);
		}

		// Token: 0x06004C69 RID: 19561 RVA: 0x00154A54 File Offset: 0x00152E54
		private static InitializationStatus ToStatus(NearbyConnectionsStatus.InitializationStatus status)
		{
			switch (status + 4)
			{
			case (NearbyConnectionsStatus.InitializationStatus)0:
				return InitializationStatus.VersionUpdateRequired;
			case (NearbyConnectionsStatus.InitializationStatus)2:
				return InitializationStatus.InternalError;
			case (NearbyConnectionsStatus.InitializationStatus)5:
				return InitializationStatus.Success;
			}
			GooglePlayGames.OurUtils.Logger.w("Unknown initialization status: " + status);
			return InitializationStatus.InternalError;
		}
	}
}
