﻿using System;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace GooglePlayGames
{
	// Token: 0x02000D2C RID: 3372
	internal class PlayGamesAchievement : IAchievement, IAchievementDescription
	{
		// Token: 0x0600432D RID: 17197 RVA: 0x00142A50 File Offset: 0x00140E50
		internal PlayGamesAchievement() : this(new ReportProgress(PlayGamesPlatform.Instance.ReportProgress))
		{
		}

		// Token: 0x0600432E RID: 17198 RVA: 0x00142A68 File Offset: 0x00140E68
		internal PlayGamesAchievement(ReportProgress progressCallback)
		{
			this.mProgressCallback = progressCallback;
		}

		// Token: 0x0600432F RID: 17199 RVA: 0x00142AD0 File Offset: 0x00140ED0
		internal PlayGamesAchievement(Achievement ach) : this()
		{
			this.mId = ach.Id;
			this.mIsIncremental = ach.IsIncremental;
			this.mCurrentSteps = ach.CurrentSteps;
			this.mTotalSteps = ach.TotalSteps;
			if (ach.IsIncremental)
			{
				if (ach.TotalSteps > 0)
				{
					this.mPercentComplete = (double)ach.CurrentSteps / (double)ach.TotalSteps * 100.0;
				}
				else
				{
					this.mPercentComplete = 0.0;
				}
			}
			else
			{
				this.mPercentComplete = ((!ach.IsUnlocked) ? 0.0 : 100.0);
			}
			this.mCompleted = ach.IsUnlocked;
			this.mHidden = !ach.IsRevealed;
			this.mLastModifiedTime = ach.LastModifiedTime;
			this.mTitle = ach.Name;
			this.mDescription = ach.Description;
			this.mPoints = ach.Points;
			this.mRevealedImageUrl = ach.RevealedImageUrl;
			this.mUnlockedImageUrl = ach.UnlockedImageUrl;
		}

		// Token: 0x06004330 RID: 17200 RVA: 0x00142BED File Offset: 0x00140FED
		public void ReportProgress(Action<bool> callback)
		{
			this.mProgressCallback(this.mId, this.mPercentComplete, callback);
		}

		// Token: 0x06004331 RID: 17201 RVA: 0x00142C08 File Offset: 0x00141008
		private Texture2D LoadImage()
		{
			if (this.hidden)
			{
				return null;
			}
			string text = (!this.completed) ? this.mRevealedImageUrl : this.mUnlockedImageUrl;
			if (!string.IsNullOrEmpty(text))
			{
				if (this.mImageFetcher == null || this.mImageFetcher.url != text)
				{
					this.mImageFetcher = new WWW(text);
					this.mImage = null;
				}
				if (this.mImage != null)
				{
					return this.mImage;
				}
				if (this.mImageFetcher.isDone)
				{
					this.mImage = this.mImageFetcher.texture;
					return this.mImage;
				}
			}
			return null;
		}

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06004332 RID: 17202 RVA: 0x00142CBF File Offset: 0x001410BF
		// (set) Token: 0x06004333 RID: 17203 RVA: 0x00142CC7 File Offset: 0x001410C7
		public string id
		{
			get
			{
				return this.mId;
			}
			set
			{
				this.mId = value;
			}
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06004334 RID: 17204 RVA: 0x00142CD0 File Offset: 0x001410D0
		public bool isIncremental
		{
			get
			{
				return this.mIsIncremental;
			}
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06004335 RID: 17205 RVA: 0x00142CD8 File Offset: 0x001410D8
		public int currentSteps
		{
			get
			{
				return this.mCurrentSteps;
			}
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06004336 RID: 17206 RVA: 0x00142CE0 File Offset: 0x001410E0
		public int totalSteps
		{
			get
			{
				return this.mTotalSteps;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06004337 RID: 17207 RVA: 0x00142CE8 File Offset: 0x001410E8
		// (set) Token: 0x06004338 RID: 17208 RVA: 0x00142CF0 File Offset: 0x001410F0
		public double percentCompleted
		{
			get
			{
				return this.mPercentComplete;
			}
			set
			{
				this.mPercentComplete = value;
			}
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x06004339 RID: 17209 RVA: 0x00142CF9 File Offset: 0x001410F9
		public bool completed
		{
			get
			{
				return this.mCompleted;
			}
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600433A RID: 17210 RVA: 0x00142D01 File Offset: 0x00141101
		public bool hidden
		{
			get
			{
				return this.mHidden;
			}
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x0600433B RID: 17211 RVA: 0x00142D09 File Offset: 0x00141109
		public DateTime lastReportedDate
		{
			get
			{
				return this.mLastModifiedTime;
			}
		}

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x0600433C RID: 17212 RVA: 0x00142D11 File Offset: 0x00141111
		public string title
		{
			get
			{
				return this.mTitle;
			}
		}

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x0600433D RID: 17213 RVA: 0x00142D19 File Offset: 0x00141119
		public Texture2D image
		{
			get
			{
				return this.LoadImage();
			}
		}

		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x0600433E RID: 17214 RVA: 0x00142D21 File Offset: 0x00141121
		public string achievedDescription
		{
			get
			{
				return this.mDescription;
			}
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x0600433F RID: 17215 RVA: 0x00142D29 File Offset: 0x00141129
		public string unachievedDescription
		{
			get
			{
				return this.mDescription;
			}
		}

		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x06004340 RID: 17216 RVA: 0x00142D31 File Offset: 0x00141131
		public int points
		{
			get
			{
				return (int)this.mPoints;
			}
		}

		// Token: 0x04004B27 RID: 19239
		private readonly ReportProgress mProgressCallback;

		// Token: 0x04004B28 RID: 19240
		private string mId = string.Empty;

		// Token: 0x04004B29 RID: 19241
		private bool mIsIncremental;

		// Token: 0x04004B2A RID: 19242
		private int mCurrentSteps;

		// Token: 0x04004B2B RID: 19243
		private int mTotalSteps;

		// Token: 0x04004B2C RID: 19244
		private double mPercentComplete;

		// Token: 0x04004B2D RID: 19245
		private bool mCompleted;

		// Token: 0x04004B2E RID: 19246
		private bool mHidden;

		// Token: 0x04004B2F RID: 19247
		private DateTime mLastModifiedTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);

		// Token: 0x04004B30 RID: 19248
		private string mTitle = string.Empty;

		// Token: 0x04004B31 RID: 19249
		private string mRevealedImageUrl = string.Empty;

		// Token: 0x04004B32 RID: 19250
		private string mUnlockedImageUrl = string.Empty;

		// Token: 0x04004B33 RID: 19251
		private WWW mImageFetcher;

		// Token: 0x04004B34 RID: 19252
		private Texture2D mImage;

		// Token: 0x04004B35 RID: 19253
		private string mDescription = string.Empty;

		// Token: 0x04004B36 RID: 19254
		private ulong mPoints;
	}
}
