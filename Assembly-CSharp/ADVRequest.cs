﻿using System;
using ADVRequestTypes;
using Meige;

// Token: 0x02000B5A RID: 2906
public static class ADVRequest
{
	// Token: 0x06003DCF RID: 15823 RVA: 0x00138150 File Offset: 0x00136550
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/adv/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("advId", param.advId);
		meigewwwParam.Add("stepCode", param.stepCode);
		return meigewwwParam;
	}

	// Token: 0x06003DD0 RID: 15824 RVA: 0x00138198 File Offset: 0x00136598
	public static MeigewwwParam Add(string advId, int stepCode, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/adv/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("advId", advId);
		meigewwwParam.Add("stepCode", stepCode);
		return meigewwwParam;
	}
}
