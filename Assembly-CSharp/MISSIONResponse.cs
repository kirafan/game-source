﻿using System;
using System.Collections.Generic;
using Meige;
using MISSIONResponseTypes;
using WWWTypes;

// Token: 0x02000C00 RID: 3072
public static class MISSIONResponse
{
	// Token: 0x06003F76 RID: 16246 RVA: 0x0013C6D0 File Offset: 0x0013AAD0
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F77 RID: 16247 RVA: 0x0013C6E8 File Offset: 0x0013AAE8
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F78 RID: 16248 RVA: 0x0013C700 File Offset: 0x0013AB00
	public static Complete Complete(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Complete>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F79 RID: 16249 RVA: 0x0013C718 File Offset: 0x0013AB18
	public static Refresh Refresh(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Refresh>(param, dialogType, acceptableResultCodes);
	}
}
