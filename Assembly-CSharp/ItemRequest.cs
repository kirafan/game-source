﻿using System;
using ItemRequestTypes;
using Meige;

// Token: 0x02000B62 RID: 2914
public static class ItemRequest
{
	// Token: 0x06003E09 RID: 15881 RVA: 0x00138EE4 File Offset: 0x001372E4
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("item/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", param.type);
		return meigewwwParam;
	}

	// Token: 0x06003E0A RID: 15882 RVA: 0x00138F18 File Offset: 0x00137318
	public static MeigewwwParam Getall(string type, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("item/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", type);
		return meigewwwParam;
	}

	// Token: 0x06003E0B RID: 15883 RVA: 0x00138F48 File Offset: 0x00137348
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("item/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("itemId", param.itemId);
		return meigewwwParam;
	}

	// Token: 0x06003E0C RID: 15884 RVA: 0x00138F7C File Offset: 0x0013737C
	public static MeigewwwParam Get(string itemId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("item/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("itemId", itemId);
		return meigewwwParam;
	}
}
